@extends('layouts.main')
@section('content')
   <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">   
              <div class="row row-cards">
                <div class="col-12">
                  <form id="addquestions" method="POST" enctype="multipart/form-data"  action="{{url('/save_questions')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="card-header">
                      <h4 class="card-title">Add Questions</h4>
                    </div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                   @endif
                    <div class="card-body">
                     
                          <div class="row">
                            <div class="col-md-6 col-xl-6">

                              <div class="mb-3">
                              <input type="radio" name="quesType" value="Individual Questions" id="questype1">  <label >Individual Questions</label> 
                              <input type="radio" name="quesType" value="Excel Upload" id="questype2">  <label >Excel Upload</label>
                           
                              </div>
</div>
</div>

                              <div id="individual_questions" style="display: none;">
                              <div class="row">
                              <div class="mb-3">
                                <label class="form-label">Question<sup>*</sup></label>
                                <textarea class="form-control" name="question" id="question" rows="4"></textarea>
                              </div>
                             </div>
                    <div class="row">
                                 <div class="col-5 col-sm-3">
                                <label class="form-label">Option A<sup>*</sup></label>
                                <input type="text" class="form-control" name="optionA" id="optionA" placeholder="Answer">
                                <input type="radio" name="answer" value="A" id="optionA"> 
                              </div>

                              <div class="col-5 col-sm-3">
                                <label class="form-label">Option B<sup>*</sup></label>
                                <input type="text" class="form-control" name="optionB" id="optionB" placeholder="Answer">
                                <input type="radio" name="answer" value="B" id="optionB"> 
                              </div>
                              <div class="col-5 col-sm-3">
                                <label class="form-label">Option C<sup>*</sup></label>
                                <input type="text" class="form-control" name="optionC" id="optionC" placeholder="Answer">
                                <input type="radio" name="answer" value="C" id="optionC"> 
                              </div>
                              <div class="col-5 col-sm-3">
                                <label class="form-label">Option D<sup>*</sup></label>
                                <input type="text" class="form-control" name="optionD" id="optionD" placeholder="Answer">
                                <input type="radio" name="answer" value="D" id="optionD"> 
                              </div>
</div>

                           </div>
                          <div id="excel_questions" style="display: none;">
                          <a href="{{url('/public/uploads/questions/QASample.csv')}}" download="Questions">Download Sample Excel Template</a>
                        
                            
                               <div class="file-upload-custom mt-3 mb-3">
                              <input style="height:inherit;" class="form-control" type="file" name="file">
                              <button type="submit" class="btn btn-blue btn-sm text-white">Upload</button>
</div>
                          
                             
</div>    
                          
                      <!--    </div>-->

                          

                        </div>
                      
                  </div>
                  <div class="card-footer text-end">
                    <div class="d-flex">
                      
                      <button type="submit" class="btn btn-primary ms-auto" style="float:left">Submit</button>
                    </div>
                  </div>
                </form>

              
                </div>
           
            </div>


          
@endsection
