@extends('layouts.main_dash')

@section('content')
        <!-- Page body -->
        <div class="page-body">
            <div class="container">



              <div class="row">



    <h2>Revenue {{ date('F Y') }}</h2>

    <div class="row">

      <div class="col-lg-6">
        <a  class="dash-grid" href="{{url('/monthlyrevenuewithgst_chry')}}">
        <div class="grid-itm  shadow-sm">
          <div class="d-flex w-100 justify-content-between align-items-center mb-3">
            <div class="subheader"><i class="bi bi-currency-rupee"></i>Chrysalis and Inspire With GST
             </div>
            <div class="h1 big">Rs {{number_format($monthrevenue_chry_withgst,2)}} </div>
          </div> 
          <div class="d-flex w-100 justify-content-between align-items-center main-tax">

            <div class="d-flex align-items-center">
              <div class="subheader  me-5">Tax</div>
              <div class="h1 mb-0">RS {{number_format($monthrevenue_chry_tax, 2)}}</div>
            </div>
          </div>       
        </div>
      </a>
      </div>
      <div class="col-lg-6">
        <a  class="dash-grid"  href="{{url('/monthlyrevenuewithoutgst_chry')}}">
        <div class="grid-itm  shadow-sm">
          <div class="d-flex w-100 justify-content-between align-items-center mb-3">
            <div class="subheader"><i class="bi bi-currency-rupee"></i>Chrysalis and Inspire Without GST
             </div>
            <div class="h1 big">Rs {{number_format($monthrevenue_chry_withoutgst,2)}} </div>
          </div> 
          
          <div class="d-flex w-100 justify-content-between align-items-center main-tax">

            <div class="d-flex align-items-center">
              <div class="subheader  me-5">Tax</div>
              <div class="h1 mb-0">RS {{number_format($monthrevenue_chry_tax, 2)}}</div>
            </div>
          </div>
        </div>
      </a>
      </div>

      <div class="col-lg-6">
        <a  class="dash-grid" href="{{url('/monthlyrevenuewithgst_regular')}}">
        <div class="grid-itm  shadow-sm">
          <div class="d-flex w-100 justify-content-between align-items-center mb-3">
            <div class="subheader"><i class="bi bi-currency-rupee"></i>Regular Admissions With GST
             </div>
            <div class="h1 big">Rs {{number_format($monthrevenue_regold_withgst,2)}} </div>
          </div>  
          <div class="d-flex w-100 justify-content-between align-items-center main-tax">

            <div class="d-flex align-items-center">
              <div class="subheader  me-5">Tax</div>
              <div class="h1 mb-0">RS {{number_format($monthrevenue_regold_tax, 2)}}</div>
            </div>
          </div>
          
        </div>
      </a>
      </div>

      <div class="col-lg-6">
        <a  class="dash-grid" href="{{url('/monthlyrevenuewithoutgst_regular')}}">
        <div class="grid-itm  shadow-sm">
          <div class="d-flex w-100 justify-content-between align-items-center mb-3">
            <div class="subheader"><i class="bi bi-currency-rupee"></i>Regular Admissions Without GST
             </div>
            <div class="h1 big">Rs {{number_format($monthrevenue_regold_withoutgst,2)}} </div>
          </div> 
          
          <div class="d-flex w-100 justify-content-between align-items-center main-tax">

            <div class="d-flex align-items-center">
              <div class="subheader  me-5">Tax</div>
              <div class="h1 mb-0">RS {{number_format($monthrevenue_regold_tax, 2)}}</div>
            </div>
          </div>
        </div>

     
      </a>
      </div>
      <div class="col-lg-6">
        <a  class="dash-grid" href="{{url('/totalrevenue_withgst')}}">
        <div class="grid-itm  shadow-sm">
          <div class="d-flex w-100 justify-content-between align-items-center mb-3">
            <div class="subheader"><i class="bi bi-currency-rupee"></i>Total Revenue With GST  
             </div>
            <div class="h1 big">Rs {{number_format($monthlytotal_withgst,2)}} </div>
          </div>
          
          <div class="d-flex w-100 justify-content-between align-items-center main-tax">

            <div class="d-flex align-items-center">
              <div class="subheader  me-5">Tax</div>
              <div class="h1 mb-0">RS {{number_format($total_tax, 2)}}</div>
            </div>
          </div>
          

        </div>
      </a>
      </div>
      <div class="col-lg-6">
        <a  class="dash-grid" href="{{url('/totalrevenue_withoutgst')}}">
        <div class="grid-itm  shadow-sm">
          <div class="d-flex w-100 justify-content-between align-items-center mb-3">
            <div class="subheader"><i class="bi bi-currency-rupee"></i>Total Revenue Without GST
             </div>
            <div class="h1 big">Rs {{number_format($monthlytotal_withoutgst,2)}} </div>
          </div> 
          
          <div class="d-flex w-100 justify-content-between align-items-center main-tax">

            <div class="d-flex align-items-center">
              <div class="subheader  me-5">Tax</div>
              <div class="h1 mb-0">RS {{number_format($total_tax, 2)}}</div>
            </div>
          </div>
        </div>
      </a>
      </div>

    </div>

    <h2>Admission {{ date('F Y') }}</h2>

    <div class="row">

      <div class="col-lg-6">
        <a  class="dash-grid" href="{{url('/monthlyadmissionwithgst_chrysalis')}}">
        <div class="grid-itm  shadow-sm">
          <div class="d-flex w-100 justify-content-between align-items-center mb-3">
            <div class="subheader"><i class="bi bi-currency-rupee"></i>Chrysalis and Inspire With GST 
             </div>
            <div class="h1 big">Rs {{number_format($monthadmi_chry_withgst,2)}} </div>
          </div> 

            <div class="d-flex w-100 justify-content-between align-items-center main-tax">

              <div class="d-flex align-items-center">
                <div class="subheader  me-5">Tax</div>
                <div class="h1 mb-0">RS {{number_format($monthadmi_chry_tax, 2)}}</div>
              </div>
            </div>       
        </div>
      </a>
      </div>
      <div class="col-lg-6">
        <a class="dash-grid" href="{{url('/monthlyadmissionwithoutgst_chrysalis')}}">
          <div class="grid-itm  shadow-sm">
            <div class="d-flex w-100 justify-content-between align-items-center mb-3">
              <div class="subheader"><i class="bi bi-currency-rupee"></i>Chrysalis and Inspire Without GST</div>
              <div class="h1 big mb-0" style="color:red; font:300"> Rs {{number_format($monthadmi_chry_withoutgst, 2)}}</div>
            </div>
            <div class="d-flex w-100 justify-content-between align-items-center main-tax">

              <div class="d-flex align-items-center">
                <div class="subheader  me-5">Tax</div>
                <div class="h1 mb-0">RS {{number_format($monthadmi_chry_tax, 2)}}</div>
              </div>
            </div>
          </div>
      </a>
      </div>
      <div class="col-lg-6">
        <a  class="dash-grid" href="{{url('/monthlyadmissionwithgst_regular')}}">
        <div class="grid-itm  shadow-sm">
          <div class="d-flex w-100 justify-content-between align-items-center mb-3">
            <div class="subheader"><i class="bi bi-currency-rupee"></i>Regular Admissions With GST
             </div>
            <div class="h1 big">Rs {{number_format($monthly_tot_admissionwithgst,2)}} </div>
          </div>
          
          <div class="d-flex w-100 justify-content-between align-items-center main-tax">

            <div class="d-flex align-items-center">
              <div class="subheader  me-5">Tax</div>
              <div class="h1 mb-0">RS {{number_format($monthly_tot_admissiontax, 2)}}</div>
            </div>
          </div>
        </div>
      </a>
      </div>

      <div class="col-lg-6">
        <a class="dash-grid" href="{{url('/monthlyadmissionwithoutgst_regular')}}">
          <div class="grid-itm  shadow-sm">
            <div class="d-flex w-100 justify-content-between align-items-center mb-3">
              <div class="subheader"><i class="bi bi-currency-rupee"></i>Regular Admissions Without GST </div>
              <div class="h1 big mb-0" style="color:red; font:300"> Rs {{number_format($monthly_tot_admissionwithoutgst, 2)}}</div>
            </div>
            <div class="d-flex w-100 justify-content-between align-items-center main-tax">

              <div class="d-flex align-items-center">
                <div class="subheader  me-5">Tax</div>
                <div class="h1 mb-0">RS {{number_format($monthly_tot_admissiontax, 2)}}</div>
              </div>
            </div>
          </div>
      </a>
      </div>


      <div class="col-lg-6">
        <a  class="dash-grid" href="{{url('/totaladmission_withgst')}}">
        <div class="grid-itm  shadow-sm">
          <div class="d-flex w-100 justify-content-between align-items-center mb-3">
            <div class="subheader"><i class="bi bi-currency-rupee"></i>Total Admissions With GST
             </div>
            <div class="h1 big">Rs {{number_format($total_reg_chry_old_withgst,2)}} </div>
          </div> 
          
          <div class="d-flex w-100 justify-content-between align-items-center main-tax">

            <div class="d-flex align-items-center">
              <div class="subheader  me-5">Tax</div>
              <div class="h1 mb-0">RS {{number_format($total_reg_chry_old_tax, 2)}}</div>
            </div>
          </div>
        </div>
      </a>
      </div>

      <div class="col-lg-6">
        <a class="dash-grid" href="{{url('/totaladmission_withoutgst')}}">
          <div class="grid-itm  shadow-sm">
            <div class="d-flex w-100 justify-content-between align-items-center mb-3">
              <div class="subheader"><i class="bi bi-currency-rupee"></i>Total Admissions Without GST </div>
              <div class="h1 big mb-0" style="color:red; font:300"> Rs {{number_format($total_reg_chry_old_withoutgst, 2)}}</div>
            </div>
            <div class="d-flex w-100 justify-content-between align-items-center main-tax">

              <div class="d-flex align-items-center">
                <div class="subheader  me-5">Tax</div>
                <div class="h1 mb-0">RS {{number_format($total_reg_chry_old_tax, 2)}}</div>
              </div>
            </div>
          </div>
      </a>
      </div>




    </div>



@endsection
