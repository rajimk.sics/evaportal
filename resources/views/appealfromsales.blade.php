@extends('layouts.main')

@section('content')

    <!-- Page body -->
    <div class="page-body">
        <div class="container-xl">

            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

            <div class="card">
                <div class="card-body">


                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                            <li><a href="{{ url('/home') }}">Home</a></li>
                            <li><a href="{{ url('/list_appealfromsales') }}">{{ $title }}</a></li>
                        </ol>
                    </nav>

                    <h4 class="card-title">{{ $title }}</h4>


                  
                        <form id="appealmaster_form" method="GET" enctype="multipart/form-data" action="{{ url('/list_appealfromsales') }}">
                            <div class="row">
                    <div class="col">
                        <div class="form-group formcontents">
                            <label for="year">Select Sales Person<sup>*</sup></label>
                            <select class="form-select" name="salesperson" id="salesperson">
                              <option value="">Select Sales Person</option>
                              @foreach($saleslist as $sales)
                              <option value="{{$sales->id }}" {{ request('salesperson') == $sales->id ? 'selected' : '' }}>{{ $sales->name }}</option>
                              @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col d-flex align-items-center">
                        <div class="form-group formcontents mb-0 flex-row">
                            <button type="submit" class="btn btn-primary me-2">Submit</button>
                            <a href="{{ url('/list_appealfromsales') }}" class="btn btn-primary">Clear All</a>
                           
                        </div>
                    </div>
                   
                            </div>
                            
            </form>
            <h4 style="text-align: center; color: blue; font-weight: bold;">
                Total number of appeal 
                @if(!empty($salesName))
                    for {{ $salesName->name }}
                @endif 
                is {{ $count }}
            </h4>
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Sales Person</th>
                                    <th>From Date</th>
                                    <th>To Date   </th>
                                    <th>College</th>
                                    <th>Department</th>
                                    <th>POC</th>
                                    <th>Comment</th>
                                    <th>Supported Document</th>
                                    <th>Status</th>
                                    <th>Appeal Reason</th>
                                    <th>Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach ($listings as $listing)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{$listing->name}}</td>
                                        <td>{{date("d-m-Y", strtotime($listing->from_date))}}</td>
                                        <td>{{date("d-m-Y", strtotime($listing->to_date))}}</td>
                                        <td>{{$listing->college}}</td>
                                        <td>{{$listing->dept}}</td>
                                        <td>{{$listing->department_pocs}}</td>
                                        <td>{{$listing->comments}}</td>
                                        <td> @if (!empty($listing->supporting_documents))
                                            @foreach (explode(',', $listing->supporting_documents) as $fileName)
                                                <a href="{{ url('/public/uploads/masterdocuments/' . $fileName) }}" target="_blank" class="view-document">{{ $fileName }}</a><br>
                                            @endforeach
                                        @else
                                            No documents uploaded
                                        @endif
                                    </td>

                                     <td>
                                        @if ($listing->status == 1)
                                        <span style="word-wrap: break-word; max-width: 150px;color:green;display:block;" id="act_{{ $listing->id }}">Approved<br>Comment:{{$listing->master_comment}}</span>
                                       
                                    @elseif ($listing->status == 2)
                                        <span style="word-wrap: break-word; max-width: 150px;color:red;display:block;" id="deact_{{ $listing->id }}">Rejected<br>Reason:{{$listing->reason}}</span>
                                    @endif
                                        <span style="word-wrap: break-word; max-width: 150px;color:red;display:none;" id="deact{{ $listing->id }}"></span>
                                 
                                    </td>
                                    <td>
                                    <span style="word-wrap: break-word; max-width: 150px;color:red;display:block;" id="appeal{{ $listing->id }}">Reason:{{$listing->appeal_reason}}</span>
                                    </td>
                                        <td>
                                            <button type="button" class="btn btn-cyan btn-sm text-white" fdprocessedid="wf07gv"  onclick="view_updateDetails('{{ $listing->id }}')">
                                                View update details
                                            </button>
                                            {{-- <button type="button" class="btn btn-blue btn-sm text-white" fdprocessedid="wf07gv" onclick="fetchClaimDetails('{{ $listing->id }}')">
                                                View Claimed Details
                                            </button><br> --}}
                                         
                                            @if ($listing->appeal_status == 1 )
                                           
                                            <button type="button" class="btn btn-success btn-sm text-white" id="approve-btn-{{ $listing->id }}" onclick="approveReqAppeal('{{ $listing->id }}')">Approve</button>         
                                            <button type="button" class="btn btn-danger btn-sm text-white" id="reject-btn-{{ $listing->id }}" onclick="rejectReqAppeal('{{ $listing->id }}')">Reject</button>
                                        @endif
 
                                       
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                @endforeach

                            </tbody>

                        </table>



                    </div>
                </div>

            </div>

            </div>
            </div>

                        <!-- Modal Structure -->
                        <div class="modal fade" id="collegeUpdateModal" tabindex="-1" role="dialog" aria-labelledby="collegeModalLabel">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="collegeModalLabel">Update Details</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                        
                                        <div id="modalContent">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            

                        <div class="modal modal-blur fade" data-bs-backdrop="static" id="modal-masterapprove" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title">Approve Request</h5>
                                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                  <form id="approveRequestAppeal"  enctype="multipart/form-data">
                                    <input type="hidden" name="_token" id="token1" value="{{ csrf_token() }}">
                                  
                                    <input type="hidden" name="reqid" id="reqid">
                                    <div class="mb-3">
                                      <label class="form-label">From Date</label>
                                      <input type="text" class="form-control"  id="editfromdate" name="editfromdate"
                                        readonly>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">To Date</label>
                                        <input type="text" class="form-control" style="width:auto;" id="edittodate" name="edittodate"
                                          data-zdp_readonly_element="true">
                                      </div>
                                    <div class="mb-3">
                                      <label class="form-label">Comments<sup>*</sup></label>
                                      <textarea class="form-control" rows="5" name="mcomments" id="mcomments"></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="submit" class="btn btn-primary">Approve</button>
                                </div>
                              </div>
                            </div>
                            </form>
                          </div>



                          <div id="claimDetailsModal" class="modal fade" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="claimDetailsModalLabel">Claim Details</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body d-flex flex-column">
                                       
                                        <h4 id="tableHeading" class="text-center mb-3"></h4>
                        
                                       
                                        <div class="table-responsive overflow-auto">
                                            <table id="claimDetailsTable" class="table table-bordered table-striped mb-0">
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th>Package Name</th>
                                                        <th>Package Amount</th>
                                                        <th>Package Tax</th>
                                                        <th>Full Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                  
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
            <script>
                @if (Session::has('message'))
                    swal({
                        title: "",
                        text: "{{ Session::get('message') }}",
                        type: "success",
                        showCancelButton: false,
                        dangerMode: false,
                        confirmButtonText: 'OK',
                    });
                @endif
            </script>
        @endsection
