@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <nav aria-label="breadcrumb">
                <ol class="breadcrumbs">
                  <li><a href="{{url('/home')}}">Home</a></li>
                  <li><a href="{{url('/manage_college')}}">Settings - College</a></li>
                  <li><a href="#">Edit College</a></li>
                </ol>
              </nav>
    
    
             <form id="editcollege" method="POST" enctype="multipart/form-data"  action="{{url('/edit_college')}}">
              <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
            
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                <label class="form-label">College<sup>*</sup></label>
               <input type="hidden" class="form-control" name="editcollegeid" id="editcollegeid" value="{{$college_id}}" placeholder="College">

               <input type="text" class="form-control" name="editcolname" id="editcolname" value="{{$college_name}}" placeholder="College">
             </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
              <label class="form-label">State<sup>*</sup></label>
              <select class="form-select selecttype selectclass collegestate" id="editcollegestate" name="editcollegestate" >
                <option value="">Select State</option>
                @foreach($states as $state)
                <option value="{{ $state->id }}" {{ $state->id == $college_state ? 'selected' : '' }}>{{ $state->name }}</option>
               @endforeach
              </select>
           </div>
              </div>
            </div>
            <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <div id="initial-data" data-district-id="{{ $college_district }}"></div>

                <label class="form-label">District<sup>*</sup></label>
                <select class="form-select selecttype selectclass collegedist" id="editcollegedist" name="editcollegedist" >
                  @foreach($districts as $districts)
                  <option value="{{ $districts->id }}" {{ $districts->id == $college_district ? 'selected' : '' }}>{{ $districts->name }}</option>
                 @endforeach
                  
                </select>
              </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                  <label class="form-label">Place<sup>*</sup></label>
                  <input type="text" class="form-control" name="collegeplace" id="collegeplace" value="{{$college_place}}"  placeholder="College Place">
                  
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6">   
                   <div class="form-group">
                      <label class="form-label">Department<sup>*</sup></label>
                      <select class="form-select selecttype selectclass" id="editcollegedep" name="editcollegedep[]" multiple="multiple">
                       
                        @foreach($departments as $department)
                          <option value="{{$department->id}}"  
                                {{ in_array($department->id, $selected_department_ids) ? 'selected' : '' }}>
                                  {{$department->department}}</option>
                         @endforeach
                      </select>
                      </div>
                     </div>
                    </div> 
                    
                      <div class="d-flex">
                        <button type="submit" class="btn btn-primary">Update</button>
                  </div>
         
          
          </form>
                    </div>
                </div>
         




@endsection
