@extends('layouts.main')

@section('content')

<!-- Page body -->
<div class="page-body">
    <div class="container-xl">
        <!-- CSRF Token -->
        <input type="hidden" name="token_eva" id="token_eva" value="{{ csrf_token() }}">

        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumbs">
                        <li><a href="{{ url('/home') }}">Home</a></li>
                        <li><a href="#">{{ $title }}</a></li>
                    </ol>
                </nav>
                <h4 class="card-title">{{ $title }}</h4>

                <div class="payment-grd">
                    <form id="searchchrys_form" method="GET" enctype="multipart/form-data" action="{{ url('/pending_payment') }}">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                        <div class="row">
                            <!-- Year Dropdown -->
                            <div class="col-lg-6 formcontents">
                                <label for="year">Select Year<sup>*</sup></label>
                                <select class="form-select" name="year" id="year">
                                    <option value="">Select Year</option>
                                    @foreach($years as $year)
                                    <option value="{{ $year }}" {{ $year == $currentYear ? 'selected' : '' }}>{{ $year }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <!-- Month Dropdown -->
                            <div class="col-lg-6 formcontents">
                                <label for="month">Select Month<sup>*</sup></label>
                                <select class="form-select" name="month" id="month">
                                    <option value="">Select Month</option>
                                    @foreach($months as $key => $month)
                                    <option value="{{ $key }}" {{ $key == $currentMonth ? 'selected' : '' }}>{{ $month }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col formcontents">
                                <button type="submit" class="btn btn-primary" style="margin-top: 26px;">Submit</button>
                                </div>

                                <div class="col formcontents"> 
                                <a href="{{url('/pending_payment/')}}" class="btn btn-primary"  style="margin-top: 26px;"> View Current Month</a>
                                </div>




                        </div>
                    </form>
                </div>

                <div class="payment-grd">
                    <p><b>Total Payment with GST:</b> <span>Rs {{ number_format($total_payment_withgst, 2) }}</span></p>
                    <p><b>Total Tax:</b> <span>Rs {{ number_format($total_tax, 2) }}</span></p>
                    <p><b>Total Payment:</b> <span>Rs {{ number_format($total_payment, 2) }}</span></p>
                </div>

                <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Student Name</th>
                                <th>Closed By</th>
                                <th>Payment Type</th>
                                <th>Transaction Id</th>
                                <th>Screenshot</th>
                                <th>Payment with GST</th>
                                <th>Tax</th>
                                <th>Total Payment</th>
                                <th>Date</th>

                                @if(Auth::user()->role==7)
                                <th>Action</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($payment_history as $history)
                            <tr id="row_{{$i}}">
                                <td>{{ $i }}</td>
                                <td>{{ ucfirst($history->name) }}</td>

                                @if($history->source=='payment')
                                    @php
                                    $empnames=App\Helpers\CustomHelper::empnames($history->payment_id); 
                                    @endphp
                                     <td>{{ $empnames }}</td>
                                @else
                                @php
                                $empnames=App\Helpers\CustomHelper::uname($history->sales_id); 
                                @endphp
                                 <td>{{ $empnames->name }}</td>
                                @endif

                                <td>
                                    @if($history->payment_type == 'bank')
                                    Bank Transfer
                                    @else
                                    Cash
                                    @endif
                                </td>
                                <td>{{ $history->transaction_id }}</td>
                                <td>
                                    @if($history->screenshot)
                                    <a href="#" data-bs-toggle="modal" data-bs-target="#imageModal" class="prviewimage" data-bs-image="{{ url('public/uploads/screenshot/' . $history->screenshot) }}" >
                                        <img src="{{ url('public/uploads/screenshot/' . $history->screenshot) }}" width="100" height="100" alt="Screenshot">
                                    </a>
                                    @endif
                                </td>
                                <td>{{ number_format($history->payment_gst, 2) }}</td>
                                <td>{{ number_format($history->tax, 2) }}</td>
                                <td>{{ number_format($history->payment, 2) }}</td>
                                <td>{{ date('d-m-Y', strtotime($history->date)) }}</td>
                                @if(Auth::user()->role==7)
                                <td>
                                  <button type="button" id="app_{{$i}}"  class="btn btn-success btn-sm text-white" fdprocessedid="pvth1" onclick="accountsapprove('{{$i}}','{{$history->payment_id}}','{{$history->source}}')">
                                  Approve
                               </button> 
                                <button type="button" id="rej_{{$i}}"  class="btn btn-danger btn-sm text-white" fdprocessedid="pvth1" onclick="accountsreject('{{$i}}','{{$history->payment_id}}','{{$history->source}}','{{$history->payment_type}}')">
                                  Reject
                                </button>
                              </td>
                              @endif

                            </tr>
                            <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <form id="paymentreject" method="POST" enctype="multipart/form-data"  action="{{url('/accountsreject')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="modal modal-blur fade" id="modal-payreject" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Reason for Rejection</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <div class="mb-3">
                    <label class="form-label">Select Reason Type<sup>*</sup></label>
                          <select class="form-select" name="reason_type"  id="reason_type" placeholder="Reason Types">
                            <option value="">Select Reason Type</option>
                    
                            <option value="1">Screenshot not correct</option>
                            <option value="2">Transaction ID not correct</option>
                            <option value="3">Amount Mismatch</option>
                           
                           
                          </select>
                  </div>
                <div class="mb-3">
                <input type="hidden" class="form-control"  name="rowid" id="rowid" value="">
                <input type="hidden" class="form-control"  name="payment_id" id="payment_id" value="">
                <input type="hidden" class="form-control"  name="source" id="source" value="">
                <label class="form-label">Please provide a reason for your action<sup>*</sup></label>
                <input type="text" class="form-control"  name="reason" id="reason" >
                </div>
           
              
            
            <div class="modal-footer">
              <button type="submit"  class="btn btn-primary btn-reject">Reject</button>
            </div>                                            
            </div>
          
          </div>
          </div>
          </form>

        <!-- Modal for Image Preview -->
        <div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="imageModalLabel">Image Preview</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <img id="modal-image" src="" class="img-fluid" alt="Large Image">
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@section('scripts')
<script>

</script>
@endsection

@endsection
