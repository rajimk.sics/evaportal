@extends('layouts.main')

@section('content')
    <!-- Page body -->
    <div class="page-body">
        <div class="container-xl">

            <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">

            <div class="card">
                <div class="card-body">

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">

                            <li><a href="{{ url('/home') }}">Home</a></li>
                            @if (Auth::user()->role == 1)
                               
                                <li><a href="{{ url('/manage_package') }}">Regular Package</a></li>
                                
                                
                            @endif

                            @if (Auth::user()->role == 3)
                         
                            <li><a href="{{ url('/packagelist_trainer') }}">{{ ucfirst($package->pac_name) }}</a></li>
                            
                            @endif
                            <li><a href="{{ url('/syllabusview/' . $pac_id) }}">{{ ucfirst($package->pac_name) }} - Topics</a></li>
                        </ol>
                    </nav>


                    <h4 class="card-title"></h4>
                    <div class="table-responsive">
                        @if (Auth::user()->role == 1 && $TopicList != null && count($TopicList) > 0)
                            <div class="mb-3">
                                <button type="button"
                                    onclick="toggleMultipleStatus('activate', 'topic', {{ $pac_id }})"
                                    class="btn btn-success">Activate Selected Topics</button>
                                <button type="button"
                                    onclick="toggleMultipleStatus('deactivate', 'topic', {{ $pac_id }})"
                                    class="btn btn-danger">Deactivate Selected Topics</button>
                            </div>
                        @endif
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    @if (Auth::user()->role == 1)
                                        <th>
                                            <div style="display: flex; align-items: center;">
                                                <input type="checkbox" class="selectalltopics" id="selectalltopics"
                                                    style="margin-right: 5px; cursor: pointer;" />
                                                <label for="selectalltopics" style="cursor: pointer;">Select All</label>
                                            </div>
                                        </th>
                                    @endif
                                    <th class="align-middle">No</th>
                                    <th class="align-middle">Topic</th>
                                    <th class="align-middle">Topic Status</th>


                                    <th class="align-middle">Action</th>


                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>

                                @foreach ($TopicList as $item)
                                    <tr>
                                        @if (Auth::user()->role == 1)
                                            <td>
                                                <input type="checkbox" class="topic-checkbox"
                                                    value="{{ $item->topic_id }}">
                                            </td>
                                        @endif
                                        <td>{{ $i }}</td>
                                        <td>{{ $item->topics }}</td>
                                        <td>
                                            @if ($item->topic_status == 1)
                                                <span style="color:green;display:block"
                                                    id="act_{{ $item->topic_id }}">Active</span>
                                                <span style="color:red;display:none"
                                                    id="deact_{{ $item->topic_id }}">Deactive</span>
                                            @else
                                                <span style="color:red;display:block"
                                                    id="deact_{{ $item->topic_id }}">Deactive</span>
                                                <span style="color:green;display:none"
                                                    id="act_{{ $item->topic_id }}">Active</span>
                                            @endif
                                        </td>

                                      
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    @if (Auth::user()->role == 1)
                                                    <!-- Toggle Topic Status -->
                                                    @if ($item->topic_status == 1)
                                                        <button type="button" class="btn btn-danger btn-sm text-white me-2"
                                                            style="display: block"
                                                            onclick="toggleStatus('{{ $item->topic_id }}','topic','{{ $pac_id }}','deactivate')"
                                                            id="deactb_{{ $item->topic_id }}">
                                                            Deactivate Topic
                                                        </button>
                                                        <button type="button"
                                                            class="btn btn-success btn-sm text-white me-2"
                                                            style="display: none"
                                                            onclick="toggleStatus('{{ $item->topic_id }}','topic','{{ $pac_id }}','activate')"
                                                            id="actb_{{ $item->topic_id }}">
                                                            Activate Topic
                                                        </button>
                                                    @else
                                                        <button type="button"
                                                            class="btn btn-success btn-sm text-white me-2"
                                                            style="display: block"
                                                            onclick="toggleStatus('{{ $item->topic_id }}','topic','{{ $pac_id }}','activate')"
                                                            id="actb_{{ $item->topic_id }}">
                                                            Activate Topic
                                                        </button>
                                                        <button type="button" class="btn btn-danger btn-sm text-white me-2"
                                                            style="display: none"
                                                            onclick="toggleStatus('{{ $item->topic_id }}','topic','{{ $pac_id }}','deactivate')"
                                                            id="deactb_{{ $item->topic_id }}">
                                                            Deactivate Topic
                                                        </button>
                                                    @endif
                                                    @endif

                                                  

                                                    <a href="{{ url('/subtopicView/'.$pac_id.'/'. $item->topic_id) }}"
                                                        class="btn btn-secondary btn-sm text-white"
                                                        title="Toggle Subtopics">
                                                        View Subtopics
                                                    </a>


                                                </div>




                                            </td>
                                       


                                    </tr>

                                    <?php $i++; ?>
                                @endforeach

                            </tbody>


                        </table>

                    </div>
                </div>

            </div>
            @if (session('duplicates'))
                <div class="alert alert-warning">
                    <strong>Warning!</strong> The following subtopics are duplicates:
                    <ul>
                        @foreach (session('duplicates') as $duplicate)
                            <li>{{ $duplicate }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <script>
                @if (Session::has('message'))
                    swal({
                        title: "",
                        text: "{{ Session::get('message') }}",
                        type: "success",
                        showCancelButton: false,
                        dangerMode: false,
                        confirmButtonText: 'OK',
                    });
                @endif
            </script>

        @endsection
