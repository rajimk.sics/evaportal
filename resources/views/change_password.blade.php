@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
                   
              <div class="card">

                <div class="card-body">


                <nav aria-label="breadcrumb">
                  <ol class="breadcrumbs">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="#">{{$title}}</a></li>
                  </ol>
                </nav>

                <h4 class="card-title">{{$title}}</h4>
                   
                     <form id="passwordform" method="POST" action="{{url('/update_password')}}" enctype="multipart/form-data">
                   
                     <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">

                     <input type="hidden" name="userid" id="userid" value="{{Auth::user()->id}}">
                     <div class="row">
                      <div class="col-lg-6">
                      <label class="form-label">Current Password<sup>*</sup></label>
                      <input type="password" class="form-control" name="current_password" id="current_password">
                      </div>
                 
                      <div class="col-lg-6">
                      <label class="form-label">New Password<sup>*</sup></label>
                      <input type="password" class="form-control" name="new_password" id="new_password">
                      </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-6">
                        <label class="form-label">Confirm New Password<sup>*</sup></label>
                        <input type="password" class="form-control" name="confirm_password" id="confirm_password">
                      </div>
                     </div>
                      <div class="card-footer text-end">
                        <div class="d-flex">
                          <button type="submit" class="btn btn-primary ms-auto">Update</button>
                        </div>
                      </div>
                       
                        </form>
               </div>
            </div>
          </div>
            <script>
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif
        
        </script>




@endsection