@extends('layouts.main')
@section('content')

<style>

  .formcontents{
    display: flex;
    flex-direction: column;
  }
  label.error{
    color: red;
    order: 3;
  }

  </style>
   <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">   
              <div class="row row-cards">
                <div class="col-12">
                    <input type="hidden" id="token_eva" name="_tokens" value="{{ csrf_token() }}">

                  <form id="addstudents" method="POST" enctype="multipart/form-data"  action="{{url('/import_chrysallis_registered')}}">
                    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
     
                    <nav aria-label="breadcrumb">
                      <ol class="breadcrumbs">
                        <li><a href="{{url('/home')}}">Home</a></li>
                      @if( $packagedetails->pac_type==1)
                        <li><a href="{{url('/manage_package')}}">Regular Package</a></li>

                        @else
                        <li><a href="{{url('/manage_package_chrysallis')}}">Chrysalis Package</a></li>
                        @endif
                       
                        <li><a href="#">{{$title}}  -  {{ucfirst($packagedetails->pac_name)}}</a></li>
                      </ol>
                    </nav>
                    <div class="card-header">
                      <h2 class="fs-title">{{$title}} - {{ucfirst($packagedetails->pac_name)}}</h2>
                    </div>

                    @if(Session::has('notexist'))
                    <div class="alert alert-warning">
                      <p>This Email id not registered</p>

                      
                       <ul>
                          @foreach(Session::get('notexist') as $notexist)
                             <li>{{ $notexist }}</li>
                          @endforeach
                      </ul>
                    </div>
                      @endif
                    @if(Session::has('duplicates'))
                    <div class="alert alert-warning">
                      <p>Already assigned this Package:</p>
                       <ul>
                          @foreach(Session::get('duplicates') as $duplicate)
                             <li>{{ $duplicate }}</li>
                          @endforeach
                      </ul>
                    </div>
                      @endif

                   

                    <div class="card-body">
                      <div class="">
                          <div class="row">
                          
                          
                            <div class="col-lg-6 formcontents">
                            <div class="form-group">
                                <label class="form-label">Fees</label>
                                <input type="text" class="form-control" readonly name="packfee" id="packfee" placeholder="Fee" value="{{$packagedetails->fee}}">
                              </div>
                            </div>
                            <div class="col-lg-6 formcontents">
                            <div class="form-group">
                                <label>Tax 18%</label>
                                <input type="text" class="form-control" readonly name="packtax" id="packtax" value="{{$packagedetails->tax}}"  readonly>
                              </div>
                            </div>
                            <div class="col-lg-6 formcontents">
                            <div class="form-group">
                                <label>Actual Fees</label>
                                <input type="text" class="form-control" readonly name="packtot" id="packtot" readonly value="{{$packagedetails->total}}">
                              </div>
                            </div>


                            <div class="col-lg-6 formcontents">
                              <div class="form-group">
                              <label class="form-label">Joining Date<sup>*</sup></label>
                              <input type="text" style="width:auto"  data-zdp_readonly_element="true" name="oldpay_date" id="oldpay_date" class="form-control" value="{{date('d-m-Y')}}">
                              </div>
                          </div>
                            <div class="col-lg-6 formcontents">
                              <div class="form-group">
                                  <label class="form-label">Start date  <sup>*</sup></label>
                                  <input type="text" style="width:auto"  data-zdp_readonly_element="true" name="start_date" id="start_date" class="form-control">
                                </div>
                            </div>

                            <input type="hidden" name="packname" id="packname" value="{{$pacid}}">
                            <div class="col-lg-6 formcontents">
                                <div class="form-group">
                                    <label class="form-label">End date  <sup>*</sup></label>
                                    <input type="text" style="width:auto"  data-zdp_readonly_element="true" name="end_date" id="end_date" class="form-control">
                                  </div>
                              </div>

                              <div class="col-lg-6 formcontents" id="reductiondiv" style="display: block">
                              
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="reductioncheck" name="reductioncheck" value="1">
                                    <label class="form-check-label" for="flexCheckDefault">
                                    Any Reduction Offered
                                    </label>
                                    </div>
                            </div>
                            <div class="col-lg-6 formcontents" id="reductionvaldiv" style="display: none">
                                <div class="form-group">
                                    <label>Reduction Amount<sup>*</sup></label>
                                    <input type="number" class="form-control"  min=1 oninput="validity.valid||(value='');" name="reduction_amount"  id="reduction_amount" >

                                    <input type="hidden" name="max_range"   id="max_range" value="{{round(($packagedetails->fee*15)/100)}}">
                                    <span id="reduction_amount_span"  style="color:red">Minimum-1 and Maximum {{round(($packagedetails->fee*15)/100)}}</span>
                                </div>
                            </div>
                            <div class="col-lg-6 formcontents"  id="reducedfees_div" style="display: none">
                                <div class="form-group">
                                    <label>Reduced Fees</label>
                                    <input type="text" class="form-control"  name="reducedfees" id="reducedfees" readonly>
                                </div>
                            </div>
                            <div class="col-lg-6 formcontents" id="redtaxdiv" style="display: none">
                                <div class="form-group">
                                    <label> Reduced Tax</label>
                                    <input type="text" class="form-control"  name="reducedtax" id="reducedtax" readonly>
                                </div>
                            </div>
                            <div class="col-lg-6 formcontents" id="afterreduction_div" style="display: none">
                                <div class="form-group">
                                    <label>Total Fees After Reduction</label>
                                    <input type="text" class="form-control"  name="after_reduction" id="after_reduction" readonly>
                                </div>
                            </div>
                            <div class="col-lg-6 formcontents" id="ineffect_div" style="display: none">
                                <div class="form-group">
                                    <label>In-Effect Total Reduction Offered</label>
                                    <input type="text" class="form-control"  name="ineffect_offered" id="ineffect_offered" readonly>
                                </div>
                            </div> 

                            <div class="col-lg-6 formcontents" style="display:none;" >
                                <div class="form-group">
                                    <label>Paid Fees <sup>*</sup></label>
                                    <input type="number" min=0 oninput="validity.valid||(value='');" class="form-control"  name="regfees" id="regfees" placeholder="Give 0 for the time being." >
                                    <span style="color:red" id="regfees_hidden_span"></span>

                                    <input type="hidden" class="form-control"  name="regfees_hidden" id="regfees_hidden" >
 
                                </div>
                            </div> 
                            <input type="hidden" class="form-control"  name="finalamount" id="finalamount" value="{{$packagedetails->total}}">
                            <div class="col-lg-6 formcontents">
                                <div class="form-group">
                                    <label class="">Reference</label>
                                    <select class="form-select selecttype" id="referenece" name="referenece" >
                                      <option value="">Select Reference</option>
                                     
                                      <option value="1">Marketing</option>
                                      <option value="2">Employees</option>
                                      <option value="3">Other</option>
                                     
                                    </select>
                                  </div>
                                </div>
                                <div class="col-lg-6 formcontents" id="ref_list" style="display:none;">
                                    <div class="form-group">
                                        <label class="">Reference List<sup>*</sup></label>
                                        <select class="form-select selecttype" id="referenece_list" name="referenece_list" >
                                                                               
                                        </select>
                                      </div>
                                    </div>
                                    <div class="col-lg-6 formcontents" id="ref_list" style="display:block;">
                                        <div class="form-group">
                                         
                                          <a href="{{url('/public/uploads/excel/chrysallisregistered.xls')}}" download="regsiteredchrysallis">Download Sample Excel Template</a>
                                            <label class="">Upload students<sup>*</sup></label>
                                            <input style="height:inherit;" class="form-control" id="file" type="file" name="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">                                          
                                          </div>
                                        </div>
                        </div>
                      </div>


                    </div>
                  </div>
                  <div class="card-footer text-end">
                    <div class="d-flex">
                      
                      <button type="submit" class="btn btn-primary ms-auto">Submit</button>
                    </div>
                  </div>
                </form>

              
                </div>
           
            </div>

            <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
            <script src="{{url('public/assets/js/scriptpackage.js')}}"></script>    
                   
@endsection
