@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
                   
              <div class="card">
                <div class="card-body">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>
                          <li><a href="#">{{$title}}</a></li>
                        </ol>
                      </nav>
                    <h4 class="card-title"> {{$title}}</h4>
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Student Name</th>
                <th>Email</th>
                <th>SICS Reg Number</th>              
                <th>Contact Number</th>              
                <th>Package</th>               
                <th>Date of Joining</th>
                <th>Action</th>

            </tr>
        </thead>
        <tbody>
          <?php $i=1;?>
            @foreach($studlist as $studlist)
            <tr>
                <td>{{$i}}</td>
                <td>{{ucfirst($studlist->name)}}</td>
                <td>{{$studlist->email}}</td>      
                <td>SIAC{{$studlist->student_admissionid}}</td>                     
                <td>{{$studlist->phone}}</td>              
                <td>{{$studlist->pac_name}}</td>
                <td><span id="doj_{{$studlist->studentpackage_id}}">{{date("d-m-Y", strtotime($studlist->joining_date))}}</span></td>
                <td>
                  @php
                  $x=date("d-m-Y", strtotime($studlist->joining_date));                    
                  @endphp

                  <input type="hidden" name="dojdate_{{$studlist->studentpackage_id}}" id="dojdate_{{$studlist->studentpackage_id}}" value="{{$x}}">
                  
                  <button type="button" class="btn btn-cyan btn-sm text-white me-2" fdprocessedid="wf07gv" onclick="studentdetails('{{$studlist->studentpackage_id}}')">
                    Edit Date of Joining
                 </button>

              </td>

            </tr>
            <?php $i++;?>
            @endforeach
           
        </tbody>
      
    </table>

                    </div>
                </div>
           
            </div>

            <form id="editdoj" method="POST" e>
              <input type="hidden" name="_token" id="token_eva" value="{{ csrf_token() }}">
        
            <input type="hidden" id="student_package_id" name="student_package_id" value="">
             
        
            <div class="modal modal-blur fade" id="modal-changedate"  role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Edit</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">

                  <div class="form-group">
                    <label class="form-label">Joining Date<sup>*</sup></label>
                    <input type="text" class="form-control"  style="width:auto;" id="stud_dob" name="stud_dob"
                    data-zdp_readonly_element="true" >
                  </div>               
                </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>                                            
              </div>
            </div>
            </div>
            
            </form>





            <script>
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif
        </script>
@endsection
