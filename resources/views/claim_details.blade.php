@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">
                   
              <div class="card">
                <div class="card-body">

                   
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>
                          <li><a href="#">{{$title}}</a></li>
                        </ol>
                      </nav>
                    <h4 class="card-title">{{$title}}</h4>


                  


                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>College Name</th>
                <th>Department</th>
                <th>POC</th>
                 <th>Action</th>
               
                
             
            </tr>
        </thead>
        <tbody>
<?php $i=1;?>
            @foreach($collegeupdate as $detaillist)
            <tr id="row{{$detaillist->id}}" >
                <td>{{$i}}</td>        
               <td>{{$detaillist->college}}</td>
               <td>{{$detaillist->dept}}</td>
               <td>{{$detaillist->poc_names}}</td>
              <td> 
                <a href="{{url('/add_students/claim/'.$detaillist->id)}}"><button type="button" class="btn btn-red btn-sm text-white" fdprocessedid="wf07gv">
                  Claim
                </button></a>
              </td>
                 
                                        
                   
                                          
               
            </tr>
            <?php $i++;?>
            @endforeach
           
        </tbody>
      
    </table>

    
    {{-- <div class="btn-list" style="float: right">
      <button type="button"  id="nextButton" data-col-id="{{$collegeId}}" data-pack-id="{{$packageId}}'"  class="btn btn-success btn-sm text-white" fdprocessedid="pvth1" >
        Next
     </button> 
    </div> --}}
    <div class="modal modal-blur fade" id="claimAllocate" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Claim Allocation</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <form id="claimallocateForm"  >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="modal-body">
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                     
                      <label for="claimName">College Name</label>
                      <input type="text" class="form-control" name="collegename" id="collegename" placeholder="College" readonly>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="claimName">% of Allocation<sup>*</sup></label>
                      <input type="text" class="form-control" name="perallocate" id="perallocate" placeholder="% of Allocation" >
                      </div>
                  </div>
                </div>
                <div class="row">
                 
                 
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="claimName">Comment<sup>*</sup></label>
                      <input type="text" class="form-control" name="comment" id="comment" placeholder="Comment">
                    </div>
                  </div>
                </div>
                
                      <input type="hidden" id="collegeId" name="collegeId">
                     
                      <input type="hidden" id="packageId" name="packageId">
                      <input type="hidden" id="claimId" name="claimId">
                     
             
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                   <button type="submit" id="claimalloc" class="btn btn-primary">Submit</button> 
              </div>
          </div>
        </form>
      </div>
    </div>
    
@endsection
