@extends('layouts.main')

@section('content')
    <style>
        th {
            text-align: center;
            vertical-align: middle;
        }
    </style>

    <div class="page-body">
        <div class="container-xl">
            @if (Session::has('duplicates'))
            <div class="alert alert-warning mt-3">
                <p><strong>Duplicate values found:</strong></p>
                <ul>
                    @foreach (Session::get('duplicates') as $duplicate)
                        <li>{{ $duplicate }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">

            <div class="card">
                <div class="card-body">

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                            <li><a href="{{ url('/home') }}">Home</a></li>
                            <li><a href="{{ url('/fetchExam') }}">Exams - View Exams</a></li>
                            <li><a href="{{ url('addExamquestions/' . $exam_id) }}">Add Questions - {{ ucfirst($exam) }}</a>
                            </li>
                        </ol>
                    </nav>
                    <h4 class="card-title">Manage Questions</h4>
                    <!-- Filter Form -->
                    <form method="GET" action="{{ url('addExamquestions/' . $exam_id) }}" class="mb-3 row mt-3"
                        id="examquestion-form">
                        <div class="col-4">
                            <select name="technology[]" class="form-select selecttech" multiple >
                                <option value="" disabled>All Technologies</option>
                                @foreach ($technologies as $technology)
                                    <option value="{{ $technology->id }}"
                                        {{ in_array($technology->id, (array) $selectedTechnologies) ? 'selected' : '' }}>
                                        {{ $technology->technology }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-8 d-flex align-items-end">
                            <button type="submit" class="btn btn-primary me-2">Filter</button>
                            <a href="{{ url('addExamquestions/' . $exam_id) }}" class="btn btn-secondary">Clear Selection</a>
                        </div>
                    </form>


                    <form action="{{ url('saveExamquestion') }}" method="POST">
                        <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="exam_id" value="{{ $exam_id }}">
                        <div class="table-responsive">
                            <table class="mt-4 table table-striped table-bordered" id="example">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" id="select-all"></th>
                                        <th>No</th>
                                        <th>Questions</th>
                                        <th>Option A</th>
                                        <th>Option B</th>
                                        <th>Option C</th>
                                        <th>Option D</th>
                                        <th>Answer</th>
                                        <th>Technology</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($questions as $index => $question)
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="row-select" name="question_ids[]"
                                                    value="{{ $question->id }}"
                                                   >
                                            </td>
                                            <td>{{ $index + 1 }}</td>
                                            <td>{{ ucfirst($question->questions) }}</td>
                                            <td>{{ $question->optiona }}</td>
                                            <td>{{ $question->optionb }}</td>
                                            <td>{{ $question->optionc }}</td>
                                            <td>{{ $question->optiond }}</td>
                                            <td>Option {{ $question->answer }}</td>
                                            <td>{{ $question->technologies }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <button type="submit" class="btn btn-primary float-end mt-3">Submit</button>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
@endsection
