@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">
                   
              <div class="card">
                <div class="card-body">

                    <div class="btn-list" style="float: right">
                     
                        <a href="#" class="btn btn-primary d-none d-sm-inline-block" onclick="addEvent()">
                          <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                          Create new 
                        </a>
                        <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal" data-bs-target="#modal-report" aria-label="Create new report">
                          <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                        </a>
                      </div>

                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>
                          <li><a href="#">{{$title}}</a></li>
                        </ol>
                      </nav>
                   
                    <h4 class="card-title">{{$title}}</h4>
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Status</th>
               <th>Action</th>
            </tr>
        </thead>
        <tbody>
<?php $i=1;?>
            @foreach($eventlist as $eventlist)
            <tr>
                <td>{{$i}}</td>
                <td>{{$eventlist->event_name}}</td>
                <td>{{$eventlist->start_date}}</td>
              
                @if(empty($eventlist->end_date))
                <td>--</td>
                @else
                <td>{{$eventlist->end_date}}</td>
                @endif
                <td>
                @if($eventlist->status==1)

                <span style="color:green">Active</span>
                
                @else
                <span style="color:red">Deactive</span>
                
                @endif
                 </td> 
                  <td>
                       <!--   <button type="button" class="btn btn-cyan btn-sm text-white" fdprocessedid="wf07gv" onclick="editEvent('{{$eventlist->id}}')">
                                    Edit
                          </button>-->
                   @if($eventlist->status==1)
                        <button type="button" class="btn btn-danger btn-sm text-white" fdprocessedid="pvth1" onclick="deactivateEvent('{{$eventlist->id}}')">
                              Deactivate
                        </button>
                  @else
                        <button type="button" class="btn btn-success btn-sm text-white" fdprocessedid="pvth1" onclick="activateEvent('{{$eventlist->id}}')">
                              Activate
                        </button>
                  @endif

                
                          
                 </td>
               </tr>
            <?php $i++;?>
            @endforeach
           
        </tbody>
      
    </table>

    <form id="event_form" method="POST" enctype="multipart/form-data"  action="{{url('/save_events')}}">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="modal modal-blur fade" id="modal-event" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">New Event</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="mb-3">
            <label class="form-label">Event Name<sup>*</sup></label>
            <input type="text" class="form-control" name="event_name" id="event_name" placeholder="Eg: Impact 2024">
          </div>
          <div class="mb-3">
            <input type="radio" id="single" name="eventDate"  value="single">
                      <label for="single">One day Event</label>
                      <input type="radio" id="multiple" name="eventDate" value="multiple">
                      <label for="multiple">Multiple day Event</label><br>
          </div>
          
           <div class="mb-3" id="singleday" style="display:none">
                 <label class="form-label">Start Date<sup>*</sup></label>
                  <input type="text" class="form-control" data-zdp_readonly_element="true" name="start_date" id="start_date" >
                  
           </div>
                    
           <div class="mb-3" id="multipleday" style="display:none">
                    <label class="form-label">End Date<sup>*</sup></label>
                    <input type="text" class="form-control" data-zdp_readonly_element="true" name="end_date" id="end_date" >
            </div>
             
        </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>                                            
      </div>
    
    </div>
    </div>
    </form>


                    </div>
                </div>
           
            </div>


<!--Edit model-->
    <form id="editevent" method="POST" enctype="multipart/form-data"  action="{{url('/update_event')}}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="modal modal-blur fade" id="modal-edit-event" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Edit Event</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
      <div class="modal-body">
      <div class="mb-3">
        <label class="form-label">Event Name<sup>*</sup></label>
        <input type="hidden" class="form-control" name="editeventid" id="editeventid" placeholder="Event">
        <input type="text" class="form-control" name="editeventname" id="editeventname" placeholder="Event">
      </div>
      <div class="mb-3">
        <input type="text" class="form-control" name="editstartdate" id="editstartdate" >
      </div>
      <div class="mb-3">
        <input type="text" class="form-control" name="editenddate" id="editenddate" >
      </div>
      </div>
      <div class="modal-footer">
        <button type="submit" id="event_edit" class="btn btn-primary">Update</button>
      </div>                                            
      </div>

        </div>
      </div>
    </form>



@endsection
