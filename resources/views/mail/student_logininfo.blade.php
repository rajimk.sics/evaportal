
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Information</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }

        .container {
            max-width: 600px;
            margin: auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        h1 {
            color: #333;
        }

        p {
            margin-bottom: 20px;
            line-height: 1.6;
        }

        .button {
            display: inline-block;
            padding: 10px 20px;
            background-color: #007bff;
            color: #fff;
            text-decoration: none;
            border-radius: 5px;
        }

        .button:hover {
            background-color: #0056b3;
        }

        .footer {
            margin-top: 20px;
            font-size: 14px;
            color: #666;
        }
    </style>
</head>




<body>
    <div class="container">
        <h1>Login Information</h1>
        <p>Hello {{ $name }},</p>
        <p>Your login credentials for Eva Portal are as follows:</p>
        <ul>
            <li><strong>Username:</strong> {{ $email }}</li>
            <li><strong>Password:</strong> {{$password}}</li>
        </ul>
        <a class="button" href="{{ $loginUrl }}">Login Now</a>
       
        <p class="footer">Thanks,<br>Eva Portal</p>
    </div>
</body>
</html>