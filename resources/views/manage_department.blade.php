@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">  
                   
              <div class="card">
                <div class="card-body">

                    <div class="btn-list" style="float: right">
                     
                      <a href="#" class="btn btn-primary d-none d-sm-inline-block"  onclick="addDepartment()">
                        <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                        <svg xmlns="http://www.w3.org/2000/svg"   class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                        Create new 
                      </a>
                        <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal" data-bs-target="#modal-report" aria-label="Create new report">
                          <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                        </a>
                      </div>

                  <nav aria-label="breadcrumb">
                      <ol class="breadcrumbs">
                        <li><a href="{{url('/home')}}">Home</a></li>
                        <li><a href="#">{{$title}}</a></li>
                      </ol>
                  </nav>
                   
                    <h4 class="card-title">{{$title}}</h4>

                   
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Department</th>
                                <th>Status</th>
                                <th>Action</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                          <?php $i=1;?>
                                      @foreach($deplist as $deplist)
                                      <tr>
                                          <td>{{$i}}</td>
                                          <td>{{$deplist->department}}</td>


                                          <td>
                                            @if($deplist->status==1)
                                            <span style="color:green;display:block;" id="act_{{$deplist->id}}">Active</span>
                                            <span style="color:red;display:none;" id="deact_{{$deplist->id}}" >Deactive</span>                       
                                            @else
                                            <span style="color:green;display:none;" id="act_{{$deplist->id}}" >Active</span>
                                            <span style="color:red;display:block;" id="deact_{{$deplist->id}}">Deactive</span>    
                                            
                                            @endif
                                            </td> 


                                          <td>
                                            <div class="d-flex">
                                            <button type="button" class="btn btn-cyan btn-sm text-white me-2" fdprocessedid="wf07gv" onclick="editDepartment('{{$deplist->id}}','{{$deplist->department}}')">
                                              Edit
                                            </button>
                                              @if($deplist->status==1)
                                                    
                                             <button type="button" style="display: block" id="deactb_{{$deplist->id}}" class="btn btn-danger btn-sm text-white" fdprocessedid="pvth1" onclick="departmentstatus('{{$deplist->id}}','deactivate')">
                                                       Deactivate
                                                      </button>

                                                      <button type="button"  style="display: none" id="actb_{{$deplist->id}}" class="btn btn-success btn-sm text-white" fdprocessedid="pvth1" onclick="departmentstatus('{{$deplist->id}}','activate')">
                                                        Activate
                                                       </button>
                                                     

                                                      @else
                                                     
                                                      <button type="button" style="display: block" id="actb_{{$deplist->id}}" class="btn btn-success btn-sm text-white" fdprocessedid="pvth1" onclick="departmentstatus('{{$deplist->id}}','activate')">
                                                        Activate
                                                       </button>

                                                       <button type="button" style="display: none" id="deactb_{{$deplist->id}}" class="btn btn-danger btn-sm text-white" fdprocessedid="pvth1" onclick="departmentstatus('{{$deplist->id}}','deactivate')">
                                                        Deactivate
                                                       </button>
                                                      </div>
                                                      @endif
                
                                          </td>
                                      </tr>
                                      <?php $i++;?>
                                      @endforeach
                                     
                                  </tbody>
                      
                    </table>

                  
                    
                    <div class="modal modal-blur fade" id="modal-dep" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title">New Department</h5>
                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form id="departmentform" method="POST" enctype="multipart/form-data"  action="{{url('/save_department')}}">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="modal-body">
                          <div class="mb-3">
                            <label class="form-label">Department<sup>*</sup></label>
                            <input type="text" class="form-control" name="department" id="department" placeholder="Department">
                          </div>
                        </div>
                      <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>                                            
                      </div>
                    
                    </div>
                    </div>
                    </form>



      </div>
    </div>         
</div>

<div class="modal modal-blur fade" id="modal-edit-dep"  tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Edit Department</h5>
      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <form id="editdepartmentform" method="POST" enctype="multipart/form-data"  action="{{url('/edit_department')}}">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="modal-body">
      <div class="mb-3">
        <label class="form-label">Department<sup>*</sup></label>
        <input type="text" class="form-control" name="editdepname" id="editdepname" placeholder="Department">
        <input type="hidden" class="form-control" name="editdepid" id="editdepid" placeholder="Department">
      </div>
    </div>
  <div class="modal-footer">
    <button type="submit" class="btn btn-primary">Update</button>
  </div>                                            
  </div>

</div>
</div>

</form>



            <script>
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif
        </script>




@endsection
