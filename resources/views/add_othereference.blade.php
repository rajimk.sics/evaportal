@extends('layouts.main')
@section('content')
   <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">   
              <div class="row row-cards">
                <div class="col-12">

                  <form id="addothereference" method="POST" enctype="multipart/form-data"  action="{{url('/save_othereference')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="card-header">
                      <h4 class="card-title"></h4>Add Reference Employee
                    </div>
                    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-6 col-xl-12">
                          <div class="row">
                            <div class="col-md-6 col-xl-6">

                              <div class="mb-3">
                              <label class="form-label">Employee Name<sup>*</sup></label>
                                <input type="text" class="form-control" name="refname" id="refname" placeholder="Name">
                              </div>
                              
                              <div class="mb-3">
                                <label class="form-label">Relation<sup>*</sup></label>
                                <input type="text" class="form-control" name="refrelation" id="refrelation" placeholder="Relation" >
                              </div>
                              
                            
                  
                          </div>
                          <div class="col-md-6 col-xl-6">

                            <div class="mb-3">
                              <label class="form-label">Email id<sup>*</sup></label>
                              <input type="text" class="form-control" name="email" id="email" placeholder="Email id">
                            </div>
                           
                            <div class="mb-3">
                              <label class="form-label">Phone<sup>*</sup></label>
                              <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" minlength="10" maxlength="13">
                            </div>


                          </div>




                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card-footer text-end">
                    <div class="d-flex">
                      
                      <button type="submit" class="btn btn-primary ms-auto">Submit</button>
                    </div>
                  </div>
                </form>

              
                </div>
           
            </div>


          
@endsection
