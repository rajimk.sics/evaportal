@extends('layouts.main')

@section('content')


<!-- Page body -->
<div class="page-body">
  <div class="container-xl">

    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">

    <div class="card">
      <div class="card-body">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumbs">
            <li><a href="{{url('/home')}}">Home</a></li>
            <li><a href="#">{{$title}}</a></li>
          </ol>
        </nav>
        <h4 class="card-title"> {{$title}}</h4>
        <div class="payment-grd">
                      <form id="searchchrys_form" method="GET" enctype="multipart/form-data" action="{{ url('/candidate_followups') }}">
                          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
      
                          <div class="row">
                              <!-- Year Dropdown -->
                              <div class="col-lg-4 formcontents">
                                  <label for="year">Select Year<sup>*</sup></label>
                                  <select class="form-select" name="year" id="year">
                                      
                                      @foreach($years as $year)
                                      <option value="{{ $year }}" {{ $year == $currentYear ? 'selected' : '' }}>{{ $year }}</option>
                                      @endforeach
                                  </select>
                              </div>
      
                              <!-- Month Dropdown -->
                              <div class="col-lg-6 formcontents">
                                  <label for="month">Select Month<sup>*</sup></label>
                                  <select class="form-select" name="month" id="month">
                                     
                                      @foreach($months as $key => $month)
                                      <option value="{{ $key }}" {{ $key == $currentMonth ? 'selected' : '' }}>{{ $month }}</option>
                                      @endforeach
                                  </select>
                              </div>
      
                              <div class="col-lg-2 formcontents">
                                  <button type="submit" class="btn btn-primary" style="margin-top: 26px;">Submit</button>
                            </div>
      
                          </div>
      
                          <div class="col formcontents"> 
                            <a href="{{url('/candidate_followups/')}}" class="btn btn-primary"  style="margin-top: 26px;"> View Current Month</a>
                            </div>
                      </form>
                  </div>
        <div class="table-responsive">
          <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Contact Number</th>
                <th>Registration Source</th>
                <th>Applied For</th>
                <th>Technical Skill</th>
                <th>Qualification</th>
                <th>Experience</th>
                <th>Expected Salary</th>
                <th>Resume</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              @foreach($candlist as $candlist)
              <tr id="row_{{$i}}">
                <td>{{$i}}</td>
                <td>{{ucfirst($candlist->name)}}</td>
                <td>{{$candlist->email}}</td>
                <td>{{$candlist->contact_pri}}</td>
                <td>{{ $candlist->app == 1 ? 'App' : 'Web' }}</td>
                <td>{{$candlist->position_applying}}</td>
                <td>{{$candlist->tech_skills}}</td>
                <td>{{$candlist->edu_qualification}}</td>
                <td>{{$candlist->exp_years}}</td>
                <td>{{$candlist->exp_salary}}</td>
                <td>
                  @if($candlist->resume_upload)
                  @php
                  $isLink = filter_var($candlist->resume_upload, FILTER_VALIDATE_URL);
                  @endphp
                  @if($isLink)
                  <a href="{{ $candlist->resume_upload }}" target="_blank" class="btn btn-sm btn-primary">Preview</a>
                  @else
                  <a href="{{ url('public/uploads/resume/' . $candlist->resume_upload) }}" target="_blank" class="btn btn-sm btn-primary">Preview</a>
                  @endif
                  @else
                  No Resume
                  @endif
                </td>
                <td>
                  <button type="button" class="btn btn-cyan btn-sm text-white" style="margin-bottom: 10px;" onclick="candidatedetails('{{$candlist->jobreg_id}}')">
                    Details
                  </button>
                  <button type="button" class="btn btn-green btn-sm text-white" style="margin-bottom: 10px;" onclick="followupUpdate('{{$candlist->jobreg_id}}')">
                    Change Follow up
                  </button>
                  <button type="button" class="btn btn-green btn-sm text-white" style="margin-bottom: 10px;" onclick="candidateFollowupHistory('{{$candlist->jobreg_id}}')">
                    Follow up History
                  </button>
                  <button type="button" class="btn btn-blue btn-sm text-white" style="margin-bottom: 10px;" onclick="candidateInterview('{{$i}}','{{$candlist->jobreg_id}}')">
                    Schedule Interview
                  </button>

                </td>
              </tr>
              <?php $i++; ?>
              @endforeach

            </tbody>

          </table>


        </div>
      </div>

    </div>





    <div class="modal modal-blur fade" id="modal-candidate" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <h5 class="modal-title">Candidate Details</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body" id="candidatedetails">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>


    <div class="modal modal-blur fade" data-bs-backdrop="static" id="modal-candidate-followupdate" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Change Follow Up</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <form id="edit_followupdate" method="POST" enctype="multipart/form-data" action="{{url('/updatefollowup')}}">
              <input type="hidden" name="_token" id="token1" value="{{ csrf_token() }}">

              <input type="hidden" name="jobregid" id="jobregid">
              <div class="form-group">
                <label class="form-label">Follow Up Date<sup>*</sup></label>
                <input type="text" class="form-control" style="width:auto;" id="editfollowupdate" name="editfollowupdate"
                  data-zdp_readonly_element="true">
              </div>
              <div class="mb-3">
                <label class="form-label">Comments<sup>*</sup></label>
                <textarea class="form-control" rows="5" name="comments_edit" id="comments_edit"></textarea>
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Change</button>
          </div>
        </div>
      </div>
      </form>
    </div>

    <div class="modal modal-blur fade" data-bs-backdrop="static" id="modal-candidate-interview" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Schedule Interview</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <form id="candidateinterview" enctype="multipart/form-data">
              <input type="hidden" name="_token" id="token1" value="{{ csrf_token() }}">
              <input type="hidden" name="rowid" id="rowid" value="">
              <input type="hidden" name="jobreg_id" id="jobreg_id">


              <div class="mb-3">
                @php
                $company=App\Helpers\CustomHelper::Company();
                @endphp
                <label class="form-label">Company Name<sup>*</sup></label>
                <select class="form-select " name="company" id="company" placeholder="Company">
                  <option value="">Select Company</option>
                  @foreach($company as $company )
                  <option value="{{$company->id}}">{{$company->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="mb-3">
                <label class="form-label">Position<sup>*</sup></label>
                <input type="text" class="form-control" name="position" id="position"></textarea>
              </div>
              <div class="mb-3">
                <label class="form-label">Interview Date<sup>*</sup></label>
                <input type="text" class="form-control" style="width:auto;" id="interviewdate" name="interviewdate"
                  data-zdp_readonly_element="true">
              </div>
              <div class="mb-3">
                <label class="form-label">Recruiter Name</label>
                <input type="text" class="form-control" id="recruitname" name="recruitname" value="{{Auth::user()->name}}" readonly>
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
      </form>
    </div>



    <div class="modal modal-blur fade" data-bs-backdrop="static" id="modal-candidate-history" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"> Follow Up History</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <div id="candidatehistory">

            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn " data-bs-dismiss="modal">Close</button>

          </div>
        </div>
      </div>
    </div>
    @endsection