@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">
     
              <div class="card">
                <div class="card-body">

                    <div class="btn-list" style="float: right">
    
                        <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal" data-bs-target="#modal-report" aria-label="Create new report">
                          <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                        </a>
                      </div>

                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>
                         
                  <li><a href="{{$url}}">{{$subtitle}}</a></li>
                 
                          <li><a href="#">{{$title}}</a></li>
                        </ol>
                      </nav>
                   
                    <h4 class="card-title">{{$title}}</h4>
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Student Name</th>
                @if($pac_type==1)
                <th>SICS Reg Number</th>

                @endif
                <th>Package Amount </th>
                <th>Tax</th>
                <th>Total</th>
              
              </tr>
        </thead>
        <tbody>
            <?php $i=1;?>
            @foreach($monthlyreport as $monthlyreport)
            <tr>
                <td>{{$i}}</td>
                <td>{{ucfirst($monthlyreport->name)}}</td>
                @if($pac_type==1)
                <td>{{'SIAC'.$monthlyreport->student_admissionid}}</td>
                @endif
                <td> Rs {{number_format($monthlyreport->package_amount,2)}}</td>
                <td> Rs {{number_format($monthlyreport->package_tax,2)}}</td>
               
                <td>Rs {{number_format($monthlyreport->package_fullamount	,2)}}</td>
              
               
            </tr>
            <?php $i++;?>
            @endforeach           
        </tbody>     
    </table>

                    </div>
                </div>         
            </div>
           

                       
@endsection
