@extends('layouts.main')
@section('content')


<style>

  .formcontents{
    display: flex;
    flex-direction: column;
  }
  label.error{
    color: red;
    order: 3;
  }

  </style>
   <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">   
              <div class="row row-cards">
                
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-header">
                     
                    </div>
                    <div class="card-body">

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>

                          <li><a href="#">{{$title}}</a></li>
                        </ol>
                      </nav>

                   
                    <h4 class="card-title"> {{$title}}</h4>

                    <div class="payment-grd">
                      <form id="searchchrys_form" method="GET" enctype="multipart/form-data" action="{{ url('/all_students') }}">
                          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
      
                          <div class="row">
                              <!-- Year Dropdown -->
                              <div class="col-lg-4 formcontents">
                                  <label for="year">Select Year<sup>*</sup></label>
                                  <select class="form-select" name="year" id="year">
                                      
                                      @foreach($years as $year)
                                      <option value="{{ $year }}" {{ $year == $currentYear ? 'selected' : '' }}>{{ $year }}</option>
                                      @endforeach
                                  </select>
                              </div>
      
                              <!-- Month Dropdown -->
                              <div class="col-lg-6 formcontents">
                                  <label for="month">Select Month<sup>*</sup></label>
                                  <select class="form-select" name="month" id="month">
                                     
                                      @foreach($months as $key => $month)
                                      <option value="{{ $key }}" {{ $key == $currentMonth ? 'selected' : '' }}>{{ $month }}</option>
                                      @endforeach
                                  </select>
                              </div>
      
                              <div class="col-lg-2 formcontents">
                                  <button type="submit" class="btn btn-primary" style="margin-top: 26px;">Submit</button>
                            </div>
      
                          </div>
      
                          <div class="col formcontents"> 
                            <a href="{{url('/all_students/')}}" class="btn btn-primary"  style="margin-top: 26px;"> View Current Month</a>
                            </div>
                      </form>
                  </div>
   

    <div class="table-responsive">
                       
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                              <thead>
                                  <tr>
                                      <th>No</th>
                                      <th>Student Name</th>
                                      <th>Email Id</th>
                                      <th>SICS Reg Number</th>

                                      @if(Auth::user()->role !=2)
                                      <th>Contact Number</th>
                                      @endif

                                      <th>Package Type</th>
                                      <th>Package</th>
                                      <th>Joining Date</th>

                                      @if(Auth::user()->role !=2)

                                      <th>Closed By</th>

                                      @endif
                                      <th>Start Date</th>
                                      <th>End Date</th>

                                      @if(Auth::user()->role !=2)
                                      <th>Payment status</th>
                                      @endif

                                      @if(Auth::user()->role ==2)
                                      <th>Action</th>
                                      @endif
                                     
                                  </tr>
                              </thead>
                              <tbody>
                                <?php $i=1;?>
                                @foreach($studlist  as $studlist)
                                <tr>
                                  <td>{{$i}}</td>
                                  <td>{{ucfirst($studlist->name)}}</td>
                                  <td>{{$studlist->email}}</td>
                                  
                                  <td>SIAC{{$studlist->student_admissionid}}</td>  

                                  @if(Auth::user()->role !=2)
                                 
                                  <td>{{$studlist->phone}}</td>
                                  @endif
                           
                                  
                                  <td>

                                    @if($studlist->pac_type==1)

                                    Regular
                                  
                                    @else
                                    Chrysalis
                
                                    @endif

                                  </td>

                                  @php
                                  $techname=App\Helpers\CustomHelper::tech($studlist->tech_id); 
                                  @endphp
                                  <td>{{$studlist->pac_name}}</td>
                                  <td>

                                    {{date("d-m-Y", strtotime($studlist->joining_date))}}
                                   
                                   </td>


                                   @if(Auth::user()->role !=2)

                                        @php
                                        $name=App\Helpers\CustomHelper::uname($studlist->sales_id); 

                                        $displayName = $name && isset($name['name']) ? ucfirst($name['name']) : '';
                                        @endphp

                                      <td>{{ucfirst($displayName)}}</td>

                                  @endif
                                  <td>

                                    @if($studlist->pac_type==1)

                                    {{date("d-m-Y", strtotime($studlist->due_date_first))}}
                                                                
                                    @else
                                    {{$studlist->due_date_first}}
                
                                    @endif
                                    
                                    
                                   
                                  
                                  </td>
                                  <td>{{$studlist->enddate}}</td>
                                  @if(Auth::user()->role !=2)
                                  <td>
                                    {!!App\Helpers\CustomHelper::paymentcompleted($studlist->id,$studlist->package_id,$studlist->pac_type,$studlist->total)!!}
                                    
                                  </td>
                                  @endif

                                  @if(Auth::user()->role ==2)
                                  <td>
                                    <a href="{{url('/select_package/'.$studlist->id.'/'.'all')}}"><button type="button" class="btn btn-green btn-sm text-white" fdprocessedid="wf07gv">
                                      Select Regular Package
                                    </button>
                                   </a>
                      
                                   <a href="{{url('/select_chrysalis_package/'.$studlist->id.'/'.'all')}}"><button type="button" class="btn btn-green btn-sm text-white" fdprocessedid="wf07gv">
                                      Select Chrysalis Package
                                    </button>
                                 </a>


                                  </td>
                                  @endif





                                </tr>
                                <?php
                                $i++;

                                ?>
                                @endforeach

                              </tbody>
                            
                          </table>
                          </div>
                        </div>
                        <div class="tab-pane fade active show" id="tabs-home-8" role="tabpanel">
                        
                          <div>
                            

                          </div>
                        </div>
                        <div class="tab-pane fade" id="tabs-profile-8" role="tabpanel">
                         
                          <div>

                           
                          </div>
                        </div>
                        <div class="tab-pane fade" id="tabs-activity-8" role="tabpanel">
                         
                          <div>

                          </div>
                        </div>

                    </div>
                  </div>
                </div>
           
            </div>
  
@endsection
