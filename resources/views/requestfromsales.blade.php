@extends('layouts.main')

@section('content')

    <!-- Page body -->
    <div class="page-body">
        <div class="container-xl">

            <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">

            <div class="card">
                <div class="card-body">


                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                            <li><a href="{{ url('/home') }}">Home</a></li>
                            <li><a href="{{ url('/list_requestfromsales') }}">{{ $title }}</a></li>
                        </ol>
                    </nav>

                    <h4 class="card-title">{{ $title }}</h4>


                    <div class="payment-grd">
                        <form id="searchchrys_form" method="GET" enctype="multipart/form-data" action="{{ url('/list_requestfromsales') }}">
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        
                            <div class="row">
  
                      <!-- Year Dropdown -->
                      <div class="col">
                        <label for="year">Select Year</label>
                        <select class="form-select" name="year" id="year">
                            <option value="" >Select Year</option>  
                            @foreach($years as $year)
                            <option value="{{ $year }}" {{ $year == request('year') ? 'selected' : '' }}>{{ $year }}</option>
                            @endforeach
                        </select>
                    </div>
  
                    <!-- Month Dropdown -->
                    <div class="col">
                        <label for="month">Select Month</label>
                        <select class="form-select" name="month" id="month">
                            <option value="" >Select Month</option>  
                            @foreach($months as $key => $month)
                            <option value="{{ $key }}" {{ $key == request('month') ? 'selected' : '' }}>{{ $month }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col">  
                        <div class="form-group formcontents">
                            <label>Start date</label>
                            <input type="text" class="form-control" data-zdp_readonly_element="true" name="start_collegeupdate" value="{{request('start_collegeupdate')}}" id="start_collegeupdate">
                        </div>
                    </div>
        
                    <div class="col">
                        <div class="form-group formcontents">
                            <label>End Date</label>
                            <input type="text" class="form-control" data-zdp_readonly_element="true" name="end_collegeupdate" value="{{request('end_collegeupdate')}}" id="end_collegeupdate">
                        </div>
                    </div>
        
                    <div class="col">
                        <button type="submit" class="btn btn-primary" style="margin-top: 26px;">Submit</button>
                    </div>
                       
  
                </div>
                <div class="row">
                    <div class="col">
                  <a href="{{url('/list_requestfromsales'). '?year=' . now()->year . '&month=' . now()->month}}" class="btn btn-primary"  style="margin-top: 26px;"> View Current Month</a>
                  </div>
                  <div class="col">   
                    <a href="{{url('/list_requestfromsales')}}" class="btn btn-primary" style="margin-top: 26px;"> Clear All</a>
              </div>
                </div>
            </form>
        </div>
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Sales Person</th>
                                    <th>From Date</th>
                                    <th>To Date   </th>
                                    <th>College</th>
                                    <th>Department</th>
                                    <th>POC</th>
                                    <th>Supported Document</th>
                                    <th>Status</th>
                                    <th>Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach ($listings as $listing)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{$listing->name}}</td>
                                        <td>{{date("d-m-Y", strtotime($listing->from_date))}}</td>
                                        <td>{{date("d-m-Y", strtotime($listing->to_date))}}</td>
                                        <td>{{$listing->college}}</td>
                                        <td>{{$listing->dept}}</td>
                                        <td>{{$listing->department_pocs}}</td>
                                        <td> @if (!empty($listing->supporting_documents))
                                            @foreach (explode(',', $listing->supporting_documents) as $fileName)
                                                <a href="{{ url('/public/uploads/masterdocuments/' . $fileName) }}" target="_blank" class="view-document">{{ $fileName }}</a><br>
                                            @endforeach
                                        @else
                                            No documents uploaded
                                        @endif
                                    </td>

                                     <td>
                                        @if ($listing->status == 1)
                                        <span style="color:green;display:block;" id="act_{{ $listing->id }}">Approved<br>Comment:{{$listing->master_comment}}</span>
                                       
                                    @elseif ($listing->status == 2)
                                        <span style="color:red;display:block;" id="deact_{{ $listing->id }}">Rejected<br>Reason:{{$listing->reason}}</span>
                                   @elseif ($listing->status == 0)
                                            <span style="color:rgb(254, 169, 9);display:block;"  id="pend_{{ $listing->id }}">Pending</span>
                                            <span style="color:red;display:none;" id="deact_{{ $listing->id }}">Rejected<br>Reason:{{$listing->reason}}</span>
                                    @endif
                                      


                                    </td>
                                        <td>
                                            <button type="button" class="btn btn-cyan btn-sm text-white" fdprocessedid="wf07gv"  onclick="view_updateDetails('{{ $listing->id }}')">
                                                View updated details
                                            </button>
                                            <button type="button" class="btn btn-blue btn-sm text-white" style="display:none;" fdprocessedid="wf07gv" onclick="fetchClaimDetails('{{ $listing->id }}')">
                                                View Claimed Details
                                            </button><br>
                                            @if ($listing->status == 0 )
                                           
                                            <button type="button" class="btn btn-success btn-sm text-white" id="approve-btn-{{ $listing->id }}" onclick="approveReq('{{ $listing->id }}')">Approve</button>         
                                            <button type="button" class="btn btn-danger btn-sm text-white" id="reject-btn-{{ $listing->id }}" onclick="rejectReq('{{ $listing->id }}')">Reject</button>
                                        @endif

                                          

                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                @endforeach

                            </tbody>

                        </table>



                    </div>
                </div>

            </div>

            </div>
            </div>

                        <!-- Modal Structure -->
                        <div class="modal fade" id="collegeUpdateModal" tabindex="-1" role="dialog" aria-labelledby="collegeModalLabel">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="collegeModalLabel">Updated  Details</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                        
                                        <div id="modalContent">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            

                        <div class="modal modal-blur fade" data-bs-backdrop="static" id="modal-masterapprove" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title">Approve Request</h5>
                                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                  <form id="approveRequest" method="POST" enctype="multipart/form-data" action="{{url('/approve_request')}}">
                                    <input type="hidden" name="_token" id="token1" value="{{ csrf_token() }}">
                                  
                                    <input type="hidden" name="reqid" id="reqid">
                                    <div class="mb-3">
                                      <label class="form-label">From Date</label>
                                      <input type="text" class="form-control" style="width:auto;" id="editfromdate" name="editfromdate"
                                        data-zdp_readonly_element="true">
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">To Date</label>
                                        <input type="text" class="form-control" style="width:auto;" id="edittodate" name="edittodate"
                                          data-zdp_readonly_element="true">
                                      </div>
                                    <div class="mb-3">
                                      <label class="form-label">Comments<sup>*</sup></label>
                                      <textarea class="form-control" rows="5" name="mcomments" id="mcomments"></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="submit" class="btn btn-primary">Approve</button>
                                </div>
                              </div>
                            </div>
                            </form>
                          </div>



                          <div id="claimDetailsModal" class="modal fade" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="claimDetailsModalLabel">Claim Details</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body d-flex flex-column">
                                       
                                        <h4 id="tableHeading" class="text-center mb-3"></h4>
                        
                                       
                                        <div class="table-responsive overflow-auto">
                                            <table id="claimDetailsTable" class="table table-bordered table-striped mb-0">
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th>Package Name</th>
                                                        <th>Package Amount</th>
                                                        <th>Package Tax</th>
                                                        <th>Full Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                  
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
            <script>
                @if (Session::has('message'))
                    swal({
                        title: "",
                        text: "{{ Session::get('message') }}",
                        type: "success",
                        showCancelButton: false,
                        dangerMode: false,
                        confirmButtonText: 'OK',
                    });
                @endif
            </script>
        @endsection
