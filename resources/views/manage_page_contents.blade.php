@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">
                   
              <div class="card">
                <div class="card-body">

                    <div class="btn-list" style="float: right">
                        <!-- <a href="#" class="btn btn-primary d-none d-sm-inline-block" onclick="addContent()">
                           Download SVG icon from http://tabler-icons.io/i/plus
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                          Create new 
                        </a>-->
                        <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal" data-bs-target="#modal-report" aria-label="Create new report">
                          <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                        </a>
                      </div>

                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>
                          <li><a href="#"></a></li>
                        </ol>
                      </nav>
                   
                    <h4 class="card-title">{{$title}}</h4>


                    <div class="payment-grd">
                      <form id="searchchrys_form" method="GET" enctype="multipart/form-data" action="{{ url('/manage_page_contents') }}">
                          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
      
                          <div class="row">
                              <!-- Year Dropdown -->
                              <div class="col-lg-4 formcontents">
                                  <label for="year">Select Year<sup>*</sup></label>
                                  <select class="form-select" name="year" id="year">
                                      
                                      @foreach($years as $year)
                                      <option value="{{ $year }}" {{ $year == $currentYear ? 'selected' : '' }}>{{ $year }}</option>
                                      @endforeach
                                  </select>
                              </div>
      
                              <!-- Month Dropdown -->
                              <div class="col-lg-6 formcontents">
                                  <label for="month">Select Month<sup>*</sup></label>
                                  <select class="form-select" name="month" id="month">
                                     
                                      @foreach($months as $key => $month)
                                      <option value="{{ $key }}" {{ $key == $currentMonth ? 'selected' : '' }}>{{ $month }}</option>
                                      @endforeach
                                  </select>
                              </div>
      
                              <div class="col-lg-2 formcontents">
                                  <button type="submit" class="btn btn-primary" style="margin-top: 26px;">Submit</button>
                            </div>
      
                          </div>
      
                          <div class="col formcontents"> 
                            <a href="{{url('/manage_page_contents/')}}" class="btn btn-primary"  style="margin-top: 26px;"> View Current Month</a>
                            </div>
                      </form>
                  </div>








                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Description</th>
                <th>Date</th>
              <!-- <th>Action</th>-->
            </tr>
        </thead>
        <tbody>
<?php $i=1;?>
            @foreach($contents as $contents)
            <tr>
                <td>{{$i}}</td>
                <td>{{$contents->name}}</td>
                <td>{{$contents->content}}</td>

                <?php

                  list($date, $time) = explode(" ", $contents->created_at);

                ?>
                <td>{{$date}}</td>
                <!--  <td>
                         <button type="button" class="btn btn-cyan btn-sm text-white" fdprocessedid="wf07gv" onclick="editContent('{{$contents->id}}')">
                                    Edit
                          </button>
                 
                          
                 </td>-->
               </tr>
            <?php $i++;?>
            @endforeach
           
        </tbody>
      
    </table>

    <form id="content_form" method="POST" enctype="multipart/form-data"  action="{{url('/save_content')}}">
     
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
          
    <div class="modal modal-blur fade" id="modal-content" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title">New Content</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
        
      <div class="modal-body">
          <div class="mb-3">
            <label class="form-label">Type<sup>*</sup></label>
            <input type="text" class="form-control" name="type" id="type" >
          </div>
          <div class="mb-3">
          <label class="form-label">Contents<sup>*</sup></label>
          <textarea class="form-control" name="content" id="content" rows="4"></textarea>
          </div>
 
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>                                            
      </div>
    
    </div>
    </div>
    </form>


                    </div>
                </div>
           
            </div>


<!--Edit model-->
    <form id="editcontent" method="POST" enctype="multipart/form-data"  action="{{url('/update_content')}}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="modal modal-blur fade" id="modal-edit-content" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Edit Content</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
      <div class="modal-body">
     
          <div class="mb-3">
            <input type="hidden" class="form-control" name="editcontentid" id="editcontentid" >
            <label class="form-label">Type<sup>*</sup></label>
            <input type="text" class="form-control" name="edittype" id="edittype" >
          </div>
          
          <div class="mb-3">
            <label class="form-label">Contents<sup>*</sup></label>
            <textarea class="form-control" name="editcontent" id="editcontent" rows="4"></textarea>
          </div>
      <div class="modal-footer">
        <button type="submit" id="event_edit" class="btn btn-primary">Update</button>
      </div>                                            
      </div>

        </div>
      </div>
    </form>



@endsection
