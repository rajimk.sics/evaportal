@extends('layouts.main')

@section('content')

<!-- Page body -->
<div class="page-body">
    <div class="container-xl">
        <!-- CSRF Token -->
        <input type="hidden" name="token_eva" id="token_eva" value="{{ csrf_token() }}">

        <div class="card">
            <div class="card-body">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumbs">
                        <li><a href="{{ url('/home') }}">Home</a></li>
                        <li><a href="#">{{ $title }}</a></li>
                    </ol>
                </nav>
                <h4 class="card-title">{{ $title }}</h4>


                <div class="payment-grd">
                    <form id="searchchrys_form" method="GET" enctype="multipart/form-data" action="{{ url('/approved_payment') }}">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                        <div class="row">
                            <!-- Year Dropdown -->
                            <div class="col-lg-6 formcontents">
                                <label for="year">Select Year<sup>*</sup></label>
                                <select class="form-select" name="year" id="year">
                                    <option value="">Select Year</option>
                                    @foreach($years as $year)
                                    <option value="{{ $year }}" {{ $year == $currentYear ? 'selected' : '' }}>{{ $year }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <!-- Month Dropdown -->
                            <div class="col-lg-6 formcontents">
                                <label for="month">Select Month<sup>*</sup></label>
                                <select class="form-select" name="month" id="month">
                                    <option value="">Select Month</option>
                                    @foreach($months as $key => $month)
                                    <option value="{{ $key }}" {{ $key == $currentMonth ? 'selected' : '' }}>{{ $month }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col formcontents">
                                <button type="submit" class="btn btn-primary" style="margin-top: 26px;">Submit</button>
                                </div>

                                <div class="col formcontents"> 
                                <a href="{{url('/approved_payment/')}}" class="btn btn-primary"  style="margin-top: 26px;"> View Current Month</a>
                                </div>

                        </div>
                    </form>
                </div>

                <div class="payment-grd">
                    <p><b>Total Payment with GST:</b> <span>Rs {{ number_format($total_payment_withgst, 2) }}</span></p>
                    <p><b>Total Tax:</b> <span>Rs {{ number_format($total_tax, 2) }}</span></p>
                    <p><b>Total Payment:</b> <span>Rs {{ number_format($total_payment, 2) }}</span></p>
                </div>

                <div class="table-responsive">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Student Name</th>
                                <th>Closed By</th>
                                <th>Payment Type</th>
                                <th>Transaction Id</th>
                                <th>Screenshot</th>
                                <th>Payment with GST</th>
                                <th>Tax</th>
                                <th>Total Payment</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($payment_history as $history)
                            <tr id="row_{{$i}}">
                                <td>{{ $i }}</td>
                                <td>{{ ucfirst($history->name) }}</td>

                                @if($history->source=='payment')
                                @php
                                $empnames=App\Helpers\CustomHelper::empnames($history->payment_id); 
                                @endphp
                                 <td>{{ $empnames }}</td>
                            @else
                            @php
                            $empnames=App\Helpers\CustomHelper::uname($history->sales_id); 
                            @endphp
                             <td>{{ $empnames->name }}</td>
                            @endif
                                <td>
                                    @if($history->payment_type == 'bank')
                                    Bank Transfer
                                    @else
                                    Cash
                                    @endif
                                </td>
                                <td>{{ $history->transaction_id }}</td>
                                <td>
                                    @if($history->screenshot)
                                    <a href="#" data-bs-toggle="modal" data-bs-target="#imageModal" class="prviewimage" data-bs-image="{{ url('public/uploads/screenshot/' . $history->screenshot) }}" >
                                        <img src="{{ url('public/uploads/screenshot/' . $history->screenshot) }}" width="100" height="100" alt="Screenshot">
                                    </a>
                                    @endif
                                </td>
                                <td>{{ number_format($history->payment_gst, 2) }}</td>
                                <td>{{ number_format($history->tax, 2) }}</td>
                                <td>{{ number_format($history->payment, 2) }}</td>
                                <td>{{ date('d-m-Y', strtotime($history->date)) }}</td>
                                <td>
                                    @php
                                    $name=App\Helpers\CustomHelper::uname($history->action_by); 
                                  @endphp
                                  <span id="action">Approved By {{$name['name']}}</span>
          
                              </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- Modal for Image Preview -->
        <div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="imageModalLabel">Image Preview</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <img id="modal-image" src="" class="img-fluid" alt="Large Image">
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@section('scripts')
<script>

</script>
@endsection

@endsection
