@extends('layouts.main')

@section('content')
   <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">   
              <div class="card">
                <div class="card-body">
                  <nav aria-label="breadcrumb">
                    <ol class="breadcrumbs">
                      <li><a href="{{url('/home')}}">Home</a></li>
                      <li><a href="#">{{$title}}</a></li>
                    </ol>
                  </nav>
                  <input type="hidden" name="_token" id="token"  value="{{ csrf_token() }}">
                   
                    <h4 class="card-title">{{$title}}</h4>
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Employee Id</th>
                <th>Poc Code</th>
                <th>Employee Name</th>
                <th>Employee Email</th>
                <th>Password</th>
                <th>Reported Person</th>
                <th>Privileges</th>
                <th>Status</th>
                <th>Action</th>
               
            </tr>
        </thead>
        <tbody>
          <?php $i=1;?>
                      @foreach($emplist as $emplist)
                      <tr>
                          <td>{{$i}}</td>
                          <td>{{$emplist->empid}}</td>
                          <td>{{$emplist->poc_code}}</td>
                          <td>{{$emplist->name}}</td>
                          <td>{{$emplist->email}}</td>
                          <td>{{$emplist->password_text}}</td> 
                          <td>
                     
                            @php
                              $name=App\Helpers\CustomHelper::username($emplist->id); 
                            @endphp
  
                            {{$name}}
  
                            </td>
                            <td>
                              
                              Add Employee -@if($emplist->addemployee_privilage==1) Yes @else No @endif
                            
                            </td> 
                            <td>

                              @if($emplist->status==1)
  
                              <span style="color:green;display:block;" id="act_{{$emplist->id}}">Active</span>
                              <span style="color:red;display:none;" id="deact_{{$emplist->id}}" >Deactive</span>  
  
                              @else
  
                              <span style="color:green;display:none;" id="act_{{$emplist->id}}">Active</span>
                              <span style="color:red;display:block;" id="deact_{{$emplist->id}}" >Deactive</span>  
                              @endif
                            </td>
                        
                          <td>
                            <button type="button" class="btn btn-cyan btn-sm text-white" fdprocessedid="wf07gv" onclick="employeedetails('{{$emplist->id}}')">
                              Details
                            </button>
                            <a href="{{url('/edit_employee/'.$emplist->id)}}"><button type="button" class="btn btn-green btn-sm text-white" fdprocessedid="wf07gv">
                              Edit
                            </button></a>
                            @if($emplist->status==1)
                            <button type="button" style="display:block;" id="deactb_{{$emplist->id}}" class="btn btn-danger btn-sm text-white" fdprocessedid="pvth1" onclick="deactivate({{$emplist->id}})">
                             Deactivate
                            </button>

                            <button type="button" style="display:none;" id="actb_{{$emplist->id}}"  class="btn btn-green btn-sm text-white" fdprocessedid="pvth1"  onclick="activate({{$emplist->id}})">
                              Activate
                             </button>

                            @else
                            <button type="button" tyle="display:block;" id="actb_{{$emplist->id}}"  class="btn btn-green btn-sm text-white" fdprocessedid="pvth1"  onclick="activate({{$emplist->id}})">
                              Activate
                             </button>
                             <button type="button" style="display:none;" id="deactb_{{$emplist->id}}" class="btn btn-danger btn-sm text-white" fdprocessedid="pvth1" onclick="deactivate({{$emplist->id}})">
                              Deactivate
                             </button>
                            @endif

                          </td>
                         
                      </tr>
                      <?php $i++;?>
                      @endforeach
                     
                  </tbody>
      
    </table>
                    </div>
                </div>
           
            </div>

      
<input type="hidden" name="_token" id="token"  value="{{ csrf_token() }}">
<div class="modal modal-blur fade" id="modal-employee" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Employee Details</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body" id="employeedetails">
         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>      
@endsection
