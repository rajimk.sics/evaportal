@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">  
                   
              <div class="card">
                <div class="card-body">

                    <div class="btn-list" style="float: right">
                     
                        <a href="#" class="btn btn-primary d-none d-sm-inline-block"  onclick="addspec()">
                          <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                          Create new 
                        </a>
                        <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal" data-bs-target="#modal-report" aria-label="Create new report">
                          <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                        </a>
                      </div>

                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>
                          <li><a href="#">{{$title}}</a></li>
                        </ol>
                      </nav>
                    <h4 class="card-title">{{$title}} </h4>

                    <a href="{{url('/public/uploads/excel/specialization.xlsx')}}" download="Specialization">Download  Sample Excel Template </a>
                    <form action="{{ url('bulkupload_specialization') }}" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                           <div class="file-upload-custom mt-3 mb-3">
                              <input style="    height: inherit;" class="form-control" type="file" name="file">
                              <button type="submit" class="btn btn-blue btn-sm text-white">Upload</button>
                           </div>
                       </form>
                      
                       @if(Session::has('duplicates'))
                      <div class="alert alert-warning">
                        <p>Duplicate values:</p>
                         <ul>
                            @foreach(Session::get('duplicates') as $duplicate)
                               <li>{{ $duplicate }}</li>
                            @endforeach
                        </ul>
                      </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                @endforeach
                           </ul>
                           </div>
                      @endif








                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Specialization</th>
                <th>Status</th>
                <th>Action</th>
               
            </tr>
        </thead>
        <tbody>
<?php $i=1;?>
            @foreach($specialization as $specialization)
            <tr>
                <td>{{$i}}</td>
                <td>{{$specialization->specialization}}</td>
                <td>

                  @if($specialization->status==1)
                  <span style="color:green;display:block;" id="act_{{$specialization->id}}">Active</span>
                  <span style="color:red;display:none;" id="deact_{{$specialization->id}}" >Deactive</span>                       
                  @else
                  <span style="color:green;display:none;" id="act_{{$specialization->id}}" >Active</span>
                  <span style="color:red;display:block;" id="deact_{{$specialization->id}}">Deactive</span>    
                  
                  @endif


                </td>
                <td>
                  <div class="d-flex">
                  <button type="button" class="btn btn-cyan btn-sm text-white me-2" fdprocessedid="wf07gv" onclick="editspecialization('{{$specialization->id}}','{{$specialization->specialization}}')">
                    Edit
                  </button>
                  @if($specialization->status==1)
                            <button type="button"  style="display: block" id="deactb_{{$specialization->id}}" class="btn btn-danger btn-sm text-white" fdprocessedid="pvth1" onclick="dspecialization('{{$specialization->id}}')">
                             Deactivate
                            </button>
                            <button type="button" style="display: none" id="actb_{{$specialization->id}}" class="btn btn-success btn-sm text-white" fdprocessedid="pvth1" onclick="aspecialization('{{$specialization->id}}')">
                              Activate
                             </button>
                            @else
                            <button type="button" style="display: block" id="actb_{{$specialization->id}}" class="btn btn-success btn-sm text-white" fdprocessedid="pvth1" onclick="aspecialization('{{$specialization->id}}')">
                              Activate
                             </button>
                             <button type="button"   style="display: none" id="deactb_{{$specialization->id}}" style="display: block" id="deactb_{{$specialization->id}}" class="btn btn-danger btn-sm text-white" fdprocessedid="pvth1" onclick="dspecialization('{{$specialization->id}}')">
                              Deactivate
                             </button>
                            @endif
                  </div>
                </td>
               
            </tr>
            <?php $i++;?>
            @endforeach
           
        </tbody>
      
    </table>
    <form id="specialization_form" method="POST" enctype="multipart/form-data"  action="{{url('/save_specialization')}}">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="modal modal-blur fade" id="modal-specialization" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">New Specialization</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="mb-3">
            <label class="form-label">Specialization<sup>*</sup></label>
            <input type="text" class="form-control" name="specialization" id="specialization" placeholder="Specialization">
          </div>
        </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>                                            
      </div>
    
    </div>
    </div>
    </form>


    <form id="edit_specialization" method="POST" enctype="multipart/form-data"  action="{{url('/edit_specialization')}}">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="modal modal-blur fade" id="modal-edit-specialization" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Edit Specialization</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="mb-3">
            <label class="form-label">Specialization<sup>*</sup></label>
            <input type="hidden" class="form-control" name="editspecid" id="editspecid" placeholder="ID">

            <input type="text" class="form-control" name="editspecname" id="editspecname" placeholder="Specialization">
          </div>
        </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Update</button>
      </div>                                            
      </div>
    
    </div>
    </div>
    </form>







                    </div>
                </div>
           
            </div>
            <script>
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif
        </script>




@endsection
