@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
                   
              <div class="card">
                <h4 class="card-title">Manage Tax</h4>
                    <div class="card-body">
                     <form id="packageTax" method="POST" action="{{url('/update_tax')}}">
                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="mb-3">
                        <label class="form-label">Tax %<sup>*</sup></label>
                        <input type="number" class="form-control" name="tax" id="tax" value="{{$tax->tax}}">

                        <input type="hidden" class="form-control" name="tax_id" id="tax_id" value="{{$tax->id}}">
                        </div>
                    </div>
                      <div class="card-footer text-end">
                        <div class="d-flex">
                          <button type="submit" class="btn btn-primary ms-auto">Update</button>
                        </div>
                      </div>
                        </form>
               </div>
            </div>
          </div>
            <script>
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif
        </script>




@endsection
