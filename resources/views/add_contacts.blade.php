@extends('layouts.main')
@section('content')


<style>

  .formcontents{
    display: flex;
    flex-direction: column;
  }
  label.error{
    color: red;
    order: 3;
  }

  </style>
   <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">   
              <div class="row row-cards">
                <div class="col-12">

                  <form id="addcontacts">
                    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">


                    
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumbs">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="#">{{$title}}</a></li>
                  </ol>
                </nav>
                    <div class="card-header">
                      <h2 class="fs-title">{{$title}}</h2>
                    </div>

                   
                    <div class="card-body">
                      <div class="">
                          <div class="row">
                            <div class="col-lg-6 formcontents">
                            <div class="form-group">
                                <label class="form-label">Source <sup>*</sup></label>
                                <select class="form-select selecttype" id="source" name="source" >
                                  <option value="">Select Source</option>
                                  @foreach ($sourcelist as $item)
                                       <option value="{{$item->id}}">{{$item->source}}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                            <div class="col-lg-6 formcontents">
                            <div class="form-group">
                                <label class="form-label">Name</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Name" >
                              </div>
                            </div>
                            <div class="col-lg-6 formcontents">
                            <div class="form-group">
                                <label class="form-label">Contact Number 1<sup>*</sup></label>
                                <input type="text" class="form-control contactnum" name="cno1" minlength="10"  maxlength="13" id="cno1" placeholder="Contact Number 1">
                              </div>
                            </div>
                            <div class="col-lg-6 formcontents">
                            <div class="form-group">
                                <label class="form-label">Contact Number 2</label>
                                <input type="text" minlength="10" maxlength="13" class="form-control contactnum" name="cno2" id="cno2" placeholder="Contact Number 2">
                              </div>
                            </div>
                            <div class="col-lg-6 formcontents">
                              <div class="form-group">
                                  <label class="form-label">Email</label>
                                  <input type="text" class="form-control" name="email" id="email" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-lg-6 formcontents">
                            <div class="form-group">
                                <label class="form-label">College Name</label>
                                <select class="form-select selectdesi" name="college"  id="college" placeholder="College Name">
                                  <option value="">Select College</option>
                                  @foreach($collegelist as $collegelist )
                                  <option value="{{$collegelist->id}}">{{$collegelist->college}}</option>
                                  @endforeach
                          
                                </select>
                              </div>
                            </div>
                            <div class="col-lg-6 formcontents">
                            <div class="form-group">
                                <label class="form-label">Qualification</label>
                                <select class="form-select selecttech" id="qualification" name="qualification">
                                  <option value="">Select Qualification</option>
                                  @foreach($qualification as $qualification )
                                  <option value="{{$qualification->id}}">{{$qualification->qualification}}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                            <div class="col-lg-6 formcontents">
                            <div class="form-group">
                                <label class="form-label">Year of passing</label>
                                <select class="form-select selecttech" id="year" name="year">
                                  <option value="">Select Year</option>
                                 <?php $currentYear = date("Y");

                                         // Define a range of years (adjust as needed)
                                          $startYear = $currentYear - 50;
                                          $endYear = $currentYear;

                                        // Loop to generate dropdown options for years
                                        for ($year = $endYear; $year >= $startYear; $year--) {
                                                 echo "<option value=\"$year\">$year</option>";
                                         }
                                 ?>
       
                                </select>
                              </div>
                            </div>





                            
                              
                          
                          

                        </div>
                      </div>


                    </div>
                  </div>
                  <div class="card-footer text-end">
                    <div class="d-flex">
                      
                      <button type="submit" class="btn btn-primary ms-auto">Submit</button>
                    </div>
                  </div>
                </form>

              
                </div>
           
            </div>

            <script>
        
             
              </script>
            
            <script>
            @if (Session::has('errormessage'))
            swal({
                title: "",
                text: "{{ Session::get('errormessage') }}",
                type: "sucess",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif 
      </script>         
@endsection
