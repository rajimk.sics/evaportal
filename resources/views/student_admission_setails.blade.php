@extends('layouts.main')
@section('content')
   <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">   
              <div class="row row-cards">
                
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-header">
                      <h2> Admission Details </h2>
                    </div>
                    <div class="card-body">
                      <div class="tab-content">
                        <div class="tab-pane fade active show" id="tabs-info-8" role="tabpanel">                       
                          <div>

                            <p>Full Name: {{ucfirst($studentdetails->name)}}</p>
                            <p>Primary Contact No : {{$studentdetails->phone}} </p>
                            <p>Secondary Contact No : {{$studentdetails->stud_sec_phone}} </p>
                            <p>Whatsapp Number: {{$studentdetails->stud_whatsapp_no}} </p>
                            <p>Primary Email Id : {{$studentdetails->stud_email}} </p>

                           
                            <p>Secondary Email Id : {{$studentdetails->stud_sec_email}} </p>
                            <p>Date Of Birth : {{$studentdetails->stud_dob}} </p>

                            <p>Blood Group : {{$studentdetails->stud_blood}} </p>
                            <p>Father Name : {{$studentdetails->stud_father_name}} </p>
                            <p>Father Occupation: {{$studentdetails->stud_father_occu	}} </p>
                            <p>Father Contact No : {{$studentdetails->stud_father_no}} </p>

                            <p>Father Email Id : {{$studentdetails->stud_father_email	}} </p>
                            <p>Mother Name : {{$studentdetails->stud_mother_name	}} </p>
                            <p>Mother Occupation : {{$studentdetails->stud_mother_occu}} </p>
                            <p>Mother Contact No : {{$studentdetails->stud_mother_no}} </p>
                            <p>Mother Email Id : {{$studentdetails->stud_mother_email	}} </p>
                            <p>Full Address (Permanent) : {{$studentdetails->stud_per_add}} </p>
                            <p>City : {{$studentdetails->stud_city}} </p>  
                            <p>Town : {{$studentdetails->stud_town}} </p>  
                          </div>
                        </div>
                       
                      </div>

                    </div>
                  </div>
                </div>
           
            </div>

            
@endsection
