@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">

              <input type="hidden" name="logname" id="logname" value="{{ Auth::user()->name }}">
                   
              <div class="card">
                <div class="card-body">

                     
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumbs">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="#">{{$title}}</a></li>
                  </ol>
                </nav>
                   
                    <h4 class="card-title">{{$title}}</h4>


                    <div class="payment-grd">
                      <form id="searchchrys_form" method="GET" enctype="multipart/form-data" action="{{ url('/approved_gatepass') }}">
                          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
      
                          <div class="row">
                              <!-- Year Dropdown -->
                              <div class="col-lg-4 formcontents">
                                  <label for="year">Select Year<sup>*</sup></label>
                                  <select class="form-select" name="year" id="year">
                                      
                                      @foreach($years as $year)
                                      <option value="{{ $year }}" {{ $year == $currentYear ? 'selected' : '' }}>{{ $year }}</option>
                                      @endforeach
                                  </select>
                              </div>
      
                              <!-- Month Dropdown -->
                              <div class="col-lg-6 formcontents">
                                  <label for="month">Select Month<sup>*</sup></label>
                                  <select class="form-select" name="month" id="month">
                                     
                                      @foreach($months as $key => $month)
                                      <option value="{{ $key }}" {{ $key == $currentMonth ? 'selected' : '' }}>{{ $month }}</option>
                                      @endforeach
                                  </select>
                              </div>
      
                              <div class="col-lg-2 formcontents">
                                  <button type="submit" class="btn btn-primary" style="margin-top: 26px;">Submit</button>
                            </div>
      
                          </div>
      
                          <div class="col formcontents"> 
                            <a href="{{url('/approved_gatepass/')}}" class="btn btn-primary"  style="margin-top: 26px;"> View Current Month</a>
                            </div>
                      </form>
                  </div>










                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Student Name</th>
                <th>Photo</th>
                <th>Requested Date</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Type</th>
               
                <th>Status</th>

                @if(Auth::user()->role !='2')

               
               <th>Action</th>

               @endif
              
            </tr>
        </thead>
        <tbody>
        <?php $i=1;?>
            @foreach($gatelist as $gatelist)
            <tr id="row_{{$gatelist->id}}">
                <td>{{$i}}</td>
                <td>{{$gatelist->name}}</td>
                <td>  <img src="{{url('public/uploads/photo/'.$gatelist->photo)}}" width="50px" height="50px" ></td>
                <td>
                  {{date("d-m-Y", strtotime($gatelist->requested_date))}}
                
                </td>
                <td>
                 <span id="sdate_{{$gatelist->id}}"> {{date("d-m-Y", strtotime($gatelist->start_date))}}</span>
                  
                 </td>
                <td>
                  <span id="edate_{{$gatelist->id}}">{{date("d-m-Y", strtotime($gatelist->end_date))}}</span>
                 </td>
                <td>

                  @php
                  $type_name=App\Helpers\CustomHelper::typename($gatelist->type,$gatelist->type_id); 
                  @endphp
                  @if($gatelist->type==1)
                  Regular-{{ $type_name['name']}}
                  @endif
                  @if($gatelist->type==2)
                  Chrysalis-{{ $type_name['name']}}
                  @endif
                  @if($gatelist->type==3)
                  Event-{{ $type_name['name']}}
                  @endif
                </td>
                <td>

                  @if($gatelist->status==0)  
                  
                  <span style="color:orange" id="pend_{{$gatelist->id}}">Pending</span>
                  <span style="color:green;display:none;" id="act_{{$gatelist->id}}">Approved</span>
                  <span style="color:red;display:none;" id="deact_{{$gatelist->id}}" >Rejected</span>  
                  
                  @elseif($gatelist->status==1)
                  <span style="color:green" id="act_{{$gatelist->id}}">Approved</span>
                  
                  @elseif($gatelist->status==2)
                  <span style="color:red" id="deact_{{$gatelist->id}}">Rejected</span>

                  @else
                  <span style="color:green"  id="status_{{$gatelist->id}}">Returned</span>
                  @endif
                  <span id="status_{{$gatelist->id}}" style="color:green;display:none;">

                  </span>


                </td>

                @if(Auth::user()->role !='2')
                
                  <td>

                    <div id="rby_{{$gatelist->id}}">

                      @if($gatelist->status==3)
                      @php
                        $name=App\Helpers\CustomHelper::uname($gatelist->return_by); 
                      @endphp
                      <span>Returned To {{$name['name']}}</span>
                    @endif

                    </div>



                  <div id="action_{{$gatelist->id}}">

                  @if(($gatelist->status==0)&& (Auth::user()->role!='2'))  

                  <button type="button" id="actb_{{$gatelist->id}}"  class="btn btn-success btn-sm text-white" fdprocessedid="pvth1" onclick="approvePass('{{$gatelist->id}}')">
                             Approve
                  </button> 
                  <button type="button" id="deactb_{{$gatelist->id}}"  class="btn btn-danger btn-sm text-white" fdprocessedid="pvth1" onclick="rejectPass('{{$gatelist->id}}')">
                             Reject
                  </button>
                  
                  <?php

                  $d1=date("d-m-Y", strtotime($gatelist->start_date));
                  $d2=date("d-m-Y", strtotime($gatelist->end_date));


                  ?>
                  <button type="button" id="date_{{$gatelist->id}}"  class="btn btn-success btn-sm text-white" fdprocessedid="pvth1" onclick="changedate('{{$gatelist->id}}','{{$d1}}','{{$d2}}')">
                    Change Date
                  </button>                               
                @endif

                @if($gatelist->status==1)
                  @php
                    $name=App\Helpers\CustomHelper::uname($gatelist->action_by); 
                  @endphp
                  <span>Approved By {{$name['name']}}</span></br>
                @endif
                @if($gatelist->status==2)

                    @php
                    $name=App\Helpers\CustomHelper::uname($gatelist->action_by); 
                    @endphp
                    <span>Rejected By {{$name['name']}}</span></br>
                    <span>Reason :{{$gatelist->reason}} </span></br>
                @endif

                @if($gatelist->status==0)
                  <span id="action_by_{{$gatelist->id}}"> </span></br>
                  <span id="reasonby_{{$gatelist->id}}"> </span>
                @endif

              @if(($gatelist->status==1)&& (Auth::user()->role!='2'))  
              
                <span id="downspan_{{$gatelist->id}}"><a href="{{url('/gatepass_download/'.$gatelist->id)}}"  id="down_{{$gatelist->id}}" class="download-linkpass">Download</a></span></br>

                  <button type="button" id="ret_{{$gatelist->id}}"  class="btn btn-success btn-sm text-white" fdprocessedid="pvth1" onclick="returnPass('{{$gatelist->id}}')">
                    Return
                 </button> 

              @else

                  <span id="downspan_{{$gatelist->id}}"></span></br>
                  <button type="button" id="ret_{{$gatelist->id}}" style="display:none"  class="btn btn-success btn-sm text-white" fdprocessedid="pvth1" onclick="returnPass('{{$gatelist->id}}')">
                    Return
                 </button> 

              @endif
                </div>
                       
                 </td>
                 @endif
               </tr>
            <?php $i++;?>
            @endforeach
           
        </tbody>
      
    </table>
<form id="returngatepass" >
  <input type="hidden" name="_token" id="token_evas" value="{{ csrf_token() }}">   
    <div class="modal modal-blur fade" data-bs-backdrop="static" id="modal-returngatepass" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Return Gatepass</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="mb-3">
          <input type="hidden" name="gpass_id" id="gpass_id" >
            <label class="form-label">Upload gatepass<sup>*</sup></label>
            <input type="file" class="form-control"  name="gate_file" id="gate_file" >
          </div>
         
        </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>                                            
      </div>
    
    </div>
    </div>
    </form>





    <form id="changedateform" method="POST" enctype="multipart/form-data"  action="{{url('/changedateform')}}">
      <input type="hidden" name="_tokens" id="token_eva" value="{{ csrf_token() }}">
      <input type="hidden" name="pass_id" id="pass_id" value="">
    <div class="modal modal-blur fade" data-bs-backdrop="static" id="modal-changedate" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Change Date</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="mb-3">
            <label class="form-label">Start Date<sup>*</sup></label>
            <input type="text" class="form-control" data-zdp_readonly_element="true" name="cdate1" id="cdate1" >
          </div>
          <div class="mb-3">
            <label class="form-label">End Date<sup>*</sup></label>
            <input type="text" class="form-control" data-zdp_readonly_element="true" name="cdate2" id="cdate2" >
          </div>
        </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>                                            
      </div>
    
    </div>
    </div>
    </form>

    


@endsection
