@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">
              <input type="hidden" name="token_eva" id="token_eva"  value="{{ csrf_token() }}">
              <div class="card">
                <div class="card-body">

                    <div class="btn-list" style="float: right">
                 @if(Auth::user()->role==2)
                      <a href="#" class="btn btn-primary d-none d-sm-inline-block" onclick="pay()">
                        <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                       
                       Pay Now
                      </a>
                    @endif
                      </div>

                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>
                          <li><a href="{{url('/manage_oldstudent')}}">Old Students</a></li>
                          <li><a href="#">{{$title}}</a></li>
                        </ol>
                      </nav>
                      <h4 class="card-title">{{$title}}</h4>


                      @php


                        $rem=$totalfees - $paidfees;


                      @endphp


                      <div class="payment-grd" >

                        <P><b> Total Amount :</b><span> Rs {{number_format($totalfees,2)}} </span>
                        <P><b> Paid Amount :</b><span>Rs {{number_format($paidfees,2)}} </span>
                        <P><b> Remaining Amount :</b>   Rs {{number_format($rem,2)}}<span>
                       
                        </span>
                      </div>
                    
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Paid Fees</th>
                <th>Pending Fees</th>
                <th>Payment Type</th>
                <th>Transaction Id</th>
                <th>Screenshot</th>

                <th>Date</th>
                <th>Receipt</th>
              
               
            </tr>
        </thead>
        <tbody>
            <?php $i=1;?>

            @foreach($payment_history as $history)
         
            <tr>
                <td>{{$i}}</td>
               
                <td>Rs {{number_format($history->paid_amount,2)}}</td>
                <td>Rs {{number_format($history->pending_amount,2)}}</td>

                <td>
                  
                  @if($history->payment_type=='bank')

                  Bank Transfer

                  @else

                  Cash

                  @endif
                  
                  
                  
                 </td>
                <td>{{$history->transaction_id}}</td>
                <td>


                
                  
                  @if($history->screenshot !='')
                  <a href="#" data-bs-toggle="modal" data-bs-target="#imageModal" class="prviewimage" data-bs-image="{{ url('public/uploads/screenshot/' . $history->screenshot) }}" >
                  <img  src="{{url('public/uploads/screenshot/'.$history->screenshot)}}" width="100px" height="100px">
                  </a>
                  @endif
                </td>
                <td>{{date("d-m-Y", strtotime($history->date))}}</td>
                <td><a href="{{url('/receipt_download/'.$history->id)}}" class="download-link">Download</a></td>
            </tr>
            <?php $i++;?>
            @endforeach
           
                
        </tbody>     
    </table>

                    </div>
                </div>         
            </div>


                   <!-- Modal for Image Preview -->
        <div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="imageModalLabel">Image Preview</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                      <img id="modal-image" src="" class="img-fluid" alt="Large Image">
                  </div>
              </div>
          </div>
      </div>
           
            <div class="modal modal-blur fade" data-bs-backdrop="static" id="modal-pay-package" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title">Payment</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body" id="studentPackage1">
                
                  <form name="paymentMethod" id="paymentMethod"  enctype="multipart/form-data"   action="{{url('/payment_old')}}"method="POST">
                  <input type="hidden" name="_token" id="token"  value="{{ csrf_token() }}">

                  
                  <input type="hidden" name="studentpackage_id" id="studentpackage_id" value="">
                  <input type="hidden" name="remamount" id="remamount" value="{{$rem}}">
                  <input type="hidden" name="student_id" id="student_id" value="{{$student_id}}">
                 

                  <input type="hidden" name="pac_type" id="pac_type" value="">
                  
                  
                      <div class="d-flex ">
                        <div class="d-flex align-items-center me-4">
                          <input class="me-2" type="radio" id="cash" name="payMethod"  value="cash" checked>
                          <label for="cash">Cash</label>
                        </div>
                        <div class="d-flex align-items-center">
                         <input class="me-2" type="radio" id="banktransfer" name="payMethod" value="bank">
                          <label for="banktransfer">Bank Transfer</label><br>
                        </div>
                      </div>

                      <div class="d-flex ">

                        <div class="mb-3">
                          <label class="form-label">Date<sup>*</sup></label>
                          <input type="text" style="width:auto"  data-zdp_readonly_element="true" name="oldpay_date" id="oldpay_date" class="form-control" value="{{date('d-m-Y')}}">
                       </div>

                      </div>
                      <div id="packagecash">
                          <div class="mb-3">
                             <label class="form-label" id="paymethod">Cash<sup>*</sup></label>
                             <input type="number" class="form-control" name="amount" id="amount"  min=1 oninput="validity.valid||(value='');">
                          </div>
                      <div id="packagebank" style="display:none">
                          <div class="mb-3">
                            <label class="form-label">Payment Screenshot<sup>*</sup></label>
                            <input type="file" class="form-control" name="package_reciept" id="package_reciept">
                          </div>
                          <div class="mb-3">
                            <label class="form-label">Transaction Id<sup>*</sup></label>
                            <input type="text" class="form-control" name="trans_id" id="trans_id">
                          </div>
                      </div>
                 <div class="modal-footer">
                      <button type="submit" class="btn btn-primary ms-auto">PAY</button>
                </div>
               
                  </div>
                 
                  
                  </form>
                 
                </div>
              </div>
            </div>   
            
            
       
  
@endsection
