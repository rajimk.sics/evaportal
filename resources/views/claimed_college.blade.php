@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">
                   
              <div class="card">
                <div class="card-body">

                   
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>
                          <li><a href="#">{{$title}}</a></li>
                        </ol>
                      </nav>
                    <h4 class="card-title">{{$title}}</h4>

                    <form id="filterDepartment_form" method="GET" action="{{ url('/claimed_collegeSales') }}">     
                      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                      <div class="row">
                          <div class="col">
                              <div class="form-group formcontents">
                                  <label for="deptname">Department<sup>*</sup></label>
                                  <select name="deptname" id="deptname" class="form-select">
                                    <option value="">Select Department</option>
                                    @foreach($departments as $departmentName)
                                        <option value="{{ $departmentName }}" {{ request('deptname') == $departmentName ? 'selected' : '' }}>
                                            {{ $departmentName }}
                                        </option>
                                    @endforeach
                                </select>
                                                              </div>
                          </div>
                          <div class="col d-flex align-items-center">
                              <div class="form-group formcontents mb-0 flex-row">
                                  <button type="submit" class="btn btn-primary me-2">Submit</button>
                                  <a href="{{ url('/claimed_collegeSales') }}" class="btn btn-primary">Clear All</a>
                              </div>
                          </div>
                      </div>
                  </form>
                  


                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>College</th>
                <th>Department</th>
                <th>Package Name</th>
                <th>Amount without GST</th>
                <th>Tax</th>
                <th>Amount with GST</th>
                {{-- <th>Claimed by</th> --}}
               
                <th>Action</th>  
                
             
            </tr>
        </thead>
        <tbody>
<?php $i=1;?>
            @foreach($claimdetails as $claimdetails)
            <tr>
                <td>{{$i}}</td>
                @php
                $colName=App\Helpers\CustomHelper::collegeName($claimdetails->college_id); 
                 @endphp
              
                 <td>{{$colName['college']}}@if(!is_null($colName['name']))
                  , {{ strtoupper($colName['name']) }}  @endif
                  </td>
                  <td>{{$claimdetails->department}}  
                  </td>
                 @php
                 $packName=App\Helpers\CustomHelper::typename(2,$claimdetails->package_id); 
                  @endphp
	

               <td>{{$packName['name']}}</td>
               <td>{{number_format($claimdetails->total_package_amount, 2)}}</td>
               <td>{{number_format($claimdetails->total_package_tax,2)}}</td>
               <td>{{number_format($claimdetails->total_package_fullamount,2)}}</td>

            
                <td>
                  @if(in_array(Auth::user()->role, [2]))
                  <a href="{{url('/claimstudents_list/'.$claimdetails->claim_id)}}"><button type="button" class="btn btn-green btn-sm text-white" fdprocessedid="wf07gv">
                    Student Details
                  </button></a>
                  @endif
                 
                   </td> 
                                        
                   
                                          
               
            </tr>
            <?php $i++;?>
            @endforeach
           
        </tbody>
      
    </table>

    
    <div class="modal modal-blur fade" id="choosepackage" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Choose Package</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <form id="choosepack" >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="modal-body">
          
                     
                      <div class="form-group">
                          <label for="packageDropdown">Select Package</label>
                          <select class="form-select selecttype" id="packageId">
                          
                          </select>
                      </div>
                   
                      <input type="hidden" id="collegeId" name="collegeId">
                      <input type="hidden" id="collegedep" name="collegedep">
                      <input type="hidden" id="collegepoc" name="collegepoc">
                     
             
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                  {{-- <button type="submit" id="add_stud" class="btn btn-primary">Submit</button> --}}
              </div>
          </div>
        </form>
      </div>
    </div>
    

@endsection
