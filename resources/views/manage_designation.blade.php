@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">  
                   
              <div class="card">
                <div class="card-body">

                    <div class="btn-list" style="float: right">
                     
                      <a href="#" class="btn btn-primary d-none d-sm-inline-block"  onclick="adddesignation()">
                        <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                        <svg xmlns="http://www.w3.org/2000/svg"   class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                        Create new 
                      </a>
                        <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal" data-bs-target="#modal-report" aria-label="Create new report">
                          <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                        </a>
                      </div>

                  <nav aria-label="breadcrumb">
                      <ol class="breadcrumbs">
                        <li><a href="{{url('/home')}}">Home</a></li>
                        <li><a href="#">{{$title}}</a></li>
                      </ol>
                  </nav>
                   
                    <h4 class="card-title">{{$title}}</h4>

                    <a href="{{url('/public/uploads/excel/designation.xlsx')}}" download="Designation">Download  Sample Excel Template </a>
                  <form action="{{ url('bulkupload_designation') }}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="file-upload-custom mt-3 mb-3">
                        <input style="    height: inherit;" class="form-control" type="file" name="file">
                        <button type="submit" class="btn btn-blue btn-sm text-white">Upload</button>
                        </div>
                   </form>
                  
                      
                       @if(Session::has('duplicates'))
                      <div class="alert alert-warning">
                        <p>Duplicate values:</p>
                         <ul>
                            @foreach(Session::get('duplicates') as $duplicate)
                               <li>{{ $duplicate }}</li>
                            @endforeach
                        </ul>
                      </div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                @endforeach
                           </ul>
                           </div>
                      @endif
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Designation</th>
                                <th>Status</th>
                                <th>Action</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                          <?php $i=1;?>
                                      @foreach($designationlist as $designationlist)
                                      <tr>
                                          <td>{{$i}}</td>
                                          <td>{{$designationlist->designation}}</td>


                                          <td>
                                            @if($designationlist->status==1)
                                            <span style="color:green;display:block;" id="act_{{$designationlist->id}}">Active</span>
                                            <span style="color:red;display:none;" id="deact_{{$designationlist->id}}" >Deactive</span>                       
                                            @else
                                            <span style="color:green;display:none;" id="act_{{$designationlist->id}}" >Active</span>
                                            <span style="color:red;display:block;" id="deact_{{$designationlist->id}}">Deactive</span>    
                                            
                                            @endif
                                            </td> 


                                          <td>
                                            <div class="d-flex">
                                            <button type="button" class="btn btn-cyan btn-sm text-white me-2" fdprocessedid="wf07gv" onclick="editDesignation('{{$designationlist->id}}','{{$designationlist->designation}}')">
                                              Edit
                                            </button>
                                                      @if($designationlist->status==1)
                                                    
                                                      <button type="button" style="display: block" id="deactb_{{$designationlist->id}}" class="btn btn-danger btn-sm text-white" fdprocessedid="pvth1" onclick="dDesignation('{{$designationlist->id}}')">
                                                       Deactivate
                                                      </button>

                                                      <button type="button"  style="display: none" id="actb_{{$designationlist->id}}" class="btn btn-success btn-sm text-white" fdprocessedid="pvth1" onclick="aDesignation('{{$designationlist->id}}')">
                                                        Activate
                                                       </button>
                                                     

                                                      @else
                                                     
                                                      <button type="button" style="display: block" id="actb_{{$designationlist->id}}" class="btn btn-success btn-sm text-white" fdprocessedid="pvth1" onclick="aDesignation('{{$designationlist->id}}')">
                                                        Activate
                                                       </button>

                                                       <button type="button" style="display: none" id="deactb_{{$designationlist->id}}" class="btn btn-danger btn-sm text-white" fdprocessedid="pvth1" onclick="dDesignation('{{$designationlist->id}}')">
                                                        Deactivate
                                                       </button>
                                                      </div>
                                                      @endif
                
                                          </td>
                                      </tr>
                                      <?php $i++;?>
                                      @endforeach
                                     
                                  </tbody>
                      
                    </table>

                    <form id="designationform" method="POST" enctype="multipart/form-data"  action="{{url('/save_designation')}}">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal modal-blur fade" id="modal-desi" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title">New Designation</h5>
                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                          <div class="mb-3">
                            <label class="form-label">Designation<sup>*</sup></label>
                            <input type="text" class="form-control" name="designation" id="designation" placeholder="Designation">
                          </div>
                        </div>
                      <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>                                            
                      </div>
                    
                    </div>
                    </div>
                    </form>



      </div>
    </div>         
</div>
<form id="designation" method="POST" enctype="multipart/form-data"  action="{{url('/edit_designation')}}">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="modal modal-blur fade" id="modal-edit-desig"  tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Edit Designation</h5>
      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body">
      <div class="mb-3">
        <label class="form-label">Designation<sup>*</sup></label>
        <input type="text" class="form-control" name="editdesigname" id="editdesigname" placeholder="Designation">
        <input type="hidden" class="form-control" name="editdesigid" id="editdesigid" placeholder="Designation">
      </div>
    </div>
  <div class="modal-footer">
    <button type="submit" class="btn btn-primary">Update</button>
  </div>                                            
  </div>

</div>
</div>

</form>



            <script>
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif
        </script>




@endsection
