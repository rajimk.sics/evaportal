@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">
     
              <div class="card">
                <div class="card-body">

                    <div class="btn-list" style="float: right">
    
                        <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal" data-bs-target="#modal-report" aria-label="Create new report">
                          <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                        </a>
                      </div>
                   
                    <h4 class="card-title">Revenue Report</h4>
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Payment</th>
                <th>Tax</th>
                <th>Date</th>
                <th>Total</th>
              </tr>
        </thead>
        <tbody>
            <?php $i=1;?>
            @foreach($monthlyreport as $monthlyreport)
            <tr>
                <td>{{$i}}</td>
                <td>{{ucfirst($monthlyreport->name)}}</td>
                <td> Rs {{number_format($monthlyreport->payment,2)}}</td>
                <td> Rs {{number_format($monthlyreport->tax,2)}}</td>
                <td>{{date('d-m-Y', strtotime($monthlyreport->date))}}</td>
                <td>Rs {{number_format($monthlyreport->total,2)}}</td>
                
               
            </tr>
            <?php $i++;?>
            @endforeach           
        </tbody>     
    </table>

                    </div>
                </div>         
            </div>
           

                       
@endsection
