@extends('layouts.main')

@section('content')
    <!-- Page body -->
    <div class="page-body">
        <div class="container-xl">

            <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">

            <div class="card">
                <div class="card-body">

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                            <li><a href="{{ url('/home') }}">Home</a></li>
                            <li><a href="{{ url('/studentAttendance') }}">Attendance</a></li>
                        </ol>
                    </nav>
                    <div class="d-flex justify-content-between w-100">
                        <h4 class="card-title mb-2">Attendance</h4>
                        <div class="mx-auto">
                            <h2 class="mb-2 text-center">
                                <b>{{ \Carbon\Carbon::parse($formattedMonthYear)->format('Y - F') }}</b>
                            </h2>
                        </div>
                    </div>


                    <form method="GET" id="attendanceFilter" action="{{ url('/studentAttendance') }}"
                        class="d-flex align-items-center">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <div class="me-3"><label class="form-label fw-bold mb-0">Filter:</label></div>
                        <div class="col-2 me-3">
                            <label for="year" class="form-label mb-0">Select Year<sup>*</sup></label>
                            <select class="form-select form-select-sm" name="AttendanceYear" id="AttendanceYear">
                                @foreach ($AttendanceYear as $year)
                                    <option value="{{ $year }}" {{ $year == $currentYear ? 'selected' : '' }}>
                                        {{ $year }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-2 me-3">
                            <label for="month" class="form-label mb-0">Select Month<sup>*</sup></label>
                            <select class="form-select form-select-sm" name="AttendanceMonth" id="AttendanceMonth">
                                @foreach ($AttendanceMonth as $monthValue => $monthName)
                                    <option value="{{ $monthValue }}"
                                        {{ $monthValue == $currentMonth ? 'selected' : '' }}>{{ $monthName }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-auto d-flex align-items-center">
                            <button type="submit" class="btn btn-primary me-3">Submit</button>
                            <a href="{{ url('/studentAttendance') }}" class="btn btn-secondary">Clear All</a>
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">

                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Student Name</th>
                                    <th>Action</th>
                                    @foreach ($daysInMonth as $day)
                                        <th>{{ $day }}
                                            ({{ \Carbon\Carbon::createFromDate($currentYear, $currentMonth, $day)->format('D') }})
                                        </th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach ($studAttendance as $studentId => $attendanceMap)
                                    @php
                                        $studentName = \App\Models\User::find($studentId)->name;
                                    @endphp
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{ ucfirst($studentName) }}</td>
                                        <td>
                                            <a href="{{ url('/studentFullAttendance/' . $studentId) }}" title="View Full Attendance">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                    stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                                    class="icon icon-tabler icons-tabler-outline icon-tabler-calendar-week">
                                                    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                    <path
                                                        d="M4 7a2 2 0 0 1 2 -2h12a2 2 0 0 1 2 2v12a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12z" />
                                                    <path d="M16 3v4" />
                                                    <path d="M8 3v4" />
                                                    <path d="M4 11h16" />
                                                    <path d="M7 14h.013" />
                                                    <path d="M10.01 14h.005" />
                                                    <path d="M13.01 14h.005" />
                                                    <path d="M16.015 14h.005" />
                                                    <path d="M13.015 17h.005" />
                                                    <path d="M7.01 17h.005" />
                                                    <path d="M10.01 17h.005" />
                                                </svg>
                                            </a>
                                        </td>

                                        @foreach ($daysInMonth as $day)
                                            @php
                                                $attendanceStatus = $attendanceMap[$day] ?? null;
                                            @endphp

                                            @if ($attendanceStatus === 1)
                                                <td class="text-center" style="color: green;">P</td>
                                            @elseif ($attendanceStatus === 0)
                                                <td class="text-center" style="color: red;">A</td>
                                            @else
                                                <td class="text-center">-</td>
                                            @endif
                                        @endforeach
                                    </tr>
                                    <?php $i++; ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection
