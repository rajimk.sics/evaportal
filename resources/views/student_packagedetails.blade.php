@extends('layouts.main')
@section('content')
   <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">   
              <div class="row row-cards">

              @if(Auth::user()->role==5)
              <nav aria-label="breadcrumb">
                  <ol class="breadcrumbs">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="{{url('/activated_packages/')}}">Activated Packages</a></li>

                    <li><a href="#"></a>Package Details of {{$pac_name}} </li>
                  </ol>
                </nav>
             @else
             <nav aria-label="breadcrumb">
              <ol class="breadcrumbs">
                <li><a href="{{url('/home')}}">Home</a></li>
                <li><a href="{{url('/students_list/'.$pac_id)}}">Student List of {{$pac_name}} </a></li>

                @if(Auth::user()->role==5)

                <li><a href="#"></a>Package Details of {{$name}} </li>
                @else

                <li><a href="#"></a>  {{ucfirst($name)}} - Package Details of {{ucfirst($pac_name)}} </li>

                @endif
              </ol>
            </nav>

             @endif
                
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-header">
                      <ul class="nav nav-tabs card-header-tabs" data-bs-toggle="tabs" role="tablist">
                        <li class="nav-item" role="presentation">
                          <a href="#tabs-home-8" class="nav-link active" data-bs-toggle="tab" aria-selected="true" role="tab">Package Details</a>
                        </li>
                        <li class="nav-item" role="presentation">
                          <a href="#tabs-profile-8" class="nav-link" data-bs-toggle="tab" aria-selected="false" tabindex="-1" role="tab">Fees Reduction</a>
                        </li>


                        @if($pac_type==1)

                          <li class="nav-item" role="presentation">
                            <a href="#tabs-activity-8" class="nav-link" data-bs-toggle="tab" aria-selected="false" tabindex="-1" role="tab">Fees Split Up</a>
                          </li>

                        @endif
                        
                      </ul>
                    </div>
                    <div class="card-body">
                      <div class="tab-content">
                        <div class="tab-pane fade active show" id="tabs-home-8" role="tabpanel">
                        
                          <div>
                              <p>Package Name : {{$packagedetails->pac_name}}</p>
                              <p>Duration : {{$packagedetails->duration}} month</p>
                              <p>Fees : Rs {{number_format($packagedetails->fee,2)}}</p>
                              <p>Tax 18% : Rs {{number_format($packagedetails->maintax,2)}}</p>
                              <p>Actual Fees :Rs {{number_format($packagedetails->total,2)}}</p>

                          </div>
                        </div>
                        <div class="tab-pane fade" id="tabs-profile-8" role="tabpanel">
                         
                          <div>

                            @if($packagedetails->reduction_check==1)

                            @else

                            <p style="color:red">Not Eligible for Reduction</p>

                            @endif
                            @if($packagedetails->reduction_check==1)

                              <h4>Referal Code:  {{$packagedetails->referalcode}}</h4>

                              <p>Reduction Amount : Rs {{number_format($packagedetails->reduction_amount,2)}}</p>
                              <p>Reduced Fees :Rs {{number_format($packagedetails->reduced_fees,2)}}</p>
                              <p>Tax :Rs {{number_format($packagedetails->tax,2)}}</p>
                              <p>Total Fees After Reduction : Rs {{number_format($packagedetails->totalfees_afterreduction,2)}}</p>
                              <p>In-Effect Total Reduction Offered : Rs {{number_format($packagedetails->ineffect_total_red,2)}}</p>

                              @endif
                          </div>
                        </div>

                        @if($pac_type==1)

                        <div class="tab-pane fade" id="tabs-activity-8" role="tabpanel">
                         
                          <div>

                            <table class="table" id="splitup">
                              <thead>
                                <tr>
                                  <th scope="col">Installment No</th>
                                  <th scope="col">Fees</th>
                                  <th scope="col">Paid Fees</th>
                                  <th scope="col">Pending Fees</th>
                                  <th scope="col">Due Date</th>
                                </tr>
                              </thead>
                              <tbody>
<?php $i=1;?>
                                @foreach($feessplitup  as $splitup)
                                <tr>
                                  <td>{{$i}}
                                  <td>

                                    Rs {{number_format($splitup->fees,2)}}
                                   </td>
                                 
                                  <td>
                                   
                                    Rs {{number_format($splitup->paidfees,2)}}
                                   
                                  </td>
                                  <td>

                                    @if($i==count($feessplitup))


                                   
                                    
                                        @if($pending_amount==0)

                                          Rs {{number_format(0,2)}}

                                        @else
                                        Rs {{number_format($splitup->pendingfees,2)}}
                                        @endif
                                   
                                    @else

                                    Rs {{number_format($splitup->pendingfees,2)}}
                                    @endif
                                   
                               
                                  </td>
                                  <td>{{$splitup->date}}</td>

                                </tr>
                                <?php $i++ ?>
                                @endforeach
                                
                              </tbody>
                            </table>
                          </div>
                        </div>
                        @endif
                       
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
           
            </div>

            
@endsection
