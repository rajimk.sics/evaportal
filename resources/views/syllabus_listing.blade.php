@extends('layouts.main')

@section('content')
    <div class="page-body">
        <div class="container-xl">
            <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::has('duplicates'))
        <div class="alert alert-warning">
            <p><strong>Duplicate values found:</strong></p>
            <ul>
                @foreach (Session::get('duplicates') as $duplicate)
                    <li>
                        <strong>Duplicate Value:</strong> {{ $duplicate['dupli'] }}
                        <strong>Technology:</strong> {{ $duplicate['technology'] }}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif


            <div class="card">
                <div class="card-body">
                    <nav aria-label="breadcrumb mb-3">
                        <ol class="breadcrumbs">
                            <li><a href="{{ url('/home') }}">Home - Mentor</a></li>
                            <li><a href="{{ url('/packagelist_trainer') }}">{{ $pac_name }}</a></li>
                            <li><a href="{{ url('/viewPacSyllabus/'.$pac_id) }}">{{ $pac_name }} - Syllabus</a></li>


                        </ol>
                    </nav>
                   
                
                    <div class="card">

                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h5 class="card-title mb-0">Topics</h5>
                            <a href="{{ url('/viewTechnologySyllabus/'.$pac_id) }}" title="Assign more topics" class="text-decoration-none text-blue">Assign more...</a>
                        </div>
                        
                    
                        <div class="card-body mt-1">
                            <div class="row">
                                <div class="col-10">
                                    <div class="card" style="width: 75vw;">

                                        <ul class="list-group list-group-flush">
                                            <div class="accordion border-0" id="accordionExample">
                                                @if (isset($topics) && $topics->isNotEmpty())
                                                    @foreach ($topics as $index => $topic)
                                                        <div class="accordion-item" data-topicid="{{ $topic->id }}">
                                                            <h2 class="accordion-header" id="heading{{ $index }}">
                                                                <div class="accordion-button d-flex align-items-center justify-content-between rounded-3 shadow-sm"
                                                                     type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{ $index }}"
                                                                     aria-expanded="{{ $index === 0 ? 'true' : 'false' }}" aria-controls="collapse{{ $index }}">
                                                                    <div class="d-flex align-items-center flex-grow-1">
                                                                        <span class="fw-bold text-dark me-2">
                                                                            Topic {{ $index + 1 }}: {{ ucfirst($topic->topics) }}
                                                                        </span>
                                                                        <small class="text-muted">Package: {{ $pac_name }}</small>
                                                                    </div>
                                                                    <button type="button" class="btn btn-danger btn-sm rounded"
                                                                            onclick="removeTopic('{{ $topic->id }}', '{{ $pac_id }}')">
                                                                        Remove
                                                                    </button>
                                                                </div>
                                                            </h2>
                                                            <div id="collapse{{ $index }}" 
                                                                 class="accordion-collapse collapse {{ $index === 0 ? 'show' : '' }}" 
                                                                 data-bs-parent="#accordionExample">
                                                                <div class="accordion-body">
                                                                    @if ($topic->subtopics->isNotEmpty())
                                                                        <ul class="list-unstyled mt-3">
                                                                            @foreach ($topic->subtopics as $subIndex => $subtopic)
                                                                                <li class="mb-2">
                                                                                    <div class="card shadow-sm rounded-3">
                                                                                        <div class="card-body">
                                                                                            <h5 class="card-title text-dark">
                                                                                                Subtopic {{ $subIndex + 1 }}:
                                                                                            </h5>
                                                                                            <p class="card-text">{{ ucfirst($subtopic->sub_topics) }}</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </li>
                                                                            @endforeach
                                                                        </ul>
                                                                    @else
                                                                        <p class="text-muted">No subtopics available for this topic.</p>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @else
                                                    <p class="text-center text-muted">No syllabus available</p>
                                                @endif
                                            </div>
                                        </ul>
                                                                                                                                                                                                                                                                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
