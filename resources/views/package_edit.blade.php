@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
                   
              <div class="card rounded">
                <div class="card-body">
              
                 

                  <nav aria-label="breadcrumb">
                    <ol class="breadcrumbs">
                      <li><a href="{{url('/packagelist_trainer')}}">Packages</a></li>
                      <li><a href="#">Edit</a></li>
                    </ol>
                  </nav>

                   
                    <div class="table-responsive mt-3">
                      <table class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr >
               
                <th>Package Details</th>
                <th>Course Id</th>
                 <th>Duration</th>
              
               
              
               
            </tr>
        </thead>
        <tbody>
          <?php $i=1;?>
          <tr>       
          <td>{{ucfirst($singlelist->pac_name)}}</td>
          <td>{{$singlelist->course_id}}</td>
          <td>{{$singlelist->duration}} {{ $singlelist->duration == 1 ? 'month' : 'months' }}</td>
          </tr>
         </tbody>
      
    </table>
</div>
</div>
     
</div>
<div class="mt-5 rounded">
    <form id="editprereq" name="editForm"   method="POST" enctype="multipart/form-data" action="{{ url( '/packageUpdate' ) }}">
        
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="pac_id" value="{{$singlelist->pac_id}}">



    <div class="card">
        <div class="card-header">
            <b>Add Prerequisites</b>
        </div>
        <div class="card-body">
            <div class="row">
            <div class="col-3">
            <h5 class="card-title">Prerequisites</h5>
        </div>
           
        <div id="pre-requisites" class="col-md-9">
                <button style="float: right" type="button" class="add-pre-req btn btn-primary">Add Prerequisites</button>
                <div class="pre-requisite-group">

                @foreach ($prerequisite as $item)
                    <div class="mb-2">
                        <input type="text" class="form-control pre_req_input" name="pre_req[]" id="pre_req1" value="{{ $item->pre_req }}" placeholder="Pre-requisite" required>
                        <button type="button" class="btn btn-danger remove-requisite">Remove</button>
                        
                    </div>
                @endforeach
            </div>
        </div>
                          
    
        <div class="d-flex justify-content-end align-items-end mt-2" style="height: 100%;">
            <button type="submit" class="save-changes btn btn-primary">Save Changes</button>
        </div>
        </div>
    </div>
    </div>
    </form>
</div>
   
            </div>
            </div>
            <script>
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif
        </script>
@endsection
