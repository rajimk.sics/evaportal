<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Login Information</title>
   </head>
   <body style="font-family: Arial, sans-serif;background-color: #f4f4f4;margin: 0;padding: 0;">
      <div class="container" style="display: flex;height: 100vh;">
         <div class="login-box" style="max-width: 600px;min-width: 400px;margin: auto;padding: 20px;background-color: #fff;border-radius: 5px;box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);display: flex;flex-direction: column;align-items: center;">
            <img src="https://eva.sicsapp.com/public/img/logo.png" alt="img" style="width: 180px;">
            <h1 style="color: #333;">Login Information</h1>
            <p style="margin-bottom: 20px;line-height: 1.6;">Hello <span class="name">{{$data['name']}}</span>,</p>
            <p style="margin-bottom: 20px;line-height: 1.6;">Your login credentials for Evaportal are as follows:</p>
            <ul style="list-style: none;margin-left: 0;padding-left: 0;width: 80%;background: #206bc424;padding: 20px;margin-top: 0px;border-radius: 10px;">
               <li style="display: flex;align-items: center;margin-bottom: 10px;">
                  <div class="thumb-icon" style="width: 40px;height: 40px;background: #206bc4;display: flex;align-items: center;justify-content: center;border-radius: 5px;margin-right: 10px;"><img src="https://eva.sicsapp.com/public/img/person-circle.png" style="width: 25px;filter: brightness(0) invert(1);"></div>
                  <span style="font-size: 18px;">{{ $data['email']}} <span class="labl" style="font-size: 15px;color: #8d8d8d;font-weight: 200 !important;">(Username)</span></span>
               </li>
               <li style="display: flex;align-items: center;margin-bottom: 0px;">
                  <div class="thumb-icon" style="width: 40px;height: 40px;background: #206bc4;display: flex;align-items: center;justify-content: center;border-radius: 5px;margin-right: 10px;"><img src="https://eva.sicsapp.com/public/img/lock-fill.png" style="width: 25px;filter: brightness(0) invert(1);"></div>
                  <span style="font-size: 18px;">{{ $data['password']}}<span class="labl" style="font-size: 15px;color: #8d8d8d;font-weight: 200 !important;">(Password)</span></span>
               </li>
               <!--            <li><strong>Password:</strong><span>rahuyl6869.a </span></li>-->
            </ul>
            <a class="button" href="https://eva.sicsapp.com/" style="display: inline-block;padding: 15px 40px;background-color: #206bc4;color: #fff;text-decoration: none;border-radius: 5px;margin-top: 20px;font-size: 18px;">Login Now</a>
            <p class="footer" style="margin-bottom: 20px;line-height: 1.6;margin-top: 20px;font-size: 14px;color: #666;">Thanks,<br>Evaportal</p>
         </div>
      </div>
   </body>
</html>