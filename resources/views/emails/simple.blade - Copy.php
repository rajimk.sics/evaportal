
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Information</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
        }

        .login-box {
            max-width: 600px;
            min-width: 400px;
            margin: auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        h1 {
            color: #333;
        }

        p {
            margin-bottom: 20px;
            line-height: 1.6;
        }

        .button {
    display: inline-block;
    padding: 15px 40px;
    background-color: #206bc4;
    color: #fff;
    text-decoration: none;
    border-radius: 5px;
    margin-top: 20px;
    font-size: 18px;
}

        .button:hover {
            background-color: #0056b3;
        }

        .footer {
            margin-top: 20px;
            font-size: 14px;
            color: #666;
        }
        .container {
            display: flex;
            height: 100vh;
        }
        .login-box {
            display: flex;
            flex-direction: column;
            align-items: center;
        }
        .login-box img {
    width: 180px;
}
    .login-box ul li img {
    width: 25px;
        filter: brightness(0) invert(1);
       
}
.login-box ul {
    list-style: none;
    margin-left: 0;
    padding-left: 0;
    width: 80%;
    background: #206bc424;
    padding: 20px;
    margin-top: 0px;
    border-radius: 10px;
}   
    .thumb-icon {
    width: 40px;
    height: 40px;
    background: #206bc4;
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 5px;
         margin-right: 10px;
}    
 .login-box ul li {
    display: flex;
    align-items: center;
     margin-bottom: 10px;
}       
 .login-box ul li:last-child {
    margin-bottom: 0px;
}       
 span.labl {
    font-weight: 200 !important;
    font-size: 15px;
    color: #8d8d8d;
}       
    
    </style>
</head>
<body>
    <div class="container">
        <div class="login-box">

          
            <img src="{{url('public/img/logo.png')}}" alt="img">
           
        <p>Hello <span class="name" >Rahul</span>,</p>
        <p>Your Payment is Sucess Please Login and Download Your Receipt:</p>

       

        <a class="button" href="https://eva.sicsapp.com/">Login Now</a>
       
        <p class="footer">Thanks,<br>Eva Portal</p>
        </div>
    </div>
</body>
</html>