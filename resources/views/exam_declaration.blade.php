@extends('layouts.main')
@section('content')
<div class="page-body">
    <div class="container-xl">


        <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">

        <div class="card">
            <div class="card-body">

        <nav aria-label="breadcrumb" class="mt-3">
            <ol class="breadcrumbs">
                <li><a href="{{ url('/home') }}">Home</a></li>
                <li><a href="{{ url('/examDeclaration') }}">Exams - Declaration</a></li>
            </ol>
        </nav>
       
        <form id="examdeclaration" name="examdeclaration"   method="POST" enctype="multipart/form-data" action="{{ url( '/examDeclarationSubmit' ) }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

        
                <h3 class="mt-2">Exam Declaration</h3>
            <div class="card-body  rounded">
                <div class="row g-3">
                    <div class="col-md-6">
                        <div class="row align-items-center">
                            <label for="examtechnology" class="form-label col-sm-4 col-form-label">Exam Type<sup>*</sup></label>
                            <div class="col-sm-8">
                                <select class="form-select rounded examtechnology" id="examtechnology" name="examtechnology">
                                    <option value="">Select Exam Type</option>
                                    <option value="Talento">Talento</option>
                                    <option value="Selection Test">Selection Test</option>
                                    <option value="Internship">Internship</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row align-items-center">
                            <label for="examname" class="form-label col-sm-4 col-form-label">Exam Name<sup>*</sup></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control rounded" name="examname" id="examname" placeholder="Exam Name">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row g-3 mt-3">
                    <div class="col-md-6">
                        <div class="row align-items-center">
                            <label for="techtrainerselect" class="form-label col-sm-4 col-form-label">Technology<sup>*</sup></label>
                            <div class="col-sm-8">
                                <select class="form-select rounded selecttech" id="techtrainerselect" name="techtrainerselect[]" multiple="multiple">
                                    <option value="" disabled>Select Technology</option>
                                    @foreach($technology as $item)
                                        <option value="{{ $item->id }}">{{ $item->technology }}</option>
                                    @endforeach 
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row align-items-center">
                            <label for="trainerpackages" class="form-label col-sm-4 col-form-label">Packages<sup>*</sup></label>
                            <div class="col-sm-8">
                                <select class="form-select rounded selecttech" id="trainerpackages" name="packages[]" multiple="multiple">
                                    <option value="" disabled>Select Package</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row g-3 mt-3">



                    <div class="col-md-6">
                        <div id="checkboxField" style="display: none;">
                            <label class="form-label">Percentage<sup>*</sup></label>
                            <div class="d-flex flex-wrap align-items-center">
                                <div class="form-check">
                                    <input type="radio"  id="checkbox1" name="percentages" value="25">
                                    <label  for="checkbox1">25%</label>
                                </div>
                                <div class="form-check">
                                    <input type="radio"  id="checkbox2" name="percentages" value="50">
                                    <label  for="checkbox2">50%</label>
                                </div>
                                <div class="form-check">
                                    <input type="radio"  id="checkbox3" name="percentages" value="75">
                                    <label  for="checkbox3">75%</label>
                                </div>
                                <div class="form-check">
                                    <input type="radio"  id="checkbox4"name="percentages" value="100">
                                    <label  for="checkbox4">100%</label>
                                </div>
                            </div>
                        </div>
                    </div>




                    <div class="col-md-6 d-flex align-items-center justify-content-end">
                        <button  type="submit" class="btn btn-primary rounded">Declare</button>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>

     

@if (Session::has('error'))
<script>
    document.addEventListener('DOMContentLoaded', function () {
        swal({
            title: "Exam already exists!",
            text: {!! json_encode(Session::get('error')) !!},
                        type: "error",
            button: "OK",
        });
    });
</script>
@endif

    </div>
</div>

<script>
      



</script>
@endsection
