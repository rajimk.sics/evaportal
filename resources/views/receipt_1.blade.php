

  
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Srishti Innovative Education Services Pvt Ltd</title>
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  </head>
  
  <body style="margin:0;padding:0;font-family: system-ui;">
   
 
  
    <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
      <tr>
        <td align="center" style="padding:20px;">
          <table role="presentation" style="width:950px;border-collapse:collapse;border:1px solid #cccccc;border-spacing:0;text-align:left; margin: : 20px">
            <tr>
              <td align="center" style="padding:20px;background:#fff; border-bottom: 31px solid #ccc;">
                <div>
                    <div style="display: flex;     justify-content: space-between; align-items: center;" >
                        <img  src="https://srishticampus.com/images/sicsfullogo.png">
                        <div class="">
  <!--                          <h1 style=" margin: 0; padding:0; text-align:right;   color: #ccc;    margin-right: 17px;" >INVOICE</h1>-->
  <!--                          <span style="    color: #104eb1;    font-weight: 500;margin-right: 0px 17px !important;     padding:0; text-align:right;" ><i style="color: #104eb1;    margin: 5px; font-size: 19px;" class="fas fa-mobile-alt"></i>9846011044</span>-->
  <!--                          <p style="    margin: 0; padding:0; text-align:right;" >1C 1st floor Carnival building Technopark Trivandrum</p>-->
                        </div>
                    </div>
                </div>
              </td>
            </tr>           
            <tr>
              <td style="    padding: 20px; display: flex;">
                <table style="width: 100%;padding-right: 30px;" >
                    <tr style="    height: 40px;" >
                        <td style="font-weight:bold;     color: #424242;" >Recepit.No</td>
                        <td style="text-align: right; padding-right:20px;" >SICS000 </td>
                    </tr>
                    <tr style="    height: 40px;" >
                        <td style="font-weight:bold;     color: #424242;" >Date</td>
                        <td style="text-align: right; padding-right:20px;" > </td>
                    </tr>
                    <tr style="    height: 40px;" >
                        <td style="font-weight:bold;     color: #424242;" >Name of Student</td>
                        <td style="text-align: right; padding-right:20px;" ></td>
                    </tr>
                    <tr style="    height: 40px;" >
                        <td style="font-weight:bold;     color: #424242;" >Phone</td>
                        <td style="text-align: right; padding-right:20px;" ></td>
                    </tr>                  
                  </table>
                  <table style="width: 100%;" >
  
                     <tr style="    height: 40px;" >
                        <td style="font-weight:bold;     color: #424242;" >Payment Mode</td>
                        <td style="text-align: right; padding-right:20px;" ></td>
                    </tr>
                   <!--  <tr style="    height: 40px;" >
                        <td style="font-weight:bold; color: #424242;" >Payment Gate Way Transaction Reference no</td>
                        <td style="text-align: right; padding-right:20px;" ></td>
                    </tr> -->
                    <tr style="height: 40px;" >
                        
                        <td style="font-weight:bold; color: #424242;" >Transaction Amount</td>
                        <td style="text-align: right; padding-right:20px;" ></td>
                    </tr>
                    <tr style="height: 40px;" >
                        <td style="font-weight:bold; color: #424242;" >Status of Transaction</td>
                        <td style="text-align: right; padding-right:20px;" ></td>
                    </tr>
                    <tr style="height: 40px;" >
                        <td style="font-weight:bold; color: #424242;" >Purpose of Payment</td>
                        <td style="text-align: right; padding-right:20px;" >Payment for </td>
                    </tr>
                    <!--   <tr style="    height: 40px;" >
                        <td style="font-weight:bold;color: #424242; " >Status description</td>
                        <td style="text-align: right; padding-right:20px;" > </td>
                    </tr> --> 
                  </table>
                  
              </td>
                
            </tr>
            <tr>
              <td style="padding:10px 30px;">
                <hr>
              </td>
            </tr>
              <tr>
              <td style="padding:10px 30px;">
                <table style="width: 100%;">
                  <thead>
              <tr style="background: #464343;color: #fff;border-bottom: 0px !important; height: 40px;">
                <th>Item No</th>
                <th>Description</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody>
              <tr style="height: 50px;" >
          <!--      <th scope="row">1</th>-->
                <td>1</td>
                <td>Payment for Cource</td>
                
                <td style="padding-left: 10px; text-align: center;" >sd</td>
              </tr>
              <tr style="height: 50px;" >
          <!--      <th scope="row">1</th>-->
                <td></td>
                <td>CGST@9%</td>
                
                <td></td>
              </tr>
              <tr style="height: 50px;" >
          <!--      <th scope="row">1</th>-->
                <td style="padding-left: 10px;" ></td>
                <td style="padding-left: 10px;" >GST@9%</td>
                
                <td style="padding-left: 10px; text-align: center;" ></td>
              </tr>
              <tr style="height: 50px;">
          <!--      <th scope="row">2</th>-->
                <td></td>
                <td style="padding-left: 10px;" ><b>TOTAL</b></td>
                <td style="padding-left: 10px; background: #ccc; text-align: center; color: #104eb1;" ><b></b></td>
              </tr>
            </tbody>
          </table>
              </td>
            </tr>
              <tr>
              <td style="height: 200px;">
                  <div style="display: flex; justify-content: flex-end;padding: 30px;    align-items: center;margin-top: 55px;" >
                      
                      <div style="display: flex;flex-direction: column;    align-items: center;" >
                          <img  style="    width: 160px; margin-right: 20px;" src="https://srishticampus.com/images/seal.png" >
                          <h5 style="font-size: 22px;font-weight: 500;margin: 3px 8px;" >Authorized Signatory</h5>
                      </div>
                  </div>
              </td>
            </tr>
            <tr>
              <td style="padding:15px;background:#104eb1; color: #fff; justify-content: space-between;display: flex;">
                  <span>Srishti Innovative Education Services Pvt Ltd</span>
  <!--                <span>1C 1st floor Carnival building Technopark Trivandrum </span>-->
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <div class="col-12">
      <button onClick="window.print();return false;" style="float: right;margin-right: 104px;margin-top: 25px;margin-bottom: 20px;">Print</button>                
    </div>
  </body>
  </html>
  