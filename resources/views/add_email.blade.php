@extends('layouts.main')

@section('content')
<!-- Page body -->
<div class="page-body">
    <div class="container-xl">
        <form id="interviewemail" method="POST" enctype="multipart/form-data" action="{{ url('/save_emails') }}">
            @csrf <!-- Laravel's CSRF token directive -->

            <div class="card">
                <div class="card-body">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                            <li><a href="{{ url('/home') }}">Home</a></li>
                            <li><a href="#">{{ $title }}</a></li>
                        </ol>
                    </nav>
                    <h4 class="card-title">{{ $title }}</h4>

                    <div class="row">
                        <div class="col-md-6 col-xl-6">
                            <div class="form-group">
                                <button type="button" id="addMail" class="btn btn-success">Add More +</button>
                            </div>
                       
                    <div id="email-container">
                      @foreach ($emailist as $list)
                      <div class="email-group">
                        <div class="mb-3">
                          <input type="email" class="form-control pre_req_input" name="emails[]"  id="emails1" placeholder="Enter email" value="{{$list->email}}">
                          <button type="button" class="btn btn-danger remove-email">Remove</button>
                      </div>
                    </div>
                      @endforeach
                  </div>
                </div>
            </div>
                    <div class="row">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary ms-auto">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    
        // Display success message if it exists
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif

</script>

@endsection
