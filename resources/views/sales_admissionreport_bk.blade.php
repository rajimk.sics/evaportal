@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">
     
              <div class="card">
                <div class="card-body">

                    <div class="btn-list" style="float: right">
    
                        <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal" data-bs-target="#modal-report" aria-label="Create new report">
                          <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                        </a>
                      </div>

                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>
                          @if($empstatus==1)
                  <li><a href="{{url('/employee_sales_report/'.$empid)}}">{{$subtitle}}</a></li>
                  @endif
                          <li><a href="#">{{$title}}</a></li>
                        </ol>
                      </nav>
                   
                    <h4 class="card-title">{{$title}}</h4>

                    @if($empstatus==0)        
                    <form id="searchmarketing" method="GET" enctype="multipart/form-data"  action="{{url('/sales_admission')}}">     
                    <input type="hidden" name="_token" id="token"  value="{{ csrf_token() }}">
                          <div class="row">
                            <div class="col">
                              <label>Select Marketing Person<sup>*</sup></label>
                                 <select class="form-select packname" id="salesname" name="salesname" >
                                    <option value="">Select Name</option>
                                    @foreach($sales as $sales)
                                        <option value="{{$sales->id}}">{{$sales->name}}</option>
                                      @endforeach
                                        </select>
                                      </div>
                                      <div class="col">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                        <div class="col"> 
                                         <a href="{{url('/sales_admission')}}" class="btn btn-primary"> View All</a>
                                          </div>
                                        </div>
                               </form>
                      @endif      


                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th> Student Name</th>
                <th> Closed By</th>                
                @if($pac_type==1)
                <th>SICS Reg Number</th>
                @endif
                <th>Package Amount </th>
                <th>Tax</th>
                <th>Total</th>
              
              </tr>
        </thead>
        <tbody>
            <?php $i=1;?>
            @foreach($monthlyreport as $monthlyreport)
            <tr>
                <td>{{$i}}</td>
                <td>{{ucfirst($monthlyreport->name)}}</td>
                <td>{{$monthlyreport->sales_id}}</td>
                
                @if($pac_type==1)
                <td>{{'SIAC'.$monthlyreport->student_admissionid}}</td>
                @endif
                <td> 

                    Rs  {{number_format(round($monthlyreport->package_amount,2))}}
                  
                 
                 </td>
                <td>

                  Rs {{number_format(round(round($monthlyreport->package_tax,2)))}}
             
                </td>
               
                <td>Rs {{number_format($monthlyreport->package_fullamount	,2)}}</td>
              
               
            </tr>
            <?php $i++;?>
            @endforeach           
        </tbody>     
    </table>

                    </div>
                </div>         
            </div>
           

                       
@endsection
