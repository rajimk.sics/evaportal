@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">
                   
              <div class="card">
                <div class="card-body">

                   
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>
                          
                          <li><a href="{{url('/claimed_collegesmaster')}}">Claimed Colleges</a></li>
                          <li><a href="#">{{$title}}</a></li>
                        </ol>
                      </nav>
                    <h4 class="card-title">{{$title}}</h4>


                  


                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Closing owner name</th>
                <th>Year</th>
                <th>Closed packages</th>
                <th>Total Amount with GST</th>
                
                 <th>Action</th>
               
                
             
            </tr>
        </thead>
        <tbody>
<?php $i=1;?>
            @foreach($claimdetails as $claimdetails)
            <tr id="row{{$claimdetails->claim_id}}">
                <td>{{$i}}</td>
              
	             @php
               $salesName=App\Helpers\CustomHelper::uname($claimdetails->sales_id); 
               $year = $claimdetails->joiningyear;
               $currentYear = date("Y");
                @endphp
               <td>{{$salesName['name']}}</td>
               <td>{{$claimdetails->joiningyear}}</td>

              <td>{{$claimdetails->package_names}}</td>
            
               <td>{{number_format($claimdetails->total_package_fullamount,2)}}</td>
               
              <td> 
               
                                    <button type="button" class="btn btn-blue btn-sm text-white" fdprocessedid="wf07gv" onclick="fetchStudents('{{ $claimdetails->college_id }}','{{ $claimdetails->joiningyear}}','{{ $claimdetails->sales_id}}')">
                                      View Student Details
                                  </button>
              </td>
                 
                                        
                   
                                          
               
            </tr>
            <?php $i++;?>
            @endforeach
           
        </tbody>
      
    </table>

                    </div>
                </div>
              </div>
            </div>
        </div>
        
  
        <div id="studentsModal" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog modal-xl modal-dialog-centered modal-fullscreen-md-down" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="studentsModalLabel">Claim Details</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body d-flex flex-column">
                <h4 id="tableHeading" class="text-center mb-3"></h4>
                <div class="table-responsive overflow-auto">
                  <table id="studentDetailsTable" class="table table-bordered table-striped mb-0">
                    <thead class="thead-dark">
                      <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Package Name</th>
                        <th>Technology</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Joining Date</th>
                        <th>Total Fee</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        
@endsection
