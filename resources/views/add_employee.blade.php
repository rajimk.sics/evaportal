@extends('layouts.main')
@section('content')
   <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">  
              <div class="row row-cards">
                <div class="col-12">
                  <form id="addemployee" method="POST" enctype="multipart/form-data"  action="{{url('/save_employee')}}">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <nav aria-label="breadcrumb">
                      <ol class="breadcrumbs">
                        <li><a href="{{url('/home')}}">Home</a></li>
                        <li><a href="#">{{$title}}</a></li>
                      </ol>
                    </nav>
                    <div class="card-header">
                     
                      <h4 class="card-title"></h4>{{$title}}
                    </div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                   @endif
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-6 col-xl-12">
                          <div class="row">
                            <div class="col-md-6 col-xl-6">

                          @if(Auth::user()->role==1)

                              <div class="mb-3">
                                <label class="form-label">Employee Type<sup>*</sup></label>
                                <select class="form-select selecttype" id="emptype" name="emptype" >
                                  <option value="">Select Employee Type</option>
                                  <option value="2">Marketing</option>
                                  <option value="3">Trainers</option>
                                  <option value="4">Process Associates</option>
                                  <option value="6">Management</option>
                                  <option value="7">Finance</option>
                                  <option value="8">Sub Admin</option>
                                  <option value="9">Placement</option>
                                  <option value="10">Recruitment</option>
                                  <option value="12">Master</option>
                                </select>
                              </div>
                              @endif

                             
                              @if(Auth::user()->role==2)
                              <div class="mb-3">
                                <label class="form-label">Employee Type<sup>*</sup></label>
                                <select class="form-select selecttype" id="emptype" name="emptype" >
                                 
                                  <option value="2">Marketing</option>
                                 
                                </select>
                              </div>
                              @endif
                              @if(Auth::user()->role==3)
                              <div class="mb-3">
                                <label class="form-label">Employee Type<sup>*</sup></label>
                                <select class="form-select selecttype" id="emptype" name="emptype" >
                                 
                                  <option value="3">Trainers</option>
                                 
                                </select>
                              </div>

                              @endif
                              @if(Auth::user()->role==4)
                              <div class="mb-3">
                                <label class="form-label">Employee Type<sup>*</sup></label>
                                <select class="form-select selecttype" id="emptype" name="emptype" >
                                 
                                  <option value="4">Process Associates</option>
                                 
                                </select>
                              </div>

                              @endif

                              @if(Auth::user()->role==6)
                              <div class="mb-3">
                                <label class="form-label">Employee Type<sup>*</sup></label>
                                <select class="form-select selecttype" id="emptype" name="emptype" >
                                 
                                  <option value="6">Management</option>
                                 
                                </select>
                              </div>

                              @endif
                              @if(Auth::user()->role==7)
                              <div class="mb-3">
                                <label class="form-label">Employee Type<sup>*</sup></label>
                                <select class="form-select selecttype" id="emptype" name="emptype" >
                                 
                                  <option value="7">Finance</option>
                                 
                                </select>
                              </div>

                              @endif
                              @if(Auth::user()->role==8)
                              <div class="mb-3">
                                <label class="form-label">Employee Type<sup>*</sup></label>
                                <select class="form-select selecttype" id="emptype" name="emptype" >
                                 
                                  <option value="8">Sub Admin</option>
                                 
                                </select>
                              </div>

                              @endif
                              @if(Auth::user()->role==9)
                              <div class="mb-3">
                                <label class="form-label">Employee Type<sup>*</sup></label>
                                <select class="form-select selecttype" id="emptype" name="emptype" >
                                 
                                  <option value="9">Placement</option>
                                 
                                </select>
                              </div>

                              @endif

                              <div class="mb-3">
                                <label class="form-label">Employee Name<sup>*</sup></label>
                                <input type="text" class="form-control" name="empname" id="empname" placeholder="Employee Name">
                              </div>
                            
                             
                              <div class="mb-3">
                                <label class="form-label">Designation<sup>*</sup></label>
                                <select class="form-select selectdesi" name="designation"  id="designation" placeholder="Designation">
                                  <option value="">Select Designation</option>
                                  @foreach($designationlist as $designationlist )
                                  <option value="{{$designationlist->id}}">{{$designationlist->designation}}</option>
                                  @endforeach
                                 
                                </select>
                              </div>

                          
                              <div class="mb-3">
                                <label class="form-label">Experience<sup>*</sup></label>
                                <input type="text" class="form-control" name="experience" id="experience" placeholder="Experience">
                              </div>
                              <div class="mb-3">
                                <label class="form-label">Phone<sup>*</sup></label>
                                <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" minlength="10" maxlength="13">
                              </div>

                              <div class="mb-3">
                                <label class="form-label">Privileges</label>
                                <input type="checkbox" id="cinterview" name="cinterview" value="1">Schedule Interview
                                <input type="checkbox" id="cdesignation" name="cdesignation" value="1">Designation
                                <input type="checkbox" id="ctechnology" name="ctechnology" value="1">Technology
                                <input type="checkbox" id="csource" name="csource" value="1">Source
                                <input type="checkbox" id="ccollege" name="ccollege" value="1">College
                                <input type="checkbox" id="cqualification" name="cqualification" value="1">Qualification<br>
                                <input type="checkbox" id="cspecialization" name="cspecialization" value="1">Specialization 
                                <input type="checkbox" id="cadd_employee" name="cadd_employee" value="1">Add Employee
                                <input type="checkbox" id="cadd_package" name="cadd_package" value="1">Add Package
                                <input type="checkbox" id="cadd_event" name="cadd_event" value="1">Add Event 
                           </div>
                           


                             

                         
                         
                          </div>

                          <div class="col-md-6 col-xl-6">
                            <div class="mb-3">
                              <label class="form-label">Employee Id<sup>*</sup></label>
                              <input type="text" class="form-control" name="empid" id="empid" placeholder="Employee Id" >
                            </div>
                            <div class="mb-3">
                              <label class="form-label">Official Email id<sup>*</sup></label>
                              <input type="text" class="form-control" name="office_email" id="office_email" placeholder="Official Email id">
                            </div>

                            <div class="mb-3" id="techdiv" style="display:none">
                              <label class="form-label">Technology<sup>*</sup></label>
                              <select class="form-select selecttech" id="technology" name="technology[]" multiple="multiple">
                                <option value="">Select Technology</option>
                                @foreach($technologylist as $technologylist )
                                <option value="{{$technologylist->id}}">{{$technologylist->technology}}</option>
                                @endforeach
                              </select>
                            </div>
                        
                            <div class="mb-3">
                              <label class="form-label">Employee Email<sup>*</sup></label>
                              <input type="text" class="form-control" name="email" id="email" placeholder="Employee Email">
                            </div>
                            <div class="mb-3">
                              <label class="form-label">Password<sup>*</sup></label>
                              <input type="text" class="form-control" name="password" id="password" placeholder="Password">
                            </div>

                            <div class="mb-3">
                              <label class="form-label">Reported to</label>
                              <select class="form-select selectdesi" name="reported_to[]"  id="reported_to" multiple="multiple">
                                @if(Auth::user()->role != 1)
                                @foreach($username as $username )
                                <option value="{{$username->id}}">{{$username->name}}</option>
                                @endforeach
                                @endif
                               
                              </select>
                            </div>

                           
                         
                          </div>

                        </div>
                      </div>


                    </div>
                  </div>
                  <div class="card-footer text-end">
                    <div class="d-flex">
                      
                      <button type="submit" class="btn btn-primary ms-auto" style="float:left">Submit</button>
                    </div>
                  </div>
                </form>

              
                </div>
           
            </div>


          
@endsection
