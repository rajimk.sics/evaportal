@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">
     
              <div class="card">
                <div class="card-body">

                    <div class="btn-list" style="float: right">
    
                        <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal" data-bs-target="#modal-report" aria-label="Create new report">
                          <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                        </a>
                      </div>

                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>
                          <li><a href="#">{{$title}}</a></li>
                        </ol>
                      </nav> 
                   
                    <h4 class="card-title">{{$title}}</h4>

                    <div class="payment-grd">
                      <form id="searchchrys_form" method="GET" enctype="multipart/form-data" action="{{ url('/followup') }}">
                          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
      
                          <div class="row">
                              <!-- Year Dropdown -->
                              <div class="col-lg-4 formcontents">
                                  <label for="year">Select Year<sup>*</sup></label>
                                  <select class="form-select" name="year" id="year">
                                      
                                      @foreach($years as $year)
                                      <option value="{{ $year }}" {{ $year == $currentYear ? 'selected' : '' }}>{{ $year }}</option>
                                      @endforeach
                                  </select>
                              </div>
      
                              <!-- Month Dropdown -->
                              <div class="col-lg-6 formcontents">
                                  <label for="month">Select Month<sup>*</sup></label>
                                  <select class="form-select" name="month" id="month">
                                     
                                      @foreach($months as $key => $month)
                                      <option value="{{ $key }}" {{ $key == $currentMonth ? 'selected' : '' }}>{{ $month }}</option>
                                      @endforeach
                                  </select>
                              </div>
      
                              <div class="col-lg-2 formcontents">
                                  <button type="submit" class="btn btn-primary" style="margin-top: 26px;">Submit</button>
                            </div>
      
                          </div>
      
                          <div class="col formcontents"> 
                            <a href="{{url('/followup/')}}" class="btn btn-primary"  style="margin-top: 26px;"> View Current Month</a>
                            </div>
                      </form>
                  </div>










                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Contact Number</th>
                <th>Follow Up Date</th>
                <th>Comments</th>
                <th>Action</th>
               
            </tr>
        </thead>
        <tbody>
            <?php $i=1;?>
            @foreach($contactlist as $contactlist)
            <tr id="{{$contactlist->id}}">
                <td>{{$i}}</td>
                <td>{{ucfirst($contactlist->name)}}</td>
                <td>{{$contactlist->contact1}}</td>
                <td>{{$contactlist->followup_date}}</td>
                <td>{{ucfirst($contactlist->followup_comments)}}</td>
                <td>
                  <button type="button" class="btn btn-green btn-sm text-white" fdprocessedid="wf07gv" onclick="contactdetails_contact('{{$contactlist->id}}')">
                    Contact Details
                  </button>
                  <button type="button" class="btn btn-cyan btn-sm text-white" fdprocessedid="wf07gv"  onclick="changefollowup('{{$contactlist->id}}')">
                    Change Follow up
                  </button>
                  <button type="button" class="btn btn-green btn-sm text-white" fdprocessedid="wf07gv" onclick="history('{{$contactlist->id}}')">
                   Follow Up History
                  </button>
                  <button type="button" class="btn btn-danger btn-sm text-white" fdprocessedid="pvth1" onclick="closed_contacts('{{$contactlist->id}}','{{$contactlist->email}}')">
                    Close
                   </button>
                </td>  
                
               
            </tr>
            <?php $i++;?>
            @endforeach           
        </tbody>     
    </table>

                    </div>
                </div>         
            </div>
            <input type="hidden" name="_token" id="token"  value="{{ csrf_token() }}">
            <div class="modal modal-blur fade" data-bs-backdrop="static" id="modal-contact" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title">Contact Details</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body" id="contactdetails">
                     
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
         

            <div class="modal modal-blur fade" data-bs-backdrop="static" id="modal-history" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title"> Follow Up History</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                    <div id="history">
                  
                    </div>
                    
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn " data-bs-dismiss="modal">Close</button>
                    
                </div>
              </div>
            </div>
            </div>





            <div class="modal modal-blur fade" data-bs-backdrop="static" id="modal-followchange" tabindex="-1" role="dialog" aria-hidden="true">
              <form id="updatefollow" method="POST" enctype="multipart/form-data"  action="{{url('/updatefollow')}}">
                <input type="hidden" name="_token" id="token1"  value="{{ csrf_token() }}">
                <input type="hidden" name="contactid_update" id="contactid_update">
              <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Follow Up</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                  <div class="form-group">
                    <label class="form-label">Follow Up Date<sup>*</sup></label>
                    <input type="text" class="form-control"  style="width:auto;" id="folloupdate_edit" name="folloupdate_edit"
                    data-zdp_readonly_element="true" >
                  </div>
                  <div class="form-group">
                    <label class="form-label">Comments<sup>*</sup></label>
                    <textarea class="form-control" rows="5" name="comments_edit" id="comments_edit"></textarea>
                  </div>
                </div>  
                
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>                                            
              
            
            </div>
              </div>
            </form>
            </div>


            <input type="hidden" id="token_eva" name="_tokens" value="{{ csrf_token() }}">
            <div class="modal modal-blur fade" data-bs-backdrop="static" id="modal-closecontact" tabindex="-1" role="dialog" aria-hidden="true">
              <form id="closedcontacts" method="POST" enctype="multipart/form-data"  action="{{url('/closedcontacts')}}">
                <input type="hidden" name="_token" id="token1"  value="{{ csrf_token() }}">
                <input type="hidden" name="contactid_close" id="contactid_close">
              <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Close</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                  <div class="form-group">
                    <label class="form-label">Email<sup>*</sup></label>
                    <input type="text" class="form-control"  style="width:auto;" id="email" name="email"  >
                  </div>
                  <div class="form-group">
                    <label class="form-label">Password<sup>*</sup></label>
                    <input type="text" class="form-control"  style="width:auto;" id="password" name="password">
                  </div>
                 
                </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>                                            
              </div>
            
            </div>
               </form>
            </div>

              
           
          
            
@endsection
