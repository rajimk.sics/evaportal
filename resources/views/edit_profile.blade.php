@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
                   
              <div class="card">

                <div class="card-body">


                <nav aria-label="breadcrumb">
                  <ol class="breadcrumbs">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="#">{{$title}}</a></li>
                  </ol>
                </nav>

                <h4 class="card-title">{{$title}}</h4>
                 
                     <form id="editprofile" method="POST" action="{{url('/update_profile')}}" enctype="multipart/form-data">
                   
                     <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">

                     <input type="hidden" name="userid" id="userid" value="{{Auth::user()->id}}">

                      <div class="col-lg-6">
                      <label class="form-label">Name<sup>*</sup></label>
                      <input type="text" class="form-control" name="name" id="name" value="{{Auth::user()->name}}">
                      </div>
                 
                      <div class="col-lg-6">
                      <label class="form-label">Upload Photo<sup></sup></label>
                      <input type="file" class="form-control" name="photo" id="photo" accept="image/*">
                      </div>
                      @if(Auth::user()->photo!='')

                      <input type="hidden" name="pic" id="pic" value="1">

                      <div class="col-sm-6 formcontents">
                        <div class="form-group">
                        <label class="form-label">Current Photo</label>
                        <img id="previewHeader" src="{{url('public/img/'.Auth::user()->photo)}}" alt="Preview" style=" max-width: 50x; max-height: 50px;">
                        </div>
                      </div>
                      @else

                      <div class="col-sm-6 formcontents imgprev" style="display:none;">
                      <div class="form-group">
                      <label class="form-label">Current Photo</label>
                      <img id="previewHeader" src="" alt="Preview" style=" max-width: 50x; max-height: 50px;">
                      </div>
                      </div>

                      <input type="hidden" name="pic" id="pic" value="0">

                      @endif

                      <div class="card-footer text-end">
                        <div class="d-flex">
                          <button type="submit" class="btn btn-primary ms-auto">Update</button>
                        </div>
                      </div>
                       
                        </form>
               </div>
            </div>
          </div>
            <script>
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif
        </script>




@endsection