<!doctype html>
<!--
* Tabler - Premium and Open Source dashboard template with responsive and high quality UI.
* @version 1.0.0-beta19
* @link https://tabler.io
* Copyright 2018-2023 The Tabler Authors
* Copyright 2018-2023 codecalm.net Paweł Kuna
* Licensed under MIT (https://github.com/tabler/tabler/blob/master/LICENSE)
-->
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>Evaportal</title>
    <!-- CSS files -->

    <link rel="icon" href="{{url('public/img/favicon.png')}}" type="image/png" sizes="16x16"> 

    <link href="{{url('public/css/tabler.min.css?1684106062')}}" rel="stylesheet"/>
    <link href="{{url('public/css/tabler-flags.min.css?1684106062')}}" rel="stylesheet"/>
    <link href="{{url('public/css/dist/css/tabler-payments.min.css?1684106062')}}" rel="stylesheet"/>
    <link href="{{url('public/css/dist/css/tabler-vendors.min.css?1684106062')}}" rel="stylesheet"/>
    <link href="{{url('public/css/dist/css/demo.min.css?1684106062')}}" rel="stylesheet"/>
    <style>
      @import url('https://rsms.me/inter/inter.css');
      :root {
      	--tblr-font-sans-serif: 'Inter Var', -apple-system, BlinkMacSystemFont, San Francisco, Segoe UI, Roboto, Helvetica Neue, sans-serif;
      }
      body {
      	font-feature-settings: "cv03", "cv04", "cv11";
      }
    </style>
  </head>
  
  <body  class=" d-flex flex-column">
    <script src="{{url('public/js/demo-theme.min.js?1684106062')}}"></script>
    <div class="page page-center">
      <div class="container container-normal py-4">
        <div class="row align-items-center g-4">
          <div class="col-lg">
            <div class="container-tight">
              <div class="text-center mb-4">

                

                <a href="." class="navbar-brand navbar-brand-autodark"><img src="{{url('public/img/logo_new.png')}}"  alt=""></a>
              </div>
              <div class="card card-md">
                <div class="card-body">
                  {{--<h2 class="h2 text-center mb-4">Login to your account</h2>--}}
                  <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="mb-3">
                        <label class="form-label">Email address</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>

                    <div class="mb-2">
                        <label class="form-label">
                          Password
                          <span class="form-label-description">
                           
                          </span>
                        </label>
                        <div class="input-group input-group-flat">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                         
                        </div>
                      </div>

                      <div class="mb-2">
                        <label class="form-check">
                         <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                          <span class="form-check-label">Remember me on this device</span>
                        </label>
                      </div>

                      <div class="form-footer">
                        <button type="submit" class="btn btn-primary w-100">Sign in</button>
                      </div>

                </form>
                </div>
             
              </div>
             
            </div>
          </div>
         
          <div class="col-lg d-none d-lg-block">
            <img src=" {{url('public/static/illustrations/undraw_secure_login_pdn4.svg')}}" height="300" class="d-block mx-auto" alt="">
          </div>
        </div>
      </div>
    </div>
    <!-- Libs JS -->
    <!-- Tabler Core -->
    <script src="{{url('public/js/tabler.min.js?1684106062')}}" defer></script>
    <script src="{{url('public/js/demo.min.js?1684106062')}}" defer></script>
  </body>
</html>



