@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input name="token_eva" type="hidden" id="token_eva" value="{{ csrf_token() }}">
                   
              <div class="card">
                <div class="card-body">
                  @if(in_array(Auth::user()->role, [1,12]))
                    <div class="btn-list" style="float: right">
                     
                        <a href="{{url('/add_college')}}" class="btn btn-primary d-none d-sm-inline-block" >
                        
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                          Create new 
                        </a>
                        <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal" data-bs-target="#modal-report" aria-label="Create new report">
                          <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                        </a>
                      </div>
                      @endif
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>
                          <li><a href="#">
                            {{ Auth::user()->role == 1 ? 'Settings - College' : 'College' }}


                            @if(in_array(Auth::user()->role, [2]))
                            College - College List
                            @endif
                          </a></li>
                        </ol>
                      </nav>
                    <h4 class="card-title">  @if(in_array(Auth::user()->role, [1,12]))
                      {{$title}}
                      @endif
                      @if(in_array(Auth::user()->role, [2]))
                      College List
                      @endif</h4>

                   
                      
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>College</th>
                @if(in_array(Auth::user()->role, [1,12]))
                <th>Status</th>  
                @endif
                <th>Action</th>
             
            </tr>
        </thead>
        <tbody>
<?php $i=1;?>
            @foreach($collegelist as $collegelist)
            <tr>
                <td>{{$i}}</td>
                <td>{{$collegelist->college}}  @if(!is_null($collegelist->district))
                 , {{ strtoupper($collegelist->district) }}
                 @endif</td>

                 @if(in_array(Auth::user()->role, [1,12]))
                
                <td>

                  @if($collegelist->college_status==1)
                  <span style="color:green;display:block;" id="act{{$collegelist->id}}">Active</span>
                  <span style="color:red;display:none;" id="deact{{$collegelist->id}}" >Deactive</span>                       
                  @else
                  <span style="color:green;display:none;" id="act{{$collegelist->id}}" >Active</span>
                  <span style="color:red;display:block;" id="deact{{$collegelist->id}}">Deactive</span>    
                  
                  @endif

                   </td>
                   
                   @endif
                                        
                    <td>
                         @if(in_array(Auth::user()->role, [1,12]))
                         <div class="d-flex">
                        
                         <a href="{{url('/get_college_details/'.$collegelist->id)}}"><button type="button" class="btn btn-cyan btn-sm text-white" fdprocessedid="wf07gv">
                          Edit
                        </button></a>
                        <button type="button" class="btn btn-blue btn-sm text-white ms-3" fdprocessedid="wf07gv" onclick="collegeDep('{{$collegelist->id}}')">
                          Department Details
                         </button>
                         @if($collegelist->college_status==1)
                          <button type="button" style="display: block" id="deactb{{$collegelist->id}}" class="btn btn-danger btn-sm text-white ms-3" fdprocessedid="pvth1" onclick="collegeStatus('{{$collegelist->id}}','deactivate')">
                                   Deactivate
                          </button>
                          <button type="button" style="display: none"  id="actb{{$collegelist->id}}" class="btn btn-success btn-sm text-white ms-3" fdprocessedid="pvth1" onclick="collegeStatus('{{$collegelist->id}}','activate')">
                             Activate
                          </button>
                           @else
                          <button type="button" style="display: block" id="actb{{$collegelist->id}}" class="btn btn-success btn-sm text-white ms-3" fdprocessedid="pvth1" onclick="collegeStatus('{{$collegelist->id}}','activate')">
                            Activate
                          </button>
                          <button type="button" style="display: none" id="deactb{{$collegelist->id}}" class="btn btn-danger btn-sm text-white ms-3" fdprocessedid="pvth1" onclick="collegeStatus('{{$collegelist->id}}','deactivate')">
                            Deactivate
                           </button>
                          
                           @endif
                         </div>
                            @endif
                         
                          @if(in_array(Auth::user()->role, [2]))
                         
                           <button type="button" class="btn btn-green btn-sm text-white me-2" fdprocessedid="wf07gv" onclick="collegeUpdates('{{$collegelist->id}}')">
                            Update
                            </button>
                            <button type="button" class="btn btn-blue btn-sm text-white me-2" fdprocessedid="wf07gv" onclick="getCollegeUpdateHistory('{{$collegelist->id}}')">
                              Updated History
                              </button>

                              @php
                                $isClaim=App\Helpers\CustomHelper::ClaimCheck($collegelist->id); 
                               @endphp
                         
                              <a href="{{ ($collegelist->departments == '' || $isClaim == 0) ? '#' : url('/add_claimstudents/'.$collegelist->id) }}">
                                  <button type="button" class="btn btn-red btn-sm text-white" 
                                      fdprocessedid="wf07gv" 
                                      {{ ($collegelist->departments == '' || $isClaim == 0) ? 'disabled' : '' }}>
                                      Claim Request
                                  </button>
                              </a>
                            
                              @endif
                         </td>
            </tr>
            <?php $i++;?>
            @endforeach
           
        </tbody>
      
    </table>

  </div>
</div>

</div>
</div>

</div>
    <div class="modal modal-blur fade" id="departmentDetailsModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Department Details</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
   

<!--Update model-->

<div class="modal modal-blur fade" id="modalCollegeUpdates" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="modalCollegeUpdatesLabel">College Updates</h5>
      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <form id="collegeups" method="POST" enctype="multipart/form-data"  action="{{url('/college_update')}}">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="collegeid" id="collegeid">
      <input type="hidden" name="updatetype" id="updatetype">
    <div class="modal-body">
     
      <div class="row">
        <div class="col-lg-6">
          <div class="form-group">
            <label class="form-label">Department<sup>*</sup></label>
            <select class="form-select" id="collegedep" name="collegedep" >
             
            </select>
          </div>
          </div>
          <div class="col-lg-6">
            <div class="form-group">
              <label class="form-label">POC Designation<sup>*</sup></label>
              <select class="form-select selectclass" id="collegepoc" name="collegepoc[]" multiple="multiple">
           
              </select>
            </div>
          </div>
        </div>
        <div class="row">
        <div class="col-lg-6">
          <div class="form-group">
            <label class="form-label">Comments<sup>*</sup>(Visible to everyone)</label>
            <textarea class="form-control" rows="5" name="comments" id="comments"></textarea>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="form-group">
            <label class="form-label">Status<sup>*</sup>(Visible only to you and your team lead)</label>
            <textarea class="form-control" rows="5" name="updatestatus" id="updatestatus"></textarea>
          </div>
        </div>
      </div>
    </div>
    
  <div class="modal-footer">
    <button type="submit" id="college_edit" class="btn btn-primary">Update</button>
  </div>                                            
  </div>

</div>
</div>
</form>


{{-- Updated college history --}}
<div class="modal fade" id="collegeHistoryModal" tabindex="-1" aria-labelledby="collegeHistoryModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="collegeHistoryModalLabel">College Update History</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              <h4 id="collegeName"></h4>
              <div id="collegeUpdates">
              </div>
          </div>
          <div class="modal-footer">
          </div>
      </div>
  </div>
</div>

<div class="modal fade" id="messageModal" tabindex="-1" aria-labelledby="messageModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="messageModalLabel">Notice</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
              </button>
          </div>
          <div class="modal-body">
              
          </div>
      </div>
  </div>
</div>
@endsection
