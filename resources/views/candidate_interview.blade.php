@extends('layouts.main')

@section('content')


<!-- Page body -->
<div class="page-body">
  <div class="container-xl">

    <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">

    <div class="card">
      <div class="card-body">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumbs">
            <li><a href="{{url('/home')}}">Home</a></li>
            <li><a href="#">{{$title}}</a></li>
          </ol>
        </nav>
        <h4 class="card-title"> {{$title}}</h4>
        <div class="payment-grd">
                      <form id="searchchrys_form" method="GET" enctype="multipart/form-data" action="{{ url('/candidate_interview') }}">
                          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
      
                          <div class="row">
                              <!-- Year Dropdown -->
                              <div class="col-lg-4 formcontents">
                                  <label for="year">Select Year<sup>*</sup></label>
                                  <select class="form-select" name="year" id="year">
                                      
                                      @foreach($years as $year)
                                      <option value="{{ $year }}" {{ $year == $currentYear ? 'selected' : '' }}>{{ $year }}</option>
                                      @endforeach
                                  </select>
                              </div>
      
                              <!-- Month Dropdown -->
                              <div class="col-lg-6 formcontents">
                                  <label for="month">Select Month<sup>*</sup></label>
                                  <select class="form-select" name="month" id="month">
                                     
                                      @foreach($months as $key => $month)
                                      <option value="{{ $key }}" {{ $key == $currentMonth ? 'selected' : '' }}>{{ $month }}</option>
                                      @endforeach
                                  </select>
                              </div>
      
                              <div class="col-lg-2 formcontents">
                                  <button type="submit" class="btn btn-primary" style="margin-top: 26px;">Submit</button>
                            </div>
      
                          </div>
      
                          <div class="col formcontents"> 
                            <a href="{{url('/candidate_interview/')}}" class="btn btn-primary"  style="margin-top: 26px;"> View Current Month</a>
                            </div>
                      </form>
                  </div>
        <div class="table-responsive">
          <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Contact Number</th>
                <th>Registration Source</th>
                <th>Position Applied</th>
                <th>Company Name</th>
                <th>Interview Date</th>
                <th>Interview Status</th>
                <th>Organizer</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              @foreach($candlist as $candlist)
              <tr id="row_{{$i}}">
                <td>{{$i}}</td>
                <td>{{ucfirst($candlist->name)}}</td>
                <td>{{$candlist->email}}</td>
                <td>{{$candlist->contact_pri}}</td>
                <td>{{ $candlist->app == 1 ? 'App' : 'Web' }}</td>
                <td>{{$candlist->position}}</td>
                <td>{{$candlist->company_name}}</td>
                <td>{{ date('d-m-Y', strtotime($candlist->interviewdate)) }}</td>
                <td id="statustext_{{$i}}">
                  @switch($candlist->interview_status)
                  @case(0)
                  Interview Scheduled
                      @break
                  @case(1)
                      Shortlisted
                      @break
                  @case(2)
                      Rejected
                      @break
                  @case(3)
                      On hold
                      @break
                  @default
                      Unknown Status
              @endswitch
                </td>
                
                <td>
                  {{$candlist->recruiter_name}}            
                </td>
                <td>
                  <button type="button" class="btn btn-cyan btn-sm text-white" style="margin-bottom: 10px;" onclick="candidatedetails('{{$candlist->jobreg_id}}')">
                    Details
                  </button>
                  @if($candlist->interview_status==0)
                  <button type="button" id="offer_{{$i}}" class="btn btn-orange btn-sm text-white" style="margin-bottom: 10px;" onclick="candidateoffer('{{$candlist->id}}','{{$i}}')">
                    Offer
                  </button>
                  @endif
                </td>
              </tr>
              <?php $i++; ?>
              @endforeach

            </tbody>

          </table>


        </div>
      </div>

    </div>
    

    <div class="modal modal-blur fade" data-bs-backdrop="static" id="modal-candidate-offer" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Follow Up</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <form id="candidateoffer" method="POST" enctype="multipart/form-data" action="{{url('/saveoffer')}}">
              <input type="hidden" name="_token" id="token1" value="{{ csrf_token() }}">
              <input type="hidden" name="rowsid" id="rowsid">
              <input type="hidden" name="interviewid" id="interviewid">
              <div class="form-group">
                <label class="form-label">Status<sup>*</sup></label>
                <select class="form-select " name="interviewstatus" id="interviewstatus" placeholder="Status">
                  <option value="">Select Status</option>
                  <option  value="1">Short Listed</option>
                  <option  value="2">Rejected</option>
                  <option  value="3">Onhold</option>
                </select>
              </div>
              <div class="mb-3">
                <label class="form-label">Comments<sup>*</sup></label>
                <textarea class="form-control" rows="5" name="comments" id="comments"></textarea>
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
      </form>
    </div>



    <div class="modal modal-blur fade" id="modal-candidate" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <h5 class="modal-title">Candidate Details</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body" id="candidatedetails">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>


    @endsection