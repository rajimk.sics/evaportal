@extends('layouts.main')

@section('content')

<style>

  .formcontents{
    display: flex;
    flex-direction: column;
  }
  label.error{
    color: red;
    order: 3;
  }

  </style>

        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
                   
              <div class="card">
                <div class="card-body">


                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>
                          <li><a href="{{url('/claimed_collegeSales')}}">Claimed College</a></li>
                          <li><a href="{{url('/manage_package_chrysallis')}}">Chrysalis Package</a></li>
                          <li><a href="#">{{$title}}</a></li>
                        </ol>
                      </nav>
                    <h4 class="card-title"> {{$title}}</h4>
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Student Name</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>Joining Date</th>
                <th>Package Total Fee</th>
                
                {{-- <th>Action</th> --}}

            </tr>
        </thead>
        <tbody>
          <?php $i=1;?>
            @foreach($studentlist as $list)
            <tr>
                <td>{{$i}}</td>
                <td>{{ucfirst($list->name)}}</td>
                <td>{{$list->email}}</td>
                <td>{{$list->phone}}</td>
                <td>{{date("d-m-Y", strtotime($list->joining_date))}}</td>
                <td>{{$list->package_fullamount}}</td>
              
              
            </tr>
            <?php $i++;?>
            @endforeach
           
        </tbody>
      
    </table>

















                    </div>
                </div>
           
            </div>
            <script>
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif
        </script>




@endsection
