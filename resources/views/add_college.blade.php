@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">
              <div class="row row-cards">
                <div class="col-12">
             <form id="college_form" method="POST" enctype="multipart/form-data"  action="{{url('/save_college')}}">
              <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumbs">
                  <li><a href="{{url('/home')}}">Home</a></li>
                  <li><a href="{{url('/manage_college')}}">{{ Auth::user()->role == 1 ? 'Settings - College' : 'College' }}</a></li>
                  <li><a href="#">{{$title}}</a></li>
                </ol>
              </nav>
                  <div class="card-header">
                    <h2 class="fs-title">{{$title}}</h2>
                  </div>

                 
                  <div class="card-body">
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                <label class="form-label">College<sup>*</sup></label>
               <input type="text" class="form-control" name="college" id="college" placeholder="College">
             </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
              <label class="form-label">State<sup>*</sup></label>
              <select class="form-select selecttype selectclass collegestate" id="collegestate" name="collegestate" >
                <option value="">Select State</option>
                @foreach($states as $state)
                <option value="{{ $state->id }}">{{ $state->name }}</option>
               @endforeach
              </select>
           </div>
              </div>
            </div>
            <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label class="form-label">District<sup>*</sup></label>
                <select class="form-select selecttype selectclass collegedist" id="collegedist" name="collegedist" >
                  
                </select>
              </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label">Place<sup>*</sup></label>
                    <input type="text" class="form-control" name="collegeplace" id="collegeplace" placeholder="College Place">
                  
            </div>
            </div>
            </div>
            <div class="row">  
              <div class="col-lg-6"> 
                <label class="form-label">Department<sup>*</sup></label>
                <select class="form-select selectdesi selectclass" name="collegedep[]"  id="collegedep" multiple="multiple">
                  @foreach($depts as $dept )
                  <option value="{{$dept->id}}">{{$dept->department}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          </div>
          <div class="card-footer text-end">
            <div class="d-flex">
              <button type="submit" class="btn btn-primary ms-auto" id="submit">Submit</button>
            </div>
          </div>
          </form>
                    </div>
                </div> 
            </div>

@endsection




