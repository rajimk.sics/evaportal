@extends('layouts.main')

@section('content')

    <!-- Page body -->
    <div class="page-body">
        <div class="container-xl">

            @if (Session::has('duplicates'))
            <div class="alert alert-warning mt-3">
                <p><strong>Duplicate values found:</strong></p>
                <ul>
                    @foreach (Session::get('duplicates') as $duplicate)
                        <li>
                            Question: {{ $duplicate['questions'] }} found on Technology: {{ $duplicate['technology'] }}
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-danger">
                {{ Session::get('error') }}
            </div>
        @endif
                

            <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">

            <div class="card">
                <div class="card-body">

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                            <li><a href="{{ url('/home') }}">Home</a></li>
                            <li><a href="{{ url('/viewQuestionUpload') }}">Questions -{{ $title }}</a></li>
                        </ol>
                    </nav>

                    <div class="card">
                        <h5 class="card-title">{{ $title }}</h5>

                        <form id="examUpload" method="POST" enctype="multipart/form-data"
                            action="{{ url('/questionsUpload') }}">
                            <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">

                            <!-- Technology Selection -->
                            <div class="row">   
                                <div class="col-6 mt-2">
                                    <label for="technology">Select Technology</label>
                                    <select id="technology" name="technology[]" class="form-select selecttech rounded"
                                        multiple required>
                                        <option value="" disabled>Choose Technology</option>
                                        @foreach ($technologies as $technology)
                                            <option value="{{ $technology->id }}">{{ $technology->technology }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <!-- CSV File Upload -->
                            <div class="row mt-3 mb-3">
                                <label for="csv" class="col-form-label">Upload CSV File</label>
                                <div class="col-6">
                                    <input type="file" class="form-control" name="csv_file" id="csv" required>
                                    <small class="form-text text-muted mt-1">
                                        <a href="{{ url('/public/uploads/excel/Questions.csv') }}" download="Questions"
                                            style="color: black;">
                                            Download sample CSV format
                                        </a>
                                    </small>
                                </div>
                            </div>

                            <!-- Submit Button -->
                            <div class="d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary">Upload</button>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>


    @if (Session::has('message'))

    <script>
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
    </script>
    @endif

@endsection
