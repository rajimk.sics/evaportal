@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">
                   
              <div class="card">
                <div class="card-body">

                  <nav aria-label="breadcrumb">
                    <ol class="breadcrumbs">
                        <li><a href="{{ url('/home') }}">Home</a></li>
                        <li><a href="{{ url('/manageQuestions') }}">Questions -Manage Questions</a></li>
                    </ol>
                </nav>

                <form method="GET" id="uploadQuesfilterform" action="{{ url('/manageQuestions') }}" class="mb-3 row mt-3">
                    <div class="col-4">
                        <select name="technology[]" class="form-select selecttech" multiple id="uploadQuesfilterSubmit">
                            <option value="" disabled>All Technologies</option>
                            @foreach($technologies as $technology)
                                <option value="{{ $technology->id }}" 
                                    {{ in_array($technology->id, (array) request('technology')) ? 'selected' : '' }}>
                                    {{ $technology->technology }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-8 d-flex align-items-end">
                        <button type="submit" class="btn btn-primary me-2" id="uploadQuesfilterSubmit">Filter</button>
                        <a href="{{ url('/manageQuestions') }}" class="btn btn-secondary">Clear All</a>
                    </div>
                </form>
                              
                    <h4 class="card-title">Manage Questions</h4>
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Questions</th>
                <th>Option A</th>
                <th>Option B</th>
                <th>Option C</th>
                <th>Option D</th>
                <th>Answer</th>
                <th>Technology</th>
                <th>Status</th>
               <th>Action</th>
            </tr>
        </thead>
        <tbody>
<?php $i=1;?>
            @foreach($questions as $question)
            <tr>
                <td>{{$i}}</td>
                <td>{{ ucfirst($question->questions) }}</td>
                <td>{{$question->optiona}}</td>
                <td>{{$question->optionb}}</td>
                <td>{{$question->optionc}}</td>
                <td>{{$question->optiond}}</td>
                <td>Option {{$question->answer}}</td>
                <td>{{ $question->technology_list }}</td>

                <td>
                  @if($question->status == 1)
                  <span style="color:green;display:block"
                  id="act_{{ $question->id }}">Active</span>
              <span style="color:red;display:none"
                  id="deact_{{  $question->id }}">Deactive</span>
              @else
                  <span style="color:red;display:block"
                  id="deact_{{ $question->id }}">Deactive</span>
              <span style="color:green;display:none"
                  id="act_{{ $question->id }}">Active</span>
              @endif
                 </td> 
                  <td>
                    @if($question->status == 1)
                    <button type="button" class="btn btn-danger btn-sm text-white me-2"
                    style="display: block"
                    onclick="managequestionstatus('{{ $question->id }}','deactivate')"
                    id="deactb_{{ $question->id }}">
                    Deactivate Question
                </button>
                <button type="button"
                    class="btn btn-success btn-sm text-white me-2"
                    style="display: none"
                    onclick="managequestionstatus('{{ $question->id }}','activate')"
                    id="actb_{{ $question->id }}">
                    Activate Question
                </button>
                @else
                    <button type="button"
                    class="btn btn-success btn-sm text-white me-2"
                    style="display: block"
                    onclick="managequestionstatus('{{ $question->id }}','activate')"
                    id="actb_{{ $question->id }}">
                    Activate Question
                </button>
                <button type="button" class="btn btn-danger btn-sm text-white me-2"
                    style="display: none"
                    onclick="managequestionstatus('{{ $question->id  }}','deactivate')"
                    id="deactb_{{ $question->id }}">
                    Deactivate Question
                </button>
                @endif 
                <button type="button" class="btn btn-primary btn-sm text-white me-2"
                                                onclick="loadQuestionData('{{ $question->id }}')">
                                                Edit
                                            </button>                         
                 </td>
               </tr>
            <?php $i++;?>
            @endforeach
           
        </tbody>
      
    </table>
</div>
</div>

                  
                </div>
                </div>
           
            </div>


    <!-- Edit Question Modal -->
    <div class="modal fade" id="editQuestionModal" tabindex="-1" aria-labelledby="editQuestionModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editQuestionModalLabel">Edit Question</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="editQuestionForm" method="POST" action="{{ url('/updateQuestion') }}">
                    <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">
                    <div class="modal-body">
                        <input type="hidden" name="id" id="questionId">
                        <div class="mb-3">
                            <label for="editQuestionText" class="form-label">Question</label>
                            <textarea class="form-control" name="questions" id="editQuestionText" rows="3" required></textarea>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="editOptionA" class="form-label">Option A</label>
                                <input type="text" class="form-control" name="optiona" id="editOptionA" required>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="editOptionB" class="form-label">Option B</label>
                                <input type="text" class="form-control" name="optionb" id="editOptionB" required>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="editOptionC" class="form-label">Option C</label>
                                <input type="text" class="form-control" name="optionc" id="editOptionC" required>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="editOptionD" class="form-label">Option D</label>
                                <input type="text" class="form-control" name="optiond" id="editOptionD" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="editAnswer" class="form-label">Answer</label>
                            <select class="form-select " name="answer" id="editAnswer" required>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="C">C</option>
                                <option value="D">D</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Update Question</button>
                    </div>
                </form>
            </div>
        </div>
    </div>




@endsection
