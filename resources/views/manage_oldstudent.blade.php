@extends('layouts.main')

@section('content')

<style>

  .formcontents{
    display: flex;
    flex-direction: column;
  }
  label.error{
    color: red;
    order: 3;
  }

  </style>

        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">
                   
              <div class="card">
                <div class="card-body">

                      <nav aria-label="breadcrumb">
                      <ol class="breadcrumbs">
                        <li><a href="{{url('/home')}}">Home</a></li>
                        <li><a href="#">{{$title}}</a></li>
                      </ol>
                    </nav>
                    <h4 class="card-title">{{$title}}</h4>



                    <a href="{{url('/public/uploads/excel/OldData.xlsx')}}" download="Old Data">Download  Sample Excel Template </a>

                    <div class="payment-grd">
                      <form id="searchchrys_form" method="GET" enctype="multipart/form-data" action="{{ url('/manage_oldstudent/') }}">
                          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
      
                          <div class="row">
                              <!-- Year Dropdown -->
                              <div class="col-lg-4 formcontents">
                                  <label for="year">Select Year<sup>*</sup></label>
                                  <select class="form-select" name="year" id="year">
                                      
                                      @foreach($years as $year)
                                      <option value="{{ $year }}" {{ $year == $currentYear ? 'selected' : '' }}>{{ $year }}</option>
                                      @endforeach
                                  </select>
                              </div>
      
                              <!-- Month Dropdown -->
                              <div class="col-lg-6 formcontents">
                                  <label for="month">Select Month<sup>*</sup></label>
                                  <select class="form-select" name="month" id="month">
                                     
                                      @foreach($months as $key => $month)
                                      <option value="{{ $key }}" {{ $key == $currentMonth ? 'selected' : '' }}>{{ $month }}</option>
                                      @endforeach
                                  </select>
                              </div>
      
                              <div class="col-lg-2 formcontents">
                                  <button type="submit" class="btn btn-primary" style="margin-top: 26px;">Submit</button>
                            </div>
      
                          </div>
      
                          <div class="col formcontents"> 
                            <a href="{{url('/manage_oldstudent/')}}" class="btn btn-primary"  style="margin-top: 26px;"> View Current Month</a>
                            </div>
                      </form>
                  </div>

                <form id="bulk_students" action="{{ url('bulkupload_student') }}" method="POST" enctype="multipart/form-data">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">

                  <div class="row">
                    <div class="col-lg-6 formcontents">
                        <div class="form-group">
                            
                            <input style="height: inherit;" class="form-control" type="file"  id="file" name="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                           
                        </div>
                    </div>
                    <div class="col-lg-6 formcontents">
                      <div class="form-group">
                        <button type="submit" class="btn btn-blue btn-sm text-white">Upload</button>
                          
                      </div>
                  </div>
                  </div>

                   </form>
                  
                   @if(Session::has('duplicates'))
                  <div class="alert alert-warning">
                    <p>Duplicate values:</p>
                     <ul>
                        @foreach(Session::get('duplicates') as $duplicate)
                           <li>{{ $duplicate }}</li>
                        @endforeach
                    </ul>
                  </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                            @endforeach
                       </ul>
                       </div>
                  @endif
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Student Name</th>
                <th>SICS Reg Number</th>              
                <th>Phone Number</th>
                <th>Email</th>
                <th>Date of Joining</th> 
                <th>Package</th>
                <th>Technology</th>             
                <th>Category</th>
                <th>Source</th>
                
                <th>Total Fees</th>
                @if(Auth::user()->role == 2 )
                <th>Action</th>
                @endif
            </tr>
        </thead>
        <tbody>
<?php $i=1;?>
            @foreach($studlist as $studlist)
            <tr>
                <td>{{$i}}</td>
                <td>{{$studlist->name}}</td>
                <td>{{$studlist->reg_no}}</td>
                <td>{{$studlist->contact_no}}</td>
                <td>{{$studlist->email_id}}</td>
                <td>{{date('d-m-Y', strtotime($studlist->date_joining))}}</td>
                <td>{{$studlist->package}}</td>
                <td>{{$studlist->technology}}</td>
                <td>{{$studlist->category}}</td>
                <td>{{$studlist->source}}</td>
              
               <td> Rs {{number_format($studlist->totalfees,2)}}</td>
               
                @if(Auth::user()->role == 2 )
                <td>
                
                <a href="{{url('/oldpayment_history/'.$studlist->id)}}">
                            <button type="button" class="btn btn-cyan btn-sm text-white" fdprocessedid="wf07gv">
                            Payment History
                            </button>
                        </a>
                
                </td>
                @endif      
            </tr>
            <?php $i++;?>
            @endforeach
           
        </tbody>
      
    </table>

            <script>
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif
        </script>

@endsection
