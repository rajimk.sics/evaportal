@extends('layouts.main')

@section('content')
    <!-- Page body -->
    <div class="page-body">
        <div class="container-xl">
            @if (Session::has('error'))
            <div class="alert alert-danger">
                {{ Session::get('error') }}
            </div>
        @endif


            <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumbs">
                    <li><a href="{{ url('/home') }}">Home</a></li>
                    <li><a href="{{ url('/fetchExam') }}">Exams - {{ $title }}</a></li>
                </ol>
            </nav>
            <h4 class="card-title mt-3"> {{ $title }}</h4>
            @if (!empty($exams))

                <div class="card mt-2">
                    <div class="card-body">

                        <form id="filter-form" method="GET" action="{{ url('/fetchExam') }}">
                            <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">

                            <div class="row g-3 align-items-center">
                                <div class="col">
                                    <label for="dropdown1" class="col-form-label">Search By Type</label>
                                </div>
                                <div class="col-3">
                                    <select class="form-select rounded examtechnology" id="examtechnology"
                                        name="examtechnology">
                                        <option {{ $selectedExamType == 'false' ? 'selected' : '' }} value="false">Select
                                            Exam Type</option>
                                        <option {{ $selectedExamType == 'Talento' ? 'selected' : '' }} value="Talento">
                                            Talento</option>
                                        <option {{ $selectedExamType == 'Selection Test' ? 'selected' : '' }}
                                            value="Selection Test">Selection Test</option>
                                        <option {{ $selectedExamType == 'Internship' ? 'selected' : '' }}
                                            value="Internship">Internship</option>
                                    </select>
                                </div>

                                <div class="col">
                                    <select class="form-select rounded" id="techselect" name="techselect">
                                        <option value="">Select Technology</option>
                                        @foreach ($technology as $item)
                                            <option {{ $selectedTechnology == $item->id ? 'selected' : '' }}
                                                value="{{ $item->id }}">{{ $item->technology }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col">
                                    <select class="form-select rounded" id="techpackages" name="techpackages">
                                        <option value="">Select Package</option>
                                       
                                    </select>
                                </div>

                                <!-- Updated button container -->
                                <div class="col-auto d-flex align-items-center">
                                    <button type="submit" class="btn btn-primary">Filter</button>
                                    <a href="{{ url('/fetchExam') }}" class="btn btn-primary ms-2">Clear All</a>
                                </div>
                            </div>
                        </form>



                        <div class="table-responsive mt-3">
                            <table id="exam-results" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Exam name</th>
                                        <th>Exam Type</th>
                                       
                                        <th>Technology</th>
                                        <th>Packages</th>
                                        <th>Percentage</th>
                                        <th>Status</th>
                                        <th>Actions</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    @if (isset($exams))
                                        @foreach ($exams as $index => $exam)
                                            <tr>
                                                <td>{{ $index + 1 }}</td>
                                                <td>{{ ucfirst($exam->exam_name) }}</td>
                                                <td>{{ ucfirst($exam->examtype) }}</td>
                                                <td>{{ ucfirst($exam->technologies) }}</td>
                                                <td>{{ ucfirst($exam->packages) }}</td>
                                                <td>{{ $exam->percentage }}</td>
                                                <td>
                                                    @if($exam->status == 1)
                                                    <span style="color:green;display:block"
                                                    id="act_{{ $exam->exam_id }}">Active</span>
                                                <span style="color:red;display:none"
                                                    id="deact_{{  $exam->exam_id }}">Deactive</span>
                                                @else
                                                    <span style="color:red;display:block"
                                                    id="deact_{{ $exam->exam_id }}">Deactive</span>
                                                <span style="color:green;display:none"
                                                    id="act_{{ $exam->exam_id }}">Active</span>
                                                @endif
                                  
                                                </td>
                                                <td>
                                                    @if($exam->status == 1)
                                                    <button type="button" class="btn btn-danger btn-sm text-white me-2"
                                                    style="display: block"
                                                    onclick="changeExamStatus('{{ $exam->exam_id }}','deactivate')"
                                                    id="deactb_{{ $exam->exam_id }}">
                                                    Deactivate exam
                                                </button>
                                                <button type="button"
                                                    class="btn btn-success btn-sm text-white me-2"
                                                    style="display: none"
                                                    onclick="changeExamStatus('{{ $exam->exam_id }}','activate')"
                                                    id="actb_{{ $exam->exam_id }}">
                                                    Activate exam
                                                </button>
                                                @else
                                                    <button type="button"
                                                    class="btn btn-success btn-sm text-white me-2"
                                                    style="display: block"
                                                    onclick="changeExamStatus('{{ $exam->exam_id }}','activate')"
                                                    id="actb_{{ $exam->exam_id }}">
                                                    Activate exam
                                                </button>
                                                <button type="button" class="btn btn-danger btn-sm text-white me-2"
                                                    style="display: none"
                                                    onclick="changeExamStatus('{{ $exam->exam_id  }}','deactivate')"
                                                    id="deactb_{{ $exam->exam_id }}">
                                                    Deactivate Question
                                                </button>
                                                @endif 
                                


                                                    <a href="{{ url('addExamquestions/' . $exam->exam_id) }}"
                                                        title="Assign Questions">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                            height="24" viewBox="0 0 24 24" fill="currentColor"
                                                            class="icon icon-tabler icons-tabler-filled icon-tabler-copy-plus">
                                                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                            <path
                                                                d="M18.333 6a3.667 3.667 0 0 1 3.667 3.667v8.666a3.667 3.667 0 0 1 -3.667 3.667h-8.666a3.667 3.667 0 0 1 -3.667 -3.667v-8.666a3.667 3.667 0 0 1 3.667 -3.667zm-4.333 4a1 1 0 0 0 -1 1v2h-2a1 1 0 0 0 -.993 .883l-.007 .117a1 1 0 0 0 1 1h2v2a1 1 0 0 0 .883 .993l.117 .007a1 1 0 0 0 1 -1v-2h2a1 1 0 0 0 .993 -.883l.007 -.117a1 1 0 0 0 -1 -1h-2v-2a1 1 0 0 0 -.883 -.993zm1 -8c1.094 0 1.828 .533 2.374 1.514a1 1 0 1 1 -1.748 .972c-.221 -.398 -.342 -.486 -.626 -.486h-10c-.548 0 -1 .452 -1 1v9.998c0 .32 .154 .618 .407 .805l.1 .065a1 1 0 1 1 -.99 1.738a3 3 0 0 1 -1.517 -2.606v-10c0 -1.652 1.348 -3 3 -3z" />
                                                        </svg>
                                                    </a>
                                                    <a href="{{ url('viewAssignedquestions/' . $exam->exam_id) }}"
                                                        title="View Questions">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                            height="24" viewBox="0 0 24 24" fill="none"
                                                            stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                                            stroke-linejoin="round">
                                                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                            <path d="M10 12a2 2 0 1 0 4 0a2 2 0 0 0 -4 0" />
                                                            <path
                                                                d="M21 12c-2.4 4 -5.4 6 -9 6c-3.6 0 -6.6 -2 -9 -6c2.4 -4 5.4 -6 9 -6c3.6 0 6.6 2 9 6" />
                                                        </svg>
                                                    </a>
                                                </td>

                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="5" class="align-middle">No exams found</td>
                                        </tr>
                                    @endif
                                </tbody>

                            </table>


                        </div>
                    </div>

                </div>
            @else
                <p>No exams found.</p>
            @endif


            <script>
                @if (Session::has('message'))
                    swal({
                        title: "",
                        text: "{{ Session::get('message') }}",
                        type: "success",
                        showCancelButton: false,
                        dangerMode: false,
                        confirmButtonText: 'OK',
                    });
                @endif
            </script>
        @endsection
