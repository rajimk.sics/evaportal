@extends('layouts.main')

@section('content')
   <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">   
              <div class="card">
                <div class="card-body">

                  <input type="hidden" name="_token" id="token"  value="{{ csrf_token() }}">
                   
                    <h4 class="card-title">{{$title}}</h4>
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>

                @if($pac_type==1)
                <th>Admission Id</th>
                @endif
                <th>Name</th>
                <th>Email Id</th>
                <th>Phone Number</th>
                <th>Action</th>
               
            </tr>
        </thead>
        <tbody>
          <?php $i=1;?>
                      @foreach($userlist as $userlist)
                      <tr>
                          <td>{{$i}}</td>
                          @if($pac_type==1)
                          <td>SIAC{{$userlist->student_admissionid}}</td>
                          @endif
                          <td>{{ucfirst($userlist->name)}}</td>
                          <td>{{$userlist->email}}</td>
                          <td>{{$userlist->phone}}</td>
                          <td></td>
                          
                         
                      </tr>
                      <?php $i++;?>
                      @endforeach
                     
                  </tbody>
      
    </table>
                    </div>
                </div>
           
            </div>

      
<input type="hidden" name="_token" id="token"  value="{{ csrf_token() }}">
<div class="modal modal-blur fade" id="modal-employee" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Employee Details</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body" id="employeedetails">
         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>      
@endsection
