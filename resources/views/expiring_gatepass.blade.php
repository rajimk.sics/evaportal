@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">

              <input type="hidden" name="logname" id="logname" value="{{ Auth::user()->name }}">
                   
              <div class="card">
                <div class="card-body">

                     
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumbs">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="#">{{$title}}</a></li>
                  </ol>
                </nav>
                   
                    <h4 class="card-title">{{$title}}</h4>


                    <div class="payment-grd">
                      <form id="searchchrys_form" method="GET" enctype="multipart/form-data" action="{{ url('/expiring_gatepass') }}">
                          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
      
                        
      
                              <!-- Day Dropdown -->
                              <div class="col-lg-12 formcontents">
                                  <label for="no_days">Select Expiring Within<sup>*</sup></label>
                                  <select class="form-select" name="no_days">
                                     <option value="5" {{$nodays == 5 ? 'selected' : '' }}>View All</option>
                                      <option value="0" {{$nodays == 0 ? 'selected' : '' }}>Today</option>
                                      <option value="1" {{ $nodays == 1 ? 'selected' : '' }}>2 Day</option>
                                      <option value="2" {{ $nodays == 2 ? 'selected' : '' }}>3 Day</option>
                                      <option value="3" {{ $nodays == 3 ? 'selected' : '' }}>4 Day</option>
                                      <option value="4" {{ $nodays == 4 ? 'selected' : '' }}>5 Day</option>
                                  
                                  </select>
                              </div>
      
                              <div class="col-lg-2 formcontents">
                                  <button type="submit" class="btn btn-primary" style="margin-top: 26px;">Submit</button>
                            </div>
      
                          </div>
      
                         
                      </form>
                  </div>


                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Student Name</th>
                <th>Photo</th>
                <th>Requested Date</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Type</th>
                <th>Expiring within</th>
                @if(Auth::user()->role =='4' || Auth::user()->role =='8' )
                <th>Action</th>
                @endif
               
               
             
              
            </tr>
        </thead>
        <tbody>
        <?php $i=1;?>
            @foreach($gatelist as $gatelist)
            <tr id="row_{{$gatelist->id}}" data-userid="{{$gatelist->user_id}}" data-gatepassid="{{$gatelist->id}}" data-gatepassendate="{{date("d-m-Y", strtotime($gatelist->end_date))}}">
                <td>{{$i}}</td>
                <td>{{$gatelist->name}}</td>
                <td>  <img src="{{url('public/uploads/photo/'.$gatelist->photo)}}" width="50px" height="50px" ></td>
                <td>
                  {{date("d-m-Y", strtotime($gatelist->requested_date))}}
                
                </td>
                <td>
                 <span id="sdate_{{$gatelist->id}}"> {{date("d-m-Y", strtotime($gatelist->start_date))}}</span>
                  
                 </td>
                <td>
                  <span id="edate_{{$gatelist->id}}">{{date("d-m-Y", strtotime($gatelist->end_date))}}</span>
                 </td>
                
                <td>

                  @php
                  $type_name=App\Helpers\CustomHelper::typename($gatelist->type,$gatelist->type_id); 
                  @endphp
                  @if($gatelist->type==1)
                  Regular-{{ $type_name['name']}}
                  @endif
                  @if($gatelist->type==2)
                  Chrysalis-{{ $type_name['name']}}
                  @endif
                  @if($gatelist->type==3)
                  Event-{{ $type_name['name']}}
                  @endif
                </td>
                <td>
              
                  {{App\Helpers\CustomHelper::daycount($gatelist->end_date);}} Days
                    </td>



                @if(Auth::user()->role =='4' || Auth::user()->role =='8' )
                  <td>
                    <button type="button" id="issue_{{$gatelist->id}}"  class="btn btn-success btn-sm text-white" fdprocessedid="pvth1" {{ $gatelist->reissue_status==1 ? 'disabled' : '' }} onclick="reissueGatepass(this)">
                    Re-issue 
                  </button>
                  <span id="action_by_{{$gatelist->id}}"> </span>
               
                 
                @if($gatelist->status==4)
                    @php
                    $lastdate=App\Helpers\CustomHelper::reissueDate($gatelist->user_id); 
                    @endphp

                   @if(!empty($lastdate))
                    </br> <span>Last Reissued Date:{{$lastdate['reissued']}}</span></br>
                    @endif
                 @endif
                </td>
                @endif

               </tr>


              





            <?php $i++;?>
            @endforeach
           
        </tbody>
      
    </table>

    <div class="modal modal-blur fade" data-bs-backdrop="static" id="modal-reissue" tabindex="-1" role="dialog" aria-hidden="true">
       
    
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          
          <h5 class="modal-title">Re-issuing Gatepass</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form id="reissue_form" method="POST" enctype="multipart/form-data"  action="{{url('/savereissuegatepass')}}">
            <input type="hidden" name="_tokens" id="token_eva" value="{{ csrf_token() }}">
            <input type="hidden" name="student_id" id="student_id" value="">
            <input type="hidden" name="rowno" id="rowno" value="">
            <input type="hidden" name="renddate" id="renddate" value="">
          <div class="mb-3">
            <label class="form-label">Start Date<sup>*</sup></label>
            <input type="text" class="form-control" data-zdp_readonly_element="true" name="redate1" id="redate1" >
          </div>
          <div class="mb-3">
            <label class="form-label">End Date<sup>*</sup></label>
            <input type="text" class="form-control" data-zdp_readonly_element="true" name="redate2" id="redate2" >
          </div>
        </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>                                            
      </div>
    
    </div>
  </form>
    </div>
  


@endsection
