@extends('layouts.main')
@section('content')
   <!-- Page body Jyothi -->
        <div class="page-body">
            <div class="container-xl">   
              <div class="row row-cards">
                <div class="col-12">

                  <form id="editpackage" method="POST" enctype="multipart/form-data"  action="{{url('/edit_savepack')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  

                    <nav aria-label="breadcrumb">
                      <ol class="breadcrumbs">
                        <li><a href="{{url('/home')}}">Home</a></li>
                        @if($package->pac_type==1)
                        <li><a href="{{url('/manage_package')}}">Manage Regular  Package</a></li>
                        @else
                        <li><a href="{{url('/manage_package_chrysallis')}}">Manage Chrysalis Package</a></li>
                        @endif
                       
                        <li><a href="#">{{$title}}</a></li>
                      </ol>
                    </nav>

                    <div class="card-header">
                      <h4 class="card-title"></h4>Edit Package
                    </div>
                    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-6 col-xl-12">
                          <div class="row">
                            <div class="col-md-6 col-xl-6">

                              <div class="mb-3">
                              <label class="form-label">Name<sup>*</sup></label>
                                <!--<select class="form-select selecttype" id="emptype" name="emptype" >
                                  <option value="">Select Employee Type</option>
                                  <option value="2">Sales</option>
                                  <option value="3">Trainers</option>
                                  <option value="4">Process Assocites</option>
                                </select>-->
                                <input type="hidden" name="packid" value="{{$package->pac_id}}">
                               
           
            <input type="text" class="form-control" name="packname" id="packname" value="{{$package->pac_name}}" placeholder="Package name">
          </div>
          
          
          <div class="mb-3">
            <label class="form-label">Technology Name<sup>*</sup></label>
            <select class="form-select selecttech" id="technology" name="technology">
                                  <option value="">Select Technology</option>
                                  @foreach($technologylist as $technologylist)
                                  <option {{$technologylist->id==$package->tech_id?"selected":""}} value="{{$technologylist->id}}">{{$technologylist->technology}}</option>
                                  @endforeach
                                </select>
          </div>
          <input type="hidden" class="form-control" name="defaulttax" id="defaulttax" value="{{$tax}}">
        
          <div class="mb-3">
            <label class="form-label">Cost<sup>*</sup></label>
            <input type="number" class="form-control"  min=1 oninput="validity.valid||(value='');" name="packfee" id="packfees" value="{{$package->fee}}" placeholder="Fee">
          </div>
          <div class="mb-3">
            <label class="form-label">Tax {{$tax}}%<sup>*</sup></label>
            <input type="text" class="form-control" name="packtax" value="{{$package->tax}}" id="packtax"  readonly>
          </div>
          <div class="mb-3">
            <label class="form-label">Cost Total<sup>*</sup></label>
            <input type="text" class="form-control" name="packtot" id="packtot" value="{{$package->total}}" readonly>
          </div>
        </div>
        <div class="col-md-6 col-xl-6">

          <div class="mb-3">
            <label class="form-label">Course ID<sup>*</sup></label>
            <input type="text" class="form-control" name="packcourseid" id="packcourseid" value="{{$package->course_id}}" placeholder="CourseID">
          </div>
          <input type="hidden" name="pac_type" value="{{$package->pac_type}}">
          
          <div class="mb-3">
            <label class="form-label">Duration<sup>*</sup></label>
            <select class="form-select selecttech" id="duration" name="duration" >
                                  <option value="">Select Duration</option>
                                  @for($j=1;$j<=12;$j=$j+.5)
                                  <option {{$package->duration==$j?"selected":""}}  value="{{$j}}">{{$j}} Months</option>
                                @endfor  
                            </select>
          </div>
          <div class="mb-3">
            <label class="form-label">Hours<sup>*</sup></label>
            <select class="form-select selecttech" id="packhrs" name="packhrs">
                                  <option value="">Select Hours</option>
                           @for($j=1;$j<=24;$j++)
                                  <option {{$package->hours==$j?"selected":""}} value="{{$j}}">{{$j}} Hours</option>
                                @endfor
                                </select>
          </div>
          <div class="mb-3">
            <label class="form-label">Project<sup>*</sup></label>
            <input type="text" class="form-control" name="packpro" id="packpro" value="{{$package->project}}" placeholder="Project">
          </div>

          <div class="row">
            <div class="mb-3">
              <label class="form-label">Pre-requisite<sup>*</sup>
              <button type="button" name="addRequisite" id="addRequisite" class="btn btn-success">Add New Pre-requisite +</button>
              </label>
              
            </div>
          </div>
          <div id="text-box-container">
         
            @foreach($pre_req as $pre_req)
            
            <div class="prereq-group" >
            <div class="mb-3">
              <input type="text" class="form-control pre_req_input" name="pack_pre[]" id="pack_pre1" value="{{$pre_req->pre_req}}" placeholder="Pre-requisite" required>
              <button type="button" class="btn btn-danger remove-requisite">Remove</button>
              
            </div>
          </div>
         
          @endforeach
      </div>




          
        </div>

                  <div class="card-footer text-end">
                    <div class="d-flex">
                      
                      <button type="submit" class="btn btn-primary ms-auto">Submit</button>
                    </div>
                  </div>
                </form>

              
                </div>
           
            </div>

            
@endsection
