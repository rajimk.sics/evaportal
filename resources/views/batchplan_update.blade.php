@extends('layouts.main')

@section('content')

    <!-- Page body -->
    <div class="page-body">
        <div class="container-xl">

            <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">

            <div class="card">
                <div class="card-body">

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                            <li><a href="{{ url('/home') }}">Home</a></li>
                            <li><a href="{{ url('/trainerStudents') }}">Your Students</a></li>
                            <li><a href="{{ url('/batchplan_update/' . $student_id) }}">{{ ucfirst($student_name) }}
                                    - Batchplan</a></li>
                        </ol>
                    </nav>


                    <div class="card mt-4">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="card"
                                        style="border: 1px solid #ddd; border-radius: 8px; box-shadow: 0 4px 8px rgba(0, 0, 0, 0.3);">
                                        <div class="card-body">
                                            <div class="col">
                                                <h1>{{ ucfirst($student_name) }}</h1>
                                            </div>
                                            <div class="row mt-4">
                                                <div class="col">
                                                    <p>Package:</p>
                                                    <h3>{{ $package_name }}</h3>
                                                </div>
                                                <div class="col">
                                                    <p>Start Date:</p>

                                                    <h3> {{ date('d-m-Y', strtotime($start_date)) }}</h3>
                                                </div>

                                                <div class="col">
                                                    <p>Assigned Days:</p>
                                                    @if ($assigned_days)
                                                        <h3>{{ $assigned_days }}</h3>
                                                    @else
                                                        <h3>No Assigned Days Available</h3>
                                                    @endif

                                                </div>
                                            </div>

                                            @php
                                                $trainer_name = App\Helpers\CustomHelper::getTrainerNameBP(
                                                    $studentpackageid,
                                                );
                                            @endphp
                                            <div class="row">
                                                <div class="col-4">
                                                    <p>Trainer Name:</p>
                                                    <h3>{{ $trainer_name }}</h3>
                                                </div>
                                                <div class="col-4">
                                                    <p>End Date:</p>

                                                    <h3>
                                                        {{ $end_date ? date('d-m-Y', strtotime($end_date)) : '' }}
                                                    </h3>

                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="card"
                                        style="border: 1px solid #ddd; border-radius: 8px; box-shadow: 0 4px 8px rgba(0, 0, 0, 0.3);">
                                        <div class="card-body">
                                            <div class="d-flex justify-content-between mb-3">
                                                <h3>Evaluation Results</h3>
                                                <button class="btn btn-primary"
                                                    onclick="evaluationModal({{ $student_id }})">Update</button>
                                            </div>
                                            @if (count($evaluationResult) != 0)
                                                <table class="table table-bordered table-lg" style="width: 100%;">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Exam</th>
                                                            <th scope="col">Date</th>
                                                            <th scope="col" colspan="2" class="text-center">Marks</th>
                                                        </tr>
                                                        <tr>
                                                            <th></th>
                                                            <th></th>
                                                            <th scope="col">Theory</th>
                                                            <th scope="col">Viva</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($evaluationResult as $index => $exam)
                                                            <tr style="background-color: #cce5ff;">
                                                                <th scope="row">{{ $index + 1 }}</th>
                                                                <td>{{ \Carbon\Carbon::parse($exam->date)->format('d-m-Y') }}
                                                                </td>
                                                                <td>{{ $exam->theory }}</td>
                                                                <td>{{ $exam->viva }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>

                                                </table>
                                            @endif

                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>




                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>No</th>
                                    <th>Syllabus</th>
                                    <th>Percentage</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Comments</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 1; @endphp
                                @php $totalTopics = count($syllabus); @endphp
                                @foreach ($syllabus as $index => $topic)
                                    @php
                                        $batchplanTopics = App\Helpers\CustomHelper::updatedBatchplanTopics(
                                            $student_id,
                                            $topic['topic_id'],
                                        );
                                        $greatestEndDate = $batchplanTopics['end_date'] ?? null;
                                        $earliestStartDate = $batchplanTopics['start_date'] ?? null;
                                    @endphp

                                    <tr class="topic-row">
                                        <td>
                                            <input type="checkbox" class="topic-checkbox"
                                                data-topic-id="{{ $topic['topic_id'] }}"
                                                id="topicCheckbox{{ $topic['topic_id'] }}" title="Select this topic">
                                        </td>
                                        <td>{{ $i }}</td>
                                        <td><strong>
                                                <h3>{{ $topic['topic'] }}</h3>
                                            </strong></td>
                                        <td>
                                            @php
                                                $percentage =
                                                    $totalTopics > 0 ? (($index + 1) / $totalTopics) * 100 : 0;
                                                $percentage = round($percentage, 2);
                                            @endphp

                                            <strong>{{ $percentage }}%</strong>
                                        </td>
                                        <td class="topic-start-date">
                                            @if ($earliestStartDate)
                                                <strong>{{ $earliestStartDate }}</strong>
                                            @endif
                                        </td>
                                        <td class="topic-end-date">
                                            @if ($greatestEndDate)
                                                <strong>{{ $greatestEndDate }}</strong>
                                            @endif
                                        </td>

                                        <td>
                                            @if ($batchplanTopics)
                                                {{ $batchplanTopics['comments'] }}
                                            @endif
                                        </td>
                                        <td>
                                            <button class="btn btn-sm btn-info col-auto mt-1" title="Updated Days"
                                                onclick="batchplanModal({{ $student_id }}, {{ auth()->id() }}, '{{ date('d-m-Y', strtotime($start_date)) }}')">
                                                Updated Days
                                            </button>
                                        </td>
                                    </tr>

                                    @php $i++; @endphp
                                    @php $subIndex = 1; @endphp
                                    @foreach ($topic['subtopics'] as $subtopic)
                                        @php
                                            $batchplanSubtopics = App\Helpers\CustomHelper::updatedBatchplanSubtopics(
                                                $student_id,
                                                $topic['topic_id'],
                                                $subtopic['subtopic_id'],
                                            );

                                        @endphp
                                        <tr class="subtopic-row">
                                            <td>
                                                <input type="checkbox" class="subtopic-checkbox"
                                                    data-topic-id="{{ $topic['topic_id'] }}"
                                                    data-subtopic-id="{{ $subtopic['subtopic_id'] }}"title="Select this subtopic">
                                            </td>
                                            <td>{{ $index + 1 }}.{{ $subIndex }}</td>
                                            <td>{{ $subtopic['subtopic'] }}</td>
                                            <td></td>
                                            <td class="subtopic-start-date">
                                                @if ($batchplanSubtopics)
                                                    {{ $batchplanSubtopics['start_date'] }}
                                                @endif
                                            </td>
                                            <td class="subtopic-end-date">
                                                @if ($batchplanSubtopics)
                                                    {{ $batchplanSubtopics['end_date'] }}
                                                @endif
                                            </td>

                                            <td>
                                                @if ($batchplanSubtopics)
                                                    {{ $batchplanSubtopics['comments'] }}
                                                @endif
                                            </td>
                                            <td>
                                                <button class="btn btn-sm btn-info col-auto mt-1" title="Updated Days"
                                                    onclick="batchplanModal({{ $student_id }}, {{ auth()->id() }}, '{{ date('d-m-Y', strtotime($start_date)) }}')">
                                                    Updated Days
                                                </button>
                                            </td>
                                        </tr>
                                        @php $subIndex++; @endphp
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>




                    </div>
                </div>
            </div>
        </div>


    @endsection


    <!-- Evaluation Result Modal  -->
    <div class="modal fade" id="evaluationModal" tabindex="-1" aria-labelledby="evaluationModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="evaluationModalLabel">Evaluation Results</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="evaluationForm" method="POST" enctype="multipart/form-data"
                        action="{{ url('/saveEvaluationResults') }}">
                        <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">
                        <input type="hidden" name="student_id" id="student_id" value="{{ $student_id }}">

                        <div id="examSections">
                            @if ($evaluationResult->count() > 0)
                                @foreach ($evaluationResult as $exam)
                                    <div class="row mb-3 exam-section" id="exam_{{ $exam->exam_number }}">
                                        <h5 class="col-12 text-center">{{ $exam->exam_number }}th Exam</h5>
                                        <input type="hidden" name="exam_data[{{ $exam->exam_number }}][exam_number]"
                                            value="{{ $exam->exam_number }}">
                                        <div class="col-4">
                                            <label>Date</label>
                                            <input type="date" name="exam_data[{{ $exam->exam_number }}][date]"
                                                class="form-control" value="{{ $exam->date }}">
                                        </div>
                                        <div class="col-4">
                                            <label>Theory Mark</label>
                                            <input type="number" name="exam_data[{{ $exam->exam_number }}][theory]"
                                                class="form-control" value="{{ $exam->theory }}"
                                                placeholder="Enter theory mark">
                                        </div>
                                        <div class="col-4">
                                            <label>Viva Mark</label>
                                            <input type="number" name="exam_data[{{ $exam->exam_number }}][viva]"
                                                class="form-control" value="{{ $exam->viva }}"
                                                placeholder="Enter viva mark">
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <!-- If no results, display default exam section -->
                                <div class="row mb-3 exam-section" id="exam_1">
                                    <h5 class="col-12 text-center">1st Exam</h5>
                                    <input type="hidden" name="exam_data[1][exam_number]" value="1">
                                    <div class="col-4">
                                        <label>Date</label>
                                        <input type="date" name="exam_data[1][date]" class="form-control">
                                    </div>
                                    <div class="col-4">
                                        <label>Theory Mark</label>
                                        <input type="number" name="exam_data[1][theory]" class="form-control"
                                            placeholder="Enter theory mark">
                                    </div>
                                    <div class="col-4">
                                        <label>Viva Mark</label>
                                        <input type="number" name="exam_data[1][viva]" class="form-control"
                                            placeholder="Enter viva mark">
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div id="examsection2"></div>


                        <div class=" mb-3 text-center">
                            <button type="button" id="addExam" class="btn btn-success">+ Add Exam</button>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" id="saveEvaluationBtn">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{-- SAVE Batchplan  --}}
    <div class="modal fade" id="batchPlanModal" tabindex="-1" role="dialog" aria-labelledby="batchPlanModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="batchPlanModalLabel">Update Batch Plan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="batchPlanForm" action="{{ url('/saveBatchPlan') }}" method="POST"
                    enctype="multipart/form-data">
                    <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">
                    <input type="hidden" id="topic_ids" name="topic_ids">
                    <input type="hidden" id="subtopic_ids" name="subtopic_ids">
                    <input type="hidden" id="studId" name="studId">
                    <input type="hidden" id="trainer_id" name="trainer_id">

                    <div class="modal-body">
                        <div id="t
                        opicCheckboxes"></div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="start_date">Start Date*</label>
                                    <input type="text" data-zdp_readonly_element="true" class="form-control"
                                        id="Bp_start_date"name="Bp_start_date">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="end_date">End Date*</label>
                                    <input type="text" data-zdp_readonly_element="true" class="form-control"
                                        id="Bp_end_date" name="Bp_end_date">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="comments">Comments*</label>
                            <textarea class="form-control" id="comments" name="comments"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
