@extends('layouts.main')

@section('content')

    <!-- Page body -->
    <div class="page-body">
        <div class="container-xl">

            @if (Session::has('message'))
            <div class="alert alert-success">
                <strong>Success:</strong> {{ Session::get('message') }}
            </div>
        @endif
            @if (Session::has('duplicates'))
            <div class="alert alert-warning">
                <p><strong>Duplicate values found:</strong></p>
                <ul>
                    @foreach (Session::get('duplicates') as $duplicate)
                    <li>
                        <strong>Duplicate Value:</strong> {{ $duplicate['dupli'] }}
                        <strong>Technology:</strong> {{ $duplicate['technology'] }}
                    </li>
                @endforeach
                </ul>
            </div>
        @endif

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

            <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">

            <div class="card">
                <div class="card-body">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                            <li><a href="{{ url('/home') }}">Home - Mentor</a></li>
                            <li><a href="{{ url('/uploadSylabus') }}">{{ $title }}</a></li>
                        </ol>
                    </nav>
                    
                   

                   





                    <div class="card">
                        <div class="card-header">
                            <h2>{{ $title }}</h2>
                        </div>
                        <div class="card-body">
                            <form id="csvupload" method="POST" enctype="multipart/form-data"
                                action="{{ url('/UploadSyllabusByTechnology') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="my-2 col-5">
                                    <label for="technology" class="mb-1"><b>Select Technology</b></label>
                                    <select id="technology" name="technology[]" class="form-select selecttech" multiple>
                                        <option value="" disabled>Select Technology</option>
                                        @foreach ($technologies as $technology)
                                            <option value="{{ $technology->id }}">{{ $technology->technology }}</option>
                                        @endforeach
                                    </select>
                                    <div class="form-text mt-1">Choose one or more technologies</div>
                                </div>

                                <div class="mb-4 col-5">
                                    <label for="csv" class="form-label">Upload CSV File</label>
                                    <input type="file" class="form-control" name="csv_file" id="csv" required>
                                    <div class="form-text mt-1">
                                        <a href="{{ url('/public/uploads/excel/syllabus.csv') }}" download="syllabus"
                                            style="color: black;">
                                            Download sample CSV format
                                        </a>
                                    </div>
                                </div>

                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-primary">Upload</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    @if (Session::has('message'))
        <script>
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        </script>
    @endif


@endsection
