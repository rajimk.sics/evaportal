@extends('layouts.main')
@section('content')
   <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">   
              <div class="row row-cards">
                <div class="col-12">

                  <form id="editcompany" method="POST" enctype="multipart/form-data"  action="{{url('/update_company')}}">
                    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">


                    
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumbs">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="#">{{$title}}</a></li>
                  </ol>
                </nav>
                    <div class="card-header">
                      <h2 class="fs-title">{{$title}}</h2>
                    </div>

                   
                    <div class="card-body">
                      <div class="">
                          <div class="row">
                            <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-label">Company Name<sup>*</sup></label>
                                <input type="hidden" class="form-control" name="id" id="id" value="{{$compdetails->id}}">
                                
                                <input type="text" class="form-control" name="name" id="name" value="{{$compdetails->name}}" placeholder="Name" >
                              </div>
                            </div>
                            <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-label">Contact Person Name<sup>*</sup></label>
                                <input type="text" class="form-control" name="cpname" id="cpname" value="{{$compdetails->contact_name}}" placeholder="Contact Person Name"  >
                              </div>
                            </div>
                            <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-label">Contact  Number<sup>*</sup></label>
                                <input type="text" class="form-control contactnum" name="cnumber" minlength="10"  maxlength="13" id="cnumber1" value="{{$compdetails->contact_number}}" placeholder="Contact Number" readonly>
                              </div>
                            </div>
                            <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-label">Email<sup>*</sup></label>
                                <input type="email"  class="form-control contactnum" name="cemail" id="cemail1" value="{{$compdetails->email}}" placeholder="Email" readonly>
                              </div>
                            </div>
                           
                            <div class="col-lg-6">
                              <div class="form-group">
                                  <label class="form-label">Company Logo<sup>*</sup></label>
                                  <input type="file" class="form-control" accept="image/*" name="file" id="file" >
                                </div>
                            </div>

                          @if($compdetails->company_logo !='')
                            <div class="mb-3">
                              <label class="form-label">Current Logo</label>
                             <img id="previewHeader" src="{{url('public/uploads/logo/'.$compdetails->company_logo)}}" alt="Preview" style=" max-width: 150x; max-height: 100px;">
                         </div>
                         @endif
                        </div>
                      </div>


                    </div>
                  </div>
                  <div class="card-footer text-end">
                    <div class="d-flex">
                      
                      <button type="submit" class="btn btn-primary ms-auto">Submit</button>
                    </div>
                  </div>
                </form>

              
                </div>
           
            </div>

            <script>
        
             
              </script>
            
            <script>
            @if (Session::has('errormessage'))
            swal({
                title: "",
                text: "{{ Session::get('errormessage') }}",
                type: "sucess",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif 
      </script>         
@endsection
