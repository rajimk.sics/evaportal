@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">

              <input type="hidden" name="logname" id="logname" value="{{ Auth::user()->name }}">
                   
              <div class="card">
                <div class="card-body">

                     
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumbs">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="#">{{$title}}</a></li>
                  </ol>
                </nav>
                   
                    <h4 class="card-title">{{$title}}</h4>
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
               
                <th>Photo</th>
                <th>Requested Date</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Type</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
<?php $i=1;?>
            @foreach($gatelist as $gatelist)
            <tr>
                <td>{{$i}}</td>
               
                <td>  <img src="{{url('public/uploads/photo/'.$gatelist->photo)}}" width="50px" height="50px" ></td>

                <td>
                  <span >{{date("d-m-Y", strtotime($gatelist->requested_date))}}</span>
                 </td>
                <td>
                 <span id="sdate_{{$gatelist->id}}"> {{date("d-m-Y", strtotime($gatelist->start_date))}}</span>
                  
                 </td>
                <td>
                  <span id="edate_{{$gatelist->id}}">{{date("d-m-Y", strtotime($gatelist->end_date))}}</span>
                 </td>
                <td>

                  @php
                  $type_name=App\Helpers\CustomHelper::typename($gatelist->type,$gatelist->type_id); 
                  @endphp
                  @if($gatelist->type==1)
                  Regular-{{ $type_name['name']}}
                  @endif
                  @if($gatelist->type==2)
                  Chrysalis-{{ $type_name['name']}}
                  @endif
                  @if($gatelist->type==3)
                  Event-{{ $type_name['name']}}
                  @endif
                </td>
                <td>

                  @if($gatelist->status==0)  
                  
                  <span style="color:orange" id="pend_{{$gatelist->id}}">Pending</span>
                  <span style="color:green;display:none;" id="act_{{$gatelist->id}}">Approved</span>
                  <span style="color:red;display:none;" id="deact_{{$gatelist->id}}" >Rejected</span>  
                  
                  @elseif($gatelist->status==1)
                  <span style="color:green" id="act_{{$gatelist->id}}">Approved</span>
                  
                  @elseif($gatelist->status==2)
                  <span style="color:red" id="deact_{{$gatelist->id}}">Rejected</span></br>
                  
                  <span> Reason :{{$gatelist->reason}}</span>
                  @elseif($gatelist->status==3)

                  <span style="color:green"  id="status_{{$gatelist->id}}">Returned</span>

                  @else
                  <span style="color:green"  id="status_{{$gatelist->id}}">Reissued</span>
                  @endif
                  <span id="status_{{$gatelist->id}}" style="color:green;display:none;">

                  </span>


                </td>
                

               </tr>
            <?php $i++;?>
            @endforeach
           
        </tbody>
      
    </table>




    <form id="changedateform" method="POST" enctype="multipart/form-data"  action="{{url('/changedateform')}}">
      <input type="hidden" name="_token" id="token_eva" value="{{ csrf_token() }}">
      <input type="hidden" name="pass_id" id="pass_id" value="">
    <div class="modal modal-blur fade" id="modal-changedate" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Change Date</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="mb-3">
            <label class="form-label">Start Date<sup>*</sup></label>
            <input type="text" class="form-control" data-zdp_readonly_element="true" name="cdate1" id="cdate1" >
          </div>
          <div class="mb-3">
            <label class="form-label">End Date<sup>*</sup></label>
            <input type="text" class="form-control" data-zdp_readonly_element="true" name="cdate2" id="cdate2" >
          </div>
        </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>                                            
      </div>
    
    </div>
    </div>
    </form>

    


@endsection
