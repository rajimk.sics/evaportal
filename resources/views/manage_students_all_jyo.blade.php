@extends('layouts.main')
@section('content')


<style>

  .formcontents{
    display: flex;
    flex-direction: column;
  }
  label.error{
    color: red;
    order: 3;
  }

  </style>
   <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">   
              <div class="row row-cards">
                
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-header">
                     
                    </div>
                    <div class="card-body">

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>

                          <li><a href="#">{{$title}}</a></li>
                        </ol>
                      </nav>

                   
                    <h4 class="card-title"> {{$title}}</h4>


             <form id="searchchrys_form" method="GET" enctype="multipart/form-data"  action="{{url('/all_students')}}">     
              <input type="hidden" name="_token" id="token"  value="{{ csrf_token() }}">


                    <div class="row">


                      <div class="col">
                        <div class="form-group formcontents">
                        <label>Package Type</label>
                           <select class="form-select packtype" id="packtype" name="packtype" >
                              <option value="">Select Package Type</option>
                              <option {{1==$packtype?"selected":""}} value="1">Regular</option>
                              <option {{2==$packtype?"selected":""}} value="2">Chrysalis</option>
                               
                          </select>

                        </div>
                      </div>


                      <div class="col">
                        <div class="form-group formcontents">
                        <label>Package</label>
                           <select class="form-select packname" id="packname" name="packname" >
                              <option value="">Select Package</option>
                                @foreach($package as $package)
                                  <option {{$package->pac_id==$packname?"selected":""}} value="{{$package->pac_id}}">{{$package->pac_name}}</option>
                                @endforeach
                                  </select>

                        </div>
                      </div>
                      <div class="col">
                        <div class="form-group formcontents">
                            <label for="techname">Technology</label>
                            <select name="techname" id="techname" class="form-select techno">
                              @if(isset($technologies))
                              @foreach ($technologies as $technology) 
                              <option value="{{ $technology->id }}" {{ request()->techname == $technology->id ? 'selected' : '' }}>
                                  {{ $technology->technology }}
                              </option>
                          @endforeach
                          @endif
                            </select>
                        </div>
                    </div>

                    <!-- College Filter -->
                    <div class="col">
                        <div class="form-group formcontents">
                            <label for="collegename">College Name</label>
                            <select name="collegename" id="collegename" class="form-select college">
                                <option value="">Select College</option>
                                @foreach ($colleges as $college)
                                    <option value="{{ $college->id }}"
                                        {{ request()->collegename == $college->id ? 'selected' : '' }}>
                                        {{ $college->college }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                      <div class="col">  
                        <div class="form-group formcontents">
                       <label>Start date<sup>*</sup></label>
                        <input type="text" class="form-control" data-zdp_readonly_element="true" name="start_studate1" value="{{$start_date1}}" id="start_studate1" >
                        </div>
                      </div>
                      <div class="col">
                        <div class="form-group formcontents">
                      <label>End Date<sup>*</sup></label>
                          <input type="text" class="form-control" data-zdp_readonly_element="true" name="end_studate1" value="{{$end_date1}}" id="end_studate1" >
                        </div>
                        </div>
                  
                <div class="col d-flex align-items-center">
                  <div class="form-group formcontents mb-0 flex-row">
                  <button type="submit" class="btn btn-primary me-2">Submit</button>
                  <a href="{{url('/all_students')}}" class="btn btn-primary"> Clear All</a>
                  </div>
                  
                </div>
               
</div>
    </form>
                   

    <div class="table-responsive">
                       
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                              <thead>
                                  <tr>
                                      <th>No</th>
                                      <th>Student Name</th>
                                      <th>Email Id</th>
                                      <th>SICS Reg Number</th>

                                      @if(Auth::user()->role !=2)
                                      <th>Contact Number</th>
                                      @endif

                                      <th>Package Type</th>
                                      <th>Package</th>
                                      <th>Joining Date</th>

                                      @if(Auth::user()->role !=2)

                                      <th>Closed By</th>

                                      @endif
                                      <th>Start Date</th>
                                      <th>End Date</th>
                                      <th>Status</th>
                                      @if(Auth::user()->role ==2 || Auth::user()->role ==9 || Auth::user()->role ==4)
                                      <th>Action</th>
                                      @endif
                                     
                                  </tr>
                              </thead>
                              <tbody>
                                <?php $i=1;?>
                                @foreach($studlist  as $studlist)
                                <tr>
                                  <td>{{$i}}</td>
                                  <td>{{ucfirst($studlist->name)}}</td>
                                  <td>{{$studlist->email}}</td>
                                  
                                  <td>SIAC{{$studlist->student_admissionid}}</td>  

                                  @if(Auth::user()->role !=2)
                                 
                                  <td>{{$studlist->phone}}</td>
                                  @endif
                           
                                  
                                  <td>

                                    @if($studlist->pac_type==1)

                                    Regular
                                  
                                    @else
                                    Chrysalis
                
                                    @endif

                                  </td>

                                  @php
                                  $techname=App\Helpers\CustomHelper::tech($studlist->tech_id); 
                                  @endphp
                                  <td>{{$studlist->pac_name}}</td>
                                  <td>

                                    {{date("d-m-Y", strtotime($studlist->joining_date))}}
                                   
                                   </td>


                                   @if(Auth::user()->role !=2)

                                        @php
                                        $name=App\Helpers\CustomHelper::uname($studlist->sales_id); 

                                        $displayName = $name && isset($name['name']) ? ucfirst($name['name']) : '';
                                        @endphp

                                      <td>{{ucfirst($displayName)}}</td>

                                  @endif
                                  <td>

                                    @if($studlist->pac_type==1)

                                    {{date("d-m-Y", strtotime($studlist->due_date_first))}}
                                                                
                                    @else
                                    {{$studlist->due_date_first}}
                
                                    @endif
                                    
                                    
                                   
                                  
                                  </td>
                                  <td>{{$studlist->enddate}}</td>

                                  @if(Auth::user()->role ==2)
                                  <td>
                                    <a href="{{url('/select_package/'.$studlist->id.'/'.'all')}}"><button type="button" class="btn btn-green btn-sm text-white" fdprocessedid="wf07gv">
                                      Select Regular Package
                                    </button>
                                   </a>
                      
                                   <a href="{{url('/select_chrysalis_package/'.$studlist->id.'/'.'all')}}"><button type="button" class="btn btn-green btn-sm text-white" fdprocessedid="wf07gv">
                                      Select Chrysalis Package
                                    </button>
                                 </a>


                                  </td>
                                  @endif
                                  

                                  <td>
                                    

                                    {!!App\Helpers\CustomHelper::paymentcompleted($studlist->id,$studlist->package_id,$studlist->pac_type,$studlist->total)!!}
                                    
                                  </td>
                                  @if(Auth::user()->role ==4)
                                  <td>
                                      <span class="m-5">
                                          <a href="">
                                              <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                  height="24" viewBox="0 0 24 24" fill="none"
                                                  stroke="currentColor" stroke-width="2"
                                                  stroke-linecap="round" stroke-linejoin="round"
                                                  class="icon icon-tabler icons-tabler-outline icon-tabler-mail">
                                                  <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                  <path
                                                      d="M3 7a2 2 0 0 1 2 -2h14a2 2 0 0 1 2 2v10a2 2 0 0 1 -2 2h-14a2 2 0 0 1 -2 -2v-10z" />
                                                  <path d="M3 7l9 6l9 -6" />
                                              </svg>
                                          </a>
                                          <div class="row" style="display: flex; align-items: center;">
                                              <button class="btn btn-grey assignTrainer" id="assignTrainer"
                                                  onclick="fetchTrainers({{ $studlist->package_id }},{{ $studlist->id }})">Assign
                                                  Trainer</button>
                                          </div>

                                      </span>
                                  </td>

                      @endif


                                  @if(Auth::user()->role ==9)
                                  <td>
                                 <button type="button" class="btn btn-primary" fdprocessedid="wf07gv" onclick="schedule_interview('{{$studlist->id}}')">
                                      Schedule Interview
                                    </button>
                                    </td>
                                  @endif



                                </tr>
                                <?php
                                $i++;

                                ?>
                                @endforeach

                              </tbody>
                            
                          </table>
                          </div>
                        </div>
                        <div class="tab-pane fade active show" id="tabs-home-8" role="tabpanel">
                        
                          <div>
                            

                          </div>
                        </div>
                        <div class="tab-pane fade" id="tabs-profile-8" role="tabpanel">
                         
                          <div>

                           
                          </div>
                        </div>
                        <div class="tab-pane fade" id="tabs-activity-8" role="tabpanel">
                         
                          <div>

                          </div>
                        </div>

                    </div>
                  </div>
                </div>
                
           
            </div>
            
            </div>
                <!-- Modal Structure -->
    <form id="assignTrainerForm" method="POST" enctype="multipart/form-data" action="{{ url('/assignTrainer') }}">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div id="hiddenTrainers"></div>

      <div class="modal" id="assignTrainerModel">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header ">
                      <h5 class="modal-title">Assign Trainer</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                      <label for="trainerName" class="w-100 form-label">
                          <h4>Trainer Name</h4>
                      </label>
                      <select name="SelectTrainer[]" class="w-100 rounded form-select mb-1 selecttrainer"
                          id="trainerName" multiple="multiple">

                      </select>
                      <label for="daysDropdown" class="w-100 form-label">
                          <h4>Select Days</h4>
                      </label>
                      <select name="SelectDays[]" class="w-100 rounded form-select mb-1 selectdays" id="daysDropdown"
                          multiple="multiple">
                          <option value="Sun" name="days[]" id="sunday">Sunday</option>
                          <option value="Mon" name="days[]" id="monday">Monday</option>
                          <option value="Tue" name="days[]" id="tuesday">Tuesday</option>
                          <option value="Wed" name="days[]" id="wednesday">Wednesday</option>
                          <option value="Thu" name="days[]" id="thursday">Thursday</option>
                          <option value="Fri" name="days[]" id="friday">Friday</option>
                          <option value="Sat" name="days[]" id="saturday">Saturday</option>

                      </select>

                      <div class="row mt-4">
                          <div class="col-md-6">
                              <label for="startTime">Start Time</label>
                              <input type="time" class="form-control" id="startTime" name="startTime"
                                  placeholder="00:00 AM">
                          </div>
                          <div class="col-md-6">
                              <label for="endTime">End Time</label>
                              <input type="time" class="form-control" id="endTime" name="endTime"
                                  placeholder="00:00 AM">
                          </div>
                      </div>

                      <div class="modal-footer">
                          <button type="submit" class="btn btn-primary">Save changes</button>
                      </div>
                  </div>
              </div>
          </div>
  </form>



                    <div class="modal modal-blur fade" id="modal-schedule" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title">Schedule Interview</h5>
                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form id="scheduleform" method="POST" enctype="multipart/form-data"  action="{{url('/save_schedule')}}">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
               
                        <div class="modal-body">
                          <div class="mb-3">
                          <input type="hidden" class="form-control"  name="stud_id" id="stud_id" value="">
                          <label class="form-label">Date<sup>*</sup></label>
                          <input type="text" class="form-control" data-zdp_readonly_element="true" name="schedule_date" id="schedule_date" >
                          </div>
                          <div class="mb-3">
                          <input type="radio" id="online" name="interviewMode"  value="online">
                           <label for="online">Online</label>
                            <input type="radio" id="offline" name="interviewMode" value="offline">
                             <label for="offline">Offline</label><br>
                             <label for="interviewMode" style="display:none" class="error">This field is required.</label>
                            </div>
                        </div>
                      <div class="modal-footer">
                        <button type="submit"  class="btn btn-primary btn-interview">Send Mail</button>
                      </div>                                            
                      </div>
                    
                    </div>
                    </div>
                    </form>
  
@endsection


