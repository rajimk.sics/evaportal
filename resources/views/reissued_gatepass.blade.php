@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">

              <input type="hidden" name="logname" id="logname" value="{{ Auth::user()->name }}">
                   
              <div class="card">
                <div class="card-body">

                     
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumbs">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="#">{{$title}}</a></li>
                  </ol>
                </nav>
                   
                    <h4 class="card-title">{{$title}}</h4>


                    <div class="payment-grd">
                      <form id="searchchrys_form" method="GET" enctype="multipart/form-data" action="{{ url('/reissued_gatepass') }}">
                          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
      
                        
      
                          <div class="row">
                            <!-- Year Dropdown -->
                            <div class="col-lg-4 formcontents">
                                <label for="year">Select Year<sup>*</sup></label>
                                <select class="form-select" name="year" id="year">
                                    
                                    @foreach($years as $year)
                                    <option value="{{ $year }}" {{ $year == $currentYear ? 'selected' : '' }}>{{ $year }}</option>
                                    @endforeach
                                </select>
                            </div>
    
                            <!-- Month Dropdown -->
                            <div class="col-lg-6 formcontents">
                                <label for="month">Select Month<sup>*</sup></label>
                                <select class="form-select" name="month" id="month">
                                   
                                    @foreach($months as $key => $month)
                                    <option value="{{ $key }}" {{ $key == $currentMonth ? 'selected' : '' }}>{{ $month }}</option>
                                    @endforeach
                                </select>
                            </div>
    
                            <div class="col-lg-2 formcontents">
                                <button type="submit" class="btn btn-primary" style="margin-top: 26px;">Submit</button>
                          </div>
    
                        </div>
    
                        <div class="col formcontents"> 
                          <a href="{{url('/approved_gatepass/')}}" class="btn btn-primary"  style="margin-top: 26px;"> View Current Month</a>
                          </div>
                    </form>
                </div>
      
                         
                      </form>
                  </div>


                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Student Name</th>
                <th>Photo</th>
                <th>Reissued Date</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Type</th>
                <th>Status</th>
                <th>Action</th>
              
              
            </tr>
        </thead>
        <tbody>
        <?php $i=1;?>
            @foreach($gatelist as $gatelist)
            <tr id="row_{{$gatelist->id}}">
                <td>{{$i}}</td>
                <td>{{$gatelist->name}}</td>
                <td>  <img src="{{url('public/uploads/photo/'.$gatelist->photo)}}" width="50px" height="50px" ></td>
                <td>
                  {{date("d-m-Y", strtotime($gatelist->requested_date))}}
                
                </td>
                <td>
                 <span id="sdate_{{$gatelist->id}}"> {{date("d-m-Y", strtotime($gatelist->start_date))}}</span>
                  
                 </td>
                <td>
                  <span id="edate_{{$gatelist->id}}">{{date("d-m-Y", strtotime($gatelist->end_date))}}</span>
                 </td>
                  <td>

                  @php
                  $type_name=App\Helpers\CustomHelper::typename($gatelist->type,$gatelist->type_id); 
                  @endphp
                  @if($gatelist->type==1)
                  Regular-{{ $type_name['name']}}
                  @endif
                  @if($gatelist->type==2)
                  Chrysalis-{{ $type_name['name']}}
                  @endif
                  @if($gatelist->type==3)
                  Event-{{ $type_name['name']}}
                  @endif
                </td>
             
                <td>

                  @if($gatelist->status==4)
                  <span style="color:green"  id="status_{{$gatelist->id}}">Reissued</span>
                  @endif
                  <span id="status_{{$gatelist->id}}" style="color:green;display:none;">
                  </span>
                </td>
                <td>

                  @if(Auth::user()->role =='4' || Auth::user()->role =='8' )
                  <span id="downspan_{{$gatelist->id}}"><a href="{{url('/gatepass_download/'.$gatelist->id)}}"  id="down_{{$gatelist->id}}" class="download-linkpass">Download</a></span></br>
                  @endif


                  

                  @php
                    $name=App\Helpers\CustomHelper::uname($gatelist->reissued_by); 
                  @endphp
                  <span>Reissued By {{$name['name']}}</span></br>
               
                </td>
               </tr>
            <?php $i++;?>
            @endforeach
           
        </tbody>
      
    </table>



@endsection
