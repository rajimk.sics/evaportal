@extends('layouts.main')
<link rel="stylesheet" href="{{url('public/assets/css/style.css')}}">
@section('content')


<style>

    .formcontents{
      display: flex;
      flex-direction: column;
    }
    label.error{
      color: red;
      order: 3;
    }
  
    </style>



   <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">   
            <div class="container-fluid">
                <input type="hidden" id="token_eva" name="_tokens" value="{{ csrf_token() }}">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumbs">
                      <li><a href="{{url('/home')}}">Home</a></li>
                      @if($type=='contact')
                      <li><a href="{{url('/closed_contacts')}}">Closed Contacts</a></li>
                      @endif
                      @if($type=='talento')
                      <li><a href="{{url('/manage_talento_registration')}}">Talento registration</a></li>
                      @endif
                      @if($type=='all')
                      <li><a href="{{url('/all_students')}}">All Students</a></li>
                      @endif
                      <li><a href="#">{{$title}}</a></li>
                    </ol>
                  </nav>
  <div class="row justify-content-center">
      <div class="col-lg-12 p-0 mt-0 mb-2">
          <div class="card p-3 mt-0 mb-3">
              <h2 id="heading">Package Details</h2>
              <p class="text-center" >Fill all form field to go to next step</p>
              <div class="progress">
                      <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
              <form id="msform"  class="msform msformpack" enctype="multipart/form-data"  action="{{url('/pack_student')}}" onsubmit="return false;" method="POST" >
                    
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                 <div>
                      <!-- progressbar -->
                  <ul id="progressbar">
                      <li class="active" id="account"><strong>Package  Details</strong></li>
                      <li id="personal"><strong>Fees  Split Up</strong></li>
                                 
                  </ul>
                   <br> <!-- fieldsets -->
                  </div>
 
                  <fieldset>
                      <div class="form-card">
                          <div class="row">
                              <div class="col-9">
                                
               
                                  <h2 class="fs-title">{{$title}}</h2>
                              </div>
                             
                          </div>
                          <div class="row">

                            <div class="col-lg-6 formcontents">
                                <div class="form-group">
                                <label class="form-label">Joining Date<sup>*</sup></label>
                                <input type="text" style="width:auto"  data-zdp_readonly_element="true" name="oldpay_date" id="oldpay_date" class="form-control" value="{{date('d-m-Y')}}">
                                </div>
                            </div>

                            <input type="hidden" name="prevval" id="prevval" value="0">
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>Select Package<sup>*</sup></label>
                                      <select class="form-select packname" id="packname" name="packname" >
                                        <option value="">Select Package</option>
                                        @foreach($package as $package)
                                        <option value="{{$package->pac_id}}">{{$package->pac_name}}</option>
                                        @endforeach
                                      </select>
                                        @if($type=='contact')

                                                                         
                                        <input type="hidden" name="contactid" id="contactid" value="{{$contactid}}">
                                    
                                        @endif

                                        @if($type=='talento')

                                                                         
                                        <input type="hidden" name="talentoid" id="talentoid" value="{{$talentoid}}">
                                    
                                        @endif
                                  
                                  
                                  
                                        <input type="hidden" name="closed_contact" id="closed_contact" value="{{$closed_date}}">
                                  <input type="hidden" name="student_id" id="student_id" value="{{$student_id}}">
                                  <input type="hidden" name="type" id="type" value="{{$type}}">
                         
                                </div>
                              </div>
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                        <label>Fees</label> 
                                      <input type="text" class="form-control" readonly name="packfee" id="packfee" placeholder="Fee">
                                  </div>
                              </div>
                            
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                    <label>Tax 18%</label>
                                    <input type="text" class="form-control" readonly name="packtax" id="packtax"  readonly>
                                  </div>
                              </div>

                              <div class="col-lg-6 formcontents">
                                <div class="form-group">
                                    <label>Actual Fees</label>
                                    <input type="text" class="form-control" readonly name="packtot" id="packtot" readonly>
                                </div>
                            </div>

                            <div class="col-lg-12 formcontents" id="reductiondiv" style="display: none">
                                <!-- <div class="form-group">
                                    <input class="form-check-input" type="checkbox" id="reductioncheck" name="reductioncheck" value="1">
                                    <span class="form-check-label">Any Reduction Offered</span>
                                </div> -->
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="reductioncheck" name="reductioncheck" value="1">
                                    <label class="form-check-label" for="flexCheckDefault">
                                    Any Reduction Offered
                                    </label>
                                    </div>
                            </div>


                            <div class="col-lg-6 formcontents" id="reductionvaldiv" style="display: none">
                                <div class="form-group">
                                    <label>Reduction Amount<sup>*</sup></label>
                                    <input type="number"  min=1 oninput="validity.valid||(value='');"  class="form-control" name="reduction_amount"  id="reduction_amount" >

                                    <input type="hidden" name="max_range" id="max_range" value="">
                                    <span id="reduction_amount_span" style="color:red"></span>
                                </div>
                            </div>
                            <div class="col-lg-6 formcontents"  id="reducedfees_div" style="display: none">
                                <div class="form-group">
                                    <label>Reduced Fees</label>
                                    <input type="text" class="form-control"  name="reducedfees" id="reducedfees" readonly>
                                </div>
                            </div>
                            <div class="col-lg-6 formcontents" id="redtaxdiv" style="display: none">
                                <div class="form-group">
                                    <label> Reduced Tax</label>
                                    <input type="text" class="form-control"  name="reducedtax" id="reducedtax" readonly>
                                </div>
                            </div>
                            <div class="col-lg-6 formcontents" id="afterreduction_div" style="display: none">
                                <div class="form-group">
                                    <label>Total Fees After Reduction</label>
                                    <input type="text" class="form-control"  name="after_reduction" id="after_reduction" readonly>
                                </div>
                            </div>
                            <div class="col-lg-6 formcontents" id="ineffect_div" style="display: none">
                                <div class="form-group">
                                    <label>In-Effect Total Reduction Offered</label>
                                    <input type="text" class="form-control"  name="ineffect_offered" id="ineffect_offered" readonly>
                                </div>
                            </div> 

                            <input type="hidden" class="form-control"  name="finalamount" id="finalamount">
                            
                            <div class="col-lg-6 formcontents" >
                                <div class="form-group">
                                    <label>Due date in completing first installment<sup>*</sup> </label>
                                    <input type="text" style="width:auto"  data-zdp_readonly_element="true" name="inst_red" id="inst_red" class="form-control">
        
                                </div>
                            </div> 

                          </div>
                     
                      </div> <input type="button" name="next" class="next action-button tab1" id="nextBtn" value="Next" />
                  </fieldset>

                  <fieldset>
                      <div class="form-card">
                          <div class="row">
                              <div class="col-9">
                                  <h2 class="fs-title">Fees Splitup</h2>
                              </div>
                              
                          </div> 
                           <div class="row">

                            <table class="table" id="splitup">
                                <thead>
                                  <tr>
                                    <th scope="col">Installment No</th>
                                    <th scope="col">Fees</th>
                                    <th scope="col">Due Date</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  
                                </tbody>
                              </table>

                              <div id="splituphidden">

                              </div>

                              <div class="col-lg-6 formcontents" >
                                <div class="form-group">
                                    
                                    <label>Payment Method<sup>*</sup></label>
                                    <div class="d-flex align-items-center me-2">
                                        <input class="me-2" type="radio" id="cash" name="payMethod_bank"  value="cash" checked>
                                         <label>Cash</label>
                                   </div>
                                   <div class="d-flex align-items-center me-2">
                                    <input class="me-2" type="radio" id="banktransfer" name="payMethod_bank"  value="bank">
                                    <label>BANK</label>
                                   </div>
                                  
                                </div>
                            </div>
                            <div class="col-lg-6 formcontents">
                                <div class="form-group">
                                    
                                    <label class="form-label">Payment Date<sup>*</sup></label>
                                    <input type="text" style="width:auto"  data-zdp_readonly_element="true" name="paydate" id="paydate" class="form-control" value="{{date('d-m-Y')}}">
                                  
                                </div>
                            </div>
                            <div class="col-lg-6 bankdetails formcontents" style="display:none" >
                                <div class="form-group">
                                    
                                    <label class="form-label">Transaction Id<sup>*</sup></label>
                                    <input type="text" class="form-control" name="trans_id" id="trans_id">
                                  
                                </div>
                            </div>
                            <div class="col-lg-6 bankdetails formcontents" style="display:none" >
                                <div class="form-group">
                                    
                                    <label class="form-label">Payment Screenshot<sup>*</sup></label>
                                    <input type="file" class="form-control" name="package_reciept" id="package_reciept">
                                  
                                </div>
                            </div>

                              <div class="col-lg-6 formcontents" >
                                <div class="form-group">
                                    <label>Registration Fees<sup>*</sup></label>
                                    <input  min=1 oninput="validity.valid||(value='');" type="number" class="form-control"  name="regfees" id="regfees" >
                                    <span style="color:red" id="regfees_hidden_span"></span>

                                    <input  type="hidden" class="form-control"  name="regfees_hidden" id="regfees_hidden" >
 
                                </div>
                            </div> 

                            <div class="col-lg-6 formcontents">
                                <div class="form-group">
                                    <label class="">Reference</label>
                                    <select class="form-select selecttype" id="referenece" name="referenece" >
                                      <option value="">Select Reference</option>
                                      <option value="1">Marketing</option>
                                      <option value="2">Employees</option>
                                      <option value="3">Other</option>
                                     
                                    </select>
                                  </div>
                                </div>
                                <div class="col-lg-6 formcontents" id="ref_list" style="display:none;">
                                    <div class="form-group">
                                        <label class="">Reference List</label>
                                        <select class="form-select selecttype" id="referenece_list" name="referenece_list" >
                                          
                                         
                                        </select>
                                      </div>
                                    </div>
                          </div>
                         
                          
                      </div>  <input type="submit" name="next" class="sub action-button" id="nextBtn" value="Submit" /><input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                  </fieldset>

                 
                
              </form>
          </div>
      </div>
  </div>
</div>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<script src="{{url('public/assets/js/scriptpackage.js')}}"></script>

<script>
$(document).ready(function(){

    var closed_date ='<?php echo $closed_date ?>';
    var default_finstall ='<?php echo $default_finstall ?>';
    $('#inst_red').Zebra_DatePicker({
        format: 'Y-m-d',
        direction: [closed_date,default_finstall]   
    });
});

</script>
            
@endsection
