@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">
                   
              <div class="card">
                <div class="card-body">

                    <div class="btn-list" style="float: right">
                     
                        <a href="#" class="btn btn-primary d-none d-sm-inline-block" onclick="addcollege()">
                          <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                          Create new 
                        </a>
                        <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal" data-bs-target="#modal-report" aria-label="Create new report">
                          <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                        </a>
                      </div>

                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>
                          <li><a href="#">{{$title}}</a></li>
                        </ol>
                      </nav>
                    <h4 class="card-title">{{$title}}</h4>


                    <a href="{{url('/public/uploads/excel/college.xlsx')}}" download="College">Download  Sample Excel Template </a>
                    <form action="{{ url('bulkupload_college') }}" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                           <div class="file-upload-custom mt-3 mb-3">
                              <input style="    height: inherit;" class="form-control" type="file" name="file">
                              <button type="submit" class="btn btn-blue btn-sm text-white">Upload</button>
                           </div>
                       </form>
                      
                       @if(Session::has('duplicates'))
                      <div class="alert alert-warning">
                        <p>Duplicate values:</p>
                         <ul>
                            @foreach(Session::get('duplicates') as $duplicate)
                               <li>{{ $duplicate }}</li>
                            @endforeach
                        </ul>
                      </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                @endforeach
                           </ul>
                           </div>
                      @endif


                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>College</th>
                <th>Status</th>
                <th>Action</th>
               
            </tr>
        </thead>
        <tbody>
<?php $i=1;?>
            @foreach($collegelist as $collegelist)
            <tr>
                <td>{{$i}}</td>
                <td>{{$collegelist->college}}</td>
                <td>

                  @if($collegelist->status==1)
                  <span style="color:green;display:block;" id="act_{{$collegelist->id}}">Active</span>
                  <span style="color:red;display:none;" id="deact_{{$collegelist->id}}" >Deactive</span>                       
                  @else
                  <span style="color:green;display:none;" id="act_{{$collegelist->id}}" >Active</span>
                  <span style="color:red;display:block;" id="deact_{{$collegelist->id}}">Deactive</span>    
                  
                  @endif


                                          </td> 
                                          <td>
                                            <div class="d-flex">
                                            <button type="button" class="btn btn-cyan btn-sm text-white me-2" fdprocessedid="wf07gv" onclick="editcollege('{{$collegelist->id}}','{{$collegelist->college}}')">
                                              Edit
                                            </button>
                                            @if($collegelist->status==1)
                                                      <button type="button" style="display: block" id="deactb_{{$collegelist->id}}" class="btn btn-danger btn-sm text-white" fdprocessedid="pvth1" onclick="dCollege('{{$collegelist->id}}')">
                                                       Deactivate
                                                      </button>

                                                      <button type="button" style="display: none"  id="actb_{{$collegelist->id}}" class="btn btn-success btn-sm text-white" fdprocessedid="pvth1" onclick="aCollege('{{$collegelist->id}}')">
                                                        Activate
                                                       </button>
                                                      @else
                                                      <button type="button" style="display: block" id="actb_{{$collegelist->id}}" class="btn btn-success btn-sm text-white" fdprocessedid="pvth1" onclick="aCollege('{{$collegelist->id}}')">
                                                        Activate
                                                       </button>
                                                       <button type="button" style="display: none" id="deactb_{{$collegelist->id}}" class="btn btn-danger btn-sm text-white" fdprocessedid="pvth1" onclick="dCollege('{{$collegelist->id}}')">
                                                        Deactivate
                                                       </button>
                                                      @endif
                                            </div>
                          
                                          </td>
                                         
               
            </tr>
            <?php $i++;?>
            @endforeach
           
        </tbody>
      
    </table>

    <form id="college_form" method="POST" enctype="multipart/form-data"  action="{{url('/save_college')}}">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="modal modal-blur fade" id="modal-college" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">New College</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="mb-3">
            <label class="form-label">College<sup>*</sup></label>
            <input type="text" class="form-control" name="college" id="college" placeholder="College">
          </div>
        </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>                                            
      </div>
    
    </div>
    </div>
    </form>


                    </div>
                </div>
           
            </div>


<!--Edit model-->
<form id="editcollege" method="POST" enctype="multipart/form-data"  action="{{url('/edit_college')}}">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="modal modal-blur fade" id="modal-edit-college" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Edit College</h5>
      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body">
      <div class="mb-3">
        <label class="form-label">College<sup>*</sup></label>
        <input type="hidden" class="form-control" name="editcollegeid" id="editcollegeid" placeholder="College">
        <input type="text" class="form-control" name="editcolname" id="editcolname" placeholder="College">
      </div>
    </div>
  <div class="modal-footer">
    <button type="submit" id="college_edit" class="btn btn-primary">Update</button>
  </div>                                            
  </div>

</div>
</div>
</form>



@endsection
