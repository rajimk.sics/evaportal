@extends('layouts.main')

@section('content')
    <!-- Page body -->
    <div class="page-body">
        <div class="container-xl">

            <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumbs mt-4">
                    <li><a href="{{ url('/home') }}">Exams</a></li>
                    <li><a href="{{ url('/fetchExam') }}">View Exams</a></li>
                    <li><a href="{{ url('/viewQuestions/'.$exam_id) }}">Question List</a></li>

                </ol>
            </nav>

            <div class="card mt-3">
                <div class="card-body">

                    <h4 class="card-title">Manage Questions</h4>
                    <button id="deactivate-selected" class="btn btn-danger"
                        onclick="toggleMultipleQuesStatus('deactivate', 'questions')">Deactivate
                        Selected</button>
                    <button id="activate-selected" class="btn btn-success"
                        onclick="toggleMultipleQuesStatus('activate', 'questions')">Activate Selected</button>

                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th class="align-middle text-center">
                                        <div class="d-flex align-items-center justify-content-center">
                                            <input type="checkbox" id="selectquestions">
                                            <span class="ms-2">Select All</span>
                                        </div>
                                    </th>
                                    <th class="align-middle text-center">No</th>
                                    <th class="align-middle">Questions</th>
                                    <th class="align-middle">Option A</th>
                                    <th class="align-middle">Option B</th>
                                    <th class="align-middle">Option C</th>
                                    <th class="align-middle">Option D</th>
                                    <th class="align-middle">Answer</th>
                                    <th class="align-middle">Status</th>
                                    <th class="align-middle">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php $i = 1; ?>
                                @foreach ($queslist as $ques)
                                    <tr>
                                        <td class="align-middle text-center">
                                            <input type="checkbox" class="row-checkbox questionbox" id="questionbox"
                                                value="{{ $ques->id }}">
                                        </td>
                                        <td class="align-middle text-center">{{ $i }}</td>
                                        <td class="align-middle">{{ $ques->questions }}</td>
                                        <td class="align-middle">{{ $ques->optiona }}</td>
                                        <td class="align-middle">{{ $ques->optionb }}</td>
                                        <td class="align-middle">{{ $ques->optionc }}</td>
                                        <td class="align-middle">{{ $ques->optiond }}</td>
                                        <td class="align-middle">Option {{ $ques->answer }}</td>
                                        <td class="align-middle">
                                            @if ($ques->status == 1)
                                                <span style="color:green;display:block"
                                                    id="act_{{ $ques->id }}">Active</span>
                                                <span style="color:red;display:none"
                                                    id="deact_{{ $ques->id }}">Deactive</span>
                                            @else
                                                <span style="color:red;display:block"
                                                    id="deact_{{ $ques->id }}">Deactive</span>
                                                <span style="color:green;display:none"
                                                    id="act_{{ $ques->id }}">Active</span>
                                            @endif
                                        </td>
                                        <td class="align-middle">
                                            <button type="button" class="btn btn-cyan btn-sm text-white"
                                                onclick="editQues('{{ $ques->id }}')">Edit</button>
                                            @if ($ques->status == 1)
                                                <button type="button" class="btn btn-danger btn-sm text-white me-2"
                                                    style="display: block"
                                                    onclick="toggleQuesStatus('{{ $ques->id }}','questions','deactivate')"
                                                    id="deactb_{{ $ques->id }}">
                                                    Deactivate Question
                                                </button>
                                                <button type="button" class="btn btn-success btn-sm text-white me-2"
                                                    style="display: none"
                                                    onclick="toggleQuesStatus('{{ $ques->id }}','questions','activate')"
                                                    id="actb_{{ $ques->id }}">
                                                    Activate Question
                                                </button>
                                            @else
                                                <button type="button" class="btn btn-success btn-sm text-white me-2"
                                                    style="display: block"
                                                    onclick="toggleQuesStatus('{{ $ques->id }}','questions','activate')"
                                                    id="actb_{{ $ques->id }}">
                                                    Activate Question
                                                </button>
                                                <button type="button" class="btn btn-danger btn-sm text-white me-2"
                                                    style="display: none"
                                                    onclick="toggleQuesStatus('{{ $ques->id }}','questions','deactivate')"
                                                    id="deactb_{{ $ques->id }}">
                                                    Deactivate Question
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                @endforeach
                            </tbody>


                        </table>

                    </div>
                </div>
                <form id="editQues" method="POST" enctype="multipart/form-data" action="{{ url('/update_questions') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal modal-blur fade" id="modal-ques" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Edit Questions</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body" id="question_details">

                                    <div class="row">

                                        <div class="mb-3">
                                            <input type="hidden" class="form-control" name="editquesid"
                                                id="editquesid">
                                            <label class="form-label">Question<sup>*</sup></label>
                                            <textarea class="form-control" name="editquestion" id="editquestion" rows="4"></textarea>
                                        </div>

                                        <div class="row">
                                            <div class="col-5 col-sm-3">
                                                <label class="form-label">Option A<sup>*</sup></label>
                                                <input type="text" class="form-control" name="editoptionA"
                                                    id="editoptionA" placeholder="Answer">
                                                <input type="radio" name="editanswer" value="A" id="optionA">
                                            </div>

                                            <div class="col-5 col-sm-3">
                                                <label class="form-label">Option B<sup>*</sup></label>
                                                <input type="text" class="form-control" name="editoptionB"
                                                    id="editoptionB" placeholder="Answer">
                                                <input type="radio" name="editanswer" value="B" id="optionB">
                                            </div>
                                            <div class="col-5 col-sm-3">
                                                <label class="form-label">Option C<sup>*</sup></label>
                                                <input type="text" class="form-control" name="editoptionC"
                                                    id="editoptionC" placeholder="Answer">
                                                <input type="radio" name="editanswer" value="C" id="optionC">
                                            </div>
                                            <div class="col-5 col-sm-3">
                                                <label class="form-label">Option D<sup>*</sup></label>
                                                <input type="text" class="form-control" name="editoptionD"
                                                    id="editoptionD" placeholder="Answer">
                                                <input type="radio" name="editanswer" value="D" id="optionD">
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
                @if (Session::has('duplicates'))
                    <script>
                        document.addEventListener('DOMContentLoaded', function() {
                            swal({
                                title: "Duplicates Found!",
                                text: "{!! Session::get('duplicates') !!}",
                                icon: "warning",
                                button: "OK",
                            });
                        });
                    </script>
                @endif




            </div>

        </div>
    @endsection
