@extends('layouts.main')

@section('content')
    <!-- Page body -->
    <div class="page-body">
        <div class="container-xl">

            <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumbs">
                    <li><a href="{{ url('/home') }}">Home</a></li>
                    <li><a href="{{ url('/addQuestions') }}">Exams - Add Questions</a></li>
                </ol>
            </nav>

            <h4 class="card-title mt-4">Exam List</h4>
            <div class="card rounded">
                <div class="card-body">

                  
                    <div class="table-responsive">
                        <form id="filter-form" method="GET" action="{{ url('/addQuestions') }}">
                            <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">

                        <div class="row g-3 align-items-center">
                            <div class="col">
                                <label for="dropdown1" class="col-form-label">Search By Type</label>
                            </div>
                            <div class="col">
                                <select class="form-select rounded examtechnology" id="examtechnology"
                                    name="examtechnology">
                                    <option  {{ $selectedExamType == 'false' ? 'selected' : '' }} value="false">Select Exam Type</option>
                                    <option {{ $selectedExamType == 'Talento' ? 'selected' : '' }} value="Talento">Talento</option>
                                    <option {{ $selectedExamType == 'Selection Test' ? 'selected' : '' }} value="Selection Test">Selection Test</option>
                                    <option {{ $selectedExamType == 'Internship' ? 'selected' : '' }} value="Internship">Internship</option>
                                </select>
                            </div>

                            <div class="col">
                                <select class="form-select rounded" id="techselect" name="techselect">
                                    <option value="">Select Technology</option>
                                    @foreach ($technology as $item)
                                        <option {{ $selectedTechnology == $item->id ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->technology }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col">
                                <select class="form-select rounded" id="techpackages" name="techpackages">
                                    <option value="">Select Package</option>
                                </select>
                            </div>
                            <div class="col d-flex align-items-center g-3">
                                <div class="form-group formcontents mb-0 flex-row me-1">
                                <button type="submit" class="btn btn-primary">Filter</button>
                                <a href="{{ url('/addQuestions') }}" class="btn btn-primary ms-2"> Clear All</a>  
                                                      </div>
                        </div>
                    </div>
                      
                    </form>
                        <h4 class="card-title mt-3">Exams</h4>

                        <div class="mt-3">
                        <table id="exam-results" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    
                                    <th>  <div class="d-flex align-items-center justify-content-center">
                                        <input type="checkbox" id="selectexams">
                                        <span class="ms-2">Select All</span>
                                    </div></th>
                                    <th class="align-middle">No</th>
                                    <th  class="align-middle">Select Exam</th>
                                    <th  class="align-middle">Technology</th>
                                    <th  class="align-middle">Packages</th>
                                    <th  class="align-middle">Percentage</th>
                                    <th  class="align-middle">Actions-View Questions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($exams))
                                    @foreach($exams as $index => $exam)
                                        <tr>
                                            <td><input type="checkbox" class="exam-checkbox" data-exam-id="{{ $exam['exam_id'] }}"></td>
                                            <td>{{ $index + 1 }}</td>
                                            <td>{{ $exam['exam_name'] }}</td>
                                            <td>{{ implode(', ', array_column($exam['technologies'], 'technology')) }}</td>
                                            <td>
                                                {{ implode(', ', array_merge(...array_column($exam['technologies'], 'packages'))) }}
                                            </td>
                                                                                        <td>{{ $exam['percentage'] }}</td>
                                            <td><a href="{{ url('viewQuestions/' . $exam['exam_id']) }}"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon icon-tabler icons-tabler-outline icon-tabler-eye"  >
                                                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                                <path d="M10 12a2 2 0 1 0 4 0a2 2 0 0 0 -4 0" />
                                                <path d="M21 12c-2.4 4 -5.4 6 -9 6c-3.6 0 -6.6 -2 -9 -6c2.4 -4 5.4 -6 9 -6c3.6 0 6.6 2 9 6" />
                                            </svg></a></td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7">No exams found</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    </div>
                </div>
            </div>

            <div class="row row-cards mt-3 rounded">
                <div class="col-12">
                    <form id="addquestions" method="POST" enctype="multipart/form-data"
                        action="{{ url('/save_questions') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="card-header">
                            <h4 class="card-title ms-2">Add Questions</h4>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="card-body p-5" style=" background-color:white ; ">

                            <div class="row">
                                <div class="col-md-6 col-xl-6">

                                    <div class="mb-3">
                                        <input type="radio" name="quesType" value="Individual Questions" id="questype1">
                                        <label>Individual Questions</label>
                                        <input type="radio" name="quesType" value="Excel Upload" id="questype2">
                                        <label>Excel Upload</label>

                                    </div>
                                </div>
                            </div>

                            <div id="individual_questions" style="display: none;">
                                <button type="button" id="add-question" class="btn btn-cyan">Add Questions</button>

                                <div class="questions-container">
                                    <div class="question-set">
                                        <div id="exam_ids">
                                        </div>
                                        {{-- <input type="hidden" name="exam_type" id="exam_type" value=""> --}}

                                        <div class="row">
                                            <div class="mb-3">

                                                <label class="form-label mt-3">Question 1<sup>*</sup></label>
                                                <textarea class="form-control" name="question[1]" rows="4"></textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-5 col-sm-3">
                                                <label class="form-label">Option A<sup>*</sup></label>
                                                <input type="text" class="form-control" name="optionA[1]"
                                                    placeholder="Answer">
                                                <input type="radio" name="answer[1]" value="A" id="optionA1">
                                            </div>
                                            <div class="col-5 col-sm-3">
                                                <label class="form-label">Option B<sup>*</sup></label>
                                                <input type="text" class="form-control" name="optionB[1]"
                                                    placeholder="Answer">
                                                <input type="radio" name="answer[1]" value="B" id="optionB1">
                                            </div>
                                            <div class="col-5 col-sm-3">
                                                <label class="form-label">Option C<sup>*</sup></label>
                                                <input type="text" class="form-control" name="optionC[1]"
                                                    placeholder="Answer">
                                                <input type="radio" name="answer[1]" value="C" id="optionC1">
                                            </div>
                                            <div class="col-5 col-sm-3">
                                                <label class="form-label">Option D<sup>*</sup></label>
                                                <input type="text" class="form-control" name="optionD[1]"
                                                    placeholder="Answer">
                                                <input type="radio" name="answer[1]" value="D" id="optionD1">
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-danger remove-question">Remove
                                            Question</button>
                                    </div>
                                </div>
                                <div class="card-footer text-end">
                                    <div class="d-flex">
                                        <button type="submit" class="btn btn-primary ms-auto">Submit</button>
                                    </div>
                                </div>
                            </div>
                    </form>


                    <form id="addexcelquestions" method="POST" enctype="multipart/form-data"
                        action="{{ url('/save_excelquestions') }}">
                        <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
                      
                        <div id="exam_id">
                        </div>
                        {{-- <input type="hidden" name="examtype" id="examtype" value=""> --}}
                        <div id="excel_questions" style="display: none;">
                            <a href="{{ url('/public/uploads/questions/QASample.csv') }}" download="Questions">Download
                                Sample Excel Template</a>


                            <div class="file-upload-custom mt-3 mb-3">
                                <input style="height:inherit;" id="excelFile" class="form-control" id="questions_file" name="questions_file" type="file"
                                    >
                                <button type="submit"  class="csvupbtn btn btn-blue btn-sm text-white">Upload</button>
                            </div>
                           


                        </div>
                    </form>
                    <div id="error-message" class="text-danger">

                    </div>
                </div>

                <!--    </div>-->

                @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif

            </div>

        </div>




    </div>
    @if ($errors->any())
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            swal({
                title: " Oops! We noticed some issues",
                text: `
                    @foreach ($errors->all() as $error)
                        {{ $error }}\n
                    @endforeach
                `,
                type: "error",
                button: "OK",
            });
        });
    </script>
@endif
@if (Session::has('duplicates'))
<script>
    document.addEventListener('DOMContentLoaded', function () {
        swal({
            title: "Duplicates Found!",
            text: "{!! Session::get('duplicates') !!}",
            type: "warning",
            button: "OK",
        });
    });
</script>
@endif



  


    <script>
     
        
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif
    </script>
@endsection
