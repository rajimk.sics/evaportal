@extends('layouts.main')

@section('content')
    <!-- Page body -->
    <div class="page-body">
        <div class="container-xl">

            <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">

            <div class="card">
                <div class="card-body">

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                            <li><a href="{{ url('/home') }}">Home</a></li>
                            <li><a href="{{ url('/fetchExam') }}">View Exams</a></li>
                            <li><a href="{{ url('/viewAssignedquestions/' . $exam_id) }}">View Questions -
                                    {{ ucfirst($exam) }}</a></li>
                        </ol>
                    </nav>

                    <button type="button" class="btn btn-primary float-end">
                        <a href="{{ url('/addExamquestions/' . $exam_id) }}" class="text-white"
                            style="text-decoration: none;">Assign more</a>
                    </button>
                    <h4 class="card-title">View Questions</h4>
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Questions</th>
                                    <th>Option A</th>
                                    <th>Option B</th>
                                    <th>Option C</th>
                                    <th>Option D</th>
                                    <th>Answer</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach ($questions as $question)
                                    <tr>
                                        <td class="index-column">{{ $i }}</td>
                                        <td>{{ ucfirst($question->questions) }}</td>
                                        <td>{{ $question->optiona }}</td>
                                        <td>{{ $question->optionb }}</td>
                                        <td>{{ $question->optionc }}</td>
                                        <td>{{ $question->optiond }}</td>
                                        <td>Option {{ $question->answer }}</td>
                                        <td>
                                            <button class="btn btn-danger btn-sm" onclick="removeQuestion({{ $exam_id }}, {{ $question->id }}, this)">
                                                Remove
                                            </button>
                                            
                                        </td>

                                    </tr>
                                    <?php $i++; ?>
                                @endforeach

                            </tbody>

                        </table>
                    </div>
                </div>


            </div>
        </div>

    </div>
@endsection


