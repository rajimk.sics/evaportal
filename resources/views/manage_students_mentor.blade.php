@extends('layouts.main')
@section('content')
    <style>
        .formcontents {
            display: flex;
            flex-direction: column;
        }

        label.error {
            color: red;
            order: 3;
        }
    </style>
    <!-- Page body -->
    <div class="page-body">
        <div class="container-xl">
            <div class="row row-cards">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">

                        </div>
                        <div class="card-body">
                            @if (Session::has('error'))
                                <div class="alert alert-danger">
                                    {{ Session::get('error') }}
                                </div>
                            @endif


                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumbs">
                                    <li><a href="{{ url('/home') }}">Home</a></li>

                                    <li><a href="{{ url('/trainerStudents') }}">{{ $title }}</a></li>
                                </ol>
                            </nav>


                            <h4 class="card-title"> {{ $title }}</h4>

                            <div class="payment-grd">
                                <form id="searchchrys_form" method="GET" enctype="multipart/form-data"
                                    action="{{ url('/trainerStudents') }}">
                                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                                    <div class="d-flex align-items-end flex-wrap gap-3">
                                        <!-- Packtype Dropdown -->
                                        <div>
                                            <label for="packtype" class="form-label">Packtype<sup>*</sup></label>
                                            <select class="form-select" name="packtype" id="packtype">
                                                <option value='' disabled>Select a Packtype</option>
                                                <option value='1'{{ $pac_type == '1' ? 'selected' : '' }}>Regular
                                                </option>
                                                <option value='2'{{ $pac_type == '2' ? 'selected' : '' }}>Chrysalis
                                                </option>
                                            </select>
                                        </div>

                                        <!-- Year Dropdown -->
                                        <div>
                                            <label for="year" class="form-label">Select Year<sup>*</sup></label>
                                            <select class="form-select" name="year" id="year">
                                                @foreach ($years as $year)
                                                    <option value="{{ $year }}"
                                                        {{ $year == $currentYear ? 'selected' : '' }}>{{ $year }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <!-- Month Dropdown -->
                                        <div>
                                            <label for="month" class="form-label">Select Month<sup>*</sup></label>
                                            <select class="form-select" name="month" id="month">
                                                @foreach ($months as $key => $month)
                                                    <option value="{{ $key }}"
                                                        {{ $key == $currentMonth ? 'selected' : '' }}>{{ $month }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <!-- Submit Button -->
                                        <div>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>

                                        <!-- View Current Month Button -->
                                        <div>
                                            <a href="{{ url('/trainerStudents/') }}" class="btn btn-primary">View Current
                                                Month</a>
                                        </div>
                                        <!--View all students -->
                                        <div>
                                            <a href="{{ url('/trainerStudents?all=true') }}" class="btn btn-secondary">All
                                                Students</a>
                                        </div>
                                    </div>
                                </form>

                            </div>


                            <div class="table-responsive">

                                <table id="example" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Student Name</th>
                                            <th>Email Id</th>


                                            <th>Package Type</th>
                                            <th>Package</th>
                                            <th>Joining Date</th>

                                            {{-- @if (Auth::user()->role != 2)
                                                <th>Assigned Days</th>
                                            @endif --}}
                                            <th>Start Date</th>
                                            <th>End Date</th>



                                            @if (in_array(Auth::user()->role, [2, 3, 4]))
                                                <th>Action</th>
                                            @endif

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>
                                        @foreach ($studlist as $studlist)
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td>{{ ucfirst($studlist->name) }}</td>
                                                <td>{{ $studlist->email }}</td>




                                                <td>

                                                    @if ($studlist->pac_type == 1)
                                                        Regular
                                                    @else
                                                        Chrysalis
                                                    @endif

                                                </td>

                                                @php
                                                    $techname = App\Helpers\CustomHelper::tech($studlist->tech_id);
                                                @endphp
                                                <td>{{ $studlist->pac_name }}</td>
                                                <td>

                                                    {{ date('d-m-Y', strtotime($studlist->joining_date)) }}

                                                </td>


                                                {{-- @if (Auth::user()->role != 2)
                                                    @php
                                                        $name = App\Helpers\CustomHelper::uname($studlist->sales_id);

                                                        $displayName =
                                                            $name && isset($name['name']) ? ucfirst($name['name']) : '';
                                                    @endphp

                                                    <td>{{ ucfirst($displayName) }}</td>
                                                @endif --}}
                                                <td>

                                                    @if ($studlist->pac_type == 1)
                                                        {{ date('d-m-Y', strtotime($studlist->due_date_first)) }}
                                                    @else
                                                        {{ $studlist->due_date_first }}
                                                    @endif




                                                </td>

                                                @php
                                                    $enddate = App\Helpers\CustomHelper::enddate(
                                                        $studlist->pac_name,
                                                        $studlist->due_date_first,
                                                    );

                                                @endphp
                                                <td>{{ $enddate }}</td>


                                                <td>

                                                    @if (in_array(Auth::user()->role, [2, 3, 4]))
                                                    <div class="row">
                                                        <div class="row">
                                                            <button type="button" class="btn btn-sm col-auto mt-1" title="Assign Trainer"
                                                            onclick="FetchTrainers({{ $studlist->id }},{{ $studlist->package_id }},{{$studlist->studentpackageid}})">
                                                            Change Mentor
    
                                                        </button>
                                                        <a href="{{ url('/batchplanUpdate/'.$studlist->studentpackageid) }}" 
                                                            class="btn btn-sm btn-info col-auto mt-1" 
                                                            title="Batchplan">
                                                            Batchplan
                                                         </a>
                                                         
                                                    </div>
                                                    @endif

                                                    

                                                </td>







                                            </tr>
                                            <?php
                                            $i++;
                                            
                                            ?>
                                        @endforeach

                                    </tbody>

                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade active show" id="tabs-home-8" role="tabpanel">

                            <div>


                            </div>
                        </div>
                        <div class="tab-pane fade" id="tabs-profile-8" role="tabpanel">

                            <div>


                            </div>
                        </div>
                        <div class="tab-pane fade" id="tabs-activity-8" role="tabpanel">

                            <div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <form id="assignTrainerForm" method="POST" enctype="multipart/form-data" action="{{ url('/assignTrainer') }}">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <input type="hidden" name="student_id" id="student_id" value="">
            <input type="hidden" name="pac_id" id="pac_id" value="">
            <input type="hidden" name="studentpackageid" id="studentpackageid" value="">


            <div class="modal" id="assignTrainerModel">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Change Trainer</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <label for="trainerName" class="w-100 form-label">
                                <h4>Trainer Name</h4>
                            </label>
                            <select name="trainer_id[]" class="w-100 rounded form-select mb-1 selecttrainer"
                                id="trainerName" multiple="multiple">
                            </select>

                            <label for="daysDropdown" class="w-100 form-label">
                                <h4>Select Days</h4>
                            </label>
                            <select name="days[]" class="w-100 rounded form-select mb-1 selectdays" id="daysDropdown"
                                multiple="multiple">

                            </select>

                            <div class="row mt-4">
                                <div class="col-md-6">
                                    <label for="startTime">Start Time</label>
                                    <input type="text" class="form-control timepicker" id="startTime"
                                        name="start_time" placeholder="hh:mm AM/PM">
                                </div>
                                <div class="col-md-6">
                                    <label for="endTime">End Time</label>
                                    <input type="text" class="form-control timepicker" id="endTime" name="end_time"
                                        placeholder="hh:mm AM/PM">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    @endsection


