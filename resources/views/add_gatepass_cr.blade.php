<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Image Upload and Crop</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.12/cropper.min.css">
    <style>
        #image {
            max-width: 100%;
        }
        .container {
            width: 50%;
            margin: 0 auto;
        }
        #croppedImage {
            max-width: 100%;
            margin-top: 20px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Image Upload and Crop</h1>
        <form id="uploadForm" enctype="multipart/form-data">
            <input type="file" id="uploadImage" name="image" accept="image/*">
            <input type="submit" value="Upload Image">
        </form>
        <br>
        <img id="image" src="" alt="Image for cropping">
        <br><br>
        <button id="cropButton">Crop Image</button>
        <br><br>
        <img id="croppedImage" src="" alt="Cropped Image">
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.12/cropper.min.js"></script>
    <script>
        $(document).ready(function() {
            var cropper;

            $('#uploadForm').on('submit', function(e) {
                e.preventDefault();
                
                var fileInput = $('#uploadImage')[0];
                var file = fileInput.files[0];
                
                if (file) {
                    var img = new Image();
                    img.onload = function() {
                        if (img.width > 1024 || img.height > 1024) {
                            alert('Image dimensions exceed 1024x1024 pixels.');
                            return;
                        }

                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $('#image').attr('src', e.target.result).show();
                            
                            if (cropper) {
                                cropper.destroy();
                            }
                            
                            cropper = new Cropper($('#image')[0], {
                                aspectRatio: 16 / 9,
                                viewMode: 1,
                                preview: '#croppedImage',
                            });
                        };
                        reader.readAsDataURL(file);
                    };
                    img.src = URL.createObjectURL(file);
                } else {
                    alert('Please select an image.');
                }
            });
            
            $('#cropButton').on('click', function() {
                if (cropper) {
                    var canvas = cropper.getCroppedCanvas();
                    var croppedImage = canvas.toDataURL('image/png');

                    $.ajax({
                        url: '/upload-cropped-image',
                        type: 'POST',
                        data: { image: croppedImage },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(response) {
                            $('#croppedImage').attr('src', response.croppedImageUrl);
                        }
                    });
                }
            });
        });
    </script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</body>
</html>
