@extends('layouts.main')

@section('content')

        <!-- Page body -->
       
    
        <div class="page-body">
            <div class="container-xl">

              <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
                   
              <div class="card">
                <div class="card-body">
              
                 

                  <nav aria-label="breadcrumb">
                    <ol class="breadcrumbs">
                        <li><a href="{{ url('/home') }}">Home</a></li>
                        <li><a href="{{ url('/packagelist_trainer') }}">Packages</a></li>
                        <li><a href="#">{{ ucfirst($singlelist->pac_name) }} - {{ $title }}</a></li>
                    </ol>
                  </nav>
                  <div class="card">
                    <div class="card-header">
                        <b>Syllabus Upload</b>
                    </div>
                    <div class="card-body shadow rounded-4 mt-4">
                        <div class="row mt-1">
                        <div class="col-7">
                        <h5 class="card-title">{{$singlelist->pac_name}}</h5>    
                    </div>
                    <div class="col-3">
                        @if ($topicscount > 0)
                            <a href="{{ url('/topicListing/' . $pac_id) }}" class="btn btn-light">Topic
                                Listing</a>
                        @endif
                    </div>


                   
                
                    <div class="col-2"><button class="btn btn-light"  onclick="upsyllabus()">Upload topic csv</button> </div>
                </div>
                     

                    
                    </div>
                    </div>

               
             
                    <form id="csvupload"  method="POST" enctype="multipart/form-data" action="{{ url('/syllabusUpload') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="pac_id" value="{{$singlelist->pac_id}}">

                        <div class="modal fade" id="modalCSV" tabindex="-1" aria-labelledby="modalCSVLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="modalCSVLabel">Upload Syllabus</h5>
                                        
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="mt-1 mb-2"><a style="color: black;" href="{{url('/public/uploads/excel/syllabus.csv')}}" download="syllabus" title="Topics CSV"> <u>Download Sample Topics CSV</u>  </a></div>  
                                        <div class="mb-3">
                                            <label for="csv" class="form-label">Upload a File</label>
                                            <input type="file" class="form-control" name="csv_file" id="csv"  required>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Upload</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    @if(Session::has('duplicates'))
                    <div class="alert alert-warning">
                      <p>Duplicate values:</p>
                       <ul>
                          @foreach(Session::get('duplicates') as $duplicate)
                             <li>{{ $duplicate }}</li>
                          @endforeach
                      </ul>
                    </div>
                      @endif

                      @if ($errors->any())
                          <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                              @endforeach
                         </ul>
                         </div>
                    @endif
                    
                    
                    
                   

                   
                    
         


            <script>
                @if (Session::has('message'))
                    swal({
                        title: "",
                        text: "{{ Session::get('message') }}",
                        type: "success",
                        showCancelButton: false,
                        dangerMode: false,
                        confirmButtonText: 'OK',
                    });
                @endif
                </script>
          
    
@endsection
