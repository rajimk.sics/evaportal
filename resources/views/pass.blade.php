<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gatepass</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Pinyon+Script&display=swap" rel="stylesheet">
    <style>
        body {
            margin: 0;
            font-family: Arial, sans-serif;
        }
        .header {
            background: #ccc;
            padding: 0;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .header img {
            width: 100%;
            height: auto;
        }
        .content {
            padding: 20px;
        }
        .content ul {
            margin: 0;
            padding-left: 0;
            list-style: none;
        }
        .content img {
            margin: 70px 0;
            object-fit: cover;
        }
        .signature_seal {
            display: flex;
            align-items: center;
        }
        .signature_seal img {
            width: 100px;
            margin-right: 10px;
        }
        .footer {
            display: flex;
            justify-content: center;
            background-color: #ccc;
            padding: 10px;
            position: absolute;
            bottom: 0;
            width: 100%;
            text-align: center;
        }
        .av-img{
            display: flex;
        }
        .ctn{
            position: absolute;
            bottom: 80px;
        }
    </style>
</head>
<body>
    <main  >
        <div class="header">
            <img src="{{ url('public/uploads/credentials/'.$logo) }}" alt="Header Image">
        </div>
        <div class="content">
            <div style="display: flex; justify-content: space-between;">
                <ul>
                    <li>To</li>
                    <li>The Security Officer</li>
                    <li>Technopark</li>
                </ul>
                <ul>
                    <li>{{ $date }}</li>
                </ul>
            </div>
            <p>Sir/Ma’am,</p>
            <?php
                $s1 = new DateTime($start_date);
                $s2 = new DateTime($end_date);
            ?>
            <p>
                Please permit our Trainee, <span>Mr/Ms. {{ $name }}</span> to enter Technopark campus
                @if ($start_date == $end_date)
                    on {{ $s1->format('jS F Y') }}
                @else
                    from {{ $s1->format('jS F Y') }} to {{ $s2->format('jS F Y') }}
                @endif
            </p>
            <div class="av-img">
                <img src="{{ url('public/uploads/photo/'.$photo) }}" alt="Trainee Photo" width="150" height="150">
            </div>
            <p>Note: For issuing a new gate pass, you have to submit this back to the admin. In case you lose your gate pass, there will be a fine of Rs. 1000/-</p>
            <div class="signature_seal">
                <img src="{{ url('public/uploads/credentials/'.$signatute) }}" alt="Signature">
                <img src="{{ url('public/uploads/credentials/'.$seal) }}" alt="Seal">
            </div>
            <ul class="ctn" >
                <li>{{ $contact_name }}</li>
                <li>Contact No: {{ $contact_no }}</li>
                <li>HR Team – Srishti Innovative</li>
            </ul>
        </div>
        <div class="footer">
            <span>Srishti Innovative | www.srishtis.com | 04714062181</span>
        </div>
    </main>
</body>
</html>
