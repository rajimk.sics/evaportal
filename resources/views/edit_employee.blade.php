@extends('layouts.main')
@section('content')
   <!-- Page body Jyothi -->
        <div class="page-body">
            <div class="container-xl">   
              <div class="row row-cards">
                <div class="col-12">

                  <form id="editemployee" method="POST" enctype="multipart/form-data"  action="{{url('/edit_save')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <nav aria-label="breadcrumb">
                      <ol class="breadcrumbs">
                        <li><a href="{{url('/home')}}">Home</a></li>
                        <li><a href="{{url('/'.$url)}}">{{$subtitle}}</a></li>
                        <li><a href="#">{{$title}}</a></li>
                      </ol>
                    </nav>
                    <div class="card-header">
                      <h4 class="card-title"></h4>{{$title}}
                    </div>
                    @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
@endif
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-6 col-xl-12">
                          <div class="row">
                            <div class="col-md-6 col-xl-6">

                              <div class="mb-3">
                                <label class="form-label">Employee Type<sup>*</sup></label>
                                <input type="hidden" name="userid" value="{{$saleuser->id}}">
                                <input type="text" class="form-control" name="emptype" id="emptype"  value="{{$type}}"readonly>
                                <input type="hidden" name="role" id="role" value="{{$saleuser->role}}">
                              </div>
                    
                              <div class="mb-3">
                                <label class="form-label">Employee Name<sup>*</sup></label>
                                <input type="text" class="form-control" name="empname" id="empname" value="{{$saleuser->name}}">
                              </div>
                              <div class="mb-3">
                                <label class="form-label">Official Email id<sup>*</sup></label>
                                <input type="text" class="form-control" name="office_email" id="office_email" value="{{$saleuser->office_email}}">
                              </div>

                              <div class="mb-3" >
                                <label class="form-label">Reported to</label>
                                <select class="form-select selectdesi" name="reported_to[]"  id="reported_to" multiple="multiple">
                                  
                                  @foreach($username as $username)
                                  <option value="{{ $username->id }}" {{ in_array($username->id,$names) ? 'selected' : '' }}>{{ $username->name }}
                                </option>
                                @endforeach
                              </select>
                            </div>
                          
                          </div>
                          <div class="col-md-6 col-xl-6">
                            <div class="mb-3">
                              <label class="form-label">Designation<sup>*</sup></label>
                              <select class="form-select selectdesi" name="designation"  id="designation" placeholder="Designation">
                                <option  value="">Select Designation</option>
                                @foreach($designationlist as $designationlist )
                                <option {{$designationlist->id==$saleuser->designation?"selected":""}} value="{{$designationlist->id}}">{{$designationlist->designation}}</option>
                                @endforeach
                               
                              </select>
                            </div>


                            @if($saleuser->role == 3)
                            <div class="mb-3">
                              <label class="form-label">Technology<sup>*</sup></label>
                              <select class="form-select selecttech" id="technology" name="technology[]" multiple="multiple">
                                <option value="">Select Technology</option>
                                @foreach($technologylist as $technologylist )
                                <option  value="{{$technologylist->id}}" {{ in_array($technologylist->id,$tech) ? 'selected' : '' }}>{{$technologylist->technology}}</option>
                                @endforeach
                              </select>
                            </div>
                            @endif
                       
                            <div class="mb-3">
                              <label class="form-label">Phone<sup>*</sup></label>
                              <input type="text" class="form-control" id="phone" name="phone" value="{{$saleuser->phone}}" minlength="10" maxlength="13">
                            </div>
                            <div class="mb-3">
                              <label class="form-label">Experience<sup>*</sup></label>
                              <input type="text" class="form-control" name="experience" id="experience" value="{{$saleuser->experience}}">
                            </div>
                          </div>

                          <div class="row">
                            <div class="mb-3">
                              <label class="form-label">Privileges</label>
                            
                              <input type="checkbox" id="cemail_interview" name="cinterview" value="1" {{ (isset($saleuser->interview))&&($saleuser->interview==1) ? 'checked' : '' }}>Schedule Interview  
                              <input type="checkbox" id="cdesignation" name="cdesignation" value="1" {{ (isset($saleuser->desig))&&($saleuser->desig==1) ? 'checked' : '' }}>Designation
                              <input type="checkbox" id="ctechnology" name="ctechnology" value="1" {{(isset( $saleuser->tech))&&($saleuser->tech==1)? 'checked' : '' }}>Technology
                              <input type="checkbox" id="csource" name="csource" value="1" {{ (isset($saleuser->source))&&($saleuser->source==1)? 'checked' : '' }}>Source
                              <input type="checkbox" id="ccollege" name="ccollege" value="1" {{ (isset($saleuser->college))&&($saleuser->college==1) ? 'checked' : '' }}>College
                              <input type="checkbox" id="cqualification" name="cqualification" value="1" {{ (isset($saleuser->qualification))&&($saleuser->qualification==1)? 'checked' : '' }}>Qualification<br>
                              <input type="checkbox" id="cspecialization" name="cspecialization" value="1" {{ (isset($saleuser->specialization))&&($saleuser->specialization==1)? 'checked' : '' }}>Specialization
                              <input type="checkbox" id="cadd_employee" name="cadd_employee" value="1" {{ (isset($saleuser->addemployee_privilage))&&($saleuser->addemployee_privilage==1) ? 'checked' : '' }}>Add Employee
                              <input type="checkbox" id="cadd_package" name="cadd_package" value="1" {{ (isset($saleuser->add_package))&&($saleuser->add_package==1) ? 'checked' : '' }}>Add Package
                              <input type="checkbox" id="cadd_event" name="cadd_event" value="1" {{ (isset($saleuser->add_event))&&($saleuser->add_event==1) ? 'checked' : '' }}>Add Event
                             
                          </div>
                        </div>





                        </div>
                      </div>


                    </div>
                  </div>
                  <div class="card-footer text-end">
                    <div class="d-flex">
                      
                      <button type="submit" class="btn btn-primary ms-auto">Submit</button>
                    </div>
                  </div>
                </form>

              
                </div>
           
            </div>

            
@endsection
