@extends('layouts.main')
@section('content')

<style>

  .formcontents{
    display: flex;
    flex-direction: column;
  }
  label.error{
    color: red;
    order: 3;
  }

  </style>
   <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">   
              <div class="row row-cards">
                <div class="col-12">

                <nav aria-label="breadcrumb">
                  <ol class="breadcrumbs">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="#">{{$title}}</a></li>
                  </ol>
                </nav>
                    <div class="card-header" >
                      <h4 class="fs-title" style="font-size: 20px;">{{$title}}</h4>
                    </div>


                  @if($enddate !='')

                  <input type="hidden" name="edate" id="edate" value="{{date("d-m-Y", strtotime($enddate))}}">

                  @else

                  <input type="hidden" name="edate" id="edate" value="{{date("d-m-Y", strtotime($requested_date))}}">

                  @endif

                
@if($aplied_status==1)
                
                  <form id="addgatepass" method="POST" enctype="multipart/form-data"  action="{{url('/save_gatepass')}}">
                      <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                    <div class="card-body">
                      <div class="">
                          <div class="row">
                            
                            <div class="col-lg-6 formcontents">
                            <div class="form-group">
                              <input type="hidden" name="userid" id="userid" value="{{$userdetails->id}}">
                              <input type="hidden" name="salesid" id="salesid" value="{{$userdetails->sales_id}}">
                              <label>Start date<sup>*</sup></label>
                              <input type="text" class="form-control" data-zdp_readonly_element="true" name="start_date1"  id="start_date1" >
                              </div>
                            </div>
                            <div class="col-lg-6 formcontents">
                            <div class="form-group">
                            <label>End Date<sup>*</sup></label>
                          <input type="text" class="form-control" data-zdp_readonly_element="true" name="end_date1" id="end_date1" >
                              </div>
                            </div>
                            <div class="col-lg-6 formcontents">
                            <div class="form-group">
                            <label class="form-label">Package Type<sup>*</sup></label>
                            <select class="form-select pack_duration selecttype" id="package_type" name="package_type" >
                                  <option value="">Select Package Type</option>
                                  <option value="1">Regular</option>
                                  <option value="2">Chrysalis</option>
                                  <option value="3">Event</option>
                            </select>
                              </div>
                            </div>
                            <div class="col-lg-6 formcontents" id="list" style="display:none;">
                            <div class="form-group">
                            <label class="form-label" id="package"><sup>*</sup></label>
                            <select class="form-select pack_duration selecttype" id="package_name" name="package_name" >
                                 
                            </select>
                              </div>
                            </div>


                            @if($userdetails->photo!='')

                            <input type="hidden" name="pic" id="pic" value="1">

                            <div class="col-sm-6 formcontents">
                              <div class="form-group">
                               <label class="form-label">Current Photo</label>
                              <img id="previewHeader" src="{{url('public/uploads/photo/'.$userdetails->photo)}}" alt="Preview" style=" max-width: 50x; max-height: 50px;">
                              </div>
                            </div>
                           @else

                           <div class="col-sm-6 formcontents imgprev" style="display:none;">
                            <div class="form-group">
                             <label class="form-label">Current Photo</label>
                            <img id="previewHeader" src="" alt="Preview" style=" max-width: 50x; max-height: 50px;">
                            </div>
                          </div>

                           <input type="hidden" name="pic" id="pic" value="0">

                           @endif
                          <div class="col-sm-6 formcontents">
                              <div class="form-group">
                                  <label class="form-label">Photo</label>
                                  <input type="file" class="form-control" name="photo" id="photo" >
                                </div>
                            </div>
                           
                        </div>
                      </div>

                    </div>

                  </div>
                  <div class="card-footer text-end">
                    <div class="d-flex">
                      
                      <button type="submit" class="btn btn-primary ms-auto">Submit</button>
                    </div>
                  </div>
                </form>

                @else
                  <span>  Cannot  Send request for Gate pass.Please Contact with  Sales Team</span>
                @endif
                </div>         
            </div>

            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.6.5/cropper.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.6.5/cropper.min.js"></script>

            <script>
  $(document).ready(function() {
    var $image = $('#image');
    var cropper;

    $('#imageInput').on('change', function(event) {
        var files = event.target.files;
        var done = function(url) {
            $image.attr('src', url);
            if (cropper) {
                cropper.destroy();
            }
            cropper = new Cropper($image[0], {
                aspectRatio: 1, // Adjust the aspect ratio as needed
                viewMode: 1,
                autoCropArea: 1,
                responsive: true,
                zoomable: true
            });
        };

        var reader;
        if (files && files.length > 0) {
            reader = new FileReader();
            reader.onload = function() {
                done(reader.result);
            };
            reader.readAsDataURL(files[0]);
        }
    });

    $('#cropButton').on('click', function() {
        if (cropper) {
            var canvas = cropper.getCroppedCanvas();
            canvas.toBlob(function(blob) {
                var url = URL.createObjectURL(blob);
                var a = document.createElement('a');
                a.href = url;
                a.download = 'cropped-image.png';
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
            });
        }
    });
});
             
              </script>
            
            <script>
            @if (Session::has('errormessage'))
            swal({
                title: "",
                text: "{{ Session::get('errormessage') }}",
                type: "sucess",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif 
      </script>         
@endsection
