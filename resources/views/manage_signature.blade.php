@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
                   
              <div class="card">




                <nav aria-label="breadcrumb">
                  <ol class="breadcrumbs">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="#">{{$title}}</a></li>
                  </ol>
                </nav>

                <h4 class="card-title">{{$title}}</h4>
                    <div class="card-body">
                     <form id="credentials" method="POST" action="{{url('/update_signature')}}" enctype="multipart/form-data">
                     <input type="hidden" name="_token" value="{{ csrf_token() }}">

                     @if($header!='')

                      <div class="mb-3">
                      <label class="form-label">Current Logo</label>
                      <img id="previewHeader" src="{{url('public/uploads/credentials/'.$header)}}" alt="Preview" style=" max-width: 150x; max-height: 100px;">
                          </div>
                  @endif
                      <div class="mb-3">
                      <label class="form-label">Logo</label>
                      <input type="file" class="form-control" name="header" id="header">
                      </div>
                        <div class="mb-3">
                       
                        <label>Contact name<sup>*</sup></label>
                        <input type="hidden" class="form-control" name="id" id="id" value="{{$id}}">
                        <input type="text" class="form-control" name="name" id="name" value="{{$name}}">
                       
                     
                      </div>
                      <label>Contact Number<sup>*</sup></label>
                     
                      <input type="text" class="form-control" name="contact_no" id="contact_no" value="{{$contact_no}}">
                     
                   
                    </div>
                    @if($signature!='')

                      <div class="mb-3">
                        <label class="form-label">Current Signature</label>
                        <img id="previewSignature" src="{{url('public/uploads/credentials/'.$signature)}}" alt="Preview" style=" max-width: 150x; max-height: 100px;">
                        
                      </div>
                      @endif
                        <div class="mb-3">
                        <label class="form-label">Signature</label>
                        <input type="file" class="form-control" name="signature" id="signature">
                      </div>
                      @if($seal!='')
                      <div class="mb-3">
                        <label class="form-label">Current Seal</label>
                        <img id="previewSeal" src="{{url('public/uploads/credentials/'.$seal)}}" alt="Preview" style=" max-width: 150x; max-height: 100px;">
                      </div>

                      @endif
                      <div class="mb-3">
                        <label class="form-label">Seal</label>
                        <input type="file" class="form-control" name="seal" id="seal"  >
                        </div>
                    </div>
                      <div class="card-footer text-end">
                        <div class="d-flex">
                          <button type="submit" class="btn btn-primary ms-auto">Save</button>
                        </div>
                      </div>
                        </form>
               </div>
            </div>
          </div>
            <script>
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif
        </script>




@endsection
