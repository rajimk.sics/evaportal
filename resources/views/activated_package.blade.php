@extends('layouts.main')
@section('content')
   <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">   
              <div class="row row-cards">
                
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-header">
                      <h2> Activated Packages</h2>
                    </div>
                    <div class="card-body">
                     
                      <div class="tab-content">
                       

                      <div class="tab-pane fade active show" id="tabs-info-8" role="tabpanel">
                         
                             
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Package Type</th>
                <th>Package Name</th>
                <th>Joining date</th>
                <th>Full Amount</th>
                <th>Paid Amount</th>
                <th>Pending Amount</th>
                <th>Payment Details</th>
              
              
            </tr>
        </thead>
        <tbody>
            <?php $i=1;?>
            @foreach($activated_packages as $package)
            @php
            $paid_amount=App\Helpers\CustomHelper::paidfees_student($package->student_id,$package->package_type,$package->package_id); 
            @endphp
            @php                            
            if($package->reduction_check==0){
             $rem=$package->total - $paid_amount['paidfees'];
             $total=$package->total;
            }
            else{
             $rem=$package->totalfees_afterreduction - $paid_amount['paidfees'];
             $total=$package->totalfees_afterreduction;;

            }                        
          @endphp
            <tr>
                <td>{{$i}}</td>
                <td>
                  
                  @if($package->package_type==1)

                    Regular

                  @else
                  Chrysalis

                  @endif
                  
                </td> 
                <td>{{$package->pac_name}}</td>
                <td>{{date("d-m-Y", strtotime($package->joining_date))}}</td>
               
               
                <td>Rs {{number_format($total,2)}}</td>
                <td>Rs {{number_format($paid_amount['paidfees'],2)}}  </td>              
                <td> Rs {{number_format($rem,2)}}</td>

               
                <td>

                  <a href="{{url('/package_details/'.$package->studentpackageid)}}"><button type="button" class="btn btn-green btn-sm text-white" fdprocessedid="wf07gv">
                    Package Details
                  </button></a>

                  <a  href="{{url('/payment_details_student/'.$package->package_type.'/'.$package->package_id)}}"><button type="button" class="btn btn-green btn-sm text-white" fdprocessedid="wf07gv">
                    Payment Details
                  </button></a>


                </td>
                
            </tr>
            <?php $i++;?>
            @endforeach           
        </tbody>     
    </table>

                    </div>
                       
                      </div>

                    </div>
                  </div>
                </div>
           
            </div>

            
@endsection
