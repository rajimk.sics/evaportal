@extends('layouts.main')

@section('content')

<style>

  .formcontents{
    display: flex;
    flex-direction: column;
  }
  label.error{
    color: red;
    order: 3;
  }

  </style>

        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
                   
              <div class="card">
                <div class="card-body">


                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>

                          @if($pac_type==2)

                          <li><a href="{{url('/manage_package_chrysallis')}}">Manage Chrysalis Package</a></li>
 
                          @else
                          <li><a href="{{url('/manage_package')}}"> Manage Regular Package</a></li>
                          @endif                         
                          <li><a href="#">{{$title}}</a></li>
                        </ol>
                      </nav>

                   
                    <h4 class="card-title"> {{$title}}</h4>

                    <div class="payment-grd">
                      <form id="searchchrys_form" method="GET" enctype="multipart/form-data" action="{{ url('/students_list/'.$pac_id) }}">
                          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
      
                          <div class="row">
                              <!-- Year Dropdown -->
                              <div class="col-lg-4 formcontents">
                                  <label for="year">Select Year<sup>*</sup></label>
                                  <select class="form-select" name="year" id="year">
                                      
                                      @foreach($years as $year)
                                      <option value="{{ $year }}" {{ $year == $currentYear ? 'selected' : '' }}>{{ $year }}</option>
                                      @endforeach
                                  </select>
                              </div>
      
                              <!-- Month Dropdown -->
                              <div class="col-lg-6 formcontents">
                                  <label for="month">Select Month<sup>*</sup></label>
                                  <select class="form-select" name="month" id="month">
                                     
                                      @foreach($months as $key => $month)
                                      <option value="{{ $key }}" {{ $key == $currentMonth ? 'selected' : '' }}>{{ $month }}</option>
                                      @endforeach
                                  </select>
                              </div>
      
                              <div class="col-lg-2 formcontents">
                                  <button type="submit" class="btn btn-primary" style="margin-top: 26px;">Submit</button>
                            </div>
      
                          </div>
      
                          <div class="col formcontents"> 
                            <a href="{{url('/students_list/'.$pac_id)}}" class="btn btn-primary"  style="margin-top: 26px;"> View Current Month</a>
                            </div>
                      </form>
                  </div>



                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Student Name</th>
                <th>Email</th>
                
                 <th>SICS Reg Number</th>

             
                <th>Phone Number</th>
                <th>College Name</th>
                <th>Joining Date</th>
                <th>Start Date</th>

                @if($pac_type==2)
                <th>End Date</th>
                @endif
                <th>Action</th>

            </tr>
        </thead>
        <tbody>
          <?php $i=1;?>
            @foreach($studentlist as $list)
            <tr>
                <td>{{$i}}</td>
                <td>{{ucfirst($list->name)}}</td>
                <td>{{$list->email}}</td>
                
                <td>SIAC{{$list->student_admissionid}}</td>
              
                <td>{{$list->phone}}</td>
                <td>{{ucfirst($list->collegename)}}</td>
                <td>{{date("d-m-Y", strtotime($list->joining_date))}}</td>

                
                <td>{{date("d-m-Y", strtotime($list->due_date_first))}}</td>
                @if($pac_type==2)
                <td>
                  {{date("d-m-Y", strtotime($list->enddate))}}
               </td>
                @endif
                <td>            
                        <a href="{{url('/package_details/'.$list->id)}}"><button type="button" class="btn btn-green btn-sm text-white" fdprocessedid="wf07gv">
                            Package Details
                          </button></a>
                          <a href="{{url('/payment_history_package/'.$list->id)}}">
                            <button type="button" class="btn btn-cyan btn-sm text-white" fdprocessedid="wf07gv">
                            Payment History
                            </button>
                        </a>
            </td>            
            </tr>
            <?php $i++;?>
            @endforeach
           
        </tbody>
      
    </table>

















                    </div>
                </div>
           
            </div>
            <script>
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif
        </script>




@endsection
