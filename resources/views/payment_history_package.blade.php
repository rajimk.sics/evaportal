@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">
              <input type="hidden" name="token_eva" id="token_eva"  value="{{ csrf_token() }}">
              <div class="card">
                <div class="card-body">

                    <div class="btn-list" style="float: right">

                      @php                            
                           if($package_details->reduction_check==0){

                            $rem=$package_details->total - $paidfees;
                            $total=$package_details->total;

                           }
                           else{
                            $rem=$package_details->totalfees_afterreduction - $paidfees;
                            $total=$package_details->totalfees_afterreduction;;

                           }                        
                         @endphp
                   @if( $rem!=0)

                                    @if((Auth::user()->role==2)||(Auth::user()->role==5))

                                      @if(Auth::user()->role==2)
                                        <a href="#" class="btn btn-primary d-none d-sm-inline-block" onclick="pay()">
                                          <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                                        
                                        Pay Now
                                        </a>

                                        @endif
                                        @if(Auth::user()->role==5)

                                        
                                        @endif



                                    @endif
                      @else
                       <span style="color:green">
                          Fees Completed
                       </span>
                  
                      @endif
                      </div>


                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>

                          @if(Auth::user()->role==5)

                          <li><a href="{{url('/activated_packages/')}}">Activated Packages</a></li>

                          @else

                          <li><a href="{{url('/students_list/'.$pac_id)}}">{{$subtitle}}</a></li>

                          @endif
                         
                          
                          <li><a href="#">{{$title}}</a></li>
                        </ol>
                      </nav>
                      <h4 class="card-title">{{$title}}</h4>


                      <div class="payment-grd" >

                        <P><b> Total Amount :</b><span> Rs {{number_format($total,2)}}</span>
                        <P><b> Paid Amount :</b><span>Rs {{number_format($paidfees,2)}}</span>
                        <P><b> Remaining Amount :</b><span>
                          
                        Rs {{number_format($rem,2)}}
                        </span>
                      </div>
                    
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Paid Fees</th>
                <th>Pending Fees</th>
                <th>Payment Type</th>
                <th>Transaction Id</th>
                <th>Screenshot</th>
                <th>Date</th>
                <th>Receipt</th>
              
               
            </tr>
        </thead>
        <tbody>
            <?php $i=1;?>
            @foreach($payment_history as $history)
            <tr>
                <td>{{$i}}</td>
                <td>Rs {{number_format($history->paid_amount,2)}}</td>
                <td>Rs {{number_format($history->pending_amount,2)}}</td>
                <td>
                  
                  @if($history->payment_type=='bank')

                  Bank Transfer

                  @else

                  Cash

                  @endif
                  
                  
                  
                 </td>
                <td>{{$history->transaction_id}}</td>
                <td>
                  
                  @if($history->screenshot !='')
                  <img  src="{{url('public/uploads/screenshot/'.$history->screenshot)}}" width="100px" height="100px">
                  @endif
                </td>
               
                <td>{{date("d-m-Y", strtotime($history->date))}}</td>
                <td><a href="{{url('/pdf_download/'.$history->id)}}">Download</a></td>
            </tr>
            <?php $i++;?>
            @endforeach           
        </tbody>     
    </table>

                    </div>
                </div>         
            </div>


           
            <div class="modal modal-blur fade" id="modal-pay-package" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title">Payment</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body" id="studentPackage1">
                
                  <form name="paymentMethod" id="paymentMethod"  enctype="multipart/form-data"   action="{{url('/payment_package')}}"method="POST">
                  <input type="hidden" name="_token" id="token"  value="{{ csrf_token() }}">

                  
                  <input type="hidden" name="studentpackage_id" id="studentpackage_id" value="{{$studentpackage_id}}">
                  <input type="hidden" name="remamount" id="remamount" value="{{$rem}}">
                  <input type="hidden" name="student_id" id="student_id" value="{{$student_id}}">
                  <input type="hidden" name="reference_type" id="reference_type" value="{{$package_details->reference_type}}">
                  <input type="hidden" name="reference_id" id="reference_id" value="{{$package_details->refence_id}}">
                  <input type="hidden" name="reference_id" id="reference_id" value="{{$package_details->refence_id}}">

                  <input type="hidden" name="pac_type" id="pac_type" value="{{$pac_type}}">
                  
                  
                      <div class="d-flex ">
                        <div class="d-flex align-items-center me-4">
                          <input class="me-2" type="radio" id="cash" name="payMethod"  value="cash" checked>
                          <label for="cash">Cash</label>
                        </div>
                        <div class="d-flex align-items-center">
                         <input class="me-2" type="radio" id="banktransfer" name="payMethod" value="bank">
                          <label for="banktransfer">Bank Transfer</label><br>
                        </div>
                      </div>
                      <div id="packagecash">


                        <div class="mb-3">
                          <label class="form-label">Date<sup>*</sup></label>
                          <input type="text" style="width:auto"  data-zdp_readonly_element="true" name="oldpay_date" id="oldpay_date" class="form-control" value="{{date('d-m-Y')}}">
                       </div>
                          <div class="mb-3">
                             <label class="form-label" id="paymethod">Cash<sup>*</sup></label>
                             <input type="number"  min=1 oninput="validity.valid||(value='');"  class="form-control" name="amount" id="amount">
                          </div>
                      <div id="packagebank" style="display:none">
                          <div class="mb-3">
                            <label class="form-label">Payment Screenshot<sup>*</sup></label>
                            <input type="file" class="form-control" name="package_reciept" id="package_reciept">
                          </div>
                          <div class="mb-3">
                            <label class="form-label">Transaction Id<sup>*</sup></label>
                            <input type="text" class="form-control" name="trans_id" id="trans_id">
                          </div>
                      </div>

                 <div class="modal-footer">
                      <button type="submit" class="btn btn-primary ms-auto">PAY</button>
                </div>
               
                  </div>
                 
                  
                  </form>
                 
                </div>
              </div>
            </div>    
  
@endsection
