@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
                   
              <div class="card">
                <div class="card-body">
                  <div class="btn-list" style="float: right">
                     
                    <a href="{{url('/add_company')}}" class="btn btn-primary d-none d-sm-inline-block" >
                      <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                      <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                      Create new 
                    </a>
                    <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal" data-bs-target="#modal-report" aria-label="Create new report">
                      <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                      <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                    </a>
                  </div>
                 
                  <nav aria-label="breadcrumb">
                    <ol class="breadcrumbs">
                      <li><a href="{{url('/home')}}">Home</a></li>
                      <li><a href="#">{{$title}}</a></li>
                    </ol>
                  </nav>
                <h4 class="card-title">{{$title}}</h4>

                <div class="payment-grd">
                  <form id="searchchrys_form" method="GET" enctype="multipart/form-data" action="{{ url('/manage_company') }}">
                      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
  
                      <div class="row">
                          <!-- Year Dropdown -->
                          <div class="col-lg-4 formcontents">
                              <label for="year">Select Year<sup>*</sup></label>
                              <select class="form-select" name="year" id="year">
                                  
                                  @foreach($years as $year)
                                  <option value="{{ $year }}" {{ $year == $currentYear ? 'selected' : '' }}>{{ $year }}</option>
                                  @endforeach
                              </select>
                          </div>
  
                          <!-- Month Dropdown -->
                          <div class="col-lg-6 formcontents">
                              <label for="month">Select Month<sup>*</sup></label>
                              <select class="form-select" name="month" id="month">
                                 
                                  @foreach($months as $key => $month)
                                  <option value="{{ $key }}" {{ $key == $currentMonth ? 'selected' : '' }}>{{ $month }}</option>
                                  @endforeach
                              </select>
                          </div>
  
                          <div class="col-lg-2 formcontents">
                              <button type="submit" class="btn btn-primary" style="margin-top: 26px;">Submit</button>
                        </div>
  
                      </div>
  
                      <div class="col formcontents"> 
                        <a href="{{url('/manage_company/')}}" class="btn btn-primary"  style="margin-top: 26px;"> View Current Month</a>
                        </div>
                  </form>
              </div>


                
                 
                 
                 
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Company Name</th>
                <th>Contact Person Name</th>
                <th>Contact Number</th>
                <th>Email ID</th>
                <th>Created Date</th>
                <th>Logo</th>
                <th>Status</th>
                <th>Action</th>
          
            </tr>
        </thead>
        <tbody>
          <?php $i=1;?>
          @foreach($complist as $complist)
            <tr>
                <td>{{$i}}</td>
                <td>{{ucfirst($complist->name)}}</td>
                <td>{{ucfirst($complist->contact_name)}}</td>
                <td>{{$complist->contact_number}}</td>
                <td>{{$complist->email}}</td>
                <td>{{date("d-m-Y", strtotime($complist->created_at))}}</td>
                <td>

                  @if($complist->company_logo!='')
                  
                  <img  src="{{url('public/uploads/logo/'.$complist->company_logo)}}" alt="Preview" style=" max-width: 100x; max-height: 100px;">
                @endif
                </td>
               <td>

                  @if($complist->status==1)  
                  <span style="color:green;display:block;" id="act_{{$complist->id}}">Active</span>
                  <span style="color:red;display:none;" id="deact_{{$complist->id}}" >Deactive</span>  
                  @else

                  <span style="color:green;display:none;" id="act_{{$complist->id}}">Active</span>
                  <span style="color:red;display:block;" id="deact_{{$complist->id}}" >Deactive</span>  
                      
             
                      
                  @endif


                </td>
              
                <td>
                           @if($complist->status==1)

                            <button type="button" id="actb_{{$complist->id}}"  style="display:none;" class="btn btn-success btn-sm text-white;" fdprocessedid="pvth1" onclick="activate_company('{{$complist->id}}')" >
                           
                             Activate
                            </button>

                            <button type="button" id="deactb_{{$complist->id}}" style="display:block;"  class="btn btn-danger btn-sm text-white" fdprocessedid="pvth1"onclick="deactivate_company('{{$complist->id}}')" >
                              Deactivate
                             </button>


                            @else
                            <button type="button" id="actb_{{$complist->id}}" style="display:block;"   class="btn btn-success btn-sm text-white" fdprocessedid="pvth1" onclick="activate_company('{{$complist->id}}')" >
                            
                              Activate
                             </button>
 
                             <button type="button" id="deactb_{{$complist->id}}" style="display:none;"  class="btn btn-danger btn-sm text-white" fdprocessedid="pvth1" onclick="deactivate_company('{{$complist->id}}')"> 
                              Deactivate
                              </button>
 
                            @endif
                            <a href="{{url('/edit_company/'.$complist->id)}}"><button type="button" class="btn btn-blue btn-sm text-white" fdprocessedid="wf07gv">
                              Edit
                            </button></a>
                           
               
                </td>
               
            </tr>
            <?php $i++;?>
         
            @endforeach
        </tbody>
      
    </table>


 

     <script>
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif
        </script>




@endsection
