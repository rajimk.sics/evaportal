@extends('layouts.main')
@section('content')

<style>

  .formcontents{
    display: flex;
    flex-direction: column;
  }
  label.error{
    color: red;
    order: 3;
  }

  </style>
   <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">   
              <div class="row row-cards">
                <div class="col-12">
                    <input type="hidden" id="token_eva" name="_tokens" value="{{ csrf_token() }}">

                  <form id="addtalentopackage" method="POST" enctype="multipart/form-data"  action="{{url('/assign_chrysalis_package')}}">
                    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="userid" id="userid" value="{{$user_details->id}}">

                    @if($type=='talento')
                    <input type="hidden" name="talentoid" id="talentoid" value="{{$talentoid}}">
                    @endif
                    <input type="hidden" name="type" id="type" value="{{$type}}">

                   

                    
            <nav aria-label="breadcrumb">
              <ol class="breadcrumbs">
                <li><a href="{{url('/home')}}">Home</a></li>

                @if($type=='talento')
                <li><a href="{{url('/manage_talento_registration')}}">Talento Registration </a></li>
                @endif

                @if($type=='all')
                <li><a href="{{url('/all_students')}}">All Students </a></li>
                @endif
                

                <li><a href="#">{{$title}}</a></li>
              </ol>
            </nav>
                    
                    <div class="card-header">
                      <h2 class="fs-title">{{$title}}</h2>
                    </div>

                    <div class="card-body">
                      <div class="">
                          <div class="row">

                          <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-label"> Student Name</label>
                                <input type="text" class="form-control" readonly name="name" id="name" placeholder="Fee" value="{{$user_details->name}}">
                              </div>
                            </div>

                            <div class="col-lg-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" readonly name="email" id="email" value="{{$user_details->email}}"  readonly>
                              </div>
                            </div>
                            <div class="col-lg-6">
                            <div class="form-group">
                                <label>Phone Number</label>
                                <input type="text" class="form-control" readonly name="phone" id="phone" readonly value="{{$user_details->phone}}">
                              </div>
                            </div>

                            <div class="col-lg-6">
                            <div class="form-group">
                                <label>College</label>
                                <input type="text" class="form-control" readonly name="phone" id="phone" readonly value="{{$user_details->college}}">
                              </div>
                            </div>

                            <div class="col-lg-6 formcontents">
                              <div class="form-group">
                              <label class="form-label">Joining Date<sup>*</sup></label>
                              <input type="text" style="width:auto"  data-zdp_readonly_element="true" name="oldpay_date" id="oldpay_date" class="form-control" value="{{date('d-m-Y')}}">
                              </div>
                          </div>

                            <div class="col-lg-6 formcontents">
                                <div class="form-group">
                                <label>Select Package<sup>*</sup></label>
                                      <select class="form-select packname" id="packname" name="packname" >
                                        <option value="">Select Package</option>
                                        @foreach($package as $package)
                                        <option value="{{$package->pac_id}}">{{$package->pac_name}}</option>
                                        @endforeach
                                      </select>
                                  </div>
                                </div>
                          
                            
                                <div class="col-lg-6">
                                  <div class="form-group">
                                        <label>Fees</label> 
                                      <input type="text" class="form-control" readonly name="packfee" id="packfee" placeholder="Fee">
                                  </div>
                              </div>
                            
                              <div class="col-lg-6">
                                  <div class="form-group">
                                    <label>Tax 18%</label>
                                    <input type="text" class="form-control" readonly name="packtax" id="packtax"  readonly>
                                  </div>
                              </div>

                              <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Actual Fees</label>
                                    <input type="text" class="form-control" readonly name="packtot" id="packtot" readonly>
                                </div>
                            </div>
                           
                            <div class="col-lg-6 formcontents">
                              <div class="form-group">
                                  <label class="form-label">Start date  <sup>*</sup></label>
                                  <input type="text" style="width:auto"  data-zdp_readonly_element="true" name="start_date" id="start_date" class="form-control">
                                </div>
                            </div>

                            
                            <div class="col-lg-6 formcontents">
                                <div class="form-group">
                                    <label class="form-label">End date  <sup>*</sup></label>
                                    <input type="text" style="width:auto"  data-zdp_readonly_element="true" name="end_date" id="end_date" class="form-control">
                                  </div>
                              </div>

                              <div class="col-lg-6" id="reductiondiv" style="display: block">
                              
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="reductioncheck" name="reductioncheck" value="1">
                                    <label class="form-check-label" for="flexCheckDefault">
                                    Any Reduction Offered
                                    </label>
                                    </div>
                            </div>
                            <div class="col-lg-6 formcontents" id="reductionvaldiv" style="display: none">
                                <div class="form-group">
                                    <label>Reduction Amount<sup>*</sup></label>
                                    <input type="number" class="form-control" min=1 oninput="validity.valid||(value='');" name="reduction_amount"  id="reduction_amount" >

                                    <input type="hidden" name="max_range" id="max_range" value="">
                                    <span id="reduction_amount_span" style="color:red"> </span>
                                </div>
                            </div>
                            <div class="col-lg-6"  id="reducedfees_div" style="display: none">
                                <div class="form-group">
                                    <label>Reduced Fees</label>
                                    <input type="text" class="form-control"  name="reducedfees" id="reducedfees" readonly>
                                </div>
                            </div>
                            <div class="col-lg-6" id="redtaxdiv" style="display: none">
                                <div class="form-group">
                                    <label> Reduced Tax</label>
                                    <input type="text" class="form-control"  name="reducedtax" id="reducedtax" readonly>
                                </div>
                            </div>
                            <div class="col-lg-6" id="afterreduction_div" style="display: none">
                                <div class="form-group">
                                    <label>Total Fees After Reduction</label>
                                    <input type="text" class="form-control"  name="after_reduction" id="after_reduction" readonly>
                                </div>
                            </div>
                            <div class="col-lg-6" id="ineffect_div" style="display: none">
                                <div class="form-group">
                                    <label>In-Effect Total Reduction Offered</label>
                                    <input type="text" class="form-control"  name="ineffect_offered" id="ineffect_offered" readonly>
                                </div>
                            </div> 

                            <div class="col-lg-6"  style="display:none;">
                                <div class="form-group">
                                    <label>Paid Fees <sup>*</sup></label>
                                    <input type="number" class="form-control" min=1 oninput="validity.valid||(value='');"  name="regfees" id="regfees" >
                                    <span style="color:red" id="regfees_hidden_span"></span>

                                    <input type="hidden" class="form-control"  name="regfees_hidden" id="regfees_hidden" >
 
                                </div>
                            </div> 
                            <input type="hidden" class="form-control"  name="finalamount" id="finalamount" value="">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="">Reference</label>
                                    <select class="form-select selecttype" id="referenece" name="referenece" >
                                      <option value="">Select Reference</option>
                                     
                                      <option value="1">Marketing</option>
                                      <option value="2">Employees</option>
                                      <option value="3">Other</option>
                                     
                                    </select>
                                  </div>
                                </div>
                                <div class="col-lg-6" id="ref_list" style="display:none;">
                                    <div class="form-group">
                                        <label class="">Reference List</label>
                                        <select class="form-select selecttype" id="referenece_list" name="referenece_list" >
                                          
                                         
                                        </select>
                                      </div>
                                    </div>

                        </div>
                      </div>

                    </div>
                  </div>
                  <div class="card-footer text-end">
                    <div class="d-flex">
                      
                      <button type="submit" class="btn btn-primary ms-auto">Submit</button>
                    </div>
                  </div>
                </form>

              
                </div>
           
            </div>

            <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
            <script src="{{url('public/assets/js/scriptpackage.js')}}"></script>    
                   
@endsection
