@extends('layouts.main')

@section('content')
<link rel="stylesheet" href="{{url('public/assets/css/style.css')}}">


<style>

    .formcontents{
      display: flex;
      flex-direction: column;
    }
    label.error{
      color: red;
      order: 3;
    }
  
    </style>
<div class="page-body">
<div class="container-xl">      
<input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
<div class="container-fluid">
  <div class="row justify-content-center">
      <div class="col-lg-12 p-0 mt-0 mb-2">
          <div class="card p-3 mt-0 mb-3">
              <h2 id="heading">Enrollment Form</h2>
              <p class="text-center" >Fill all form field to go to next step</p>
              <div class="progress">
                      <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
              <form id="msform" action="{{url('/save_student')}}"  class="msform"   method="POST" enctype="multipart/form-data" onsubmit="return false;" >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="contact_id" value="{{$contact_id}}">                
                 <div>
                      <!-- progressbar -->
                  <ul id="progressbar">
                      <li class="active" id="account"><strong>Personal</strong></li>
                      <li id="personal"><strong>Qualification</strong></li>
                      <li id="payment"><strong>Srishti Campus</strong></li>
                      <li id="feereduction"><strong>Fees Reduction</strong></li>
                      <li id="feesplitup"><strong>Fees Splitup</strong></li>                 
                  </ul>
                   <br> <!-- fieldsets -->
                  </div>
                  <fieldset>
                      <div class="form-card">
                          <div class="row">
                              <div class="col-9">
                                  <h2 class="fs-title">Account Information:</h2>
                              </div>
                              <div class="col-3">
                                  <h2 class="steps">Step 1</h2>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>FULL NAME<sup>*</sup></label>
                                      <input type="text"   name="stud_name" id="stud_name" class="form-control"  value="{{$name}}">
                                      <span id="error-fname"></span>
                                  </div>
                              </div>
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>PRIMARY CONTACT NO<sup>*</sup></label>
                                      <input type="text" minlength="10" maxlength="13" name="stud_phone" id="stud_phone" value="{{$phone}}"  class="form-control">
                                  </div>
                              </div>
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>SECONDARY CONTACT NO<sup>*</sup></label>
                                      <input type="text" minlength="10" maxlength="13" name="stud_sec_phone" id="stud_sec_phone" value="{{$phone1}}"  class="form-control">
                                  </div>
                              </div>
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>WHATSAPP NUMBER<sup>*</sup></label>
                                      <input type="text" minlength="10" maxlength="13" name="stud_whatsapp_no" id="stud_whatsapp_no"  class="form-control">
                                  </div>
                              </div>
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>PRIMARY EMAIL ID<sup>*</sup></label>
                                      <input type="text" name="stud_email" id="stud_email" value="{{$email}}" readonly  class="form-control">
                                  </div>
                              </div>
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>SECONDARY EMAIL ID(IF ANY)<sup>*</sup></label>
                                      <input type="text" name="stud_sec_email" id="stud_sec_email"  class="form-control">
                                  </div>
                              </div>
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>DATE OF BIRTH<sup>*</sup></label>
                                      <input type="text" name="stud_dob" data-zdp_readonly_element="true"  id="stud_dob" class="form-control">
                                  </div>
                              </div>
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>Blood Group<sup>*</sup></label>
                                      <select name="stud_blood" id="stud_blood" class="form-control stud_blood">
                                          <option value="">Select Blood Group </option>
                                          <option value="A+">A+</option>
                                          <option value="A-">A-</option>
                                          <option value="B+">B+</option>
                                          <option value="B-">B-</option>
                                          <option value="AB+">AB+</option>
                                          <option value="AB-">AB-</option>
                                          <option value="O+">O+</option>
                                          <option value="O-">O-</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>FATHER NAME<sup>*</sup></label>
                                      <input type="text" name="stud_father_name" id="stud_father_name"  class="form-control">
                                  </div>
                              </div>
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>FATHER OCCUPATION<sup>*</sup></label>
                                      <input type="text" name="stud_father_occu" id="stud_father_occu"  class="form-control">
                                  </div>
                              </div>
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>FATHER CONTACT NO<sup>*</sup></label>
                                      <input type="text"  minlength="10" maxlength="13" name="stud_father_no" id="stud_father_no"  class="form-control">
                                  </div>
                              </div>
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>Father mail ID<sup>*</sup></label>
                                      <input type="text" name="stud_father_email" id="stud_father_email"  class="form-control">
                                  </div>
                              </div>
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>MOTHER NAME<sup>*</sup></label>
                                      <input type="text" name="stud_mother_name" id="stud_mother_name"  class="form-control">
                                  </div>
                              </div>
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>MOTHER OCCUPATION<sup>*</sup></label>
                                      <input type="text" name="stud_mother_occu" id="stud_mother_occu"  class="form-control">
                                  </div>
                              </div>
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>MOTHER CONTACT NO<sup>*</sup></label>
                                      <input type="text"  minlength="10" maxlength="13" name="stud_mother_no" id="stud_mother_no"  class="form-control">
                                  </div>
                              </div>
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>Mother Email ID<sup>*</sup></label>
                                      <input type="text" name="stud_mother_email" id="stud_mother_email"  class="form-control">
                                  </div>
                              </div>
                              <div class="col-lg-12 formcontents">
                                  <div class="form-group">
                                      <label>FULL ADDRESS (PERMENANT)<sup>*</sup></label>
                                      <textarea class="form-control" name="stud_per_add" id="stud_per_add" rows="4"></textarea>
                                  </div>
                              </div>
                              <div class="col-lg-12 formcontents">
                                  <div class="form-group">
                                      <label>FULL ADDRESS (TEMPERORY)<sup>*</sup></label>
                                      <textarea class="form-control" name="stud_temp_add" id="stud_temp_add" rows="4"></textarea>
                                  </div>
                              </div>
                               <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>City<sup>*</sup></label>
                                      <input type="text" name="stud_city" id="stud_city"  class="form-control">
                                  </div>
                              </div>
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>Town<sup>*</sup></label>
                                      <input type="text" name="stud_town" id="stud_town"  class="form-control">
                                  </div>
                              </div>
                              <div class="col-lg-12 formcontents">
                                  <div class="form-group">
                                      <label>Upload your Passport Size Photo<sup>*</sup></label>
                                      <input type="file" name="stud_photo" id="stud_photo" class="form-control">
                                  </div>
                              </div>
                              <div class="col-lg-12 formcontents">
                                  <div class="form-group">
                                      <label>Upload the copy of your Aadhar Card<sup>*</sup></label>
                                      <input type="file" name="stud_aadh_photo" id="stud_aadh_photo" class="form-control">
                                  </div>
                              </div>
                          </div>
                          
                    
                      </div> <input type="button" name="next" class="next action-button tab1" id="nextBtn" value="Next" />
                  </fieldset>
                  <fieldset>
                      <div class="form-card">
                          <div class="row">
                              <div class="col-9">
                                  <h2 class="fs-title">Educational Qualification</h2>
                              </div>
                              <div class="col-3">
                                  <h2 class="steps">Step 2</h2>
                              </div>
                          </div> 
                           <div class="row">
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>QUALIFICATION<sup>*</sup></label>
                                      <select name="stud_qualification" id="stud_qualification" class="form-control stud_qualification">
                                        <option value="">Select</option>
                                        @foreach($qualification as $qualification)
                                            <option value="{{$qualification->id}}">{{$qualification->qualification}}</option>
                                        @endforeach
                                    </select>
                                     
                                  </div>
                              </div>
                               <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>INSTITUTION LAST STUDIED<sup>*</sup></label>
                                      <input type="text" name="stud_institute" id="stud_institute"  class="form-control">
                                  </div>
                              </div><div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>YEAR OF PASSOUT<sup>*</sup></label>
                                      <select name="stud_year_of_pass" id="stud_year_of_pass" class="form-control stud_year_of_pass">
                                        <option value="">Select Year</option>
                                            <?php $currentYear = date("Y");
                                                // Define a range of years (adjust as needed)
                                                $startYear = $currentYear - 50;
                                                $endYear = $currentYear;
                                                // Loop to generate dropdown options for years
                                            for ($year = $endYear; $year >= $startYear; $year--) {
                                                    echo "<option value=\"$year\">$year</option>";
                                            }
                                            ?>
                                    </select>
                                     
                                  </div>
                              </div><div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                        <label>DO YOU HAVE ANY ARREARS?<sup>*</sup></label>
                                       <select name="stud_arrears" id="stud_arrears" class="form-control">
                                          <option value="">Select</option>
                                          <option value="No">No</option>
                                          <option value="Yes">Yes</option>
                                          <option value="Awaiting Result">Awaiting Result</option>
                                      </select>
                                  </div>
                              </div><div class="col-lg-6 formcontents"  id="arrear" style="display: none;">
                                  <div class="form-group">
                                      <label>How many Arrears you have?<sup>*</sup></label>
                                      <select name="stud_arrears_no" id="stud_arrears_no" class="form-control stud_arrears_no">
                                        <option value="">Select</option>
                                      <?php
                                      for ($i = 1; $i <= 30; $i++) {
                                        echo "<option value=\"$i\">$i</option>";
                                        }
                                        ?>
                                      </select>
                                  </div>
                              </div><div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>Specialization<sup>*</sup></label>
                                      <select name="stud_specialization" id="stud_specialization" class="form-control stud_specialization">
                                            <option value="">Select</option>
                                          @foreach($specialization as $specialization)
                                          <option value="{{$specialization->id}}">{{$specialization->specialization}}</option>
                                          @endforeach
                                      </select>
                                  </div>
                              </div>
                               <div class="col-lg-12 formcontents">
                                  <div class="form-group">
                                      <label>Percentage Aggregate<sup>*</sup></label>
                                      <select name="stud_percentage" id="stud_percentage" class="form-control stud_percentage">
                                           <option value="">Select Percentage</option>
                                            <?php
                                            
                                                // Loop to generate dropdown options for years
                                            for ($i =1; $i <= 100; $i++) {
                                                    echo "<option value=\"$i\">$i</option>";
                                            }
                                            ?>
                                    </select>
                                      
                                    
                                      <p>You can mention the CGPA or consolidated % score for your degree.</p>
                                  </div>
                              </div>
                               <div class="col-lg-12 formcontents">
                                  <div class="form-group">
                                      <label>Institution in which you have completed your Higher Secondary Education.
<sup>*</sup></label>

                                      <input type="text" name="stud_hsst_name" id="stud_hsst_name"  class="form-control">
                                  </div>
                              </div>
                               <div class="col-lg-12 formcontents">
                                  <div class="form-group">
                                      <label>% scored in Higher Secondary Education<sup>*</sup></label>
                                      <select name="stud_hsst_per" id="stud_hsst_per" class="form-control stud_hsst_per">
                                        <option value="">Select Percentage</option>
                                         <?php
                                         
                                             // Loop to generate dropdown options for years
                                         for ($i =1; $i <= 100; $i++) {
                                                 echo "<option value=\"$i\">$i</option>";
                                         }
                                         ?>
                                 </select>
                                
                                  </div>
                              </div>
                               <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>Year of completion of Higher Secondary Education<sup>*</sup></label>
                                      <select name="stud_hsst_year" id="stud_hsst_year" class="form-control stud_hsst_year">
                                        <option value="">Select Year</option>
                                            <?php $currentYear = date("Y");
                                                // Define a range of years (adjust as needed)
                                                $startYear = $currentYear - 50;
                                                $endYear = $currentYear;
                                                // Loop to generate dropdown options for years
                                            for ($year = $endYear; $year >= $startYear; $year--) {
                                                    echo "<option value=\"$year\">$year</option>";
                                            }
                                            ?>
                                    </select>
                                  </div>
                              </div>
                               <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>Institution in which you have completed your Senior Secondary Education<sup>*</sup></label>
                                     <input type="text" name="stud_ss_name" id="stud_ss_name"  class="form-control">
                                  </div>
                              </div>
                               <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>% scored in Senior Secondary Education.<sup>*</sup></label>

                                      <select name="stud_ss_per" id="stud_ss_per" class="form-control stud_ss_per">
                                        <option value="">Select Percentage</option>
                                         <?php
                                         
                                             // Loop to generate dropdown options for years
                                         for ($i =1; $i <= 100; $i++) {
                                                 echo "<option value=\"$i\">$i</option>";
                                         }
                                         ?>
                                 </select>
                                  </div>
                              </div>
                               <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>Year of completion of Senior Secondary Education.<sup>*</sup></label>
                                      <select name="stud_ss_year" id="stud_ss_year" class="form-control stud_ss_year">
                                        <option value="">Select Year</option>
                                            <?php $currentYear = date("Y");
                                                // Define a range of years (adjust as needed)
                                                $startYear = $currentYear - 50;
                                                $endYear = $currentYear;
                                                // Loop to generate dropdown options for years
                                            for ($year = $endYear; $year >= $startYear; $year--) {
                                                    echo "<option value=\"$year\">$year</option>";
                                            }
                                            ?>
                                    </select>
                                                                    </div>
                              </div>
                          </div>
                          
                      </div> <input type="button" name="next" class="next action-button" value="Next" /> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                  </fieldset>
                  <fieldset>
                      <div class="form-card">
                          <div class="row">
                              <div class="col-9 formcontents">
                                  <h2 class="fs-title">Get your facts updated with Srishti Campus</h2>
                              </div>
                              <div class="col-3">
                                  <h2 class="steps">Step 3</h2>
                              </div>
                          </div>
                           <div class="row">
                               <div class="col-lg-12">
                                   <p>This is being collected to have a transparent communication among the details communicated between the POC of Srishti Campus & You. We value what we commit.</p>
                               </div>
                              <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>DATE OF JOINING with Srishti Campus<sup>*</sup></label>
                                      <input type="text" style="width:auto" value="<?php echo  date('d-m-Y') ?>" name="stud_doj" id="stud_doj" class="form-control">
                                  </div>
                              </div>
                               <div class="col-lg-12 formcontents">
                                  <div class="form-group">
                                      <label>Do you have any preference with Class Start Dates?<sup>*</sup></label>
                                      <p>Classes with Srishti Campus will be commenced on specific dates Only. We will try to accommodate your preference while scheduling the classes. If, not kindly align with the schedule for a smooth running of your training. You can discuss with your POC for the latest information about the next class start date.</p>         
                                      <input type="text"  data-zdp_readonly_element="true" name="stud_pref_doj" id="stud_pref_doj" class="form-control">
                                  </div>
                              </div>
                               <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>Technology<sup>*</sup></label>
                                      <select name="stud_technology" id="stud_technology" class="form-control stud_technology">
                                          <option value="">Select Technology</option>
                                          @foreach($technology as $technology)
                                          <option value="{{$technology->id}}">{{ $technology->technology}}</option>
                                          @endforeach                          
                                      </select>
                                  </div>
                              </div>
                               <div class="col-lg-6 formcontents">
                                  <div class="form-group">
                                      <label>You POC Code<sup>*</sup></label>
                                      <input type="text" name="stud_poc_code" id="stud_poc_code"  class="form-control" readonly value="{{$poc_code}}">
                                  </div>
                              </div>
                               <div class="col-lg-12 formcontents">
                                  <div class="form-group">
                                      <label>Specify in detail about your understanding with the package offered by your POC with Srishti Campus<sup>*</sup></label>
                                      <p>Please mention if you are offered with any specific ad-on. Eg: Certificates, projects in which you work, any referral discounts etc.</p>
                                      <input type="text" name="stud_ad_on" id="stud_ad_on"  class="form-control">
                                  </div>
                              </div>
                              
                                <div class="col-lg-12 formcontents">
                                  <div class="form-group">
                                      <label>Are you opting for Placement Training?<sup>*</sup></label>
                                      <p>This is applicable only for Internship students. We offer placement training which includes both aptitude and other technology training for the students who join with us for internship for FREE of cost. Kindly show your interest in attending the same.</p>
                                      <select name="stud_place_training" id="stud_place_training" class="form-control">
                                        <option value="">Select</option>
                                          <option value="Yes">Yes</option>
                                          <option value="No">No</option>
                                          <option value="Maybe">Maybe</option>
                                      </select>
                                  </div>
                              </div>
                               <div class="col-lg-12 formcontents">
                                  <div class="form-group">
                                      <label>Upload your Payment Screenshot</label>
                                      <div class="drop-zone">
                                       <i class="bi bi-cloud-arrow-up"></i>   
                                      <span class="drop-zone__prompt">Drop file here or click to upload</span>
                                      <input type="file"  name="stud_resume" id="stud_resume" class="drop-zone__input">
                                    </div>
                                  </div>
                              </div>
                            </div>
                        </div> <input type="button" name="su" class="next action-button" value="Next" /> 
                        <input type="button" name="previous" class="previous action-button-previous" value="Previous" />        
                    </fieldset>
                    <fieldset>
                        <div class="form-card">
                            <div class="row">
                                <div class="col-9">
                                    <h2 class="fs-title">Fees Reduction</h2>
                                </div>
                                <div class="col-3">
                                    <h2 class="steps">Step 4</h2>
                                </div>
                            </div>
                             <div class="row">
                                
                                <div class="col-lg-6 formcontents">
                                    <div class="form-group">
                                        <label>Referal code for Reduction</label>
                                        <div class="d-flex">
                                        <input type="text"   name="reduction_code" id="reduction_code" class="form-control" value="{{$referal_code}}" readonly>
                                        <button class="next1 action-button1" onclick="discountdetails()">Redeem</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 formcontents">
                                    <div class="form-group">
      
                                       
                                    <div id="notreduction"></div>

                                    <div id="reductiondetails" style="display: none;">




                                    </div>


                                    </div>
                                </div>
                               
                              </div>
                          </div> <input type="button" name="next" class="next action-button" value="Next" /> 
                          <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                
                      </fieldset>

                    <fieldset>
                        <div class="form-card">
                            <div class="row">
                                <div class="col-9">
                                    <h2 class="fs-title">Fees Splitup</h2>
                                </div>
                                <div class="col-3">
                                    <h2 class="steps">Step 5</h2>
                                </div>
                            </div>
                             <div class="table-responsive">

                                <table class="table" id="splitup">
                                    <thead>
                                      <tr>
                                        <th scope="col">InstalmentNo:</th>
                                        <th scope="col">Fees</th>
                                        <th scope="col">Paid Fees</th>
                                        <th scope="col">Pending Fees</th>
                                        <th scope="col">Due Date</th>
                                      </tr>
                                    </thead>
                                    <tbody>
      <?php $i=1;?>
                                      @foreach($feessplitup  as $splitup)
                                      <tr>
                                        <td>{{$i}}
                                        <td>
      
                                           Rs {{number_format($splitup->fees,2)}}
      
                                         
                                        </td>
                                        
                                        <td>
                                         
                                         Rs {{number_format($splitup->paidfees,2)}}
                                        
                                        </td>
                                        <td>
                                         
                                          Rs {{number_format($splitup->pendingfees,2)}}
                                        
      
                                        </td>
                                        <td>{{$splitup->date}}</td>
      
                                      </tr>
                                      <?php $i++ ?>
                                      @endforeach                                     
                                    </tbody>
                                  </table>
                                
                             
                            </div>
                          </div> 
                          <input type="submit" name="next" class="admissionsubmit  action-button" value="Submit" /> 
                          <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                
                      </fieldset>
               
              </form>
          </div>
      </div>
  </div>
</div>



            <script>
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif
        </script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<script src="{{url('public/assets/js/script.js')}}"></script>
@endsection
