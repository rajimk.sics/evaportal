@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">
              <input type="hidden" name="logname" id="logname" value="{{ Auth::user()->name }}">
              <div class="card">
                <div class="card-body">

                    
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>
                          <li><a href="#">{{$title}}</a></li>
                        </ol>
                      </nav>
                   
                    <h4 class="card-title">{{$title}}</h4>

                    <div class="payment-grd">
                      <form id="searchchrys_form" method="GET" enctype="multipart/form-data" action="{{ url('/pending_paymentrequest') }}">
                          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
      
                          <div class="row">
                              <!-- Year Dropdown -->
                              <div class="col-lg-4 formcontents">
                                  <label for="year">Select Year<sup>*</sup></label>
                                  <select class="form-select" name="year" id="year">
                                      
                                      @foreach($years as $year)
                                      <option value="{{ $year }}" {{ $year == $currentYear ? 'selected' : '' }}>{{ $year }}</option>
                                      @endforeach
                                  </select>
                              </div>
      
                              <!-- Month Dropdown -->
                              <div class="col-lg-6 formcontents">
                                  <label for="month">Select Month<sup>*</sup></label>
                                  <select class="form-select" name="month" id="month">
                                     
                                      @foreach($months as $key => $month)
                                      <option value="{{ $key }}" {{ $key == $currentMonth ? 'selected' : '' }}>{{ $month }}</option>
                                      @endforeach
                                  </select>
                              </div>
      
                              <div class="col-lg-2 formcontents">
                                  <button type="submit" class="btn btn-primary" style="margin-top: 26px;">Submit</button>
                            </div>
      
                          </div>
      
                          <div class="col formcontents"> 
                            <a href="{{url('/pending_paymentrequest/')}}" class="btn btn-primary"  style="margin-top: 26px;"> View Current Month</a>
                            </div>
                      </form>
                  </div>




                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Student Name</th>
                <th>Payment Method</th>
               
                <th>Amount</th>
                <th>Transaction ID</th>
                <th>Screenshot</th>
                <th>Requested Date</th>
                <th>Status</th>
               <th>Action</th>
            </tr>
        </thead>
        <tbody>
          <?php $i=1;?>
            @foreach($paylist as $paylist)
            <tr id="row_{{$paylist->id}}">
                <td>{{$i}}</td>  
                <td>{{ucfirst($paylist->name)}}</td>            
                <td>
                @if($paylist->payment_method=='cash')  
                Cash
                @else
                Bank
                @endif
              </td>
              <td>Rs {{number_format($paylist->amount,2)}}</td>
              <td>{{$paylist->transaction_id}}</td>
              <td>
                  
                @if($paylist->payment_screenshort !='')
                <img  src="{{url('public/uploads/screenshot/'.$paylist->payment_screenshort)}}" width="100px" height="100px">
                @endif
              </td>
              <td>{{date('d-m-Y', strtotime($paylist->date))}}</td>
                <td>
                @if($paylist->status==1)
               
                <span style="color:green;display: block" id="ap_{{$paylist->id}}" >Approved</span>
                <span style="color:orange;display: none" id="pe_{{$paylist->id}}">Pending</span>
                <span style="color:red;display: none"  id="re_{{$paylist->id}}" >Rejected</span>     


                @elseif($paylist->status==0)


                <span style="color:green;display: none" id="ap_{{$paylist->id}}" >Approved</span>
                <span style="color:orange;display: block" id="pe_{{$paylist->id}}">Pending</span>
                <span style="color:red;display: none"  id="re_{{$paylist->id}}" >Rejected</span>     

                @elseif($paylist->status==2)

                <span style="color:green;display: none" id="ap_{{$paylist->id}}" >Approved</span>
                <span style="color:orange;display: none" id="pe_{{$paylist->id}}">Pending</span>
                <span style="color:red;display: block"  id="re_{{$paylist->id}}" >Rejected</span>                
                @endif
                 </td>               
                  <td>              
                   @if($paylist->status==0)
                       
                   <div id="button_{{$paylist->id}}">
                        <button type="button" class="btn btn-success btn-sm text-white" fdprocessedid="pvth1" onclick="approvePayment('{{$paylist->id}}')">
                              Approve
                        </button>
                        <button type="button" class="btn btn-danger btn-sm text-white" fdprocessedid="pvth1" onclick="rejectPayment('{{$paylist->id}}')">
                              Reject
                        </button>

                  </div>
                  <span id="action_{{$paylist->id}}"></span></br>
                  <span id="reasonby_{{$paylist->id}}"></span>
                  @endif
                        @if($paylist->status==1)
                        @php
                          $name=App\Helpers\CustomHelper::uname($paylist->action_by); 
                        @endphp
                        <span id="action_{{$paylist->id}}">Approved By {{$name['name']}}</span>

                        @endif
                        @if($paylist->status==2)

                        
                        @php
                        $name=App\Helpers\CustomHelper::uname($paylist->action_by); 
                       @endphp
                        <span id="action_{{$paylist->id}}">Rejected By {{$name['name']}}</span>
                     
                       <span id="reasonby_{{$paylist->id}}"> Reason:{{$paylist->reason}}</span>
                        @endif
    
                 </td>
               </tr>
            <?php $i++;?>
            @endforeach        
        </tbody>  
    </table>

                    </div>
                </div>
           
            </div>

@endsection
