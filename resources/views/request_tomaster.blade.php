@extends('layouts.main')

@section('content')

    <!-- Page body -->
    <div class="page-body">
        <div class="container-xl">

            <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">

            <div class="card">
                <div class="card-body">
                    <div class="btn-list" style="float: right">
                     
                        <a  class="btn btn-primary d-none d-sm-inline-block" onclick="reqtoMaster()" >
                        
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                          Request new 
                        </a>
                        <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal" data-bs-target="#modal-report" aria-label="Create new report">
                       
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                        </a>
                      </div>
                   

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                            <li><a href="{{ url('/home') }}">Home</a></li>
                            <li><a href="{{ url('/request_tomaster') }}">{{ $title }}</a></li>
                        </ol>
                    </nav>

                    <h4 class="card-title">{{ $title }}</h4>


                     <div class="payment-grd">
                        <form id="searchreqmas_form" method="GET" enctype="multipart/form-data" action="{{ url('/request_tomaster') }}">
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <div class="row">
                      <div class="col">
                        <label for="year">Select Status<sup>*</sup></label>
                        <select class="form-select" name="status" id="status">
                          <option value="">Select Status</option>
                          <option value="0" {{ request('status') == '0' ? 'selected' : '' }}>Pending</option>
                          <option value="1" {{ request('status') == '1' ? 'selected' : '' }}>Approved</option>
                          <option value="2" {{ request('status') == '2' ? 'selected' : '' }}>Rejected</option>
                        </select>
                    </div>

                    <div class="col d-flex align-items-center">
                      <div class="form-group formcontents mb-0 flex-row">
                      <button type="submit" class="btn btn-primary me-2">Submit</button>
                      <a href="{{url('/request_tomaster')}}" class="btn btn-primary"> Clear All</a>
                      </div>
                      
                    </div>
               
            </form>
        </div> 
                </div>
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>College</th>
                                    <th>Department</th>
                                    <th>POC</th>
                                    <th>From Date</th>
                                    <th>To Date   </th>
                                    <th>Supported Document</th>
                                    <th>Status</th>
                                    <th>Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach ($listings as $listing)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>{{$listing->college}}</td>
                                        <td>{{$listing->dept}}</td>
                                        <td>{{$listing->department_pocs}}</td>
                                        <td>{{date("d-m-Y", strtotime($listing->from_date))}}</td>
                                        <td>{{date("d-m-Y", strtotime($listing->to_date))}}</td>
                                      
                                        <td> @if (!empty($listing->supporting_documents))
                                            @foreach (explode(',', $listing->supporting_documents) as $fileName)
                                                <a href="{{ url('/public/uploads/masterdocuments/' . $fileName) }}" target="_blank" class="view-document">{{ $fileName }}</a><br>
                                            @endforeach
                                        @else
                                            No documents uploaded
                                        @endif
                                    </td>

                                    <td>
                                      @if ($listing->status == 1)
                                        <span style="word-wrap: break-word; max-width: 150px;color:green;display:block;">Approved<br>Comment:{{$listing->master_comment}}</span>
                                      @elseif ($listing->status == 2 )
                                        <span style="word-wrap: break-word; max-width: 150px;color:red;display:block;">Rejected<br>{{$listing->reason}}</span>
                                        <span style="color:rgb(76, 0, 255);display:none;" id="appeal{{ $listing->id }}"></span>
                                      @elseif ($listing->status == 0)
                                            <span style="color:#FFC107;display:block;">Pending</span> 
                                       @endif
                                       @if ($listing->appeal_status == 1)
                                       <span style="word-wrap: break-word; max-width: 150px;color:rgb(38, 0, 255);display:block;"  id="appeal_{{ $listing->id }}">Appealed<br>Reason:{{$listing->appeal_reason}}</span>
                                      
                                       @endif
                                      </td>

                                    <td>
                                      <button type="button" class="btn btn-warning hover:bg-orange-600 btn-sm text-white" id="appeal-btn-{{ $listing->id }}" 
                                        onclick="appealReq('{{ $listing->id }}')" {{ ($listing->status == 2) && ($listing->appeal_status == 0) ? '' : 'disabled' }}>
                                        Appeal</button>
                                    </td>
                                       </tr>
                                    <?php $i++; ?>
                                @endforeach

                            </tbody>

                        </table>



                    </div>
                </div>

            </div>

            </div>
            </div>

                       
                         <!-- Request To Master -->
<div class="modal fade" id="requestToMasterModal" tabindex="-1" aria-labelledby="requestToMasterLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="requestToMasterLabel">Request to Master</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form id="requestForm" enctype="multipart/form-data" method="POST" action="{{ url('/save_masterRequest') }}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          
          <div class="modal-body">
            <div class="row">
                <div class="mb-3">
                    <label for="department" class="form-label">College<sup>*</sup></label>
                    <select class="form-control selectclass" id="collegeId" name="collegeId">
                      <option value="">Select College</option>
                    </select>
                  </div>
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="fromDate" class="form-label">From Date</label>
                <input type="text" class="form-control" id="from_date" name="from_date" readonly>
              </div>
              <div class="col-md-6 mb-3">
                <label for="toDate" class="form-label">To Date<sup>*</sup></label>
                <input type="text" class="form-control" id="to_date" name="to_date">
              </div>
            </div>     
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="department" class="form-label">Department<sup>*</sup></label>
                <select class="form-control selectclass" id="department" name="department">
                  <option value="">Select Department</option>
                </select>
              </div>
              <div class="col-md-6 mb-3">
                <label for="poc" class="form-label">POC<sup>*</sup></label>
                <select class="form-control selectclass pocmulti" id="pocId" name="pocId[]" multiple="multiple">
                  <option value="">Select POC</option>
                </select>
              </div>
            </div>
            <div class="mb-3">
              <label for="supportedDocuments" class="form-label">Supported Document<sup>*</sup>
              <button type="button" name="addDoc" id="addDoc" class="btn btn-success">Add +</button></label>
              <input type="file" class="form-control fileclass" id="documents" name="documents[]" accept=".pdf, .jpg, .jpeg, .png">
            </div>
            <div id="text-box-container">
              <div class="doc-group" >
            </div>
        </div>
            <div class="mb-3">
              <label for="comment" class="form-label">Comment</label>
              <textarea class="form-control" id="comment" name="comment" rows="3"></textarea>
            </div>
          </div>
  
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Send Request</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  
            <script>
                @if (Session::has('message'))
                    swal({
                        title: "",
                        text: "{{ Session::get('message') }}",
                        type: "success",
                        showCancelButton: false,
                        dangerMode: false,
                        confirmButtonText: 'OK',
                    });
                @endif
            </script>
        @endsection
