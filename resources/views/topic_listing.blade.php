@extends('layouts.main')

@section('content')
    <div class="page-body">
        <div class="container-xl">
            <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::has('duplicates'))
        <div class="alert alert-warning">
            <p><strong>Duplicate values found:</strong></p>
            <ul>
                @foreach (Session::get('duplicates') as $duplicate)
                    <li>
                        <strong>Duplicate Value:</strong> {{ $duplicate['dupli'] }}
                        <strong>Technology:</strong> {{ $duplicate['technology'] }}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif


            <div class="card">
                <div class="card-body">
                    <nav aria-label="breadcrumb mb-3">
                        <ol class="breadcrumbs">
                            <li><a href="{{ url('/home') }}">Home - Mentor</a></li>

                            <li><a href="{{ url('/topicListingByTechnology') }}">Syllabus - Topics</a></li>

                        </ol>
                    </nav>
                   
                
                    <form id="topic_filter" method="GET" enctype="multipart/form-data"
                        action="{{ url('/topicListingByTechnology') }}">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                        <div class="row mt-3">
                            <div class="col">
                                <div class="form-group formcontents">
                                    <label for="techname">Please Select a Technology</label>
                                    <select name="techname[]" id="techname" class="form-select selecttech" multiple>
                                        <option value="" disabled>All Technologies</option>
                                        @foreach ($technologies as $technology)
                                            <option value="{{ $technology->id }}"
                                                {{ in_array($technology->id, (array) $tech_id) ? 'selected' : '' }}>
                                                {{ $technology->technology }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col d-flex align-items-center">
                                <div class="form-group formcontents mb-0 flex-row">
                                    <button type="submit" class="btn btn-primary me-2">Submit</button>
                                    <a href="{{ url('/topicListingByTechnology') }}" class="btn btn-primary">Clear All</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="card">

                        <div>
                            <b>
                                <h5 class="card-title" id="topicsheading" style="display: none">Topics</h5>
                            </b>
                        </div>
                    
                        <div class="card-body mt-1">
                            <div class="row">
                                <div class="col-10">
                                    <div class="card" style="width: 75vw;">

                                        <ul class="list-group list-group-flush">
                                            <div class="accordion border border-0" id="accordionExample">
                                                <div id="topiclistingbytech">

                                                
                                            </div>

                                            </div>
                                        </ul>
                                       
                                        
                                        <form id="csvsubupload" method="POST" enctype="multipart/form-data"
                                        action="{{ url('/subtopicUploadByTechnology') }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        {{-- model --}}
                                        <div class="modal fade" id="subtopicCSV" tabindex="-1"
                                            aria-labelledby="modalCSVLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="modalCSVLabel">Upload CSV
                                                            File</h5>
                                                        <button type="button" class="btn-close"
                                                            data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div>
                                                            <label for="technology" class="mb-1"><b>Select
                                                                    Technology</b></label>
                                                            <select id="technology" name="technology[]"
                                                                class="form-select selecttech" multiple>
                                                                <option value="" disabled>Select
                                                                    Technology</option>

                                                            </select>
                                                            <div class="form-text mt-1">Choose one or more
                                                                technologies</div>
                                                        </div>
                                                        <div class="mb-3">
                                                            <label for="csvfile" class="form-label">Select
                                                                CSV
                                                                File</label>
                                                            <div class="mt-1 mb-2"><a style="color: black;"
                                                                    href="{{ url('/public/uploads/excel/subtopics.csv') }}"
                                                                    download="subtopics"
                                                                    title="Subtopics File Format"> Sample csv
                                                                    format </a></div>


                                                            <input type="file" class="form-control"
                                                                name="csv_file" id="csvfile" required>
                                                            <input type="hidden" name="tech_id"
                                                                id="tech_id">
                                                            <input type="hidden" name="topic_id"
                                                                id="topic_id">


                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-bs-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary"
                                                            id="upload-csv">Upload</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </form>

                                    </div>
                                    <div class="text-center mt-3">
                                        <button id="loadMoreTopicsButton" class="btn btn-primary" style="display: none;">
                                            Show More
                                        </button>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
