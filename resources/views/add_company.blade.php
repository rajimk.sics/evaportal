@extends('layouts.main')
@section('content')
   <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">   
              <div class="row row-cards">
                <div class="col-12">

                  <form id="addcompany" method="POST" enctype="multipart/form-data"  action="{{url('/save_company')}}">
                    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
  
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumbs">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="#">{{$title}}</a></li>
                  </ol>
                </nav>
                    <div class="card-header">
                      <h2 class="fs-title">{{$title}}</h2>
                    </div>

                    <div class="card-body">
                      <div class="">
                          <div class="row">
                            <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-label">Company Name<sup>*</sup></label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Name" >
                              </div>
                            </div>
                            <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-label">Contact Person Name<sup>*</sup></label>
                                <input type="text" class="form-control" name="cpname" id="cpname" placeholder="Contact Person Name" >
                              </div>
                            </div>
                            <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-label">Contact  Number<sup>*</sup></label>
                                <input type="text" class="form-control contactnum" name="cnumber" minlength="10"  maxlength="13" id="cnumber" placeholder="Contact Number">
                              </div>
                            </div>
                            <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-label">Email<sup>*</sup></label>
                                <input type="email"  class="form-control contactnum" name="cemail" id="cemail" placeholder="Email">
                              </div>
                            </div>
                           
                            <div class="col-lg-6">
                              <div class="form-group">
                                  <label class="form-label">Company Logo<sup>*</sup></label>
                                  <input type="file" accept="image/*" class="form-control" name="file" id="file" >
                                </div>
                            </div>
                            <div class="mb-3" id="prview" style="display:none;">
                              <label class="form-label">Current Logo</label>
                            
                             <img id="previewHeader" src="" alt="Preview" style=" max-width: 150x; max-height: 100px;">
                         </div>


                        </div>
                      </div>


                    </div>
                  </div>
                  <div class="card-footer text-end">
                    <div class="d-flex">
                      
                      <button type="submit" class="btn btn-primary ms-auto" id="submit">Submit</button>
                    </div>
                  </div>
                </form>

              
                </div>
           
            </div>

            <script>
        
             
              </script>
            
            <script>
            @if (Session::has('errormessage'))
            swal({
                title: "",
                text: "{{ Session::get('errormessage') }}",
                type: "sucess",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif 
      </script>         
@endsection
