@extends('layouts.main')
@section('content')

<style>
  .formcontents {
    display: flex;
    flex-direction: column;
  }

  label.error {
    color: red;
    order: 3;
  }
</style>
<!-- Page body -->
<div class="page-body">
  <div class="container-xl">
    <div class="row row-cards">
      <div class="col-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumbs">
            <li><a href="{{url('/home')}}">Home</a></li>
            <li><a href="#">{{$title}}</a></li>
          </ol>
        </nav>
        <div class="card-header">
          <h4 class="fs-title" style="font-size: 20px;">{{$title}}</h4>
        </div>
        @if(Session::has('digit'))
        <div class="alert alert-warning">
          <p>Invalid Contact Number</p>
          <ul>
            @foreach(Session::get('digit') as $digit)
            <li>{{ $digit }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        @if(Session::has('duplicates'))
        <div class="alert alert-warning">
          <p>Duplicate values:</p>
          <ul>
            @foreach(Session::get('duplicates') as $duplicate)
            <li>{{ $duplicate }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        <a href="{{url('/public/uploads/excel/candidatedetails.xlsx')}}" download="CandidateData">Download Sample Excel Template </a>

        <form id="bulk_candidate" action="{{ url('bulkupload_candidate') }}" method="POST" enctype="multipart/form-data">
          <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
          <div class="card-body">
            <div class="">
              <div class="row">
                <div class="col-lg-6 formcontents" id="list">
                  <div class="form-group">
                    <label class="form-label">Upload Candidate <sup>*</sup></label>
                    <input class="form-control" id="candidate_file" type="file" name="candidate_file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
      <div class="card-footer text-end">
        <div class="d-flex">

          <button type="submit" class="btn btn-primary ms-auto">Upload</button>
        </div>
      </div>
      </form>

    </div>

  </div>

  @endsection