@extends('layouts.main')
@section('content')


<style>

  .formcontents{
    display: flex;
    flex-direction: column;
  }
  label.error{
    color: red;
    order: 3;
  }

  </style>
   <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">   
              <div class="row row-cards">
                <div class="col-12">
                  <nav aria-label="breadcrumb">
                    <ol class="breadcrumbs">
                      <li><a href="{{url('/home')}}">Home</a></li>
                      <li><a href="#">{{$title}}</a></li>
                    </ol>
                  </nav>
                  
                    <div class="card-header">
                      <h4 class="card-title"></h4>{{$title}}
                    </div>
                   
<form id="package" method="POST" enctype="multipart/form-data"  action="{{url('/save_package')}}">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">

      <div class="card-body">
        <div class="row">
          <div class="col-md-6 col-xl-12">
            <div class="row">
          <div class="col-md-6 col-xl-6">
   
          <div class="mb-3">
            <label class="form-label">Name<sup>*</sup></label>
            <input type="text" class="form-control" name="packname" id="packname" placeholder="Package name">
          </div>

          <div class="mb-3 formcontents">
            <label class="form-label">Duration<sup>*</sup></label>
            <select class="form-select selecttype" id="duration" name="duration" >
                                  <option value="">Select Duration</option>
                                  @for($j=1;$j<=12;$j=$j+.5)
                                  <option value="{{$j}}">{{$j}} Months</option>
                                @endfor  
                            </select>
          </div>
    
          <div class="mb-3">
            <label class="form-label">Course ID<sup>*</sup></label>
            <input type="text" class="form-control" name="packcourseid" id="packcourseid" placeholder="CourseID">
          </div>
        
         
          <div class="mb-3">
            <label class="form-label">Cost<sup>*</sup></label>
            <input type="text" class="form-control"  min=1 oninput="validity.valid||(value='');" name="packfee" id="packfees" placeholder="Fee">
          </div>
          <div class="mb-3">
            <label class="form-label">Tax {{$tax}}%<sup>*</sup></label>
            <input type="text" class="form-control" name="packtax" id="packtax"  readonly>
            <input type="hidden" class="form-control" name="defaulttax" id="defaulttax" value="{{$tax}}">
          </div>
          <div class="mb-3">
            <label class="form-label">Cost Total<sup>*</sup></label>
            <input type="text" class="form-control" name="packtot" id="packtot" readonly>
          </div>
        </div>

        <div class="col-md-6 col-xl-6">
          <div class="mb-3">
            <label class="form-label">Package Type<sup>*</sup></label>
            <select class="form-select pack_duration selecttype" id="pac_type" name="pac_type" >
                                  <option value="">Select Package Type</option>
                                  <option value="1">Regular</option>
                                  <option value="2">Chrysalis</option>
                                
                                       </select>
          </div>
          <div class="mb-3">
            <label class="form-label">Hours<sup>*</sup></label>
            <select class="form-select pack_hours selecttype" id="packhrs" name="packhrs">
                                  <option value="">Select Hours</option>
                           @for($j=1;$j<=24;$j++)
                                  <option value="{{$j}}">{{$j}} Hours</option>
                                @endfor
                                </select>
          </div>
          <div class="mb-3">
            <label class="form-label">Technology Name<sup>*</sup></label>
            <select class="form-select pack_tech stud_technology" id="technology" name="technology">
                                  <option value="">Select Technology</option>
                                  @foreach($technologylist as $technologylist)
                                  <option value="{{$technologylist->id}}">{{$technologylist->technology}}</option>
                                  @endforeach
                                </select>
                              
          </div>
          <div class="mb-3">
            <label class="form-label">Project<sup>*</sup></label>
            <input type="text" class="form-control" name="packpro" id="packpro" placeholder="Project">
          </div>
          <div class="mb-3">
            <label class="form-label">Pre-requisite<sup>*</sup></label>
            <input type="text" class="form-control" name="packpre" id="packpre" placeholder="Pre-requisite">
          </div>

          
        </div>









      <div>
        <button type="submit" class="btn btn-primary ms-auto">Submit</button>
      </div>                                            
      </div>
    
    </div>
    </div>
  </div>
</div>
</div>
</div>
    
    </form>

    <script>
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif
        </script>
          
@endsection
