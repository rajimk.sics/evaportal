@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
                   
              <div class="card">
                <div class="card-body">


                         
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumbs">
                      <li><a href="{{url('/home')}}">Home</a></li>
                      <li><a href="#">{{$title}}</a></li>
                    </ol>
                  </nav>

                    <h4 class="card-title">{{$title}}</h4>
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Student Name</th>
              
                <th>Email</th>
                <th>Phone Number</th>
               
                <th>College</th>
                <th>Registration Type</th>
                <th>Date</th>
                <th>Action</th>

            </tr>
        </thead>
        <tbody>
          <?php $i=1;?>
            @foreach($studentlist as $studentlist)
            <tr>
                <td>{{$i}}</td>
                <td>{{ucfirst($studentlist->name)}}</td>
               
                <td>{{$studentlist->email}} </td>
                <td>{{$studentlist->phone}}</td>
                <td>{{$studentlist->college}}</td>

                <td>@if($studentlist->registration_type==1)

                        <b>Regular</b>

                   @endif
                   @if($studentlist->registration_type==2)

                   <b>Chrysalis</b>

                   @endif
                   @if($studentlist->registration_type==3)
                      <b>Events</b>
                   @endif


                </td>

                <td>{{date("d-m-Y", strtotime($studentlist->created_at))}}</td>
             
                <td>
              <a href="{{url('/select_chrysalis_package/'.$studentlist->student_id.'/'.'talento')}}"><button type="button" class="btn btn-green btn-sm text-white" fdprocessedid="wf07gv">
                  Select Chrysalis Package
                </button></a>
                <a href="{{url('/select_package/'.$studentlist->student_id.'/'.'talento')}}"><button type="button" class="btn btn-green btn-sm text-white" fdprocessedid="wf07gv">
                  Select Regular Package
                </button>
               </a>
  
            </td>     
            </tr>
            <?php $i++;?>
            @endforeach
        </tbody>
    </table>

                        </div>
                </div>
            </div>
            <script>
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif
        </script>




@endsection
