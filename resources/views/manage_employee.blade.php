@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
                   
              <div class="card">
                <div class="card-body">
                  <div class="btn-list" style="float: right">
                     
                    <a href="{{url('/add_srishtians')}}" class="btn btn-primary d-none d-sm-inline-block" >
                      <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                      <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                      Create new 
                    </a>
                    <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal" data-bs-target="#modal-report" aria-label="Create new report">
                      <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                      <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                    </a>
                  </div>

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumbs">
                      <li><a href="{{url('/home')}}">Home</a></li>
                      <li><a href="#">Manage Srishtis Employees</a></li>
                    </ol>
                </nav>

                  <h4 class="card-title">Manage Srishtis Employees</h4>
                  <a href="{{url('/public/uploads/excel/Employees.xls')}}" download="Employees">Download  Sample Excel Template </a>
                 
                  <form action="{{ route('upload-excel') }}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                       <div class="file-upload-custom mt-3 mb-3">
                          <input style="    height: inherit;" class="form-control" type="file" name="file">
                          <button type="submit" class="btn btn-blue btn-sm text-white">Upload</button>
                       </div>
                   </form>

                    
                   
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                            @endforeach
                       </ul>
                       </div>
                  @endif
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Employee ID</th>
                <th>Email ID</th>
                <th>Phone</th>
                <th>Status</th>
                <th>Action</th>
          
            </tr>
        </thead>
        <tbody>
          <?php $i=1;?>
          @foreach($emplist as $emplist)
            <tr>
                <td>{{$i}}</td>
                <td>{{ucfirst($emplist->emp_name)}}</td>
                <td>{{$emplist->empid}}</td>
                <td>{{$emplist->email}}</td>
                <td>{{$emplist->phone}}</td>
                
                <td>
                      @if($emplist->status==1)

                         <span style="color:green">Active</span>

                    @else
                        <span style="color:red">Inactive</span>

                     @endif
              </td>
                <td>
                  @if($emplist->status==1)
                            <button type="button" class="btn btn-danger btn-sm text-white" fdprocessedid="pvth1" onclick="deactivate_employee('{{$emplist->id}}')">
                             Deactivate
                            </button>
                            @else
                            <button type="button" class="btn btn-success btn-sm text-white" fdprocessedid="pvth1" onclick="activate_employee('{{$emplist->id}}')">
                              Activate
                             </button>
                            @endif
               
               
                </td>
               
            </tr>
            <?php $i++;?>
         
            @endforeach
        </tbody>
      
    </table>


 

     <script>
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif
        </script>




@endsection
