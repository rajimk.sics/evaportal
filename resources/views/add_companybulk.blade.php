@extends('layouts.main')
@section('content')

<style>

  .formcontents{
    display: flex;
    flex-direction: column;
  }
  label.error{
    color: red;
    order: 3;
  }
  </style>
   <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">   
              <div class="row row-cards">
                <div class="col-12">

                <nav aria-label="breadcrumb">
                  <ol class="breadcrumbs">
                    <li><a href="{{url('/home')}}">Home</a></li>
                    <li><a href="#">{{$title}}</a></li>
                  </ol>
                </nav>
                    <div class="card-header" >
                      <h4 class="fs-title" style="font-size: 20px;">{{$title}}</h4>
                    </div>
                    @if(Session::has('invalidPhones'))
                    <div class="alert alert-warning">
                      <p>Invalid Contact Number</p>
                       <ul>
                          @foreach(Session::get('invalidPhones') as $digit)
                             <li>{{ $digit }}</li>
                          @endforeach
                      </ul>
                    </div>
                      @endif
                    @if(Session::has('duplicates'))
                    <div class="alert alert-warning">
                      <p>Duplicate values:</p>
                       <ul>
                          @foreach(Session::get('duplicates') as $duplicate)
                             <li>{{ $duplicate }}</li>
                          @endforeach
                      </ul>
                    </div>
                      @endif
                      @if ($errors->any())
                          <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                              @endforeach
                         </ul>
                         </div>
                    @endif
                    <a href="{{url('/public/uploads/excel/company.xlsx')}}" download="Company">Download  Sample Excel Template </a>

                    <form id="bulk_company" action="{{ url('bulkupload_company') }}" method="POST" enctype="multipart/form-data">
                      <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                    <div class="card-body">
                      <div class="">
                          <div class="row">
                       
                            <div class="col-lg-6 formcontents" id="list" >
                            <div class="form-group">
                            <label class="form-label">Upload company<sup>*</sup></label>
                            <input  class="form-control" id="company_file" type="file" name="company_file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" >
                                 
                              </div>
                            </div>


                           
                        </div>
                      </div>

                    </div>

                  </div>
                  <div class="card-footer text-end">
                    <div class="d-flex">
                      
                      <button type="submit" class="btn btn-primary ms-auto">Upload</button>
                    </div>
                  </div>
                </form>

                </div>
           
            </div>

            <script>
        
             
              </script>
            
            <script>
            @if (Session::has('errormessage'))
            swal({
                title: "",
                text: "{{ Session::get('errormessage') }}",
                type: "sucess",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif 
      </script>         
@endsection
