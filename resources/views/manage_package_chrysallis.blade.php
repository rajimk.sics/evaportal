@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">
             
              <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
                   
              <div class="card">
                <div class="card-body">
                  @if(Auth::user()->role == 1 )
                   <!--  <div class="btn-list" style="float: right">
                     
                        <a href="" class="btn btn-primary d-none d-sm-inline-block" >
                          Download SVG icon from http://tabler-icons.io/i/plus 
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                          Create new 
                        </a>
                        <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal" data-bs-target="#modal-report" aria-label="Create new report">
                          Download SVG icon from http://tabler-icons.io/i/plus 
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                        </a>
                      </div>-->

                      @endif
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>
                          <li><a href="#">{{$title}}</a></li>
                        </ol>
                      </nav>
                   
                    <h4 class="card-title"> @if(Auth::user()->role == 1 ) Manage @endif Chrysalis Package</h4>
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Package Name</th>
                <th>Duration</th>
                <th>Technology</th>
                <th>Course Id</th>
                <th>Fee Amount</th>
                <th>Tax 18%</th>
                <th>Total</th>
                <th>Status</th>

              
                <th>Action</th>

              
             
              
               
            </tr>
        </thead>
        <tbody>
          <?php $i=1;?>
            @foreach($package as $package)
            <tr>
                <td>{{$i}}</td>
                <td>{{ucfirst($package->pac_name)}}</td>
                <td>{{$package->duration}} month</td>
                <td>{{$package->technology}}</td>
                <td>{{$package->course_id}}</td>
                <td>{{number_format($package->fee,2)}}</td>
                <td>{{number_format($package->tax,2)}}</td>
                <td>{{number_format($package->total,2)}}</td>
                <td>@if($package->status==1)
                  <span style="color:green">Active</span>
                  @else
                  <span style="color:red">Deactive</span>

                  @endif
              </td>
              @if(Auth::user()->role == 2 )
              <td>
              <a href="{{url('/add_students/'.$package->pac_id)}}"><button type="button" class="btn btn-cyan btn-sm text-white" fdprocessedid="wf07gv">
                Add New Students
              </button></a>
            </br>
          </br>

              <a href="{{url('/add_students_registered/'.$package->pac_id)}}"><button type="button" class="btn btn-cyan btn-sm text-white" fdprocessedid="wf07gv">
                Add Registered Students
              </button></a>

            </br>
          </br>

              <a href="{{url('/students_list/'.$package->pac_id)}}"><button type="button" class="btn btn-cyan btn-sm text-white" fdprocessedid="wf07gv">
                Students
              </button></a>
            </td>

              @endif
              

              @if(Auth::user()->role == 1 )
             
                <td>
                <a href="{{url('/edit_package/'.$package->pac_id)}}"><button type="button" class="btn btn-cyan btn-sm text-white" fdprocessedid="wf07gv">
                    Edit
                  </button></a>
                
                @if($package->status==1)
                            <button type="button" class="btn btn-danger btn-sm text-white" fdprocessedid="pvth1" onclick="dpackage('{{$package->pac_id}}')">
                             Deactivate
                            </button>
                            @else
                            <button type="button" class="btn btn-success btn-sm text-white" fdprocessedid="pvth1" onclick="apackage('{{$package->pac_id}}')">
                              Activate
                             </button>
                            @endif
               
                            <a href="{{url('/students_list/'.$package->pac_id)}}"><button type="button" class="btn btn-cyan btn-sm text-white" fdprocessedid="wf07gv">
                              Students
                            </button></a>

                </td>

                @endif
             
               
            </tr>
            <?php $i++;?>
            @endforeach
           
        </tbody>
      
    </table>


                    </div>
                </div>
           
            </div>
            <script>
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif
        </script>
@endsection
