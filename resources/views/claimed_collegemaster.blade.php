@extends('layouts.main')

@section('content')
    <!-- Page body -->
    <div class="page-body">
        <div class="container-xl">



            <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">

            <div class="card">
                <div class="card-body">


                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                            <li><a href="{{ url('/home') }}">Home</a></li>
                            <li><a href="#">{{ $title }}</a></li>
                        </ol>
                    </nav>
                    <h4 class="card-title">{{ $title }}</h4>




                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Claimed by</th>
                                    <th>Claim raised date</th>
                                    <th>College</th>
                                    <th>Department</th>
                                    <th>Package Name</th>
                                    {{-- <th>Amount without GST</th>
                <th>Tax</th> --}}
                                    <th>Claim total amount</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach ($claimdetails as $claimdetails)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        @php
                                            $colName = App\Helpers\CustomHelper::collegeName($claimdetails->college_id);
                                            $year = date('Y', strtotime($claimdetails->current_datetime));
                                            $currentYear = date('Y');
                                        @endphp
                                        <td>{{ $claimdetails->sales_person }}</td>
                                        <td>{{ date('d-m-Y', strtotime($claimdetails->current_datetime)) }}</td>
                                        <td>{{ $colName['college'] }}@if (!is_null($colName['name']))
                                                , {{ strtoupper($colName['name']) }}
                                            @endif
                                        </td>
                                        <td>{{ $claimdetails->department }}
                                        </td>
                                        @php
                                            $packName = App\Helpers\CustomHelper::typename(
                                                2,
                                                $claimdetails->package_id,
                                            );
                                        @endphp
                                        <td>{{ $packName['name'] }}</td>
                                        {{-- <td>{{number_format($claimdetails->total_package_amount, 2)}}</td>
               <td>{{number_format($claimdetails->total_package_tax,2)}}</td> --}}
                                        <td>{{ number_format($claimdetails->total_package_fullamount, 2) }}</td>
                                        <td>
                                            <a href="{{ url('/claim_masterallocate/' . $claimdetails->college_id) }}">
                                                <button class="btn btn-cyan btn-sm text-white" fdprocessedid="wf07gv">
                                                    View Details

                                                </button>
                                            </a>
                                            <button type="button" id="alloc{{ $claimdetails->claimid }}"
                                                class="btn btn-success btn-sm text-white"
                                                onclick="claimAllocat('{{ $claimdetails->claimid }}','{{ $claimdetails->college_id }}','{{ $claimdetails->package_id }}')"
                                                @if ($claimdetails->claim_status == 1 || $year < $currentYear) disabled @endif>
                                                Allocate
                                            </button>
                                            <button type="button" class="btn btn-blue btn-sm text-white"
                                                fdprocessedid="wf07gv"
                                                onclick="fetchAllocationClaim('{{ $claimdetails->claimid }}')">
                                                View Claim Details
                                            </button>
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- <div class="modal modal-blur fade" id="claimAllocate" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Claim Allocation</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <form id="claimallocateForm" enctype="multipart/form-data" method="POST" action="{{ url('/save_allocation') }}" >
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="modal-body">
                    <div class="row">

                      <div class="section">
                        <div class="row align-items-center">
                          <div class="col-lg-2 text-center">
                            <button type="button" name="addPercentage" id="addPercentage" class="btn btn-success mt-4">Add</button>
                        </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="salesperson">Sales Person</label>
                                    <select class="form-select selectclass" id="salesperson" name="salesperson[]">
                                        <!-- Salesperson options -->
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="perallocate">% of Allocation<sup>*</sup></label>
                                    <input type="text" class="form-control" name="perallocate[]" id="perallocate" placeholder="% of Allocation" min="1">
                                </div>
                            </div> 
                           
                        </div>
                    </div>
                    <div id="dynamic-sections"></div>
                    
                      <div id="text-box-container">
                        <div class="doc-group" >
                      </div>
                  </div>
                    </div>
                    <div class="row">
                     
                     
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label for="claimName">Comment<sup>*</sup></label>
                          <textarea class="form-control" id="comment" name="comment" rows="3"></textarea>
                        </div>
                      </div>
                    </div>
                    
                          <input type="hidden" id="collegeId" name="collegeId">
                         
                          <input type="hidden" id="packageId" name="packageId">
                          <input type="hidden" id="claimId" name="claimId">
                         
                 
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                       <button type="submit" id="claimalloc" class="btn btn-primary">Submit</button> 
                  </div>
              </div>
            </form>
          </div>
        </div> --}}



    <div class="modal modal-blur fade" id="claimAllocate" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title" id="exampleModalLabel">Claim Allocation</h5>
                    <button type="button" class="btn-close allocate-btn-close text-white" data-bs-dismiss="modal"
                        aria-label="Close"></button>
                </div>


                <form id="claimallocateForm" enctype="multipart/form-data" method="POST"
                    action="{{ url('/save_allocation') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="modal-body">
                        <div class="container">
                            <div class="row">

                                <div class="col-lg-12">

                                    <div class="row align-items-center float-center">
                                        <div class="col-lg-2 text-center">
                                            <button type="button" name="addPercentage" id="addPercentage"
                                                class="btn btn-success">
                                                <i class="bi bi-plus"></i> &nbsp; Add a Salesperson
                                            </button>
                                        </div>

                                    </div>
                                </div>
                                <div id="dynamic-sections" class="col-lg-12 mt-2"></div>
                                <div id="text-box-container" class="col-lg-12">
                                    <div class="doc-group"></div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="comment">Comment<sup>*</sup></label>
                                        <textarea class="form-control" id="comment" name="comment" rows="3" placeholder="Add any comment..."></textarea>
                                    </div>
                                </div>

                                <input type="hidden" id="collegeId" name="collegeId">
                                <input type="hidden" id="packageId" name="packageId">
                                <input type="hidden" id="claimId" name="claimId">
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm allocate-btn-close"
                            data-bs-dismiss="modal">Close</button>
                        <button type="submit" id="claimalloc" class="btn btn-primary btn-sm">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>




    <div id="AllocationclaimModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-xl modal-dialog-centered modal-fullscreen-md-down" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="claimDetailsModalLabel">Claim Details</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body d-flex flex-column">
                    <h4 id="tableHeading" class="text-center mb-3"></h4>
                    <div class="table-responsive overflow-auto">
                        <table id="claimDetailsTable" class="table table-bordered table-striped mb-0">
                            <thead class="thead-dark">
                                <tr>
                                    <th>No</th>
                                    <th>Student Name</th>
                                    <th>Email</th>
                                    <th>Phone Number</th>
                                    <th>Joining Date</th>
                                    <th>Package Total Fee</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @if (Session::has('message'))
        <script>
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        </script>
    @endif
@endsection
