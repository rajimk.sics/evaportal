@extends('layouts.main')

@section('content')

<style>

.box {
  background-color: #ffffff;
  padding: 20px;
  border-radius: 8px;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.1); /* Optional: Add a subtle shadow */
  width: 30%; /* Adjust width as needed */
}

.box h2 {
  font-size: 1.5em;
  margin-bottom: 10px;
}

.box p {
  font-size: 1em;
  line-height: 1.6;
  color: #555555;
}

    #tab2 {
      display: none; /* Hide all tabs by default */
    }
  </style>


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <div class="card">
                <div class="card-body">

                    <div class="btn-list" style="float: right">
    
                        <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal" data-bs-target="#modal-report" aria-label="Create new report">
                          <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M12 5l0 14"></path><path d="M5 12l14 0"></path></svg>
                        </a>
                      </div>

                  
              <nav aria-label="breadcrumb">
                <ol class="breadcrumbs">
                  <li><a href="{{url('/home')}}">Home</a></li>
                  @if($empstatus==1)
                  <li><a href="{{url('/employee_sales_report/'.$empid)}}">{{$subtitle}}</a></li>
                  @endif
                  <li><a href="#">{{$title}}</a></li>
                </ol>
              </nav>
                    <h4 class="card-title">{{$title}}</h4>
                    <div class="row">
                      <div class="box col-lg-6">
                        <h2>Payment</h2>
                        <p>Rs {{number_format($monthrevenue_regularold_withoutgst,2)}}</p>
                      </div>
                      <div class="box col-lg-6">
                        <h2>Tax</h2>
                        <p>Rs {{number_format($monthrevenue_regularold_tax,2)}}</p>
                      </div>
                      <div class="box col-lg-6">
                        <h2>Total</h2>
                        <p>Rs {{number_format($monthrevenue_regularold_withgst,2)}}</p>
                      </div>
                    </div>

                    





                  @if((Auth::user()->role==2)||(Auth::user()->role==1))
                      <form id="searchchrysalis" method="GET" enctype="multipart/form-data"  action="{{url('/rpmonthlyrevenuewithgst_regular')}}">     
                      <input type="hidden" name="_token" id="token"  value="{{ csrf_token() }}">
                      <div class="row">
                      <div class="col-lg-3 formcontents">
                                      <label>From date</label>
                                      <input type="text" class="form-control" data-zdp_readonly_element="true" name="search_date1" id="search_date1" value="{{$search_date1}}">
                                      </div>
                                      <div class="col-lg-3 formcontents">
                                      <label>To date</label>
                                      <input type="text" class="form-control" data-zdp_readonly_element="true" name="search_date2" id="search_date2" value="{{$search_date2}}" >
                                      </div>
                                      <div class="col formcontents">
                                        <button type="submit" class="btn btn-primary" style="margin-top: 26px;">Submit</button>
                                        </div>
                                        <div class="col formcontents"> 
                                         <a href="{{url('/rpmonthlyrevenuewithgst_regular')}}" class="btn btn-primary"  style="margin-top: 26px;"> View All</a>
                                          </div>
                                        </div>
                               </form>
                      @endif 

                      
                      <div class="tab-btn mt-5 mb-3" >
                        <button class="active" id="tab1b" onclick="openTab('tab1')">New Admissions</button>
                        <button onclick="openTab('tab2')" id="tab2b">Old Admissions</button>
                      </div>
                      <div id="tab1" class="tab">
                    <div class="table-responsive">
                      <h4 class="card-title">New Admissions</h4>
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
            <tr>
                <th>No</th>
                <th>Student Name</th>
                <th>SICS Reg Number</th>
                <th>Package Name</th>
                <th>Payment</th>
                <th>Tax</th>
                <th>Total</th>
                <th>Employee Name</th>
                <th>Date</th>
              </tr>
        </thead>
        <tbody>
            <?php $i=1;?>
            @foreach($report as $reports)
            <tr>
                <td>{{$i}}</td>
                <td>{{ucfirst($reports->name)}}</td>
               
                <td>{{'SIAC'.$reports->student_admissionid}}</td>
                <td>{{$reports->pac_name}}</td>
               
                <td> Rs {{number_format($reports->payment,2)}}</td>
                <td> Rs {{number_format($reports->tax,2)}}</td>
                
                <td>Rs {{number_format($reports->total,2)}}</td>
                @php
                $name=App\Helpers\CustomHelper::uname($reports->sales_id); 
              @endphp
                <td>{{ucfirst($name->name)}}</td>
                <td>{{date('d-m-Y', strtotime($reports->date))}}</td>
                
               
            </tr>
            <?php $i++;?>
            @endforeach           
        </tbody>     
    </table>

                    </div>

                      </div>

                      <div id="tab2" class="tab">

                        <div class="table-responsive">

                          <h4 class="card-title">Old Admissions</h4>
                          <table id="example1" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Student Name</th>
                    <th>SICS Reg Number</th>
                    <th>Package Name</th>
                    <th>Payment</th>
                    <th>Tax</th>
                    <th>Total</th>
                    <th>Date</th>
                  </tr>
            </thead>
            <tbody>
                <?php $i=1;?>
                @foreach($oldreport as $oldreport)
                <tr>
                    <td>{{$i}}</td>
                    <td>{{ucfirst($oldreport->name)}}</td>
                   
                    <td>{{$oldreport->student_admissionid}}</td>
                    <td>{{$oldreport->pac_name}}</td>
                   
                    <td> Rs {{number_format($oldreport->payment,2)}}</td>
                    <td> Rs {{number_format($oldreport->tax,2)}}</td>
                    
                    <td>Rs {{number_format($oldreport->total,2)}}</td>
                    <td>{{date('d-m-Y', strtotime($oldreport->date))}}</td>
                    
                   
                </tr>
                <?php $i++;?>
                @endforeach           
            </tbody>     
        </table>
    
                        </div>





                      </div>






                </div>         
            </div>
           

                       
@endsection
<script>
  function openTab(tabName) {
    var i, tabContent;

    // Hide all tabs
    tabContent = document.getElementsByClassName("tab");
    for (i = 0; i < tabContent.length; i++) {
      tabContent[i].style.display = "none";
    }

    

    // Show the specific tab
    document.getElementById(tabName).style.display = "block";

    if(tabName=='tab1'){

 

      $('#tab1b').addClass('active');
      $('#tab2b').removeClass('active');

      //document.getElementById(tab1b).classList.add('active');
      //document.getElementById(tab2b).classList.remove('active');

}
else{



  $('#tab2b').addClass('active');
      $('#tab1b').removeClass('active');

//document.getElementById(tab2b).classList.add('active');
//document.getElementById(tab1b).classList.remove('active');

}
  }
</script>
