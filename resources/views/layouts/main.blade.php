<!doctype html>
<!--
* Tabler - Premium and Open Source dashboard template with responsive and high quality UI.
* @version 1.0.0-beta19
* @link https://tabler.io
* Copyright 2018-2023 The Tabler Authors
* Copyright 2018-2023 codecalm.net Paweł Kuna
* Licensed under MIT (https://github.com/tabler/tabler/blob/master/LICENSE)
-->
<html lang="en" >
<head>
  <meta charset="UTF-8">
     <!-- Fav icon -->
       <!-- Fav icon -->
       <link rel="icon" href="{{url('public/img/favicon.png')}}" type="image/png" sizes="16x16"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>EVA Portal</title>

        
    <!-- CSS files -->

    <link href=" {{url('public/css/tabler.min.css?1684106062')}}" rel="stylesheet"/>
    <link href="{{url('public/css/tabler-flags.min.css?1684106062')}}" rel="stylesheet"/>
    <link href="{{url('public/css/tabler-payments.min.css?1684106062')}}" rel="stylesheet"/>
    <link href=" {{url('public/css/tabler-vendors.min.css?1684106062')}}" rel="stylesheet"/>
    <link href="{{url('public/css/demo.min.css?1684106062')}}" rel="stylesheet"/>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css'>
    <link rel="stylesheet" href="{{url('public/css/zebra_datepicker.min.css')}}" type="text/css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.13.1/jquery.timepicker.min.css">
    
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{url('public/assets/css/style.css')}}">
    <style>
      @import url('https://rsms.me/inter/inter.css');
      :root {
      	--tblr-font-sans-serif: 'Inter Var', -apple-system, BlinkMacSystemFont, San Francisco, Segoe UI, Roboto, Helvetica Neue, sans-serif;
      }
      body {
      	font-feature-settings: "cv03", "cv04", "cv11";
      }

      label.error{
          color: red;
      }

  .pageloader {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 999999999999;
	opacity: 1;
	/* background-position: 50% 50% !important; */
	text-align: center;
	background-color: #ffffff63;
}
.pageloader:before {
	background: url("http://localhost/evaportal/PUBLIC/img/spinner.gif") rgb(255 255 255 / 14%);
	background-repeat: no-repeat !important;
    background-size: cover!important;
    width: 70px;
    height: 70px;
    content: ' ';
    position: absolute;
    left: 46%;
    top: 45%;
    z-index: 10;
}

.breadcrumbs {
    list-style: none;
    padding: 0;
    margin: 0;
  }
  .breadcrumbs li {
    display: inline;
    margin-right: 5px;
  }
  .breadcrumbs li:not(:last-child):after {
    content: "›";
    margin-left: 5px;
    margin-right: 5px;
    color: #666;
  }
  .highlight-red {
    background-color:#e41313 !important;
    color: white;
}

    </style>
  </head>
  <body >
    <script src="{{url('public/js/demo-theme.min.js?1684106062')}}"></script>
    <div class="page">
      <!-- Navbar -->
      <div class="sticky-top">
        <header class="navbar navbar-expand-md sticky-top d-print-none" >
          <div class="container-xl">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-menu" aria-controls="navbar-menu" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <h1 class="navbar-brand navbar-brand-autodark d-none-navbar-horizontal pe-0 pe-md-3">
              <a href="{{url('/home')}}">
                <img src="{{url('public/img/logo_new.png')}}" width="110" height="32" alt="Evaportal" class="navbar-brand-image">
              </a>
            </h1>
            <div class="navbar-nav flex-row order-md-last">
              <div class="d-none d-md-flex">
              
              </div>
              <div class="nav-item dropdown">
                <a href="#" class="nav-link d-flex lh-1 text-reset p-0" data-bs-toggle="dropdown" aria-label="Open user menu">
                  <img class="avatar avatar-sm" src="{{url('public/img/'.Auth::user()->photo)}}" alt="Avatar"  onerror="this.onerror=null; this.src='{{ url('public/img/default.png') }}';">
                  
                 
                  <div class="name" style="color:black;font-size:14">
                    @if(Auth::user()->role==1)
                    <div>{{Auth::user()->name}}</div>
                    
                    @endif
                    @if(Auth::user()->role==2)
                    <div>Sales-{{Auth::user()->name}}</div>
                    @endif
                    @if(Auth::user()->role==3)
                    <div>Mentor-{{Auth::user()->name}}</div>
                    @endif
                    @if(Auth::user()->role==4)
                    <div>Process Associates -{{Auth::user()->name}}</div>
                    @endif
                    @if(Auth::user()->role==5)
                    <div>Student @if(Auth::user()->name!='')-{{Auth::user()->name}} @endif</div>
                    @endif
                    @if(Auth::user()->role==6)
                    <div>Management @if(Auth::user()->name!='')-{{Auth::user()->name}} @endif</div>
                    @endif
                
                    @if(Auth::user()->role==7)
                    <div>Finance @if(Auth::user()->name!='')-{{Auth::user()->name}} @endif</div>
                    @endif
                
                    @if(Auth::user()->role==8)
                    <div>Subadmin @if(Auth::user()->name!='')-{{Auth::user()->name}} @endif</div>
                    @endif
                    @if(Auth::user()->role==9)
                    <div>Placement @if(Auth::user()->name!='')-{{Auth::user()->name}} @endif</div>
                    @endif

                    @if(Auth::user()->role==10)
                    <div>Recruiter @if(Auth::user()->name!='')-{{Auth::user()->name}} @endif</div>
                    @endif
                    @if(Auth::user()->role==12)
                    <div>Master @if(Auth::user()->name!='')-{{Auth::user()->name}} @endif</div>
                    @endif
                
                
                  </div>
                </a>
                <div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">  

                  <a href="{{url('/change_password')}}" class="dropdown-item">Change Password</a>
                  <a href="{{url('/edit_profile')}}" class="dropdown-item">Edit Profile</a>
                  <a href="{{url('/logout')}}" class="dropdown-item">Logout</a>
                </div>
              </div>
            </div>
          </div>
        </header>
        <header class="navbar-expand-md blue-bg">
          <div class="collapse navbar-collapse" id="navbar-menu">
            <div class="navbar">
              <div class="container-xl">
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <a class="nav-link" href="{{url('/home')}}" >
                      <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/home -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M5 12l-2 0l9 -9l9 9l-2 0" /><path d="M5 12v7a2 2 0 0 0 2 2h10a2 2 0 0 0 2 -2v-7" /><path d="M9 21v-6a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v6" /></svg>
                      </span>
                      <span class="nav-link-title">
                       Home
                      </span>
                    </a>
                  </li>

                  @php
                  $empprivillage=App\Helpers\CustomHelper::employeeprivillages(Auth::user()->id); 
                 @endphp



                  @if(Auth::user()->role==5)

                    @php
                      $rcount=App\Helpers\CustomHelper::regularcount(Auth::user()->id); 
                    @endphp

@if($rcount !=0)

<li class="nav-item dropdown">
  <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
    <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
      <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
    </span>
    <span class="nav-link-title">
      Admission Information
    </span>
  </a>
  <div class="dropdown-menu">
    <div class="dropdown-menu-columns">
      <div class="dropdown-menu-column">

        @if(Auth::user()->admission_completed	==0)
        <a class="dropdown-item" href="{{url('/student_admission')}}">
         Admission form
        </a>
        @endif
        @if(Auth::user()->admission_completed	==1)
        <a class="dropdown-item" href="{{url('/student_admission_details')}}">
         Admission Details
        </a>
        @endif
                                
      </div>
    </div>
  </div>
</li>
@endif



<li class="nav-item dropdown">
  <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
    <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
      <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
    </span>
    <span class="nav-link-title">
      Course Details
    </span>
  </a>
  <div class="dropdown-menu">
    <div class="dropdown-menu-columns">
      <div class="dropdown-menu-column">                        
       
         <a class="dropdown-item" href="{{url('/activated_packages/')}}">
          Activated Packages
         </a>                        
       
                                
      </div>
    </div>
  </div>
</li>

<li class="nav-item dropdown">
  <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
    <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
      <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
    </span>
    <span class="nav-link-title">
      Gate Pass
    </span>
  </a>
  <div class="dropdown-menu">
    <div class="dropdown-menu-columns">
      <div class="dropdown-menu-column">

        
        <a class="dropdown-item" href="{{url('/add_gatepass')}}">
         Request New 
        </a>
        <a class="dropdown-item" href="{{url('/student_gatepass')}}">
          Applied 
          </a>                    
      </div>
    </div>
  </div>
</li>


@endif

@if(Auth::user()->role==12)
             
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                  <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                  </span>
                  <span class="nav-link-title">
                  College
                  </span>
                </a>
                <div class="dropdown-menu">
                  <div class="dropdown-menu-columns">
                    <div class="dropdown-menu-column">
                    <a class="dropdown-item" href="{{url('/manage_college')}}">
                        Add College
                       </a> 

                       <a class="dropdown-item" href="{{url('/claimed_collegesmaster')}}">
                        New Claims
                       </a> 

                       <a class="dropdown-item" href="{{url('/list_appealfromsales')}}">
                        Appeal Requests
                        </a> 
                       <!--<a class="dropdown-item" href="{{url('/claimed_colleges')}}">
                       Claimed Colleges
                       </a> -->
                       <a class="dropdown-item" href="{{url('/list_requestfromsales')}}">
                        Request From Sales
                        </a> 
                      </div>
                  </div>
                </div>

              </li>
@endif

                  
              @if(Auth::user()->role==4)

              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                  <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                  </span>
                  <span class="nav-link-title">
                    Gate Pass 
                  </span>
                </a>
                <div class="dropdown-menu">
                  <div class="dropdown-menu-columns">
                    <div class="dropdown-menu-column">
                      
                     
                      <a class="dropdown-item" href="{{url('/pending_gatepass')}}">
                        Pending  
                       </a> 
                       <a class="dropdown-item" href="{{url('/approved_gatepass')}}">
                        Approved  
                       </a> 


                     <a class="dropdown-item" href="{{url('/rejected_gatepass')}}">
                      Rejected 
                     </a> 
                     <a class="dropdown-item" href="{{url('/returned_gatepass')}}">
                      Returned 
                     </a> 

                     <a class="dropdown-item" href="{{url('/expiring_gatepass')}}">
                      Expiring 
                     </a>
                     <a class="dropdown-item" href="{{url('/reissued_gatepass')}}">
                      Reissued 
                     </a>

                   
                      </div>
                  </div>
                </div>
              </li>

             


            
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                  <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                  </span>
                  <span class="nav-link-title">
                    Students
                  </span>
                </a>
                <div class="dropdown-menu">
                  <div class="dropdown-menu-columns">
                    <div class="dropdown-menu-column">
                      <a class="dropdown-item" href="{{url('/all_students')}}">
                        All  Students 
                        </a>
                                       
                    </div>
                  </div>
                </div>
              </li>
              @if(isset( $empprivillage['employeeprivillege']['add_employee']) && $empprivillage['employeeprivillege']['add_employee'] == 1)         
                  
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                  <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                  </span>
                  <span class="nav-link-title">
                    Employees
                  </span>
                </a>
                <div class="dropdown-menu">
                  <div class="dropdown-menu-columns">
                    <div class="dropdown-menu-column">
                      <a class="dropdown-item" href="{{url('/add_employee')}}">
                        Add Employee
                      </a>
                      <a class="dropdown-item" href="{{url('/manage_process')}}">
                       Manage Process Associates
                      </a>
                     
                                               
                    </div>
                  </div>
                </div>
              </li>
              @endif
              @endif

              @if(Auth::user()->role==9)

              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                  <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                  </span>
                  <span class="nav-link-title">
                    Students
                  </span>
                </a>
                <div class="dropdown-menu">
                  <div class="dropdown-menu-columns">
                    <div class="dropdown-menu-column">
                      <a class="dropdown-item" href="{{url('/all_students')}}">
                        All  Students
                        </a>
                                       
                    </div>
                  </div>
                </div>
              </li>

              @if(isset( $empprivillage['employeeprivillege']['add_employee']) && $empprivillage['employeeprivillege']['add_employee'] == 1)         
                  
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                  <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                  </span>
                  <span class="nav-link-title">
                    Employees
                  </span>
                </a>
                <div class="dropdown-menu">
                  <div class="dropdown-menu-columns">
                    <div class="dropdown-menu-column">
                      <a class="dropdown-item" href="{{url('/add_employee')}}">
                        Add Employee
                      </a>
                      <a class="dropdown-item" href="{{url('/manage_placement')}}">
                       Manage Placement
                      </a>
                     
                                               
                    </div>
                  </div>
                </div>
              </li>
              @endif

              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                  <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                  </span>
                  <span class="nav-link-title">
                  Company
                  </span>
                </a>
                <div class="dropdown-menu">
                  <div class="dropdown-menu-columns">
                    <div class="dropdown-menu-column">
                <a class="dropdown-item" href="{{url('/add_company')}}">
                       Add Company
                      </a>
                      
                      <a class="dropdown-item" href="{{url('/bulk_companyupload')}}">
                        Company Bulk Upload
                        </a>
                      
                      <a class="dropdown-item" href="{{url('/manage_company')}}">
                        Manage Company
                       </a>   
                      </div>
                  </div>
                </div>

              </li>
              @endif
              @if(Auth::user()->role==6)
              @if(isset( $empprivillage['employeeprivillege']['add_employee']) && $empprivillage['employeeprivillege']['add_employee'] == 1)              
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                  <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                  </span>
                  <span class="nav-link-title">
                    Employees
                  </span>
                </a>
                <div class="dropdown-menu">
                  <div class="dropdown-menu-columns">
                    <div class="dropdown-menu-column">
                      <a class="dropdown-item" href="{{url('/add_employee')}}">
                        Add Employee
                      </a>
                      <a class="dropdown-item" href="{{url('/manage_management')}}">
                       Manage Management
                      </a>                        
                    </div>
                  </div>
                </div>
              </li>
              @endif

              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                  <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                  </span>
                  <span class="nav-link-title">
                  Company
                  </span>
                </a>
                <div class="dropdown-menu">
                  <div class="dropdown-menu-columns">
                    <div class="dropdown-menu-column">
                <a class="dropdown-item" href="{{url('/add_company')}}">
                       Add Company
                      </a>  

                      <a class="dropdown-item" href="{{url('/bulk_companyupload')}}">
                        Company Bulk Upload
                        </a>
                      
                      <a class="dropdown-item" href="{{url('/manage_company')}}">
                        Manage Company
                       </a>   
                      </div>
                  </div>
                </div>

              </li>
             


              @endif

              @if(Auth::user()->role==7)

              @if(isset( $empprivillage['employeeprivillege']['add_employee']) && $empprivillage['employeeprivillege']['add_employee'] == 1)         
                  
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                  <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                  </span>
                  <span class="nav-link-title">
                    Employees
                  </span>
                </a>
                <div class="dropdown-menu">
                  <div class="dropdown-menu-columns">
                    <div class="dropdown-menu-column">
                      <a class="dropdown-item" href="{{url('/add_employee')}}">
                        Add Employee
                      </a>
                      <a class="dropdown-item" href="{{url('/manage_finance')}}">
                       Manage Finance
                      </a>                        
                    </div>
                  </div>
                </div>
              </li>
              @endif

              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                  <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                  </span>
                  <span class="nav-link-title">
                    Payments
                  </span>
                </a>
                <div class="dropdown-menu">
                  <div class="dropdown-menu-columns">
                    <div class="dropdown-menu-column">
                      <a class="dropdown-item" href="{{url('/pending_payment')}}">
                       Pending Payments
                      </a>
                      <a class="dropdown-item" href="{{url('/approved_payment')}}">
                      Approved Payments
                      </a>   
                      <a class="dropdown-item" href="{{url('/rejected_payment')}}">
                        Rejected Payments
                        </a>                      
                    </div>
                  </div>
                </div>
              </li>
              @endif

              @if(Auth::user()->role==10)

              @if(isset( $empprivillage['employeeprivillege']['add_employee']) && $empprivillage['employeeprivillege']['add_employee'] == 1)         
                  
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                  <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                  </span>
                  <span class="nav-link-title">
                    Employees
                  </span>
                </a>
                <div class="dropdown-menu">
                  <div class="dropdown-menu-columns">
                    <div class="dropdown-menu-column">
                      <a class="dropdown-item" href="{{url('/add_employee')}}">
                        Add Employee
                      </a>
                      <a class="dropdown-item" href="{{url('/manage_recruitment')}}">
                       Manage Recruiter
                      </a>
                     
                                               
                    </div>
                  </div>
                </div>
              </li>
              @endif

              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                  <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                  </span>
                  <span class="nav-link-title">
                  Candidate
                  </span>
                </a>
                <div class="dropdown-menu">
                  <div class="dropdown-menu-columns">
                    <div class="dropdown-menu-column">
                    <a class="dropdown-item" href="{{url('/bulk_candidateupload')}}">
                        Candidate Bulk Upload
                       </a> 
                      <a class="dropdown-item" href="{{url('/manage_candidate')}}">
                       Candidate Listing
                       </a>   
                       <a class="dropdown-item" href="{{url('/candidate_followups')}}">
                        Follow Up
                       </a> 
                       <a class="dropdown-item" href="{{url('/candidate_interview')}}">
                       Interview Schedules
                       </a> 
                      </div>
                  </div>
                </div>
              </li>

              @endif


                   
              @if(Auth::user()->role==8)


              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                  <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                  </span>
                  <span class="nav-link-title">
                    Gate Pass 
                  </span>
                </a>
                <div class="dropdown-menu">
                  <div class="dropdown-menu-columns">
                    <div class="dropdown-menu-column">
                      
                     
                      <a class="dropdown-item" href="{{url('/pending_gatepass')}}">
                       Pending
                       </a> 
                       <a class="dropdown-item" href="{{url('/approved_gatepass')}}">
                        Approved 
                       </a> 


                     <a class="dropdown-item" href="{{url('/rejected_gatepass')}}">
                      Rejected 
                     </a> 
                     <a class="dropdown-item" href="{{url('/returned_gatepass')}}">
                      Returned 
                     </a> 
                     <a class="dropdown-item" href="{{url('/expiring_gatepass')}}">
                      Expiring 
                     </a>
                     <a class="dropdown-item" href="{{url('/reissued_gatepass')}}">
                      Reissued 
                     </a>


                   
                      </div>
                  </div>
                </div>
              </li>


            
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                  <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                  </span>
                  <span class="nav-link-title">
                    Students
                  </span>
                </a>
                <div class="dropdown-menu">
                  <div class="dropdown-menu-columns">
                    <div class="dropdown-menu-column">
                      <a class="dropdown-item" href="{{url('/all_students')}}">
                        All  Students
                        </a>                                           
                    </div>
                  </div>
                </div>
              </li>
              @if(isset( $empprivillage['employeeprivillege']['add_employee']) && $empprivillage['employeeprivillege']['add_employee'] == 1)         
                  
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                      </span>
                      <span class="nav-link-title">
                        Employees
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          <a class="dropdown-item" href="{{url('/add_employee')}}">
                            Add Employee
                          </a>
                          <a class="dropdown-item" href="{{url('/manage_subadmin')}}">
                           Manage Subadmin
                          </a>
                         
                                                   
                        </div>
                      </div>
                    </div>
                  </li>
                  @endif


              

              @endif



              @if(Auth::user()->role==3)
                  

                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-title">
                        Packages
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          <a class="dropdown-item"  href="{{url('/packagelist_trainer')}}">
                            Package List
                           </a>
                                        
                        </div>
                      </div>
                    </div>
                  </li>

                  

                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      
                      <span class="nav-link-title">
                      Syllabus
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          <a class="dropdown-item"  href="{{url('/uploadSylabus')}}">
                            Syllabus Upload
                           </a>
                           <a class="dropdown-item"  href="{{url('/topicListingByTechnology')}}">
                            Topic Listing
                           </a>
                         
                                               
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-title">
                       Questions
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          <a class="dropdown-item"  href="{{url('/viewQuestionUpload')}}">
                           Questions Upload
                           </a>
                          <a class="dropdown-item"  href="{{url('/manageQuestions')}}">
                           View Questions
                           </a>
                                               
                        </div>
                      </div>
                    </div>
                  </li>

                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-title">
                       Exams
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          <a class="dropdown-item"  href="{{url('/examDeclaration')}}">
                           Declaration
                           </a>
                          <a class="dropdown-item"  href="{{url('/fetchExam')}}">
                           View Exams
                           </a>
                          
                                               
                        </div>
                      </div>
                    </div>
                  </li> 
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-title">
                       Students
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          <a class="dropdown-item"  href="{{url('/trainerStudents')}}">
                           Your Students
                           </a>
                           <a class="dropdown-item"  href="{{url('/studentAttendance')}}">
                            Attendance
                            </a>
                          
                                               
                        </div>
                      </div>
                    </div>
                  </li>           
                  


                  @if(isset( $empprivillage['employeeprivillege']['add_employee']) && $empprivillage['employeeprivillege']['add_employee'] == 1)         
                  
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                      </span>
                      <span class="nav-link-title">
                        Employees
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          <a class="dropdown-item" href="{{url('/add_employee')}}">
                            Add Employee
                          </a>
                          <a class="dropdown-item" href="{{url('/manage_trainers')}}">
                           Manage Mentor
                          </a>
                         
                                                   
                        </div>
                      </div>
                    </div>
                  </li>
                  @endif
                  @endif
                  @if(Auth::user()->role==2)

                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                      </span>
                      <span class="nav-link-title">
                        Marketing
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          <a class="dropdown-item" href="{{url('/add_contacts')}}">
                            Add Contacts
                          </a>
                          <a class="dropdown-item" href="{{url('/upload_contacts')}}">
                            Add Bulk Contacts
                          </a>
                          <a class="dropdown-item" href="{{url('/outboundcalls')}}">
                            Outbound Calls
                          </a> 
                          <a class="dropdown-item" href="{{url('/followup')}}">
                           Follow up
                          </a> 
                          <a class="dropdown-item" href="{{url('/closed_contacts')}}">
                           Closed Contacts
                          </a> 
                                                   
                        </div>
                      </div>
                    </div>
                  </li>
                
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                      </span>
                      <span class="nav-link-title">
                         College
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          <a class="dropdown-item" href="{{url('/manage_college')}}">
                            College List
                             </a> 
                             <a class="dropdown-item" href="{{url('/request_tomaster')}}">
                             Request To Master
                              </a> 
                             <!--<a class="dropdown-item" href="{{url('/claims_collegeSales')}}">
                              Claim Colleges
                              </a> -->   
                             <a class="dropdown-item" href="{{url('/claimed_collegeSales')}}">
                              New Claims
                              </a> 
                              <a class="dropdown-item" href="{{url('/college_updateReport')}}">
                                Colleges Update Report
                                </a>
                                           
                        </div>
                      </div>
                    </div>
                  </li>

                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                      </span>
                      <span class="nav-link-title">
                        Package
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">                         
                          <a class="dropdown-item" href="{{url('/manage_package_chrysallis')}}">
                              Chrysalis Package
                          </a>
                          <a class="dropdown-item" href="{{url('/manage_package')}}">
                              Regular Package
                          </a>
                          </div>
                      </div>
                    </div>
                  </li>

                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                      </span>
                      <span class="nav-link-title">
                        Talento 
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">                         
                          <a class="dropdown-item" href="{{url('/manage_talento_registration')}}">
                              All Registration
                          </a>
                        </a>
                        <a class="dropdown-item" href="{{url('/pending_paymentrequest')}}">
                          Pending Payment Requests
                         </a> 
                         <a class="dropdown-item" href="{{url('/approved_paymentrequest')}}">
                           Approved Payment Requests
                        </a> 
                        <a class="dropdown-item" href="{{url('/rejected_paymentrequest')}}">
                          Rejected Payment Requests
                      </a> 
                          </div>
                      </div>
                    </div>
                  </li>

                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                      </span>
                      <span class="nav-link-title">
                        Gate Pass 
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          
                         
                          <a class="dropdown-item" href="{{url('/pending_gatepass')}}">
                            Pending 
                           </a> 
                           <a class="dropdown-item" href="{{url('/approved_gatepass')}}">
                            Approved 
                           </a> 


                         <a class="dropdown-item" href="{{url('/rejected_gatepass')}}">
                          Rejected 
                         </a> 
                         <a class="dropdown-item" href="{{url('/returned_gatepass')}}">
                          Returned 
                         </a> 

                         <a class="dropdown-item" href="{{url('/expiring_gatepass')}}">
                          Expiring 
                         </a>

                         <a class="dropdown-item" href="{{url('/reissued_gatepass')}}">
                          Reissued 
                         </a>
    
                       
                          </div>
                      </div>
                    </div>
                  </li>

                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                      </span>
                      <span class="nav-link-title">
                         Students
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          <a class="dropdown-item" href="{{url('/your_students')}}">
                            Your  Students
                            </a>
                            <a class="dropdown-item" href="{{url('/all_students')}}">
                              All  Students
                              </a>

                                           
                        </div>
                      </div>
                    </div>
                  </li>



                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                      </span>
                      <span class="nav-link-title">
                        Finance
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          <a class="dropdown-item" href="{{url('/pending_payment')}}">
                           Pending Payments 
                          </a>
                          <a class="dropdown-item" href="{{url('/approved_payment')}}">
                          Approved Payments 
                          </a>   
                          <a class="dropdown-item" href="{{url('/rejected_payment')}}">
                            Rejected Payments  
                            </a>                      
                        </div>
                      </div>
                    </div>
                  </li>
    
                
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                      </span>
                      <span class="nav-link-title">
                      Old Data 
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          
                          <a class="dropdown-item" href="{{url('/manage_oldstudent')}}">
                              Old Student Upload
                          </a>
                          
                          </div>
                      </div>
                    </div>
                  </li>

                  

            @if(isset( $empprivillage['employeeprivillege']['add_employee']) && $empprivillage['employeeprivillege']['add_employee'] == 1)
                  
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                      </span>
                      <span class="nav-link-title">
                        Employees
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          <a class="dropdown-item" href="{{url('/add_employee')}}">
                            Add Employee
                          </a>
                          <a class="dropdown-item" href="{{url('/manage_sales')}}">
                           Manage Marketing
                          </a>
                         
                                                   
                        </div>
                      </div>
                    </div>
                  </li>
                  @endif
                  @endif
                  @if(Auth::user()->role==1)
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                      </span>
                      <span class="nav-link-title">
                       Settings
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          <a class="dropdown-item" href="{{url('/add_email')}}">
                            Interview schedule email
                          </a>
                          <a class="dropdown-item" href="{{url('/departments')}}">
                            Department
                         </a>
                         <a class="dropdown-item" href="{{url('/departmentPoc')}}">
                          Department Poc
                       </a>
                          
                          <a class="dropdown-item" href="{{url('/manage_designation')}}">
                             Designation
                          </a>
                          <a class="dropdown-item" href="{{url('/manage_technology')}}">
                             Technology
                          </a> 
                          <a class="dropdown-item" href="{{url('/manage_source')}}">
                             Source
                          </a> 
                          
                          <a class="dropdown-item" href="{{url('/manage_college')}}">
                             College
                          </a>
                         
                          <a class="dropdown-item" href="{{url('/manage_qualification')}}">
                             Qualification
                          </a>
                          <a class="dropdown-item" href="{{url('/manage_specialization')}}">
                             Specialization
                          </a>
                         
                        </div>
                      </div>
                    </div>
                  </li>                
                  <!-- <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-icon d-md-none d-lg-inline-block">Download SVG icon from http://tabler-icons.io/i/package
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                      </span>
                      <span class="nav-link-title">
                        Students
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          <a class="dropdown-item" href="{{url('/regular_students')}}">
                            Regular Students
                           </a>
                           <a class="dropdown-item" href="{{url('/chrysalis_students')}}">
                           Chrysalis students
                           </a>
              
                        </div>
                      </div>
                    </div>
                  </li> -->

                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                      </span>
                      <span class="nav-link-title">
                        Employees
                      </span>
                    </a>

                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          <a class="dropdown-item" href="{{url('/add_employee')}}">
                            Add Employee
                          </a>
                          <a class="dropdown-item" href="{{url('/manage_recruitment')}}">
                            Recruitment
                           </a>
                          
                          <a class="dropdown-item" href="{{url('/manage_sales')}}">
                            Marketing
                          </a>
                         
                          <a class="dropdown-item" href="{{url('/manage_trainers')}}">
                            Mentors
                          </a>
                          <a class="dropdown-item" href="{{url('/manage_process')}}">
                            Process Associates
                          </a>
                          <a class="dropdown-item" href="{{url('/manage_management')}}">
                            Management
                          </a>
                            <a class="dropdown-item" href="{{url('/manage_finance')}}">
                              Finance
                            </a>
                            <a class="dropdown-item" href="{{url('/manage_subadmin')}}">
                             Subadmin
                            </a>
                            <a class="dropdown-item" href="{{url('/manage_placement')}}">
                             Placement
                            </a>
                            <a class="dropdown-item" href="{{url('/manage_master')}}">
                              Master
                             </a>
              
                        </div>
                      </div>
                    </div>
                  </li>




                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                      </span>
                      <span class="nav-link-title">
                        Package
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          <a class="dropdown-item" href="{{url('/add_package')}}">
                          Add New Package
                          </a>
                          <a class="dropdown-item" href="{{url('/manage_package_chrysallis')}}">
                              Chrysalis Package
                          </a>
                          <a class="dropdown-item" href="{{url('/manage_package')}}">
                              Regular Package
                          </a>

                          </div>
                      </div>
                    </div>
                  </li>

                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                      </span>
                      <span class="nav-link-title">
                       Reference
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          <a class="dropdown-item" href="{{url('/manage_employee')}}">
                              Srishtis Employees
                          </a>
                          <a class="dropdown-item" href="{{url('/manage_reference')}}">
                             Other Reference
                          </a>
                          
                          </div>
                      </div>
                    </div>                  
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                      </span>
                      <span class="nav-link-title">
                      Talento
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          <a class="dropdown-item" href="{{url('/manage_events')}}">
                              Events
                          </a>
                          <a class="dropdown-item" href="{{url('/manage_settings')}}">
                             Settings
                          </a>
                          <a class="dropdown-item" href="{{url('/manage_page_contents')}}">
                            Help Requests
                         </a> 
                        
                         <a class="dropdown-item" href="{{url('/pending_paymentrequest')}}">
                          Pending Payment Requests
                         </a> 
                         <a class="dropdown-item" href="{{url('/approved_paymentrequest')}}">
                           Approved Payment Requests
                        </a> 
                        <a class="dropdown-item" href="{{url('/rejected_paymentrequest')}}">
                          Rejected Payment Requests
                      </a>   
                       
                      </div>
                      </div>
                    </div>
                   
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                      </span>
                      <span class="nav-link-title">
                        Gate Pass 
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                          
                         
                          <a class="dropdown-item" href="{{url('/pending_gatepass')}}">
                            Pending 
                           </a> 
                           <a class="dropdown-item" href="{{url('/approved_gatepass')}}">
                            Approved 
                           </a> 
    
    
                         <a class="dropdown-item" href="{{url('/rejected_gatepass')}}">
                          Rejected 
                         </a> 
                         <a class="dropdown-item" href="{{url('/returned_gatepass')}}">
                          Returned 
                         </a> 
    
                         <a class="dropdown-item" href="{{url('/expiring_gatepass')}}">
                          Expiring 
                         </a>
                         <a class="dropdown-item" href="{{url('/reissued_gatepass')}}">
                          Reissued 
                         </a>
    
                       
                          </div>
                      </div>
                    </div>
                  </li>


                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown" data-bs-auto-close="outside" role="button" aria-expanded="false" >
                      <span class="nav-link-icon d-md-none d-lg-inline-block"><!-- Download SVG icon from http://tabler-icons.io/i/package -->
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" /><path d="M12 12l8 -4.5" /><path d="M12 12l0 9" /><path d="M12 12l-8 -4.5" /><path d="M16 5.25l-8 4.5" /></svg>
                      </span>
                      <span class="nav-link-title">
                      Company
                      </span>
                    </a>
                    <div class="dropdown-menu">
                      <div class="dropdown-menu-columns">
                        <div class="dropdown-menu-column">
                    <a class="dropdown-item" href="{{url('/add_company')}}">
                           Add Company
                          </a> 
                          <a class="dropdown-item" href="{{url('/bulk_companyupload')}}">
                            Company Bulk Upload
                            </a> 
                          
                          <a class="dropdown-item" href="{{url('/manage_company')}}">
                            Manage Company
                           </a>   
                          </div>
                      </div>
                    </div>

                  </li>








                  @endif
                </ul>
             
              </div>
            </div>
          </div>
        </header>
      </div>
      <div class="page-wrapper">
        <!-- Page header -->

        <div class="pageloader" style="display:none;" >
        </div>
        @yield('content')
<footer class="footer footer-transparent d-print-none">
  <div class="container-xl">
    <div class="row text-center align-items-center flex-row-reverse">
 
      <div class="col-12 col-lg-auto mt-3 mt-lg-0" style="text-align: center;11">
        <ul class="list-inline list-inline-dots mb-0">
          <li class="list-inline-item">
            Copyright &copy; 2023
            <a href="." class="link-secondary">Evaportal</a>.
            All rights reserved.
          </li>
         
        </ul>
      </div>

    </div>
  </div>
</footer>
</div>
</div>

<!-- Libs JS -->

<script src="{{url('public/js/jquery.min.js')}}"></script>
<script src="{{url('public/libs/apexcharts/dist/apexcharts.min.js?1684106062')}}" defer></script>
<script src="{{url('public/libs/jsvectormap/dist/js/jsvectormap.min.js?1684106062')}}" defer></script>
<script src="{{url('public/libs/jsvectormap/dist/maps/world.js?1684106062')}}" defer></script>
<script src="{{url('public/libs/jsvectormap/dist/maps/world-merc.js?1684106062')}}" defer></script>
<!-- Tabler Core -->
<script src="{{url('public/js/tabler.min.js?1684106062')}}" defer></script>
<script src="{{url('public/js/demo.min.js?1684106062')}}" defer></script>
<!-- this page js --> 
<script src="{{url('public/libs/DataTables/datatables.min.js')}}"></script>



<script>



    /****************************************
     *       Basic Table                   *
     ****************************************/

     $(function(){
  $('.selecttrainer').select2({
        dropdownParent: $('#assignTrainerModel'),
        width: 'resolve' 
    }); });

    $(function(){
    $('.selectdays').select2({
        dropdownParent: $('#assignTrainerModel'),
        width: 'resolve'
    });
   });




$(function(){
   $('#collegepoc').select2({
     dropdownParent: $('#modalCollegeUpdates')
   });
 }); 

 $(function(){
   $('.pocmulti').select2({
     dropdownParent: $('#requestToMasterModal')
   });
 });
 $(function(){
   $('.salesmulti').select2({
     dropdownParent: $('#claimAllocate')
   });
 });

$('#example').DataTable();
$('#example1').DataTable();


$(function() {
  $(".selecttype").select2();
  $(".selectdesi").select2();
  $(".selecttech").select2();
  $(".packname").select2();
  $(".stud_qualification").select2();
  $(".stud_year_of_pass").select2();
  $(".stud_arrears_no").select2();
  $(".stud_specialization").select2();
  $(".stud_percentage").select2();
  $(".stud_hsst_per").select2();
  $(".stud_hsst_year").select2();
  $(".stud_ss_per").select2();
  $(".stud_ss_year").select2();
  $(".stud_technology").select2();
  $(".stud_blood").select2();
  
});


$(document).ready(function () {

@if (Session::has('success'))
swal("", "{{ Session::get('success') }}", "success");  

@endif

@if (Session::has('errormessage'))
swal("", "{{ Session::get('errormessage') }}", "error");  

@endif

});




</script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/additional-methods.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.13.1/jquery.timepicker.min.js"></script>



<script src="{{url('public/js/zebra_datepicker.min.js')}}"></script>
<script src="{{url('public/js/validation.js')}}"></script>
<script src="{{url('public/js/actdeact.js')}}"></script>
<script src="{{url('public/js/common.js')}}"></script>
<script src="{{url('public/js/claim.js')}}"></script>
<script src="{{url('public/js/trainer.js')}}"></script>
<script src="{{url('public/js/recruiter.js')}}"></script>
<script src="{{url('public/js/process.js')}}"></script>






<script>
// @formatter:off
document.addEventListener("DOMContentLoaded", function () {
window.ApexCharts && (new ApexCharts(document.getElementById('chart-revenue-bg'), {
  chart: {
    type: "area",
    fontFamily: 'inherit',
    height: 40.0,
    sparkline: {
      enabled: true
    },
    animations: {
      enabled: false
    },
  },
  dataLabels: {
    enabled: false,
  },
  fill: {
    opacity: .16,
    type: 'solid'
  },
  stroke: {
    width: 2,
    lineCap: "round",
    curve: "smooth",
  },
  series: [{
    name: "Profits",
    data: [37, 35, 44, 28, 36, 24, 65, 31, 37, 39, 62, 51, 35, 41, 35, 27, 93, 53, 61, 27, 54, 43, 19, 46, 39, 62, 51, 35, 41, 67]
  }],
  tooltip: {
    theme: 'dark'
  },
  grid: {
    strokeDashArray: 4,
  },
  xaxis: {
    labels: {
      padding: 0,
    },
    tooltip: {
      enabled: false
    },
    axisBorder: {
      show: false,
    },
    type: 'datetime',
  },
  yaxis: {
    labels: {
      padding: 4
    },
  },
  labels: [
    '2020-06-20', '2020-06-21', '2020-06-22', '2020-06-23', '2020-06-24', '2020-06-25', '2020-06-26', '2020-06-27', '2020-06-28', '2020-06-29', '2020-06-30', '2020-07-01', '2020-07-02', '2020-07-03', '2020-07-04', '2020-07-05', '2020-07-06', '2020-07-07', '2020-07-08', '2020-07-09', '2020-07-10', '2020-07-11', '2020-07-12', '2020-07-13', '2020-07-14', '2020-07-15', '2020-07-16', '2020-07-17', '2020-07-18', '2020-07-19'
  ],
  colors: [tabler.getColor("primary")],
  legend: {
    show: false,
  },
})).render();
});
// @formatter:on
</script>
<script>
// @formatter:off
document.addEventListener("DOMContentLoaded", function () {
window.ApexCharts && (new ApexCharts(document.getElementById('chart-new-clients'), {
  chart: {
    type: "line",
    fontFamily: 'inherit',
    height: 40.0,
    sparkline: {
      enabled: true
    },
    animations: {
      enabled: false
    },
  },
  fill: {
    opacity: 1,
  },
  stroke: {
    width: [2, 1],
    dashArray: [0, 3],
    lineCap: "round",
    curve: "smooth",
  },
  series: [{
    name: "May",
    data: [37, 35, 44, 28, 36, 24, 65, 31, 37, 39, 62, 51, 35, 41, 35, 27, 93, 53, 61, 27, 54, 43, 4, 46, 39, 62, 51, 35, 41, 67]
  },{
    name: "April",
    data: [93, 54, 51, 24, 35, 35, 31, 67, 19, 43, 28, 36, 62, 61, 27, 39, 35, 41, 27, 35, 51, 46, 62, 37, 44, 53, 41, 65, 39, 37]
  }],
  tooltip: {
    theme: 'dark'
  },
  grid: {
    strokeDashArray: 4,
  },
  xaxis: {
    labels: {
      padding: 0,
    },
    tooltip: {
      enabled: false
    },
    type: 'datetime',
  },
  yaxis: {
    labels: {
      padding: 4
    },
  },
  labels: [
    '2020-06-20', '2020-06-21', '2020-06-22', '2020-06-23', '2020-06-24', '2020-06-25', '2020-06-26', '2020-06-27', '2020-06-28', '2020-06-29', '2020-06-30', '2020-07-01', '2020-07-02', '2020-07-03', '2020-07-04', '2020-07-05', '2020-07-06', '2020-07-07', '2020-07-08', '2020-07-09', '2020-07-10', '2020-07-11', '2020-07-12', '2020-07-13', '2020-07-14', '2020-07-15', '2020-07-16', '2020-07-17', '2020-07-18', '2020-07-19'
  ],
  colors: [tabler.getColor("primary"), tabler.getColor("gray-600")],
  legend: {
    show: false,
  },
})).render();
});
// @formatter:on
</script>
<script>
// @formatter:off
document.addEventListener("DOMContentLoaded", function () {
window.ApexCharts && (new ApexCharts(document.getElementById('chart-active-users'), {
  chart: {
    type: "bar",
    fontFamily: 'inherit',
    height: 40.0,
    sparkline: {
      enabled: true
    },
    animations: {
      enabled: false
    },
  },
  plotOptions: {
    bar: {
      columnWidth: '50%',
    }
  },
  dataLabels: {
    enabled: false,
  },
  fill: {
    opacity: 1,
  },
  series: [{
    name: "Profits",
    data: [37, 35, 44, 28, 36, 24, 65, 31, 37, 39, 62, 51, 35, 41, 35, 27, 93, 53, 61, 27, 54, 43, 19, 46, 39, 62, 51, 35, 41, 67]
  }],
  tooltip: {
    theme: 'dark'
  },
  grid: {
    strokeDashArray: 4,
  },
  xaxis: {
    labels: {
      padding: 0,
    },
    tooltip: {
      enabled: false
    },
    axisBorder: {
      show: false,
    },
    type: 'datetime',
  },
  yaxis: {
    labels: {
      padding: 4
    },
  },
  labels: [
    '2020-06-20', '2020-06-21', '2020-06-22', '2020-06-23', '2020-06-24', '2020-06-25', '2020-06-26', '2020-06-27', '2020-06-28', '2020-06-29', '2020-06-30', '2020-07-01', '2020-07-02', '2020-07-03', '2020-07-04', '2020-07-05', '2020-07-06', '2020-07-07', '2020-07-08', '2020-07-09', '2020-07-10', '2020-07-11', '2020-07-12', '2020-07-13', '2020-07-14', '2020-07-15', '2020-07-16', '2020-07-17', '2020-07-18', '2020-07-19'
  ],
  colors: [tabler.getColor("primary")],
  legend: {
    show: false,
  },
})).render();
});
// @formatter:on
</script>
<script>
// @formatter:off
document.addEventListener("DOMContentLoaded", function () {
window.ApexCharts && (new ApexCharts(document.getElementById('chart-mentions'), {
  chart: {
    type: "bar",
    fontFamily: 'inherit',
    height: 240,
    parentHeightOffset: 0,
    toolbar: {
      show: false,
    },
    animations: {
      enabled: false
    },
    stacked: true,
  },
  plotOptions: {
    bar: {
      columnWidth: '50%',
    }
  },
  dataLabels: {
    enabled: false,
  },
  fill: {
    opacity: 1,
  },
  series: [{
    name: "Web",
    data: [1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 2, 12, 5, 8, 22, 6, 8, 6, 4, 1, 8, 24, 29, 51, 40, 47, 23, 26, 50, 26, 41, 22, 46, 47, 81, 46, 6]
  },{
    name: "Social",
    data: [2, 5, 4, 3, 3, 1, 4, 7, 5, 1, 2, 5, 3, 2, 6, 7, 7, 1, 5, 5, 2, 12, 4, 6, 18, 3, 5, 2, 13, 15, 20, 47, 18, 15, 11, 10, 0]
  },{
    name: "Other",
    data: [2, 9, 1, 7, 8, 3, 6, 5, 5, 4, 6, 4, 1, 9, 3, 6, 7, 5, 2, 8, 4, 9, 1, 2, 6, 7, 5, 1, 8, 3, 2, 3, 4, 9, 7, 1, 6]
  }],
  tooltip: {
    theme: 'dark'
  },
  grid: {
    padding: {
      top: -20,
      right: 0,
      left: -4,
      bottom: -4
    },
    strokeDashArray: 4,
    xaxis: {
      lines: {
        show: true
      }
    },
  },
  xaxis: {
    labels: {
      padding: 0,
    },
    tooltip: {
      enabled: false
    },
    axisBorder: {
      show: false,
    },
    type: 'datetime',
  },
  yaxis: {
    labels: {
      padding: 4
    },
  },
  labels: [
    '2020-06-20', '2020-06-21', '2020-06-22', '2020-06-23', '2020-06-24', '2020-06-25', '2020-06-26', '2020-06-27', '2020-06-28', '2020-06-29', '2020-06-30', '2020-07-01', '2020-07-02', '2020-07-03', '2020-07-04', '2020-07-05', '2020-07-06', '2020-07-07', '2020-07-08', '2020-07-09', '2020-07-10', '2020-07-11', '2020-07-12', '2020-07-13', '2020-07-14', '2020-07-15', '2020-07-16', '2020-07-17', '2020-07-18', '2020-07-19', '2020-07-20', '2020-07-21', '2020-07-22', '2020-07-23', '2020-07-24', '2020-07-25', '2020-07-26'
  ],
  colors: [tabler.getColor("primary"), tabler.getColor("primary", 0.8), tabler.getColor("green", 0.8)],
  legend: {
    show: false,
  },
})).render();
});
// @formatter:on
</script>
<script>
// @formatter:on
document.addEventListener("DOMContentLoaded", function() {
const map = new jsVectorMap({
  selector: '#map-world',
  map: 'world',
  backgroundColor: 'transparent',
  regionStyle: {
    initial: {
      fill: tabler.getColor('body-bg'),
      stroke: tabler.getColor('border-color'),
      strokeWidth: 2,
    }
  },
  zoomOnScroll: false,
  zoomButtons: false,
  // -------- Series --------
  visualizeData: {
    scale: [tabler.getColor('bg-surface'), tabler.getColor('primary')],
    values: { "AF": 16, "AL": 11, "DZ": 158, "AO": 85, "AG": 1, "AR": 351, "AM": 8, "AU": 1219, "AT": 366, "AZ": 52, "BS": 7, "BH": 21, "BD": 105, "BB": 3, "BY": 52, "BE": 461, "BZ": 1, "BJ": 6, "BT": 1, "BO": 19, "BA": 16, "BW": 12, "BR": 2023, "BN": 11, "BG": 44, "BF": 8, "BI": 1, "KH": 11, "CM": 21, "CA": 1563, "CV": 1, "CF": 2, "TD": 7, "CL": 199, "CN": 5745, "CO": 283, "KM": 0, "CD": 12, "CG": 11, "CR": 35, "CI": 22, "HR": 59, "CY": 22, "CZ": 195, "DK": 304, "DJ": 1, "DM": 0, "DO": 50, "EC": 61, "EG": 216, "SV": 21, "GQ": 14, "ER": 2, "EE": 19, "ET": 30, "FJ": 3, "FI": 231, "FR": 2555, "GA": 12, "GM": 1, "GE": 11, "DE": 3305, "GH": 18, "GR": 305, "GD": 0, "GT": 40, "GN": 4, "GW": 0, "GY": 2, "HT": 6, "HN": 15, "HK": 226, "HU": 132, "IS": 12, "IN": 1430, "ID": 695, "IR": 337, "IQ": 84, "IE": 204, "IL": 201, "IT": 2036, "JM": 13, "JP": 5390, "JO": 27, "KZ": 129, "KE": 32, "KI": 0, "KR": 986, "KW": 117, "KG": 4, "LA": 6, "LV": 23, "LB": 39, "LS": 1, "LR": 0, "LY": 77, "LT": 35, "LU": 52, "MK": 9, "MG": 8, "MW": 5, "MY": 218, "MV": 1, "ML": 9, "MT": 7, "MR": 3, "MU": 9, "MX": 1004, "MD": 5, "MN": 5, "ME": 3, "MA": 91, "MZ": 10, "MM": 35, "NA": 11, "NP": 15, "NL": 770, "NZ": 138, "NI": 6, "NE": 5, "NG": 206, "NO": 413, "OM": 53, "PK": 174, "PA": 27, "PG": 8, "PY": 17, "PE": 153, "PH": 189, "PL": 438, "PT": 223, "QA": 126, "RO": 158, "RU": 1476, "RW": 5, "WS": 0, "ST": 0, "SA": 434, "SN": 12, "RS": 38, "SC": 0, "SL": 1, "SG": 217, "SK": 86, "SI": 46, "SB": 0, "ZA": 354, "ES": 1374, "LK": 48, "KN": 0, "LC": 1, "VC": 0, "SD": 65, "SR": 3, "SZ": 3, "SE": 444, "CH": 522, "SY": 59, "TW": 426, "TJ": 5, "TZ": 22, "TH": 312, "TL": 0, "TG": 3, "TO": 0, "TT": 21, "TN": 43, "TR": 729, "TM": 0, "UG": 17, "UA": 136, "AE": 239, "GB": 2258, "US": 4624, "UY": 40, "UZ": 37, "VU": 0, "VE": 285, "VN": 101, "YE": 30, "ZM": 15, "ZW": 5 },
  },
});
window.addEventListener("resize", () => {
  map.updateSize();
});
});
// @formatter:off
</script>
<script>
// @formatter:off
document.addEventListener("DOMContentLoaded", function () {
window.ApexCharts && (new ApexCharts(document.getElementById('sparkline-activity'), {
  chart: {
    type: "radialBar",
    fontFamily: 'inherit',
    height: 40,
    width: 40,
    animations: {
      enabled: false
    },
    sparkline: {
      enabled: true
    },
  },
  tooltip: {
    enabled: false,
  },
  plotOptions: {
    radialBar: {
      hollow: {
        margin: 0,
        size: '75%'
      },
      track: {
        margin: 0
      },
      dataLabels: {
        show: false
      }
    }
  },
  colors: [tabler.getColor("blue")],
  series: [35],
})).render();
});
// @formatter:on
</script>
<script>
// @formatter:off
document.addEventListener("DOMContentLoaded", function () {
window.ApexCharts && (new ApexCharts(document.getElementById('chart-development-activity'), {
  chart: {
    type: "area",
    fontFamily: 'inherit',
    height: 192,
    sparkline: {
      enabled: true
    },
    animations: {
      enabled: false
    },
  },
  dataLabels: {
    enabled: false,
  },
  fill: {
    opacity: .16,
    type: 'solid'
  },
  stroke: {
    width: 2,
    lineCap: "round",
    curve: "smooth",
  },
  series: [{
    name: "Purchases",
    data: [3, 5, 4, 6, 7, 5, 6, 8, 24, 7, 12, 5, 6, 3, 8, 4, 14, 30, 17, 19, 15, 14, 25, 32, 40, 55, 60, 48, 52, 70]
  }],
  tooltip: {
    theme: 'dark'
  },
  grid: {
    strokeDashArray: 4,
  },
  xaxis: {
    labels: {
      padding: 0,
    },
    tooltip: {
      enabled: false
    },
    axisBorder: {
      show: false,
    },
    type: 'datetime',
  },
  yaxis: {
    labels: {
      padding: 4
    },
  },
  labels: [
    '2020-06-20', '2020-06-21', '2020-06-22', '2020-06-23', '2020-06-24', '2020-06-25', '2020-06-26', '2020-06-27', '2020-06-28', '2020-06-29', '2020-06-30', '2020-07-01', '2020-07-02', '2020-07-03', '2020-07-04', '2020-07-05', '2020-07-06', '2020-07-07', '2020-07-08', '2020-07-09', '2020-07-10', '2020-07-11', '2020-07-12', '2020-07-13', '2020-07-14', '2020-07-15', '2020-07-16', '2020-07-17', '2020-07-18', '2020-07-19'
  ],
  colors: [tabler.getColor("primary")],
  legend: {
    show: false,
  },
  point: {
    show: false
  },
})).render();
});
// @formatter:on
</script>
<script>
// @formatter:off
document.addEventListener("DOMContentLoaded", function () {
window.ApexCharts && (new ApexCharts(document.getElementById('sparkline-bounce-rate-1'), {
  chart: {
    type: "line",
    fontFamily: 'inherit',
    height: 24,
    animations: {
      enabled: false
    },
    sparkline: {
      enabled: true
    },
  },
  tooltip: {
    enabled: false,
  },
  stroke: {
    width: 2,
    lineCap: "round",
  },
  series: [{
    color: tabler.getColor("primary"),
    data: [17, 24, 20, 10, 5, 1, 4, 18, 13]
  }],
})).render();
});
// @formatter:on
</script>
<script>
// @formatter:off
document.addEventListener("DOMContentLoaded", function () {
window.ApexCharts && (new ApexCharts(document.getElementById('sparkline-bounce-rate-2'), {
  chart: {
    type: "line",
    fontFamily: 'inherit',
    height: 24,
    animations: {
      enabled: false
    },
    sparkline: {
      enabled: true
    },
  },
  tooltip: {
    enabled: false,
  },
  stroke: {
    width: 2,
    lineCap: "round",
  },
  series: [{
    color: tabler.getColor("primary"),
    data: [13, 11, 19, 22, 12, 7, 14, 3, 21]
  }],
})).render();
});
// @formatter:on
</script>
<script>
// @formatter:off
document.addEventListener("DOMContentLoaded", function () {
window.ApexCharts && (new ApexCharts(document.getElementById('sparkline-bounce-rate-3'), {
  chart: {
    type: "line",
    fontFamily: 'inherit',
    height: 24,
    animations: {
      enabled: false
    },
    sparkline: {
      enabled: true
    },
  },
  tooltip: {
    enabled: false,
  },
  stroke: {
    width: 2,
    lineCap: "round",
  },
  series: [{
    color: tabler.getColor("primary"),
    data: [10, 13, 10, 4, 17, 3, 23, 22, 19]
  }],
})).render();
});
// @formatter:on
</script>
<script>
// @formatter:off
document.addEventListener("DOMContentLoaded", function () {
window.ApexCharts && (new ApexCharts(document.getElementById('sparkline-bounce-rate-4'), {
  chart: {
    type: "line",
    fontFamily: 'inherit',
    height: 24,
    animations: {
      enabled: false
    },
    sparkline: {
      enabled: true
    },
  },
  tooltip: {
    enabled: false,
  },
  stroke: {
    width: 2,
    lineCap: "round",
  },
  series: [{
    color: tabler.getColor("primary"),
    data: [6, 15, 13, 13, 5, 7, 17, 20, 19]
  }],
})).render();
});
// @formatter:on
</script>
<script>
// @formatter:off
document.addEventListener("DOMContentLoaded", function () {
window.ApexCharts && (new ApexCharts(document.getElementById('sparkline-bounce-rate-5'), {
  chart: {
    type: "line",
    fontFamily: 'inherit',
    height: 24,
    animations: {
      enabled: false
    },
    sparkline: {
      enabled: true
    },
  },
  tooltip: {
    enabled: false,
  },
  stroke: {
    width: 2,
    lineCap: "round",
  },
  series: [{
    color: tabler.getColor("primary"),
    data: [2, 11, 15, 14, 21, 20, 8, 23, 18, 14]
  }],
})).render();
});
// @formatter:on
</script>
<script>
// @formatter:off
document.addEventListener("DOMContentLoaded", function () {
window.ApexCharts && (new ApexCharts(document.getElementById('sparkline-bounce-rate-6'), {
  chart: {
    type: "line",
    fontFamily: 'inherit',
    height: 24,
    animations: {
      enabled: false
    },
    sparkline: {
      enabled: true
    },
  },
  tooltip: {
    enabled: false,
  },
  stroke: {
    width: 2,
    lineCap: "round",
  },
  series: [{
    color: tabler.getColor("primary"),
    data: [22, 12, 7, 14, 3, 21, 8, 23, 18, 14]
  }],
})).render();
});
// @formatter:on
</script>
</body>
</html>