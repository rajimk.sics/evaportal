@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">
                   
              <div class="card">
                <div class="card-body">

                   
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>
                          <li><a href="#">{{$title}}</a></li>
                        </ol>
                      </nav>
                    <h4 class="card-title">{{$title}}</h4>
                    <div class="payment-grd">
                      <form id="searchchrys_form" method="GET" enctype="multipart/form-data" action="{{ url('/college_updateReport') }}">
                          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                  
                          <div class="row">
                              <div class="col">
                                  <div class="form-group formcontents">
                                      <label for="year">Select Year</label>
                                      <select class="form-select" name="year" id="year">
                                        <option value="" >Select Year</option>  
                                          @foreach($years as $year)
                                              <option value="{{ $year }}" {{ $year == $currentYear ? 'selected' : '' }}>{{ $year }}</option>
                                          @endforeach
                                      </select>
                                  </div>
                              </div>
                  
                              <div class="col">
                                  <div class="form-group formcontents">
                                      <label for="month">Select Month</label>
                                      <select class="form-select" name="month" id="month">  
                                        <option value="" >Select Month</option> 
                                          @foreach($months as $key => $month)
                                              <option value="{{ $key }}" {{ $key == $currentMonth ? 'selected' : '' }} >{{ $month }}</option>
                                          @endforeach
                                      </select>
                                  </div>
                              </div>
                  
                              <div class="col">
                                  <div class="form-group formcontents">
                                      <label class="reports">Reports of</label>
                                      <select class="form-select selectdesi" name="reported_to[]"  id="reported_to" multiple="multiple">
                                        <option value="">Select Salesperson</option>
                                          @foreach($reportedlist as $reportedlist )
                                              <option value="{{$reportedlist->id}}" {{ in_array( $reportedlist->id,$reported_to) ? 'selected' : '' }}>{{$reportedlist->name}}</option>
                                          @endforeach                     
                                      </select>
                                  </div>
                              </div>         
                              
                              <div class="col">  
                                  <div class="form-group formcontents">
                                      <label>Start date</label>
                                      <input type="text" class="form-control" data-zdp_readonly_element="true" name="start_collegeupdate" value="{{$start_date}}" id="start_collegeupdate">
                                  </div>
                              </div>
                  
                              <div class="col">
                                  <div class="form-group formcontents">
                                      <label>End Date</label>
                                      <input type="text" class="form-control" data-zdp_readonly_element="true" name="end_collegeupdate" value="{{$end_date}}" id="end_collegeupdate">
                                  </div>
                              </div>
                  
                              <div class="col d-flex align-items-center">
                                  <div class="form-group formcontents mb-0 flex-row">
                                      <button type="submit" class="btn btn-primary me-2">Submit</button>
                                    
                                  </div>
                              </div>
                          </div>      
                      </form> 
                  </div>
                  
                  <!-- New row for the "View Current Month" button -->
                  <div class="row mt-3">
                      <div class="col">
                          <div class="form-group formcontents">
                            <a href="{{ url('/college_updateReport') . '?year=' . now()->year . '&month=' . now()->month }}" class="btn btn-primary">View Current Month</a>
                            <a href="{{url('/college_updateReport')}}" class="btn btn-primary"> Clear All</a>
                          </div>
                      </div>
                  </div>
                  


                    <div class="table-responsive">
                      <table id="example" class="table table table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>College</th>
                <th>Date</th>
                <th>Department</th>
                <th>POC</th>
                <th>Comments</th>
                <th>Action</th>  
            </tr>
        </thead>
        <tbody>
<?php $i=1;?>
            @foreach($report as $collegeUpdate)
            @php
  $isOld = (strtotime(date('Y-m-d')) - strtotime($collegeUpdate->date)) / (60 * 60 * 24) > 5;
@endphp
            <tr id="row_{{$collegeUpdate->id}}"  class="{{ $isOld==1 ? 'highlight-red' : '' }}"   >
                <td>{{$i}}</td>
                @php
                $colName=App\Helpers\CustomHelper::collegeName($collegeUpdate->college_id); 
                 @endphp
              
                 <td>{{$colName['college']}}@if(!is_null($colName['name']))
                  , {{ strtoupper($colName['name']) }}  @endif
                  </td>
                  <td>{{date("d-m-Y", strtotime($collegeUpdate->date))}} </td>
                  <td>{{$collegeUpdate->dept}}</td>
                  <td>{{$collegeUpdate->pocs}}</td>
                   <td>{{$collegeUpdate->comment}}</td>
                <td>
                  @if(in_array(Auth::user()->role, [2]))
                 <button type="button" class="btn btn-blue btn-sm text-white" fdprocessedid="wf07gv"  onclick="updateReports(this)" data-updateid="{{$collegeUpdate->id}}" >
                    Details
                  </button></a>
                  <button type="button" class="btn btn-green btn-sm text-white" fdprocessedid="wf07gv"  onclick="collegeUpdates(this)" data-collegeid="{{$collegeUpdate->college_id}}" data-dept="{{$collegeUpdate->department}}" data-poc="{{$collegeUpdate->poc_ids}}">
                    Update
                  </button></a>
                  <button type="button" class="btn btn-blue btn-sm text-white  me-3" fdprocessedid="wf07gv" onclick="getCollegeUpdateHistory('{{$collegeUpdate->college_id}}')">
                    Updated History
                    </button> 
                  @endif
                 
                   </td> 
                                        
                   
                                          
               
            </tr>
            <?php $i++;?>
            @endforeach
           
        </tbody>
      
    </table>

                    </div>
                  </div>
              </div>
            </div>
        </div>
        
    <div class="modal fade" id="collegeUpdateReport" tabindex="-1" role="dialog" aria-labelledby="collegeModalLabel">
      <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="collegeUpdateReportLabel">Update Details</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                 
              </div>
              <div class="modal-body">
              
              
                <div id="modalContent">
                      
                  </div>
              </div>
          </div>
      </div>
  </div>
   
<!--Update model-->

<div class="modal modal-blur fade" id="modalCollegeUpdates" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">College Updates</h5>
      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <form id="collegeups" method="POST" enctype="multipart/form-data"  action="{{url('/college_update')}}">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="collegeid" id="collegeid">
      <input type="hidden" name="updatetype" id="updatetype">
    <div class="modal-body">
     
      <div class="row">
        <div class="col-lg-6">
          <div class="form-group">
            <label class="form-label">Department<sup>*</sup></label>
            <select class="form-select selectclass" id="collegedep" name="collegedep" >
             
            </select>
           
          </div>
          </div>
          <div class="col-lg-6">
            <div class="form-group">
              <label class="form-label">POC Designation<sup>*</sup></label>
              <select class="form-select selectclass" id="collegepoc" name="collegepoc[]" multiple="multiple">
           
              </select>
            </div>
          </div>
        </div>
        <div class="row">
        <div class="col-lg-6">
          <div class="form-group">
            <label class="form-label">Comments<sup>*</sup></label>
            <textarea class="form-control" rows="5" name="comments" id="comments"></textarea>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="form-group">
            <label class="form-label">Status<sup>*</sup></label>
            <textarea class="form-control" rows="5" name="updatestatus" id="updatestatus"></textarea>
          </div>
        </div>
      </div>
    </div>
    
  <div class="modal-footer">
    <button type="submit" id="college_edit" class="btn btn-primary">Update</button>
  </div>                                            
  </div>

</div>
</div>
</form>

{{-- Updated college history --}}
<div class="modal fade" id="collegeHistoryModal" tabindex="-1" aria-labelledby="collegeHistoryModalLabel">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="collegeHistoryModalLabel">College Update History</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              <h4 id="collegeName"></h4>
              <div id="collegeUpdates">
              </div>
          </div>
          <div class="modal-footer">
          </div>
      </div>
  </div>
</div>

<div class="modal fade" id="messageModal" tabindex="-1" aria-labelledby="messageModalLabel" >
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="messageModalLabel">Notice</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
              </button>
          </div>
          <div class="modal-body">
              
          </div>
      </div>
  </div>
</div>
@endsection
