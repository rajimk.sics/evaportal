@extends('layouts.main')

@section('content')


        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">

              <input type="hidden" id="token_eva" name="_token" value="{{ csrf_token() }}">
                   
              <div class="card">
                <div class="card-body">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                          <li><a href="{{url('/home')}}">Home</a></li>
                          <li><a href="#">{{$title}}</a></li>
                        </ol>
                      </nav>
                    <h4 class="card-title"> {{$title}}</h4>
                    <div class="table-responsive">
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Student Name</th>
                <th>Email</th>
                <th>SICS Reg Number</th>
                @if(Auth::user()->role!=2)
                <th>Contact Number</th>
                @endif
                <th>Technology</th>               
                <th>POC Code</th>
                <th>Date of Joining</th>

            </tr>
        </thead>
        <tbody>
          <?php $i=1;?>
            @foreach($studlist as $studlist)
            <tr>
                <td>{{$i}}</td>
                <td>{{ucfirst($studlist->name)}}</td>
                <td>{{$studlist->email}}</td>      
                <td>SIAC{{$studlist->student_admissionid}}</td>  
                @if(Auth::user()->role!=2)       
                <td>{{$studlist->phone}}</td>
                @endif

                @php
                    $techname=App\Helpers\CustomHelper::technology($studlist->id); 
                 @endphp

                 
                <td>{{$techname}}</td>
                @php
                  $code=App\Helpers\CustomHelper::ucode($studlist->sales_id); 
                @endphp
                <td>{{$code['poc_code']}}</td>
                <td>{{$studlist->stud_doj}}</td>
      
            </tr>
            <?php $i++;?>
            @endforeach
           
        </tbody>
      
    </table>

















                    </div>
                </div>
           
            </div>
            <script>
        @if (Session::has('message'))
            swal({
                title: "",
                text: "{{ Session::get('message') }}",
                type: "success",
                showCancelButton: false,
                dangerMode: false,
                confirmButtonText: 'OK',
            });
        @endif
        </script>




@endsection
