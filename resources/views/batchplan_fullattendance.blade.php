@extends('layouts.main')

@section('content')
    <div class="page-body">
        <div class="container-xl">
            <style>
                .shadow-box {
                    background: #fff;
                    padding: 15px;
                    box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.2);
                    border-radius: 8px;
                    margin-bottom: 20px;
                }

                .shadow-box h3 {
                    margin-bottom: 10px;
                }

                .bold-text {
                    display: inline;
                    margin-left: 10px;
                }

                .inline-elements {
                    display: flex;
                    align-items: flex-start;
                }
            </style>
            <input name="_token" type="hidden" id="token_eva" value="{{ csrf_token() }}">

            <div class="card mb-3">
                <div class="card-body">

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumbs">
                            <li><a href="{{ url('/home') }}">Home</a></li>
                            <li><a href="{{ url('/studentAttendance') }}">Attendance</a></li>
                            <li><a href="{{ url('/studentFullAttendance/' . $studentId) }}">{{ ucfirst($studentName) }}'s
                                    Full Attendance</a></li>
                        </ol>
                    </nav>

                    <div class="d-flex justify-content-center align-items-center mb-4">
                        <h2 class="mb-0 ">{{ ucfirst($studentName) }}</h2>
                        <p class="mb-0 ml-2">({{ $package }})</p>
                    </div>

                    <div class="row">
                        <div class="box col-lg-2 shadow-box">
                            <div class="inline-elements">
                                <h3>Total Present :</h3>
                                <h3 class="bold-text">{{ $totalPresent }}</h3>
                            </div>
                        </div>
                        <div class="box col-lg-2 shadow-box">
                            <div class="inline-elements">
                                <h3>Total Absent :</h3>
                                <h3 class="bold-text">{{ $totalAbsent }}</h3>
                            </div>
                        </div>
                        <div class="box col-lg-2 shadow-box">
                            <div class="inline-elements">
                                <h3>Percentage :</h3>
                                <h3 class="bold-text">{{ $attendancePercentage }}%</h3>
                            </div>
                        </div>
                    </div>


                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Month & Year</th>
                                    @foreach ($daysInMonth as $day)
                                        <th>{{ $day }}
                                            ({{ \Carbon\Carbon::createFromDate($currentYear, $currentMonth, $day)->format('D') }})
                                        </th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                @foreach ($groupedAttendance as $monthYear => $attendance)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ \Carbon\Carbon::parse($monthYear . '-01')->format('F Y') }}</td>

                                        @foreach ($daysInMonth as $day)
                                            @php
                                                $attendanceStatus = $attendance[$day] ?? null;
                                            @endphp
                                            <td class="text-center"
                                                style="color: {{ $attendanceStatus === 1 ? 'green' : ($attendanceStatus === 0 ? 'red' : 'black') }}">
                                                {{ $attendanceStatus === 1 ? 'P' : ($attendanceStatus === 0 ? 'A' : '-') }}
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            @if ($groupedAttendance->isEmpty())
                <p class="text-center text-muted">No attendance records found for this student.</p>
            @endif
        </div>
    </div>
@endsection
