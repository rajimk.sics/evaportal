window.baseurl = 'https://eva.sicsapp.com/';

var feesfinal;
var duration;
var btnchange=0;
function formatDate(date) {
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    // Add leading zero if needed
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }

    return year + "-" + month + "-" + day;
}
function daysadd(inputdate) {
    var inputDate = new Date(inputdate);
    var daysToAdd = parseInt(10);
    var resultDate = new Date(inputDate);
    resultDate.setDate(resultDate.getDate() + daysToAdd);
    var formattedDate = formatDate(resultDate);
    return formattedDate;
}
function monthadd(inputdate) {
    var inputDate = new Date(inputdate);
    inputDate.setMonth(inputDate.getMonth() + 1);
    var newMonth = inputDate.getMonth() + 1; // Adding 1 because months are zero-indexed
    var newYear = inputDate.getFullYear();
    var day = inputDate.getDate();

    if (day < 10) {
        day = "0" + day;
    }

    // Display the new date
    var formattedDate = newYear + "-" + (newMonth < 10 ? "0" + newMonth : newMonth) + "-" + day;

    return formattedDate;
}

$(document).ready(function () {
    $("#packname").on("change", function () {

        $("#reductionvaldiv").css("display", "none");
        $("#reducedfees_div").css("display", "none");
        $("#redtaxdiv").css("display", "none");
        $("#afterreduction_div").css("display", "none");
        $("#ineffect_div").css("display", "none");

        $("#reduction_amount").val('');
        $("#reducedfees").val('');
        $("#reducedtax").val('');
        $("#after_reduction").val('');
        $("#ineffect_offered").val('');
        
       
        var package = this.value;

        $("#reductioncheck").prop("checked", false);

        if (package != "") {
            data = {
                _token: $("#token_eva").val(),
                package: package,
            };
            $.ajax({
                url: baseurl + "packagedetails",
                type: "POST",
                data: data,
                success: function (res) {
                    var obj = jQuery.parseJSON(res);
                    var fees = obj.package_details.fee;
                    var tax = obj.package_details.tax;
                    var total = obj.package_details.total;

                    $("#packfee").val(fees);
                    $("#packtax").val(tax);
                    $("#packtot").val(total);
                    $("#finalamount").val(total);

                    $("#packtot").val(total);
                    var max_range = Math.round((fees * 15) / 100);

                    $("#reduction_amount_span").html("Minimum-1 and Maximum " + max_range);

                    $("#regfees_hidden").val(total);
                    $("#regfees_hidden_span").html("Minimum-1 and Maximum-" + total);

                    // $('#regfees').attr('max',total);
                    $("#max_range").val(max_range);

                    feesfinal = total;
                    duration = Math.floor(obj.package_details.duration);

                    $("#costdiv").css("display", "block");
                    $("#taxdiv").css("display", "block");
                    $("#totaldiv").css("display", "block");

                    $("#reductiondiv").css("display", "block");
                },
            });
        } else {
            $("#costdiv").css("display", "none");
            $("#taxdiv").css("display", "none");
            $("#totaldiv").css("display", "none");
            $("#reductiondiv").css("display", "none");
            $("#reductionvaldiv").css("display", "none");
            $("#reducedfees_div").css("display", "none");
            $("#redtaxdiv").css("display", "none");
            $("#afterreduction_div").css("display", "none");
            $("#ineffect_div").css("display", "none");
        }
    });
    $("#reduction_amount").blur(function () {
        //alert(max_range);
        var maxval = $("#max_range").val();
        if ($("#reduction_amount").val() != "") {
            let fees = $("#packfee").val();

            let packtot = $("#packtot").val();

            let reduction_amount = $("#reduction_amount").val();

            let reducedfees = Math.round(fees - reduction_amount);
            let reducedtax = Math.round((reducedfees * 18) / 100);
            let after_reduction = Math.round(reducedfees + reducedtax);

            let ineffect_offered = Math.round(packtot - after_reduction);

            feesfinal = after_reduction;

            $("#reducedfees").val(reducedfees);
            $("#reducedtax").val(reducedtax);
            $("#after_reduction").val(after_reduction);

            $("#finalamount").val(after_reduction);
            $("#ineffect_offered").val(ineffect_offered);

            $("#regfees_hidden").val(after_reduction);

            $("#regfees_hidden_span").html("Minimum-1 and Maximum-" + after_reduction);
        } else {
            $("#reducedfees").val("");
            $("#reducedtax").val("");
            $("#after_reduction").val("");
            $("#ineffect_offered").val("");
        }
    });
    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;
    var current = 1;
    var steps = $("fieldset").length;
    setProgressBar(current);
   

    $(".next").click(function (e) {
        current_fs = $(this).parent();
        next_fs = $(this).parent().next();
       
     
        var form = $(".msformpack");
        form.validate({
            rules: {
                oldpay_date: {
                    required: true,
                },
                packname: {
                    required: true,
                },
                reduction_amount: {
                    required: true,
                },
                regfees: {
                    required: true,
                },

                referenece_list: {
                    required: true,
                },

                inst_red: {
                    required: true,
                },
                paydate: {
                    required: true,
                },
                payMethod_bank: {
                    required: true,
                },
                package_reciept: {
                    required: true,
                },
                trans_id: {
                    required: true,
                },

                email: {
                    required: true,
                    email: true,
                },
                password: {
                    required: true,
                    letters_numbers_special: true,
                },
            },
        });
        if (form.valid() === true) {
          

            if ($("input[name='payMethod_bank']:checked").val() == "bank" && btnchange==0){
               

                var data = {
                    _token: $("#token_eva").val(),
                    trans_id: $("#trans_id").val(),
                };

                $.ajax({
                    url: baseurl + "transaction_unique",
                    type: "POST",
                    data: data,
                    success: function (res) {
                        

                        if (res == 0) {
                            //Add Class Active
                            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                            var reducedduration = duration - 1;

                            if (reducedduration != 0) {
                                var monthlyfees = Math.round(feesfinal / (duration - 1));
                            } else {
                                reducedduration = 1;
                                var monthlyfees = Math.round(feesfinal);
                            }
                            var closed_dates = $("#closed_contact").val();
                            var inst_red = $("#inst_red").val();
                            var finalamount = $("#finalamount").val();
                            var inputDate = new Date(inst_red);
                            var resultDate = new Date(inputDate);
                            resultDate.setDate(resultDate.getDate());
                            var formattedDatefirst = formatDate(resultDate);

                            var inputDate1 = new Date(closed_dates);
                            var resultDate1 = new Date(inputDate1);
                            resultDate1.setDate(resultDate1.getDate());
                            var formattedDate = formatDate(resultDate1);

                            $("#splitup tbody").html("");
                            var opt = "";

                            var opthidden = "";

                            var total = 0;

                            for (i = 1; i <= reducedduration; i++) {
                                if (i == 1) {
                                    var firstdate1 = formattedDatefirst;
                                    var firstdate = closed_dates;
                                } else {
                                    //alert(firstdate);
                                    var splitdate = monthadd(firstdate);
                                    firstdate = splitdate;
                                }
                                if (i != reducedduration) {
                                    opt += "<tr>";
                                    opt += "<td>" + i + "</td>";
                                    opt += "<td>";
                                    opt += "<span>";

                                    total += Math.round(monthlyfees);
                                    opt += "Rs" + " " + formatPrice(Math.round(monthlyfees));

                                    opt += "</span>";
                                    opt += "</td>";
                                    opthidden += '<input type="hidden" name="monthlyfees[]" value="' + Math.round(monthlyfees) + '">';
                                } else {
                                    opt += "<tr>";
                                    opt += "<td>" + i + "</td>";
                                    opt += "<td>";
                                    opt += "<span>";

                                    total += Math.round(monthlyfees);

                                    if (finalamount == total) {
                                        opt += "Rs" + " " + formatPrice(Math.round(monthlyfees));
                                        opthidden += '<input type="hidden" name="monthlyfees[]" value="' + Math.round(monthlyfees) + '">';
                                    } else {
                                        var lastamount = total - finalamount;

                                        opt += "Rs" + " " + formatPrice(Math.round(monthlyfees) - lastamount);
                                        var am = Math.round(monthlyfees) - lastamount;

                                        opthidden += '<input type="hidden" name="monthlyfees[]" value="' + am + '">';
                                    }

                                    opt += "</span>";
                                    opt += "</td>";
                                }

                                if (i == 1) {
                                    opt += "<td>" + firstdate1 + "</td>";

                                    opthidden += '<input type="hidden" name="splitdate[]" value="' + firstdate1 + '">';
                                } else {
                                    opt += "<td>" + splitdate + "</td>";
                                    opthidden += '<input type="hidden" name="splitdate[]" value="' + splitdate + '">';
                                }
                                opt += "</tr>";
                            }

                            $("#splitup tbody").html(opt);
                            $("#splituphidden").html(opthidden);

                            //show the next fieldset
                            next_fs.show();
                            //hide the current fieldset with style
                            current_fs.animate(
                                { opacity: 0 },
                                {
                                    step: function (now) {
                                        // for making fielset appear animation
                                        opacity = 1 - now;

                                        current_fs.css({
                                            display: "none",
                                            position: "relative",
                                        });
                                        next_fs.css({ opacity: opacity });
                                    },
                                    duration: 500,
                                }
                            );
                            setProgressBar(++current);

                            return false;
                        } else {

                         

                            swal("", "Transaction Id must be unique", "error");
                        }
                    },
                });
            } else {
                btnchange=0;
                //Add Class Active
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                var reducedduration = duration - 1;

                if (reducedduration != 0) {
                    var monthlyfees = Math.round(feesfinal / (duration - 1));
                } else {
                    reducedduration = 1;
                    var monthlyfees = Math.round(feesfinal);
                }
                var closed_dates = $("#closed_contact").val();
                var inst_red = $("#inst_red").val();
                var finalamount = $("#finalamount").val();
                var inputDate = new Date(inst_red);
                var resultDate = new Date(inputDate);
                resultDate.setDate(resultDate.getDate());
                var formattedDatefirst = formatDate(resultDate);

                var inputDate1 = new Date(closed_dates);
                var resultDate1 = new Date(inputDate1);
                resultDate1.setDate(resultDate1.getDate());
                var formattedDate = formatDate(resultDate1);

                $("#splitup tbody").html("");
                var opt = "";

                var opthidden = "";

                var total = 0;

                for (i = 1; i <= reducedduration; i++) {
                    if (i == 1) {
                        var firstdate1 = formattedDatefirst;
                        var firstdate = closed_dates;
                    } else {
                        //alert(firstdate);
                        var splitdate = monthadd(firstdate);
                        firstdate = splitdate;
                    }
                    if (i != reducedduration) {
                        opt += "<tr>";
                        opt += "<td>" + i + "</td>";
                        opt += "<td>";
                        opt += "<span>";

                        total += Math.round(monthlyfees);
                        opt += "Rs" + " " + formatPrice(Math.round(monthlyfees));

                        opt += "</span>";
                        opt += "</td>";
                        opthidden += '<input type="hidden" name="monthlyfees[]" value="' + Math.round(monthlyfees) + '">';
                    } else {
                        opt += "<tr>";
                        opt += "<td>" + i + "</td>";
                        opt += "<td>";
                        opt += "<span>";

                        total += Math.round(monthlyfees);

                        if (finalamount == total) {
                            opt += "Rs" + " " + formatPrice(Math.round(monthlyfees));
                            opthidden += '<input type="hidden" name="monthlyfees[]" value="' + Math.round(monthlyfees) + '">';
                        } else {
                            var lastamount = total - finalamount;

                            opt += "Rs" + " " + formatPrice(Math.round(monthlyfees) - lastamount);
                            var am = Math.round(monthlyfees) - lastamount;

                            opthidden += '<input type="hidden" name="monthlyfees[]" value="' + am + '">';
                        }

                        opt += "</span>";
                        opt += "</td>";
                    }

                    if (i == 1) {
                        opt += "<td>" + firstdate1 + "</td>";

                        opthidden += '<input type="hidden" name="splitdate[]" value="' + firstdate1 + '">';
                    } else {
                        opt += "<td>" + splitdate + "</td>";
                        opthidden += '<input type="hidden" name="splitdate[]" value="' + splitdate + '">';
                    }
                    opt += "</tr>";
                }

                $("#splitup tbody").html(opt);
                $("#splituphidden").html(opthidden);

                //show the next fieldset
                next_fs.show();
                //hide the current fieldset with style
                current_fs.animate(
                    { opacity: 0 },
                    {
                        step: function (now) {
                            // for making fielset appear animation
                            opacity = 1 - now;

                            current_fs.css({
                                display: "none",
                                position: "relative",
                            });
                            next_fs.css({ opacity: opacity });
                        },
                        duration: 500,
                    }
                );
                setProgressBar(++current);

                return false;
            }
        }

      
        
    });

    $(".previous").click(function () {
        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();
        btnchange=1;


        $("#prevval").val(1);

        //Remove class active
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        //show the previous fieldset
        previous_fs.show();

        //hide the current fieldset with style
        current_fs.animate(
            { opacity: 0 },
            {
                step: function (now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        display: "none",
                        position: "relative",
                    });
                    previous_fs.css({ opacity: opacity });
                },
                duration: 500,
            }
        );
        setProgressBar(--current);
        buttonchange=1;
    });

    function setProgressBar(curStep) {
        var percent = parseFloat(100 / steps) * curStep;
        percent = percent.toFixed();
        $(".progress-bar").css("width", percent + "%");
    }

    $(".sub1").click(function (e) {
        var form = $(".msformpack");

        form.validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                },
                password: {
                    required: true,
                    letters_numbers_special: true,
                },
            },
        });

        if (form.valid() === true) {
            e.preventDefault();

            var data = {
                _token: $("#token_eva").val(),
                email: $("#email").val(),
            };
            $.ajax({
                url: baseurl + "emailexist",
                type: "POST",
                data: data,
                success: function (res) {
                    console.log(res);
                    $(".pageloader").show();
                    if (res == 1) {
                        $(".msformpack").removeAttr("onsubmit");
                        $(".msformpack").removeAttr("novalidate");
                        form.submit();
                    } else {
                        $(".pageloader").hide();
                        swal("", "Email id already exist", "error");
                        e.preventDefault();
                    }
                },
            });
        }
    });

    $(".sub").click(function (e) {
      var form = $(".msformpack");

if($("input[name='payMethod_bank']:checked").val()=='bank'){


      if (form.valid() === true) {
          e.preventDefault();
          var data = {
            _token: $("#token_eva").val(),
            trans_id: $("#trans_id").val(),
           
          };
          $.ajax({
              url: baseurl + "transaction_unique",
              type: "POST",
              data: data,
              success: function (res) {

                  console.log(res);
                  $(".pageloader").show();
                  if (res == 0) {
                      $(".msformpack").removeAttr("onsubmit");
                      $(".msformpack").removeAttr("novalidate");
                      form.submit();
                  } else {
                      $(".pageloader").hide();
                      swal("", "Transaction Id Must be unique", "error");
                      e.preventDefault();
                  }



              },
          });
      }
    }
    else{
        $(".msformpack").removeAttr("onsubmit");
        $(".msformpack").removeAttr("novalidate");
        form.submit();

    }



  });











});

document.querySelectorAll(".drop-zone__input").forEach((inputElement) => {
    const dropZoneElement = inputElement.closest(".drop-zone");

    dropZoneElement.addEventListener("click", (e) => {
        inputElement.click();
    });

    inputElement.addEventListener("change", (e) => {
        if (inputElement.files.length) {
            updateThumbnail(dropZoneElement, inputElement.files[0]);
        }
    });

    dropZoneElement.addEventListener("dragover", (e) => {
        e.preventDefault();
        dropZoneElement.classList.add("drop-zone--over");
    });

    ["dragleave", "dragend"].forEach((type) => {
        dropZoneElement.addEventListener(type, (e) => {
            dropZoneElement.classList.remove("drop-zone--over");
        });
    });

    dropZoneElement.addEventListener("drop", (e) => {
        e.preventDefault();

        if (e.dataTransfer.files.length) {
            inputElement.files = e.dataTransfer.files;
            updateThumbnail(dropZoneElement, e.dataTransfer.files[0]);
        }

        dropZoneElement.classList.remove("drop-zone--over");
    });
});

/**
 * Updates the thumbnail on a drop zone element.
 *
 * @param {HTMLElement} dropZoneElement
 * @param {File} file
 */
function updateThumbnail(dropZoneElement, file) {
    let thumbnailElement = dropZoneElement.querySelector(".drop-zone__thumb");

    // First time - remove the prompt
    if (dropZoneElement.querySelector(".drop-zone__prompt")) {
        dropZoneElement.querySelector(".drop-zone__prompt").remove();
    }

    // First time - there is no thumbnail element, so lets create it
    if (!thumbnailElement) {
        thumbnailElement = document.createElement("div");
        thumbnailElement.classList.add("drop-zone__thumb");
        dropZoneElement.appendChild(thumbnailElement);
    }

    thumbnailElement.dataset.label = file.name;

    // Show thumbnail for image files
    if (file.type.startsWith("image/")) {
        const reader = new FileReader();

        reader.readAsDataURL(file);
        reader.onload = () => {
            thumbnailElement.style.backgroundImage = `url('${reader.result}')`;
        };
    } else {
        thumbnailElement.style.backgroundImage = null;
    }
}
