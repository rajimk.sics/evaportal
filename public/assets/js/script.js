$(document).ready(function(){

  $("#stud_qualification").on('change', function () {

    if(this.val!=''){

      $('label[for="stud_qualification"]').hide();
  }
  else{

      $('label[for="stud_qualification"]').show();
  }

});

  $("#stud_year_of_pass").on('change', function () {
    if(this.val!=''){

      $('label[for="stud_year_of_pass"]').hide();
  }
  else{

      $('label[for="stud_year_of_pass"]').show();
  }
});


  $("#stud_arrears_no").on('change', function () {
    if(this.val!=''){

      $('label[for="stud_arrears_no"]').hide();
  }
  else{

      $('label[for="stud_arrears_no"]').show();
  }
});


  $("#stud_percentage").on('change', function () {
    if(this.val!=''){

      $('label[for="stud_percentage"]').hide();
  }
  else{

      $('label[for="stud_percentage"]').show();
  }
});

  $("#stud_hsst_per").on('change', function () {
    if(this.val!=''){

      $('label[for="stud_hsst_per"]').hide();
  }
  else{

      $('label[for="stud_hsst_per"]').show();
  }
});


  $("#stud_hsst_year").on('change', function () {
    if(this.val!=''){

      $('label[for="stud_hsst_year"]').hide();
  }
  else{

      $('label[for="stud_hsst_year"]').show();
  }
});


  $("#stud_ss_per").on('change', function () {
    if(this.val!=''){

      $('label[for="stud_ss_per"]').hide();
  }
  else{

      $('label[for="stud_ss_per"]').show();
  }
});


  $("#stud_ss_year").on('change', function () {
    if(this.val!=''){

      $('label[for="stud_ss_year"]').hide();
  }
  else{

      $('label[for="stud_ss_year"]').show();
  }
});

  $("#stud_blood").on('change', function () {
    if(this.val!=''){

      $('label[for="stud_blood"]').hide();
  }
  else{

      $('label[for="stud_blood"]').show();
  }
 
  });


  $("#stud_technology").on('change', function () {
    if(this.val!=''){

      $('label[for="stud_technology"]').hide();
  }
  else{

      $('label[for="stud_technology"]').show();
  }
 
  });

  $("#stud_specialization").on('change', function () {

    if(this.val!=''){

      $('label[for="stud_specialization"]').hide();
    }
    else{

        $('label[for="stud_specialization"]').show();
    }
  });

  $("#stud_arrears").on('change', function () {

    var arrear =this.value;
    if(arrear=='Yes'){

      $("#arrear").css("display", "block");

    }
    else{

      $("#arrear").css("display", "none");

    }
   
  });
    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;
    var current = 1;
    var steps = $("fieldset").length;
  
    setProgressBar(current);
    
    $(".next").click(function(){

      current_fs = $(this).parent();
      next_fs = $(this).parent().next();
      var form = $(".msform");
      form.validate({
        rules: {
          stud_name: {
            required: true,
            nameval:true      
          },
          stud_phone: {
            required: true, 
            number:true    
          },
          stud_sec_phone: {
            required: true,
            number:true               
          },
          stud_whatsapp_no: {
            required: true,
            number:true        
          },
          stud_email: {
            required: true,
            email:true      
          },
          stud_dob: {
            required: true,             
          },

          stud_sec_email: {
            required: true,
            email:true      
          },
          stud_blood: {
            required: true,            
          },
          stud_father_name: {
            required: true,
            nameval:true                
          },
          stud_father_occu: {
            required: true,            
          },
          stud_father_no: {
            required: true,
            number:true              
          },
          stud_father_email: {
            required: true,
            email:true           
          },
          stud_mother_name: {
            required: true,
            nameval:true    
                  
          },
          stud_mother_occu: {
            required: true,
          },
          stud_mother_no: {
            required: true,
            number:true  
          },
          stud_mother_email: {
            required: true,
            email:true
          },
          stud_per_add: {
            required: true,       
          },
          stud_temp_add: {
            required: true,       
          },
          stud_city: {
            required: true,       
          },
          stud_town: {
            required: true,       
          },
          stud_photo: {
            required: true,       
          },
          stud_aadh_photo: {
            required: true,       
          },
          stud_qualification: {
            required: true,       
          },

          stud_qualification: {
            required: true,       
          },
          stud_institute: {
            required: true,       
          },
          stud_year_of_pass: {
            required: true,       
          },
          stud_arrears: {
            required: true,       
          },
          stud_arrears_no: {
            required: true,       
          },
          stud_specialization: {
            required: true,       
          },
          stud_percentage: {
            required: true,       
          },
          stud_hsst_name: {
            required: true,       
          },
          stud_hsst_per: {
            required: true,       
          },
          stud_hsst_year: {
            required: true,       
          },
          stud_ss_name: {
            required: true,       
          },
          stud_ss_per: {
            required: true,       
          },
          stud_ss_year: {
            required: true,       
          },
          stud_doj: {
            required: true,       
          },
          stud_doj: {
            required: true,       
          },
          stud_pref_doj: {
            required: true,       
          },
          stud_technology: {
            required: true,       
          },
          stud_poc_code: {
            required: true,       
          },
          stud_ad_on: {
            required: true,       
          },
         
          stud_place_training: {
            required: true,       
          },
        
        },    
      });
  if (form.valid() === true){
          //Add Class Active
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
    
    //show the next fieldset
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
    step: function(now) {
    // for making fielset appear animation
    opacity = 1 - now;
    
    current_fs.css({
    'display': 'none',
    'position': 'relative'
    });
    next_fs.css({'opacity': opacity});
    },
    duration: 500
    });
    setProgressBar(++current);
        
      }


    });
    
    $(".previous").click(function(){
    
    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();
    
    //Remove class active
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
    
    //show the previous fieldset
    previous_fs.show();
    
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
    step: function(now) {
    // for making fielset appear animation
    opacity = 1 - now;
    
    current_fs.css({
    'display': 'none',
    'position': 'relative'
    });
    previous_fs.css({'opacity': opacity});
    },
    duration: 500
    });
    setProgressBar(--current);
    });
    
    function setProgressBar(curStep){
    var percent = parseFloat(100 / steps) * curStep;
    percent = percent.toFixed();
    $(".progress-bar")
    .css("width",percent+"%")
    }
    
    $(".admissionsubmit").click(function(){
      var form = $(".msform");  
      if (form.valid() === true){
        $(".pageloader").show();
     
      $('.msform').removeAttr('onsubmit');
      $('.msform').removeAttr('novalidate');
      form.submit();
      }

    })
    
    });



document.querySelectorAll(".drop-zone__input").forEach((inputElement) => {
  const dropZoneElement = inputElement.closest(".drop-zone");

  dropZoneElement.addEventListener("click", (e) => {
    inputElement.click();
  });

  inputElement.addEventListener("change", (e) => {
    if (inputElement.files.length) {
      updateThumbnail(dropZoneElement, inputElement.files[0]);
    }
  });

  dropZoneElement.addEventListener("dragover", (e) => {
    e.preventDefault();
    dropZoneElement.classList.add("drop-zone--over");
  });

  ["dragleave", "dragend"].forEach((type) => {
    dropZoneElement.addEventListener(type, (e) => {
      dropZoneElement.classList.remove("drop-zone--over");
    });
  });

  dropZoneElement.addEventListener("drop", (e) => {
    e.preventDefault();

    if (e.dataTransfer.files.length) {
      inputElement.files = e.dataTransfer.files;
      updateThumbnail(dropZoneElement, e.dataTransfer.files[0]);
    }

    dropZoneElement.classList.remove("drop-zone--over");
  });
});

/**
 * Updates the thumbnail on a drop zone element.
 *
 * @param {HTMLElement} dropZoneElement
 * @param {File} file
 */
function updateThumbnail(dropZoneElement, file) {
  let thumbnailElement = dropZoneElement.querySelector(".drop-zone__thumb");

  // First time - remove the prompt
  if (dropZoneElement.querySelector(".drop-zone__prompt")) {
    dropZoneElement.querySelector(".drop-zone__prompt").remove();
  }

  // First time - there is no thumbnail element, so lets create it
  if (!thumbnailElement) {
    thumbnailElement = document.createElement("div");
    thumbnailElement.classList.add("drop-zone__thumb");
    dropZoneElement.appendChild(thumbnailElement);
  }

  thumbnailElement.dataset.label = file.name;

  // Show thumbnail for image files
  if (file.type.startsWith("image/")) {
    const reader = new FileReader();

    reader.readAsDataURL(file);
    reader.onload = () => {
      thumbnailElement.style.backgroundImage = `url('${reader.result}')`;
    };
  } else {
    thumbnailElement.style.backgroundImage = null;
  }
}











