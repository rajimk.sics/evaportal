/*!
*created By Raji
*/
window.baseurl = "https://eva.sicsapp.com/";





function changeExamStatus(id, action) {
   
    var actionText = action =='activate' ? "activate" : "deactivate";
    var successText = action == 'activate' ? "Successfully activated" : "Successfully deactivated";
    var url = baseurl +'changeExamStatus';
    var buttonText = action == 'activate' ? 'Yes, activate it!' : 'Yes, deactivate it!';
    var status = (action == 'activate') ? 1 : 2; 
    swal({
        title: `Are you sure you want to ${actionText}?`,
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: buttonText,
        closeOnConfirm: false,
        html: false
    }, function () {
        var data = {
            _token: $('#token_eva').val(),
            id: id,
            status:status
        };
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            success: function (res) {
                swal({
                    title: "",
                    text: successText,
                    type: "success",
                    showCancelButton: false,
                    dangerMode: false,
                    confirmButtonText: 'OK'
                }, function () {
                    if (action === 'activate') {
                        $("#act_" + id).css("display", "block");
                        $("#deact_" + id).css("display", "none");
                        $("#deactb_" + id).css("display", "block");
                        $("#actb_" + id).css("display", "none");


                    } else {
                        $("#act_" + id).css("display", "none");
                        $("#deact_" + id).css("display", "block");
                        $("#deactb_" + id).css("display", "none");
                        $("#actb_" + id).css("display", "block");
                    }
                });
            }
        });
    });
}

//CHANGE Department STATUS
function departmentstatus(id, action) {
   
    var actionText = action === 'activate' ? "activate" : "deactivate";
    var successText = action === 'activate' ? "Successfully activated" : "Successfully deactivated";
    var url = baseurl +'departmentStatus';
    var buttonText = action === 'activate' ? 'Yes, activate it!' : 'Yes, deactivate it!';
    var status = (action === 'activate') ? 1 : 2; // 1 for activate, 2 for deactivate

    swal({
        title: `Are you sure you want to ${actionText}?`,
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: buttonText,
        closeOnConfirm: false,
        html: false
    }, function () {
        var data = {
            _token: $('#token_eva').val(),
            id: id,
            status:status
        
        };
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            success: function (res) {
                swal({
                    title: "",
                    text: successText,
                    type: "success",
                    showCancelButton: false,
                    dangerMode: false,
                    confirmButtonText: 'OK'
                }, function () {
                    if (action === 'activate') {
                        $("#act_" + id).css("display", "block");
                        $("#deact_" + id).css("display", "none");
                        $("#deactb_" + id).css("display", "block");
                        $("#actb_" + id).css("display", "none");


                    } else {
                        $("#act_" + id).css("display", "none");
                        $("#deact_" + id).css("display", "block");
                        $("#deactb_" + id).css("display", "none");
                        $("#actb_" + id).css("display", "block");
                    }
                });
            }
        });
    });
}
    //CHANGE Manage Questions STATUS

    function managequestionstatus(id, action) {
   
        var actionText = action === 'activate' ? "activate" : "deactivate";
        var successText = action === 'activate' ? "Successfully activated" : "Successfully deactivated";
        var url = baseurl +'managequestionstatus';
        var buttonText = action === 'activate' ? 'Yes, activate it!' : 'Yes, deactivate it!';
        var status = (action === 'activate') ? 1 : 2; // 1 for activate, 2 for deactivate
    
        swal({
            title: `Are you sure you want to ${actionText}?`,
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: buttonText,
            closeOnConfirm: false,
            html: false
        }, function () {
            var data = {
                _token: $('#token_eva').val(),
                id: id,
                status:status
            
            };
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                success: function (res) {
                    console.log(res);
                    swal({
                        title: "",
                        text: successText,
                        type: "success",
                        showCancelButton: false,
                        dangerMode: false,
                        confirmButtonText: 'OK'
                    }, function () {
                        if (action == 'activate') {
                            $("#act_" + id).css("display", "block");
                            $("#deact_" + id).css("display", "none");
                            $("#deactb_" + id).css("display", "block");
                            $("#actb_" + id).css("display", "none");
    
    
                        } else {
                            $("#act_" + id).css("display", "none");
                            $("#deact_" + id).css("display", "block");
                            $("#deactb_" + id).css("display", "none");
                            $("#actb_" + id).css("display", "block");
                        }
                    });
                }
            });
        });
    }

//CHANGE Department POC STATUS

function departmentpocstatus(id, action) {
   
    var actionText = action === 'activate' ? "activate" : "deactivate";
    var successText = action === 'activate' ? "Successfully activated" : "Successfully deactivated";
    var url = baseurl +'departmentpocstatus';
    var buttonText = action === 'activate' ? 'Yes, activate it!' : 'Yes, deactivate it!';
    var status = (action === 'activate') ? 1 : 2; // 1 for activate, 2 for deactivate

    swal({
        title: `Are you sure you want to ${actionText}?`,
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: buttonText,
        closeOnConfirm: false,
        html: false
    }, function () {
        var data = {
            _token: $('#token_eva').val(),
            id: id,
            status:status
        
        };
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            success: function (res) {
                swal({
                    title: "",
                    text: successText,
                    type: "success",
                    showCancelButton: false,
                    dangerMode: false,
                    confirmButtonText: 'OK'
                }, function () {
                    if (action === 'activate') {
                        $("#act_" + id).css("display", "block");
                        $("#deact_" + id).css("display", "none");
                        $("#deactb_" + id).css("display", "block");
                        $("#actb_" + id).css("display", "none");


                    } else {
                        $("#act_" + id).css("display", "none");
                        $("#deact_" + id).css("display", "block");
                        $("#deactb_" + id).css("display", "none");
                        $("#actb_" + id).css("display", "block");
                    }
                });
            }
        });
    });


}

function collegeStatus(id, action) {
   
    var actionText = action === 'activate' ? "activate" : "deactivate";
    var successText = action === 'activate' ? "Successfully activated" : "Successfully deactivated";
    var url = baseurl +'collegeStatus';
    var buttonText = action === 'activate' ? 'Yes, activate it!' : 'Yes, deactivate it!';
    var status = (action === 'activate') ? 1 : 2; // 1 for activate, 2 for deactivate

    swal({
        title: `Are you sure you want to ${actionText}?`,
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: buttonText,
        closeOnConfirm: false,
        html: false
    }, function () {
        var data = {
            _token: $('#token_eva').val(),
            id: id,
            status:status
        
        };
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            success: function (res) {
                swal({
                    title: "",
                    text: successText,
                    type: "success",
                    showCancelButton: false,
                    dangerMode: false,
                    confirmButtonText: 'OK'
                }, function () {
                    if (action === 'activate') {
                        $("#act" + id).css("display", "block");
                        $("#deact" + id).css("display", "none");
                        $("#deactb" + id).css("display", "block");
                        $("#actb" + id).css("display", "none");


                    } else {
                        $("#act" + id).css("display", "none");
                        $("#deact" + id).css("display", "block");
                        $("#deactb" + id).css("display", "none");
                        $("#actb" + id).css("display", "block");
                    }
                });
            }
        });
    });
}

function changeTopicStatus(id, action) {
   
    var actionText = action =='activate' ? "activate" : "deactivate";
    var successText = action == 'activate' ? "Successfully activated" : "Successfully deactivated";
    var url = baseurl +'changeTopicStatus';
    var buttonText = action == 'activate' ? 'Yes, activate it!' : 'Yes, deactivate it!';
    var status = (action == 'activate') ? 1 : 2; 
    swal({
        title: `Are you sure you want to ${actionText}?`,
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: buttonText,
        closeOnConfirm: false,
        html: false
    }, function () {
        var data = {
            _token: $('#token_eva').val(),
            id: id,
            status:status
        
        };
      
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            success: function (res) {
                swal({
                    title: "",
                    text: successText,
                    type: "success",
                    showCancelButton: false,
                    dangerMode: false,
                    confirmButtonText: 'OK'
                }, function () {
                    if (action === 'activate') {
                        $("#act_" + id).css("display", "block");
                        $("#deact_" + id).css("display", "none");
                        $("#deactb_" + id).css("display", "block");
                        $("#actb_" + id).css("display", "none");


                    } else {
                        $("#act_" + id).css("display", "none");
                        $("#deact_" + id).css("display", "block");
                        $("#deactb_" + id).css("display", "none");
                        $("#actb_" + id).css("display", "block");
                    }
                });
            }
        });
    });
}