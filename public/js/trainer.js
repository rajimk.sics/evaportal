/*!
*created By Raji
*/
window.baseurl = "https://eva.sicsapp.com/";
function renderSubtopicsByTech(subtopics) {
    
    if (!subtopics || subtopics.length === 0) {
        return `<li>No subtopics available</li>`;
    }

    return subtopics.map(subtopic => {
        const technologies = subtopic.technologies || 'No technologies assigned';
        return `
            <li class="mb-2">
                <div class="card">
                    <div class="card-body">
                        <strong>${subtopic.subtopic_name}</strong>
                        <div class="text-muted">
                            <small>Technologies: ${technologies}</small>
                        </div>
                    </div>
                </div>
            </li>
        `;
    }).join('');
}



// Select/Deselect all topics and subtopics
$(document).on('change', '#selectAll', function () {
    const isChecked = $(this).prop('checked');
    $('#topics-container .topic-checkbox, #topics-container .subtopic-checkbox').prop('checked', isChecked);
});

$(document).on('change', '#topics-container .topic-checkbox', function () {
    const topicId = $(this).val();
    const isChecked = $(this).prop('checked');
    $(`#collapse${topicId} .subtopic-checkbox`).prop('checked', isChecked);

    updateSelectAllState();
});

$(document).on('change', '#topics-container .subtopic-checkbox', function () {
    const topicId = $(this).closest('.accordion-collapse').attr('id').replace('collapse', '');
    const allSubtopics = $(`#collapse${topicId} .subtopic-checkbox`);
    const allChecked = allSubtopics.length == allSubtopics.filter(':checked').length;

    $(`#topicCheckbox${topicId}`).prop('checked', allChecked);

    updateSelectAllState();
});

function updateSelectAllState() {
    const totalCheckboxes = $('#topics-container .topic-checkbox, #topics-container .subtopic-checkbox').length;
    const checkedCheckboxes = $('#topics-container .topic-checkbox:checked, #topics-container .subtopic-checkbox:checked').length;

    $('#selectAll').prop('checked', totalCheckboxes == checkedCheckboxes);
}


function removeTopic(topicid, pacid) {
    swal(
        {
            title: "Are you sure  you want to remove this Topic",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, approve it!",
            closeOnConfirm: false,
            html: false,
        },
        function () {
            var data = {
                _token: $("#token_eva").val(),
                topicid: topicid,
                pacid: pacid,
            };
            $.ajax({
                url: baseurl + "removeTopic",
                type: "POST",
                data: data,
                success: function () {
                    swal(
                        {
                            title: "",
                            text: "Successfully Removed",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: "OK",
                        }, function() {
                            $(".accordion-item[data-topicid='" + topicid + "']").remove();
                            $(".accordion-item").each(function (index) {
                                $(this).find(".accordion-header").attr("id", "heading" + index);
                
                                $(this).find(".accordion-button").attr({
                                    "data-bs-target": "#collapse" + index,
                                    "aria-controls": "collapse" + index,
                                });
                
                                $(this).find(".accordion-collapse").attr("id", "collapse" + index);
                
                                const topicName = $(this).find(".fw-bold.text-dark").text().replace(/Topic \d+:/
, "").trim();
                                $(this).find(".fw-bold.text-dark").text(`Topic ${index + 1}: ${topicName}`);
                            });
                        });
                  
                   
                },
                error: function() {
                    swal({
                        title: "Error!",
                        text: "There was an issue removing the topic.",
                        type: "error",
                        confirmButtonText: "Try Again",
                    });
                }
            });
        }
    );
}





    jQuery.validator.prototype.checkForm = function () {
        this.prepareForm(); 
        for (
            var i = 0, elements = (this.currentElements = this.elements());
            elements[i];
            i++
        ) {
            if (this.findByName(elements[i].name).length > 1) {
                for (
                    var cnt = 0;
                    cnt < this.findByName(elements[i].name).length;
                    cnt++
                ) {
                    this.check(this.findByName(elements[i].name)[cnt]);
                }
            } else {
                this.check(elements[i]); 
            }
        }
        return this.valid(); 
    };


    $(".examtechnology").change(function () {
        var selectedTechnology = $(this).val();

        if (selectedTechnology === "Internship") {
            $("#checkboxField").show();
        } else {
            $("#checkboxField").hide();
        }
    });


    $(document).on("change", "#techtrainerselect", function () {
        var technologyIds = $(this).val();

        if (technologyIds.length > 0) {
            var data = {
                _token: $("#token_eva").val(),
                techtrainerselect: technologyIds,
            };
            $.ajax({
                url: baseurl + "getPackagesByTechnology",
                type: "POST",
                data: data,
                success: function (res) {

                    var obj         = res.data; 
                    var count       = obj.length;    

                $("#trainerpackages").html('');
                if(count!=0){
                                                            
                    var opt=''; 
                    opt+='<option value="" disabled >Select Package</option>';

                    for (i = 0; i < count; i++) {
                        var pac_id      = obj[i].pac_id; 
                        var pac_name    = obj[i].pac_name;   
                        
                         opt+='<option value="'+pac_id+'">'+pac_name+'</option>';
                     }
                     $("#trainerpackages").html(opt);
                 }
                    else{

                        opt+=res.message;
                        $("#trainerpackages").html(opt);

                    }

                    
                }
            });
        }
        else{
            $("#trainerpackages").html('');
        }
    });


    

    $("#examdeclaration").validate({
        rules: {
            examtechnology: {
                required: true,
            },
            examname: {
                required: true,
            },
            "techtrainerselect[]": {
                required: true,
            },
            "packages[]": {
                required: true,
            },
            "percentages": {
                required: true,
            },
        },
        messages: {
            examtechnology: {
                required: "Type Field is Required.",
            },
            percentages: {
                required: "Please select atleast one.",
            },
            examname: {
                required: "Name Field is Required.",
            },
            "techtrainerselect[]": {
                required: "Technology Field is Required.",
            },
            "packages[]": {
                required: "Packages Field is Required.",
            },
        },
        errorPlacement: function (error, element) {
            error.appendTo(element.closest(".col-md-6")).show();
        },
        highlight: function (element) {
            $(element).addClass("error");
            $(element).siblings("label.error").show();
        },
        unhighlight: function (element) {
            $(element).removeClass("error");
            $(element).siblings("label.error").hide();
        },

        submitHandler: function (form) {
            $(".pageloader").show();
            form.submit();
        },
    });

    // $("select, input").on("change", function () {
    //     $(this).valid();
    // });
    var examResults = $("#exam-results").DataTable({
        paging: true,
        pageLength: 10,
        lengthMenu: [5, 10, 25, 50],
        columnDefs: [{ targets: 0, orderable: false, searchable: false }],
    });


    function toggleQuesStatus(id, type,  action) {
        const selectedCheckboxes = $("#questionbox:checked");
        const selectall = $("#selectquestions");
    
        let swalTitle = `Are you sure you want to ${action} this ${type}?`;
        swal(
            {
                title: swalTitle,
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: `Yes, ${action} it!`,
                closeOnConfirm: false,
                html: false,
            },
            function () {
                var data = {
                    _token: $("#token_eva").val(),
                    type: type,
                    action: action,
                    id: id,
                };
    
                $.ajax({
                    url: baseurl + "activateDeactivesingleQues",
                    type: "POST",
                    data: data,
                    success: function (res) {
                        console.log(res);
                        swal(
                            {
                                title: "",
                                text: `Successfully ${action}d`,
                                type: "success",
                                showCancelButton: false,
                                dangerMode: false,
                                confirmButtonText: "OK",
                            },
                            function () {
                               
                                updateButtonDisplay(action, id);
                                selectedCheckboxes.prop("checked", false);
                                selectall.prop("checked", false);
                            }
                        );
                    },
                    error: function (xhr) {
                        swal({
                            title: "Error!",
                            text: xhr.responseJSON.error || "An error occurred.",
                            type: "error",
                            confirmButtonText: "OK",
                        });
                    },
                });
            }
        );
    }
    
    function toggleMultipleQuesStatus(action, type) {
        const selectedCheckboxes = $(".questionbox:checked");
        const selectall = $("#selectquestions");
        const Ids = selectedCheckboxes
            .map(function () {
                return $(this).val();
            })
            .get();
    
        let swalTitle = `Are you sure you want to ${action} these ${type}?`;
        swal(
            {
                title: swalTitle,
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: `Yes, ${action} it!`,
                closeOnConfirm: false,
                html: false,
            },
            function () {
                if (Ids.length > 0) {
                    $.ajax({
                        url: baseurl + "activateDeactivemultipleQues",
                        method: "POST",
                        data: {
                            _token: $("#token_eva").val(),
                            action: action,
                            type: type,
                            Ids: Ids,
                        },
                      
                        success: function (response) {
                            console.log(response);
    
                            swal(
                                {
                                    title: "",
                                    text: `Successfully ${action}d ${type}s`,
                                    icon: "success",
                                    button: "OK",
                                },
                                function () {
                                    Ids.forEach(function (id)
     {
                                        updateButtonDisplay(action, id);
    
                                      
                                        selectedCheckboxes.prop("checked", false);
                                        selectall.prop("checked", false);
                                    });
                                }
                            );
                        },
                        error: function (xhr) {
                            console.error(xhr.responseJSON.errors || xhr.responseJSON.message || "An error occurred.");
                            swal({
                                title: "Error!",
                                text: xhr.responseJSON.errors
                                    ? Object.values(xhr.responseJSON.errors).join("\n")
                                    : "An error occurred.",
                                icon: "error",
                                button: "OK",
                            });
                        },
                    });
                } else {
                    swal("Please select at least one topic.");
                }
            }
        );
    }
    

   
    $(document).on("change", "#techselect", function () {
        var technologyId = $(this).val();

        if (technologyId.length > 0) {
            var data = {
                _token: $("#token_eva").val(),
                techselect: technologyId,
            };

            $.ajax({
                url: baseurl + "getPackage",
                type: "POST",
                data: data,
                success: function (res) {
                    var packageSelect = $("#techpackages");
                    packageSelect.empty();
                    packageSelect.append(
                        '<option value="">Select Package</option>'
                    );

                    if (res.length > 0) {
                        $.each(res, function (index, package) {
                            packageSelect.append(
                                '<option value="' +
                                    package.pac_id +
                                    '">' +
                                    package.pac_name +
                                    "</option>"
                            );
                        });
                    } else {
                        packageSelect.append(
                            '<option value="">No packages found</option>'
                        );
                    }
                },
                error: function (xhr, status, error) {
                    console.log(xhr.responseText);
                    alert(
                        "Failed to retrieve packages. Status: " +
                            status +
                            ", Error: " +
                            error
                    );
                },
            });
        }
    });
    $("#example2").DataTable({
        paging: true, // Enable pagination
        pageLength: 10, // Number of entries per page
        columnDefs: [{ targets: 0, orderable: false, searchable: false }],
    });
    // $(document).ready(function () {

    $("#filter-form").on("submit", function (e) {
        var selectedExamType = $("#examtechnology").val();
        $("#examtype").val(selectedExamType);
        $("#exam_type").val(selectedExamType);

        if (
            !selectedExamType ||
            selectedExamType === "" ||
            selectedExamType === "false"
        ) {
            swal("Please select a valid exam type before submitting.");
            e.preventDefault();
        } else {
            $("#examtype").val(selectedExamType);
        }
    });

    let selectedExams = new Set();

    $(document).ready(function () {
        $("#selectexams").on("change", function () {
            var isChecked = this.checked;
            $(".exam-checkbox").prop("checked", isChecked);
            updateSelectedExams();
        });

        $(document).on("change", ".exam-checkbox", function () {
            updateSelectedExams();
        });

        function updateSelectedExams() {
            $("#exam_ids, #exam_id").empty();

            $(".exam-checkbox").each(function () {
                const examId = $(this).data("exam-id");
                if (this.checked) {
                    selectedExams.add(examId);
                } else {
                    selectedExams.delete(examId);
                }
            });

            selectedExams.forEach((examId) => {
                $("#exam_ids, #exam_id").append(
                    `<input type="hidden" name="exam_ids[]" value="${examId}">`
                );
            });

            $("#selectexams").prop(
                "checked",
                $(".exam-checkbox:checked").length ===
                    $(".exam-checkbox").length
            );

            if (selectedExams.size > 0) {
                console.log("Selected Exam IDs:", Array.from(selectedExams));
            } else {
                console.log("No exams selected.");
            }
        }
    });

    // Handle select all functionality
    //     $('#selectexams').on("change", function () {
    //         var isChecked = this.checked;
    //         $(".exam-checkbox").prop("checked", isChecked);
    //         updateSelectedExams();
    //     });

    //     // Handle individual checkbox selection
    //     $(document).on("change", ".exam-checkbox", function () {
    //         updateSelectedExams();
    //     });

    //     // Update the hidden input fields based on selected exams
    //     function updateSelectedExams() {
    //         var selectedIds = [];
    //         $("#exam_ids").empty();
    //         $("#exam_id").empty();

    //         $(".exam-checkbox:checked").each(function () {
    //             var examId = $(this).data("exam-id");
    //             selectedIds.push(examId);
    //             $("#exam_ids").append(`<input type="hidden" name="exam_ids[]" value="${examId}">`);
    //             $("#exam_id").append(`<input type="hidden" name="exam_ids[]" value="${examId}">`);
    //         });

    //         // Update the "Select All" checkbox state
    //         $('#selectexams').prop("checked", $(".exam-checkbox:checked").length === $(".exam-checkbox").length);

    //         if (selectedIds.length > 0) {
    //             console.log("Selected Exam IDs:", selectedIds);
    //         } else {
    //             console.log("No exams selected.");
    //         }
    //     }
    //  });

    $(document).ready(function () {
        $("#addquestions").validate({
            rules: {
                "question[1]": { required: true },
                "optionA[1]": { required: true },
                "optionB[1]": { required: true },
                "optionC[1]": { required: true },
                "optionD[1]": { required: true },
                "answer[1]": { required: true },
            },
            messages: {
                "question[1]": { required: "Please enter a question." },
                "optionA[1]": { required: "Option A is required." },
                "optionB[1]": { required: "Option B is required." },
                "optionC[1]": { required: "Option C is required." },
                "optionD[1]": { required: "Option D is required." },
                "answer[1]": { required: "Please select the correct answer." },
            },
        });
    });

    $(document).ready(function () {
        function updateQuestionNumbers() {
            $(".question-set").each(function (index) {
                const qn = index + 1;
                $(this).find(".form-label").first().text(`Question ${qn}`);
                $(this).find("textarea").attr("name", `question[${qn}]`);
                $(this)
                    .find("input[name^='optionA']")
                    .attr("name", `optionA[${qn}]`);
                $(this)
                    .find("input[name^='optionB']")
                    .attr("name", `optionB[${qn}]`);
                $(this)
                    .find("input[name^='optionC']")
                    .attr("name", `optionC[${qn}]`);
                $(this)
                    .find("input[name^='optionD']")
                    .attr("name", `optionD[${qn}]`);
                $(this)
                    .find("input[name^='answer']")
                    .attr("name", `answer[${qn}]`);

                // Update validation rules for the current question set
                let validator = $("#addquestions").validate();

                validator.settings.rules[`question[${qn}]`] = {
                    required: true,
                };
                validator.settings.rules[`optionA[${qn}]`] = { required: true };
                validator.settings.rules[`optionB[${qn}]`] = { required: true };
                validator.settings.rules[`optionC[${qn}]`] = { required: true };
                validator.settings.rules[`optionD[${qn}]`] = { required: true };
                validator.settings.rules[`answer[${qn}]`] = { required: true };

                // Update validation messages
                validator.settings.messages[`question[${qn}]`] = {
                    required: "Please enter a question.",
                };
                validator.settings.messages[`optionA[${qn}]`] = {
                    required: "Option A is required.",
                };
                validator.settings.messages[`optionB[${qn}]`] = {
                    required: "Option B is required.",
                };
                validator.settings.messages[`optionC[${qn}]`] = {
                    required: "Option C is required.",
                };
                validator.settings.messages[`optionD[${qn}]`] = {
                    required: "Option D is required.",
                };
                validator.settings.messages[`answer[${qn}]`] = {
                    required: "Please select the correct answer.",
                };
            });
        }

        // Function to add a new question set
        $("#add-question").on("click", function () {
            var qn = $(".question-set").length + 1;
            var newQuestionSet = `
                <div class="question-set">
                    <div class="row mt-2">
                        <div class="mb-3">
                            <label class="form-label">Question ${qn}<sup>*</sup></label>
                            <textarea class="form-control" name="question[${qn}]" rows="4"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-5 col-sm-3">
                            <label class="form-label">Option A<sup>*</sup></label>
                            <input type="text" class="form-control" name="optionA[${qn}]" placeholder="Answer">
                            <input type="radio" name="answer[${qn}]" id="optionA${qn}" value="A">
                        </div>
                        <div class="col-5 col-sm-3">
                            <label class="form-label">Option B<sup>*</sup></label>
                            <input type="text" class="form-control" name="optionB[${qn}]" placeholder="Answer">
                            <input type="radio" name="answer[${qn}]" id="optionB${qn}" value="B">
                        </div>
                        <div class="col-5 col-sm-3">
                            <label class="form-label">Option C<sup>*</sup></label>
                            <input type="text" class="form-control" name="optionC[${qn}]" placeholder="Answer">
                            <input type="radio" name="answer[${qn}]" id="optionC${qn}" value="C">
                        </div>
                        <div class="col-5 col-sm-3">
                            <label class="form-label">Option D<sup>*</sup></label>
                            <input type="text" class="form-control" name="optionD[${qn}]" placeholder="Answer">
                            <input type="radio" name="answer[${qn}]" id="optionD${qn}" value="D">
                        </div>
                    </div>
                    <button type="button" class="btn btn-danger remove-question">Remove Question</button>
                </div>
            `;
            $(".questions-container").append(newQuestionSet);
            updateQuestionNumbers();
        });

        // Remove question button click
        $(document).on("click", ".remove-question", function () {
            $(this).closest(".question-set").remove();
            updateQuestionNumbers();
        });

        // Select all questions checkbox
    });
    $("#searchquestions_form").on("submit", function (event) {
        const examTech = $("#examtech").val();
        if (examTech === "false") {
            event.preventDefault(); // Prevent form submission
            swal({
                title: "Selection Required",
                text: "Please select an exam type before submitting.",
                icon: "warning",
                button: "OK",
            });
        }
    });

    $(document).ready(function () {
        $("#selectquestions").on("change", function () {
            $(".row-checkbox").prop("checked", this.checked);
        });

        $(".row-checkbox").on("change", function () {
            if (
                $(".row-checkbox:checked").length == $(".row-checkbox").length
            ) {
                $("#select-all").prop("checked", true);
            } else {
                $("#select-all").prop("checked", false);
            }
        });

        $("#deactivate-selected").on("click", function () {
            var selectedIds = [];
            $(".row-checkbox:checked").each(function () {
                selectedIds.push($(this).val());
            });

            if (selectedIds.length > 0) {
                deactivateQues(selectedIds);
            } else {
                swal(
                    "No questions selected",
                    "Please select at least one question.",
                    "warning"
                );
            }
        });
        $("#activate-selected").on("click", function () {
            var selectedIds = [];
            $(".row-checkbox:checked").each(function () {
                selectedIds.push($(this).val());
            });

            if (selectedIds.length > 0) {
                activateQues(selectedIds);
            } else {
                swal(
                    "No questions selected",
                    "Please select at least one question.",
                    "warning"
                );
            }
        });
    });

    ////////////////////  sortable jquery for topics

    $(document).ready(function () {
        $("#sortableTopicList").sortable({
            update: function (event, ui) {
                $("#sortableTopicList tr").each(function (index) {
                    $(this)
                        .find(".chapter-number")
                        .text(index + 1); 
                    $(this).attr("data-chapter", index + 1); 
                });
            },
        });

        $("#saveorder").on("click", function () {
            let topicsOrder = [];
            $("#sortableTopicList tr").each(function () {
                topicsOrder.push({
                    id: $(this).data("id"), 
                    chapter_number: $(this).data("chapter"), 
                });
            
            });
            console.log(topicsOrder);

            $.ajax({
                url: baseurl + "updateTopicOrder",

                type: "POST",
                data: {
                    topics: topicsOrder,
                    _token: $("#token_eva").val(),
                },
                success: function (response) {
                    swal(response.message);
                },
                error: function (error) {
                    console.error("Error updating order:", error);
                },
            });
        });
    });




    function toggleStatus(id, type, pac_id, action) {
        let swalTitle = `Are you sure you want to ${action} this ${type}?`;
        swal(
            {
                title: swalTitle,
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: `Yes, ${action} it!`,
                closeOnConfirm: false,
                html: false,
            },
            function () {
                var data = {
                    _token: $("#token_eva").val(),
                    type: type,
                    action: action,
                    id:id
                };
    
                $.ajax({
                    url: baseurl + "activateDeactivesingle" ,
                    type: "POST",
                    data: data,
                    success: function (res) {
                        console.log(res);
                        swal(
                            {
                                title: "",
                                text: `Successfully ${action}d`,
                                type: "success",
                                showCancelButton: false,
                                dangerMode: false,
                                confirmButtonText: "OK",
                            },
                            function () {
                                if (action === 'activate') {
                                    $("#act_" + id).css("display", "block");
                                    $("#actb_" + id).css("display", "none");
    
                                    $("#deact_" + id).css("display", "none");
                                    $("#deactb_" + id).css("display", "block");
                                } else {
                                    $("#act_" + id).css("display", "none");
                                    $("#deact_" + id).css("display", "block");
                                    $("#actb_" + id).css("display", "block");
                                    $("#deactb_" + id).css("display", "none");
                                }
                            }
                        );
                    },
                    error: function (xhr) {
                        swal({
                            title: "Error!",
                            text: xhr.responseJSON.error || "An error occurred.",
                            type: "error",
                            confirmButtonText: "OK",
                        });
                    },
                });
            }
        );
    }

    // Function to toggle multiple statuses
function toggleMultipleStatus(action, type) {
    const selectedCheckboxes = $(".topic-checkbox:checked");
    const selectall = $("#selectalltopics");
    const Ids = selectedCheckboxes
        .map(function () {
            return $(this).val(); 
        })
        .get();

    let swalTitle = `Are you sure you want to ${action} these ${type}s?`;

    if (Ids.length > 0) {

        swal(
            {
                title: swalTitle,
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: `Yes, ${action} it!`,
                closeOnConfirm: false,
                html: false,
            },function(){
               
                    $.ajax({
                        url: baseurl + "activateDeactivemultiple",
                        method: "POST",
                        data: {
                            _token: $("#token_eva").val(), 
                            action: action,
                            type: type,
                            Ids: Ids 
                        },
                        success: function (response) {
                            console.log(response);
            
                            swal({
                                title: "",
                                text: `Successfully ${action}d ${type}s`,
                                icon: "success", 
                                button: "OK",
                            }, function() {
                                Ids.forEach(function(id)
     {
                                    if (action === 'activate') {
                                        $("#act_" + id).css("display", "block");
                                        $("#actb_" + id).css("display", "none");
                            
                                        $("#deact_" + id).css("display", "none");
                                        $("#deactb_" + id).css("display", "block");
                                    } else {
                                        $("#act_" + id).css("display", "none");
                                        $("#deact_" + id).css("display", "block");
                                        $("#actb_" + id).css("display", "block");
                                        $("#deactb_" + id).css("display", "none");
                                    }
                                    selectedCheckboxes.prop('checked', false);
                                    selectall.prop('checked', false);
                                });
                            });
                        },
                        error: function (xhr) {
                            console.error(xhr.responseText);
                            swal({
                                title: "Error!",
                                text: xhr.responseJSON.message || "An error occurred.",
                                icon: "error", 
                                button: "OK",
                            });
                        },
                    });
                
            
    
            });

    }
    else{
        swal("", "Please select at least one topic.", "warning");
        
        
    }
    
   

}
$(document).ready(function () {

        $("#selectalltopics").on("change", function () {
            $(".topic-checkbox").prop("checked", this.checked);
        });

        let preReqCount = 1;
          //function checkPreReqCount() {
            //if ($('input[name="pre_req[]"]').length > 0) {
               // $(".save-changes").show();
            //} else {
                //$(".save-changes").hide(); 
            //}
        //}
        //checkPreReqCount();    
        $(".add-pre-req").click(function () {
            preReqCount++;
            $(".pre-requisite-group").append(`
                <div class="mb-2">
                    <input type="text" class="form-control pre_req_input" name="pre_req[]" id="pre_req${preReqCount}" placeholder="Pre-requisite">
                    <button type="button" class="btn btn-danger remove-requisite">Remove</button>
                </div>
            `);
            //checkPreReqCount();
        });
    
        $(document).on("click", ".remove-requisite", function () {
            $(this).closest(".mb-2").remove();
            preReqCount--;
            //checkPreReqCount();
        });
        $("#editprereq").validate({
            rules: {
                "pre_req[]": {
                    required: true,
                },
            },
            messages: {
                "pre_req[]": {
                    required: "This Field is Required.",
                },
            },
            errorPlacement: function (error, element) {
                error.appendTo(element.closest(".mb-2")).show();
            },
            highlight: function (element) {
                $(element).addClass("error"); 
                $(element).siblings("label.error").show(); 
            },
            unhighlight: function (element) {
                $(element).removeClass("error"); 
                $(element).siblings("label.error").hide(); 
            },
            
        });
      
    
    
      
    });


    function upsyllabus() {
        $('#modalCSV').modal({
            backdrop: 'static',
            keyboard: false
        }).modal('show');

        $("#csv").val("");
    }
    
 


    function deactivateStatus(id, pac_id) {

        let swalTitle = `Are you sure  want to Deactivate?`;

        swal(
            {
                title: swalTitle,
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: `Yes, Deactivate it!`,
                closeOnConfirm: false,
                html: false,
            },
            function () {
                var data = {
                    _token: $("#token_eva").val(),
                    id: id,
                    pac_id: pac_id,
                };
    
                $.ajax({
                    url: baseurl + "topicDeactivate",
                    type: "POST",
                    data: data,
                    success: function (res) {
                        console.log(res);
    
                        swal(
                            {
                                title: "",
                                text: `Successfully Deactivated`,
                                type: "success",
                                showCancelButton: false,
                                dangerMode: false,
                                confirmButtonText: "OK",
                            },
                            function () {
                                window.location =
                                    baseurl + "syllabusview/" + pac_id;
                            }
                        );
                    },
                });
            }
        );
    }
    function activateStatus(id, pac_id) {
        let swalTitle = `Are you sure  want to Activate?`;
        swal(
            {
                title: swalTitle,
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: `Yes, Activate it!`,
                closeOnConfirm: false,
                html: false,
            },
            function () {
                var data = {
                    _token: $("#token_eva").val(),
                    id: id,
                    pac_id: pac_id,
                };
    
                $.ajax({
                    url: baseurl + "topicActivate",
                    type: "POST",
                    data: data,
                    success: function (res) {
                        console.log(res);
    
                        swal(
                            {
                                title: "",
                                text: `Successfully Activated`,
                                type: "success",
                                showCancelButton: false,
                                dangerMode: false,
                                confirmButtonText: "OK",
                            },
                            function () {
                                window.location =
                                    baseurl + "syllabusview/" + pac_id;
                            }
                        );
                    },
                });
            }
        );
    }
    function deletetopic(id, pac_id) {
        let swalTitle = `Are you sure  want to Delete this Topic?`;
        swal(
            {
                title: swalTitle,
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: `Yes, Delete it!`,
                closeOnConfirm: false,
                html: false,
            },
            function () {
                var data = {
                    _token: $("#token_eva").val(),
                    id: id,
                    pac_id: pac_id,
                };
    
                $.ajax({
                    url: baseurl + "topicDelete",
                    type: "POST",
                    data: data,
                    success: function (res) {
                        console.log(res);
    
                        swal(
                            {
                                title: "",
                                text: `Successfully Deleted`,
                                type: "success",
                                showCancelButton: false,
                                dangerMode: false,
                                confirmButtonText: "OK",
                            },
                            function () {
                                window.location =
                                    baseurl + "syllabusview/" + pac_id;
                            }
                        );
                    },
                });
            }
        );
    }


     //// Manage examquestion

let selectedIds = new Set();

$("#select-all").on("change", function () {
    const isChecked = $(this).prop("checked");
    $(".row-select")
        .prop("checked", isChecked)
        .each(function () {
            const id = $(this).val();
            isChecked ? selectedIds.add(id)
 : selectedIds.delete(id)
;
        });
});

$(document).on("change", ".row-select", function () {
    const id = $(this).val();
    $(this).prop("checked") ? selectedIds.add(id)
 : selectedIds.delete(id)
;
    $("#select-all").prop(
        "checked",
        $(".row-select:checked").length === $(".row-select").length
    );
});

$("form").on("submit", function () {
    $("<input>")
        .attr("type", "hidden")
        .attr("name", "question_ids")
        .val(Array.from(selectedIds).join(","))
        .appendTo(this);
});

$(document).on("click", ".pagination a", function (e) {
    e.preventDefault();
    $("#select-all").prop("checked", false);                        
    selectedIds.clear();
});

function loadQuestionData(id)
 {
    let data = {
        _token: $("#token_eva").val(),
    };

    $.ajax({
        url: baseurl + `getQuestion/${id}`,
        type: "POST",
        data: data,
        success: function (data) {
            $("#questionId").val(data.id);
            $("#editQuestionText").val(data.questions);
            $("#editOptionA").val(data.optiona);
            $("#editOptionB").val(data.optionb);
            $("#editOptionC").val(data.optionc);
            $("#editOptionD").val(data.optiond);
            $("#editAnswer").val(data.answer);

            $("#editQuestionModal")
                .modal({
                    backdrop: "static",
                    keyboard: false,
                })
                .modal("show");
        },
        error: function (error) {
            alert("Failed to load question data. Please try again.");
        },
    });
}

$("#editQuestionForm").submit(function (e) {
    e.preventDefault();

    $.post($(this).attr("action"), $(this).serialize())
        .done(function () {
            $("#editQuestionModal").modal("hide");
            location.reload();
        })
        .fail(function () {
            alert("An error occurred. Please try again.");
        });
});

//View assignedquestion
function removeQuestion(examId, questionId, button) {
    swal({
        title: 'Are you sure  you want to remove this Question?',
        text: '',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#dc3545',
        confirmButtonText: 'Yes, remove it!',
        cancelButtonText: 'Cancel'
    }, function() {
        var data = {
            _token: $("#token_eva").val(),
            exam_id: examId,
            question_id: questionId
        };

        $.ajax({
            url: baseurl + "removeExamQuestion",
            type: 'POST',
            data: data,
            success: function(res) {
                swal(
                    {
                        title: "",
                        text: "Successfully Removed",
                        type: "success",
                        showCancelButton: false,
                        dangerMode: false,
                        confirmButtonText: "OK",
                    });

                    if (res.status === 'success') {
                        $(button).closest('tr').remove(); 
                    }
                    let i = 1; 
                    $('table tbody tr').each(function() {
                        $(this).find('.index-column').text(i++);
                    });
            },
            error: function() {
                swal({
                    title: 'Error!',
                    text: 'There was an issue removing the question.',
                    type: 'error',
                    confirmButtonText: 'Try Again'
                });
            }
        });
    });
}



////Batch plan Evaluation result /////////////////

function evaluationModal(student_id){
    $("#evaluationForm")[0].reset();
    $("#evaluationForm").validate().resetForm();
    $("#student_id").val(student_id);
    $("#examsection2").html(""); 
    $("#evaluationModal")
    .modal({
        backdrop: "static", 
        keyboard: false,    
    })
    .modal("show"); 

}

$(document).ready(function () {
    function validateForm() {
        $("#evaluationForm").validate({
            rules: {},
            messages: {},
            submitHandler: function (form) {
                form.submit();
            }
        });

        $(".exam-section").each(function (index) {
            const examNumber = index + 1;

            $("#evaluationForm").validate().settings.rules[`exam_data[${examNumber}][date]`] = {
                required: true
            };
            $("#evaluationForm").validate().settings.rules[`exam_data[${examNumber}][theory]`] = {
                required: true,
                number: true
            };
            $("#evaluationForm").validate().settings.rules[`exam_data[${examNumber}][viva]`] = {
                required: true,
                number: true
            };

            $("#evaluationForm").validate().settings.messages[`exam_data[${examNumber}][date]`] = {
                required: "Please select a date"
            };
            $("#evaluationForm").validate().settings.messages[`exam_data[${examNumber}][theory]`] = {
                required: "Please enter theory marks",
                number: "Please enter a valid number"
            };
            $("#evaluationForm").validate().settings.messages[`exam_data[${examNumber}][viva]`] = {
                required: "Please enter viva marks",
                number: "Please enter a valid number"
            };
        });
    
    }


    validateForm();

    $(document).on("click", "#addExam", function () {
        let examCount = $(".exam-section").length + 1;

        const newExamSection = `
            <div class="row mb-3 exam-section" id="exam_${examCount}">
                <h5 class="col-12 text-center">${examCount}th Exam</h5>
                <input type="hidden" name="exam_data[${examCount}][exam_number]" value="${examCount}">
                <div class="col-4">
                    <label>Date</label>
                    <input type="date" name="exam_data[${examCount}][date]" class="form-control">
                </div>
                <div class="col-4">
                    <label>Theory Mark</label>
                    <input type="number" name="exam_data[${examCount}][theory]" class="form-control" placeholder="Enter theory mark">
                </div>
                <div class="col-4">
                    <label>Viva Mark</label>
                    <input type="number" name="exam_data[${examCount}][viva]" class="form-control" placeholder="Enter viva mark">
                </div>
                <div class="col-12 text-center">
                    <button type="button" class="btn btn-danger remove-exam mt-2">Remove</button>
                </div>
            </div>
        `;
        $("#examsection2").append(newExamSection);
        validateForm();
    });

    $(document).on("click", ".remove-exam", function () {
        $(this).closest(".exam-section").remove();
        $(".exam-section").each(function (index) {
            $(this).find("h5").text(`${index + 1}th Exam`);
            $(this).find("input[name^='exam_data']").each(function () {
                const name = $(this).attr('name');
                const newName = name.replace(/\[\d+\]/, `[${index + 1}]`);
                $(this).attr('name', newName);
            });
        });

        validateForm();
    });
});
//evaluation ends///////

function batchplanModal(student_id, trainer_id, startDate) {
    const selectedTopics = $('.topic-checkbox:checked').length;
    const selectedSubtopics = $('.subtopic-checkbox:checked').length;

    if (selectedTopics == 0 && selectedSubtopics == 0) {
        swal({
            icon: 'warning',
            title: 'Oops...',
            text: 'Please select at least one topic or subtopic!'
        });
        return;
    }

    $("#batchPlanForm")[0].reset();
    $("#batchPlanForm").validate().resetForm(); 
    $("#studId").val(student_id);
    $("#trainer_id").val(trainer_id);

    const selectedTopicIds = $('.subtopic-checkbox:checked').map(function() {
        return $(this).data('topic-id'); 
    }).get();

    const selectedSubtopicIds = $('.subtopic-checkbox:checked').map(function() {
        return $(this).data('subtopic-id'); 
    }).get().join(',');

    $.ajax({
        url: baseurl + 'editBatchPlan', 
        method: 'POST',
        data: {
            _token: $("#token_eva").val(),
            student_id: student_id,
            trainer_id: trainer_id,
            topic_id: selectedTopicIds.join(','), 
            subtopic_id: selectedSubtopicIds 
        },
        success: function(response) {

            if (selectedSubtopicIds.split(',').length > 1) {

                $("#Bp_start_date").val('');
                $("#Bp_end_date").val('');
                $("#comments").val('');
               
            } else if (response.batchplan) {

                $("#studId").val(response.batchplan.student_id);
                $("#trainer_id").val(response.batchplan.trainer_id);
                $("#Bp_start_date").val(response.batchplan.start_date);
                $("#Bp_end_date").val(response.batchplan.end_date);
                $("#comments").val(response.batchplan.comments);
               

            } else {
                $("#Bp_start_date").val('');
                $("#Bp_end_date").val('');
                $("#comments").val('');
            }

           
            $("#batchPlanModal").modal({
                backdrop: "static",
                keyboard: false
            }).modal("show");
        },
      
    });
    $('#Bp_start_date, #Bp_end_date').on('change', function () {
        $(this).valid();  
    });

    $('#Bp_start_date, #Bp_end_date').on('focus', function () {
        $(this).valid();
    });

    $('#batchPlanModal').on('shown.bs.modal', function () {
              
        $('#Bp_start_date').Zebra_DatePicker({
            format: 'd-m-Y',
            direction: [startDate, false],
            pair: $('#Bp_end_date')

            
        });

        $('#Bp_end_date').Zebra_DatePicker({
            format: 'd-m-Y',
            direction: true
            
        });
        $('#Bp_start_date').val(response.batchplan.start_date || startDate);
    });

}

$(document).ready(function () {
    $('button[title="Batchplan"]').on('click', function (e) {
        e.preventDefault();
    });

    $(document).on('change', '.topic-checkbox', function () {
        const topicId = $(this).data('topic-id');
        const isChecked = $(this).prop('checked');
        $(`.subtopic-checkbox[data-topic-id="${topicId}"]`).each(function () {
            if ($(this).data('topic-id') == topicId) {
                $(this).prop('checked', isChecked);
            }
        });
    });
    
    $(document).on('change', '.subtopic-checkbox', function () {
        const topicId = $(this).data('topic-id');
        const allSubtopics = $(`.subtopic-checkbox[data-topic-id="${topicId}"]`);
        const allChecked = allSubtopics.length === allSubtopics.filter(':checked').length;
        $(`#topicCheckbox${topicId}`).prop('checked', allChecked);

    });
    $("#batchPlanForm").validate({
        rules: {
            Bp_start_date: {
                required: true,
            },
            Bp_end_date: {
                required: true,
            },
        },
        messages: {
            Bp_start_date: {
                required: "Please select a start date.",
            },
            Bp_end_date: {
                required: "Please select an end date.",
            },
        },
        
        
        submitHandler: function (form) {
            const selectedTopics = [];
            const selectedSubtopics = [];

            $('.subtopic-checkbox:checked').each(function () {
                const subtopicId = $(this).data('subtopic-id');
                const topicId = $(this).data('topic-id');
                selectedSubtopics.push(subtopicId);
                if (!selectedTopics.includes(topicId)) {
                    selectedTopics.push(topicId);
                }

            });
            $('#topic_ids').val(selectedTopics.length > 0 ? selectedTopics.join(',') : null);
            $('#subtopic_ids').val(selectedSubtopics.length > 0 ? selectedSubtopics.join(',') : null);
            form.submit();
        },
    }); 
       
});



//////////////////////////////////////////////////Batchplan ends




/////////// for topic listing page 


            // Attach the click event to the "Show More" button
            $('#loadMoreTopicsButton').on('click', function () {
                var selectedTechIds = $('#techname').val();
                console.log(selectedTechIds);
                data = {
                    tech_id: selectedTechIds,
                    assigning: 'filter',
                    _token: $('#token_eva').val(),
                };
                loadTopicsAndAppend(data); 
            });
            
            $('#topic_filter').on('submit', function (e) {
                e.preventDefault();
                var selectedTechIds = $('#techname').val();
            if(selectedTechIds && selectedTechIds.length > 0){
                data = {
                    tech_id: selectedTechIds,
                    assigning: 'filter',
                    _token: $('#token_eva').val(),
                };
            
                nextPageUrl = 'getTopics'; 
                $('#topiclistingbytech').empty(); 
                    loadTopicsAndAppend(data); 
            }
            });
            
            // Function to load topics and append
            function loadTopicsAndAppend(data) {
                $('#topicsheading').hide();
                $('#loadMoreTopicsButton').hide();
                let requestUrl = nextPageUrl.startsWith('http') ? nextPageUrl : baseurl + nextPageUrl;
            
                $.ajax({
                    url: requestUrl,
                    method: 'POST',
                    data: data,
                    success: function (response) {
                        if (response && response.data.length > 0) {

                            $('#topicsheading').show();
            
                            // Append topics
                                renderTopicsByTech(response.data);
            
                            nextPageUrl = response.next_page_url;
            
                            // Show or hide "Load More" button
                            if (response.hasMore) {
                                $('#loadMoreTopicsButton').show();
                            } else {
                                $('#loadMoreTopicsButton').hide();
                            }
                        } else {
                            // No topics available
                          
                            swal('No topics available to load.');
                            $('#topicsheading').hide();
                            $('#loadMoreTopicsButton').hide();
                        }
                    },
                    error: function (xhr, status, error) {
                        console.error('Error fetching topics:', error);
                        alert('An error occurred while loading the topics.');
                    }
                });
            }


            function renderTopicsByTech(topics) {
                if (!topics || topics.length === 0) {
                    $('#topiclistingbytech').html('<p style="text-align:center;">No topics available.</p>');
                    return;
                }
            
                let content = '';
            
                topics.forEach((topic, index) => {
                    const status = topic.status == 1 ? 'Active' : 'Deactive';
                    const statusDisplay = topic.status == 1 ? 'text-success' : 'text-danger';
                    const otherStatusDisplay = topic.status == 1 ? 'text-danger' : 'text-success';
                    const otherStatus = topic.status == 1 ? 'Deactive' : 'Active';
            
                    const subtopicsHTML = renderSubtopicsByTech(topic.subtopics);
            
                    content += `
                        <div class="accordion-item">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h2 class="accordion-header">
                                        <button class="accordion-button d-flex justify-content-between align-items-center"
                                                type="button" data-bs-toggle="collapse"
                                                data-bs-target="#collapse${index}"
                                                aria-expanded="${index === 0 ? 'true' : 'false'}"
                                                aria-controls="collapse${index}">
                                            <span>${topic.topics}</span>
                                            <div class="me-3">
                                                <small class="ms-2 text-muted">Technologies: ${topic.all_technologies || 'N/A'}</small>
                                            </div>
                                            <span class="${statusDisplay}" id="act_${topic.id}">${status}</span>
                                            <span class="${otherStatusDisplay}" style="display: none;" id="deact_${topic.id}">${otherStatus}</span>
                                        </button>
                                    </h2>
                                </div>
                                <div class="col-auto d-flex align-items-center gap-2">
                                    <button class="btn btn-light btn-sm upload-csv-btn"
                                            type="button" onclick="subtopicuploads('${topic.id}')">
                                        Upload subtopic CSV
                                    </button>
                                    <button class="btn btn-danger btn-sm text-white me-2"
                                            onclick="changeTopicStatus('${topic.id}', 'deactivate')" id="deactb_${topic.id}">
                                        Deactivate Topic
                                    </button>
                                    <button class="btn btn-success btn-sm text-white me-2"
                                            style="display: none;" onclick="changeTopicStatus('${topic.id}', 'activate')" id="actb_${topic.id}">
                                        Activate Topic
                                    </button>
                                </div>
                            </div>
                            <div id="collapse${index}" class="accordion-collapse collapse ${index === 0 ? 'show' : ''}"
                                 data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <ul class="list-unstyled mt-3">${subtopicsHTML}</ul>
                                </div>
                            </div>
                        </div>
                    `;
                });
            
                $('#topiclistingbytech').append(content);
            }

        let nextPageUrl = 'getTopics';
        let isFetching = false; 
        let data = {
            _token: $("#token_eva").val(),
        };
    
     

        $('#syllabus_filter').on('submit', function (e) {
            e.preventDefault();
            var selectedTechIds = $('#techname').val();
            if(selectedTechIds && selectedTechIds.length > 0){

            data = {
                tech_id: selectedTechIds,
                assigning: 'filter',
                _token: $('#token_eva').val(),
            };
    
            nextPageUrl = 'getTopics'; 
            $('#topics-container').empty();
            fetchTopics(data); }
        });
    





        $('#loadingIndicator').on('click', function () {
            if (isFetching) return; 
            fetchTopics();
        });
    
        function fetchTopics() {
            $('#syllabusassign').hide(); 
            $('.selectAll').hide(); 
            $('#selectAll').hide(); 
            $('#loadingIndicator').hide(); 


            if (!nextPageUrl) return;
    
            $('#loading').show();
    
            let requestUrl = nextPageUrl.startsWith('http') ? nextPageUrl : baseurl + nextPageUrl;
    
            $.ajax({
                url: requestUrl,
                method: 'POST',
                data: data, 
                success: function (response) {
                    nextPageUrl = response.next_page_url; 
                   renderTopics(response.data);
                    $('#loading').hide(); 
                    isFetching = false; 
                    if (response.data.length > 0) {
                        $('#syllabusassign').show();
                        $('.selectAll').show(); 
                        $('#selectAll').show();
                        $('#loadingIndicator').show();
                    } else {
                        $('#syllabusassign').hide(); 
                        $('.selectAll').hide(); 
                        $('#selectAll').hide(); 
                        $('#loadingIndicator').hide(); 
                    }
                    if (!response.hasMore) {
                        $('#loadingIndicator').hide();
                    }
                },
                error: function () {
                    $('#loading').hide();
                    isFetching = false; 
                   
                }
            });
        }
    
        // Function to render topics in the container
        function renderTopics(topics) {
            if (topics.length === 0) {
                $('#topics-container').empty();  
                $('#topics-container').html('<p>No topics available.</p>');
            } else {
                topics.forEach(function (topic) {
                    const accordionItem = `
                        <div class="accordion-item">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h2 class="accordion-header">
                                        <button
                                            class="accordion-button d-flex justify-content-between align-items-center"
                                            type="button" data-bs-toggle="collapse"
                                            data-bs-target="#collapse${topic.id}"
                                            aria-expanded="false"
                                            aria-controls="collapse${topic.id}">
                                            <input type="checkbox"
                                                class="topic-checkbox form-check-input"
                                                id="topicCheckbox${topic.id}"
                                                value="${topic.id}"
                                                name="selected_topics[]"
                                            >
                                            <span class="ms-2">${topic.topics}</span>
                                            <div>
                                                <small class="ms-2 text-muted">Technologies:
                                                    ${topic.all_technologies || 'N/A'}</small>
                                            </div>
                                        </button>
                                    </h2>
                                </div>
                            </div>
                            <div id="collapse${topic.id}"
                                class="accordion-collapse collapse"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <ul class="list-unstyled mt-3">
                                        ${renderSubtopics(topic.subtopics)}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    `;
                    $('#topics-container').append(accordionItem);
                });
            }
        }
                    
        // Function to render subtopics
        function renderSubtopics(subtopics) {
            if (!subtopics || subtopics.length === 0) {
                return `<li>No subtopics available</li>`;
            }
            return subtopics.map(function (subtopic, subIndex) {
                return `
                    <li class="mb-2">
                        <div class="card">
                            <div class="card-body">
                                <input type="checkbox"
                                    class="form-check-input subtopic-checkbox"
                                    id="subtopicCheckbox${subtopic.id}_${subIndex}"
                                    value="${subtopic.subtopic_id}"
                                    name="selected_subtopics[]"
                                >
                                <strong>${subtopic.subtopic_name}</strong>
                                <div class="text-muted">
                                    <small>Technologies: ${subtopic.technologies || 'No technologies assigned'}</small>
                                </div>
                            </div>
                        </div>
                    </li>
                `;
            }).join('');
        }

    




    