/*!
*created By Raji
*/
window.baseurl = "https://eva.sicsapp.com/";
/*
*validation for syllabus upload form - Rohan - 08/11/2024

*/

$("#assignTrainerForm").validate({
    rules: {
        "trainer_id[]": {
            required: true,
        },
        "days[]": {
            required: true,
        },
        start_time: {
            required: true,
        },
        end_time: {
            required: true,
            greaterThan: "#startTime"
        }
    },
    messages: {
        "trainer_id[]": {
            required: "Please select at least one trainer."
        },
        "days[]": {
            required: "Please select at least one day."
        },
        start_time: {
            required: "Please select a start time."
        },
        end_time: {
            required: "Please select an end time.",
            greaterThan: "End time must be after start time."
        }
    },
    errorPlacement: function (error, element) {
        
            error.insertAfter(element);
    },
    submitHandler: function (form) {
        form.submit();
    }
});
$('#assignTrainerForm input, #assignTrainerForm select').on('input change', function () {
    var validator = $("#assignTrainerForm").validate();
    validator.element($(this)); 
});
$("#csvupload").validate({
    rules: {
        "technology[]": {
            required: true,
        },
        csv_file: {
            required: true,
        },
    },
    messages: {
        "technology[]": {
            required: "Please select at least one technology.",
        },
        csv_file: {
            required: "Please upload a CSV file.",
        },
    },
    errorPlacement: function (error, element) {
        error.insertAfter(element.closest(".col-5").find(".form-text").last());
    },
    submitHandler: function (form) {
        form.submit();
    },
});
$('#csvupload input, #csvupload select').on('input change', function () {
    var validator = $("#csvupload").validate();
    validator.element($(this)); 
});


/*
*validation for subtopic upload form - Rohan - 08/11/2024
*/

$("#csvsubupload").validate({
    rules: {
        csv_file: {
            required: true,
        },
    },
    messages: {
        csv_file: {
            required: 'Please upload a file',
        },
    },

    submitHandler: function (form) {
        form.submit();
        $(".pageloader").show();
    },
});
$('#csvsubupload input[name="csv_file"]').on("change", function () {
    $("#csvsubupload").validate().element($(this));
});



$("#syllabus_filter").validate({
    rules: {
        "techname[]": {
            required: true
        }
    },
    messages: {
        "techname[]": {
            required: "Please select at least one technology."
        }
    },
    errorPlacement: function(error, element) {
        if (element.attr("name") === "techname[]") {
            swal({
                type: "error",
                title: " Error",
                text: "Please select at least one technology.",
                button: "OK"
            });
            $('#topics-container').empty();
            $('#selectAll').hide();
            $('.selectAll').hide();
            $('#syllabusassign').hide();
            $('#loadingIndicator').hide();
                } 
    },
});



$("#topic_filter").validate({
    rules: {
        "techname[]": {
            required: true
        }
    },
    messages: {
        "techname[]": {
            required: "Please select at least one technology."
        }
    },
    errorPlacement: function(error, element) {
        if (element.attr("name") === "techname[]") {
            swal({
                type: "error",
                title: " Error",
                text: "Please select at least one technology.",
                button: "OK"
            });
            $('#topiclistingbytech').empty();
            $('#loadMoreTopicsButton').hide();
            $('#topicsheading').hide();
                } 
    },
});




$("[name='techtrainerselect[]'], [name='packages[]']").on("change", function () {
    if ($(this).val() !== null && $(this).val() !== "") {
        $("#examdeclaration").validate().element($(this));
    }
});


$("#examUpload").validate({
    rules: {
        "technology[]": { required: true },
        csv_file: { 
            required: true, 
            extension: "csv" 
        }
    },
    messages: {
        "technology[]": "Please select at least one technology.",
        csv_file: {
            required: "Please upload a CSV file.",
            extension: "Only .csv files are allowed."
        }
    },
    errorPlacement: function(error, element) {
        error.insertAfter(element.closest('.col-6'));
        error.insertAfter(element.closest('.col-6').find('.form-text').last());
    },

    submitHandler: function(form) {
        form.submit();
    }
});
$('#examUpload input[name="csv_file"]').on("change", function () {
    $("#examUpload").validate().element($(this));
});
    ////Manage questions
    $("#uploadQuesfilterform").validate({
        rules: {
            "technology[]": {
                required: true
            }
        },
        messages: {
            "technology[]": {
                required: "Please select at least one technology."
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") === "technology[]") {
                error.remove();
            } else {
                error.insertAfter(element);
            }
        },
        invalidHandler: function(event, validator) {
            if (validator.numberOfInvalids()) {
                swal({
                    type: "error",
                    title: "Error",
                    text: "Please select at least one technology.",
                    button: "OK"
                });
            }
        }
    });
/////////////////////


    $("#assignSyllabusForm").validate({
        rules: {
          
            "selected_topics[]": {
                required: true,
            },
        },
        messages: {
          
            "selected_topics[]": {
                required: "Please select at least one topic.",
            },
        },
        errorPlacement: function () {
        },
        invalidHandler: function (validator) {
            if (validator.numberOfInvalids()) {

                swal({
                    title: "Validation Error",
                    text: "Please select at least one topic",
                    type: "error",
                    button: "OK",
                });
            }
        },
        submitHandler: function (form) {
            form.submit();
        },
    });

    $("#examdeclaration").validate({
        rules: {
            examtechnology: {
                required: true,
            },
            examname: {
                required: true,
            },
            "techtrainerselect[]": {
                required: true,
            },
            "packages[]": {
                required: true,
            },
            percentages: {
                required: function () {
                    return $("#checkboxField").is(":visible");
                },
            },
        },
        messages: {
            examtechnology: "Please select an exam type.",
            examname: "Please enter an exam name.",
            "techtrainerselect[]": "Please select at least one technology.",
            "packages[]": "Please select at least one package.",
            percentages: "Please select at least one percentage.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") === "percentages") {
                error.appendTo("#checkboxField");
            } else if (element.attr("name") === "techtrainerselect[]" || element.attr("name") === "packages[]") {
                element.closest(".col-sm-8").append(error);
            } else {
                error.insertAfter(element);
            }   
        },
            submitHandler: function (form) {
            form.submit();
        },
    });
    $("[name='techtrainerselect[]'], [name='packages[]']").on("change", function () {
        if ($(this).val() !== null && $(this).val() !== "") {
            $("#examdeclaration").validate().element($(this));
        }
    });
// filter validation
$('#exam-question-form').on('submit', function(e) {
    if ($('input[name="question_ids[]"]:checked').length === 0) {
        e.preventDefault(); 
        Swal({
            icon: 'warning',
            title: 'Oops...',
            text: 'Please select at least one question!'
        });
    }
});





/////////////////////

