window.baseurl = "https://eva.sicsapp.com/";
function candidatedetails(id){
 
    var data = {
        _token	:$('#token').val(),
        id     :id			
    }
    $.ajax({
        url: baseurl + 'candidate_details',
        type: "POST",
        data: data,
        success: function (obj) {
            $("#candidatedetails").html('');
           console.log(obj);
            var opt='';

            opt +='<p>Name :<span>'+capitalizeFirstLetter(obj.candidatedetails.name)+'</span></p>';
            if(obj.candidatedetails.contact_sec!=null){
                opt +='<p>Secondary Number :<span>'+obj.candidatedetails.contact_sec+'</span></p>';
            }
            if(obj.candidatedetails.yearof_passout!=null){
                opt +='<p>Year of Passout :<span>'+obj.candidatedetails.yearof_passout+'</span></p>';
            }
          
            if(obj.candidatedetails.address!=null){
                opt +='<p>Address :'+obj.candidatedetails.address+'</p>';
                }
              
            if(obj.candidatedetails.ref_last_organisation!=null){
                opt +='<p>Last Organization :<span>'+obj.candidatedetails.ref_last_organisation+'</span></p>';
            }
            if(obj.candidatedetails.organisation_ref_contact!=null){
                opt +='<p>Reference Contact No :<span>'+obj.candidatedetails.organisation_ref_contact+'</span></p>';
            }
            if(obj.candidatedetails.carrier_break!=null){
                opt +='<p>Career Break :<span>'+obj.candidatedetails.carrier_break+'</span></p>';
            }
           
            if(obj.candidatedetails.curr_organisation!=null){
                opt +='<p>Current Organization:<span>'+obj.candidatedetails.curr_organisation+'</span></p>';
            }
            if(obj.candidatedetails.curr_designation!=null){
                opt +='<p>Current Designation:<span>'+obj.candidatedetails.curr_designation+'</span></p>';   
            }
             
            $("#candidatedetails").append(opt);
        }
    });
    $("#modal-candidate").modal('show');
}

function candidateFollowupHistory(id){

    data = {
        _token	:$('#token').val(),
        id     :id			
    }
    $.ajax({
        url: baseurl + 'candidatefollowup_history',
        type: "POST",
        data: data,
        success: function (obj) {

            $("#candidatehistory").html('');
           
            console.log(obj);

          

            var count=obj.history.length;

          
			if(count!=0){
                var opt='';

				for (i = 0; i < count; i++) {

                    opt+='<p>'+obj.history[i].date+'</p>';
                    opt+='<p>'+obj.history[i].comment+'</p>';
                    opt+='<hr>';

				}
                $("#candidatehistory").append(opt);
               

			}
           
            $("#modal-candidate-history").modal('show');   

        }
    });
}


function followupUpdate(id){
    data = {
    _token	:$('#token').val(),
    id     :id			
}
$.ajax({
    url: baseurl + 'candidate_details',
    type: "POST",
    data: data,
    success: function (res) {
        var dateInYMD = res.candidatedetails.date;
        var dateInDMY = formatDateYMDtoDMY(dateInYMD);
        console.log(dateInDMY);
        $("#editfollowupdate").val(dateInDMY);
        $("#comments_edit").val(res.candidatedetails.comment);
        $("#jobregid").val(id)
;
        $("#modal-candidate-followupdate").modal('show');   

    }
});

}
function candidateFollowup(id){

    $("#jobregid").val(id)
;
    
    $("#modal-candidate-follow").modal('show');

}

function candidateoffer(id,row){

    $("#interviewid").val(id)
;
    $("#rowsid").val(row);
    $("#modal-candidate-offer").modal('show');

}


function candidateInterview(no,id){

    $("#jobreg_id").val(id)
;
   $("#rowid").val(no)
;
    $("#modal-candidate-interview").modal('show');

}

 $("#candidateinterview").validate({
        rules: {
            "scheduledate": {  
                required: true,
            }, 
            "company": {
                required: true,
               
               
            },
            "position": {
                required: true,
             
            },
            "interviewdate": {  
                required: true,
               
            },
                
        },
        errorPlacement: function(error, element) {
         
            error.appendTo(element.parent());     
      },

        submitHandler: function (form,e) {
            e.preventDefault();
            $("#modal-candidate-interview").modal('hide');
            $(".pageloader").show();
          
            var formData = new FormData($("#candidateinterview")[0]);
            console.log(formData);
           var id= $("#rowid").val()
            $(".pageloader").hide();
                 $.ajax({
                   url:baseurl + "saveschedule",
                   type: "POST",
                  
                   data: formData,
                   contentType: false,
                   processData: false,
                   success: function (res) {
                      
                      swal({
                               title: "",
                               text: "Successfully Saved",
                               type: "success",
                               showCancelButton: false,
                               dangerMode: false,
                               confirmButtonText: 'OK'
                           },function () {
       
                               var table = $('#example').DataTable();
                               var row = $('#row_'+id).closest('tr');
                                table.row(row).remove().draw();
                               
                              $("#candidateinterview")[0].reset();
                            })
         
                            }
                        });
                
                    }
                });
                
                $("#candidateoffer").validate({
                    rules: {
                        "interviewstatus": {  
                            required: true,
                        }, 
                        "comments": {
                            required: true,   
                        },            
                    },
                    errorPlacement: function(error, element) {
                     
                        error.appendTo(element.parent());     
                  },
            
                    submitHandler: function (form,e) {
                        e.preventDefault();
                        $("#modal-candidate-offer").modal('hide');
                        $(".pageloader").show();
                      
                        var formData = new FormData($("#candidateoffer")[0]);
                        console.log(formData);
                       var id= $("#rowsid").val()
                        $(".pageloader").hide();
                             $.ajax({
                               url:baseurl + "savecandidateoffer",
                               type: "POST",
                              
                               data: formData,
                               contentType: false,
                               processData: false,
                               success: function (res) {
                                  
                                  swal({
                                           title: "",
                                           text: "Successfully Saved",
                                           type: "success",
                                           showCancelButton: false,
                                           dangerMode: false,
                                           confirmButtonText: 'OK'
                                  }, function () {
                                    
                                  

                                   $("#statustext_" + id).html(res.newStatus);
                                   
                                      $('#offer_'+id).hide();
                                           console.log(id);

                                          $("#candidateoffer")[0].reset();
                                        })
                     
                                        }
                                    });
                            
                                }
                            });
                            
    
    $("#candidatefollowup").validate({
        rules: {
            "followupdate": {  
                required: true,
            }, 
            "comments": {
                required: true,
               
               
            },
            
                
        },
        errorPlacement: function(error, element) {
         
            error.appendTo(element.parent());     
      },

        submitHandler: function (form) {
            $(".pageloader").show();
            form.submit();
        }
    });

    
    $("#edit_followupdate").validate({
        rules: {
            "editfollowupdate": {  
                required: true,
            }, 
            "comments_edit": {
                required: true,
               
               
            },
            
                
        },
        errorPlacement: function(error, element) {
         
            error.appendTo(element.parent());     
      },

        submitHandler: function (form) {
            $(".pageloader").show();
            form.submit();
        }
    });

    $("#bulk_candidate").validate({
        rules: {
            "candidate_file": {
                required: true
                       
            }                
        },
        submitHandler: function (form) { 
            $(".pageloader").show();
            form.submit();
        }
    });


    $("#bulk_candidate").validate({
        rules: {
            "candidate_file": {
                required: true
                       
            }                
        },
        submitHandler: function (form) { 
            $(".pageloader").show();
            form.submit();
        }
    });
    
    $('#editfollowupdate').Zebra_DatePicker({
            format: 'd-m-Y',
            direction:1
        });
        $('#followupdate').Zebra_DatePicker({
            format: 'd-m-Y',
            direction:1
        });
        $('#scheduledate').Zebra_DatePicker({
            format: 'd-m-Y',
            direction:1
        });
        
        $('#interviewdate').Zebra_DatePicker({
            format: 'd-m-Y',
            direction:1
        });
        