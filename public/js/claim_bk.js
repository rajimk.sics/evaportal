window.baseurl = "https://eva.sicsapp.com/";

var ajaxvar = "";
$.validator.addMethod(
    "alphabetOnly",
    function (value, element) {
        return this.optional(element) || /^[A-Za-z\s]+$/.test(value);
    },
    "Please enter only Alphabets."
);

/*$(document).ready(function () {

  $('#college').on('change', function () {

      const collegeId = $(this).val();
      if (collegeId) {
          fetchPackCollegeDepartments(collegeId);
      } else {
          $('#department').empty().append('<option value="">Select Department</option>');
          $('#pocId').empty();
      }
  });
});
function fetchPackCollegeDepartments(collegeId) {
  $.ajax({
      url: baseurl + 'getcollege-departments',
      type: 'POST',
      data: {
          _token: $('#token_eva').val(),
          id: collegeId,
          isUpdate: true
      },
      dataType: 'json',
      success: function (data) {

          $('#department').empty().append('<option value="">Select Department</option>');
          $.each(data.departments, function (key, department) {
              $('#department').append('<option value="' + department.id + '">' + department.department + '</option>');
          });

          $('#pocId').empty();
          $.each(data.pocs, function (key, pocDetails) {
              $('#pocId').append('<option value="' + pocDetails.id + '">' + pocDetails.department_poc + '</option>');
          });

          if (data.selectedDep.length === 1) {
              $('#department').val(data.selectedDep[0]).trigger('change');
              fetchAndPopulatePOCs(data.selectedDep[0], data.dep_pocs, data.pocs);
          }

          $('#department').on('change', function () {
              const selectedDeptId = $(this).val();
              fetchAndPopulatePOCs(selectedDeptId, data.dep_pocs, data.pocs);
          });
      },
      error: function (xhr, status, error) {
          console.error('Error fetching departments and POCs:', error);
      }
  });
}*/

function claimAllocat(id, colId, packId) {
    $("#dynamic-sections").html("");
    $(".pageloader").show();
    var data = {
        _token: $("#token_eva").val(),
        colId: colId,
        claimid: id,
        packaId: packId,
    };

    ajaxvar = "";
    $.ajax({
        url: baseurl + "claim_names",
        type: "POST",
        data: data,
        success: function (data) {
            $(".pageloader").hide();
            $("#claimAllocate")
                .modal({
                    backdrop: "static",
                    keyboard: false,
                })
                .modal("show");

            $("#collegeId").val(colId);
            $("#packageId").val(packId);
            $("#claimId").val(id);
            console.log("claimid", id);

            console.log(data.claimNames);
            var opt = "";
            opt += '<option value="" >Select</option>';
            $.each(data.claimNames, function (key, claimnames) {
                opt +=
                    '<option value="' +
                    claimnames.userid +
                    '">' +
                    claimnames.name +
                    "</option>";
            });
            console.log(opt);

            ajaxvar = opt;
            $("#salesperson").html(opt);
            // console.log(data.claimNames);
        },

        error: function (xhr, status, error) {
            $(".pageloader").hide();
            console.error("Error adding students:", error);
            console.log("An error occurred. Please try again.");
        },
    });
}
$(document).ready(function () {
    let sectionCount = 1;

    $(document).on("click", "#addPercentage", function () {
        const newSection =
            `
            <div class="section">
                <div class="row align-items-center">
                    <div class="col-lg-5 sub">
                        <div class="form-group">
                        <label for="salesperson_${sectionCount}">Sales Person</label>
                            <select class="form-select" id="salesperson_${sectionCount}" name="salesperson[]">` +
            ajaxvar +
            `</select>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="form-group">
                        <label for="perallocate_${sectionCount}">% of Allocation<sup>*</sup></label>
                            <input type="text" class="form-control" id="perallocate_${sectionCount}" name="perallocate[]" placeholder="% of Allocation" min="1">
                        </div>
                    </div>
                    <div class="col-lg-2 text-center">
                        <button type="button" class="btn btn-danger remove-section">Remove</button>
                    </div>
                </div>
            </div>
        `;

        $("#dynamic-sections").append(newSection);

        sectionCount++;
    });

    $(document).on("click", ".remove-section", function () {
        $(this).closest(".section").remove();
    });
});

$(document).ready(function () {
    $("#claimAllocate").on("hidden.bs.modal", function () {
        $("#claimallocateForm")[0].reset();
    });
});

$("#claimallocateForm").validate({
    rules: {
        "salesperson[]": {
            required: true,
        },
        "perallocate[]": {
            required: true,
        },
    },
    messages: {
        "salesperson[]": {
            required: "Please select a salesperson.",
        },
        "perallocate[]": {
            required: "Please enter the percentage of allocation.",
        },
    },
    errorPlacement: function (error, element) {
        error.addClass("text-danger");
        error.insertAfter(element);
    },
    highlight: function (element) {
        $(element).addClass("is-invalid");
    },
    unhighlight: function (element) {
        $(element).removeClass("is-invalid");
    },
    submitHandler: function (form) {
        var salespersonValues = $("select[name='salesperson[]']")
            .map(function () {
                return $(this).val();
            })
            .get();

        var duplicateFound = salespersonValues.some(function (val, index) {
            return salespersonValues.indexOf(val) != index;
        });

        if (duplicateFound) {
            swal(
                "Duplicate salesperson selection is not allowed.",
                "",
                "warning"
            );
            return false;
        } else {
            form.submit();
        }
    },
});

// $(document).ready(function () {

//   let addDocCount = 1;

//   $('#addPercentage').click(function () {

//       addDocCount++;
//       $('#text-box-container').append(`<div class="doc-group" >
//             <div class="mb-3">
//                  <input type="text" class="form-control" id="perallocate${addDocCount}" name="perallocate[]" placeholder="% of Allocation" min=1>

//                 <button type="button" class="btn btn-danger remove-percentage">Remove</button>
//                   </div>
//                 </div>`);
//   });
// });
$(document).on("click", ".remove-percentage", function () {
    $(this).closest(".doc-group").remove();
});

function fetchStudents(collegeid, year, salesid) {
    var data = {
        _token: $("#token_eva").val(),
        collegeid: collegeid,
        year: year,
        salesid: salesid,
    };
    $.ajax({
        url: baseurl + "master-studentdetails",
        data: data,
        method: "post",
        success: function (data) {
            if (data.error) {
                alert(data.error);
                return;
            }
            console.log(data);
            $("#studentsModalLabel").text("Student Details");
            // $('#tableHeading').text(`${data.college_name} - ${data.department_name}`);
            $("#studentDetailsTable tbody").empty();

            // Populate the table with claim details
            let i = 1;
            $.each(data.claim_details, function (index, detail) {
                const joiningDate = new Date(detail.joining_date)
                    .toLocaleDateString("en-GB")
                    .replace(/\//g, "-");
                $("#studentDetailsTable tbody").append(`
                  <tr>
                      <td>${i}</td>
                      <td>${detail.name}</td>
                       <td>${detail.pac_name}</td>
                        <td>${detail.technology}</td>
                      <td>${detail.email}</td>
                      <td>${detail.phone}</td>
                       <td>${joiningDate}</td>
                      
                      <td>${Number(detail.package_fullamount).toFixed(2)}</td>
                  </tr>
              `);
                i++;
            });

            // Show the modal
            $("#studentsModal").modal("show");
        },
        error: function (xhr, status, error) {
            $("#modalContent")
                .empty()
                .append("<p>No claim details available.</p>");
            $("#studentsModal")
                .modal({
                    backdrop: "static",
                    keyboard: false,
                })
                .modal("show");
        },
    });
}

function fetchAllocationClaim(id) {
    var data = {
        _token: $("#token_eva").val(),
        claimid: id,
    };
    $.ajax({
        url: baseurl + "claimdetails-allocation",
        data: data,
        method: "post",
        success: function (data) {
            if (data.error) {
                alert(data.error);
                return;
            }
            console.log(data);
            $("#claimDetailsModalLabel").text("Claim Details");
            // $('#tableHeading').text(`${data.college_name} - ${data.department_name}`);
            $("#claimDetailsTable tbody").empty();

            // Populate the table with claim details
            let i = 1;
            $.each(data.claim_details, function (index, detail) {
                const joiningDate = new Date(detail.joining_date)
                    .toLocaleDateString("en-GB")
                    .replace(/\//g, "-");
                $("#claimDetailsTable tbody").append(`
                    <tr>
                        <td>${i}</td>
                        <td>${detail.name}</td>
                        <td>${detail.email}</td>
                        <td>${detail.phone}</td>
                         <td>${joiningDate}</td>
                        
                        <td>${Number(detail.package_fullamount).toFixed(2)}</td>
                    </tr>
                `);
                i++;
            });

            // Show the modal
            $("#AllocationclaimModal").modal("show");
        },
        error: function (xhr, status, error) {
            $("#modalContent")
                .empty()
                .append("<p>No claim details available.</p>");
            $("#AllocationclaimModal")
                .modal({
                    backdrop: "static",
                    keyboard: false,
                })
                .modal("show");
        },
    });
}

function appealReq(id) {
    swal(
        {
            title: "",
            text: "Please provide a reason for your action:",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            animation: "slide-from-top",
            inputPlaceholder: "Enter reason here",
            confirmButtonText: "Appeal",
            cancelButtonText: "Cancel",
        },
        function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("You need to enter a reason!");
                return false;
            }

            $(".pageloader").show();

            var data = {
                id: id,
                _token: $("#token_eva").val(),
                inputValue: inputValue,
                reqid: id,
            };
            $.ajax({
                url: baseurl + "save-appeal",
                type: "POST",
                data: data,
                success: function (res) {
                    $(".pageloader").hide();
                    swal(
                        {
                            title: "",
                            text: "Successfully Appealed",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: "OK",
                        },
                        function () {
                            $("#appeal_" + id).hide();
                            $("#appeal" + id)
                                .html("Appealed<br>Reason: " + inputValue)
                                .css("color", "blue")
                                .show();
                            $("#appeal-btn-" + id).prop("disabled", true);
                        }
                    );
                },
            });
        }
    );
}

$(document).on("change", ".collegestate", function () {
    var state_id = $(this).val();

    $(".collegedist")
        .empty()
        .append('<option value="">Select District</option>');

    if (state_id) {
        $.ajax({
            url: baseurl + "get-districts",
            type: "POST",
            data: {
                _token: $("#_token").val(),
                state_id: state_id,
            },
            success: function (response) {
                console.log(response);
                var opt = "";
                opt += '<option value="">Select District</option>';
                $.each(response.districts, function (key, district) {
                    opt +=
                        '<option value="' +
                        district.id +
                        '">' +
                        district.name +
                        "</option>";
                });

                $(".collegedist").html(opt);
            },
            error: function (xhr, status, error) {
                console.error("Error fetching districts:", error);
            },
        });
    }
});

function collegeDep(collegeId) {
    if (collegeId) {
        var data = {
            _token: $("#token_eva").val(),
            id: collegeId,
            isUpdate: "false",
        };
    }
    $.ajax({
        url: baseurl + `getcollege-departments`,
        method: "POST",
        data: data,
        dataType: "json",
        success: function (response) {
            let departmentList = response.departments;
            let departmentHTML = "";
            departmentHTML += `<b>Department:</b><br>`;
            if (departmentList.length > 0) {
                departmentHTML += "<ul>";
                departmentList.forEach(function (department) {
                    departmentHTML += `<li> ${department.department} </li>`;
                });
                departmentHTML += "</ul>";
            } else {
                departmentHTML =
                    "<p>No departments found for this college.</p>";
            }

            $("#departmentDetailsModal .modal-body").html(departmentHTML);
            $("#departmentDetailsModal").modal("show");
        },
        error: function (xhr, status, error) {
            console.error("Error fetching departments:", error);
            alert("Unable to fetch department details at this time.");
        },
    });
}

$(document).ready(function () {
    let addDocCount = 1;

    $("#addDoc").click(function () {
        addDocCount++;
        $("#text-box-container").append(`<div class="doc-group" >
              <div class="mb-3">
                   <input type="file" class="form-control" id="documents_${addDocCount}" name="documents[]" accept=".pdf, .jpg, .jpeg, .png">
                 
                   
                  <button type="button" class="btn btn-danger remove-doc">Remove</button>
                    </div>
                  </div>`);
    });
});
$(document).on("click", ".remove-doc", function () {
    $(this).closest(".doc-group").remove();
});

function getCollegeUpdateHistory(collegeId) {
    $.ajax({
        url: baseurl + "college-update-history",
        data: {
            _token: $("#token_eva").val(),
            collegeid: collegeId,
        },
        method: "POST",
        success: function (response) {
            console.log(response);

            $("#collegeName").text(response.college);

            let updatesHtml = "";
            if (response.updates.length > 0) {
                $.each(response.updates, function (index, update) {
                    const formDate = new Date(update.date)
                        .toLocaleDateString("en-GB")
                        .replace(/\//g, "-");
                    const toDate = new Date(update.end_date)
                        .toLocaleDateString("en-GB")
                        .replace(/\//g, "-");
                    updatesHtml += `
                <div class="update-section">
                    <h4>${update.department_name}</h4>
                      ${
                          update.end_date != null
                              ? `<h5 style="color: red;">Approved request submitted to Master from ` +
                                formDate +
                                ` to ` +
                                toDate +
                                `</h5>`
                              : ""
                      }
                   
                    <p><strong>Sales Person:</strong> ${update.sales_person}</p>
                     <p><strong>POC:</strong> ${update.poc_names}</p>
                    <p><strong>Date:</strong> ${formDate}</p>
                    <p><strong>Comment(Visible to everyone):</strong> ${
                        update.comment
                    }</p>
                   
               
            `;
                    if (update.updatestatus != null) {
                        updatesHtml += `
                        <p><strong>Update Status (Visible only to you , <span style="color: magenta;">
                          ${
                              update.sales_id_person != null
                                  ? update.sales_id_person + " ,"
                                  : ""
                          }
                           ${
                               update.reporting_persons != null
                                   ? update.reporting_persons 
                                   : ""
                           }
                        
                        
                      </span>  ):
                       
                        </strong> ${update.updatestatus}</p>`;
                    }
                    updatesHtml += `<hr></div>`;
                });
            } else {
                updatesHtml = "<p>No updates available for this college.</p>";
            }

            $("#collegeUpdates").html(updatesHtml);

            $("#collegeHistoryModal")
                .modal({
                    backdrop: "static",
                    keyboard: false,
                })
                .modal("show");
        },
        error: function () {
            alert("Failed to load update history. Please try again.");
        },
    });
}

$("#modalCollegeUpdates").on("shown.bs.modal", function () {
    $("#collegeups").validate();
});
$(document).ready(function () {
    $("#modalCollegeUpdates").on("hidden.bs.modal", function () {
        $("#collegeups")[0].reset();
        $("#collegepoc").val(null).trigger("change");
    });
});

function collegeUpdates(collegeIdOrButton) {
    var departmentId = null;
    var pocId = null;
    var collegeId;
    hasSelected = true;
    let flag = 0;
    if (typeof collegeIdOrButton == "object") {
        var button = collegeIdOrButton;

        departmentId = button.getAttribute("data-dept");
        pocId = button.getAttribute("data-poc");
        collegeId = button.getAttribute("data-collegeid");
        var types = false;
    } else {
        collegeId = collegeIdOrButton;
        var types = true;
    }

    if (collegeId) {
        var data = {
            _token: $("#token_eva").val(),
            id: collegeId,
            depId: departmentId,
            isUpdate: true,
        };
        $.ajax({
            url: baseurl + "getcollege-departments",
            type: "POST",
            data: data,
            dataType: "json",
            success: function (data) {
                $("#comments").val("");
                $("#updatestatus").val("");
                let collegeName = data.college.colname || "College";
                let districtName = data.college.district_name || "District";
                $("#modalCollegeUpdatesLabel").text(
                    `Updates for ${collegeName} - ${districtName}`
                );

                $("#collegedep").empty();

                console.log(data);

                if (
                    data.departments.length > 0 &&
                    (data.isActive.length == 0 || data.isActive == 1)
                ) {
                    $.each(data.departments, function (key, department) {
                        var selected =
                            department.id == departmentId ? "selected" : "";
                        var hidden =
                            !types && selected !== "selected"
                                ? "style='display:none;'"
                                : "";
                        if (selected == "selected") {
                            flag = 1;
                        }

                        $("#collegedep").append(
                            '<option value="' +
                                department.id +
                                '" ' +
                                selected +
                                " " +
                                hidden +
                                ">" +
                                department.department +
                                "</option>"
                        );
                    });
                }

                if (types == false && flag == 0) {
                    hasSelected = false;
                }
                if (
                    hasSelected &&
                    data.departments.length > 0 &&
                    (data.isActive.length == 0 || data.isActive == 1)
                ) {
                    $("#collegepoc").empty();

                    var pocIdArray =
                        pocId != null
                            ? pocId.split(",").map(function (id) {
                                  return id.trim();
                              })
                            : [];

                    $.each(data.pocs, function (key, poc) {
                        var selected = pocIdArray.includes(String(poc.id))
                            ? "selected"
                            : "";

                        $("#collegepoc").append(
                            '<option value="' +
                                poc.id +
                                '" ' +
                                selected +
                                ">" +
                                poc.department_poc +
                                "</option>"
                        );
                    });
                    $("#modalCollegeUpdates").modal("show");
                    $("#collegeid").val(collegeId);
                    $("#updatetype").val(types);
                } else {
                    $("#modalCollegeUpdates").modal("hide");
                    let modalContent;

                    if (data.isActive == 2 && data.isActive.length > 0) {
                        modalContent = ` <p>This departments is deactivated by Admin.</p>
                         <button id="okButton" class="btn btn-primary">OK</button>
         `;
                    } else {
                        modalContent =
                            data.salesPeople && data.salesPeople.length > 0
                                ? `
                  <p>There are no new departments added in this college. The already added department is being contacted by the sales person(s): ${data.salesPeople
                      .map(
                          (person) =>
                              `<span style="color: magenta;">${person.name}</span>`
                      )
                      .join(
                          ", "
                      )}. For more details, please check the updated history.</p>
                  <button id="okButton" class="btn btn-primary">OK</button>
                 <button id="updateHistoryButton" class="btn btn-secondary" data-bs-toggle="modal">Updated History</button>`
                                : `
                 <p>There are no new departments added in this college.</p>
                  <button id="okButton" class="btn btn-primary">OK</button>
  `;
                    }

                    $("#messageModal .modal-body").html(modalContent);
                    $("#messageModal").modal("show");

                    $("#okButton").on("click", function () {
                        $("#messageModal").modal("hide");
                    });

                    $("#updateHistoryButton").on("click", function () {
                        $("#messageModal").modal("hide");

                        getCollegeUpdateHistory(collegeId);
                    });
                }
            },
            error: function (xhr, status, error) {
                console.error("Error fetching departments:", error);
            },
        });
    } else {
        $("#department").empty();
        $("#department").append('<option value="">Select Department</option>'); // Reset to placeholder if no college selected
    }
}

function reqtoMaster() {
    $("#requestForm")[0].reset();
    $("#requestForm").validate().resetForm();
    $("#requestToMasterModal").modal("show");
    $("#from_date").val(edate);
    $.ajax({
        url: baseurl + "getrequest-details",
        type: "POST",
        data: {
            _token: $("#token_eva").val(),
        },
        dataType: "json",
        success: function (data) {
            $("#collegeId").empty();
            $("#department")
                .empty()
                .append('<option value="" selected>Select Department</option>');
            $("#pocId").empty().append('<option value="">Select POC</option>');
            $("#collegeId").append('<option value="">Select College</option>');

            $.each(data.college, function (key, college) {
                if (college.district_name) {
                    $("#collegeId").append(
                        '<option value="' +
                            college.id +
                            '">' +
                            college.colname +
                            ", " +
                            college.district_name +
                            "</option>"
                    );
                } else {
                    $("#collegeId").append(
                        '<option value="' +
                            college.id +
                            '">' +
                            college.colname +
                            "</option>"
                    );
                }
            });

            $("#collegeId").on("change", function () {
                const collegeId = $(this).val();
                if (collegeId) {
                    fetchCollegeDepartments(collegeId);
                } else {
                    $("#department")
                        .empty()
                        .append('<option value="">Select Department</option>');
                    $("#pocId").empty();
                }
            });
        },
        error: function (xhr, status, error) {
            console.error("Error fetching departments and POCs:", error);
        },
    });
}

// $("#collegeIds").on("change", function () {
//     const collegeId = $(this).val();
//     if (collegeId) {
//         fetchCollegeDepartments(collegeId);
//     } else {
//         $("#department")
//             .empty()
//             .append('<option value="">Select Department</option>');
//         $("#pocId").empty();
//     }
// });
function fetchCollegeDepartments(collegeId) {
    $.ajax({
        url: baseurl + "getCollegeDepartmentsForReqForm",
        type: "POST",
        data: {
            _token: $("#token_eva").val(),
            id: collegeId,
        },
        dataType: "json",
        success: function (data) {
            console.log(data.departments);
            $("#department")
                .empty()
                .append('<option value="">Select Department</option>');
            $.each(data.departments, function (key, department) {
                $("#department").append(
                    '<option value="' +
                        department.id +
                        '">' +
                        department.department +
                        "</option>"
                );
            });

            $("#pocId").empty();
            $.each(data.pocs, function (key, pocDetails) {
                $("#pocId").append(
                    '<option value="' +
                        pocDetails.id +
                        '">' +
                        pocDetails.department_poc +
                        "</option>"
                );
            });

            // if (data.selectedDep.length === 1) {
            //     $('#department').val(data.selectedDep[0]).trigger('change');
            //     fetchAndPopulatePOCs(data.selectedDep[0], data.dep_pocs, data.pocs);
            // }

            $("#department").on("change", function () {
                const selectedDeptId = $(this).val();
                fetchAndPopulatePOCs(selectedDeptId, data.dep_pocs, data.pocs);
            });
        },
        error: function (xhr, status, error) {
            console.error("Error fetching departments and POCs:", error);
        },
    });
}

// function fetchCollegeDepartments(collegeId) {
//     $.ajax({
//         url: baseurl + "getCollegeDepartmentsForReqForm",
//         // url: baseurl + "getcollege-departments",
//         type: "POST",
//         data: {
//             _token: $("#token_eva").val(),
//             id: collegeId,
//             // isUpdate: false,
//         },
//         dataType: "json",
//         success: function (data) {
//             console.log(data);
//             // $("#department")
//             //     .empty()
//             //     .append('<option value="" selected >Select Department</option>');
//             // $.each(data.departments, function (key, department) {
//             //     $("#department").append(
//             //         '<option value="' +
//             //             department.id +
//             //             '">' +
//             //             department.department +
//             //             "</option>"
//             //     );
//             // });
//             $("#department")
//                 .empty()
//                 .append('<option value="" selected>Select Department</option>');

//             for (let i = 0; i < data.departments.length; i++) {
//                 const department = data.departments[i];
//                 $("#department").append(
//                     '<option value="' +
//                         department.id +
//                         '">' +
//                         department.department +
//                         "</option>"
//                 );
//             }

//             $("#pocId").empty();
//             $.each(data.pocs, function (key, pocDetails) {
//                 $("#pocId").append(
//                     '<option value="' +
//                         pocDetails.id +
//                         '">' +
//                         pocDetails.department_poc +
//                         "</option>"
//                 );
//             });

//             if (data.selectedDep.length === 1) {
//                 $("#department").val(data.selectedDep[0]).trigger("change");
//                 fetchAndPopulatePOCs(
//                     data.selectedDep[0],
//                     data.dep_pocs,
//                     data.pocs
//                 );
//             }

//             $("#department").on("change", function () {
//                 const selectedDeptId = $(this).val();
//                 fetchAndPopulatePOCs(selectedDeptId, data.dep_pocs, data.pocs);
//             });
//         },
//         error: function (xhr, status, error) {
//             console.error("Error fetching departments and POCs:", error);
//         },
//     });
// }

function fetchAndPopulatePOCs(departmentId, dep_pocs, pocs) {
    const relatedPocs = dep_pocs.filter(
        (poc) => poc.department == parseInt(departmentId)
    );
    $("#pocId").empty();

    $.each(pocs, function (index, poc) {
        // const isSelected = relatedPocs.some(
        //     (related) => related.poc_id === poc.id
        // );
        $("#pocId").append(
            '<option value="' +
                poc.id +
                '"' +
                ">" +
                poc.department_poc +
                "</option>"
        );
    });

    if (relatedPocs.length > 0) {
        const selectedPocIds = relatedPocs.map((poc) => poc.poc_id);
        $("#reqpoc").val(selectedPocIds).trigger("change");
    }
}

function updateReports(button) {
    var updateId = button.getAttribute("data-updateid");

    var data = {
        _token: $("#token_eva").val(),
    };

    $.ajax({
        url: baseurl + "updateDetails/" + updateId,
        type: "POST",
        data: data,
        success: function (response) {
            const modalContent = $("#modalContent");
            modalContent.empty();

            if (response.collegeupdates && response.collegeupdates.length > 0) {
                const {
                    college,
                    department,
                    department_poc: poc,
                } = response.collegeupdates[0];

                modalContent.append(
                    $("<p>")
                        .css("font-weight", "bold")
                        .text(`${college} - ${department}`)
                );

                response.collegeupdates.forEach((item) => {
                    const text =
                        item.end_date == null
                            ? `Updated at: ${item.date} - ${item.poc_names}`
                            : `Requested from: ${item.date} to:- ${item.end_date} - ${item.poc_names}`;

                    const color = item.end_date == null ? "green" : "red";

                    modalContent.append(
                        $("<p>").css("color", color).text(text)
                    );
                });
            } else {
                modalContent.append($("<p>").text("No updates available."));
            }
        },
        error: function () {
            $("#modalContent").append(
                $("<p>").text("Failed to retrieve data. Please try again.")
            );
        },
    });
    $("#collegeUpdateReport").modal("show");
}

$("#collegeUpdateReport").on("hidden.bs.modal", function () {
    $("#modalContent").empty();
});

function view_updateDetails(reqId) {
    var data = {
        _token: $("#token").val(),
        reqid: reqId,
    };

    $.ajax({
        url: baseurl + "getUpdateDetails",
        method: "POST",
        data: data,
        success: function (response) {
            console.log(response);
            $("#modalContent").empty();

            if (response.error || response.length == 0) {
                $("#modalContent").append(
                    "<p>No update details available.</p>"
                );
            } else {
                let collegeData = {};

                response.forEach(function (item) {
                    const college = item.college;
                    const department = item.department_name;
                    const poc = item.poc_names;
                    const formattedDate = new Date(item.date)
                        .toLocaleDateString("en-GB")
                        .replace(/\//g, "-");

                    if (!collegeData[college]) {
                        collegeData[college] = {};
                    }

                    if (!collegeData[college][department]) {
                        collegeData[college][department] = [];
                    }

                    collegeData[college][department].push(
                        `Updated at: ${formattedDate} - POC: ${poc}`
                    );
                });

                for (let college in collegeData) {
                    $("#modalContent").append(
                        $("<p>").css("font-weight", "bold").text(college)
                    );

                    for (let department in collegeData[college]) {
                        $("#modalContent").append(
                            $("<p>").css("font-weight", "bold").text(department)
                        );

                        collegeData[college][department].forEach(function (
                            datePoc
                        ) {
                            $("#modalContent").append($("<p>").text(datePoc));
                        });
                    }
                }
            }

            $("#collegeUpdateModal")
                .modal({
                    backdrop: "static",
                    keyboard: false,
                })
                .modal("show");
        },
        error: function (xhr, status, error) {
            $("#modalContent")
                .empty()
                .append("<p>No update details available.</p>");
            $("#collegeUpdateModal")
                .modal({
                    backdrop: "static",
                    keyboard: false,
                })
                .modal("show");
        },
    });
}

function fetchClaimDetails(id) {
    $.ajax({
        url: baseurl + "claimdetails-master/" + id, // Adjust the route if necessary
        method: "GET",
        success: function (data) {
            if (data.error) {
                alert(data.error);
                return;
            }
            console.log(data);
            $("#claimDetailsModalLabel").text("Claim Details");
            $("#tableHeading").text(
                `${data.college_name} - ${data.department_name}`
            );
            $("#claimDetailsTable tbody").empty();

            // Populate the table with claim details
            let i = 1;
            $.each(data.claim_details, function (index, detail) {
                $("#claimDetailsTable tbody").append(`
                    <tr>
                        
                        <td>${detail.pac_name}</td>
                        <td>${Number(detail.total_package_amount).toFixed(
                            2
                        )}</td>
                        <td>${Number(detail.total_package_tax).toFixed(2)}</td>
                        <td>${Number(detail.total_package_fullamount).toFixed(
                            2
                        )}</td>
                    </tr>
                `);
            });

            // Show the modal
            $("#claimDetailsModal").modal("show");
        },
        error: function (xhr, status, error) {
            $("#modalContent")
                .empty()
                .append("<p>No claim details available.</p>");
            $("#collegeUpdateModal")
                .modal({
                    backdrop: "static",
                    keyboard: false,
                })
                .modal("show");
        },
    });
}

function approveReq(id) {
    data = {
        _token: $("#token").val(),
        id: id,
    };
    $.ajax({
        url: baseurl + "request_details",
        type: "POST",
        data: data,
        success: function (res) {
            var dateInYMD1 = res.from_date;
            var dateInDMY1 = formatDateYMDtoDMY(dateInYMD1);
            var dateInYMD2 = res.to_date;
            var dateInDMY2 = formatDateYMDtoDMY(dateInYMD2);
            console.log(dateInDMY1);
            $("#editfromdate").val(dateInDMY1);
            $("#edittodate").val(dateInDMY2);
            $("#reqid").val(id);
            $("#modal-masterapprove").modal("show");
        },
    });
}
function approveReqAppeal(id) {
    data = {
        _token: $("#token").val(),
        id: id,
    };
    $.ajax({
        url: baseurl + "request_details",
        type: "POST",
        data: data,
        success: function (res) {
            var dateInYMD1 = res.from_date;
            var dateInDMY1 = formatDateYMDtoDMY(dateInYMD1);
            var dateInYMD2 = res.to_date;
            var dateInDMY2 = formatDateYMDtoDMY(dateInYMD2);
            console.log(dateInDMY1);
            $("#editfromdate").val(dateInDMY1);
            $("#edittodate").val(dateInDMY2);
            $("#reqid").val(id);
            $("#modal-masterapprove").modal("show");
        },
    });
}
    
$("#approveRequestAppeal").on("submit", function (e) {
    e.preventDefault();
    var data = {
        _token: $("#token1").val(),
        reqid: $("#reqid").val(),
        editfromdate: $("#editfromdate").val(),
        edittodate: $("#edittodate").val(),
        mcomments: $("#mcomments").val(),
    };
    $.ajax({
        url: baseurl + "approveRequestAppeal",
        type: "POST",
        data: data,
        success: function (response) {
            swal("Request approved successfully!");
           
            $('#approveRequestAppeal')[0].reset(); 
            $("#modal-masterapprove").modal('hide');
            id = $("#reqid").val();
            $("#approve-btn-" + id).hide();
            $("#reject-btn-" + id).hide();
            
        },
        error: function (xhr) {
            console.error(xhr.responseText); 
            swal("An error occurred while approving the request.");
        }
        
    });
});





function rejectReq(id) {
    swal(
        {
            title: "",
            text: "Please provide a reason for your action:",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            animation: "slide-from-top",
            inputPlaceholder: "Enter reason here",
            confirmButtonText: "Reject",
            cancelButtonText: "Cancel",
        },
        function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("You need to enter a reason!");
                return false;
            }

            $(".pageloader").show();

            var data = {
                id: id,
                _token: $("#token_eva").val(),
                inputValue: inputValue,
                reqid: id,
            };
            $.ajax({
                url: baseurl + "reject_request",
                type: "POST",
                data: data,
                success: function (res) {
                    $(".pageloader").hide();
                    swal(
                        {
                            title: "",
                            text: "Successfully rejected",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: "OK",
                        },
                        function () {
                            $("#act_" + id).hide(); // Hide the Approved status
                            $("#pend_" + id).hide(); // Hide the Pending status

                            // Show Rejected status with reason
                            $("#deact_" + id)
                                .html("Rejected<br>Reason: " + inputValue)
                                .css("color", "red")
                                .show();
                            console.log($("#deact_" + id));
                            $("#approve-btn-" + id).hide();
                            $("#reject-btn-" + id).hide();
                        }
                    );
                },
            });
        }
    );
}
function rejectReqAppeal(id) {
    swal(
        {
            title: "",
            text: "Please provide a reason for your action:",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            animation: "slide-from-top",
            inputPlaceholder: "Enter reason here",
            confirmButtonText: "Reject",
            cancelButtonText: "Cancel",
        },
        function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("You need to enter a reason!");
                return false;
            }

            $(".pageloader").show();

            var data = {
                id: id,
                _token: $("#token").val(),
                inputValue: inputValue,
                reqid: id,
            };
            $.ajax({
                url: baseurl + "rejectrequestappeal",
                type: "POST",
                data: data,
                success: function (res) {
                    $(".pageloader").hide();
                    swal(
                        {
                            title: "",
                            text: "Successfully rejected",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: "OK",
                        },
                        function () {
                            // $("#act_" + id).hide(); 
                            // $("#pend_" + id).hide();

                           
                            $("#deact_" + id)
                                .html("Rejected<br>Reason: " + inputValue)
                                .css("color", "red")
                                .show();
                            console.log($("#deact_" + id));
                            $("#approve-btn-" + id).hide();
                            $("#reject-btn-" + id).hide();
                        }
                    );
                },
            });
        }
    );
}

////add student
$(document).ready(function () {
    // Package selection change event
    $("#packname").change(function () {
        var packageId = $(this).val();

        if (packageId) {
            $.ajax({
                url: baseurl + "getpackage-details/" + packageId,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    if (data && data.packagedetails.fee) {
                        $("#packfee").val(data.packagedetails.fee);
                        $("#packtax").val(data.packagedetails.tax);
                        $("#packtot").val(data.packagedetails.total);
                        // $('#reductionvaldiv').show();
                        $("#max_range").val(
                            ((data.packagedetails.fee * 15) / 100).toFixed(2)
                        );
                        var max_range = Math.round(
                            (data.packagedetails.fee * 15) / 100
                        );

                        $("#reduction_amount_span").html(
                            "Minimum-1 and Maximum " + max_range
                        );
                    } else {
                        $("#reductionvaldiv").hide();
                    }
                },
                error: function (xhr, status, error) {
                    console.error("Error fetching package details:", error);
                },
            });
        } else {
            $("#packfee, #packtax, #packtot").val("");
            $("#reductionvaldiv").hide();
        }
    });

    $("#reduction_amount").on("input", function () {
        const originalFee = parseFloat($("#packfee").val()) || 0;
        const reductionAmount = parseFloat($(this).val()) || 0;
        const maxRange = parseFloat($("#max_range").val()) || 0;

        if (reductionAmount > 0 && reductionAmount <= maxRange) {
            // Calculate and show fields
            const reducedFees = (originalFee - reductionAmount).toFixed(2);
            const reducedTax = (reducedFees * 0.18).toFixed(2); // Assuming 18% tax
            const totalFeesAfterReduction = (
                parseFloat(reducedFees) + parseFloat(reducedTax)
            ).toFixed(2);
            const inEffectTotalReductionOffered = (
                reductionAmount + parseFloat(reducedTax)
            ).toFixed(2);

            $("#reducedfees").val(reducedFees);
            $("#reducedtax").val(reducedTax);
            $("#after_reduction").val(totalFeesAfterReduction);
            $("#ineffect_offered").val(inEffectTotalReductionOffered);
            $(
                "#reducedfees_div, #redtaxdiv, #afterreduction_div, #ineffect_div"
            ).show();
        } else {
            // Hide fields if reduction amount is invalid
            $(
                "#reducedfees_div, #redtaxdiv, #afterreduction_div, #ineffect_div"
            ).hide();
        }
    });
});

////validation
console.log($("#collegeups").length);

$("#college_form").validate({
    rules: {
        college: {
            required: true,
            alphabetOnly: true,
        },
        collegestate: {
            required: true,
        },
        collegedist: {
            required: true,
        },
        collegeplace: {
            required: true,
        },
        "collegedep[]": {
            required: true,
        },
    },

    errorPlacement: function (error, element) {
        error.appendTo(element.parent());
    },
    submitHandler: function (form) {
        form.submit();
    },
});

$("#editcollege").validate({
    rules: {
        editcolname: {
            required: true,
            alphabetOnly: true,
        },
        editcollegestate: {
            required: true,
        },
        editcollegedist: {
            required: true,
        },
        collegeplace: {
            required: true,
        },
        "editcollegedep[]": {
            required: true,
        },
    },
    errorPlacement: function (error, element) {
        error.appendTo(element.parent());
    },
    submitHandler: function (form) {
        form.submit();
    },
});
$(document).ready(function () {});
/////ERROR MESSAGE
