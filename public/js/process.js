window.baseurl = "https://eva.sicsapp.com/";

    $(".timepicker").timepicker({
    timeFormat: 'H:i', 
    interval: 30,
    minTime: '00:00', 
    maxTime: '23:59', 
    defaultTime: '', 
    startTime: '00:00',
    dynamic: false, 
    dropdown: true, 
    scrollbar: true 
});
function FetchTrainers(studentId, pacId, studentpackageid) {
    $("#startTime").val("");
    $("#endTime").val("");
    $("#studentpackageid").val(studentpackageid);
    $("#daysDropdown").empty("");
    $("#trainerName").empty("");
    $("#assignTrainerForm").validate().resetForm();
    $("#assignTrainerForm").find(".is-invalid").removeClass("is-invalid");

    var data = {
        _token: $("#token").val(),
        studentId: studentId,
        pacId: pacId,
        studentpackageid: studentpackageid,
    };

    $.ajax({
        url: baseurl + "fetchTrainers",
        type: "POST",
        data: data,
        success: function (res) {
            var obj = res.trainers;
            var assignedTrainers = res.assignedTrainers;
            var assigneddays = res.assigneddays;
            var starttime = res.start_time;
            var endtime = res.end_time;
            var count = obj.length;

            $("#trainerName").html("");

            if (count != 0) {
                var opt = '<option value="" disabled>Select Trainers</option>';

                for (var i = 0; i < count; i++) {
                    var trainer_id = obj[i].trainer_id;
                    var trainer_name = obj[i].trainer_name;

                    var selected = assignedTrainers.includes(trainer_id) ? "selected" : "";

                    opt +=
                        '<option value="' +
                        trainer_id +
                        '" ' +
                        selected +
                        ">" +
                        trainer_name +
                        "</option>";
                }

                $("#trainerName").html(opt);
            } else {
                $("#trainerName").html('<option value="" disabled>' + res.message + '</option>');
            }
            fordays(assigneddays);
            fortime(starttime, endtime);


            showModal(studentId, pacId);
        },
        error: function (xhr, status, error) {

            console.error("Error:", error);

        },
    });
}

function showModal(studentId, pacId) {
    $("#assignTrainerModel")
        .modal({
            backdrop: "static",
            keyboard: false,
        })
        .modal("show");

    $("#student_id").val(studentId);
    $("#pac_id").val(pacId);
}

function fordays(assigneddays) {
    $("#daysDropdown").html("");

    var opt = '<option value="" disabled>Select Days</option>';
    var daysOfWeek = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

    for (var i = 0; i < daysOfWeek.length; i++) {
        var selecteddays = assigneddays.includes(daysOfWeek[i]) ? "selected" : "";
        opt +=
            '<option value="' +
            daysOfWeek[i] +
            '" ' +
            selecteddays +
            ">" +
            daysOfWeek[i] +
            "</option>";
    }

    $("#daysDropdown").html(opt);
}

function fortime(starttime, endtime) {
    if (starttime && endtime) {
        $("#startTime").val(starttime);
        $("#endTime").val(endtime);
    } else {
        $("#startTime, #endTime").val("");
    }
}



