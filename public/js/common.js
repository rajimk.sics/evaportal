/*!
*created By Raji
*/
window.baseurl = "https://eva.sicsapp.com/";

    var currentDate = new Date();
    var currentYear = currentDate.getFullYear();
    let currentMonth = ('0' + (currentDate.getMonth() + 1)).slice(-2);
    var currentDay = currentDate.getDate();
    var sdate ='01-'+currentMonth+'-'+currentYear;
    var edate =currentDay+'-'+currentMonth+'-'+currentYear;
    $(document).on('click', '.prviewimage', function() {    
        var imageUrl = $(this).data('bs-image'); 
      
        $('#modal-image').attr('src', imageUrl); 
      
    });
    document.addEventListener('DOMContentLoaded', function () {
        const loader = document.getElementById('loader');

        document.querySelectorAll('a.download-link').forEach(function (link) {
            link.addEventListener('click', function (event) {
               
                $(".pageloader").show();
                 setTimeout(() => {
                    $(".pageloader").hide();
                 }, 5000); // Adjust time as needed
            });
        });
    });

    $(".selectclass").change(function () {

        var elementId = $(this).attr('id')
      
        if (this.val != '') {
      
            $('label[for="'+elementId+'"').hide();
      
        }
        else {
      
          $('label[for="'+elementId+'"').show();
      
        }
      
      });


    $("#departmentsform").validate({
        rules: {
            "departmentPoc": {
                required: true          
            },        
        },
      
        submitHandler: function (form) { 
            form.submit();
        }
    });

    function subtopicuploads(topic_id) {
       
        var data = {
            _token: $("#token_eva").val(),
            topic_id: topic_id,
        };
    
        $.ajax({
            url: baseurl + "fetchLinkedTechnologies",
            method: 'POST',
            data: data,
            success: function(response) {

                $("#subtopicCSV")
                .modal({
                    backdrop: "static", 
                    keyboard: false,
                })
                .modal("show");
        
                $("#csvfile").val("");
                $("#topic_id").val(topic_id);

                
                
                let options = '<option value="">Select Technology</option>';
                response.tech.forEach(function(techs) {
                    options += `<option value="${techs.id}" selected>${techs.technology}</option>`;
                });
                $('#technology').html(options).trigger('change');
            },
            error: function(xhr) {
                console.error("Error fetching technologies:", xhr.responseText);
            }
        });
    
    }


    $("#addclaimstudents").validate({
        rules: {
            "oldpay_date": {
                required: true
    
            },
           
            "department": {
                required: true
    
            },
            "pocId[]": {
                required: true
    
            },
            "packname": {
                required: true,
    
            },
    
            "claimstart_date": {
                required: true
            },
    
    
            "claimend_date": {
                required: true,
    
            },
            "regfees": {
                required: true,
    
            },
    
            "referenece_list": {
                required: true,
    
            },
            "claimfile": {
                required: true
    
            },
    
    
    
        },
    
        submitHandler: function (form) {
            form.submit();
            $(".pageloader").show();
    
        }
    });

 
    $('#claimstart_date').Zebra_DatePicker({
        format: 'd-m-Y',
        direction: true,
        pair: $('#claimend_date')

    });

    $('#claimend_date').Zebra_DatePicker({
        format: 'd-m-Y',
        direction: true



    });

    function edit_departmentPoc(id,name){
   
        $("#modal-edit-departmentPoc").modal({
            backdrop: 'static', 
            keyboard: false     
        }).modal('show');
        $('#editdepartid').val(id)

;
   
        $('#editdepartPoc').val(name);
       }
   
       function add_departmentPoc() {
        $("#departmentsform")[0].reset(); 
        $("#departmentsform").validate().resetForm();
        $('#modal-DepartmentPoc').modal({
            backdrop: 'static', 
            keyboard: false     
        }).modal('show'); 
    }

$("#edit_department_pocForm").validate({
    rules: {
        "editdepartPoc": {
            required: true          
        },        
    },
  
    submitHandler: function (form) { 
        form.submit();
    }
});

function toggleDeptPocStatus(id, type,  action) {
    let swalTitle = `Are you sure you want to ${action} this ${type} Poc?`;
    swal(
        {
            title: swalTitle,
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: `Yes, change it!`,
            closeOnConfirm: false,
            html: false,
        },
        function () {
            var data = {
                _token: $("#token_eva").val(),
                type: type,
                action: action,
                id:id
            };

            $.ajax({
                url: baseurl + "activateDeactivePoc" ,
                type: "POST",
                data: data,
                success: function (res) {
                    console.log(res);
                    swal(
                        {
                            title: "",
                            text: `Successfully ${action}d`,
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: "OK",
                        },
                        function () {
                            if (action === 'activate') {
                                $("#act_" + id).css("display", "block");
                                $("#actb_" + id).css("display", "none");

                                $("#deact_" + id).css("display", "none");
                                $("#deactb_" + id).css("display", "block");
                            } else {
                                $("#act_" + id).css("display", "none");
                                $("#deact_" + id).css("display", "block");
                                $("#actb_" + id).css("display", "block");
                                $("#deactb_" + id).css("display", "none");
                            }
                        }
                    );
                },
                error: function (xhr) {
                    swal({
                        title: "Error!",
                        text: xhr.responseJSON.error || "An error occurred.",
                        type: "error",
                        confirmButtonText: "OK",
                    });
                },
            });
        }
    );
}



















 $("#departmentform").validate({
    rules: {
      "department": {
        required: true
      },
    },
    errorPlacement: function (error, element) {

      error.appendTo(element.parent());
    },
    submitHandler: function (form) {
      form.submit();
    }
  });
  
  $("#editdepartmentform").validate({
    rules: {
      "editdepname": {
        required: true
      },
    },
    errorPlacement: function (error, element) {

      error.appendTo(element.parent());
    },
    submitHandler: function (form) {
      form.submit();
    }
  });



  function addDepartment() {

    $("#modal-dep").modal('show');
    $("#department").val('');

  }


  function editDepartment(id, name) {
    //alert(name);
    $("#modal-edit-dep").modal('show');
    $('#editdepid').val(id)
;

    $('#editdepname').val(name);
  }
function actDepartment(id)
{

    swal({
      title: "Are you sure  you want to  activate?",
      text: "",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, activate it!",
      closeOnConfirm: false,
      html: false
    }, function () {

      var data = {
        _token: $('#token_eva').val(),
        id: id
      }
      $.ajax({
        url: baseurl + "departmentStatus",
        type: "POST",
        data: data,
        success: function (res) {

          swal({
            title: "",
            text: "Successfully activated",
            type: "success",
            showCancelButton: false,
            dangerMode: false,
            confirmButtonText: 'OK'
          }, function () {

            $("#act_" + id).css("display", "block");
            $("#deact_" + id).css("display", "none");

            $("#deactb_" + id).css("display", "block");

            $("#actb_" + id).css("display", "none");
            //window.location = baseurl+"manage_designation";
          })

        }
      });
    });

  }


function deactDepartment(id)
{
    //alert(id)
;
    swal({
      title: "Are you sure  you want to  deactivate?",
      text: "",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, deactivate it!",
      closeOnConfirm: false,
      html: false
    }, function () {

      var data = {
        _token: $('#token_eva').val(),
        id: id
      }
      $.ajax({
        url: baseurl + "departmentStatus",
        type: "POST",
        data: data,
        success: function (res) {

          swal({
            title: "",
            text: "Successfully deactivated",
            type: "success",
            showCancelButton: false,
            dangerMode: false,
            confirmButtonText: 'OK'
          }, function () {


            $("#act_" + id).css("display", "none");
            $("#deact_" + id).css("display", "block");

            $("#deactb_" + id).css("display", "none");

            $("#actb_" + id).css("display", "block");

          })

        }
      });
    });

  }









$("#reissue_form").validate({
        rules: {
            "redate1": {  
                required: true,
            }, 
            "redate2": {
                required: true,   
               
            },            
        },
        errorPlacement: function(error, element) {
         
            error.appendTo(element.parent());     
      },
      submitHandler: function (form,e) {
        e.preventDefault();
        $("#modal-reissue").modal('hide');
        $(".pageloader").show();
        var table = $('#example').DataTable();
        var no=$("#rowno").val();
        var data = {
            _token: $("#token_eva").val(),
            redate1:  $("#redate1").val(),
            redate2: $("#redate2").val(),
            student_id:$("#student_id").val(),
            id:$("#rowno").val(),
        };     
        $(".pageloader").hide();
             $.ajax({
               url:baseurl + "savereissuegatepass",
               type: "POST",
               data: data,
              
               success: function (res) {
               
                  swal({
                           title: "",
                           text: "Successfully Reissued",
                           type: "success",
                           showCancelButton: false,
                           dangerMode: false,
                           confirmButtonText: 'OK'
                  }, function () {
                 
                    //var row = $('#row_'+no).closest('tr');
                    ///table.row(row).remove().draw();
                  
                });
        
            }
        });
    }
    });


    function reissueGatepass(button){

        var row = $(button).closest('tr');
    
        $("#student_id").val(row.data('userid'));
        $("#rowno").val(row.data('gatepassid'));
        $("#renddate").val(row.data('gatepassendate'));
    
        $("#modal-reissue").modal('show');  
    
    } 
    $('#modal-reissue').on('show.bs.modal', function () {
        var nextDay = getNextDay($("#renddate").val());
        $("#renddate").val(nextDay);
        initializeDatePickers(nextDay); 
    });
    $('#start_collegeupdate').Zebra_DatePicker({
        format: 'd-m-Y',
        pair: $('#end_collegeupdate'),
       
      });
      
      
      $('#end_collegeupdate').Zebra_DatePicker({
        format: 'd-m-Y',
      
      });
    
    function initializeDatePickers(startDate) {
        $('#redate1').Zebra_DatePicker({
            format: 'd-m-Y',
            direction: [startDate, false],
            pair: $('#redate2')
        });
    
        $('#redate2').Zebra_DatePicker({
            format: 'd-m-Y',
            direction: true
        });
    }
    function getNextDay(dateString) {
        var dateParts = dateString.split('-'); 
        var dateObj = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]); 
        dateObj.setDate(dateObj.getDate() + 1);
        var day = ("0" + dateObj.getDate()).slice(-2);
        var month = ("0" + (dateObj.getMonth() + 1)).slice(-2);
        var year = dateObj.getFullYear();
        return day + '-' + month + '-' + year; 
    }














    $("#editprofile").validate({
        rules: {
            
            "name": {
                required: true          
            },
             
          
        },
      
        submitHandler: function (form) { 
            form.submit();
            $(".pageloader").show();
    
        }
    });

    $("#passwordform").validate({
        rules: {
            "current_password": {
                required: true,
                          
            },
            "new_password": {
                required: true,
                letters_numbers_special:true             
            },
            
            
            "confirm_password": {
                required: true,
                equalTo: "#new_password"
            },
          
            
           
            },
       
      
        submitHandler: function (form) { 
            form.submit();
            $(".pageloader").show();
    
       
    
        }
    });

    

    function candidatedetails(id){
  

        var data = {
            _token	:$('#token').val(),
            id     :id			
        }
        $.ajax({
            url: baseurl + 'candidate_details',
            type: "POST",
            data: data,
            success: function (obj) {
                $("#candidatedetails").html('');
               
                var opt='';
    
                opt +='<p>Name :<span>'+capitalizeFirstLetter(obj.candidatedetails.name)+'</span></p>';
                if(obj.candidatedetails.contact_sec!=null){
                    opt +='<p>Secondary Number :<span>'+obj.candidatedetails.contact_sec+'</span></p>';
                }
                if(obj.candidatedetails.yearof_passout!=null){
                    opt +='<p>Year of Passout :<span>'+obj.candidatedetails.yearof_passout+'</span></p>';
                }
              
                if(obj.candidatedetails.address!=null){
                    opt +='<p>Address :'+obj.candidatedetails.address+'</p>';
                    }
                  
                if(obj.candidatedetails.ref_last_organisation!=null){
                    opt +='<p>Last Organization :<span>'+obj.candidatedetails.ref_last_organisation+'</span></p>';
                }
                if(obj.candidatedetails.organisation_ref_contact!=null){
                    opt +='<p>Reference Contact No :<span>'+obj.candidatedetails.organisation_ref_contact+'</span></p>';
                }
                if(obj.candidatedetails.carrier_break!=null){
                    opt +='<p>Career Break :<span>'+obj.candidatedetails.carrier_break+'</span></p>';
                }
               
                if(obj.candidatedetails.curr_organisation!=null){
                    opt +='<p>Current Organization:<span>'+obj.candidatedetails.curr_organisation+'</span></p>';
                }
                if(obj.candidatedetails.curr_designation!=null){
                    opt +='<p>Current Designation:<span>'+obj.candidatedetails.curr_designation+'</span></p>';   
                }
                 
                $("#candidatedetails").append(opt);
            }
        });
        $("#modal-candidate").modal('show');
    }










    document.addEventListener('DOMContentLoaded', function () {
        const loader = document.getElementById('loader');

        document.querySelectorAll('a.download-linkpass').forEach(function (link) {
            link.addEventListener('click', function (event) {
               
                $(".pageloader").show();
                // Optionally, you can hide the loader after a delay if you know the exact time
                 setTimeout(() => {
                    $(".pageloader").hide();
                 }, 1000); // Adjust time as needed
            });
        });
    });
    function formatDateYMDtoDMY(dateString) {
        if (!dateString) return '';

        var parts = dateString.split('-');
        if (parts.length !== 3) return '';

        var year = parts[0];
        var month = parts[1];
        var day = parts[2];

        return day + '-' + month + '-' + year;
    }

    $(document).on('click', '.remove-email', function() {
        $(this).closest('.email-group').remove();
    });

$(document).on('change', '#emptype', function(){
    var emptype =this.value;          
    if((emptype !='')){
        var data = {
            _token	     :$('#token').val(),
            emptype      :emptype		
        }
        $.ajax({
            url:baseurl + "employeelisting",
            type: "POST",
            data: data,
            success: function (res) {
               var obj = jQuery.parseJSON(res); 
                var count=obj.username.length;                 
                var opt='';                                
            for (i = 0; i < count; i++) {

               var id = obj.username[i].id;
               var name=obj.username[i].name;
                opt+='<option value="'+id+'">'+name+'</option>';
            }
            $("#reported_to").html(opt);        
            }
        });
    }
});


$(document).on('change', '#packtype', function(){

  
    var types =this.value;          
    if((types !='')){
     
        $("#packname").html('');

        var data = {
            _token	        :$('#token').val(),
            type            :$("#packtype").val()			
        }
              $.ajax({
                url:baseurl + "packdetails",
                type: "POST",
                data: data,
                success: function (res) {
                   var obj = jQuery.parseJSON(res); 
                    var count=obj.package.length;                 
                    var opt='';                  
                    var typeid = $("#package_type").val();              
                   
                for (i = 0; i < count; i++) {
                   var id = obj.package[i].pac_id;
                   var name=obj.package[i].pac_name;
                    opt+='<option value="'+id+'">'+name+'</option>';
                }
                $("#packname").html(opt);
            }
               
            });
    }
    else{
        $("#packname").html('');
          
     
    }
 });
$(document).on('change', '#package_type', function(){
    var types =this.value;          
    if((types !='')){
     
        $("#package_name").html('');

        var data = {
            _token	        :$('#token').val(),
            type            :$("#package_type").val()			
        }
              $.ajax({
                url:baseurl + "package_details",
                type: "POST",
                data: data,
                success: function (res) {
                   var obj = jQuery.parseJSON(res); 
                    var count=obj.gatepass.length;                 
                    var opt='';                  
                    var typeid = $("#package_type").val();
                    
                   if(typeid==1)
                    $("#package").html("Package for Regular"+"<sup>*</sup>");
                   else if(typeid==2)
                   $("#package").html("Package for Chrysalis"+"<sup>*</sup>");
                   else if(typeid==3)
                    $("#package").html("Events"+"<sup>*</sup>");
                    else
                    $("#package").html("");

                    opt+='<option value="">Select</option>';
                for (i = 0; i < count; i++) {

                   var id = obj.gatepass[i].id;
                   var name=obj.gatepass[i].name;
                    opt+='<option value="'+id+'">'+name+'</option>';
                }
                $("#package_name").html(opt);
                $("#list").css('display', 'block');                         
                  
                }
            });
    }
    else{
        $("#package_name").html('');
        $("#list").css('display', 'block');      
     
    }

 });


 /*simple function*/

function changedate(id,start_date,end_date){


    $("#cdate1").val();
    $("#cdate2").val();
    $("#pass_id").val(id);

    $("#modal-changedate").modal('show');

    $("#modal-changedate").modal({backdrop: 'static', keyboard: false})  

} 
function accountsapprove(variable,paymentid,source){

    var table = $('#example').DataTable();

    swal({
		title: "Are you sure  you want to  approve?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, Approve it!",
		closeOnConfirm: false,
		html: false
	  }, function(){
        $(".pageloader").show();
		  var data = {
				  payment_id     :paymentid,	
                  _token	:$('#token_eva').val(),	
                  source:source
			  }
              $.ajax({
                url:baseurl+ "accountsapprove",
                type: "POST",
                data: data,
                success: function (res) {

                    console.log(res);
                    $(".pageloader").hide();
                    swal({
                            title: "",
                            text: "Successfully approved",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function (res) {
                            
                            var row = $('#row_'+variable).closest('tr');
                            table.row(row).remove().draw();
                       
                        })
                  
                }
            });
        });

}







function approvePayment(id){

    var table = $('#example').DataTable();
    //alert(id)
    swal({
		title: "Are you sure  you want to  approve?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, Approve it!",
		closeOnConfirm: false,
		html: false
	  }, function(){
        $(".pageloader").show();
		  var data = {
				  id     :id,	
                  _token	:$('#token_eva').val(),	
			  }
              $.ajax({
                url:baseurl+ "approve_payment",
                type: "POST",
                data: data,
                success: function (res) {

                    console.log(res);
                    $(".pageloader").hide();
                    swal({
                            title: "",
                            text: "Successfully approved",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function (res) {

                            /*$("#button_"+id).html('');
                            $("#ap_"+id).css("display","block");
                            $("#pe_"+id).css("display","none");
                            $("#re_"+id).css("display","none");

                            $("#action_"+id).html('Approved By '+$("#logname").val())*/

                          
                            var row = $('#row_'+id).closest('tr');
                            table.row(row).remove().draw();
                           
                           
                        })
                  
                }
            });
        });
		
}




function accountsreject(variable,paymentid,source,paymentType){

    
    $("#modal-payreject").modal('show');
    $("#payment_id").val(paymentid);
    $("#source").val(source);
    $("#rowid").val(variable);
    $("#reason_type").empty();

   
    $("#reason_type").append('<option value="">Select Reason Type</option>');

    
    if (paymentType == 'bank') {
        
        $("#reason_type").append('<option value="1">Screenshot not correct</option>');
        $("#reason_type").append('<option value="2">Transaction ID not correct</option>');
        $("#reason_type").append('<option value="3">Amount Mismatch</option>');
    } else if (paymentType == 'cash') {
       
        $("#reason_type").append('<option value="3">Amount Mismatch</option>');
    }
}





function rejectPayment(id){

    var table = $('#example').DataTable();
  
    swal({
		title: "",
        text: "Please provide a reason for your action:",
        type: "input",
		showCancelButton: true,
        closeOnConfirm: false,
        animation: "slide-from-top",
        inputPlaceholder: "Enter reason here",
        confirmButtonText: "Submit",
        cancelButtonText: "Cancel"
	  }, function(inputValue){

        if (inputValue === false) return false;
        if (inputValue === "") {
            swal.showInputError("You need to enter a reason!");
            return false
        }

        $(".pageloader").show();
		 
		  var data = {
				  id     :id,	
                  _token	:$('#token_eva').val(),	
                  inputValue:inputValue
			  }
              $.ajax({
                url:baseurl+ "reject_payment",
                type: "POST",
                data: data,
                success: function (res) {
                    $(".pageloader").hide();
                    swal({
                            title: "",
                            text: "Successfully rejected",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function () {
                            //window.location = baseurl+"manage_paymentrequest";

                          
                           /* $("#button_"+id).html('');
                            $("#ap_"+id).css("display","none");
                            $("#pe_"+id).css("display","none");
                            $("#re_"+id).css("display","block");

                           $("#action_"+id).html('Rejected By '+$("#logname").val())
                           $("#reasonby_"+id).html('Reason: '+inputValue)*/
                          
                           var row = $('#row_'+id).closest('tr');
                           table.row(row).remove().draw();


                        })
                  
                }
            });
        });
		
}


function returnPass(id){
    swal({
        title: "Are you sure  you want to  Return?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Return it!",
        closeOnConfirm: false,
        html: false
      }, function(){ 

        $("#modal-returngatepass").modal('show');
        $("#gpass_id").val(id)
;
        $("#modal-returngatepass").modal({backdrop: 'static', keyboard: false}); 
        swal.close();

});

}

function rejectPass(id){
     
    swal({
        title: "",
        text: "Please provide a reason for your action:",
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        animation: "slide-from-top",
        inputPlaceholder: "Enter reason here",
        confirmButtonText: "Submit",
        cancelButtonText: "Cancel"
    }, function(inputValue){
        if (inputValue === false) return false;
        if (inputValue === "") {
            swal.showInputError("You need to enter a reason!");
            return false
        }


        var data = {
            _token	:$('#token_eva').val(),
            id     :id,
            inputValue:inputValue

        }
              $.ajax({
                url:baseurl+"reject_gatepass",
                type: "POST",
                data: data,
                success: function (res) {
              
                    swal({
                            title: "",
                            text: "Successfully Rejected",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function () {

                            var table = $('#example').DataTable();

                            // ID of the row you want to remove
                            var rowId = 'row_' +id;
    
                            // Find the row using its ID and remove it
                            table.row('#' + rowId).remove().draw();
    


                           // window.location = baseurl+"manage_gatepass";
                      
                          /* $("#pend_"+id).css("display","none");
                           $("#act_"+id).css("display","none");
                           $("#deact_"+id).css("display","block");
                           
                           $("#deactb_"+id).css("display","none");
                           
                           $("#actb_"+id).css("display","none");
                           $("#date_"+id).css("display","none");

                           $("#down_"+id).css("display","none");

                           $("#action_by_"+id).html('Rejected By '+$("#logname").val())
                           $("#reasonby_"+id).html('Reason: '+inputValue)*/

                           
                           
                        })
                  
                }
            });


    });
        
}



function approvePass(id){

swal({
    title: "Are you sure  you want to  approve",
    text: "",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, approve it!",
    closeOnConfirm: false,
    html: false
  }, function(){
     
    var data = {
        _token	:$('#token_eva').val(),
        id     :id			
    }
          $.ajax({
            url:baseurl+"approve_gatepass",
            type: "POST",
            data: data,
            success: function (res) {
          
                swal({
                        title: "",
                        text: "Successfully Approved",
                        type: "success",
                        showCancelButton: false,
                        dangerMode: false,
                        confirmButtonText: 'OK'
                    },function () {


                        var table = $('#example').DataTable();

                        // ID of the row you want to remove
                        var rowId = 'row_' +id;

                        // Find the row using its ID and remove it
                        table.row('#' + rowId).remove().draw();

                       /* $("#pend_"+id).css("display","none");
                        $("#act_"+id).css("display","block");
                        $("#deact_"+id).css("display","none");
                        
                        $("#deactb_"+id).css("display","none");
                        
                        $("#actb_"+id).css("display","none");

                        $("#actb_"+id).css("display","none");

                        $("#date_"+id).css("display","none");

                        

                        $("#downspan_"+id).html('<a href="'+baseurl+'gatepass_download/'+id+'">Download</a>');

                        $("#action_by_"+id).html('Approved By '+$("#logname").val());
                        $("#ret_"+id).css("display","block");
                       // window.location =  baseurl+"manage_gatepass";*/


                       



                    })
              
            }
        });
    });




    
}

function editContent(id){
    data = {
    _token  :$('#token_eva').val(),
    id     :id          
    }
    $.ajax({
    url: 'edit_content',
    type: "POST",
    data: data,
    success: function (res) {
    
      //  $("#question_details").html('');
        var obj = jQuery.parseJSON(res);        
        $("#edittype").val(obj.contents.type);
        $("textarea#editcontent").val(obj.contents.content);
       
        $("#editcontentid").val(id)
;
        $("#modal-edit-content").modal('show');   
       
    }
    });
    
    }

 function addContent(){

    $("#modal-content").modal('show');
    $("#type").val('');
    $("#content").val('');

  }

  $("#editcnum1sss").blur(function() {
        

    if($("#editcnum1").val()!=''){

    var data = {
        _token	:$('#token').val(),
        no     :$("#editcnum1").val(),
        id     :$("#contact_id").val()
    }
    $.ajax({
        url: baseurl + 'checkeditno',
        type: "POST",
        data: data,
        success: function (res) {

            if(res==0){
                swal("", "This Contacts Already Exist", "error");

            }
      
        }
    });
}

});


/*validation*/

$('#collegeups').validate({
    rules: {
        'collegedep': {
            required: true
        },
        'collegepoc[]': {
            required: true
        },
        'comments': {
            required: true,
  
        },
        'updatestatus': {
            required: true,
  
        }
    },
    errorPlacement: function (error, element) {
  
        error.appendTo(element.parent());
    },
    submitHandler: function (form) {
        form.submit();
    }
  });

  $("#requestForm").validate({
    rules: {
  
      collegeId:{
        required: true,
      },
     
      department: {
        required: true,
       
      },
      'pocId[]': {
        required: true,
    
      },
      to_date: {
        required: true,
       
      },
      comment: {
        required: true,
       
      },
      'documents[]': {
        required: true,
        extension: "pdf,jpg,jpeg,png",
      }
    },
    messages: {
     
      department: {
        required: "Please select department",
      },
      'pocId[]': {
        required: "Please select  Poc",
      },
      to_date: {
        required: "Please enter an end date.",
      },
      comment: {
        required: "Please enter a comment.",
      
      },
      'documents[]': {
        required: "Please select a document.",
        extension: "Only PDF, JPG, JPEG, and PNG files are allowed.",
      }
    },
    errorPlacement: function (error, element) {
  
      error.appendTo(element.parent());
  },

  highlight: function (element) {
    $(element).addClass("error"); 
    $(element).siblings("label.error").show(); 
},
unhighlight: function (element) {
    $(element).removeClass("error"); 
    $(element).siblings("label.error").hide(); 
},
   
    submitHandler: function (form) {
      form.submit();
    }
  });

  $("#approveRequest").validate({
    rules: {
        
        "mcomments": {
            required: true, 
        },
        
    },
   
    errorPlacement: function (error, element) {

        error.appendTo(element.parent());
    },
    submitHandler: function (form) {
        form.submit();
    }
});


$("#returngatepass").validate({
    rules: {
        "gate_file": {  
            required: true,
        }, 
       
    },
    submitHandler: function (form,e) {
        
    e.preventDefault();
    $("#modal-returngatepass").modal('hide');
    $(".pageloader").show();
  
    var formData = new FormData($("#returngatepass")[0]);
     console.log(formData);
    var id= $("#gpass_id").val()
     $(".pageloader").hide();
          $.ajax({
            url:baseurl + "pass_return",
            type: "POST",
           
            data: formData,
            contentType: false,
            processData: false,
            success: function (res) {
               
               swal({
                        title: "",
                        text: "Successfully returned",
                        type: "success",
                        showCancelButton: false,
                        dangerMode: false,
                        confirmButtonText: 'OK'
                    },function () {


                        var table = $('#example').DataTable();

                        // ID of the row you want to remove
                        var rowId = 'row_' +id;

                        // Find the row using its ID and remove it
                        table.row('#' + rowId).remove().draw();

                        /*$("#ret_"+id).css("display","none");

                       $("#pend_"+id).css("display","none");
                       $("#act_"+id).css("display","none");
                       $("#deact_"+id).css("display","none");
                       $("#status_"+id).css("display","block");
                        $("#status_"+id).html('Returned');

                        $("#action_"+id).html('');

                        $("#rby_"+id).html('<span>Returned To '+$("#logname").val()+'</span>');
                        
                      //  window.location = baseurl+"manage_gatepass";*/
                    })
              
            }
        });



        //form.submit();
    }
});


$("#searchmarketing").validate({
    rules: {
        
        "salesname": {
            required: true          
        }
  
    },
  
    submitHandler: function (form) { 
        form.submit();
        $(".pageloader").show();

    }
});

$("#bulk_students").validate({
    rules: {
        
        "file": {
            required: true          
        }
  
    },
  
    submitHandler: function (form) { 
        form.submit();
        $(".pageloader").show();

    }
});


$("#bulk_contact").validate({
    rules: {
        "source": {
            required: true,
                 
        },
        "file": {
            required: true          
        }
  
    },
  
    submitHandler: function (form) { 
        form.submit();
        $(".pageloader").show();

    }
});





$("#editdoj").validate({
    rules: {
        "stud_dob": {  
            required: true,
        }, 
        
       
    },
  
    submitHandler: function (form) {
        $(".pageloader").show();

        var data = {
            _token  :$('#token_eva').val(),
            student_package_id     :$("#student_package_id").val(),
            doj:$("#stud_dob").val()
                   
        }
        var doj=$("#stud_dob").val();
        var student_package_id=$("#student_package_id").val();
              $.ajax({
                url:baseurl + "update_doj",
                type: "POST",
                data: data,
                success: function (res) {
                    $("#dojdate_"+student_package_id).val($("#stud_dob").val());
                    $(".pageloader").hide();
                    $("#stud_dob").val('')

                  


                    $("#modal-changedate").modal('hide');

                    swal({
                        title: "",
                        text: "Successfully updated",
                        type: "success",
                        showCancelButton: false,
                        dangerMode: false,
                        confirmButtonText: 'OK'
                    },function () {
                        
                        $("#doj_"+student_package_id).html(doj);

                        
                    })
                    
                   
                  
                }
            });

    }
});



jQuery.validator.prototype.checkForm = function() {
    this.prepareForm(); // Prepare the form for validation
    for (var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++) {
        // Check if the element has multiple instances
        if (this.findByName(elements[i].name).length > 1) {
            // Loop through each instance of the element
            for (var cnt = 0; cnt < this.findByName(elements[i].name).length; cnt++) {
                this.check(this.findByName(elements[i].name)[cnt]); // Validate each instance
            }
        } else {
            this.check(elements[i]); // Validate the single element
        }
    }
    return this.valid(); // Return the validity of the form
};

$("#interviewemail").validate({
    rules: {
        "emails[]": {
            required: true,
            email: true // Ensures that each email is valid
        }
    },
    messages: {
        "emails[]": {
            required: "Please enter an email address.",
            email: "Please enter a valid email address."
        }
    },
    errorPlacement: function(error, element) {
        // Find the email group and insert the error label after the input field
        error.appendTo(element.closest('.email-group')).show();
    },
    highlight: function(element) {
        $(element).addClass("error"); // Add error class to the input field
        $(element).siblings("label.error").show(); // Show the error label
    },
    unhighlight: function(element) {
        $(element).removeClass("error"); // Remove error class when valid
        $(element).siblings("label.error").hide(); // Hide the error label
    }
});






$("#changedateform").validate({
    rules: {
        "cdate1": {  
            required: true,
        }, 
        "cdate2": {
            required: true,
           
           
        },
       
    },
  
    submitHandler: function (form) {
        $(".pageloader").show();

        var data = {
            _token  :$('#token_eva').val(),
            id     :$("#pass_id").val(),
            start_date:$("#cdate1").val(),
            end_date:$("#cdate2").val()          
        }
              $.ajax({
                url:baseurl + "changedate",
                type: "POST",
                data: data,
                success: function (res) {
                    $("#modal-changedate").modal('hide');
                    $(".pageloader").hide();
                    swal({
                            title: "",
                            text: "Successfully Changed",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function () {

                            $("#sdate_"+$("#pass_id").val()).html($("#cdate1").val());
                            $("#edate_"+$("#pass_id").val()).html($("#cdate2").val());
                            
                            //window.location = baseurl+"manage_questions";

                            




                        })
                  
                }
            });





       
    }
});




$("#addgatepass").validate({
    rules: {
        "name": {
            required: true,
            nameval:true           
        },
        "start_date1": {
            required: true,
                  
        },
        
        
        "end_date1": {
            required: true,
           
                  
        },
        "package_type": {
            required: true,
                   
        },
       
        "package_name":{
            required: true,
        },
        "photo": {

            required: function(element) {
                // Check if the particular value is selected in another field
                return $("#pic").val() === "0";
            },

        },
        
    },
  
    submitHandler: function (form) { 
        form.submit();
        $(".pageloader").show();

    }
});

$("#editcontact").validate({
    rules: {
        "source": {  
            required: true,
        }, 
        "name": {  
            nameval: true,
        },
        "editcnum1": {
            required: true,
           
            digits: true,
            noLeadingZeros: true
           
        },
        "editcnum2": {
            
            checkForDuplicate: true,
            digits: true,
            noLeadingZeros: true
           
        },
        "email": {  
            email: true    
        },    
    },
  
    submitHandler: function (form) {
        var formData = new FormData($("#editcontact")[0]);
        $(".pageloader").show();
        $.ajax({
         url:baseurl + "update_contacts",
                   type: "POST",
                   data: formData,
                   contentType: false,
                   processData: false,
                   success: function (res) {
                    $(".pageloader").hide();
                    console.log(res);
                        if(res==1){
                            swal("", "This Contacts Already Exist", "error");
                        } 
                        else if(res==3){
                            swal("", "This Email Id Exist", "error");
                        }    
                    else if(res==2){
                    swal({
                        title: "",
                        text: "Successfully Updated",
                        type: "success",
                        showCancelButton: false,
                        dangerMode: false,
                        confirmButtonText: 'OK'
                    },function (res) {
        
                        window.location = baseurl+"outboundcalls";
                       
                    })
        
                }
                }
            });
        }
});

$("#closedcontacts").validate({
    rules: {
        email: {
            required: true,
            email: true
        },
        password: {
            required: true,
            letters_numbers_special: true
        }
    },
    submitHandler: function (form, e) {
        e.preventDefault(); // Prevent default form submission

        var table = $('#example').DataTable();

        var data = {
            _token: $("#token_eva").val(),
            email: $("#email").val()
        };

        $.ajax({
            url: baseurl + "emailexist",
            type: "POST",
            data: data,
            success: function (res) {
                console.log(res);
                $(".pageloader").show();
                if (res == 1) {

                    var data = {
                        _token	:$('#token').val(),
                      
                        email:$('#email').val(),
                        password:$('#password').val(),
                        contactid:$('#contactid_close').val()
                        			
                    }
                    $.ajax({
                        url: baseurl + 'closedcontacts',
                        type: "POST",
                        data: data,
                        success: function (res) {
                            $(".pageloader").hide();
                            swal({
                                    title: "",
                                    text: "Successfully closed",
                                    type: "success",
                                    showCancelButton: false,
                                    dangerMode: false,
                                    confirmButtonText: 'OK'
                                },function () {

                                    $("#email").val('');
                                    $("#password").val('');
                                    var id=$('#contactid_close').val()
                                    var row = $('#'+id).closest('tr');


                                    table.row(row).remove().draw();
                                  
                                    $("#modal-closecontact").modal("hide");


                                  
                                })
                          
                        }
                    });

                    
                    
                } else {
                    $(".pageloader").hide();
                    swal("", "Email id already exists", "error");
                }
            }
        });
    }
});

  $("#addtalentopackage").validate({
    rules: {
        "oldpay_date": {
            required: true          
        }, 
        "packname": {
            required: true          
        },  
        "start_date": {
            required: true          
        },
        "end_date": {
            required: true          
        }, 
        "reduction_amount": {
            required: true          
        },
        "regfees": {
            required: true          
        },  

        
        
        
    },
  
    submitHandler: function (form) { 
        form.submit();
    }
});

$("#content_form").validate({
    rules: {
        "type": {
            required: true          
        },  
        "content": {
            required: true          
        },      
    },
  
    submitHandler: function (form) { 
        form.submit();
    }
});

$("#bulk_desig").validate({
    rules: {
        "file": {
            required: true
                   
        },
          
    },
  
    submitHandler: function (form) { 
        $(".pageloader").show();
        form.submit();
    }
});

$("#bulk_college").validate({
    rules: {
        "file": {
            required: true
                   
        },
          
    },
  
    submitHandler: function (form) { 
        $(".pageloader").show();
        form.submit();
    }
});

$("#bulk_quali").validate({
    rules: {
        "file": {
            required: true
                   
        },
          
    },
  
    submitHandler: function (form) { 
        $(".pageloader").show();
        form.submit();
    }
});

$("#bulk_source").validate({
    rules: {
        "file": {
            required: true
                   
        },
          
    },
  
    submitHandler: function (form) { 
        $(".pageloader").show();
        form.submit();
    }
});

$("#bulk_special").validate({
    rules: {
        "file": {
            required: true
                   
        },
          
    },
    submitHandler: function (form) { 
        $(".pageloader").show();
        form.submit();
    }
});

$("#editcontent").validate({
    rules: {
        "edittype": {
            required: true          
        },  
        "editcontent": {
            required: true          
        },      
    },
  
    submitHandler: function (form) { 
        form.submit();
    }
});



$("#editQues").validate({
    rules: {
    
        "editquestion": {
            required: true          
        }, 

        "editoptionA": {
            required: true          
        },
        "editoptionB": {
            required: true          
        },
        "editoptionC": {
            required: true          
        },
        "editoptionD": {
            required: true          
        },

    },
  
    submitHandler: function (form) {
        var options = document.getElementsByName('editanswer');
        for (var i = 0; i < options.length; i++) {
            if (options[i].checked) {
              selected2 = true;
              break;
            }
          }
         if (!selected2) {
            swal('Please select one Correct Answer!');
            return false; // Prevent form submission
          }
          else{
         
              $(".pageloader").show();
              form.submit();
              return true;
             
          } 
        }
});

$("#addquestions").validate({
    rules: {
    
        "question": {
            required: true          
        }, 

        "optionA": {
            required: true          
        },
        "optionB": {
            required: true          
        },
        "optionC": {
            required: true          
        },
        "optionD": {
            required: true          
        },

    },
  
    submitHandler: function (form) {

      
        var options = document.getElementsByName('answer');
        var qType = document.getElementsByName('quesType');
        var selected1 = false;
        var selected2 = false;
      
        for (var i = 0; i < qType.length; i++) {
            if (qType[i].checked) {
              selected1 = true;
              break;
            }
          }
      
        for (var i = 0; i < options.length; i++) {
          if (options[i].checked) {
            selected2 = true;
            break;
          }
        }

        if(!selected1){

            swal('Please select any of the option!');
            return false;

        }
        else if (!selected2) {
          swal('Please select one Correct Answer!');
          return false; // Prevent form submission
        }
        else{
       
            $(".pageloader").show();
            form.submit();
            return true;
           
        }

    
       
    }
});


$("input[type='radio'][name='quesType']").click(function() {
    var value = $(this).val();
    if(value=='Individual Questions'){
        console.log(value);
        $("#individual_questions").css("display","block");
        $("#excel_questions").css("display","none");
    }
    else{
        console.log(value);
        $("#excel_questions").css("display","block");
        $("#individual_questions").css("display","none");

    }
});

function editQues(id){
data = {
_token  :$('#token_eva').val(),
id     :id          
}
$.ajax({
url: baseurl + 'edit_question',
type: "POST",
data: data,
success: function (res) {

  //  $("#question_details").html('');
    var obj = jQuery.parseJSON(res);        
    $("#editquestion").val(obj.quesdetails.questions);
    $("#editoptionA").val(obj.quesdetails.optiona);
    $("#editoptionB").val(obj.quesdetails.optionb);
    $("#editoptionC").val(obj.quesdetails.optionc);
    $("#editoptionD").val(obj.quesdetails.optiond);
    $("input[name=editanswer][value=" + obj.quesdetails.answer + "]").prop('checked', true);
    //$("#editanswer").val(obj.quesdetails.answer);
    $("#editquesid").val(id)
;
    $("#modal-ques").modal('show');   
   
}
});

}

function deactivateQues(id){

swal({
    title: "Are you sure  you want to  deactivate?",
    text: "",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, deactivate it!",
    closeOnConfirm: false,
    html: false
  }, function(){     
    var data = {
        _token  :$('#token_eva').val(),
        id     :id          
    }
          $.ajax({
            url:baseurl + "deactivate_questions",
            type: "POST",
            data: data,
            success: function (res) {
          
                swal({
                        title: "",
                        text: "Successfully deactivated",
                        type: "success",
                        showCancelButton: false,
                        dangerMode: false,
                        confirmButtonText: 'OK'
                    },function () {
                        
                        window.location = baseurl+"manage_questions";
                    })
              
            }
        });
    });
    
}

function activateQues(id){
//alert(id)


swal({
    title: "Are you sure  you want to  activate?",
    text: "",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, activate it!",
    closeOnConfirm: false,
    html: false
  }, function(){
     
      var data = {
              id     :id,   
              _token    :$('#token_eva').val(), 
          }
          $.ajax({
            url:baseurl+ "activate_questions",
            type: "POST",
            data: data,
            success: function (res) {
          
                swal({
                        title: "",
                        text: "Successfully activated",
                        type: "success",
                        showCancelButton: false,
                        dangerMode: false,
                        confirmButtonText: 'OK'
                    },function () {
                        window.location = baseurl+"manage_questions";
                    })
              
            }
        });
    });
    
}



function downloadfun(payment_id){

    $(".pageloader").show();

    var data = {
        _token	:$('#token_eva').val(),
        id     :payment_id			
    }
          $.ajax({
            url:baseurl + "pdf_download",
            type: "GET",
            data: data,
            success: function (res) {

                swal("Sucessfully Downloaded");

                $(".pageloader").hide();
         
               
            }
        });
}
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

$( document ).ready(function() {
    $('#file').change(function() {
        var file = this.files[0];
        var reader = new FileReader();
       
        reader.onload = function(e) {
          $('#previewHeader').attr('src', e.target.result);
          $('#prview').css("display","block");
        }
    
        reader.readAsDataURL(file);
      });

      $('#photo').change(function() {
        var file = this.files[0];
        var reader = new FileReader();
        $(".imgprev").css("display", "block");
        reader.onload = function(e) {
          $('#previewHeader').attr('src', e.target.result);
          
        }
    
        reader.readAsDataURL(file);
      });


    $("#emptype").change(function() {

        var value = $(this).val();
        if(value==3){
            $("#techdiv").css("display", "block");
        }
        else{
            $("#techdiv").css("display", "none");
        }

    });

    


    $("input[type='radio'][name='payMethod_bank']").click(function() {
        var value = $(this).val();
        if(value=='bank'){
            $("#paymethod").html('Amount<sup>*</sup>');
            $(".bankdetails").css("display","block");
        }
        else{

            $("#paymethod").html('Cash<sup>*</sup>');

            $(".bankdetails").css("display","none");

        }
    });



    $("input[type='radio'][name='payMethod']").click(function() {
        var value = $(this).val();
        if(value=='bank'){
            $("#paymethod").html('Amount<sup>*</sup>');
            $("#packagebank").css("display","block");
        }
        else{

            $("#paymethod").html('Cash<sup>*</sup>');

            $("#packagebank").css("display","none");

        }
    });

    $("input[type='radio'][name='eventDate']").click(function() {
        var value = $(this).val();
        if(value=='single'){
            $("#singleday").css("display","block");
            $("#multipleday").css("display","none");
        }
        else{

            $("#singleday").css("display","block");
            $("#multipleday").css("display","block");

        }
    });

    $('#signature').on('change', function() {
      

        var fileInput = document.getElementById('signature').files[0];
        var previewContainer = document.getElementById('previewSignature');
        previewContainer.src='';
        
        
        var reader = new FileReader();
    
        reader.onloadend = function () {
          previewContainer.src = reader.result;
          previewContainer.style.display = 'block';
       //  
      }

      
      
      if (fileInput) {
          reader.readAsDataURL(fileInput);
      } else {
          preview.src = '';
          preview.style.display = 'none';
      }
      
    });
    
    
    $('#seal').on('change', function() {
      

        var fileInput = document.getElementById('seal').files[0];
        var previewContainer = document.getElementById('previewSeal');
        previewContainer.src='';
        
        
        var reader = new FileReader();
    
        reader.onloadend = function () {
          previewContainer.src = reader.result;
          previewContainer.style.display = 'block';
       //  
      }

      
      
      if (fileInput) {
          reader.readAsDataURL(fileInput);
      } else {
          preview.src = '';
          preview.style.display = 'none';
      }
      
    });


    $("#event_form").validate({
        rules: {
            "event_name": {
                required: true          
            }, 
            "eventDate": {
                required: true          
            },

            "start_date": {
                required: true          
            }, 
            "end_date": {
                required: true       
            },

        },
      
        submitHandler: function (form) { 
            $(".pageloader").show();
            form.submit();
        }
    });
    


    $("#credentials").validate({
        rules: {
            "description": {
                required: true          
            }, 
            
        },
      
        submitHandler: function (form) { 
            $(".pageloader").show();
            form.submit();
        }
    });

    $('#amount').on('keyup', function() {

        var inputValue = parseInt($(this).val());
        var minValue =1;
        var maxValue = parseInt($("#remamount").val());
    
        if (inputValue < minValue) {
          $(this).val(minValue);
        } else if (inputValue > maxValue) {
          // Remove the last digit to bring the value within the range
          $(this).val(function(_, value) {
            return value.slice(0, -1);
          });
        }
      });
   

});



function addEvent(){

    $("#modal-event").modal('show');
    $("#event_name").val('');
   

  }

function deactivateEvent(id){


    swal({
		title: "Are you sure  you want to  deactivate?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, deactivate it!",
		closeOnConfirm: false,
		html: false
	  }, function(){	 
        var data = {
            _token	:$('#token_eva').val(),
            id     :id			
        }
              $.ajax({
                url:baseurl + "deactivate_events",
                type: "POST",
                data: data,
                success: function (res) {
              
                    swal({
                            title: "",
                            text: "Successfully deactivated",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function () {
                            
                            window.location = baseurl+"manage_events";
                        })
                  
                }
            });
        });
		
}

function activateEvent(id){
    //alert(id)


    swal({
		title: "Are you sure  you want to  activate?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, activate it!",
		closeOnConfirm: false,
		html: false
	  }, function(){
		 
		  var data = {
				  id     :id,	
                  _token	:$('#token_eva').val(),	
			  }
              $.ajax({
                url:baseurl+ "activate_events",
                type: "POST",
                data: data,
                success: function (res) {
              
                    swal({
                            title: "",
                            text: "Successfully activated",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function () {
                            window.location = baseurl+"manage_events";
                        })
                  
                }
            });
        });
		
}


$("#addbulkemployee").validate({
    rules: {
        "emptype": {
            required: true          
        }, 
       
        "designation": {
            required: true          
        },
        "technology": {
            required: true          
        },
        "experience": {
            required: true          
        },

        
     
    },
  
    submitHandler: function (form) { 
        form.submit();
        $(".pageloader").show();

   

    }
});


$("#viewsalesreport").validate({
    rules: {
        "report_of[]": {
            required: true,      
        },
      
    },
  
    submitHandler: function (form) { 
        form.submit();
        $(".pageloader").show();

    }
});





    $("#paymentMethod").validate({
        rules: {
            "amount": {
                required: true          
            }, 
            "oldpay_date":{
                required: true 
            },
            "package_reciept": {
                required: true          
            },
            "trans_id": {
                required: true          
            }, 

        },
      
        submitHandler: function (form) { 

           

            if($("input[name='payMethod']:checked").val()=='bank'){

            var data = {
                _token	     :$('#token_eva').val(),
                trans_id     :$("#trans_id").val()			
            }

            $.ajax({
                url:baseurl+"transaction_unique",
                type: "POST",
                data: data,
                success: function (res) {

                  

                    if(res==0){

                        $(".pageloader").show();
                        form.submit();

                    }
                    else{

                        swal("", "Transaction Id must be unique", "error");

                    }
              
                   
                  
                }
            });

        }
        else{
            $(".pageloader").show();
            form.submit();

        }

            
        }
    });



    $('#amount').on('keyup', function() {

        var inputValue = parseInt($(this).val());
        var minValue =1;
        var maxValue = parseInt($("#remamount").val());
    
        if (inputValue < minValue) {
          $(this).val(minValue);
        } else if (inputValue > maxValue) {
          // Remove the last digit to bring the value within the range
          $(this).val(function(_, value) {
            return value.slice(0, -1);
          });
        }
      });
   




function deactivate_employee(id){
  
    swal({
		title: "Are you sure  you want to  deactivate?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, deactivate it!",
		closeOnConfirm: false,
		html: false
	  }, function(){
		 
        var data = {
            _token	:$('#token_eva').val(),
            id     :id			
        }
              $.ajax({
                url:baseurl+"deactivate_employee",
                type: "POST",
                data: data,
                success: function (res) {
              
                    swal({
                            title: "",
                            text: "Successfully deactivated",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function () {
                            window.location = baseurl+"manage_employee";
                        })
                  
                }
            });
        });
		
}
function activate_employee(id){
    
    swal({
		title: "Are you sure  you want to  activate?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, activate it!",
		closeOnConfirm: false,
		html: false
	  }, function(){
		 
        var data = {
            _token	:$('#token_eva').val(),
            id     :id			
        }
              $.ajax({
                url:baseurl+"activate_employee",
                type: "POST",
                data: data,
                success: function (res) {
              
                    swal({
                            title: "",
                            text: "Successfully activated",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function () {
                            window.location = baseurl+"manage_employee";
                        })
                  
                }
            });
        });
		
}




function formatPrice(price) {
    // Convert the price to a string with exactly 2 decimal places
    return parseFloat(price).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function pay(){

    $("#modal-pay-package").modal('show');

}

  function addcollege(){

    $("#modal-college").modal('show');
    $("#college").val('');

  }

  function addquali(){

    $("#modal-qualification").modal('show');
    $("#qualification").val('');

  }

  function addspec(){

    $("#modal-specialization").modal('show');
    $("#specialization").val('');

  }


function adddesignation(){

    $("#modal-desi").modal('show');
    $("#designation").val('');
    //$('#modal-desi').modal({backdrop: 'static', keyboard: false}) 
}
function adddtec(){

    $("#modal-technology").modal('show');
    $("#technology").val('');
    //$('#modal-desi').modal({backdrop: 'static', keyboard: false}) 
}

function addsource(){

    $("#modal-source").modal('show');
    $("#source").val('');
    //$('#modal-desi').modal({backdrop: 'static', keyboard: false}) 
}

function discountdetails(){

    if($("#reduction_code").val()==''){


        swal("", "In eligible for reduction", "error");

    }
    else{

        var opterror = '';
        var opt = '';
        var data = {
            _token	:$('#token_eva').val(),
            code     :$("#reduction_code").val()			
        }
              $.ajax({
                url:baseurl + "discountdetails",
                type: "POST",
                data: data,
                success: function (res) {
                    console.log(res);  
                    
                    
                    var obj = jQuery.parseJSON(res); 
                    var codecount=obj.codecount;
                    if(codecount==1){

                        $("#notreduction").html('');
                        opt+='<p>Fees : '+'Rs'+' '+formatPrice(obj.packagedetails.fee)+'</p>';
                        opt+='<p>Tax 18% :'+'Rs'+' '+formatPrice(obj.packagedetails.maintax)+'</p>';
                        opt+='<p>Actual Fees : '+'Rs'+' '+formatPrice(obj.packagedetails.total)+'</p>';
                        opt+='<p>Reduction Amount : '+'Rs'+' '+formatPrice(obj.packagedetails.reduction_amount)+'</p>';
                        opt+='<p>Reduced Fees : '+'Rs'+' '+formatPrice(obj.packagedetails.reduced_fees)+'</p>';
                        opt+='<pTax  : '+'Rs'+' '+formatPrice(obj.packagedetails.tax)+'</p>';
                        opt+='<p>Total Fees After Reduction  : '+'Rs'+' '+formatPrice(obj.packagedetails.totalfees_afterreduction)+'</p>';
                        opt+='<p>In-Effect Total Reduction Offered   : '+'Rs'+' '+formatPrice(obj.packagedetails.ineffect_total_red)+'</p>';
                        $("#reductiondetails").css("display","block");
                        $("#reductiondetails").html(opt);

                    }
                    else{
                        $("#reductiondetails").css("display","none");
                        opterror += '<p style="color:red">Invalid Referal Code</p>';
                        $("#notreduction").html(opterror);
                       
                    }
                    
                  
                }
            });

    }

}
function dpackage(id){


    swal({
		title: "Are you sure  you want to  deactivate?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, deactivate it!",
		closeOnConfirm: false,
		html: false
	  }, function(){	 
        var data = {
            _token	:$('#token_eva').val(),
            id     :id			
        }
              $.ajax({
                url:baseurl + "deact_package",
                type: "POST",
                data: data,
                success: function (res) {
              
                    swal({
                            title: "",
                            text: "Successfully deactivated",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function () {

                            if(res==1){

                                window.location = baseurl+"manage_package";

                            }
                            else{

                                window.location = baseurl+"manage_package_chrysallis";

                            }
                            
                           
                        })
                  
                }
            });
        });
		
}
function apackage(id){
    //alert(id)
;
    swal({
		title: "Are you sure  you want to  activate?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, activate it!",
		closeOnConfirm: false,
		html: false
	  }, function(){
		 
		  var data = {
				  id     :id,	
                  _token	:$('#token_eva').val(),	
			  }
              $.ajax({
                url:baseurl+ "act_package",
                type: "POST",
                data: data,
                success: function (res) {
              
                    swal({
                            title: "",
                            text: "Successfully activated",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function () {
                            if(res==1){

                                window.location = baseurl+"manage_package";

                            }
                            else{

                                window.location = baseurl+"manage_package_chrysallis";

                            }
                        })
                  
                }
            });
        });
		
}

function employeedetails(id){
  

    var data = {
        _token	:$('#token').val(),
        id     :id			
    }
    $.ajax({
        url: baseurl + 'employee_details',
        type: "POST",
        data: data,
        success: function (res) {
            $("#employeedetails").html('');
            var obj = jQuery.parseJSON(res);
            var opt='';

            opt +='<p>Employee ID :<span>'+obj.employeedetails.empid+'</span></p>';
            if(obj.employeedetails.name!=null){
                opt +='<p>Name :<span>'+obj.employeedetails.name+'</span></p>';
            }
            if(obj.employeedetails.designation!=null){
                opt +='<p>Designation :<span>'+obj.employeedetails.designation+'</span></p>';
            }
            if(obj.employeedetails.role==3){
                if(obj.employeedetails.technology!=null){
                opt +='<p>Technology :<span>'+obj.techno+'</span></p>';
                }
            }   
            if(obj.employeedetails.experience!=null){
                opt +='<p>Experience :<span>'+obj.employeedetails.experience+'</span></p>';
            }
            if(obj.employeedetails.phone!=null){
                opt +='<p>Phone :<span>'+obj.employeedetails.phone+'</span></p>';
            }
            if(obj.employeedetails.email!=null){
                opt +='<p>Email :<span>'+obj.employeedetails.email+'</span></p>';
            }
           
            if(obj.employeedetails.office_email!=null){
                opt +='<p>Office email:<span>'+obj.employeedetails.office_email+'</span></p>';
            }
            if(obj.employeedetails.poc_code!=null){
                opt +='<p>Poc Code:<span>'+obj.employeedetails.poc_code+'</span></p>';
            }

            opt += '<hr style="width: 100%; border: 1px solid #333; margin: 10px auto;">';
            opt+= '<b><p>PRIVILEGES </b><br>' +
                    'Interview Schedule : ' + (obj.employeedetails.interview==1 ? 'YES' : 'NO') + '<br>' +
                    'Designation : ' + (obj.employeedetails.desig ==1 ? 'YES' : 'NO') + '<br>' +
                    'Technology : ' + (obj.employeedetails.techs==1 ? 'YES' : 'NO') + '<br>' +
                    'Source: ' + (obj.employeedetails.source==1 ? 'YES' : 'NO') + '<br>' +
                    'College : ' + (obj.employeedetails.college==1 ? 'YES' : 'NO') + '<br>' +
                    'Qualification : ' + (obj.employeedetails.qualification==1 ? 'YES' : 'NO') + '<br>' +
                    'Specialization : ' + (obj.employeedetails.specialization==1 ? 'YES' : 'NO') + '<br>' +
                    'Add Employee : ' + (obj.employeedetails.employee==1 ? 'YES' : 'NO') + '<br>' +
                    'Add Package: ' + (obj.employeedetails.package==1 ? 'YES' : 'NO') + '<br>' +
                    'Add Event : ' + (obj.employeedetails.event==1 ? 'YES' : 'NO') + '</p>'

          
           
            $("#employeedetails").append(opt);
        }
    });
    $("#modal-employee").modal('show');
}

function editspecialization(ids,names){
    //alert(names);
    $("#modal-edit-specialization").modal('show');
    $('#editspecid').val(ids);
    $('#editspecname').val(names);
    }

function dspecialization(id){
  
    swal({
		title: "Are you sure  you want to  deactivate?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, deactivate it!",
		closeOnConfirm: false,
		html: false
	  }, function(){
		 
        var data = {
            _token	:$('#token_eva').val(),
            id     :id			
        }
              $.ajax({
                url:baseurl+"dact_specialization",
                type: "POST",
                data: data,
                success: function (res) {
              
                    swal({
                            title: "",
                            text: "Successfully deactivated",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function () {

                            $("#act_"+id).css("display","none");
                            $("#deact_"+id).css("display","block");
                            
                            $("#deactb_"+id).css("display","none");
                            
                            $("#actb_"+id).css("display","block");


                            //window.location = baseurl+"manage_specialization";
                        })
                  
                }
            });
        });
		
}
function aspecialization(id){
    
    swal({
		title: "Are you sure  you want to  activate?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, activate it!",
		closeOnConfirm: false,
		html: false
	  }, function(){
		 
        var data = {
            _token	:$('#token_eva').val(),
            id     :id			
        }
              $.ajax({
                url:baseurl+"act_specialization",
                type: "POST",
                data: data,
                success: function (res) {
              
                    swal({
                            title: "",
                            text: "Successfully activated",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function () {

                            $("#act_"+id).css("display","block");
                                $("#deact_"+id).css("display","none");

                                $("#deactb_"+id).css("display","block");

                                $("#actb_"+id).css("display","none");

                            //window.location = baseurl+"manage_specialization";
                        })
                  
                }
            });
        });
		
}

function editqualification(id,name){
    //alert(name);
    $("#modal-edit-qualification").modal('show');
    $('#editqualiid').val(id)
;
    $('#editqualiname').val(name);
    }

    function dQualification(id){
     
        swal({
            title: "Are you sure  you want to  deactivate?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, deactivate it!",
            closeOnConfirm: false,
            html: false
          }, function(){
             
            var data = {
                _token	:$('#token_eva').val(),
                id     :id			
            }
                  $.ajax({
                    url:baseurl+"dact_qualification",
                    type: "POST",
                    data: data,
                    success: function (res) {
                  
                        swal({
                                title: "",
                                text: "Successfully deactivated",
                                type: "success",
                                showCancelButton: false,
                                dangerMode: false,
                                confirmButtonText: 'OK'
                            },function () {

                                $("#act_"+id).css("display","none");
                                $("#deact_"+id).css("display","block");
                                
                                $("#deactb_"+id).css("display","none");
                                
                                $("#actb_"+id).css("display","block");
                                


                                //window.location = baseurl+"manage_qualification";
                            })
                      
                    }
                });
            });
            
    }
    function aQualification(id){
       
        swal({
            title: "Are you sure  you want to  activate?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, activate it!",
            closeOnConfirm: false,
            html: false
          }, function(){
             
            var data = {
                _token	:$('#token_eva').val(),
                id     :id			
            }
                  $.ajax({
                    url:baseurl+"act_qualification",
                    type: "POST",
                    data: data,
                    success: function (res) {
                  
                        swal({
                                title: "",
                                text: "Successfully activated",
                                type: "success",
                                showCancelButton: false,
                                dangerMode: false,
                                confirmButtonText: 'OK'
                            },function () {


                                $("#act_"+id).css("display","block");
                                $("#deact_"+id).css("display","none");
                                
                                $("#deactb_"+id).css("display","block");
                                
                                $("#actb_"+id).css("display","none");

                               // window.location = baseurl+"manage_qualification";
                            })
                      
                    }
                });
            });
            
    }




function editcollege(id,name){

    $("#modal-edit-college").modal('show');
    $('#editcollegeid').val(id)
    ;
    $('#editcolname').val(name);
    }

    function dCollege(id){
     
        swal({
            title: "Are you sure  you want to  deactivate?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, deactivate it!",
            closeOnConfirm: false,
            html: false
          }, function(){
             
            var data = {
                _token	:$('#token_eva').val(),
                id     :id			
            }
                  $.ajax({
                    url:baseurl+ "dact_college",
                    type: "POST",
                    data: data,
                    success: function (res) {
                  
                        swal({
                                title: "",
                                text: "Successfully deactivated",
                                type: "success",
                                showCancelButton: false,
                                dangerMode: false,
                                confirmButtonText: 'OK'
                            },function () {

                                $("#act_"+id).css("display","none");
                                $("#deact_"+id).css("display","block");

                                $("#deactb_"+id).css("display","none");

                                $("#actb_"+id).css("display","block");

                               // window.location = baseurl+"manage_college";
                            })
                      
                    }
                });
            });
            
    }
    function aCollege(id){
        //alert(id)
    ;
        swal({
            title: "Are you sure  you want to  activate?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, activate it!",
            closeOnConfirm: false,
            html: false
          }, function(){
             
            var data = {
                _token	:$('#token_eva').val(),
                id     :id			
            }
                  $.ajax({
                    url:baseurl+ "act_college",
                    type: "POST",
                    data: data,
                    success: function (res) {
                  
                        swal({
                                title: "",
                                text: "Successfully activated",
                                type: "success",
                                showCancelButton: false,
                                dangerMode: false,
                                confirmButtonText: 'OK'
                            },function () {

                                $("#act_"+id).css("display","block");
                                $("#deact_"+id).css("display","none");

                                $("#deactb_"+id).css("display","block");

                                $("#actb_"+id).css("display","none");
                                //window.location = baseurl+"manage_college";
                            })
                      
                    }
                });
            });
            
    }

function editSource(id,name){
    //alert(name);
    $("#modal-edit-source").modal('show');
    $('#editsourceid').val(id)
;
    $('#editsourcename').val(name);
    }

function dSource(id){
    //alert(id)
;
    swal({
		title: "Are you sure  you want to  deactivate?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, deactivate it!",
		closeOnConfirm: false,
		html: false
	  }, function(){
		 
        var data = {
            _token	:$('#token_eva').val(),
            id     :id			
        }
              $.ajax({
                url:baseurl+"dact_source",
                type: "POST",
                data: data,
                success: function (res) {
              
                    swal({
                            title: "",
                            text: "Successfully deactivated",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function () {

                            $("#act_"+id).css("display","none");
                            $("#deact_"+id).css("display","block");

                            $("#deactb_"+id).css("display","none");

                            $("#actb_"+id).css("display","block");

                        })
                  
                }
            });
        });
		
}
function aSource(id){
    //alert(id)
;
    swal({
		title: "Are you sure  you want to  activate?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, activate it!",
		closeOnConfirm: false,
		html: false
	  }, function(){
		 
		var data = {
            _token	:$('#token_eva').val(),
            id     :id			
        }
              $.ajax({
                url:baseurl+"act_source",
                type: "POST",
                data: data,
                success: function (res) {
              
                    swal({
                            title: "",
                            text: "Successfully activated",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function () {
                            $("#act_"+id).css("display","block");
                            $("#deact_"+id).css("display","none");

                            $("#deactb_"+id).css("display","block");

                            $("#actb_"+id).css("display","none");

                        })
                  
                }
            });
        });
		
}







function editTechnology(id,name){
    //alert(name);
    $("#modal-edit-technology").modal('show');
    $('#edittechid').val(id)
;
    $('#edittechname').val(name);
    }


    function dTechnology(id){
   
        swal({
            title: "Are you sure  you want to  deactivate?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, deactivate it!",
            closeOnConfirm: false,
            html: false
          }, function(){
             
            var data = {
                _token	:$('#token_eva').val(),
                id     :id			
            }
                  $.ajax({
                    url:baseurl+"dact_technology",
                    type: "POST",
                    data: data,
                    success: function (res) {
                  
                        swal({
                                title: "",
                                text: "Successfully deactivated",
                                type: "success",
                                showCancelButton: false,
                                dangerMode: false,
                                confirmButtonText: 'OK'
                            },function () {

                                $("#act_"+id).css("display","none");
                                $("#deact_"+id).css("display","block");
                                
                                $("#deactb_"+id).css("display","none");
                                
                                $("#actb_"+id).css("display","block");

                                //window.location = baseurl+"manage_technology";
                            })
                      
                    }
                });
            });
            
    }
    function aTechnology(id){
     
        swal({
            title: "Are you sure  you want to  activate?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, activate it!",
            closeOnConfirm: false,
            html: false
          }, function(){
             
            var data = {
                _token	:$('#token_eva').val(),
                id     :id			
            }
                  $.ajax({
                    url:baseurl+"act_technology",
                    type: "POST",
                    data: data,
                    success: function (res) {
                  
                        swal({
                                title: "",
                                text: "Successfully activated",
                                type: "success",
                                showCancelButton: false,
                                dangerMode: false,
                                confirmButtonText: 'OK'
                            },function () {

                                $("#act_"+id).css("display","block");
                                $("#deact_"+id).css("display","none");
                                
                                $("#deactb_"+id).css("display","block");
                                
                                $("#actb_"+id).css("display","none");
                            })
                      
                    }
                });
            });
            
    }



function aDesignation(id){

    swal({
		title: "Are you sure  you want to  activate?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, activate it!",
		closeOnConfirm: false,
		html: false
	  }, function(){
		 
        var data = {
            _token	:$('#token_eva').val(),
            id     :id			
        }
              $.ajax({
                url:baseurl+"act_designation",
                type: "POST",
                data: data,
                success: function (res) {
              
                    swal({
                            title: "",
                            text: "Successfully activated",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function () {

                            $("#act_"+id).css("display","block");
                            $("#deact_"+id).css("display","none");

                            $("#deactb_"+id).css("display","block");

                            $("#actb_"+id).css("display","none");
                            //window.location = baseurl+"manage_designation";
                        })
                  
                }
            });
        });
		
}


function dDesignation(id){
    //alert(id)
;
    swal({
		title: "Are you sure  you want to  deactivate?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, deactivate it!",
		closeOnConfirm: false,
		html: false
	  }, function(){
		 
        var data = {
            _token	:$('#token_eva').val(),
            id     :id			
        }
              $.ajax({
                url:baseurl+"dact_designation",
                type: "POST",
                data: data,
                success: function (res) {
              
                    swal({
                            title: "",
                            text: "Successfully deactivated",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function () {

                          
                            $("#act_"+id).css("display","none");
                            $("#deact_"+id).css("display","block");

                            $("#deactb_"+id).css("display","none");

                            $("#actb_"+id).css("display","block");





                            //window.location = baseurl+"manage_designation";
                        })
                  
                }
            });
        });
		
}


function editDesignation(id,name){
    //alert(name);
    $("#modal-edit-desig").modal('show');
    $('#editdesigid').val(id);

    $('#editdesigname').val(name);
    }

$(document).ready(function(){
        // Add more email input fields
       

    $("#addstudents").validate({
        rules: {
            "oldpay_date": {
                required: true
                         
            },
            "college": {
                required: true
                         
            },
            "start_date": {
                required: true          
            },
            
            
            "end_date": {
                required: true,
                     
            },
            "regfees": {
                required: true,
                     
            },
         
            "referenece_list": {
                required: true,
                     
            },
            "file": {
                required: true
                     
            },
           
            
            
        },
      
        submitHandler: function (form) { 
            form.submit();
            $(".pageloader").show();
    
        }
    });


    $("#bulk_company").validate({
        rules: {
            
            "company_file": {
                required: true          
            }
      
        },
      
        submitHandler: function (form) { 
            form.submit();
            $(".pageloader").show();
    
        }
    });


    $("#addothereference").validate({
        rules: {
            "refname": {
                required: true,
                nameval:true           
            },
            "refrelation": {
                required: true          
            },
            
            
            "phone": {
                required: true,
                number: true         
            },
            "email": {
                required: true,
                email :true         
            },
           
            
            
        },
      
        submitHandler: function (form) { 
            form.submit();
            $(".pageloader").show();
    
        }
    });


    

    $("#referenece").change(function() {

        var reference =this.value;

        
        if((reference !='')){
         
            $("#referenece_list").html('');

            var data = {
                _token	        :$('#token_eva').val(),
                referenece      :$("#referenece").val()			
            }
                  $.ajax({
                    url:baseurl + "reference_details",
                    type: "POST",
                    data: data,
                    success: function (res) {
                        console.log(res);  
                        var obj = jQuery.parseJSON(res); 
                        var count=obj.list.length;
                        var opt='';
                        opt+='option value="">Select Reference Name</option>';
                    for (i = 0; i < count; i++) {

                       var id = obj.list[i].id;
                       var name=capitalizeFirstLetter(obj.list[i].name);
                        opt+='<option value="'+id+'">'+name+'</option>';
                    }
                    $("#referenece_list").html(opt);

                    $("#ref_list").css('display', 'block')                 
                      
                    }
                });
        }
        else{
            $("#referenece_list").html('');
            $("#ref_list").css('display', 'none');
         
        }

    });

    $("#packfees").on("keyup", function(){
        var valid = /^\d{0,9}(\.\d{0,2})?$/.test(this.value),
            val = this.value;

            if(val!=''){

                $('label[for="packfee"]').hide();
            }
        
        if(!valid){
            console.log("Invalid input!");
            this.value = val.substring(0, val.length - 1);
        }
    });



    $("#packfee").on("keyup", function(){
        var valid = /^\d{0,9}(\.\d{0,2})?$/.test(this.value),
            val = this.value;

            if(val!=''){

                $('label[for="packfee"]').hide();
            }
        
        if(!valid){
            console.log("Invalid input!");
            this.value = val.substring(0, val.length - 1);
        }
    });

   
        $('#reduction_amount').on('keyup', function() {

          var inputValue = parseInt($(this).val());
          var minValue =1;
          var maxValue = parseInt($("#max_range").val());
      
          if (inputValue < minValue) {
            $(this).val(minValue);
          } else if (inputValue > maxValue) {
            // Remove the last digit to bring the value within the range
            $(this).val(function(_, value) {
              return value.slice(0, -1);
            });
          }
        });

     


        $('#regfees').on('keyup', function() {

            var inputValue = parseInt($(this).val());
            var minValue = 0;
            var maxValue = parseInt($("#finalamount").val());
        
            if (inputValue < minValue) {
              $(this).val(minValue);
            } else if (inputValue > maxValue) {
              // Remove the last digit to bring the value within the range
              $(this).val(function(_, value) {
                return value.slice(0, -1);
              });
            }
          });

          


          $("#inst_red").change(function() {

            if(this.val!=''){
                $('label[for="inst_red"]').hide();
            }
            else{
    
                $('label[for="inst_red"]').show();
            }
        });

          $("#package_reciept").change(function() {

            if(this.val!=''){
                $('label[for="package_reciept"]').hide();
            }
            else{
    
                $('label[for="package_reciept"]').show();
            }
        });


          $("#package_type").change(function() {

            if(this.val!=''){
                $('label[for="package_type"]').hide();
            }
            else{
    
                $('label[for="package_type"]').show();
            }
        });


        $("#file").change(function() {

            if(this.val!=''){
                $('label[for="file"]').hide();
            }
            else{
    
                $('label[for="file"]').show();
            }
        });



        $("#package_name").change(function() {

            if(this.val!=''){
                $('label[for="package_name"]').hide();
            }
            else{
    
                $('label[for="package_name"]').show();
            }
        });



        $("#candidate_file").change(function() {

            if(this.val!=''){
                $('label[for="candidate_file"]').hide();
            }
            else{
    
                $('label[for="candidate_file"]').show();
            }
        });
        $("#company_file").change(function() {

            if(this.val!=''){
                $('label[for="company_file"]').hide();
            }
            else{
    
                $('label[for="company_file"]').show();
            }
        });
 
    $("#emptype").change(function() {

        if(this.val!=''){
            $('label[for="emptype"]').hide();
        }
        else{

            $('label[for="emptype"]').show();
        }
    });

    $("#pac_type").change(function() {

        if(this.val!=''){

            $('label[for="pac_type"]').hide();

        }
        else{

            $('label[for="pac_type"]').show();

        }

    });



    $("#college").change(function() {

        if(this.val!=''){

            $('label[for="college"]').hide();

        }
        else{

            $('label[for="college"]').show();

        }

    });

    $("#referenece").change(function() {

        if(this.val!=''){

            $('label[for="referenece"]').hide();

        }
        else{

            $('label[for="referenece"]').show();

        }

    });





    $("#packname").change(function() {
        if(this.val!=''){
            $('label[for="packname"]').hide();
        }
        else{
            $('label[for="packname"]').show();
        }

    });


    $("#source").change(function() {
        if(this.val!=''){
            $('label[for="source"]').hide();
        }
        else{
            $('label[for="source"]').show();
        }

    });

    $("#designation").change(function() {

        if(this.val!=''){

            $('label[for="designation"]').hide();

        }
        else{

            $('label[for="designation"]').show();

        }

    });

    $("#technology").change(function() {

        if(this.val!=''){

            $('label[for="technology"]').hide();

        }
        else{

            $('label[for="technology"]').show();

        }

    });



    $("#packhrs").change(function() {

        if(this.val!=''){

            $('label[for="packhrs"]').hide();

        }
        else{

            $('label[for="packhrs"]').show();

        }

    });


    $("#technology").change(function() {

        if(this.val!=''){

            $('label[for="technology"]').hide();

        }
        else{

            $('label[for="technology"]').show();

        }

    });


    
    



    $("#duration").change(function() {

        if(this.val!=''){

            $('label[for="duration"]').hide();

        }
        else{

            $('label[for="duration"]').show();

        }

    });
    



   
    $("#reductioncheck").change(function() {
        if(this.checked) {
           
            $("#reductionvaldiv").css("display", "block");
            $("#reducedfees_div").css("display", "block");
            $("#redtaxdiv").css("display", "block");
            $("#afterreduction_div").css("display", "block");
            $("#ineffect_div").css("display", "block");
        }
        else{

            $("#reductionvaldiv").css("display", "none");
            $("#reducedfees_div").css("display", "none");
            $("#redtaxdiv").css("display", "none");
            $("#afterreduction_div").css("display", "none");
            $("#ineffect_div").css("display", "none");

        }
    });




});


jQuery.validator.addMethod("nameval", function(value, element) {
    //test user value with the regex
    return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
  }, "Enter only Albhabets");

  jQuery.validator.addMethod("letters_numbers_special", function(value, element) {
    return this.optional(element) || /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$/i.test(value);
  //(?=.*[a-zA-Z\d].*)[a-zA-Z\d!@#$%&*]
}, "Password should contain at least one digit, one lower case or upper case and  one special characters");
var folloupid ='';
function addpackage(contact_id,email){

    $("#contact_id").val(contact_id);
    $("#contact_email").val(email);
    $('#packname option[value=""]').prop("selected", true);   
                $("#packfee").val('');
                $("#packtax").val('');
                $("#packtot").val('');

                $("#costdiv").css("display", "none");
                $("#taxdiv").css("display", "none");
                $("#totaldiv").css("display", "none");
                $("#reductiondiv").css("display", "none");


                $("#reduction_amount").val();
                $("#reducedfees").val('');
                $("#reducedtax").val('');
                $("#after_reduction").val('');
                $("#ineffect_offered").val('');


                $("#reductiondiv").css("display", "none");
                $("#reductionvaldiv").css("display", "none");
                $("#reducedfees_div").css("display", "none");
                $("#redtaxdiv").css("display", "none");
                $("#afterreduction_div").css("display", "none");
                $("#ineffect_div").css("display", "none");

                $('#myModal').modal({backdrop: 'static', keyboard: false})  




                $("#modal-package").modal('show');
}
function addnew(){

    $("#modal-package").modal('show'); 
}
function check(){


    var data = {
        _token	:$('#token').val(),
        no     :cno1		
    }
    $.ajax({
        url: baseurl + 'checkno',
        type: "POST",
        data: data,
        success: function (res) {

            if(res==0){
                swal("", "This Contacts Already Exist", "error");

            }
      
           
          
        }
    });
   
}

function activate(userid){
	swal({
		title: "Are you sure  you want to  Activate?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, Activate it!",
		closeOnConfirm: false,
		html: false
	  }, function(){
        $(".pageloader").show();
		  var data = {
				  _token	:$('#token').val(),
				  id     :userid			
			  }


			  $.ajax({
				  url: baseurl + 'activate_user',
				  type: "POST",
				  data: data,
				  success: function (res) {

                    $(".pageloader").hide();
				
					  swal({
							  title: "",
							  text: "Successfully Activated",
							  type: "success",
							  showCancelButton: false,
							  dangerMode: false,
							  confirmButtonText: 'OK'
						  },function () {

                            $("#act_"+userid).css("display","block");
                            $("#deact_"+userid).css("display","none");

                            $("#deactb_"+userid).css("display","block");
                            $("#actb_"+userid).css("display","none");
                        
                       
						  })
					
				  }
			  });
		  });
}
function deactivate(userid){
	swal({
		title: "Are you sure  you want to  Deactivate?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, Deactivate it!",
		closeOnConfirm: false,
		html: false
	  }, function(){

        $(".pageloader").show();
		 
		  var data = {
				  _token	:$('#token').val(),
				  id     :userid			
			  }
			  $.ajax({
				  url: baseurl + 'deactivate_user',
				  type: "POST",
				  data: data,
				  success: function (res) {
                    $(".pageloader").hide();
					  swal({
							  title: "",
							  text: "Successfully Deactivated",
							  type: "success",
							  showCancelButton: false,
							  dangerMode: false,
							  confirmButtonText: 'OK'
						  },function () {

                            $("#act_"+userid).css("display","none");
                            $("#deact_"+userid).css("display","block");

                            $("#deactb_"+userid).css("display","none");
                            $("#actb_"+userid).css("display","block");
                        

      
						  })
					
				  }
			  });
		  });
}

function closed_contacts(id,email){

    $("#contactid_close").val(id);
    $("#email").val(email);
    $("#modal-closecontact").modal('show');  
}
function history(id){

    data = {
        _token	:$('#token').val(),
        id     :id			
    }
    $.ajax({
        url: baseurl + 'followup_history',
        type: "POST",
        data: data,
        success: function (res) {

            $("#history").html('');
            var obj = jQuery.parseJSON(res); 

            console.log(res);

          

            var count=obj.history.length;

          
			if(count!=0){
                var opt='';

				for (i = 0; i < count; i++) {

                    opt+='<p>'+obj.history[i].follow_date+'</p>';
                    opt+='<p>'+obj.history[i].comments+'</p>';
                    opt+='<hr>';

				}
                $("#history").append(opt);
               

			}
           
            $("#modal-history").modal('show');   

        }
    });
}
function changefollowup(id){
        data = {
        _token	:$('#token').val(),
        id     :id,
        type:'contact'			
    }
    $.ajax({
        url: baseurl + 'contact_details',
        type: "POST",
        data: data,
        success: function (res) {
            console.log(res);
            $("#contactdetails").html('');
            var obj = jQuery.parseJSON(res);
            
            var dateInYMD = obj.contactdetails.followup_date;
            var dateInDMY = formatDateYMDtoDMY(dateInYMD);


            $("#folloupdate_edit").val(dateInDMY);
            $("#comments_edit").val(obj.contactdetails.followup_comments);
            $("#contactid_update").val(id);
            $("#modal-followchange").modal('show');   

        }
    });

}
function studentdetails(id){
   

    $("#student_package_id").val(id);

    $("#stud_dob").val($("#dojdate_"+id).val());
    $("#modal-changedate").modal('show');

}
function followup(id){

    $("#contactid").val(id);
    $("#modal-follow").modal('show');

}

function contactdetails_contact(id){
  

    var data = {
        _token	:$('#token').val(),
        id     :id,
        type:'contact'			
    }
    $.ajax({
        url: baseurl + 'contact_details',
        type: "POST",
        data: data,
        success: function (res) {
            $("#contactdetails").html('');
            var obj = jQuery.parseJSON(res);
            var opt='';


            opt +='<p>Contact Number 1 :<span>'+ obj.contactdetails.contact1+'</span></p>';
            if(obj.contactdetails.contact2!=null){
                opt +='<p>Contact Number 2 :<span>'+obj.contactdetails.contact2+'</span></p>';
            }
            if(obj.contactdetails.source!=null){
                opt +='<p>Source :<span>'+obj.contactdetails.source+'</span></p>';
            }
            if(obj.contactdetails.name!=null){
                opt +='<p>Name :<span>'+obj.contactdetails.name+'</span></p>';
            }    
            if(obj.contactdetails.email!=null){
                opt +='<p>Email :<span>'+obj.contactdetails.email+'</span></p>';
            }
            if(obj.contactdetails.college!=null){
                opt +='<p>College :<span>'+obj.contactdetails.college+'</span></p>';
            }
            if(obj.contactdetails.qualification!=null){
                opt +='<p>Qualification :<span>'+obj.contactdetails.qualification+'</span></p>';
            }
           
            if(obj.contactdetails.year!=null){
                opt +='<p>Year:<span>'+obj.contactdetails.year+'</span></p>';
            }
            $("#contactdetails").append(opt);
        }
    });
  
    $("#modal-contact").modal('show');
   
}
function contactdetails(id){
  

    var data = {
        _token	:$('#token').val(),
        id     :id			
    }
    $.ajax({
        url: baseurl + 'contact_details',
        type: "POST",
        data: data,
        success: function (res) {
            $("#contactdetails").html('');
            var obj = jQuery.parseJSON(res);
            var opt='';


            opt +='<p>Contact Number 1 :<span>'+ obj.contactdetails.phone+'</span></p>';
            if(obj.contactdetails.contact2!=null){
                opt +='<p>Contact Number 2 :<span>'+obj.contactdetails.phone+'</span></p>';
            }
            if(obj.contactdetails.source!=null){
                opt +='<p>Source :<span>'+obj.contactdetails.source+'</span></p>';
            }
            if(obj.contactdetails.name!=null){
                opt +='<p>Name :<span>'+obj.contactdetails.name+'</span></p>';
            }    
            if(obj.contactdetails.email!=null){
                opt +='<p>Email :<span>'+obj.contactdetails.email+'</span></p>';
            }
            if(obj.contactdetails.college!=null){
                opt +='<p>College :<span>'+obj.contactdetails.college+'</span></p>';
            }
            if(obj.contactdetails.qualification!=null){
                opt +='<p>Qualification :<span>'+obj.contactdetails.qualification+'</span></p>';
            }
           
            if(obj.contactdetails.year!=null){
                opt +='<p>Year:<span>'+obj.contactdetails.year+'</span></p>';
            }
            $("#contactdetails").append(opt);
        }
    });
  
    $("#modal-contact").modal('show');
   
}


function deactivate_company(id){

    swal({
        title: "Are you sure  you want to  deactivate?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, deactivate it!",
        closeOnConfirm: false,
        html: false
      }, function(){     
        var data = {
            _token  :$('#token_eva').val(),
            id     :id          
        }
              $.ajax({
                url:baseurl + "deactivate_company",
                type: "POST",
                data: data,
                success: function (res) {
              
                    swal({
                            title: "",
                            text: "Successfully deactivated",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function () {


                            $("#act_"+id).css("display","none");
                            $("#deact_"+id).css("display","block");
                            
                            $("#deactb_"+id).css("display","none");
                            
                            $("#actb_"+id).css("display","block");



                            
                            //window.location = baseurl+"manage_company";
                        })
                  
                }
            });
        });
        
    }


    function activate_company(id){

        swal({
            title: "Are you sure  you want to  activate?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, activate it!",
            closeOnConfirm: false,
            html: false
          }, function(){     
            var data = {
                _token  :$('#token_eva').val(),
                id     :id          
            }
                  $.ajax({
                    url:baseurl + "activate_company",
                    type: "POST",
                    data: data,
                    success: function (res) {
                  
                        swal({
                                title: "",
                                text: "Successfully Activated",
                                type: "success",
                                showCancelButton: false,
                                dangerMode: false,
                                confirmButtonText: 'OK'
                            },function () {
                                
                                //window.location = baseurl+"manage_company";


                                $("#act_"+id).css("display","block");
                                $("#deact_"+id).css("display","none");
                                
                                $("#deactb_"+id).css("display","block");
                                
                                $("#actb_"+id).css("display","none");
    
                            })
                      
                    }
                });
            });
            
        }
    

function editdesignation(){

    var data = {
        _token	:$('#token_eva').val(),
        id     :id			
    }
    $.ajax({
        url: baseurl + 'edit_designation',
        type: "POST",
        data: data,
        success: function (res) {
      
            
          
        }
    });

    $("#designation_edit").modal('show');
}

function deletedesignation(id){
	swal({
		title: "Are you sure  you want to  delete?",
		text: "",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, delete it!",
		closeOnConfirm: false,
		html: false
	  }, function(){
		 
		  var data = {
				  _token	:$('#token_eva').val(),
				  id     :id			
			  }
			  $.ajax({
				  url: baseurl + 'delete_designation',
				  type: "POST",
				  data: data,
				  success: function (res) {
				
					  swal({
							  title: "",
							  text: "Successfully deleteed",
							  type: "success",
							  showCancelButton: false,
							  dangerMode: false,
							  confirmButtonText: 'OK'
						  },function () {
							  window.location = baseurl + 'manage_designation';
						  })
					
				  }
			  });
		  });
}

jQuery.validator.addMethod("checkForDuplicate", function (value, element) {
    var textValues = [];
        //initially mark as valid state
        valid = true;
    $(".contactnum").each(function () {

        if ($(this).val() !== "") {
            var doesExisit = ($.inArray($(this).val(), textValues) === -1) ? false : true;
            if (doesExisit === false) {
                //console.log("adding the values to array");
                textValues.push($(this).val());
                //console.log(textValues);
            } else {
                //console.log(textValues);
                //update the state as invalid
                valid = false;
                return false;
            }
        }

    });
    //return the valid state
    return valid;
}, 'Contact Number exist');

$(document).ready(function () {

    //hremail
    
    let emailcount =  parseInt($('#email-container').data('email-count'))-1 || 0;// Initial count based on existing items
    emailcount++;
        // Add new pre-requisite textbox
        $('#addMail').click(function() {
            
            emailcount++; // Increment the counter
            $('#email-container').append(`<div class="email-group"><div class="mb-3">
                      <input type="email" class="form-control pre_req_input" name="emails[]"  id="emails${emailcount}" placeholder="Enter email" >
                      <button type="button" class="remove-email btn btn-danger remove-email">Remove</button>
                  </div>
            `);

            // Apply validation rules to the newly added field
         // Apply validation to the newly added field
         
    });


    $("#addcompany").validate({
        rules: {
            "name": {  
                required: true,
            }, 
            "cpname": {
                required: true,
               
               
            },
            "cnumber": {
                required: true,
                checkForDuplicate: true,
                digits: true
               
            },
            "cemail": {  
                required: true,
                email: true    
            },
            "file": {  
                required: true,
               
            },     
        },
 
        submitHandler: function (form) {
            var formData = new FormData($("#addcompany")[0]);
            $(".pageloader").hide();
            $.ajax({
             url:baseurl + "save_company",
                       type: "POST",
                       data: formData,
                       contentType: false,
                       processData: false,
                       success: function (res) {

            if(res==1){
                swal("", "This Contacts Number Already Exist", "error");
            }
            else if(res==2){
                swal("", "This Email Already Exist", "error");
            }
            else if(res==3){

                $("#submit").css("display", "none");

                        swal({
                            title: "",
                            text: "Successfully saved",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function () {
                            window.location = baseurl+"manage_company";

                        })

                        }          
                    }
                 });
                    }
        });


    $("#editcompany").validate({
        rules: {
            "name": {  
                required: true,
            }, 
            "cpname": {
                required: true,
               
               
            },
            "cnumber": {
                required: true,
                checkForDuplicate: true,
                digits: true
               
            },
            "cemail": {  
                required: true,
                email: true    
            },
                
        },
      
        submitHandler: function (form) {
            $(".pageloader").show();
            form.submit();
        }
    });
    







    $("#searchchrys_form").validate({
        rules: {
            "packname": {
                required: true,
                
            },
            "packtype": {
                required: true,
                
            },
            "start_date1": {
                required: true,
                
            },
            "end_date1": {
                required: true,
                
            }
        },
      
        submitHandler: function (form) { 
            $(".pageloader").show();
            form.submit();
        }
    });






    $("#addsrishtians").validate({
        rules: {
            "empname": {
                required: true,
                nameval:true           
            },
            "empid": {
                required: true          
            },
            
            
            "phone": {
                required: true,
                number: true         
            },
            "email": {
                required: true,
                email :true         
            },
           
            
            
        },
      
        submitHandler: function (form) { 
            form.submit();
            $(".pageloader").show();
    
       
    
        }
    });





//Admissioin form Validation
    var max_range = '';

    /*$("#cno1").blur(function() {

        if($("#cno1").val()!=''){

        var data = {
            _token	:$('#token').val(),
            no     :$("#cno1").val()	
        }
        $.ajax({
            url: baseurl + 'checkno',
            type: "POST",
            data: data,
            success: function (res) {
    
                if(res==0){
                    swal("", "This Contacts Already Exist", "error");
    
                }
          
            }
        });
    }
   
    });*/


    $('#to_date').Zebra_DatePicker({
        format: 'd-m-Y',
        direction: [true, 45] 
    });

 $('#edittodate').Zebra_DatePicker({
        format: 'd-m-Y',
        
      });

 

 $('#search_date1').Zebra_DatePicker({
        format: 'd-m-Y',
        direction: [sdate,edate]
       
      
    });

    $('#search_date2').Zebra_DatePicker({
        format: 'd-m-Y',
        direction: [sdate,edate],
        
      
    });
    


    $('#oldpay_date').Zebra_DatePicker({
        format: 'd-m-Y',
      
    });
    $('#paydate').Zebra_DatePicker({
        format: 'd-m-Y',
      
    });

     $('#start_date').Zebra_DatePicker({
        format: 'd-m-Y',
      
    });

    $('#folloupdate').Zebra_DatePicker({
        format: 'd-m-Y',
        direction:1
    });

    
    $('#folloupdate_edit').Zebra_DatePicker({
        format: 'd-m-Y',
        direction:1
    });
    $('#stud_dob').Zebra_DatePicker({
        format: 'd-m-Y',
      
    });
    $('#stud_doj').Zebra_DatePicker({
        format: 'd-m-Y',
        direction:1
      
    });

    $('#start_date1ch').Zebra_DatePicker({
        format: 'd-m-Y', 
       
        pair: $('#end_date1')
    });

    $('#start_date1').Zebra_DatePicker({
        format: 'd-m-Y', 
        direction: [$("#edate").val(), false],
        pair: $('#end_date1')
    });






    $('#end_date1').Zebra_DatePicker({
        format: 'd-m-Y',
        direction:true
       
      
    });

    $('#cdate1').Zebra_DatePicker({
        format: 'd-m-Y',
        direction: true,
        pair: $('#cdate2')
      
    });

    $('#cdate2').Zebra_DatePicker({
        format: 'd-m-Y',
        direction: true
       
      
    });

    $('#start_date').Zebra_DatePicker({
        format: 'd-m-Y',
        
        pair: $('#end_date')
      
    });

    $('#end_date').Zebra_DatePicker({
        format: 'd-m-Y',
        direction:true
      
       
      
    });

    $("#searchchrysalis").validate({
        rules: {
            "search_date1": {
                required: true,
                
            },
            "search_date2": {
                required: true          
            },
     
        },
      
        submitHandler: function (form) { 
            form.submit();
            $(".pageloader").show();
    
       
    
        }
    });



    $("#specialization_form").validate({
        rules: {
            "specialization": {
                required: true
               
            },
             
        },
      
        submitHandler: function (form) {
            form.submit();
        }
    });

    $("#edit_specialization").validate({
        rules: {
            "editspecname": {
                required: true
               
            },
             
        },
      
        submitHandler: function (form) {
            form.submit();
        }
    });

    

//updatefollow

$("#updatefollow").validate({
    rules: {
        "folloupdate_edit": {  
            required: true,
        }, 
        "comments_edit": {
            required: true,
            
           
        },          
    },  
    submitHandler: function (form) {
        $(".pageloader").show();
        form.submit();
    }
});

//followup
$("#followup").validate({
    rules: {
        "folloupdate": {  
            required: true,
        }, 
        "comments": {
            required: true,
            
           
        },          
    },  
    submitHandler: function (form) {
        $(".pageloader").show();
        form.submit();
    }
});


$("#packageTax").validate({
    rules: {
        "tax": {  
            required: true,
        }, 
        
       
    },
  
    submitHandler: function (form) {
        $(".pageloader").show();
        form.submit();
    }
});

//Add contacts

// Custom method to validate digits avoiding leading zeros
$.validator.addMethod("noLeadingZeros", function(value, element) {
    return this.optional(element) || /^[1-9]\d*$/.test(value);
}, "Please enter a valid number without leading zeros");



    $("#addcontacts").validate({
        rules: {
            "source": {  
                required: true,
            }, 
            "name": {  
                nameval:true
            },

            "cno1": {
                required: true,
                
                digits: true,
                noLeadingZeros: true
               
            },
            "cno2": {
                
                checkForDuplicate: true,
                digits: true,
                noLeadingZeros: true
               
            },
            "email": {  
                email: true    
            },    
        },
      
        submitHandler: function (form) {

            $(".pageloader").show();
            var formData = new FormData($("#addcontacts")[0]);
            $.ajax({
             url:baseurl + "save_contacts",
                       type: "POST",
                       data: formData,
                       contentType: false,
                       processData: false,
                       success: function (res) {
                        $(".pageloader").hide();
                if(res==1){
                    swal("", "This Contact Number Already Exist", "error");
                }
                else if(res==3){

                    swal("", "This Email Id Already Exist", "error");

                }
                else if(res==2){
                        swal({
                            title: "",
                            text: "Successfully Saved",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function (res) {
            
                            window.location = baseurl+"add_contacts";
                           
                        })

                    }
                    }
                });
            
        }
    });




    $("#paymentreject").validate({
        rules: {
            "reason": {  
                required: true,
            }, 
            "reason_type": {  
                required: true,
            },
        },
        submitHandler: function (form,e) {
            
        e.preventDefault();
        $("#modal-payreject").modal('hide');
        $(".pageloader").show();
      
        var formData = new FormData($("#paymentreject")[0]);
         console.log(formData);
        var id= $("#rowid").val()
         $(".pageloader").hide();
              $.ajax({
                url:baseurl + "accountsreject",
                type: "POST",
               
                data: formData,
                contentType: false,
                processData: false,
                success: function (res) {
                   
                   swal({
                            title: "",
                            text: "Successfully rejected",
                            type: "success",
                            showCancelButton: false,
                            dangerMode: false,
                            confirmButtonText: 'OK'
                        },function () {
                            $("#paymentreject")[0].reset();
                            var table = $('#example').DataTable();
                            var row = $('#row_'+id).closest('tr');
                             table.row(row).remove().draw();
                           
                        })
                  
                }
            });
    
    
    
            //form.submit();
        }
    });

     //Source
 
     $("#source_form").validate({
        rules: {
            "source": {
                required: true
               
            },
             
        },
      
        submitHandler: function (form) {
            form.submit();
        }
    });

    $("#editsource").validate({
        rules: {
            "editsourcename": {
                required: true
               
            },
             
        },
      
        submitHandler: function (form) {
            form.submit();
        }
    });

     //college
 
     $("#college_form").validate({
        rules: {
            "college": {
                required: true
               
            },
             
        },
      
        submitHandler: function (form) {
            form.submit();
        }
    });

    $("#editcollege").validate({
        rules: {
            "editcolname": {
                required: true
               
            },
             
        },
      
        submitHandler: function (form) {
            form.submit();
        }
    });

      //qualification
 
      $("#qualification_form").validate({
        rules: {
            "qualification": {
                required: true
               
            },
             
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    $("#editqualification").validate({
        rules: {
            "editqualiname": {
                required: true
               
            },
             
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
  //Technology
 
     $("#technology_form").validate({
         rules: {
             "technology": {
                 required: true
                
             },
              
         },
         submitHandler: function (form) {
             form.submit();
         }
     });
     $("#edittechnology").validate({
        rules: {
            "edittechname": {
                required: true       
            },           
        },     
        submitHandler: function (form) {
            form.submit();
        }
    });

//Designation
 
$("#designationform").validate({
    rules: {
        "designation": {
            required: true          
        },        
    },
  
    submitHandler: function (form) { 
        form.submit();
    }
});


$("#editdesig").validate({
    rules: {
        "editdesigname": {
            required: true          
        },        
    },
  
    submitHandler: function (form) { 
        form.submit();
    }
});

//Edit Employee

$("#editemployee").validate({
    rules: {
        "empname": {
            required: true,
            nameval:true           
        },
        "office_email": {
            required: true,
            email:true,         
        },
        "designation": {
            required: true          
        },
        "technology[]": {
            required: true          
        },
        "experience": {
            required: true          
        },

        
        "phone": {
            required: true,
            digits: true,
            noLeadingZeros: true     
        },
                          
    },
    errorPlacement: function(error, element) {
         
        error.appendTo(element.parent());     
  },
  
    submitHandler: function (form) { 
        form.submit();
    }
});

//Addemployee
 
$("#addemployee").validate({
    rules: {
        "emptype": {
            required: true          
        }, 
        "empid": {
            required: true          
        },
        "empname": {
            required: true,
            nameval:true           
        },
        "office_email": {
            required: true,
            email:true,         
        },
        "designation": {
            required: true          
        },
        "technology[]": {
            required: true          
        },
        "experience": {
            required: true          
        },

        
        "phone": {
            required: true,
            digits: true,
            noLeadingZeros: true       
        },
        "email": {
            required: true,
            email :true         
        },
        "password": {
            required: true,
            letters_numbers_special:true          
        },
        
     
    },

    errorPlacement: function(error, element) {
         
        error.appendTo(element.parent());     
  },
  
    submitHandler: function (form) { 
        form.submit();
        $(".pageloader").show();

   

    }
});

$("#package").validate({
    rules: {
        "packname": {
            required: true
                   
        },
        "packcourseid": {
            required: true        
        },
        "duration": {
            required: true        
        },
        "packhrs": {
            required: true        
        },
        "packpro": {
            required: true        
        },
        "packpre":{
            required: true
        },
        "technology": {
            required: true          
        },
        "packfee": {
            required: true
           
    
        },
        "pac_type": {
            required: true
           

        },
        
                          
    },

    errorPlacement: function(error, element) {
         
        error.appendTo(element.parent());     
  },
  
    submitHandler: function (form) { 
        $(".pageloader").show();
        form.submit();
    }
});


let preReqCount = 1;// Initial count based on existing items
    
        // Add new pre-requisite textbox
        $('#addRequisite').click(function() {
            
            preReqCount++; // Increment the counter
            $('#text-box-container').append(`<div class="prereq-group" >
                    <div class="mb-3">
                        <input type="text" class="form-control pre_req_input" name="pack_pre[]" id="pack_pre_${preReqCount}" placeholder="Pre-requisite">
                        <button type="button" class="btn btn-danger remove-requisite">Remove</button>
                    </div>
                </div>
            `);

           
    });
    
        // Remove a pre-requisite textbox
        $(document).on('click', '.remove-requisite', function() {
            $(this).closest('.prereq-group').remove();
        });



$("#editpackage").validate({
    rules: {
        "packname": {
            required: true
                   
        },
        "packcourseid": {
            required: true        
        },
        "duration": {
            required: true        
        },
        "packhrs": {
            required: true        
        },
        "packpro": {
            required: true        
        },
        "pack_pre[]":{
            required: true
        },
        "technology": {
            required: true          
        },
        "packfee": {
            required: true
                   
        },
        "packtax": {
            required: true          
        },

        "packtot": {
            required: true          
        },
                          
    },
    messages: {
        "packname": {
            required: "This field required",
        },
        "packcourseid": {
            required: "This field required",
        },
        "duration": {
            required: "This field required",
        },
        "packhrs": {
            required: "This field required",
        },
        "packpro": {
            required: "This field required",
        },
        "pack_pre[]": {
            required: "This field required",
        },
        "technology": {
            required: "This field required",
        },
        "packfee": {
            required: "This field required",
        },
        "packtax": {
            required: "This field required",
        },
        "packtot": {
            required: "This field required",
        },
    },
    errorPlacement: function(error, element) {
      
        if (element.attr("name") == "pack_pre[]") {
            // Place the error message just below the input field
            error.insertAfter(element); 
        } else {
            // For all other inputs, append error to the parent element
            error.appendTo(element.parent());
        }



    },
    highlight: function(element) {
        $(element).addClass("error"); // Add error class to the input field
        $(element).siblings("label.error").show(); // Show the error label
    },
    unhighlight: function(element) {
        $(element).removeClass("error"); // Remove error class when valid
        $(element).siblings("label.error").hide(); // Hide the error label
    }
});



/*validation End*/
/*onfunction*/

$("#packfees").blur(function() {

    let fee=$("input[name='packfee']").val();
    let tax=$("#defaulttax").val();

 //   if(tax=='18%'){
        let fees = Math.round(fee);
        let cal_tax=Math.round((fee*tax)/100);
      let  total=fees+cal_tax;
      if(isNaN(total)){
       // $('0').val(total); 
       total=0;
      }
      $('#packtax').val(cal_tax);
      $('#packtot').val(total); 
 /*   }
    else if(tax=='15%'){
        let fees = parseInt(fee);
     let   cal_tax=(fee*15)/100;
      let  total=fees+cal_tax;
      $('#packtot').val(total); 
    }*/
   // alert(tax);
});


  });

