<?php

namespace App\Helpers;

use App\Models\Employeeinfo;
use DB;
use DateTime;
use App\Models\User;
use App\Models\Event;
use App\Models\Package;
use App\Models\Reporting;
use App\Models\Studentpackage;
use App\Models\Technology;
use App\Models\Payment;
use App\Models\Employeepriv;
use App\Models\Gatepass;
use App\Models\College;
use App\Models\Salespayment;
use App\Models\Collegeupdates;
use App\Models\Collegedep;

use App\Models\BatchplanStudents;


use App\Models\Company;
use Carbon\Carbon;
use Auth;

class CustomHelper
{

	public static function getTrainerNameBP($studentpackageid) {
        return Studentpackage::join('ev_student_trainer', 'ev_student_trainer.studentpackageid', '=', 'ev_student_package.id')
            ->join('users', 'users.id', '=', 'ev_student_trainer.trainer_id')
            ->where('ev_student_package.id', $studentpackageid)
            ->pluck('users.name') 
            ->implode(', ');
    }	




	public static function updatedBatchplanTopics($student_id, $topic_id)
{
    $topic = BatchplanStudents::where('student_id', $student_id)
        ->where('topic_id', $topic_id)
        ->first();

    if ($topic) {
        $greatestEndDate = $topic->end_date;
        $earliestStartDate = $topic->start_date;

        $subtopics = BatchplanStudents::where('student_id', $student_id)
            ->where('topic_id', $topic_id)
            ->get();

        foreach ($subtopics as $subtopic) {
            $subtopicData = CustomHelper::updatedBatchplanSubtopics($student_id, $topic_id, $subtopic->subtopic_id);

            if ($subtopicData) {
                if ($subtopicData['end_date'] && (!$greatestEndDate || strtotime($subtopicData['end_date']) > strtotime($greatestEndDate))) {
                    $greatestEndDate = $subtopicData['end_date'];
                }

                if ($subtopicData['start_date'] && (!$earliestStartDate || strtotime($subtopicData['start_date']) < strtotime($earliestStartDate))) {
                    $earliestStartDate = $subtopicData['start_date'];
                }
            }
        }

        return [
            'start_date' => $earliestStartDate,
            'end_date' => $greatestEndDate,
            'comments' => $topic->comments ?? '',
        ];
    }

    return null;
}
public static function updatedBatchplanSubtopics($student_id, $topic_id, $subtopic_id = null)
{
	$query = BatchplanStudents::where('student_id', $student_id)
		->where('topic_id', $topic_id);

	if ($subtopic_id) {
		$query->where('subtopic_id', $subtopic_id);
	}

	$subtopic = $query->first();

	if ($subtopic) {
		return [
			'start_date' => $subtopic->start_date,
			'end_date' => $subtopic->end_date,
			'comments' => $subtopic->comments ?? '',
		];
	}

	return null;
}

	public static function ClaimCheck($collegeId)
	{
	  
		$deptIds1 = Collegeupdates::where('college_id', $collegeId)
		->where('sales_id', '!=', Auth::user()->id)
		->where(function ($query) {
			$query->whereRaw("DATE_ADD(date, INTERVAL 1 MONTH) >= CURDATE()")
				  ->orWhere(function ($subQuery) {
					  $subQuery->where('end_date', '>', now())
							   ->whereNotNull('end_date');
				  });
		})
		->pluck('department')
		->toArray();
	
		$deptIds2 = Collegeupdates::where('college_id', $collegeId)
		->where('sales_id', '=', Auth::user()->id)
		->where(function ($query) {
			$query->whereRaw("DATE_ADD(date, INTERVAL 1 MONTH) >= CURDATE()")
				  ->orWhere(function ($subQuery) {
					  $subQuery->where('end_date', '>', now())
							   ->whereNotNull('end_date');
				  });
		})
		->pluck('department')
		->toArray();
		$deptIds = array_diff($deptIds1,$deptIds2); 
	
		$departments = Collegedep::join('ev_department', 'ev_department.id', '=', 'ev_collegedep.department')
						->where('ev_collegedep.college_id', $collegeId)
						->where('ev_collegedep.status',1)
						->whereIn('ev_collegedep.department', $deptIds)
						->distinct()
						->pluck('ev_department.department');
		if ($departments->isEmpty()) {
							
			return 0;
		} else {
							
			return 1;
		}
			

			

	}

	public static function enddate($pac_name, $startdate) {

		$pac_duration = Package::where('pac_name', $pac_name)->value('duration');
		$startdate = trim($startdate);
	
		$date = DateTime::createFromFormat('Y-m-d', $startdate);
	
		if ($date && $pac_duration) {
			$date->modify("+{$pac_duration} months");
			
			return $date->format('d-m-Y');
		}
	
		return null;
	}

	public static function collegeName($colId)

	{

		$colName = College::join('ev_district','ev_district.id','=','ev_college.district')
		->where('ev_college.id',$colId)
							->first(['ev_college.college','ev_district.name']);
		return $colName;
	}


	public static function reissueDate($userid)
	{

		$reissued = Gatepass::where('ev_gatepassrequest.status', '4')
							->where('ev_gatepassrequest.reissue_status','1' )
							->where('ev_gatepassrequest.user_id',$userid )
							->orderBy('ev_gatepassrequest.id', 'desc')
							->first(['ev_gatepassrequest.requested_date as reissued']);

		;
		return $reissued;
	}

	public static function Company()
	{
		$companies = Company::where('status', 1)->orderBy('id', 'desc')->get(['id', 'name']);
		return $companies;
	}


	public static function daycount($endDate)
    {
        $endDate = is_string($endDate) ? Carbon::parse($endDate) : $endDate;
        $today = Carbon::today();
		$diffInDays = $today->diffInDays($endDate, false);
        return $diffInDays + 1; 
    }


	public static function paymentcompleted($id,$pac_id,$pac_type,$total)
    {
        $sum = Payment::where('student_id', $id)
		->where('package_type', $pac_type)
		->where('package_id', $pac_id)
		->sum('paid_amount');
		$pending_amount = $total-$sum;
		if($pending_amount == 0){
			return '<span style="color: green; font-weight: bold;">Fees Completed</span>';
		}
		return '<span style="color: red; font-weight: bold;">Fees Pending</span>';
       // return 'No names found'; // Default message if no names are found
    }

	public static function paidfees_student($studentid, $pac_type, $pac_id){

		$data['paidfees'] = Payment::where('student_id', $studentid)
									->where('package_type', $pac_type)
									->where('package_id', $pac_id)
									->sum('paid_amount');
		return $data;
	}
	public static function admissionstatus($studentid)

	{
		$data['admission_status']=User::find($studentid)->admission_completed;
		return $data;		
	}

	public static function employeeprivillages($empid)
	{
		$data['employeeprivillege']=Employeepriv::where('empid',$empid)->first(['add_employee']);
		return $data;		
	}

	public static function regularcount($studentid)
	{
		$count=Studentpackage::where('student_id',$studentid)->where('package_type',1)->count();
		return $count;		
	}


	public static function tech($techid)

	{


		$technology    = Technology::where('id',$techid)->where('status',1)->first(['technology']);
		

		return $technology;

		
	}


	public static function technology($userid)

	{


		$data['reportedlist']           =Studentpackage::join("ev_package","ev_package.pac_id", "=","ev_student_package.package_id")
														->join("ev_technology","ev_technology.id", "=","ev_package.tech_id")
														->where('ev_student_package.student_id',$userid)
														->where('ev_student_package.package_type',1)
														->get(['ev_technology.technology']);
		
		$allNames = '';

		if(!empty($data['reportedlist'] )){
			foreach($data['reportedlist'] as $list){

				$allNames .= $list->technology . ', ';

			}

			return $allNames = rtrim($allNames, ', ');

		}
		
	}


	public static function empnames($payment_id)
    {
        $emplist = Salespayment::join("users", "users.id", "=", "sales_payment.sales_id")
                                ->where('sales_payment.payment_id', $payment_id)
                                ->get(['users.name']);

        $allNames = '';
        if ($emplist->isNotEmpty()) {
            foreach ($emplist as $list) {
                $allNames .= $list->name . ', ';
            }
            return rtrim($allNames, ', ');
        }

        return 'No names found'; // Default message if no names are found
    }

	public static function username($userid)

	{


		$data['reportedlist']           =Reporting::join("users","users.id", "=","ev_reporting.reported_to")
											->where('ev_reporting.employee_id',$userid)->get(['users.id','users.name']);

		$allNames = '';

		if(!empty($data['reportedlist'] )){
			foreach($data['reportedlist'] as $list){

				$allNames .= $list->name . ', ';

			}

			return $allNames = rtrim($allNames, ', ');

		}
		
	}

	public static function ucode($userid)

	{


		$code           =Employeeinfo::where('user_id',$userid)->first(['poc_code']);

		return $code;

		
	}

	public static function uname($userid)

	{


		$name           =User::where('id',$userid)->first(['name']);

		return $name;

		
	}
	public static function poc_code($userid)

	{


		$poc_code           =User::where('id',$userid)->first(['poc_code']);

		return $poc_code;

		
	}

	public static function typename($type,$type_id)

	{

		if($type==3){
			$tname           =Event::where('id',$type_id)->first(['event_name as name']);
		}
		else{
			$tname           =Package::where('pac_id',$type_id)->first(['pac_name as name']);

		}
		return $tname;
		
	}


	

	
}
