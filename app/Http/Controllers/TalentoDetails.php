<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Attendance;
use App\Models\Event;
use App\Models\College;
use App\Models\Technology;
use App\Models\Talento;
use App\Models\PageContent;
use Illuminate\Support\Facades\Mail;
use App\Models\Gatepass;
use App\Models\Package;
use App\Models\Signature;
use App\Models\Studentpackage;
use App\Models\Paymentrequest;
use App\Models\Studentfeessplit;
use App\Models\Payment;
use App\Models\OldPayment;
use App\Models\Tax;
use App\Models\Employee;
use App\Models\Jobrole;
use App\Models\Employeeinfo;
use App\Models\Employeepriv;
use App\Models\Oldstudent;
use App\Models\Student;
use App\Models\Contacts;
use App\Models\Qualification;
use App\Models\Specialization;
use App\Mail\ForgetMail;
use App\Models\Studentinfo;
use App\Models\StudentEvent;
use App\Models\Salespackage;
use App\Models\Salespayment;

use App\Models\JobRegister;


use Carbon\Carbon;
use App\Models\Appversion;
class TalentoDetails extends Controller
{


    public function jobinfo_view(Request $request)
    {
        $user_id = $request->get("user_id");
    
        $userdetails = JobRegister::where('user_id', $user_id)->first();
    
        return response()->json($userdetails);
    }
    
    public function updateJobInfo(Request $request)
    {
        $user_id = $request->get("user_id");
        $jobInfo = JobRegister::where('user_id', $user_id)->first();
    
        if (!$jobInfo) {
            return response()->json(['message' => 'No records found'], 404);
        }
    
        if ($request->hasFile('resume_upload')) {
            $resume = $request->file('resume_upload');
            $resumeName = time() . '_' . $resume->getClientOriginalName();
            $resume->move(public_path('uploads/resume'), $resumeName);
            $jobInfo->resume_upload = $resumeName;  // Update resume in database
        }
    
        $jobInfo->update($request->except('user_id', 'resume_upload'));
    
        return response()->json([
            'status' => true,
            'message' => 'Job info updated successfully',
            'data' => $jobInfo
        ]);
    }


    public function job_register(Request $request)
    {
        $userId = $request->input('user_id');
        $user = User::find($userId);

    
        if (!$user || $user->role != 11) {
            return response()->json([
                'error' => 'Unauthorized or user role is not valid',
            ], 403); 
        }
    
    
        if ($userId != $user->id) {
            return response()->json([
            ], 403);
        }
    
        $data = $request->only([
            'user_id', 'name', 'email', 'contact_pri', 'contact_sec', 'position_applying',
            'edu_qualification', 'last_studied', 'yearof_passout', 'exp_years', 'curr_organisation',
            'curr_designation', 'carrier_break', 'ifcarrier_break', 'tech_skills', 'notice_period',
            'nego_notice_period', 'min_notice_period', 'address', 'city', 'curr_salary',
            'exp_salary', 'ref_last_organisation', 'organisation_ref_contact', 'ref_college',
            'college_ref_contact', 'hear_about_us'
        ]);
    
        if ($request->hasFile('resume_upload')) {
            $resume = $request->file('resume_upload');
            $ResumeName = $resume->getClientOriginalName();
            $resume->move(public_path('uploads/resume'), $ResumeName);
            $data['resume_upload'] = $ResumeName;
        } else {
            $data['resume_upload'] = '';
        }

    
        $jobData = JobRegister::create($data);
    
        return response()->json([
            'status' => true,
            'message' => 'Job Registration Successful',
        ]);
    }



public function tvsection(Request $request){

        $datauser = User::where('role', 2)
        ->where('status', 1)
        ->get(['id', 'name']);
    
        $today = date("Y-m-d");
        $first_date = date("Y-m-01");
    
        $revenueList = [];
        $admissionList = [];
       
    
    foreach ($datauser as $user) {
        $user_id = $user->id;
        $name = $user->name;
    
        // Revenue
        $monthrevenue_chry_withoutgst   = round(Salespayment::where("sales_payment.sales_id",$user_id)->where("sales_payment.package_type",2)
                                                ->whereBetween("sales_payment.date", [$first_date, $today])
                                                ->sum("sales_payment.payment"));
    
        $monthrevenue_regular_withoutgst = round( Salespayment::where("sales_payment.sales_id",$user_id)->where("sales_payment.package_type",1)
                                            ->whereBetween("sales_payment.date", [$first_date, $today])
                                            ->sum("sales_payment.payment"));

        
        $monthrevenue_old_withoutgst      = round(OldPayment::where("olddata_payment.sales_id",$user_id)
                                                    ->whereBetween("olddata_payment.date", [$first_date, $today])
                                                     ->sum("olddata_payment.fees_income"));

        $monthrevenue_regold_withoutgst = $monthrevenue_regular_withoutgst + $monthrevenue_old_withoutgst;
        $total_revenue = $monthrevenue_chry_withoutgst + $monthrevenue_regold_withoutgst;
    
        $revenueList[$name] = $total_revenue;
        // Admission
       

        $monthadmi_chry_withoutgst      =round(Salespackage::where("sales_package.sales_id",$user_id)
                                        ->where("sales_package.package_type",2)
                                        ->whereBetween("sales_package.joining_date",[$first_date, $today])
                                        ->sum("sales_package.package_amount")); 


        $monthadmi_regular_withoutgst      = round(Salespackage::where("sales_package.sales_id",$user_id)
                                                ->where("sales_package.package_type",1)
                                                ->whereBetween("sales_package.joining_date", [$first_date, $today])
                                                ->sum("sales_package.package_amount"));

        $reg_old_withoutgst                 = round(Oldstudent::where("ev_oldstudents.sales_id",$user_id)
                                                ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                                                ->sum("ev_oldstudents.fees"));
       

        $monthly_tot_admissionwithoutgst = $monthadmi_regular_withoutgst + $reg_old_withoutgst;
        $total_admission = $monthly_tot_admissionwithoutgst + $monthadmi_chry_withoutgst;
    
        $admissionList[$name] = $total_admission;
         // Combine Revenue and Admission
  
    }
    
    // Sort the arrays to find top users
    arsort($revenueList);
    arsort($admissionList);
   
    
    // Get top users' IDs
    $datapass['topRevenueUser'] = array_keys(array_slice($revenueList, 0, 5, true)); // Top 5 revenue users
    $datapass['topAdmissionUser'] = array_keys(array_slice($admissionList, 0, 5, true)); // Top 5 admission users
   
    $datapass['status']=true;
    echo json_encode($datapass);
   
    }
    public function deleteattendance (Request $request)
    {
        $student_id = $request->get("student_id");
        $date =date('Y-m-d');
        Attendance::where('student_id',$student_id)->where('date',$date)->delete();
        $data['status']=true;
        echo json_encode($data);

    }
    public function email_validation(Request $request)
    {
        $email = $request->get("email");
        $phone = $request->get("phone");

        // Check if email exists in any of the tables
        // Check if email exists in any of the tables

        $emailcheckuser = User::where('email', $email)->count();
        $emailcheckold = Oldstudent::where('email_id', $email)->count();
        $emailcheckcontact = Contacts::where('email', $email)->count();

        $phonecheckuser = User::where('phone', $phone)->count();
        $phonecheckold = Oldstudent::where('contact_no', $phone)->count();
        $phonecheckcontact = Contacts::where(function ($query) use ($phone) {
            $query->where('contact1', $phone)->orWhere('contact2', $phone);
        })->count();

        if ($emailcheckuser > 0) {
            $data['status'] = false;
            $data["msg"] = 'Email already registered';
        } elseif ($emailcheckold > 0) {
            $data['status'] = false;
            $data["msg"] = 'Email already registered in Old Portal Please Contact with Hr Team';
        } elseif ($emailcheckcontact > 0) {
            $data['status'] = false;
            $data["msg"] = 'Email already registered in Contact section Please Contact With POC team';
        } elseif ($phonecheckuser > 0) {
            $data['status'] = false;
            $data["msg"] = 'This Phone number Already registered';
        } elseif ($phonecheckold > 0) {
            $data['status'] = false;
            $data["msg"] = 'This Phone number Already registered Old Portal Please Contact with Hr Team ';
        } elseif ($phonecheckcontact > 0) {
            $data['status'] = false;
            $data["msg"] = 'This Phone number Already registeredin Contact section Please Contact With POC team';
        } else {
            $data['status'] = true; // Default to true unless conditions are met
            $data["msg"] = 'Proceed to continue';
        }

        return response()->json($data);
    }

    

    public function admission_details(Request $request)
    {
        $student_id = $request->get("student_id");
        $data['studentdetails'] = Student::where('stud_id', $student_id)->first();

        if (!empty($data['studentdetails'])) {
            $data['status'] = true;
        } else {
            $data['status'] = false;
        }

        echo json_encode($data);
    }


    public function user_registration(Request $request)
    {

        if($request->input("role")==11){

            $datauser = [
                'name' => $request->input("name"),
                'phone' => $request->input("phone"),
                'email' => $request->input("email"),
                'password' => Hash::make($request->input("password")),
                'password_text' => $request->input("password"),
                'role' => $request->input("role"),
                'register_by' => 2,
                'status' => 1,
                'created_at' => now(),
                'device_token' => $request->input("device_token"),
                'device_type' => $request->input("device_type"),
            ];
            $insertId = User::insertGetId($datauser);
            $registerDetails = User::find($insertId, ['id', 'name', 'phone', 'email', 'photo','role']);
            $data['userdetails'] = $registerDetails;
            $data['status'] = true;
            $data['msg'] = 'Successfully Registered';

            $data['qualification'] = Qualification::where('status', 1)->get();
            $data['specialization'] = Specialization::where('status', 1)->get();
            $data['job_roles'] = Jobrole::where('status', 1)->get();
           
        

        }

       else{
       
        $datauser = [
            'name' => $request->input("name"),
            'phone' => $request->input("phone"),
            'email' => $request->input("email"),
            'password' => Hash::make($request->input("password")),
            'password_text' => $request->input("password"),
            'role' => $request->input("role"),
            'register_by' => 2,
            'status' => 1,
            'created_at' => now(),
            'device_token' => $request->input("device_token"),
            'device_type' => $request->input("device_type"),
        ];
    
        $staff_code = $request->input("staff_code");
        $staffcount = Employeeinfo::where('poc_code', $staff_code)->count();
    
        if ($staffcount > 0) {
            $insertId = User::insertGetId($datauser);
            $code = Employeeinfo::where('poc_code', $staff_code)->first(['user_id']);
    
            if ($insertId) {
                // Insert into Talento
                $talentoData = [
                    'user_id' => $insertId,
                    'registration_type' => $request->input("type"),
                    'status' => 0,
                    'created_at' => now(),
                    'college_id' => $request->input("college"),
                    'staff_code' => $staff_code,
                ];
                Talento::create($talentoData);
    
                // Insert into Studentinfo
                $student_admno = Studentinfo::max('student_admissionid') + 1;
                $datastudentinfo = [
                    'college_id' => $request->input("college"),
                    'student_id' => $insertId,
                    'student_admissionid' => $student_admno,
                    'sales_id' => $code['user_id'],
                ];
                Studentinfo::create($datastudentinfo);
    
                
                if ($request->input("type") == 3) {
                    $eventData = [
                        'student_id' => $insertId,
                        'created_at' => now(),
                        'event_id' => $request->input("event_id"),
                        'sales_id' => $code['user_id'],
                    ];
                    StudentEvent::create($eventData);
                }
    
                $registerDetails = User::find($insertId, ['id', 'name', 'phone', 'email', 'photo','role']);
                $data['userdetails'] = $registerDetails;

                $data['qualification'] = Qualification::where('status', 1)->get();
                $data['specialization'] = Specialization::where('status', 1)->get();
                $data['job_roles'] = Jobrole::where('status', 1)->get();


                $data['status'] = true;
                $data['registration_type'] = $request->input("type");

                $data['student_admno'] = 'SIAC' . $student_admno;
                $data['msg'] = 'Successfully Registered';
                $data['collegename'] = College::find($request->input("college"))->college;
                $data['college_id'] = $request->input("college");
            }
        } else {
        
            $data['status'] = false;
            $data['msg'] = 'Staff code incorrect';
        }
    }
    
        return response()->json($data);
    }

    public function package_details(Request $request)
    {
        $student_id = $request->get("student_id");
        $package_id = $request->get("package_id");
        $package_type = $request->get("package_type");

        $data['packagedetails'] = Studentpackage::join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
            ->where('ev_student_package.student_id', $student_id)
            ->where('ev_student_package.package_id', $package_id)
            ->where('ev_student_package.package_type', $package_type)
            ->first(['ev_student_package.*', 'ev_package.pac_name', 'ev_package.duration', 'ev_package.fee', 'ev_package.tax as maintax', 'ev_package.total']);
        $data['feessplitup'] = Studentfeessplit::where('student_id', $student_id)->get();
        $data['payment_history'] = Payment::orderBy('id', 'desc')
            ->where('student_id', $student_id)
            ->where('package_type', $package_type)
            ->where('package_id', $package_id)
            ->get();

        $data['tax'] = 'Tax' . ' ' . Tax::find(1)->tax . '%';
        $data['status'] = true;
        echo json_encode($data);
    }

    public function student_admission(Request $request)
    {
        $data['stud_id'] = $request->get("student_id");
        $data['stud_name'] = $request->get("stud_name");
        $data['stud_phone'] = $request->get("stud_phone");
        $data['stud_sec_phone'] = $request->get("stud_sec_phone");
        $data['stud_whatsapp_no'] = $request->get("stud_whatsapp_no");
        $data['stud_email'] = $request->get("stud_email");
        $data['stud_sec_email'] = $request->get("stud_sec_email");
        $data['stud_dob'] = $request->get("stud_dob");
        $data['stud_blood'] = $request->get("stud_blood");
        $data['stud_father_name'] = $request->get("stud_father_name");
        $data['stud_father_occu'] = $request->get("stud_father_occu");
        $data['stud_father_no'] = $request->get("stud_father_no");
        $data['stud_father_email'] = $request->get("stud_father_email");
        $data['stud_mother_name'] = $request->get("stud_mother_name");
        $data['stud_mother_occu'] = $request->get("stud_mother_occu");
        $data['stud_mother_no'] = $request->get("stud_mother_no");
        $data['stud_mother_email'] = $request->get("stud_mother_email");
        $data['stud_per_add'] = $request->get("stud_per_add");
        $data['stud_temp_add'] = $request->get("stud_temp_add");
        $data['stud_city'] = $request->get("stud_city");
        $data['stud_town'] = $request->get("stud_town");

        $image = $request->file('stud_photo');
        if ($request->hasFile('stud_photo')) {
            $imageName = $image->getClientOriginalName();
            $image->move(public_path('uploads/pic'), $imageName);
        } else {
            $imageName = '';
        }
        $data['stud_photo'] = $imageName;

        $aahr_image = $request->file('stud_aadh_photo');
        if ($request->hasFile('stud_aadh_photo')) {
            $aahrName = $aahr_image->getClientOriginalName();
            $aahr_image->move(public_path('uploads/aadhar'), $aahrName);
        } else {
            $aahrName = '';
        }

        $data['stud_aadh_photo'] = $aahrName;
        $data['stud_qualification'] = $request->get("stud_qualification");
        $data['stud_institute'] = $request->get("stud_institute");
        $data['stud_year_of_pass'] = $request->get("stud_year_of_pass");
        $data['stud_arrears'] = $request->get("stud_arrears");

        if ($data['stud_arrears'] == 'No') {
            $data['stud_arrears_no'] = 0;
        } 
        if ($data['stud_arrears'] == 'Yes') {
            $data['stud_arrears_no'] = $request->get("stud_arrears_no");
        }
        if ($data['stud_arrears'] == 'Awaiting Results') {
            $data['stud_arrears_no'] = 0;
        }

        $data['stud_specialization'] = $request->get("stud_specialization");
        $data['stud_percentage'] = $request->get("stud_percentage");
        $data['stud_hsst_name'] = $request->get("stud_hsst_name");
        $data['stud_hsst_per'] = $request->get("stud_hsst_per");
        $data['stud_hsst_year'] = $request->get("stud_hsst_year");
        $data['stud_ss_name'] = $request->get("stud_ss_name");
        $data['stud_ss_per'] = $request->get("stud_ss_per");
        $data['stud_ss_year'] = $request->get("stud_ss_year");
        $data['stud_doj']   = $request->get("stud_doj");
        $data['stud_pref_doj'] = $request->get("stud_pref_doj");

       
        $data['stud_ad_on'] = $request->get("stud_ad_on");

        $data['stud_place_training'] = $request->get("stud_place_training");

        $resume = $request->file('screenshot');
        if ($request->hasFile('screenshot')) {
            $resumeName = $resume->getClientOriginalName();
            $resume->move(public_path('uploads/receipt'), $resumeName);
        } else {
            $resumeName = '';
        }
        $data['stud_receipt'] = $resumeName;

        Student::insert($data);

        $datauser['name'] = $request->get("stud_name");
        $datauser['phone'] = $request->get("stud_phone");
        $datauser['phone2'] = $request->get("stud_sec_phone");
        $datauser['admission_completed'] = 1;

        $datastudentinfo['college_id'] = $request->get("college_id");

        User::where('id', $data['stud_id'])->update($datauser);

        Studentinfo::where('student_id', $data['stud_id'])->update($datastudentinfo);

        $datastatus['status'] = true;
        $datastatus['stud_photo'] = $imageName;

        echo json_encode($datastatus);
    }

    public function updateversion(Request $request)
    {
        $data1['app_version'] = $request->get("version");
        $data1['msg']= $request->get("msg");
        Appversion::where('id', 1)->update($data1);
        $data['version'] = Appversion::where('id', 1)->first(['app_version']);
        $data['status'] = true;
        echo json_encode($data);
    }
    public function getversion(Request $request)
    {
        $data['version'] = Appversion::where('id', 1)->first(['app_version']);
      
        $data['status'] = true;
        echo json_encode($data);
    }

    public function receipt(Request $request)
    {
        $payment_id = $request->get("payment_id");

        $count = Payment::where('id', $payment_id)->count();

        if($count==0){

            $data['historystatus']=false;

        }
        else{

        $paymenthistory = Payment::where('id', $payment_id)->first();
        $student_id = $paymenthistory['student_id'];
        $date = $paymenthistory['date'];
        $payment_type = $paymenthistory['payment_type'];
        $amount = $paymenthistory['paid_amount'];
        $package_id = $paymenthistory['package_id'];
        $student_name = User::find($student_id)->name;
        $student_phone = User::find($student_id)->phone;
        $packagedetails = Package::where('pac_id', $package_id)->first(['course_id']);
        $signatute = Signature::find(1)->signature;
        $seal = Signature::find(1)->seal;
        $logo = Signature::find(1)->header;

        $datatax['tax'] = Tax::find(1)->tax;
        $x = 1 + $datatax['tax'] / 100;
        $income = round($amount / $x, 2);
        $taxval = round($amount - $income, 2);
        $data = [
            'receipt_no' => $payment_id,
            'date' => $date,
            'name_student' => ucfirst($student_name),
            'name_phone' => $student_phone,
            'payment_type' => ucfirst($payment_type),
            'amount' => number_format($amount, 2),
            'status' => 'Success',
            'cource' => $packagedetails['course_id'],
            'actual_amount' => number_format($income, 2),
            'cgst' => number_format($taxval / 2, 2),
            'gst' => number_format($taxval / 2, 2),
            'signatute' => $signatute,
            'seal' => $seal,
            'logo' => $logo,
        ];

        $data['historystatus']=true;
     }
        // $data['status']=true;
       echo json_encode($data);
    }
    public function student_package(Request $request)
    {
        $student_id = $request->get("student_id");

        $data['activated_packages_regular'] = Studentpackage::join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
            ->where('ev_student_package.student_id', $student_id)
            ->where('ev_student_package.package_type', 1)
            ->distinct('ev_student_package.package_id')
            ->get(['ev_student_package.package_id', 'ev_student_package.package_type', 'ev_package.pac_name', 'ev_student_package.id as studentpackageid', 'ev_student_package.sales_id']);

        $data['activated_packages_chrysalis'] = Studentpackage::join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
            ->where('ev_student_package.student_id', $student_id)
            ->where('ev_student_package.package_type', 2)
            ->distinct('ev_student_package.package_id')
            ->get(['ev_student_package.package_id', 'ev_student_package.package_type', 'ev_package.pac_name', 'ev_student_package.id as studentpackageid', 'ev_student_package.sales_id']);

        $list_regular = [];
        $list_chrysalis = [];

        if (!empty($data['activated_packages_regular'])) {
            foreach ($data["activated_packages_regular"] as $alist) {
                $studentpackage_id = $alist->studentpackageid;
                $pac_type = $alist->package_type;
                $pac_id = $alist->package_id;

                $data['package_details'] = Studentpackage::join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
                    ->where('ev_student_package.id', $studentpackage_id)
                    ->first(['ev_package.total', 'ev_student_package.*', 'ev_package.pac_name']);

                $data['paidfees'] = Payment::where('student_id', $student_id)
                    ->where('package_type', $pac_type)
                    ->where('package_id', $pac_id)
                    ->sum('paid_amount');

                if ($data['package_details']->reduction_check == 0) {
                    $rem = $data['package_details']->total - $data['paidfees'];
                    $total = $data['package_details']->total;
                } else {
                    $rem = $data['package_details']->totalfees_afterreduction - $data['paidfees'];
                    $total = $data['package_details']->totalfees_afterreduction;
                }
                $alist->total = number_format($total, 2);
                $alist->paid = number_format($data['paidfees'], 2);
                $alist->rem = number_format($rem, 2);
                $list_regular[] = $alist;
            }
        }

        if (!empty($data['activated_packages_chrysalis'])) {
            foreach ($data["activated_packages_chrysalis"] as $alist) {
                $studentpackage_id = $alist->studentpackageid;
                $pac_type = $alist->package_type;
                $pac_id = $alist->package_id;

                $data['package_details'] = Studentpackage::join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
                    ->where('ev_student_package.id', $studentpackage_id)
                    ->first(['ev_package.total', 'ev_student_package.*', 'ev_package.pac_name']);

                $data['paidfees'] = Payment::where('student_id', $student_id)
                    ->where('package_type', $pac_type)
                    ->where('package_id', $pac_id)
                    ->sum('paid_amount');

                if ($data['package_details']->reduction_check == 0) {
                    $rem = $data['package_details']->total - $data['paidfees'];
                    $total = $data['package_details']->total;
                } else {
                    $rem = $data['package_details']->totalfees_afterreduction - $data['paidfees'];
                    $total = $data['package_details']->totalfees_afterreduction;
                }
                $alist->total = number_format($total, 2);
                $alist->paid = number_format($data['paidfees'], 2);
                $alist->rem = number_format($rem, 2);
                $list_chrysalis[] = $alist;
            }
        }

        $data1['regularpackages'] = $list_regular;
        $data1['chrysalispackages'] = $list_chrysalis;
        $data1['eventlist'] = StudentEvent::join('ev_event', 'ev_event.id', '=', 'ev_student_events.event_id')
                                           ->where('ev_event.status', '1')->where('ev_student_events.student_id',$student_id)
                                            ->get();
        $data1['status'] = true;
        return json_encode($data1);
    }

    public function allpayment_request(Request $request)
    {
        $data['student_id'] = $request->get("student_id");
        $data1['allpaymentrequest'] = Paymentrequest::where('student_id', $data['student_id'])
            ->orderBy('id', 'desc')
            ->get();

        if (!empty($data1['allpaymentrequest'])) {
            $data1['status'] = true;
        } else {
            $data1['status'] = false;
        }
        echo json_encode($data1);
    }

    public function paymentrequest(Request $request)
    {
        $data['payment_method'] = $request->get("payment_method");
        $data['student_id'] = $request->get("student_id");
        $data['date'] = date('Y-m-d');
        $data['amount'] = $request->get("amount");
        $data['sales_id'] = $request->get("sales_id");
        $data['studentpackage_id'] = $request->get("studentpackage_id");

        //ANy pending

        $payreqcount = Paymentrequest::where('student_id', $data['student_id'])
            ->where('studentpackage_id', $data['studentpackage_id'])
            ->where('status', 0)
            ->count();

        if ($payreqcount == 0) {
            if ($data['payment_method'] == 'bank') {
                $data['transaction_id'] = $request->get("transaction_id");
                $count = Payment::where('transaction_id', $data['transaction_id'])->count();
                $ocount = OldPayment::where('transaction_id', $data['transaction_id'])->count();
                $payreq = Paymentrequest::where('transaction_id', $data['transaction_id'])
                    ->where('status', 0)
                    ->count();

                if ($count == 0 && $ocount == 0 && $payreq == 0) {
                    if ($request->file("payment_screenshort") != '') {
                        $extension = $request->file("payment_screenshort")->getClientOriginalExtension();
                        $destinationPath = "public/uploads/screenshot"; // upload path
                        $fileName = rand(11111, 99999) . "." . $extension;
                        $request->file("payment_screenshort")->move($destinationPath, $fileName);
                        $data['payment_screenshort'] = $fileName;
                    }
                    $insert = Paymentrequest::insert($data);
                    $datastatus['status'] = true;
                    $datastatus['msg'] = "Sucessfully Send";
                } else {
                    $datastatus['status'] = false;
                    $datastatus['msg'] = "Transaction id must be unique";
                }
            } else {
                $insert = Paymentrequest::insert($data);
                $datastatus['status'] = true;
                $datastatus['msg'] = "Sucessfully Send";
            }
        } else {
            $datastatus['status'] = false;
            $datastatus['msg'] = "Your Payment Request is Pending";
        }
        return json_encode($datastatus);
    }

    public function forgot_password(Request $request)
    {
        $email = $request->get("email");
        $data['details'] = User::where('email', $email)
            ->where('role', 5)
            ->first(['password_text', 'name']);

        if ($data['details'] != '') {
            $student_name = $data['details']->name;
            $student_email = $email;
            $recipient = $student_email; // Replace with the recipient's email address

            $dynamicData = ['name' => ucfirst($student_name), 'subject' => 'Eva Portal', 'email' => $student_email, 'password' => $data['details']->password_text];
            Mail::to($recipient)->send(new ForgetMail($dynamicData));

            $data1['status'] = true;
            $data1['msg'] = 'Please Check your Email';
        } else {
            $data1['status'] = false;
            $data1['msg'] = 'Invalid Email';
        }
        return json_encode($data1);
    }
    public function mark_attendance(Request $request)
    {
        $data1['student_id'] = $request->get("student_id");
        $data1['date'] = date('Y-m-d');
        $data1['sales_id'] = $request->get("sales_id");
        $data1['type'] = $request->get("type");
        $data1['type_id'] = $request->get("type_id");
        $data1['attendance_status'] = 1;
      

        $attendance = Attendance::where('student_id', $data1['student_id'])->where('type', $data1['type'])
                            ->where('type_id', $data1['type_id'])
                            ->where('date', date('Y-m-d'))
                            ->count();

        if ($attendance <= 0) {
            $result = Attendance::insert($data1);
            $data['status'] = true;
            $data["msg"] = 'Attendance Successfully added';
        } else {
            $data['status'] = false;
            $data["msg"] = 'Attendance Already Marked';
        }
        return json_encode($data);
    }
private function isPhoneUnique($phone, $userId)
{
    // Query to check if the phone number exists for other users
    return User::where('phone', $phone)
               ->where('id', '!=', $userId)
               ->doesntExist();
}

    public function edit_profile(Request $request)
    {
        $id                             = $request->get("user_id");
        $datauser['name']               = $request->get("name");
        $datauser['phone']              = $request->get("phone");

        $datauser1['college_id']        = $request->get("college");




        if ($this->isPhoneUnique($request->input("phone"), $request->input("user_id"))) {

            if ($request->file("photo") != '') {
                $extension = $request->file("photo")->getClientOriginalExtension();
                $destinationPath = "public/uploads/photo"; // upload path
                $fileName = rand(11111, 99999) . "." . $extension;
                $request->file("photo")->move($destinationPath, $fileName);
    
                //User::where('id',$data['user_id'])->update(array('photo'=>$fileName));
    
                $datauser['photo'] = $fileName;
            } else {
                $datauser['photo'] = User::find($id)->photo;
            }
            $result = User::where("id", $id)->update($datauser);
            Studentinfo::where('student_id',$id)->update($datauser1);

            $datastudent['stud_name']       =  $datauser['name'] ;
            $datastudent['stud_phone']      =  $datauser['phone'];


            Student::where('stud_id',$id)->update($datastudent);


            if ($result) {
                $data['status'] = true;
                $data['userdetails'] = User::where("id", $id)->first();

                if($datauser1['college_id'] !=''){
                     $data['college_name'] = College::find($datauser1['college_id'])->college;
                }
                else{

                    $data['college_name']='';

                }
                $data["msg"] = 'Successfully Updated';
            } else {
                $data['status'] = false;
                $data["msg"] = 'Fail to Update';
            }
    
        }
        else{

            $data['status'] = false;
            $data["msg"] = 'Phone number already exists for another user.';

        }

        return json_encode($data);
    }

    public function change_password(Request $request)
    {
        $id = $request->get("user_id");
        $current_password = $request->get("current_password");

        $count = User::where('id', $id)
            ->where('password_text', $current_password)
            ->count();

        if ($count == 0) {
            $data['status'] = false;
            $data["msg"] = 'Entered incorrect password';
        } else {
            $datauser['password'] = Hash::make($request->get("new_password"));
            $datauser['password_text'] = $request->get("new_password");

            $result = User::where("id", $id)->update($datauser);

            if ($result) {
                $data['status'] = true;
                $data["msg"] = 'Password Changed Successfully';
            } else {
                $data['status'] = false;
                $data["msg"] = 'Fail to Change Password';
            }
        }

        return json_encode($data);
    }

    public function gatepass_list(Request $request)
    {
        $user_id = $request->get("user_id");

        $data['gatepassrequest'] = Gatepass::where('user_id', $user_id)
            ->orderBy('id', 'desc')
            ->get();

        $data1['gatepassrequestfirst'] = Gatepass::where('user_id', $user_id)
            ->orderBy('id', 'desc')
            ->first();
        //0-notapply,1-apply
        if (!empty($data1['gatepassrequestfirst'])) {
            if ($data1['gatepassrequestfirst']['status'] == 3) {
                $data['aplied_status'] = true;
            } elseif ($data1['gatepassrequestfirst']['status'] == 2) {
                $data['aplied_status'] = true;
            } else {
                $data['aplied_status'] = false;
            }
        } else {
            $data['aplied_status'] = false;
        }

        if (count($data['gatepassrequest']) == 0) {
            $data['status'] = false;
        } else {
            $data['status'] = true;
        }

        $data['signatute '] = Signature::find(1)->signature;

        $data['seal'] = Signature::find(1)->seal;
        $data['contact_name'] = Signature::find(1)->name;
        $data['contact_no'] = Signature::find(1)->contact_no;
        $data['photo'] = User::find($user_id)->photo;
        $data['enddate'] = Gatepass::where('user_id', $user_id)
            ->where('status', 1)
            ->max('end_date');

        echo json_encode($data);
    }

    public function gatepass_request(Request $request)
    {
        $data['user_id'] = $request->get("user_id");
        $data['start_date'] = $request->get("start_date");
        $data['end_date'] = $request->get("end_date");
        $data['type'] = $request->get("type");
        $data['sales_id'] = $request->get("sales_id");
        $data['type_id'] = $request->get("type_id");

        $data['status'] = 0;
        $data['requested_date'] = date("Y-m-d");

        if ($request->file("photo") != '') {
            $extension = $request->file("photo")->getClientOriginalExtension();
            $destinationPath = "public/uploads/photo"; // upload path
            $fileName = rand(11111, 99999) . "." . $extension;
            $request->file("photo")->move($destinationPath, $fileName);

            User::where('id', $data['user_id'])->update(['photo' => $fileName]);

            $datastatus['filename'] = $fileName;

            $data['photo'] = $fileName;
        } else {
            $datastatus['filename'] = User::find($data['user_id'])->photo;
            $data['photo'] = $datastatus['filename'];
        }

        $insert = Gatepass::insert($data);

        if ($insert) {
            $datastatus['status'] = true;
            $datastatus['msg'] = "Sucessfully Send";
        } else {
            $datastatus['status'] = false;
            $datastatus['msg'] = "Cannot Send Your Request";
        }
        echo json_encode($datastatus);
    }


    

public function login(Request $request)
{
    // Retrieve inputs
    $email = $request->input("email");
    $password = $request->input("password");
    $device_token = $request->input("device_token");
    $device_type = $request->input("device_type");

    // Attempt to find user
    $user = User::where('email', $email)->first();

    // Check if user exists and verify password
    if ($user && Hash::check($password, $user->password)) {

        if ($user->role == 5) {
           
           
            $col = Studentinfo::where('student_id', $user->id)->first(['college_id', 'student_admissionid']);
            $college_id = $col ? $col['college_id'] : null;
            $student_admissionid = $col ? $col['student_admissionid'] : null;

            // Prepare user details for response
            $userDetails = [
                'id' => $user->id,
                'name' => $user->name,
                'last_name' => '', // Add appropriate field if available
                'email' => $user->email,
                'phone' => $user->phone,
                'student_admno' => $student_admissionid ? 'SIAC' . $student_admissionid : '',
                'place' => '', // Add appropriate field if available
                'school' => '', // Add appropriate field if available
                'picture_url' => $user->photo,
                'technology_id' => $user->technology,
                'course_type' => '', // Add appropriate field if available
                'college_id' => $college_id,
                'created_at' =>date('Y-m-d'),
                'role' => $user->role,
                'admission_status' => (bool) $user->admission_completed,
            ];

            // Fetch college name if college_id exists
            $userDetails['collegename'] = $college_id ? College::find($college_id)->college : '';

            // Update device_token and device_type for the user
            $user->update([
                'device_token' => $device_token,
                'device_type' => $device_type,
            ]);

            // Prepare response data
            $response = [
                'status' => true,
                'msg' => 'Successfully Logged in!',
                'userDetails' => $userDetails,
                'userrole' => $userDetails['role'],
                'qualification' => Qualification::where('status', 1)->get(),
                'specialization' => Specialization::where('status', 1)->get(),
                'regularcount' => Studentpackage::where('student_id', $user->id)->where('package_type', 1)->count()
            ];
        } 
        else if($user->role==11){

            $response=['jobinfo'=>JobRegister::where('user_id', $user->id)->count(),
            'status' => true,
            'userrole' => $user->role,
            'job_roles' => Jobrole::where('status', 1)->get(),
            'userDetails' => User::find($user->id, ['id', 'name', 'phone', 'email', 'photo','role'])
        ];

        }
        else {
            // Handle non-role 5 users if needed
            $response = [
                'status' => true,
                'userrole' => $user->role,
            ];
        }

        
    } else {
        // Authentication failed
        $response = [
            'status' => false,
            'msg' => 'Invalid username or password or account not active',
        ];
    }

    // Return JSON response
    return response()->json($response);
}


    



    public function helpcontent(Request $request)
    {
        $data['user_id'] = $request->get("user_id");
        $data['content'] = $request->get("content");
        $data['created_at'] = date("Y-m-d");
        $insert = PageContent::insert($data);

        if ($insert) {
            $datastatus['status'] = true;
            $datastatus['msg'] = "Sucessfully Send";
        } else {
            $datastatus['status'] = true;
            $datastatus['msg'] = "Cannot Send";
        }
        echo json_encode($datastatus);
    }

    public function userdetails(Request $request){

        $user_id        =$request->get("user_id");
        $user           = User::where('id', $user_id)->first();

        $col = Studentinfo::where('student_id',  $user_id)->first(['college_id', 'student_admissionid']);
        $college_id = $col ? $col['college_id'] : null;
        $student_admissionid = $col ? $col['student_admissionid'] : null;

        $userDetails = [
            'id' => $user->id,
            'name' => $user->name,
            'last_name' => '', // Add appropriate field if available
            'email' => $user->email,
            'phone' => $user->phone,
            'student_admno' => $student_admissionid ? 'SIAC' . $student_admissionid : '',
            'place' => '', // Add appropriate field if available
            'school' => '', // Add appropriate field if available
            'picture_url' => $user->photo,
            'technology_id' => $user->technology,
            'course_type' => '', // Add appropriate field if available
            'college_id' => $college_id,
            'created_at' =>date('Y-m-d'),
            'role' => $user->role,
            'admission_status' => (bool) $user->admission_completed,
        ];

        $userDetails['collegename'] = $college_id ? College::find($college_id)->college : '';

        $data['collegelist'] = College::where('status', '1')->get();

        $data['userDetails'] = $userDetails;

        $data['status'] = true;
        return json_encode($data);



    }

    public function get_details(Request $request)
    {
        $data['collegelist'] = College::where('status', '1')->get();
        $data['eventlist'] = Event::where('status', '1')->get();
        $data['chrysalis'] = $data["package"] = Package::leftJoin("ev_technology", "ev_technology.id", "=", "ev_package.tech_id")
            ->where("ev_technology.status", 1)
            ->where("ev_package.pac_type", 2)
            ->where("ev_package.status", 1)
            ->get(['ev_package.pac_id', 'ev_package.pac_name']);

        $data['regular'] = Package::leftJoin("ev_technology", "ev_technology.id", "=", "ev_package.tech_id")
            ->where("ev_package.pac_type", 1)
            ->where("ev_technology.status", 1)

            ->where("ev_package.status", 1)
            ->get(['ev_package.pac_id', 'ev_package.pac_name']);

        $data['technologylist'] = Technology::where('status', '1')->get();



        $data['status'] = true;
        return json_encode($data);
    }
}
////JYOTHI ENDS
