<?php
namespace App\Http\Middleware\CheckStatus;
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Technology;
use App\Models\Designation;
use App\Models\Qualification;
use App\Models\Source;
use App\Models\College;
use App\Models\User;
use App\Models\Contacts;
use App\Models\Followup;
use App\Models\Package;
use App\Models\Studentpackage;
use App\Models\Studentfeessplit;
use App\Models\Payment;
use App\Models\Tax;
use App\Models\Salespayment;
use App\Models\Salespackage;
use App\Models\Othereference;
use Illuminate\Support\Facades\View;
use Dompdf\Dompdf;
use Dompdf\Options;
use Illuminate\Support\Facades\Storage;
use App\Models\Employee;
use App\Mail\SimpleMail;
use App\Mail\PayMail;
use App\Models\OldPayment;

use App\Models\Signature;
use Illuminate\Support\Facades\Mail;
use App\Models\Talento;
use App\Models\Gatepass;
use App\Models\Oldstudent;
use App\Models\Reporting;
use Session;
use Auth;
class ReportsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware("auth");
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     public function claim_withgst($empid = null)
     {
         if (Auth::check()) {
             if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                 $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                 $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));
 
                 $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                 $today = date("Y-m-d", strtotime($_GET['search_date2']));
             } else {
                 $today = date('Y-m-d');
                 $first_date = date('Y-m-01');
 
                 $data['search_date1'] = '';
                 $data['search_date2'] = '';
             }
 
             if (Auth::user()->role == 2 || Auth::user()->role == 1) {
                 if ($empid == null) {
                     $user_id = Auth::user()->id;
                     $data['empstatus'] = 0;
                 } else {
                     $user_id = $empid;
 
                     $data['subtitle'] = 'Reports of' . ucfirst(User::find($empid)->name);
                     $data['empstatus'] = 1;
                     $data['empid'] = $empid;
                 }
 
                 $data['title'] = 'Claim with GST Admissions' . ' ' . date('F Y');
                 $data['pac_type'] = 2;
 
 // Determine if the authenticated user's role is 2
 $isRole2 = auth()->user()->role == 2;
 
 // Report Query with Conditional Filter
 $queryReport = Salespackage::join('users', 'users.id', '=', 'sales_package.student_id')
     ->join('ev_package', 'ev_package.pac_id', '=', 'sales_package.package_id')
     ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_package.student_id')
     ->where("sales_package.package_type", 2)
     ->where("sales_package.claim_status", 1)
     ->whereBetween("sales_package.joining_date", [$first_date, $today])
     ->orderby("sales_package.id", "desc")
     ->distinct();
 
 // Apply role-based filter if the user's role is 2
 if ($isRole2) {
     $queryReport->where("sales_package.sales_id", $user_id);
 }
 
 $data['report'] = $queryReport->get([
     'sales_package.*',
     'users.name',
     'ev_student_info.student_admissionid',
     'ev_package.pac_id',
     'ev_package.pac_name',
     'ev_package.pac_type'
 ]);
 
 // Calculations with Conditional Filter
 $data["claim_withgst"] = round(
     Salespackage::where("sales_package.package_type", 2)
     ->where("sales_package.claim_status", 1)
         ->whereBetween("sales_package.joining_date", [$first_date, $today])
         ->when($isRole2, function ($query) use ($user_id) {
             return $query->where("sales_package.sales_id", $user_id);
         })
         ->sum("sales_package.package_fullamount")
 );
 
 $data["claim_withoutgst"] = round(
     Salespackage::where("sales_package.package_type", 2)
     ->where("sales_package.claim_status", 1)
         ->whereBetween("sales_package.joining_date", [$first_date, $today])
         ->when($isRole2, function ($query) use ($user_id) {
             return $query->where("sales_package.sales_id", $user_id);
         })
         ->sum("sales_package.package_amount")
 );
 
 $data["claim_tax"] = round(
     Salespackage::where("sales_package.package_type", 2)
         ->whereBetween("sales_package.joining_date", [$first_date, $today])
         ->when($isRole2, function ($query) use ($user_id) {
             return $query->where("sales_package.sales_id", $user_id);
         })
         ->sum("sales_package.package_tax")
 );
 
 
 
 
                 
 
                
                 return view('claimwithgst_chrysallis', $data);
             } else {
                 return redirect('/acessdenied');
             }
         } else {
             return redirect('/');
         }
     }
     public function claim_withoutgst($empid = null)
     {
         if (Auth::check()) {
             if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                 $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                 $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));
 
                 $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                 $today = date("Y-m-d", strtotime($_GET['search_date2']));
             } else {
                 $today = date('Y-m-d');
                 $first_date = date('Y-m-01');
 
                 $data['search_date1'] = '';
                 $data['search_date2'] = '';
             }
 
             if (Auth::user()->role == 2 || Auth::user()->role == 1) {
                 if ($empid == null) {
                     $user_id = Auth::user()->id;
                     $data['empstatus'] = 0;
                 } else {
                     $user_id = $empid;
 
                     $data['subtitle'] = 'Reports of' . ucfirst(User::find($empid)->name);
                     $data['empstatus'] = 1;
                     $data['empid'] = $empid;
                 }
 
                 $data['title'] = 'Claim without GST Admissions' . ' ' . date('F Y');
                 $data['pac_type'] = 2;
 
 // Determine if the authenticated user's role is 2
 $isRole2 = auth()->user()->role == 2;
 
 // Report Query with Conditional Filter
 $queryReport = Salespackage::join('users', 'users.id', '=', 'sales_package.student_id')
     ->join('ev_package', 'ev_package.pac_id', '=', 'sales_package.package_id')
     ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_package.student_id')
     ->where("sales_package.package_type", 2)
     ->where("sales_package.claim_status", 1)
     ->whereBetween("sales_package.joining_date", [$first_date, $today])
     ->orderby("sales_package.id", "desc")
     ->distinct();
 
 // Apply role-based filter if the user's role is 2
 if ($isRole2) {
     $queryReport->where("sales_package.sales_id", $user_id);
 }
 
 $data['report'] = $queryReport->get([
     'sales_package.*',
     'users.name',
     'ev_student_info.student_admissionid',
     'ev_package.pac_id',
     'ev_package.pac_name',
     'ev_package.pac_type'
 ]);
 
 // Calculations with Conditional Filter
 $data["claim_withgst"] = round(
     Salespackage::where("sales_package.package_type", 2)
     ->where("sales_package.claim_status", 1)
         ->whereBetween("sales_package.joining_date", [$first_date, $today])
         ->when($isRole2, function ($query) use ($user_id) {
             return $query->where("sales_package.sales_id", $user_id);
         })
         ->sum("sales_package.package_fullamount")
 );
 
 $data["claim_withoutgst"] = round(
     Salespackage::where("sales_package.package_type", 2)
     ->where("sales_package.claim_status", 1)
         ->whereBetween("sales_package.joining_date", [$first_date, $today])
         ->when($isRole2, function ($query) use ($user_id) {
             return $query->where("sales_package.sales_id", $user_id);
         })
         ->sum("sales_package.package_amount")
 );
 
 $data["claim_tax"] = round(
     Salespackage::where("sales_package.package_type", 2)
     ->where("sales_package.claim_status", 1)
         ->whereBetween("sales_package.joining_date", [$first_date, $today])
         ->when($isRole2, function ($query) use ($user_id) {
             return $query->where("sales_package.sales_id", $user_id);
         })
         ->sum("sales_package.package_tax")
 );
 
 
 
 
                 
 
                
                 return view('claimwithoutgst_chrysallis', $data);
             } else {
                 return redirect('/acessdenied');
             }
         } else {
             return redirect('/');
         }
     }





    public function monthlyrevenuewithgst_chry($user_id = null)
    {
        if (Auth::check()) {
            if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));

                $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                $today = date("Y-m-d", strtotime($_GET['search_date2']));
            } else {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');

                $data['search_date1'] = '';
                $data['search_date2'] = '';
            }

            if (Auth::user()->role == 2 || Auth::user()->role == 1) {
                if ($user_id == null) {
                    $empid = Auth::user()->id;
                    $data['empstatus'] = 0;
                } else {
                    $empid = $user_id;

                    $data['subtitle'] = 'Reports of' . ucfirst(User::find($empid)->name);
                    $data['empstatus'] = 1;
                    $data['empid'] = $user_id;
                }

                $data['title'] = 'Chrysalis and Inspire With GST Revenue' . ' ' . date('F Y');
                $data['pac_type'] = 2;

                // Prepare common query conditions based on user role
                $query = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_payment.package_id')
                    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_payment.student_id')
                    ->where("sales_payment.package_type", 2)
                    ->whereBetween("sales_payment.date", [$first_date, $today]);

                // Apply role-based filtering
                if (Auth::user()->role == 2) {
                    $query->where("sales_payment.sales_id", $empid);
                }

                // Get the report data
                $data['report'] = $query
                    ->orderby("sales_payment.id", "desc")

                    ->get(['sales_payment.*', 'users.name', 'ev_student_info.student_admissionid', 'ev_package.pac_id', 'ev_package.pac_name', 'ev_package.pac_type']);

                // Calculate revenues with GST
                $data["monthrevenue_chry_withgst"] = round($query->sum("sales_payment.total"));

                // Calculate revenues without GST
                $data["monthrevenue_chry_withoutgst"] = round($query->sum("sales_payment.payment"));

                // Calculate tax
                $data["monthrevenue_chry_tax"] = round($query->sum("sales_payment.tax"));

                return view('chrysalisrevenue_monthlyreport_gst', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function monthlyrevenuewithoutgst_chry($user_id = null)
    {
        if (Auth::check()) {
            if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));

                $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                $today = date("Y-m-d", strtotime($_GET['search_date2']));
            } else {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');

                $data['search_date1'] = '';
                $data['search_date2'] = '';
            }

            if (Auth::user()->role == 2 || Auth::user()->role == 1) {
                if ($user_id == null) {
                    $empid = Auth::user()->id;
                    $data['empstatus'] = 0;
                } else {
                    $empid = $user_id;

                    $data['subtitle'] = 'Reports of' . ucfirst(User::find($empid)->name);
                    $data['empstatus'] = 1;
                    $data['empid'] = $user_id;
                }

                $data['title'] = 'Chrysalis and Inspire Without GST Revenue' . ' ' . date('F Y');
                $data['pac_type'] = 2;

                $baseQuery = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_payment.student_id')
                    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_payment.package_id')
                    ->where("sales_payment.package_type", 2)
                    ->whereBetween("sales_payment.date", [$first_date, $today]);

                // Apply role-based filtering if needed
                if (Auth::user()->role == 2) {
                    $baseQuery->where("sales_payment.sales_id", $empid);
                }

                // Get the report data
                $data['report'] = $baseQuery
                    ->orderby("sales_payment.id", "desc")

                    ->get(['sales_payment.*', 'users.name', 'ev_student_info.student_admissionid', 'ev_package.pac_id', 'ev_package.pac_name', 'ev_package.pac_type']);

                // Calculate revenues with GST
                $data["monthrevenue_chry_withgst"] = round($baseQuery->sum("sales_payment.total"));

                // Calculate revenues without GST
                $data["monthrevenue_chry_withoutgst"] = round($baseQuery->sum("sales_payment.payment"));

                // Calculate tax
                $data["monthrevenue_chry_tax"] = round($baseQuery->sum("sales_payment.tax"));

                return view('chrysalisrevenue_monthlyreport_withoutgst', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    //3

    public function monthlyrevenuewithgst_regular($uid = null)
    {
        if (Auth::check()) {
            if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));

                $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                $today = date("Y-m-d", strtotime($_GET['search_date2']));
            } else {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');

                $data['search_date1'] = '';
                $data['search_date2'] = '';
            }

            if (Auth::user()->role == 2 || Auth::user()->role == 1) {
                if ($uid == null) {
                    $user_id = Auth::user()->id;
                    $data['empstatus'] = 0;
                } else {
                    $user_id = $uid;

                    $data['subtitle'] = 'Reports of' . ucfirst(User::find($uid)->name);
                    $data['empstatus'] = 1;
                    $data['empid'] = $uid;
                }

                $data['title'] = 'Regular Admission with GST Revenue' . ' ' . date('F Y');
                $data['pac_type'] = 1;


                // Determine if the authenticated user's role is 2
$isRole2 = auth()->user()->role == 2;

// Report Query with Conditional Filter
$data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_payment.package_id')
    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_payment.student_id')
    ->when($isRole2, function ($query) use ($user_id) {
        $query->where('sales_payment.sales_id', $user_id);
    })
    ->where("sales_payment.package_type", 1)
    ->whereBetween("sales_payment.date", [$first_date, $today])
    ->orderby("sales_payment.id", "desc")
    ->distinct()
    ->get([
        'sales_payment.*', 
        'users.name', 
        'ev_student_info.student_admissionid', 
        'ev_package.pac_id', 
        'ev_package.pac_name', 
        'ev_package.pac_type'
    ]);

// Old Report Query with Conditional Filter
$data['oldreport'] = OldPayment::join('ev_oldstudents', 'ev_oldstudents.id', '=', 'olddata_payment.oldstudent_id')
    ->when($isRole2, function ($query) use ($user_id) {
        $query->where('olddata_payment.sales_id', $user_id);
    })
    ->whereBetween("olddata_payment.date", [$first_date, $today])
    ->get([
        'olddata_payment.paid_amount as total',
        'olddata_payment.fees_income as payment',
        'olddata_payment.tax',
        'olddata_payment.date',
        'ev_oldstudents.name',
        'ev_oldstudents.reg_no as student_admissionid',
        'ev_oldstudents.package as pac_name',
    ]);

// Revenue Calculations with Conditional Filter

$data["monthrevenue_regular_withoutgst"] = round(
    Salespayment::when($isRole2, function ($query) use ($user_id) {
        $query->where('sales_payment.sales_id', $user_id);
    })
    ->where("sales_payment.package_type", 1)
    ->whereBetween("sales_payment.date", [$first_date, $today])
    ->sum("sales_payment.payment")
);

$data["monthrevenue_old_withoutgst"] = round(
    OldPayment::when($isRole2, function ($query) use ($user_id) {
        $query->where('olddata_payment.sales_id', $user_id);
    })
    ->whereBetween("olddata_payment.date", [$first_date, $today])
    ->sum("olddata_payment.fees_income")
);

$data['monthrevenue_regularold_withoutgst'] = $data["monthrevenue_regular_withoutgst"] + $data["monthrevenue_old_withoutgst"];

$data["monthrevenue_regular_withgst"] = round(
    Salespayment::when($isRole2, function ($query) use ($user_id) {
        $query->where('sales_payment.sales_id', $user_id);
    })
    ->where("sales_payment.package_type", 1)
    ->whereBetween("sales_payment.date", [$first_date, $today])
    ->sum("sales_payment.total")
);

$data["monthrevenue_old_withgst"] = round(
    OldPayment::when($isRole2, function ($query) use ($user_id) {
        $query->where('olddata_payment.sales_id', $user_id);
    })
    ->whereBetween("olddata_payment.date", [$first_date, $today])
    ->sum("olddata_payment.paid_amount")
);

$data['monthrevenue_regularold_withgst'] = $data["monthrevenue_regular_withgst"] + $data["monthrevenue_old_withgst"];

$data["monthrevenue_regular_tax"] = round(
    Salespayment::when($isRole2, function ($query) use ($user_id) {
        $query->where('sales_payment.sales_id', $user_id);
    })
    ->where("sales_payment.package_type", 1)
    ->whereBetween("sales_payment.date", [$first_date, $today])
    ->sum("sales_payment.tax")
);

$data["monthrevenue_old_tax"] = round(
    OldPayment::when($isRole2, function ($query) use ($user_id) {
        $query->where('olddata_payment.sales_id', $user_id);
    })
    ->whereBetween("olddata_payment.date", [$first_date, $today])
    ->sum("olddata_payment.tax")
);

$data['monthrevenue_regularold_tax'] = $data["monthrevenue_regular_tax"] + $data["monthrevenue_old_tax"];


                

                return view('monthlyregoldwithgst_revenue', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    //4

    public function monthlyrevenuewithoutgst_regular($empid = null)
    {
        if (Auth::check()) {

            if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));

                $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                $today = date("Y-m-d", strtotime($_GET['search_date2']));
            } else {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');

                $data['search_date1'] = '';
                $data['search_date2'] = '';
            }

            if (Auth::user()->role == 2 || Auth::user()->role == 1) {

                if ($empid == null) {
                    $user_id = Auth::user()->id;
                    $data['empstatus'] = 0;
                } else {
                    $user_id = $empid;

                    $data['subtitle'] = 'Reports of' . ucfirst(User::find($empid)->name);
                    $data['empstatus'] = 1;
                    $data['empid'] = $empid;
                }

                $data['title'] = 'Regular Admission without GST Revenue' . ' ' . date('F Y');
                $data['pac_type'] = 1;

 // Determine if the authenticated user's role is 2
 $isRole2 = auth()->user()->role == 2;

 // Report Query with Conditional Filter
 $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
     ->join('ev_package', 'ev_package.pac_id', '=', 'sales_payment.package_id')
     ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_payment.student_id')
     ->when($isRole2, function ($query) use ($user_id) {
         $query->where('sales_payment.sales_id', $user_id);
     })
     ->where("sales_payment.package_type", 1)
     ->whereBetween("sales_payment.date", [$first_date, $today])
     ->orderby("sales_payment.id", "desc")
     ->distinct()
     ->get([
         'sales_payment.*', 
         'users.name', 
         'ev_student_info.student_admissionid', 
         'ev_package.pac_id', 
         'ev_package.pac_name', 
         'ev_package.pac_type'
     ]);
 
 // Old Report Query with Conditional Filter
 $data['oldreport'] = OldPayment::join('ev_oldstudents', 'ev_oldstudents.id', '=', 'olddata_payment.oldstudent_id')
     ->when($isRole2, function ($query) use ($user_id) {
         $query->where('olddata_payment.sales_id', $user_id);
     })
     ->whereBetween("olddata_payment.date", [$first_date, $today])
     ->get([
         'olddata_payment.paid_amount as total',
         'olddata_payment.fees_income as payment',
         'olddata_payment.tax',
         'olddata_payment.date',
         'ev_oldstudents.name',
         'ev_oldstudents.reg_no as student_admissionid',
         'ev_oldstudents.package as pac_name',
     ]);
 
 // Revenue Calculations with Conditional Filter
 
 $data["monthrevenue_regular_withoutgst"] = round(
     Salespayment::when($isRole2, function ($query) use ($user_id) {
         $query->where('sales_payment.sales_id', $user_id);
     })
     ->where("sales_payment.package_type", 1)
     ->whereBetween("sales_payment.date", [$first_date, $today])
     ->sum("sales_payment.payment")
 );
 
 $data["monthrevenue_old_withoutgst"] = round(
     OldPayment::when($isRole2, function ($query) use ($user_id) {
         $query->where('olddata_payment.sales_id', $user_id);
     })
     ->whereBetween("olddata_payment.date", [$first_date, $today])
     ->sum("olddata_payment.fees_income")
 );
 
 $data['monthrevenue_regularold_withoutgst'] = $data["monthrevenue_regular_withoutgst"] + $data["monthrevenue_old_withoutgst"];
 
 $data["monthrevenue_regular_withgst"] = round(
     Salespayment::when($isRole2, function ($query) use ($user_id) {
         $query->where('sales_payment.sales_id', $user_id);
     })
     ->where("sales_payment.package_type", 1)
     ->whereBetween("sales_payment.date", [$first_date, $today])
     ->sum("sales_payment.total")
 );
 
 $data["monthrevenue_old_withgst"] = round(
     OldPayment::when($isRole2, function ($query) use ($user_id) {
         $query->where('olddata_payment.sales_id', $user_id);
     })
     ->whereBetween("olddata_payment.date", [$first_date, $today])
     ->sum("olddata_payment.paid_amount")
 );
 
 $data['monthrevenue_regularold_withgst'] = $data["monthrevenue_regular_withgst"] + $data["monthrevenue_old_withgst"];
 
 $data["monthrevenue_regular_tax"] = round(
     Salespayment::when($isRole2, function ($query) use ($user_id) {
         $query->where('sales_payment.sales_id', $user_id);
     })
     ->where("sales_payment.package_type", 1)
     ->whereBetween("sales_payment.date", [$first_date, $today])
     ->sum("sales_payment.tax")
 );
 
 $data["monthrevenue_old_tax"] = round(
     OldPayment::when($isRole2, function ($query) use ($user_id) {
         $query->where('olddata_payment.sales_id', $user_id);
     })
     ->whereBetween("olddata_payment.date", [$first_date, $today])
     ->sum("olddata_payment.tax")
 );
 
 $data['monthrevenue_regularold_tax'] = $data["monthrevenue_regular_tax"] + $data["monthrevenue_old_tax"];




               

                return view('monthlyregoldwithoutgst_revenue', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    //5

    public function totalrevenue_withgst($empid = null)
    {
        if (Auth::check()) {
            if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));
                $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                $today = date("Y-m-d", strtotime($_GET['search_date2']));
            } else {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');

                $data['search_date1'] = '';
                $data['search_date2'] = '';
            }

            if (Auth::user()->role == 2 || Auth::user()->role == 1) {
                if ($empid == null) {
                    $user_id = Auth::user()->id;
                    $data['empstatus'] = 0;
                } else {
                    $user_id = $empid;

                    $data['subtitle'] = 'Reports of' . ucfirst(User::find($empid)->name);
                    $data['empstatus'] = 1;
                    $data['empid'] = $empid;
                }

                $data['title'] = 'Total Revenue With GST' . ' ' . date('F Y');
                $data['pac_type'] = 1;

                
// Determine if the authenticated user's role is 2
$isRole2 = auth()->user()->role == 2;

// Report Query with Conditional Filter
$data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_payment.package_id')
    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_payment.student_id')
    ->where(function ($query) use ($isRole2, $user_id) {
        if ($isRole2) {
            $query->where('sales_payment.sales_id', $user_id);
        }
    })
    ->where(function ($query) {
        $query->where('sales_payment.package_type', 1)
              ->orWhere('sales_payment.package_type', 2);
    })
    ->whereBetween("sales_payment.date", [$first_date, $today])
    ->orderby("sales_payment.id", "desc")
    ->distinct()
    ->get(['sales_payment.*', 'users.name', 'ev_student_info.student_admissionid', 'ev_package.pac_id', 'ev_package.pac_name', 'ev_package.pac_type']);

// Old Report Query with Conditional Filter
$data['oldreport'] = OldPayment::join('ev_oldstudents', 'ev_oldstudents.id', '=', 'olddata_payment.oldstudent_id')
    ->where(function ($query) use ($isRole2, $user_id) {
        if ($isRole2) {
            $query->where('olddata_payment.sales_id', $user_id);
        }
    })
    ->whereBetween("olddata_payment.date", [$first_date, $today])
    ->get([
        'olddata_payment.paid_amount as total',
        'olddata_payment.fees_income as payment',
        'olddata_payment.tax',
        'olddata_payment.date',
        'ev_oldstudents.name',
        'ev_oldstudents.reg_no as student_admissionid',
        'ev_oldstudents.package as pac_name',
    ]);

// Revenue Calculations with Conditional Filter

$data["monthrevenue_regular_withoutgst"] = round(
    Salespayment::where(function ($query) use ($isRole2, $user_id) {
        if ($isRole2) {
            $query->where('sales_payment.sales_id', $user_id);
        }
        $query->where('sales_payment.package_type', 1)
              ->orWhere('sales_payment.package_type', 2);
    })
    ->whereBetween("sales_payment.date", [$first_date, $today])
    ->sum("sales_payment.payment")
);

$data["monthrevenue_old_withoutgst"] = round(
    OldPayment::where(function ($query) use ($isRole2, $user_id) {
        if ($isRole2) {
            $query->where('olddata_payment.sales_id', $user_id);
        }
    })
    ->whereBetween("olddata_payment.date", [$first_date, $today])
    ->sum("olddata_payment.fees_income")
);

$data['monthrevenue_regularold_withoutgst'] = $data["monthrevenue_regular_withoutgst"] + $data["monthrevenue_old_withoutgst"];

$data["monthrevenue_regular_withgst"] = round(
    Salespayment::where(function ($query) use ($isRole2, $user_id) {
        if ($isRole2) {
            $query->where('sales_payment.sales_id', $user_id);
        }
        $query->where('sales_payment.package_type', 1)
              ->orWhere('sales_payment.package_type', 2);
    })
    ->whereBetween("sales_payment.date", [$first_date, $today])
    ->sum("sales_payment.total")
);

$data["monthrevenue_old_withgst"] = round(
    OldPayment::where(function ($query) use ($isRole2, $user_id) {
        if ($isRole2) {
            $query->where('olddata_payment.sales_id', $user_id);
        }
    })
    ->whereBetween("olddata_payment.date", [$first_date, $today])
    ->sum("olddata_payment.paid_amount")
);

$data['monthrevenue_regularold_withgst'] = $data["monthrevenue_regular_withgst"] + $data["monthrevenue_old_withgst"];

$data["monthrevenue_regular_tax"] = round(
    Salespayment::where(function ($query) use ($isRole2, $user_id) {
        if ($isRole2) {
            $query->where('sales_payment.sales_id', $user_id);
        }
        $query->where('sales_payment.package_type', 1)
              ->orWhere('sales_payment.package_type', 2);
    })
    ->whereBetween("sales_payment.date", [$first_date, $today])
    ->sum("sales_payment.tax")
);

$data["monthrevenue_old_tax"] = round(
    OldPayment::where(function ($query) use ($isRole2, $user_id) {
        if ($isRole2) {
            $query->where('olddata_payment.sales_id', $user_id);
        }
    })
    ->whereBetween("olddata_payment.date", [$first_date, $today])
    ->sum("olddata_payment.tax")
);

$data['monthrevenue_regularold_tax'] = $data["monthrevenue_regular_tax"] + $data["monthrevenue_old_tax"];







                return view('totalrevenue_withgst', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }
    //6

    public function totalrevenue_withoutgst($empid = null)
    {
        if (Auth::check()) {
            if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));
                $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                $today = date("Y-m-d", strtotime($_GET['search_date2']));
            } else {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');

                $data['search_date1'] = '';
                $data['search_date2'] = '';
            }

            if (Auth::user()->role == 2 || Auth::user()->role == 1) {
                if ($empid == null) {
                    $user_id = Auth::user()->id;
                    $data['empstatus'] = 0;
                } else {
                    $user_id = $empid;

                    $data['subtitle'] = 'Reports of' . ucfirst(User::find($empid)->name);
                    $data['empstatus'] = 1;
                    $data['empid'] = $empid;
                }

                $data['title'] = 'Total Revenue Without GST' . ' ' . date('F Y');
                $data['pac_type'] = 1;


                // Check if the authenticated user's role is 2
$isRole2 = auth()->user()->role == 2;

// Report Query with Conditional Filter
$data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_payment.package_id')
    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_payment.student_id')
    ->when($isRole2, function ($query) use ($user_id) {
        $query->where('sales_payment.sales_id', $user_id);
    })
    ->where(function ($query) {
        $query->where('sales_payment.package_type', 1)
              ->orWhere('sales_payment.package_type', 2);
    })
    ->whereBetween("sales_payment.date", [$first_date, $today])
    ->orderby("sales_payment.id", "desc")
    ->distinct()
    ->get(['sales_payment.*', 'users.name', 'ev_student_info.student_admissionid', 'ev_package.pac_id', 'ev_package.pac_name', 'ev_package.pac_type']);

// Old Report Query with Conditional Filter
$data['oldreport'] = OldPayment::join('ev_oldstudents', 'ev_oldstudents.id', '=', 'olddata_payment.oldstudent_id')
    ->when($isRole2, function ($query) use ($user_id) {
        $query->where('olddata_payment.sales_id', $user_id);
    })
    ->whereBetween("olddata_payment.date", [$first_date, $today])
    ->get([
        'olddata_payment.paid_amount as total',
        'olddata_payment.fees_income as payment',
        'olddata_payment.tax',
        'olddata_payment.date',
        'ev_oldstudents.name',
        'ev_oldstudents.reg_no as student_admissionid',
        'ev_oldstudents.package as pac_name',
    ]);

// Revenue Calculations with Conditional Filter

$data["monthrevenue_regular_withoutgst"] = round(
    Salespayment::when($isRole2, function ($query) use ($user_id) {
        $query->where('sales_payment.sales_id', $user_id);
    })
    ->where(function ($query) {
        $query->where('sales_payment.package_type', 1)
              ->orWhere('sales_payment.package_type', 2);
    })
    ->whereBetween("sales_payment.date", [$first_date, $today])
    ->sum("sales_payment.payment")
);

$data["monthrevenue_old_withoutgst"] = round(
    OldPayment::when($isRole2, function ($query) use ($user_id) {
        $query->where('olddata_payment.sales_id', $user_id);
    })
    ->whereBetween("olddata_payment.date", [$first_date, $today])
    ->sum("olddata_payment.fees_income")
);

$data['monthrevenue_regularold_withoutgst'] = $data["monthrevenue_regular_withoutgst"] + $data["monthrevenue_old_withoutgst"];

$data["monthrevenue_regular_withgst"] = round(
    Salespayment::when($isRole2, function ($query) use ($user_id) {
        $query->where('sales_payment.sales_id', $user_id);
    })
    ->where(function ($query) {
        $query->where('sales_payment.package_type', 1)
              ->orWhere('sales_payment.package_type', 2);
    })
    ->whereBetween("sales_payment.date", [$first_date, $today])
    ->sum("sales_payment.total")
);

$data["monthrevenue_old_withgst"] = round(
    OldPayment::when($isRole2, function ($query) use ($user_id) {
        $query->where('olddata_payment.sales_id', $user_id);
    })
    ->whereBetween("olddata_payment.date", [$first_date, $today])
    ->sum("olddata_payment.paid_amount")
);

$data['monthrevenue_regularold_withgst'] = $data["monthrevenue_regular_withgst"] + $data["monthrevenue_old_withgst"];

$data["monthrevenue_regular_tax"] = round(
    Salespayment::when($isRole2, function ($query) use ($user_id) {
        $query->where('sales_payment.sales_id', $user_id);
    })
    ->where(function ($query) {
        $query->where('sales_payment.package_type', 1)
              ->orWhere('sales_payment.package_type', 2);
    })
    ->whereBetween("sales_payment.date", [$first_date, $today])
    ->sum("sales_payment.tax")
);

$data["monthrevenue_old_tax"] = round(
    OldPayment::when($isRole2, function ($query) use ($user_id) {
        $query->where('olddata_payment.sales_id', $user_id);
    })
    ->whereBetween("olddata_payment.date", [$first_date, $today])
    ->sum("olddata_payment.tax")
);

$data['monthrevenue_regularold_tax'] = $data["monthrevenue_regular_tax"] + $data["monthrevenue_old_tax"];


               





                return view('totalrevenue_withoutgst', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    //7

    public function monthlyadmissionwithgst_chrysalis($empid = null)
    {
        if (Auth::check()) {
            if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));

                $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                $today = date("Y-m-d", strtotime($_GET['search_date2']));
            } else {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');

                $data['search_date1'] = '';
                $data['search_date2'] = '';
            }

            if (Auth::user()->role == 2 || Auth::user()->role == 1) {
                if ($empid == null) {
                    $user_id = Auth::user()->id;
                    $data['empstatus'] = 0;
                } else {
                    $user_id = $empid;

                    $data['subtitle'] = 'Reports of' . ucfirst(User::find($empid)->name);
                    $data['empstatus'] = 1;
                    $data['empid'] = $empid;
                }

                $data['title'] = 'Chrysalis and Inspire with GST Admissions' . ' ' . date('F Y');
                $data['pac_type'] = 2;

// Determine if the authenticated user's role is 2
$isRole2 = auth()->user()->role == 2;

// Report Query with Conditional Filter
$queryReport = Salespackage::join('users', 'users.id', '=', 'sales_package.student_id')
    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_package.package_id')
    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_package.student_id')
    ->where("sales_package.package_type", 2)
    ->whereBetween("sales_package.joining_date", [$first_date, $today])
    ->orderby("sales_package.id", "desc")
    ->distinct();

// Apply role-based filter if the user's role is 2
if ($isRole2) {
    $queryReport->where("sales_package.sales_id", $user_id);
}

$data['report'] = $queryReport->get([
    'sales_package.*',
    'users.name',
    'ev_student_info.student_admissionid',
    'ev_package.pac_id',
    'ev_package.pac_name',
    'ev_package.pac_type'
]);

// Calculations with Conditional Filter
$data["monthadmi_chry_withgst"] = round(
    Salespackage::where("sales_package.package_type", 2)
        ->whereBetween("sales_package.joining_date", [$first_date, $today])
        ->when($isRole2, function ($query) use ($user_id) {
            return $query->where("sales_package.sales_id", $user_id);
        })
        ->sum("sales_package.package_fullamount")
);

$data["monthadmi_chry_withoutgst"] = round(
    Salespackage::where("sales_package.package_type", 2)
        ->whereBetween("sales_package.joining_date", [$first_date, $today])
        ->when($isRole2, function ($query) use ($user_id) {
            return $query->where("sales_package.sales_id", $user_id);
        })
        ->sum("sales_package.package_amount")
);

$data["monthadmi_chry_tax"] = round(
    Salespackage::where("sales_package.package_type", 2)
        ->whereBetween("sales_package.joining_date", [$first_date, $today])
        ->when($isRole2, function ($query) use ($user_id) {
            return $query->where("sales_package.sales_id", $user_id);
        })
        ->sum("sales_package.package_tax")
);




                

               
                return view('admissionwithgst_chrysalis', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    //8

    public function monthlyadmissionwithoutgst_chrysalis($empid = null)
    {
        if (Auth::check()) {
            if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));

                $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                $today = date("Y-m-d", strtotime($_GET['search_date2']));
            } else {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');

                $data['search_date1'] = '';
                $data['search_date2'] = '';
            }

            if (Auth::user()->role == 2 || Auth::user()->role == 1) {
                if ($empid == null) {
                    $user_id = Auth::user()->id;
                    $data['empstatus'] = 0;
                } else {
                    $user_id = $empid;
                    $data['subtitle'] = 'Reports of' . ucfirst(User::find($empid)->name);
                    $data['empstatus'] = 1;
                    $data['empid'] = $empid;
                }

                $data['title'] = 'Chrysalis and Inspire without GST Admissions' . ' ' . date('F Y');
                $data['pac_type'] = 2;


// Determine if the authenticated user's role is 2
$isRole2 = auth()->user()->role == 2;

// Report Query with Conditional Filter
$data['report'] = Salespackage::join('users', 'users.id', '=', 'sales_package.student_id')
    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_package.package_id')
    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_package.student_id')
    ->where("sales_package.package_type", 2)
    ->whereBetween("sales_package.joining_date", [$first_date, $today])
    ->when($isRole2, function ($query) use ($user_id) {
        return $query->where("sales_package.sales_id", $user_id);
    })
    ->orderby("sales_package.id", "desc")
    ->distinct()
    ->get([
        'sales_package.*',
        'users.name',
        'ev_student_info.student_admissionid',
        'ev_package.pac_id',
        'ev_package.pac_name',
        'ev_package.pac_type'
    ]);

// Calculations with Conditional Filter
$data["monthadmi_chry_withgst"] = round(
    Salespackage::where("sales_package.package_type", 2)
        ->whereBetween("sales_package.joining_date", [$first_date, $today])
        ->when($isRole2, function ($query) use ($user_id) {
            return $query->where("sales_package.sales_id", $user_id);
        })
        ->sum("sales_package.package_fullamount")
);

$data["monthadmi_chry_withoutgst"] = round(
    Salespackage::where("sales_package.package_type", 2)
        ->whereBetween("sales_package.joining_date", [$first_date, $today])
        ->when($isRole2, function ($query) use ($user_id) {
            return $query->where("sales_package.sales_id", $user_id);
        })
        ->sum("sales_package.package_amount")
);

$data["monthadmi_chry_tax"] = round(
    Salespackage::where("sales_package.package_type", 2)
        ->whereBetween("sales_package.joining_date", [$first_date, $today])
        ->when($isRole2, function ($query) use ($user_id) {
            return $query->where("sales_package.sales_id", $user_id);
        })
        ->sum("sales_package.package_tax")
);




                

                return view('admissionwithoutgst_chrysalis', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    //9

    public function monthlyadmissionwithgst_regular($empid = null)
    {
        if (Auth::check()) {
            if (isset($_GET["search_date1"]) != "" && isset($_GET["search_date2"]) != "") {
                $data["search_date1"] = date("d-m-Y", strtotime($_GET["search_date1"]));
                $data["search_date2"] = date("d-m-Y", strtotime($_GET["search_date2"]));

                $first_date = date("Y-m-d", strtotime($_GET["search_date1"]));
                $today = date("Y-m-d", strtotime($_GET["search_date2"]));
            } else {
                $today = date("Y-m-d");
                $first_date = date("Y-m-01");

                $data["search_date1"] = "";
                $data["search_date2"] = "";
            }
            if (Auth::user()->role == 2 || Auth::user()->role == 1) {
                if ($empid == null) {
                    $user_id = Auth::user()->id;
                    $data['empstatus'] = 0;
                } else {
                    $user_id = $empid;
                    $data['subtitle'] = 'Reports of' . ucfirst(User::find($empid)->name);
                    $data['empstatus'] = 1;
                    $data['empid'] = $empid;
                }

                $data["title"] = "Regular Admission with GST" . " " . date("F Y");
                $data["pac_type"] = 1;


                // Determine if the authenticated user's role is 2
$isRole2 = auth()->user()->role == 2;

// Report Query with Conditional Filter
$queryReport = Salespackage::join("users", "users.id", "=", "sales_package.student_id")
    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_package.package_id')
    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_package.student_id')
    ->where("sales_package.package_type", 1)
    ->whereBetween("sales_package.joining_date", [$first_date, $today])
    ->orderby("sales_package.id", "desc")
    ->distinct();

// Apply role-based filter if the user's role is 2
if ($isRole2) {
    $queryReport->where("sales_package.sales_id", $user_id);
}

$data["report"] = $queryReport->get([
    "sales_package.id",
    "sales_package.package_amount as payment",
    "sales_package.package_tax as tax",
    "sales_package.package_fullamount as total",
    "sales_package.joining_date as date",
    "users.name",
    "ev_student_info.student_admissionid",
    "ev_package.pac_name",
]);

// Old Report Query with Conditional Filter
$queryOldReport = Oldstudent::whereBetween("date_joining", [$first_date, $today]);

// Apply role-based filter if the user's role is 2
if ($isRole2) {
    $queryOldReport->where("sales_id", $user_id);
}

$data["oldreport"] = $queryOldReport->get([
    "ev_oldstudents.name",
    "ev_oldstudents.reg_no as student_admissionid",
    "ev_oldstudents.package as pac_name",
    "ev_oldstudents.fees as payment",
    "ev_oldstudents.tax",
    "ev_oldstudents.totalfees as total",
    "ev_oldstudents.date_joining as date",
]);

// Calculations with Conditional Filter
$data["monthadmi_regular_withgst"] = round(
    Salespackage::where("sales_package.package_type", 1)
        ->whereBetween("sales_package.joining_date", [$first_date, $today])
        ->when($isRole2, function ($query) use ($user_id) {
            return $query->where("sales_package.sales_id", $user_id);
        })
        ->sum("sales_package.package_fullamount")
);

$data["reg_old_withgst"] = round(
    Oldstudent::whereBetween("date_joining", [$first_date, $today])
        ->when($isRole2, function ($query) use ($user_id) {
            return $query->where("sales_id", $user_id);
        })
        ->sum("totalfees")
);

$data["monthly_tot_admissionwithgst"] = $data["monthadmi_regular_withgst"] + $data["reg_old_withgst"];

$data["monthadmi_regular_withoutgst"] = round(
    Salespackage::where("sales_package.package_type", 1)
        ->whereBetween("sales_package.joining_date", [$first_date, $today])
        ->when($isRole2, function ($query) use ($user_id) {
            return $query->where("sales_package.sales_id", $user_id);
        })
        ->sum("sales_package.package_amount")
);

$data['reg_old_withoutgst'] = round(
    Oldstudent::whereBetween("date_joining", [$first_date, $today])
        ->when($isRole2, function ($query) use ($user_id) {
            return $query->where("sales_id", $user_id);
        })
        ->sum("fees")
);

$data['monthly_tot_admissionwithoutgst'] = $data["monthadmi_regular_withoutgst"] + $data['reg_old_withoutgst'];

$data["monthadmi_regular_tax"] = round(
    Salespackage::where("sales_package.package_type", 1)
        ->whereBetween("sales_package.joining_date", [$first_date, $today])
        ->when($isRole2, function ($query) use ($user_id) {
            return $query->where("sales_package.sales_id", $user_id);
        })
        ->sum("sales_package.package_tax")
);

$data['reg_old_tax'] = round(
    Oldstudent::whereBetween("date_joining", [$first_date, $today])
        ->when($isRole2, function ($query) use ($user_id) {
            return $query->where("sales_id", $user_id);
        })
        ->sum("tax")
);

$data['monthly_tot_admissiontax'] = $data["monthadmi_regular_tax"] + $data['reg_old_tax'];








                
                return view("monthlyregoldwithgst_admission", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    //10

    public function monthlyadmissionwithoutgst_regular($empid = null)
    {
        if (Auth::check()) {
            if (isset($_GET["search_date1"]) != "" && isset($_GET["search_date2"]) != "") {
                $data["search_date1"] = date("d-m-Y", strtotime($_GET["search_date1"]));
                $data["search_date2"] = date("d-m-Y", strtotime($_GET["search_date2"]));

                $first_date = date("Y-m-d", strtotime($_GET["search_date1"]));
                $today = date("Y-m-d", strtotime($_GET["search_date2"]));
            } else {
                $today = date("Y-m-d");
                $first_date = date("Y-m-01");

                $data["search_date1"] = "";
                $data["search_date2"] = "";
            }

            if (Auth::user()->role == 2 || Auth::user()->role == 1) {
                if ($empid == null) {
                    $user_id = Auth::user()->id;
                    $data['empstatus'] = 0;
                } else {
                    $user_id = $empid;

                    $data['subtitle'] = 'Reports of' . ucfirst(User::find($empid)->name);
                    $data['empstatus'] = 1;
                    $data['empid'] = $empid;
                }

                $data["title"] = "Regular Admission without GST" . " " . date("F Y");
                $data["pac_type"] = 1;


// Determine if the authenticated user's role is 2
$isRole2 = auth()->user()->role == 2;

// Report Query with Conditional Filter
$queryReport = Salespackage::join("users", "users.id", "=", "sales_package.student_id")
    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_package.package_id')
    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_package.student_id')
    ->where("sales_package.package_type", 1)
    ->whereBetween("sales_package.joining_date", [$first_date, $today])
    ->orderby("sales_package.id", "desc")
    ->distinct();

// Apply role-based filter if the user's role is 2
if ($isRole2) {
    $queryReport->where("sales_package.sales_id", $user_id);
}

$data["report"] = $queryReport->get([
    "sales_package.id",
    "sales_package.package_amount as payment",
    "sales_package.package_tax as tax",
    "sales_package.package_fullamount as total",
    "sales_package.joining_date as date",
    "users.name",
    "ev_student_info.student_admissionid",
    "ev_package.pac_name",
]);

// Old Report Query with Conditional Filter
$queryOldReport = Oldstudent::whereBetween("date_joining", [$first_date, $today]);

// Apply role-based filter if the user's role is 2
if ($isRole2) {
    $queryOldReport->where("sales_id", $user_id);
}

$data["oldreport"] = $queryOldReport->get([
    "ev_oldstudents.name",
    "ev_oldstudents.reg_no as student_admissionid",
    "ev_oldstudents.package as pac_name",
    "ev_oldstudents.fees as payment",
    "ev_oldstudents.tax",
    "ev_oldstudents.totalfees as total",
    "ev_oldstudents.date_joining as date",
]);

// Calculations with Conditional Filter
$data["monthadmi_regular_withgst"] = round(
    Salespackage::where("sales_package.package_type", 1)
        ->whereBetween("sales_package.joining_date", [$first_date, $today])
        ->when($isRole2, function ($query) use ($user_id) {
            return $query->where("sales_package.sales_id", $user_id);
        })
        ->sum("sales_package.package_fullamount")
);

$data["reg_old_withgst"] = round(
    Oldstudent::whereBetween("date_joining", [$first_date, $today])
        ->when($isRole2, function ($query) use ($user_id) {
            return $query->where("sales_id", $user_id);
        })
        ->sum("totalfees")
);

$data["monthly_tot_admissionwithgst"] = $data["monthadmi_regular_withgst"] + $data["reg_old_withgst"];

$data["monthadmi_regular_withoutgst"] = round(
    Salespackage::where("sales_package.package_type", 1)
        ->whereBetween("sales_package.joining_date", [$first_date, $today])
        ->when($isRole2, function ($query) use ($user_id) {
            return $query->where("sales_package.sales_id", $user_id);
        })
        ->sum("sales_package.package_amount")
);

$data['reg_old_withoutgst'] = round(
    Oldstudent::whereBetween("date_joining", [$first_date, $today])
        ->when($isRole2, function ($query) use ($user_id) {
            return $query->where("sales_id", $user_id);
        })
        ->sum("fees")
);

$data['monthly_tot_admissionwithoutgst'] = $data["monthadmi_regular_withoutgst"] + $data['reg_old_withoutgst'];

$data["monthadmi_regular_tax"] = round(
    Salespackage::where("sales_package.package_type", 1)
        ->whereBetween("sales_package.joining_date", [$first_date, $today])
        ->when($isRole2, function ($query) use ($user_id) {
            return $query->where("sales_package.sales_id", $user_id);
        })
        ->sum("sales_package.package_tax")
);

$data['reg_old_tax'] = round(
    Oldstudent::whereBetween("date_joining", [$first_date, $today])
        ->when($isRole2, function ($query) use ($user_id) {
            return $query->where("sales_id", $user_id);
        })
        ->sum("tax")
);

$data['monthly_tot_admissiontax'] = $data["monthadmi_regular_tax"] + $data['reg_old_tax'];








                

               

                return view("monthlyregoldwithoutgst_admission", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    //11

    public function totaladmission_withgst($empid = null)
    {
        if (Auth::check()) {


            if (isset($_GET["search_date1"]) != "" && isset($_GET["search_date2"]) != "") {
                $data["search_date1"] = date("d-m-Y", strtotime($_GET["search_date1"]));
                $data["search_date2"] = date("d-m-Y", strtotime($_GET["search_date2"]));

                $first_date = date("Y-m-d", strtotime($_GET["search_date1"]));
                $today = date("Y-m-d", strtotime($_GET["search_date2"]));
            } else {
                $today = date("Y-m-d");
                $first_date = date("Y-m-01");

                $data["search_date1"] = "";
                $data["search_date2"] = "";
            }

            if (Auth::user()->role == 2 || Auth::user()->role == 1) {
                if ($empid == null) {
                    $user_id = Auth::user()->id;
                    $data['empstatus'] = 0;
                } else {
                    $user_id = $empid;

                    $data['subtitle'] = 'Reports of' . ucfirst(User::find($empid)->name);
                    $data['empstatus'] = 1;
                    $data['empid'] = $empid;
                }

                $data["title"] = "Total Admission with GST" . " " . date("F Y");


                $query = Salespackage::join("users", "users.id", "=", "sales_package.student_id")
    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_package.student_id')
    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_package.package_id')
    ->where(function ($query) {
        $query->orWhere('sales_package.package_type', 1)
              ->orWhere('sales_package.package_type', 2);
    })
    ->whereBetween("sales_package.joining_date", [$first_date, $today])
    ->orderby("sales_package.id", "desc")
    ->distinct();

if (auth()->user()->role == 2) {
    $query->where("sales_package.sales_id", $user_id);
}

$data["report"] = $query->get([
    "sales_package.id",
    "sales_package.package_amount as payment",
    "sales_package.package_tax as tax",
    "sales_package.package_fullamount as total",
    "sales_package.joining_date as date",
    "users.name",
    "ev_student_info.student_admissionid",
    "ev_package.pac_name",
]);



// Define the role-based condition
$isRole2 = auth()->user()->role == 2;

$query = Oldstudent::whereBetween("date_joining", [$first_date, $today]);

// Apply role-based filter to Oldstudent if role is 2
if ($isRole2) {
    $query->where("sales_id", $user_id);
}

$data["oldreport"] = $query->get([
    "ev_oldstudents.name",
    "ev_oldstudents.reg_no as student_admissionid",
    "ev_oldstudents.package as pac_name",
    "ev_oldstudents.fees as payment",
    "ev_oldstudents.tax",
    "ev_oldstudents.totalfees as total",
    "ev_oldstudents.date_joining as date",
]);

$query = Salespackage::where(function ($query) {
    $query->orWhere('sales_package.package_type', 1)
          ->orWhere('sales_package.package_type', 2);
})
->whereBetween("sales_package.joining_date", [$first_date, $today]);

// Apply role-based filter to Salespackage if role is 2
if ($isRole2) {
    $query->where("sales_package.sales_id", $user_id);
}

$data["monthadmi_regular_withgst"] = round(
    $query->sum("sales_package.package_fullamount")
);

$data["reg_old_withgst"] = round(
    Oldstudent::whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
    ->sum("ev_oldstudents.totalfees")
);

$data["monthly_tot_admissionwithgst"] = $data["monthadmi_regular_withgst"] + $data["reg_old_withgst"];

$data["monthadmi_regular_withoutgst"] = round(
    $query->sum("sales_package.package_amount")
);

$data['reg_old_withoutgst'] = round(
    Oldstudent::whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
    ->sum("ev_oldstudents.fees")
);

$data['monthly_tot_admissionwithoutgst'] = $data["monthadmi_regular_withoutgst"] + $data['reg_old_withoutgst'];

$data["monthadmi_regular_tax"] = round(
    $query->sum("sales_package.package_tax")
);

$data['reg_old_tax'] = round(
    Oldstudent::whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
    ->sum("ev_oldstudents.tax")
);

$data['monthly_tot_admissiontax'] = $data["monthadmi_regular_tax"] + $data['reg_old_tax'];












                

                return view("totaladmission_withgst", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    //12

    public function totaladmission_withoutgst($empid = null)
    {
        if (Auth::check()) {
            if (isset($_GET["search_date1"]) != "" && isset($_GET["search_date2"]) != "") {
                $data["search_date1"] = date("d-m-Y", strtotime($_GET["search_date1"]));
                $data["search_date2"] = date("d-m-Y", strtotime($_GET["search_date2"]));

                $first_date = date("Y-m-d", strtotime($_GET["search_date1"]));
                $today = date("Y-m-d", strtotime($_GET["search_date2"]));
            } else {
                $today = date("Y-m-d");
                $first_date = date("Y-m-01");

                $data["search_date1"] = "";
                $data["search_date2"] = "";
            }

            if (Auth::user()->role == 2 || Auth::user()->role == 1) {
                if ($empid == null) {
                    $user_id = Auth::user()->id;
                    $data['empstatus'] = 0;
                } else {
                    $user_id = $empid;

                    $data['subtitle'] = 'Reports of' . ucfirst(User::find($empid)->name);
                    $data['empstatus'] = 1;
                    $data['empid'] = $empid;
                }

                $data["title"] = "Total Admission without GST" . " " . date("F Y");

// Determine if the authenticated user's role is 2
$isRole2 = auth()->user()->role == 2;

// Query for Salespackage with role-based condition
$query = Salespackage::join("users", "users.id", "=", "sales_package.student_id")
    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_package.student_id')
    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_package.package_id')
    ->where(function ($query) {
        $query->orWhere('sales_package.package_type', 1)
              ->orWhere('sales_package.package_type', 2);
    })
    ->whereBetween("sales_package.joining_date", [$first_date, $today])
    ->orderby("sales_package.id", "desc")
    ->distinct();

// Apply role-based filter if the user's role is 2
if ($isRole2) {
    $query->where("sales_package.sales_id", $user_id);
}

$data["report"] = $query->get([
    "sales_package.id",
    "sales_package.package_amount as payment",
    "sales_package.package_tax as tax",
    "sales_package.package_fullamount as total",
    "sales_package.joining_date as date",
    "users.name",
    "ev_student_info.student_admissionid",
    "ev_package.pac_name",
]);

// Query for Oldstudent with role-based condition
$queryOld = Oldstudent::whereBetween("date_joining", [$first_date, $today]);

// Apply role-based filter if the user's role is 2
if ($isRole2) {
    $queryOld->where("sales_id", $user_id);
}

$data["oldreport"] = $queryOld->get([
    "ev_oldstudents.name",
    "ev_oldstudents.reg_no as student_admissionid",
    "ev_oldstudents.package as pac_name",
    "ev_oldstudents.fees as payment",
    "ev_oldstudents.tax",
    "ev_oldstudents.totalfees as total",
    "ev_oldstudents.date_joining as date",
]);

// Calculate totals with role-based condition
$data["monthadmi_regular_withgst"] = round(
    Salespackage::where(function ($query) {
        $query->orWhere('sales_package.package_type', 1)
              ->orWhere('sales_package.package_type', 2);
    })
    ->whereBetween("sales_package.joining_date", [$first_date, $today])
    ->when($isRole2, function ($query) use ($user_id) {
        return $query->where("sales_package.sales_id", $user_id);
    })
    ->sum("sales_package.package_fullamount")
);

$data["reg_old_withgst"] = round(
    Oldstudent::whereBetween("date_joining", [$first_date, $today])
    ->when($isRole2, function ($query) use ($user_id) {
        return $query->where("sales_id", $user_id);
    })
    ->sum("totalfees")
);

$data["monthly_tot_admissionwithgst"] = $data["monthadmi_regular_withgst"] + $data["reg_old_withgst"];

$data["monthadmi_regular_withoutgst"] = round(
    Salespackage::where(function ($query) {
        $query->orWhere('sales_package.package_type', 1)
              ->orWhere('sales_package.package_type', 2);
    })
    ->whereBetween("sales_package.joining_date", [$first_date, $today])
    ->when($isRole2, function ($query) use ($user_id) {
        return $query->where("sales_package.sales_id", $user_id);
    })
    ->sum("sales_package.package_amount")
);

$data['reg_old_withoutgst'] = round(
    Oldstudent::whereBetween("date_joining", [$first_date, $today])
    ->when($isRole2, function ($query) use ($user_id) {
        return $query->where("sales_id", $user_id);
    })
    ->sum("fees")
);

$data['monthly_tot_admissionwithoutgst'] = $data["monthadmi_regular_withoutgst"] + $data['reg_old_withoutgst'];

$data["monthadmi_regular_tax"] = round(
    Salespackage::where(function ($query) {
        $query->orWhere('sales_package.package_type', 1)
              ->orWhere('sales_package.package_type', 2);
    })
    ->whereBetween("sales_package.joining_date", [$first_date, $today])
    ->when($isRole2, function ($query) use ($user_id) {
        return $query->where("sales_package.sales_id", $user_id);
    })
    ->sum("sales_package.package_tax")
);

$data['reg_old_tax'] = round(
    Oldstudent::whereBetween("date_joining", [$first_date, $today])
    ->when($isRole2, function ($query) use ($user_id) {
        return $query->where("sales_id", $user_id);
    })
    ->sum("tax")
);

$data['monthly_tot_admissiontax'] = $data["monthadmi_regular_tax"] + $data['reg_old_tax'];








                

               

                return view("totaladmission_withoutgst", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    /*INDIVIDUAL REPORTS END IN SALES*/

    //1

    public function rpmonthlyrevenuewithgst_chry()
    {
        if (Auth::check()) {
            $arrayVariable = Session::get('array_key');
            $data1['names'] = User::whereIn('id', $arrayVariable)->pluck('name');

            $keysString = '';
            foreach ($data1['names'] as $names) {
                $keysString .= $names . ', ';
            }
            $name = rtrim($keysString, ', ');

            if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));

                $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                $today = date("Y-m-d", strtotime($_GET['search_date2']));
            } else {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');

                $data['search_date1'] = '';
                $data['search_date2'] = '';
            }

            if (Auth::user()->role == 2) {
                $empid = Auth::user()->id;
                $data['empstatus'] = 0;
                $data['title'] = 'Chrysalis and Inspire with GST Revenue' . ' ' . date('F Y') . ' - ' . ucfirst($name);
                $data['pac_type'] = 2;

                $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_payment.student_id')
                    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_payment.package_id')

                    ->whereIn("sales_payment.sales_id", $arrayVariable)
                    ->where("sales_payment.package_type", 2)
                    ->whereBetween("sales_payment.date", [$first_date, $today])
                    ->orderby("sales_payment.id", "desc")
                    ->distinct()
                    ->get(['sales_payment.*', 'users.name', 'ev_student_info.student_admissionid', 'ev_package.pac_id', 'ev_package.pac_name', 'ev_package.pac_type', 'sales_payment.sales_id']);

                $data["monthrevenue_chry_withgst"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $arrayVariable)
                        ->where("sales_payment.package_type", 2)
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.total")
                );

                $data["monthrevenue_chry_withoutgst"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $arrayVariable)
                        ->where("sales_payment.package_type", 2)
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.payment")
                );

                $data["monthrevenue_chry_tax"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $arrayVariable)
                        ->where("sales_payment.package_type", 2)
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.tax")
                );

                return view('rp_chrysalisrevenue_monthlyreport_gst', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    //2

    public function rpmonthlyrevenuewithoutgst_chry()
    {
        if (Auth::check()) {
            $arrayVariable = Session::get('array_key');
            $data1['names'] = User::whereIn('id', $arrayVariable)->pluck('name');

            $keysString = '';
            foreach ($data1['names'] as $names) {
                $keysString .= $names . ', ';
            }
            $name = rtrim($keysString, ', ');

            if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));

                $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                $today = date("Y-m-d", strtotime($_GET['search_date2']));
            } else {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');

                $data['search_date1'] = '';
                $data['search_date2'] = '';
            }

            if (Auth::user()->role == 2) {
                $empid = Auth::user()->id;
                $data['empstatus'] = 0;
                $data['title'] = 'Chrysalis and Inspire without GST Revenue' . ' ' . date('F Y') . ' - ' . ucfirst($name);
                $data['pac_type'] = 2;

                $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')

                    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_payment.package_id')
                    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_payment.student_id')
                    ->whereIn("sales_payment.sales_id", $arrayVariable)
                    ->where("sales_payment.package_type", 2)
                    ->whereBetween("sales_payment.date", [$first_date, $today])
                    ->orderby("sales_payment.id", "desc")
                    ->distinct()
                    ->get(['sales_payment.*', 'users.name', 'ev_student_info.student_admissionid', 'ev_package.pac_id', 'ev_package.pac_name', 'ev_package.pac_type', 'sales_payment.sales_id']);

                $data["monthrevenue_chry_withgst"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $arrayVariable)
                        ->where("sales_payment.package_type", 2)
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.total")
                );

                $data["monthrevenue_chry_withoutgst"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $arrayVariable)
                        ->where("sales_payment.package_type", 2)
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.payment")
                );

                $data["monthrevenue_chry_tax"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $arrayVariable)
                        ->where("sales_payment.package_type", 2)
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.tax")
                );

                return view('rp_chrysalisrevenue_monthlyreport_withoutgst', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    //3

    public function rpmonthlyrevenuewithgst_regular()
    {
        if (Auth::check()) {
            if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));

                $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                $today = date("Y-m-d", strtotime($_GET['search_date2']));
            } else {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');

                $data['search_date1'] = '';
                $data['search_date2'] = '';
            }

            $arrayVariable = Session::get('array_key');

            $data1['names'] = User::whereIn('id', $arrayVariable)->pluck('name');

            $keysString = '';
            foreach ($data1['names'] as $names) {
                $keysString .= $names . ', ';
            }
            $name = rtrim($keysString, ', ');

            if (Auth::user()->role == 2) {
                $user_id = Auth::user()->id;
                $data['empstatus'] = 0;
                $data['title'] = 'Regular Admission with GST Revenue' . ' ' . date('F Y') . ' - ' . $name;

                $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_payment.package_id')
                    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_payment.student_id')
                    ->whereIn("sales_payment.sales_id", $arrayVariable)
                    ->where("sales_payment.package_type", 1)
                    ->whereBetween("sales_payment.date", [$first_date, $today])
                    ->orderby("sales_payment.id", "desc")
                    ->distinct()
                    ->get(['sales_payment.*', 'users.name', 'ev_student_info.student_admissionid', 'ev_package.pac_id', 'ev_package.pac_name', 'ev_package.pac_type']);

                $data['oldreport'] = OldPayment::join('ev_oldstudents', 'ev_oldstudents.id', '=', 'olddata_payment.oldstudent_id')

                    ->whereIn("olddata_payment.sales_id", $arrayVariable)
                    ->whereBetween("olddata_payment.date", [$first_date, $today])
                    ->get([
                        'olddata_payment.paid_amount as total',
                        'olddata_payment.fees_income as payment',
                        'olddata_payment.tax',
                        'olddata_payment.date',
                        'ev_oldstudents.name',
                        'ev_oldstudents.reg_no as student_admissionid',
                        'ev_oldstudents.package as pac_name',
                    ]);

                $data["monthrevenue_regular_withoutgst"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $arrayVariable)
                        ->where("sales_payment.package_type", 1)
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.payment")
                );

                $data["monthrevenue_old_withoutgst"] = round(
                    OldPayment::whereIn("olddata_payment.sales_id", $arrayVariable)
                        ->whereBetween("olddata_payment.date", [$first_date, $today])
                        ->sum("olddata_payment.fees_income")
                );

                $data['monthrevenue_regularold_withoutgst'] = $data["monthrevenue_regular_withoutgst"] + $data["monthrevenue_old_withoutgst"];

                $data["monthrevenue_regular_withgst"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $arrayVariable)
                        ->where("sales_payment.package_type", 1)
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.total")
                );

                $data["monthrevenue_old_withgst"] = round(
                    OldPayment::whereIn("olddata_payment.sales_id", $arrayVariable)
                        ->whereBetween("olddata_payment.date", [$first_date, $today])
                        ->sum("olddata_payment.paid_amount")
                );

                $data['monthrevenue_regularold_withgst'] = $data["monthrevenue_regular_withgst"] + $data["monthrevenue_old_withgst"];

                $data["monthrevenue_regular_tax"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $arrayVariable)
                        ->where("sales_payment.package_type", 1)
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.tax")
                );

                $data["monthrevenue_old_tax"] = round(
                    OldPayment::whereIn("olddata_payment.sales_id", $arrayVariable)
                        ->whereBetween("olddata_payment.date", [$first_date, $today])
                        ->sum("olddata_payment.tax")
                );

                $data['monthrevenue_regularold_tax'] = $data["monthrevenue_regular_tax"] + $data["monthrevenue_old_tax"];

                return view('rp_monthlyregoldwithgst_revenue', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    //4

    public function rpmonthlyrevenuewithoutgst_regular()
    {
        if (Auth::check()) {
            if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));

                $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                $today = date("Y-m-d", strtotime($_GET['search_date2']));
            } else {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');

                $data['search_date1'] = '';
                $data['search_date2'] = '';
            }

            $arrayVariable = Session::get('array_key');

            $data1['names'] = User::whereIn('id', $arrayVariable)->pluck('name');

            $keysString = '';
            foreach ($data1['names'] as $names) {
                $keysString .= $names . ', ';
            }
            $name = rtrim($keysString, ', ');

            if (Auth::user()->role == 2) {
                $user_id = Auth::user()->id;
                $data['empstatus'] = 0;
                $data['title'] = 'Regular Admission without GST Revenue' . ' ' . date('F Y') . ' - ' . $name;

                $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_payment.package_id')
                    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_payment.student_id')
                    ->whereIn("sales_payment.sales_id", $arrayVariable)
                    ->where("sales_payment.package_type", 1)
                    ->whereBetween("sales_payment.date", [$first_date, $today])
                    ->orderby("sales_payment.id", "desc")
                    ->distinct()
                    ->get(['sales_payment.*', 'users.name', 'ev_student_info.student_admissionid', 'ev_package.pac_id', 'ev_package.pac_name', 'ev_package.pac_type']);

                $data['oldreport'] = OldPayment::join('ev_oldstudents', 'ev_oldstudents.id', '=', 'olddata_payment.oldstudent_id')

                    ->whereIn("olddata_payment.sales_id", $arrayVariable)
                    ->whereBetween("olddata_payment.date", [$first_date, $today])
                    ->get([
                        'olddata_payment.paid_amount as total',
                        'olddata_payment.fees_income as payment',
                        'olddata_payment.tax',
                        'olddata_payment.date',
                        'ev_oldstudents.name',
                        'ev_oldstudents.reg_no as student_admissionid',
                        'ev_oldstudents.package as pac_name',
                    ]);

                $data["monthrevenue_regular_withoutgst"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $arrayVariable)
                        ->where("sales_payment.package_type", 1)
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.payment")
                );

                $data["monthrevenue_old_withoutgst"] = round(
                    OldPayment::whereIn("olddata_payment.sales_id", $arrayVariable)
                        ->whereBetween("olddata_payment.date", [$first_date, $today])
                        ->sum("olddata_payment.fees_income")
                );

                $data['monthrevenue_regularold_withoutgst'] = $data["monthrevenue_regular_withoutgst"] + $data["monthrevenue_old_withoutgst"];

                $data["monthrevenue_regular_withgst"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $arrayVariable)
                        ->where("sales_payment.package_type", 1)
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.total")
                );

                $data["monthrevenue_old_withgst"] = round(
                    OldPayment::whereIn("olddata_payment.sales_id", $arrayVariable)
                        ->whereBetween("olddata_payment.date", [$first_date, $today])
                        ->sum("olddata_payment.paid_amount")
                );

                $data['monthrevenue_regularold_withgst'] = $data["monthrevenue_regular_withgst"] + $data["monthrevenue_old_withgst"];

                $data["monthrevenue_regular_tax"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $arrayVariable)
                        ->where("sales_payment.package_type", 1)
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.tax")
                );

                $data["monthrevenue_old_tax"] = round(
                    OldPayment::whereIn("olddata_payment.sales_id", $arrayVariable)
                        ->whereBetween("olddata_payment.date", [$first_date, $today])
                        ->sum("olddata_payment.tax")
                );

                $data['monthrevenue_regularold_tax'] = $data["monthrevenue_regular_tax"] + $data["monthrevenue_old_tax"];

                return view('rp_monthlyregoldwithoutgst_revenue', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    //5

    public function rptotalrevenue_withgst()
    {
        if (Auth::check()) {
            if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));
                $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                $today = date("Y-m-d", strtotime($_GET['search_date2']));
            } else {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');

                $data['search_date1'] = '';
                $data['search_date2'] = '';
            }

            $arrayVariable = Session::get('array_key');

            $data1['names'] = User::whereIn('id', $arrayVariable)->pluck('name');

            $keysString = '';
            foreach ($data1['names'] as $names) {
                $keysString .= $names . ', ';
            }
            $name = rtrim($keysString, ', ');

            if (Auth::user()->role == 2) {
                $user_id = Auth::user()->id;
                $data['empstatus'] = 0;
                $data['title'] = 'Total Revenue' . ' ' . date('F Y') . ' - ' . $name;
                $data['pac_type'] = 1;

                $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_payment.package_id')
                    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_payment.student_id')

                    ->whereIn("sales_payment.sales_id", $arrayVariable)

                    ->where(function ($query) {
                        $query->orWhere('sales_payment.package_type', 1)->orWhere('sales_payment.package_type', 2);
                    })
                    ->whereBetween("sales_payment.date", [$first_date, $today])
                    ->orderby("sales_payment.id", "desc")
                    ->distinct()
                    ->get(['sales_payment.*', 'users.name', 'ev_student_info.student_admissionid', 'ev_package.pac_id', 'ev_package.pac_name', 'ev_package.pac_type']);

                $data['oldreport'] = OldPayment::join('ev_oldstudents', 'ev_oldstudents.id', '=', 'olddata_payment.oldstudent_id')
                    ->whereIn("olddata_payment.sales_id", $arrayVariable)
                    ->whereBetween("olddata_payment.date", [$first_date, $today])
                    ->get([
                        'olddata_payment.paid_amount as total',
                        'olddata_payment.fees_income as payment',
                        'olddata_payment.tax',
                        'olddata_payment.date',
                        'ev_oldstudents.name',
                        'ev_oldstudents.reg_no as student_admissionid',
                        'ev_oldstudents.package as pac_name',
                    ]);

                $data["monthrevenue_regular_withoutgst"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $arrayVariable)

                        ->where(function ($query) {
                            $query->orWhere('sales_payment.package_type', 1)->orWhere('sales_payment.package_type', 2);
                        })

                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.payment")
                );

                $data["monthrevenue_old_withoutgst"] = round(
                    OldPayment::whereIn("olddata_payment.sales_id", $arrayVariable)
                        ->whereBetween("olddata_payment.date", [$first_date, $today])
                        ->sum("olddata_payment.fees_income")
                );

                $data['monthrevenue_regularold_withoutgst'] = $data["monthrevenue_regular_withoutgst"] + $data["monthrevenue_old_withoutgst"];

                $data["monthrevenue_regular_withgst"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $arrayVariable)
                        ->where(function ($query) {
                            $query->orWhere('sales_payment.package_type', 1)->orWhere('sales_payment.package_type', 2);
                        })
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.total")
                );

                $data["monthrevenue_old_withgst"] = round(
                    OldPayment::whereIn("olddata_payment.sales_id", $arrayVariable)
                        ->whereBetween("olddata_payment.date", [$first_date, $today])
                        ->sum("olddata_payment.paid_amount")
                );

                $data['monthrevenue_regularold_withgst'] = $data["monthrevenue_regular_withgst"] + $data["monthrevenue_old_withgst"];

                $data["monthrevenue_regular_tax"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $arrayVariable)
                        ->where(function ($query) {
                            $query->orWhere('sales_payment.package_type', 1)->orWhere('sales_payment.package_type', 2);
                        })
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.tax")
                );

                $data["monthrevenue_old_tax"] = round(
                    OldPayment::whereIn("olddata_payment.sales_id", $arrayVariable)
                        ->whereBetween("olddata_payment.date", [$first_date, $today])
                        ->sum("olddata_payment.tax")
                );

                $data['monthrevenue_regularold_tax'] = $data["monthrevenue_regular_tax"] + $data["monthrevenue_old_tax"];

                return view('rptotalrevenue_withgst', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    //6

    public function rptotalrevenue_withoutgst()
    {
        if (Auth::check()) {
            if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));
                $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                $today = date("Y-m-d", strtotime($_GET['search_date2']));
            } else {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');

                $data['search_date1'] = '';
                $data['search_date2'] = '';
            }

            $arrayVariable = Session::get('array_key');

            $data1['names'] = User::whereIn('id', $arrayVariable)->pluck('name');

            $keysString = '';
            foreach ($data1['names'] as $names) {
                $keysString .= $names . ', ';
            }
            $name = rtrim($keysString, ', ');

            if (Auth::user()->role == 2) {
                $user_id = Auth::user()->id;
                $data['empstatus'] = 0;
                $data['title'] = 'Total Revenue' . ' ' . date('F Y') . ' - ' . $name;
                $data['pac_type'] = 1;

                $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_payment.package_id')

                    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_payment.student_id')
                    ->whereIn("sales_payment.sales_id", $arrayVariable)

                    ->where(function ($query) {
                        $query->orWhere('sales_payment.package_type', 1)->orWhere('sales_payment.package_type', 2);
                    })
                    ->whereBetween("sales_payment.date", [$first_date, $today])
                    ->orderby("sales_payment.id", "desc")
                    ->distinct()
                    ->get(['sales_payment.*', 'users.name', 'ev_student_info.student_admissionid', 'ev_package.pac_id', 'ev_package.pac_name', 'ev_package.pac_type']);

                $data['oldreport'] = OldPayment::join('ev_oldstudents', 'ev_oldstudents.id', '=', 'olddata_payment.oldstudent_id')
                    ->whereIn("olddata_payment.sales_id", $arrayVariable)
                    ->whereBetween("olddata_payment.date", [$first_date, $today])
                    ->get([
                        'olddata_payment.paid_amount as total',
                        'olddata_payment.fees_income as payment',
                        'olddata_payment.tax',
                        'olddata_payment.date',
                        'ev_oldstudents.name',
                        'ev_oldstudents.reg_no as student_admissionid',
                        'ev_oldstudents.package as pac_name',
                    ]);

                $data["monthrevenue_regular_withoutgst"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $arrayVariable)

                        ->where(function ($query) {
                            $query->orWhere('sales_payment.package_type', 1)->orWhere('sales_payment.package_type', 2);
                        })

                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.payment")
                );

                $data["monthrevenue_old_withoutgst"] = round(
                    OldPayment::whereIn("olddata_payment.sales_id", $arrayVariable)
                        ->whereBetween("olddata_payment.date", [$first_date, $today])
                        ->sum("olddata_payment.fees_income")
                );

                $data['monthrevenue_regularold_withoutgst'] = $data["monthrevenue_regular_withoutgst"] + $data["monthrevenue_old_withoutgst"];

                $data["monthrevenue_regular_withgst"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $arrayVariable)
                        ->where(function ($query) {
                            $query->orWhere('sales_payment.package_type', 1)->orWhere('sales_payment.package_type', 2);
                        })
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.total")
                );

                $data["monthrevenue_old_withgst"] = round(
                    OldPayment::whereIn("olddata_payment.sales_id", $arrayVariable)
                        ->whereBetween("olddata_payment.date", [$first_date, $today])
                        ->sum("olddata_payment.paid_amount")
                );

                $data['monthrevenue_regularold_withgst'] = $data["monthrevenue_regular_withgst"] + $data["monthrevenue_old_withgst"];

                $data["monthrevenue_regular_tax"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $arrayVariable)
                        ->where(function ($query) {
                            $query->orWhere('sales_payment.package_type', 1)->orWhere('sales_payment.package_type', 2);
                        })
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.tax")
                );

                $data["monthrevenue_old_tax"] = round(
                    OldPayment::whereIn("olddata_payment.sales_id", $arrayVariable)
                        ->whereBetween("olddata_payment.date", [$first_date, $today])
                        ->sum("olddata_payment.tax")
                );

                $data['monthrevenue_regularold_tax'] = $data["monthrevenue_regular_tax"] + $data["monthrevenue_old_tax"];

                return view('rptotalrevenue_withgst', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    //7.

    public function rpmonthlyadmissionwithgst_chrysalis()
    {
        if (Auth::check()) {
            if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));

                $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                $today = date("Y-m-d", strtotime($_GET['search_date2']));
            } else {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');

                $data['search_date1'] = '';
                $data['search_date2'] = '';
            }

            $arrayVariable = Session::get('array_key');
            $data1['names'] = User::whereIn('id', $arrayVariable)->pluck('name');

            $keysString = '';
            foreach ($data1['names'] as $names) {
                $keysString .= $names . ', ';
            }
            $name = rtrim($keysString, ', ');

            if (Auth::user()->role == 2) {
                $user_id = Auth::user()->id;

                $data['empstatus'] = 0;
                $data['title'] = 'Chrysalis and Inspire  Admissions' . ' ' . date('F Y') . ' - ' . $name;
                $data['pac_type'] = 2;

                $data['report'] = Salespackage::join('users', 'users.id', '=', 'sales_package.student_id')
                    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_package.package_id')
                    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_package.student_id')
                    ->whereIn("sales_package.sales_id", $arrayVariable)
                    ->where("sales_package.package_type", 2)
                    ->whereBetween("sales_package.joining_date", [$first_date, $today])
                    ->orderby("sales_package.id", "desc")
                    ->distinct()
                    ->get(['sales_package.*', 'users.name', 'ev_student_info.student_admissionid', 'ev_package.pac_id', 'ev_package.pac_name', 'ev_package.pac_type']);

                $data["monthadmi_chry_withgst"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $arrayVariable)
                        ->where("sales_package.package_type", 2)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_fullamount")
                );

                $data["monthadmi_chry_withoutgst"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $arrayVariable)
                        ->where("sales_package.package_type", 2)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_amount")
                );
                $data["monthadmi_chry_tax"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $arrayVariable)
                        ->where("sales_package.package_type", 2)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_tax")
                );

                return view('rpadmissionwithgst_chrysalis', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    //8.

    public function rpmonthlyadmissionwithoutgst_chrysalis()
    {
        if (Auth::check()) {
            if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));

                $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                $today = date("Y-m-d", strtotime($_GET['search_date2']));
            } else {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');

                $data['search_date1'] = '';
                $data['search_date2'] = '';
            }

            $arrayVariable = Session::get('array_key');
            $data1['names'] = User::whereIn('id', $arrayVariable)->pluck('name');

            $keysString = '';
            foreach ($data1['names'] as $names) {
                $keysString .= $names . ', ';
            }
            $name = rtrim($keysString, ', ');

            if (Auth::user()->role == 2) {
                $user_id = Auth::user()->id;

                $data['empstatus'] = 0;
                $data['title'] = 'Chrysalis and Inspire  Admissions' . ' ' . date('F Y') . ' - ' . $name;
                $data['pac_type'] = 2;

                $data['report'] = Salespackage::join('users', 'users.id', '=', 'sales_package.student_id')
                    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_package.package_id')

                    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_package.student_id')
                    ->whereIn("sales_package.sales_id", $arrayVariable)
                    ->where("sales_package.package_type", 2)
                    ->whereBetween("sales_package.joining_date", [$first_date, $today])
                    ->orderby("sales_package.id", "desc")
                    ->distinct()
                    ->get(['sales_package.*', 'users.name', 'ev_student_info.student_admissionid', 'ev_package.pac_id', 'ev_package.pac_name', 'ev_package.pac_type']);

                $data["monthadmi_chry_withgst"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $arrayVariable)
                        ->where("sales_package.package_type", 2)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_fullamount")
                );

                $data["monthadmi_chry_withoutgst"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $arrayVariable)
                        ->where("sales_package.package_type", 2)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_amount")
                );
                $data["monthadmi_chry_tax"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $arrayVariable)
                        ->where("sales_package.package_type", 2)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_tax")
                );

                return view('rpadmissionwithoutgst_chrysalis', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    //9

    public function rpmonthlyadmissionwithgst_regular()
    {
        if (Auth::check()) {
            if (isset($_GET["search_date1"]) != "" && isset($_GET["search_date2"]) != "") {
                $data["search_date1"] = date("d-m-Y", strtotime($_GET["search_date1"]));
                $data["search_date2"] = date("d-m-Y", strtotime($_GET["search_date2"]));

                $first_date = date("Y-m-d", strtotime($_GET["search_date1"]));
                $today = date("Y-m-d", strtotime($_GET["search_date2"]));
            } else {
                $today = date("Y-m-d");
                $first_date = date("Y-m-01");

                $data["search_date1"] = "";
                $data["search_date2"] = "";
            }

            $arrayVariable = Session::get('array_key');
            $data1['names'] = User::whereIn('id', $arrayVariable)->pluck('name');

            $keysString = '';
            foreach ($data1['names'] as $names) {
                $keysString .= $names . ', ';
            }
            $name = rtrim($keysString, ', ');

            if (Auth::user()->role == 2) {
                $user_id = Auth::user()->id;
                $data["empstatus"] = 0;
                $data["title"] = "Regular Admission with GST" . " " . date("F Y") . ' - ' . $name;
                $data["pac_type"] = 1;

                $data["report"] = Salespackage::join("users", "users.id", "=", "sales_package.student_id")
                    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_package.package_id')
                    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_package.student_id')
                    ->whereIn("sales_package.sales_id", $arrayVariable)
                    ->where("sales_package.package_type", 1)
                    ->whereBetween("sales_package.joining_date", [$first_date, $today])
                    ->orderby("sales_package.id", "desc")
                    ->distinct()
                    ->get([
                        "sales_package.id",
                        "sales_package.package_amount  as payment",
                        "sales_package.package_tax  as tax",
                        "sales_package.package_fullamount  as total",
                        "sales_package.joining_date  as date",
                        "users.name",
                        "ev_student_info.student_admissionid",
                        "ev_package.pac_name",
                    ]);

                $data["oldreport"] = Oldstudent::whereIn("sales_id", $arrayVariable)
                    ->whereBetween("date_joining", [$first_date, $today])
                    ->get([
                        "ev_oldstudents.name",
                        "ev_oldstudents.reg_no as student_admissionid",
                        "ev_oldstudents.package as pac_name",
                        "ev_oldstudents.fees as payment",
                        "ev_oldstudents.tax",
                        "ev_oldstudents.totalfees as total",
                        "ev_oldstudents.date_joining as date",
                    ]);

                $data["monthadmi_regular_withgst"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $arrayVariable)
                        ->where("sales_package.package_type", 1)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_fullamount")
                );
                $data["reg_old_withgst"] = round(
                    Oldstudent::whereIn("ev_oldstudents.sales_id", $arrayVariable)
                        ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                        ->sum("ev_oldstudents.totalfees")
                );
                $data["monthly_tot_admissionwithgst"] = $data["monthadmi_regular_withgst"] + $data["reg_old_withgst"];

                $data["monthadmi_regular_withoutgst"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $arrayVariable)
                        ->where("sales_package.package_type", 1)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_amount")
                );

                $data['reg_old_withoutgst'] = round(
                    Oldstudent::whereIn("ev_oldstudents.sales_id", $arrayVariable)
                        ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                        ->sum("ev_oldstudents.fees")
                );
                $data['monthly_tot_admissionwithoutgst'] = $data["monthadmi_regular_withoutgst"] + $data['reg_old_withoutgst'];

                $data["monthadmi_regular_tax"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $arrayVariable)
                        ->where("sales_package.package_type", 1)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_tax")
                );

                $data['reg_old_tax'] = round(
                    Oldstudent::whereIn("ev_oldstudents.sales_id", $arrayVariable)
                        ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                        ->sum("ev_oldstudents.tax")
                );
                $data['monthly_tot_admissiontax'] = $data["monthadmi_regular_tax"] + $data['reg_old_tax'];

                return view("rp_monthlyregoldwithgst_admission", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    //10

    public function rpmonthlyadmissionwithoutgst_regular()
    {
        if (Auth::check()) {
            if (isset($_GET["search_date1"]) != "" && isset($_GET["search_date2"]) != "") {
                $data["search_date1"] = date("d-m-Y", strtotime($_GET["search_date1"]));
                $data["search_date2"] = date("d-m-Y", strtotime($_GET["search_date2"]));

                $first_date = date("Y-m-d", strtotime($_GET["search_date1"]));
                $today = date("Y-m-d", strtotime($_GET["search_date2"]));
            } else {
                $today = date("Y-m-d");
                $first_date = date("Y-m-01");

                $data["search_date1"] = "";
                $data["search_date2"] = "";
            }

            $arrayVariable = Session::get('array_key');
            $data1['names'] = User::whereIn('id', $arrayVariable)->pluck('name');

            $keysString = '';
            foreach ($data1['names'] as $names) {
                $keysString .= $names . ', ';
            }
            $name = rtrim($keysString, ', ');

            if (Auth::user()->role == 2) {
                $user_id = Auth::user()->id;
                $data["empstatus"] = 0;
                $data["title"] = "Regular Admission without GST" . " " . date("F Y") . ' - ' . $name;
                $data["pac_type"] = 1;

                $data["report"] = Salespackage::join("users", "users.id", "=", "sales_package.student_id")
                    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_package.package_id')
                    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_package.student_id')
                    ->whereIn("sales_package.sales_id", $arrayVariable)
                    ->where("sales_package.package_type", 1)
                    ->whereBetween("sales_package.joining_date", [$first_date, $today])
                    ->orderby("sales_package.id", "desc")
                    ->distinct()
                    ->get([
                        "sales_package.id",
                        "sales_package.package_amount  as payment",
                        "sales_package.package_tax  as tax",
                        "sales_package.package_fullamount  as total",
                        "sales_package.joining_date  as date",
                        "users.name",
                        "ev_student_info.student_admissionid",
                        "ev_package.pac_name",
                    ]);

                $data["oldreport"] = Oldstudent::whereIn("sales_id", $arrayVariable)
                    ->whereBetween("date_joining", [$first_date, $today])
                    ->get([
                        "ev_oldstudents.name",
                        "ev_oldstudents.reg_no as student_admissionid",
                        "ev_oldstudents.package as pac_name",
                        "ev_oldstudents.fees as payment",
                        "ev_oldstudents.tax",
                        "ev_oldstudents.totalfees as total",
                        "ev_oldstudents.date_joining as date",
                    ]);

                $data["monthadmi_regular_withgst"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $arrayVariable)
                        ->where("sales_package.package_type", 1)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_fullamount")
                );
                $data["reg_old_withgst"] = round(
                    Oldstudent::whereIn("ev_oldstudents.sales_id", $arrayVariable)
                        ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                        ->sum("ev_oldstudents.totalfees")
                );
                $data["monthly_tot_admissionwithgst"] = $data["monthadmi_regular_withgst"] + $data["reg_old_withgst"];

                $data["monthadmi_regular_withoutgst"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $arrayVariable)
                        ->where("sales_package.package_type", 1)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_amount")
                );

                $data['reg_old_withoutgst'] = round(
                    Oldstudent::whereIn("ev_oldstudents.sales_id", $arrayVariable)
                        ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                        ->sum("ev_oldstudents.fees")
                );
                $data['monthly_tot_admissionwithoutgst'] = $data["monthadmi_regular_withoutgst"] + $data['reg_old_withoutgst'];

                $data["monthadmi_regular_tax"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $arrayVariable)
                        ->where("sales_package.package_type", 1)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_tax")
                );

                $data['reg_old_tax'] = round(
                    Oldstudent::whereIn("ev_oldstudents.sales_id", $arrayVariable)
                        ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                        ->sum("ev_oldstudents.tax")
                );
                $data['monthly_tot_admissiontax'] = $data["monthadmi_regular_tax"] + $data['reg_old_tax'];

                return view("rp_monthlyregoldwithoutgst_admission", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    //11

    public function rptotaladmission_withgst()
    {
        if (Auth::check()) {
            if (isset($_GET["search_date1"]) != "" && isset($_GET["search_date2"]) != "") {
                $data["search_date1"] = date("d-m-Y", strtotime($_GET["search_date1"]));
                $data["search_date2"] = date("d-m-Y", strtotime($_GET["search_date2"]));

                $first_date = date("Y-m-d", strtotime($_GET["search_date1"]));
                $today = date("Y-m-d", strtotime($_GET["search_date2"]));
            } else {
                $today = date("Y-m-d");
                $first_date = date("Y-m-01");

                $data["search_date1"] = "";
                $data["search_date2"] = "";
            }

            $arrayVariable = Session::get('array_key');

            $data1['names'] = User::whereIn('id', $arrayVariable)->pluck('name');

            $keysString = '';
            foreach ($data1['names'] as $names) {
                $keysString .= $names . ', ';
            }
            $name = rtrim($keysString, ', ');

            if (Auth::user()->role == 2) {
                $user_id = Auth::user()->id;
                $data["empstatus"] = 0;
                $data["title"] = "Total Admission with GST" . " " . date("F Y") . ' - ' . $name;

                $data["report"] = Salespackage::join("users", "users.id", "=", "sales_package.student_id")
                    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_package.package_id')
                    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_package.student_id')
                    ->whereIn("sales_package.sales_id", $arrayVariable)

                    ->where(function ($query) {
                        $query->orWhere('sales_package.package_type', 1)->orWhere('sales_package.package_type', 2);
                    })

                    ->whereBetween("sales_package.joining_date", [$first_date, $today])
                    ->orderby("sales_package.id", "desc")
                    ->distinct()
                    ->get([
                        "sales_package.id",
                        "sales_package.package_amount  as payment",
                        "sales_package.package_tax  as tax",
                        "sales_package.package_fullamount  as total",
                        "sales_package.joining_date  as date",
                        "sales_package.sales_id",
                        "users.name",
                        "ev_student_info.student_admissionid",
                        "ev_package.pac_name",
                    ]);

                $data["oldreport"] = Oldstudent::whereIn("sales_id", $arrayVariable)
                    ->whereBetween("date_joining", [$first_date, $today])
                    ->get([
                        "ev_oldstudents.name",
                        "ev_oldstudents.reg_no as student_admissionid",
                        "ev_oldstudents.package as pac_name",
                        "ev_oldstudents.fees as payment",
                        "ev_oldstudents.tax",
                        "ev_oldstudents.totalfees as total",
                        "ev_oldstudents.date_joining as date",
                    ]);

                $data["monthadmi_regular_withgst"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $arrayVariable)
                        ->where(function ($query) {
                            $query->orWhere('sales_package.package_type', 1)->orWhere('sales_package.package_type', 2);
                        })
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_fullamount")
                );

                $data["reg_old_withgst"] = round(
                    Oldstudent::whereIn("ev_oldstudents.sales_id", $arrayVariable)
                        ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                        ->sum("ev_oldstudents.totalfees")
                );
                $data["monthly_tot_admissionwithgst"] = $data["monthadmi_regular_withgst"] + $data["reg_old_withgst"];

                $data["monthadmi_regular_withoutgst"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $arrayVariable)
                        ->where(function ($query) {
                            $query->orWhere('sales_package.package_type', 1)->orWhere('sales_package.package_type', 2);
                        })
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_amount")
                );

                $data['reg_old_withoutgst'] = round(
                    Oldstudent::whereIn("ev_oldstudents.sales_id", $arrayVariable)
                        ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                        ->sum("ev_oldstudents.fees")
                );
                $data['monthly_tot_admissionwithoutgst'] = $data["monthadmi_regular_withoutgst"] + $data['reg_old_withoutgst'];

                $data["monthadmi_regular_tax"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $arrayVariable)
                        ->where(function ($query) {
                            $query->orWhere('sales_package.package_type', 1)->orWhere('sales_package.package_type', 2);
                        })
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_tax")
                );

                $data['reg_old_tax'] = round(
                    Oldstudent::whereIn("ev_oldstudents.sales_id", $arrayVariable)
                        ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                        ->sum("ev_oldstudents.tax")
                );
                $data['monthly_tot_admissiontax'] = $data["monthadmi_regular_tax"] + $data['reg_old_tax'];

                return view("rp_totaladmission_withgst", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    //12

    public function rptotaladmission_withoutgst()
    {
        if (Auth::check()) {
            if (isset($_GET["search_date1"]) != "" && isset($_GET["search_date2"]) != "") {
                $data["search_date1"] = date("d-m-Y", strtotime($_GET["search_date1"]));
                $data["search_date2"] = date("d-m-Y", strtotime($_GET["search_date2"]));

                $first_date = date("Y-m-d", strtotime($_GET["search_date1"]));
                $today = date("Y-m-d", strtotime($_GET["search_date2"]));
            } else {
                $today = date("Y-m-d");
                $first_date = date("Y-m-01");

                $data["search_date1"] = "";
                $data["search_date2"] = "";
            }

            $arrayVariable = Session::get('array_key');

            $data1['names'] = User::whereIn('id', $arrayVariable)->pluck('name');

            $keysString = '';
            foreach ($data1['names'] as $names) {
                $keysString .= $names . ', ';
            }
            $name = rtrim($keysString, ', ');

            if (Auth::user()->role == 2) {
                $user_id = Auth::user()->id;
                $data["empstatus"] = 0;
                $data["title"] = "Total Admission without GST" . " " . date("F Y") . ' - ' . $name;

                $data["report"] = Salespackage::join("users", "users.id", "=", "sales_package.student_id")
                    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_package.package_id')

                    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_package.student_id')
                    ->whereIn("sales_package.sales_id", $arrayVariable)

                    ->where(function ($query) {
                        $query->orWhere('sales_package.package_type', 1)->orWhere('sales_package.package_type', 2);
                    })

                    ->whereBetween("sales_package.joining_date", [$first_date, $today])
                    ->orderby("sales_package.id", "desc")
                    ->distinct()
                    ->get([
                        "sales_package.id",
                        "sales_package.package_amount  as payment",
                        "sales_package.package_tax  as tax",
                        "sales_package.package_fullamount  as total",
                        "sales_package.joining_date  as date",
                        "sales_package.sales_id",
                        "users.name",
                        "ev_student_info.student_admissionid",
                        "ev_package.pac_name",
                    ]);

                $data["oldreport"] = Oldstudent::whereIn("sales_id", $arrayVariable)
                    ->whereBetween("date_joining", [$first_date, $today])
                    ->get([
                        "ev_oldstudents.name",
                        "ev_oldstudents.reg_no as student_admissionid",
                        "ev_oldstudents.package as pac_name",
                        "ev_oldstudents.fees as payment",
                        "ev_oldstudents.tax",
                        "ev_oldstudents.totalfees as total",
                        "ev_oldstudents.date_joining as date",
                    ]);

                $data["monthadmi_regular_withgst"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $arrayVariable)
                        ->where(function ($query) {
                            $query->orWhere('sales_package.package_type', 1)->orWhere('sales_package.package_type', 2);
                        })
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_fullamount")
                );

                $data["reg_old_withgst"] = round(
                    Oldstudent::whereIn("ev_oldstudents.sales_id", $arrayVariable)
                        ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                        ->sum("ev_oldstudents.totalfees")
                );
                $data["monthly_tot_admissionwithgst"] = $data["monthadmi_regular_withgst"] + $data["reg_old_withgst"];

                $data["monthadmi_regular_withoutgst"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $arrayVariable)
                        ->where(function ($query) {
                            $query->orWhere('sales_package.package_type', 1)->orWhere('sales_package.package_type', 2);
                        })
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_amount")
                );

                $data['reg_old_withoutgst'] = round(
                    Oldstudent::whereIn("ev_oldstudents.sales_id", $arrayVariable)
                        ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                        ->sum("ev_oldstudents.fees")
                );
                $data['monthly_tot_admissionwithoutgst'] = $data["monthadmi_regular_withoutgst"] + $data['reg_old_withoutgst'];

                $data["monthadmi_regular_tax"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $arrayVariable)
                        ->where(function ($query) {
                            $query->orWhere('sales_package.package_type', 1)->orWhere('sales_package.package_type', 2);
                        })
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_tax")
                );

                $data['reg_old_tax'] = round(
                    Oldstudent::whereIn("ev_oldstudents.sales_id", $arrayVariable)
                        ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                        ->sum("ev_oldstudents.tax")
                );
                $data['monthly_tot_admissiontax'] = $data["monthadmi_regular_tax"] + $data['reg_old_tax'];

                return view("rp_totaladmission_withoutgst", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function totalrevenue_monthly()
    {
        if (Auth::check()) {
            if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));
                $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                $today = date("Y-m-d", strtotime($_GET['search_date2']));
            } else {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');

                $data['search_date1'] = '';
                $data['search_date2'] = '';
            }

            if (Auth::user()->role == 2) {
                $user_id = Auth::user()->id;
                $data['empstatus'] = 0;
                $data['title'] = 'Total Revenue' . ' ' . date('F Y');
                $data['pac_type'] = 1;

                $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_payment.package_id')
                    ->where("sales_payment.sales_id", $user_id)

                    ->where(function ($query) {
                        $query->orWhere('sales_payment.package_type', 1)->orWhere('sales_payment.package_type', 2);
                    })
                    ->whereBetween("sales_payment.date", [$first_date, $today])
                    ->orderby("sales_payment.id", "desc")
                    ->distinct()
                    ->get(['sales_payment.*', 'users.name', 'users.student_admissionid', 'ev_package.pac_id', 'ev_package.pac_name', 'ev_package.pac_type']);

                $data['oldreport'] = OldPayment::join('ev_oldstudents', 'ev_oldstudents.id', '=', 'olddata_payment.oldstudent_id')
                    ->where("olddata_payment.sales_id", Auth::user()->id)
                    ->whereBetween("olddata_payment.date", [$first_date, $today])
                    ->get([
                        'olddata_payment.paid_amount as total',
                        'olddata_payment.fees_income as payment',
                        'olddata_payment.tax',
                        'olddata_payment.date',
                        'ev_oldstudents.name',
                        'ev_oldstudents.reg_no as student_admissionid',
                        'ev_oldstudents.package as pac_name',
                    ]);

                $data["monthrevenue_regular_withoutgst"] = round(
                    Salespayment::where("sales_payment.sales_id", $user_id)

                        ->where(function ($query) {
                            $query->orWhere('sales_payment.package_type', 1)->orWhere('sales_payment.package_type', 2);
                        })

                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.payment")
                );

                $data["monthrevenue_old_withoutgst"] = round(
                    OldPayment::where("olddata_payment.sales_id", $user_id)
                        ->whereBetween("olddata_payment.date", [$first_date, $today])
                        ->sum("olddata_payment.fees_income")
                );

                $data['monthrevenue_regularold_withoutgst'] = $data["monthrevenue_regular_withoutgst"] + $data["monthrevenue_old_withoutgst"];

                $data["monthrevenue_regular_withgst"] = round(
                    Salespayment::where("sales_payment.sales_id", $user_id)
                        ->where(function ($query) {
                            $query->orWhere('sales_payment.package_type', 1)->orWhere('sales_payment.package_type', 2);
                        })
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.total")
                );

                $data["monthrevenue_old_withgst"] = round(
                    OldPayment::where("olddata_payment.sales_id", $user_id)
                        ->whereBetween("olddata_payment.date", [$first_date, $today])
                        ->sum("olddata_payment.paid_amount")
                );

                $data['monthrevenue_regularold_withgst'] = $data["monthrevenue_regular_withgst"] + $data["monthrevenue_old_withgst"];

                $data["monthrevenue_regular_tax"] = round(
                    Salespayment::where("sales_payment.sales_id", $user_id)
                        ->where(function ($query) {
                            $query->orWhere('sales_payment.package_type', 1)->orWhere('sales_payment.package_type', 2);
                        })
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.tax")
                );

                $data["monthrevenue_old_tax"] = round(
                    OldPayment::where("olddata_payment.sales_id", $user_id)
                        ->whereBetween("olddata_payment.date", [$first_date, $today])
                        ->sum("olddata_payment.tax")
                );

                $data['monthrevenue_regularold_tax'] = $data["monthrevenue_regular_tax"] + $data["monthrevenue_old_tax"];

                return view('monthlyregold_revenue', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function view_salesreport(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 2) {
                $user_ids = $request->get("report_of");
                $arrayVariable = $user_ids;

                session(['array_key' => $arrayVariable]);
                Session::put('array_key', $arrayVariable);
                Session::put('fullurl', url()->full());

                $data1['names'] = User::whereIn('id', $user_ids)->pluck('name');
                $keysString = '';
                foreach ($data1['names'] as $names) {
                    $keysString .= $names . ', ';
                }
                $name = rtrim($keysString, ', ');

                $data['title'] = 'Report Of ' . $name;

                $today = date("Y-m-d");
                $first_date = date("Y-m-01");

                //MONTHLY REVENUE START

                // Monthly chrysalis
                $data["monthrevenue_chry_withgst"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $user_ids)
                        ->where("sales_payment.package_type", 2)
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.total")
                );

                $data["monthrevenue_chry_withoutgst"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $user_ids)
                        ->where("sales_payment.package_type", 2)
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.payment")
                );

                $data["monthrevenue_chry_tax"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $user_ids)
                        ->where("sales_payment.package_type", 2)
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.tax")
                );

                // Monthly Regular and old

                $data["monthrevenue_regular_withgst"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $user_ids)
                        ->where("sales_payment.package_type", 1)
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.total")
                );

                $data["monthrevenue_old_withgst"] = round(
                    OldPayment::whereIn("olddata_payment.sales_id", $user_ids)
                        ->whereBetween("olddata_payment.date", [$first_date, $today])
                        ->sum("olddata_payment.paid_amount")
                );

                $data['monthrevenue_regold_withgst'] = $data["monthrevenue_regular_withgst"] + $data["monthrevenue_old_withgst"];

                $data["monthrevenue_regular_tax"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $user_ids)
                        ->where("sales_payment.package_type", 1)
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.tax")
                );

                $data["monthrevenue_old_tax"] = round(
                    OldPayment::whereIn("olddata_payment.sales_id", $user_ids)
                        ->whereBetween("olddata_payment.date", [$first_date, $today])
                        ->sum("olddata_payment.tax")
                );

                $data['monthrevenue_regold_tax'] = $data["monthrevenue_regular_tax"] + $data["monthrevenue_old_tax"];

                $data["monthrevenue_regular_withoutgst"] = round(
                    Salespayment::whereIn("sales_payment.sales_id", $user_ids)
                        ->where("sales_payment.package_type", 1)
                        ->whereBetween("sales_payment.date", [$first_date, $today])
                        ->sum("sales_payment.payment")
                );

                $data["monthrevenue_old_withoutgst"] = round(
                    OldPayment::whereIn("olddata_payment.sales_id", $user_ids)
                        ->whereBetween("olddata_payment.date", [$first_date, $today])
                        ->sum("olddata_payment.fees_income")
                );
                $data['monthrevenue_regold_withoutgst'] = $data["monthrevenue_regular_withoutgst"] + $data["monthrevenue_old_withoutgst"];

                // Monthly total
                $data['monthlytotal_withgst'] = $data["monthrevenue_chry_withgst"] + $data['monthrevenue_regold_withgst'];
                $data['monthlytotal_withoutgst'] = $data["monthrevenue_chry_withoutgst"] + $data['monthrevenue_regold_withoutgst'];

                $data['total_tax'] = $data["monthrevenue_chry_tax"] + $data["monthrevenue_regular_tax"] + $data["monthrevenue_old_tax"];

                //MONTHLY REVENUE END

                //MONTHLY ADMISSION  START

                // Monthly chrysalis
                $data["monthadmi_chry_withgst"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $user_ids)
                        ->where("sales_package.package_type", 2)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_fullamount")
                );

                $data["monthadmi_chry_withoutgst"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $user_ids)
                        ->where("sales_package.package_type", 2)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_amount")
                );
                $data["monthadmi_chry_tax"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $user_ids)
                        ->where("sales_package.package_type", 2)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_tax")
                );
                //Monthly Admissions

                $data["monthadmi_regular_withgst"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $user_ids)
                        ->where("sales_package.package_type", 1)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_fullamount")
                );

                $data['reg_old_withgst'] = round(
                    Oldstudent::whereIn("ev_oldstudents.sales_id", $user_ids)
                        ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                        ->sum("ev_oldstudents.totalfees")
                );

                $data['monthly_tot_admissionwithgst'] = $data["monthadmi_regular_withgst"] + $data['reg_old_withgst'];

                $data["monthadmi_regular_withoutgst"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $user_ids)
                        ->where("sales_package.package_type", 1)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_amount")
                );

                $data['reg_old_withoutgst'] = round(
                    Oldstudent::whereIn("ev_oldstudents.sales_id", $user_ids)
                        ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                        ->sum("ev_oldstudents.fees")
                );
                $data['monthly_tot_admissionwithoutgst'] = $data["monthadmi_regular_withoutgst"] + $data['reg_old_withoutgst'];

                $data["monthadmi_regular_tax"] = round(
                    Salespackage::whereIn("sales_package.sales_id", $user_ids)
                        ->where("sales_package.package_type", 1)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_tax")
                );

                $data['reg_old_tax'] = round(
                    Oldstudent::whereIn("ev_oldstudents.sales_id", $user_ids)
                        ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                        ->sum("ev_oldstudents.tax")
                );
                $data['monthly_tot_admissiontax'] = $data["monthadmi_regular_tax"] + $data['reg_old_tax'];

                //Total(Admission_Old_Chrysalis)

                $data['total_reg_chry_old_withgst'] = $data['monthly_tot_admissionwithgst'] + $data["monthadmi_chry_withgst"];

                $data['total_reg_chry_old_withoutgst'] = $data['monthly_tot_admissionwithoutgst'] + $data["monthadmi_chry_withoutgst"];

                $data['total_reg_chry_old_tax'] = $data['monthadmi_chry_tax'] + $data["monthly_tot_admissiontax"];

                return view('sales_userreport', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function monthlyadmissionreport_chrysalis()
    {
        if (Auth::check()) {
            if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));

                $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                $today = date("Y-m-d", strtotime($_GET['search_date2']));
            } else {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');

                $data['search_date1'] = '';
                $data['search_date2'] = '';
            }

            if (Auth::user()->role == 2) {
                $user_id = Auth::user()->id;

                $data['empstatus'] = 0;
                $data['title'] = 'Chrysalis and Inspire  Admissions' . ' ' . date('F Y');
                $data['pac_type'] = 2;

                $data['report'] = Salespackage::join('users', 'users.id', '=', 'sales_package.student_id')
                    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_package.package_id')
                    ->where("sales_package.sales_id", $user_id)
                    ->where("sales_package.package_type", 2)
                    ->whereBetween("sales_package.joining_date", [$first_date, $today])
                    ->orderby("sales_package.id", "desc")
                    ->distinct()
                    ->get(['sales_package.*', 'users.name', 'users.student_admissionid', 'ev_package.pac_id', 'ev_package.pac_name', 'ev_package.pac_type']);

                $data["monthadmi_chry_withgst"] = round(
                    Salespackage::where("sales_package.sales_id", $user_id)
                        ->where("sales_package.package_type", 2)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_fullamount")
                );

                $data["monthadmi_chry_withoutgst"] = round(
                    Salespackage::where("sales_package.sales_id", $user_id)
                        ->where("sales_package.package_type", 2)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_amount")
                );
                $data["monthadmi_chry_tax"] = round(
                    Salespackage::where("sales_package.sales_id", $user_id)
                        ->where("sales_package.package_type", 2)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_tax")
                );

                return view('admission_monthlyreport', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }
    public function totaladmission_monthly()
    {
        if (Auth::check()) {
            if (isset($_GET["search_date1"]) != "" && isset($_GET["search_date2"]) != "") {
                $data["search_date1"] = date("d-m-Y", strtotime($_GET["search_date1"]));
                $data["search_date2"] = date("d-m-Y", strtotime($_GET["search_date2"]));

                $first_date = date("Y-m-d", strtotime($_GET["search_date1"]));
                $today = date("Y-m-d", strtotime($_GET["search_date2"]));
            } else {
                $today = date("Y-m-d");
                $first_date = date("Y-m-01");

                $data["search_date1"] = "";
                $data["search_date2"] = "";
            }

            if (Auth::user()->role == 2) {
                $user_id = Auth::user()->id;
                $data["empstatus"] = 0;
                $data["title"] = "Total Admission" . " " . date("F Y");

                $data["report"] = Salespackage::join("users", "users.id", "=", "sales_package.student_id")
                    ->join('ev_package', 'ev_package.pac_id', '=', 'sales_package.package_id')
                    ->where("sales_package.sales_id", $user_id)

                    ->where(function ($query) {
                        $query->orWhere('sales_package.package_type', 1)->orWhere('sales_package.package_type', 2);
                    })

                    ->whereBetween("sales_package.joining_date", [$first_date, $today])
                    ->orderby("sales_package.id", "desc")
                    ->distinct()
                    ->get([
                        "sales_package.id",
                        "sales_package.package_amount  as payment",
                        "sales_package.package_tax  as tax",
                        "sales_package.package_fullamount  as total",
                        "sales_package.joining_date  as date",
                        "users.name",
                        "users.student_admissionid",
                        "ev_package.pac_name",
                    ]);

                $data["oldreport"] = Oldstudent::where("sales_id", Auth::user()->id)
                    ->whereBetween("date_joining", [$first_date, $today])
                    ->get([
                        "ev_oldstudents.name",
                        "ev_oldstudents.reg_no as student_admissionid",
                        "ev_oldstudents.package as pac_name",
                        "ev_oldstudents.fees as payment",
                        "ev_oldstudents.tax",
                        "ev_oldstudents.totalfees as total",
                        "ev_oldstudents.date_joining as date",
                    ]);

                $data["monthadmi_regular_withgst"] = round(
                    Salespackage::where("sales_package.sales_id", $user_id)
                        ->where(function ($query) {
                            $query->orWhere('sales_package.package_type', 1)->orWhere('sales_package.package_type', 2);
                        })
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_fullamount")
                );
                $data["reg_old_withgst"] = round(
                    Oldstudent::where("ev_oldstudents.sales_id", $user_id)
                        ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                        ->sum("ev_oldstudents.totalfees")
                );
                $data["monthly_tot_admissionwithgst"] = $data["monthadmi_regular_withgst"] + $data["reg_old_withgst"];

                $data["monthadmi_regular_withoutgst"] = round(
                    Salespackage::where("sales_package.sales_id", $user_id)
                        ->where(function ($query) {
                            $query->orWhere('sales_package.package_type', 1)->orWhere('sales_package.package_type', 2);
                        })
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_amount")
                );

                $data['reg_old_withoutgst'] = round(
                    Oldstudent::where("ev_oldstudents.sales_id", $user_id)
                        ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                        ->sum("ev_oldstudents.fees")
                );
                $data['monthly_tot_admissionwithoutgst'] = $data["monthadmi_regular_withoutgst"] + $data['reg_old_withoutgst'];

                $data["monthadmi_regular_tax"] = round(
                    Salespackage::where("sales_package.sales_id", $user_id)
                        ->where(function ($query) {
                            $query->orWhere('sales_package.package_type', 1)->orWhere('sales_package.package_type', 2);
                        })
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_tax")
                );

                $data['reg_old_tax'] = round(
                    Oldstudent::where("ev_oldstudents.sales_id", $user_id)
                        ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                        ->sum("ev_oldstudents.tax")
                );
                $data['monthly_tot_admissiontax'] = $data["monthadmi_regular_tax"] + $data['reg_old_tax'];

                return view("monthlyregold_admission", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }
}
