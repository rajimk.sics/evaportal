<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Illuminate\Support\Carbon;
use App\Models\User;
use App\Models\Topic;
use League\Csv\Reader;
use App\Models\Package;
use App\Models\Subtopic;
use App\Models\Questions;
use App\Models\Technology;
use App\Models\Studentpackage;
use App\Models\Employeeinfo;
use App\Models\Prerequisite;
use Illuminate\Http\Request;
use App\Models\Packagesubtopic;
use App\Models\Packagesyllabus;
use App\Models\Subtopictechnology;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ExcelcommonController;


class MentorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $ExcelcommonController;

    public function __construct()
    {
        $this->middleware("auth");
        $this->ExcelcommonController = new ExcelcommonController();
    }

    public function trainerStudents(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
    
        if (!in_array(Auth::user()->role, [3])) {
            return redirect('/accessdenied');
        }
    
        $trainer_id = Auth::user()->id;
        $data['title'] = 'Your Students';
        $year = $request->query('year', Carbon::now()->year);
        $month = $request->query('month', Carbon::now()->format('m'));
        $pac_type = $request->query('packtype', '1');
        $allStudents = $request->query('all', false);
    
        if (!checkdate($month, 1, $year) && !$allStudents) {
            return redirect('/accessdenied');   
        }
    
        $query = Studentpackage::join('users', 'users.id', '=', 'ev_student_package.student_id')
            ->join('ev_student_info', 'ev_student_info.student_id', '=', 'users.id')
            ->join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
            ->leftJoin('ev_student_trainer', 'ev_student_trainer.student_id', '=', 'users.id')
            ->where('ev_student_trainer.trainer_id', $trainer_id)
            ->where('ev_package.pac_type', $pac_type);
    
        if (!$allStudents) {
            $startOfMonth = Carbon::create($year, $month, 1)->startOfMonth()->format('Y-m-d');
            $endOfMonth = Carbon::create($year, $month, 1)->endOfMonth()->format('Y-m-d');
            $query->whereBetween('ev_student_package.joining_date', [$startOfMonth, $endOfMonth]);
        }
    
        $data1['studlist'] = $query->orderby('users.id', 'desc')
            ->get([
                'users.name',
                'users.id',
                'users.phone',
                'ev_package.pac_name',
                'users.email',
                'ev_student_info.sales_id',
                'ev_student_package.due_date_first',
                'ev_student_package.enddate',
                'ev_student_package.package_type as pac_type',
                'ev_student_package.package_id as package_id',
                'ev_student_package.joining_date',
                'ev_package.tech_id',
                'ev_student_info.student_admissionid',
            ]);
    
        $data = [
            'title' => 'Your Students',
            'studlist' => $data1['studlist'],
            'years' => CommonController::generateYearRange(date('Y') - 10),
            'months' => CommonController::generateMonthOptions(),
            'currentYear' => $year,
            'currentMonth' => $month,
            'pac_type' => $pac_type,
        ];
    
        return view("manage_students_mentor", $data);
    }







    public function getTopics(Request $request, $techId = null, $assigning = null)
    {
        $techId = $request->input('tech_id');
        $assigning = $request->input('assigning');
        
        $topics = Topic::join('ev_topictechnology', 'ev_topics.id', '=', 'ev_topictechnology.topic_id')
            ->join('ev_technology', 'ev_topictechnology.technology', '=', 'ev_technology.id');
        
        if ($assigning === "filter") {
            $topics->where('ev_topics.status', '=', 1);
        }
        
        if ($techId) {
            $topics->whereIn('ev_technology.id', (array)$techId);
        }
        
        $topics->select(
            'ev_topics.id',
            'ev_topics.topics',
            'ev_topics.trainer_id',
            'ev_topics.status',
            DB::raw("GROUP_CONCAT(DISTINCT ev_technology.technology SEPARATOR ', ') as all_technologies")
        )
        ->groupBy('ev_topics.id', 'ev_topics.topics', 'ev_topics.trainer_id', 'ev_topics.status')
        ->orderBy('ev_topics.id', 'desc');

        $paginatedTopics = $topics->paginate(10);
        $hasMore = $paginatedTopics->hasMorePages();
        $nextPageUrl = $paginatedTopics->appends([
            'tech_id' => $techId,
            'assigning' => $assigning,
        ])->nextPageUrl();
    
        $subtopicsByTopics = $this->getSubtopicsByTopics($paginatedTopics, $techId);
        
        $topicsWithSubtopics = $paginatedTopics->map(function ($topic) use ($subtopicsByTopics) {
            $topic->subtopics = $subtopicsByTopics[$topic->id] ?? [];
            return $topic;
        });
    
        return response()->json([
            'data' => $topicsWithSubtopics,
            'next_page_url' => $nextPageUrl,
            'hasMore' => $hasMore,

        ]);
    }
        //TOPIC AND SUBTOPIC
    private function handleRedirect($responseData)
    {
        if (is_array($responseData) && isset($responseData['duplicates']) && $responseData['duplicates']) {
            Session::flash('duplicates', $responseData['duplicates']);
            return redirect()->back();
        }
        if (isset($responseData['error']) && !empty($responseData['error'])) {
            return redirect()->back()->withErrors(['error' => $responseData['error']]);
                }
        else {
            Session::flash('success', 'Successfully Updated');
            return redirect('/topicListingByTechnology');
        }
    }
    
    private function redirectWithSubtopics($responseData)
    {
        if (isset($responseData['duplicates']) && !empty($responseData['duplicates'])) {
            Session::flash('duplicates', $responseData['duplicates']);
            return redirect()->back();
        } if (isset($responseData['error']) && !empty($responseData['error'])) {
            return redirect()->back()->withErrors(['error' => $responseData['error']]);
                }
        else {
            Session::flash('success', 'Successfully Updated');
            return redirect('/topicListingByTechnology');
        }
    }
    private function getTechnologyOfTrainer($id)
    {
        return  Employeeinfo::join('ev_technology', function ($join) {
            $join->on(DB::raw("FIND_IN_SET(ev_technology.id, employee_info.technology)"), '>', DB::raw('0'));
        })
            ->where('employee_info.user_id', $id)
            ->where("ev_technology.status", 1)
            ->get();
    }

    public function uploadSylabus()
    {

        if (!Auth::check()) {
            return redirect('/');
        }

        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }

        $data = [
            'title' => 'Syllabus Upload',
            'technologies' =>  Technology::where('status', '1')->get()
        ];
        return view('syllabusupload', $data);
    }
    public function UploadSyllabusByTechnology(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }

        $request->validate([
            'csv_file' => 'required|mimes:csv',
            'technology' => 'required|array|min:1',
            'technology.*' => 'exists:ev_technology,id',
        ]);

        $technologyId = $request->input('technology');
        $file = $request->file('csv_file');
        $result = $this->ExcelcommonController->processSyllabusUpload($file, $technologyId);
        return $this->handleRedirect($result);
    }
    private function getTopicListing($techId = null, $assigning = null)
    {
        $query = Topic::join('ev_topictechnology', 'ev_topics.id', '=', 'ev_topictechnology.topic_id')
            ->join('ev_technology', 'ev_topictechnology.technology', '=', 'ev_technology.id'); 
    
        if ($assigning == "filter") {
            $query->where('ev_topics.status', '=', 1);
        }
    
        if ($techId) {
            $query->whereIn('ev_technology.id', (array)$techId);
        }
    
            $query->select(
            'ev_topics.id',
            'ev_topics.topics',
            'ev_topics.trainer_id',
            'ev_topics.status',
            
            DB::raw("GROUP_CONCAT(DISTINCT ev_technology.technology SEPARATOR ', ') as all_technologies")
        )
        ->groupBy('ev_topics.id', 'ev_topics.topics', 'ev_topics.trainer_id', 'ev_topics.status')
        ->orderBy('ev_topics.id', 'desc');
    
        return $query->get();
    }
       

    private function selectedTopics(Request $request)
    {
        return $request->input('selected_topics');
    }

    public function topicListingByTechnology(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }

        $selectedTopics = $this->selectedTopics($request);
        $techId = $request->input('techname');
        $data = [
            "technologies" => Technology::all(),
            "selectedTopics" => $selectedTopics,
            "title" => "Topic Listing",
            "tech_id" => $techId,
        ];

        return view("topic_listing", $data);
    }
    private function getSubtopicsByTopics($topics, $techId = null)
    {
        $topicIds = $topics->pluck('id');

        $query = Subtopic::leftJoin('ev_subtopictechnology', 'ev_subtopics.id', '=', 'ev_subtopictechnology.subtopic_id')
            ->leftJoin('ev_technology', 'ev_subtopictechnology.technology', '=', 'ev_technology.id')
            ->whereIn('ev_subtopictechnology.topic_id', $topicIds);

        if ($techId) {
            $query->whereIn('ev_technology.id', (array)$techId);
        }

        return $query->select(
            'ev_subtopics.id as subtopic_id',
            'ev_subtopictechnology.topic_id',
            'ev_subtopics.sub_topics as subtopic_name',
            DB::raw("GROUP_CONCAT(DISTINCT ev_technology.technology SEPARATOR ', ') as technologies")
        )
            ->groupBy('ev_subtopics.id', 'ev_subtopictechnology.topic_id', 'ev_subtopics.sub_topics')
            ->get()
            ->groupBy('topic_id');
    }

    public function fetchLinkedTechnologies(Request $request)
    {
        $request->validate([
            'topic_id' => 'required|integer',
        ]);

        $topic_id = $request->get('topic_id');

        $technologies = Topic::join('ev_topictechnology', 'ev_topics.id', '=', 'ev_topictechnology.topic_id')
            ->join('ev_technology', 'ev_topictechnology.technology', '=', 'ev_technology.id')
            ->where('ev_topics.id', $topic_id)
            ->get(['ev_technology.technology', 'ev_technology.id']);

        return response()->json(['tech' => $technologies]);
    }


    public function subtopicUploadByTechnology(Request $request)
    {
        $topicId = $request->input('topic_id');
        $tech_id = $request->input('technology');


        $request->validate([
            'csv_file' => 'required|mimes:csv,txt',
        ]);

        $file = $request->file('csv_file');
        $result = $this->ExcelcommonController->processSubtopicUpload($topicId, $file, $tech_id);



        return $this->redirectWithSubtopics($result);
    }




    public function viewTechnologySyllabus($pacid, Request $request)
    {

        if (!Auth::check()) {
            return redirect('/');
        }

        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }

        $package = Package::where('pac_id', $pacid)->first(['tech_id', 'pac_name']);
        $selectedTechId = $request->input('techname') ?? $package->tech_id;





        $data = [
            "technologies" => Technology::all(),
            "tech_id" => $selectedTechId,
            'pac_name' => $package->pac_name,
            "pac_id" => $pacid
        ];


        return view("add_packagesyllabus", $data);
    }
    //function for assigning syllabus to packages
    public function assignSyllabus(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'pac_id' => 'required',
            'selected_topics' => 'required',
            'selected_topics.*' => 'required',
        ]);


        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Please select at least one topic');
        }

        $pac_id = $request->input('pac_id');
        $topicIds = $request->input('selected_topics', []);
        $subtopicIds = $request->input('selected_subtopics');
        $subtopicDuplicates = [];

        $topicDuplicates = $this->processSyllabusAssignment($pac_id, $topicIds);
        if($subtopicIds){
            $subtopicDuplicates = $this->processSubtopicAssignment($pac_id, $subtopicIds, $topicIds);

        }

        
        $duplicates = array_merge($topicDuplicates, $subtopicDuplicates);

        if (!empty($duplicates)) {
            return redirect()->back()->with('duplicates', $duplicates)
           ->with('success', 'Syllabus assigned successfully!');
        } else {
            return redirect('/viewPacSyllabus/'.$pac_id)->with('success', 'Syllabus assigned successfully!');
        }
    }

    private function processSyllabusAssignment($pac_id, array $topicIds)
    {
        $duplicates = [];

        foreach ($topicIds as $topicId) {
            if ($this->isDuplicateSyllabus($pac_id, $topicId)) {
                $duplicates[] = $this->getDuplicateDetails($pac_id, $topicId);
            } else {
                $this->createSyllabusEntry($pac_id, $topicId);
            }
        }

        return $duplicates;
    }
    private function isDuplicateSyllabus($pac_id, $topicId)
    {
        return Packagesyllabus::where('pac_id', $pac_id)
            ->where('topic_id', $topicId)
            ->exists();
    }
    private function getDuplicateDetails($pac_id, $topicId)
    {
        $topic = Topic::where('id', '=', $topicId)->value('topics');
        $package = Package::where('pac_id', '=', $pac_id)->value('pac_name');

        return [
            'dupli' => $topic,
            'technology' => $package,
        ];
    }
    private function createSyllabusEntry($pac_id, $topicId)
    {
        Packagesyllabus::create([
            'pac_id' => $pac_id,
            'topic_id' => $topicId,
        ]);
    }

    ///// for subtopics
    private function processSubtopicAssignment($pac_id, array $subtopicIds, array $topics)
{
    $duplicates = [];

    foreach ($subtopicIds as $subtopicId) {
        $topicIds = Subtopictechnology::where('subtopic_id', $subtopicId)->pluck('topic_id')->toArray();

        foreach ($topics as $topicId) {
            if (!in_array($topicId, $topicIds)) {
                continue; 
            }
        
            if ($this->isDuplicateSubtopic($pac_id, $topicId, $subtopicId)) {
                $duplicates[] = $this->getSubtopicDuplicateDetails($pac_id, $topicId, $subtopicId);
            } else {
                $this->createSubtopicMapping($pac_id, $topicId, $subtopicId);
            }
        }
    }

    return $duplicates;
}

private function isDuplicateSubtopic($pac_id, $topicId, $subtopicId)
{
    return Packagesubtopic::where('pac_id', $pac_id)
        ->where('topic_id', $topicId)
        ->where('subtopic_id', $subtopicId)
        ->exists();
}

private function getSubtopicDuplicateDetails($pac_id, $topicId, $subtopicId)
{
    $topicName = Topic::where('id', $topicId)->value('topics');
    $subtopicName = Subtopic::where('id', $subtopicId)->value('sub_topics');
    $packageName = Package::where('pac_id', $pac_id)->value('pac_name');

    return [
        'technology' => $packageName,
        'dupli' => $topicName,
        'subtopic' => $subtopicName,
    ];
}

private function createSubtopicMapping($pac_id, $topicId, $subtopicId)
{
    Packagesubtopic::create([
        'pac_id' => $pac_id,
        'topic_id' => $topicId,
        'subtopic_id' => $subtopicId,
    ]);
}



public function viewPacSyllabus($pacId)
{
    $data['pac_id'] = $pacId;
    $data['pac_name'] = Package::where('pac_id', $pacId)->value('pac_name');
    $topicIds = Packagesyllabus::where('pac_id', '=', $pacId)->pluck('topic_id')->toArray();
    $data['topics'] = Topic::whereIn('id',$topicIds)->get();


    $topicsWithSubtopics = [];

    foreach ($topicIds as $topicId) {
        $subtopicIds = Packagesubtopic::where('topic_id', '=', $topicId)
        ->where('pac_id','=',$pacId)
        -> pluck('subtopic_id')->toArray();

        $subtopics = Subtopic::whereIn('id', $subtopicIds)->get(['id', 'sub_topics']);

        $topicsWithSubtopics[$topicId] = $subtopics;
    }

    foreach ($data['topics'] as $topic) {
        if (isset($topicsWithSubtopics[$topic->id])) {
            $topic->subtopics = $topicsWithSubtopics[$topic->id];
        } else {
            $topic->subtopics = collect(); 
        }
    }

    return view('syllabus_listing', $data);
}

function removeTopic(Request $request)
{
    $pacid = $request->input('pacid');
    $topicid = $request->input('topicid');

    Packagesyllabus::where('pac_id', $pacid)->where('topic_id', $topicid)->delete();
    Packagesubtopic::where('pac_id', $pacid)->where('topic_id', $topicid)->delete();

    return response()->json(['status' => 'success', 'message' => 'Topic removed successfully.']);
}
}
