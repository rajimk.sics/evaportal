<?php
namespace App\Http\Middleware\CheckStatus;
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Technology;
use App\Models\Designation;
use App\Models\Qualification;
use App\Models\Source;
use App\Models\College;
use App\Models\Tax;
use App\Models\Package;
use App\Models\User;
use App\Models\Specialization;
use App\Models\Employee;
use App\Models\Othereference;
use App\Models\Payment;
use App\Models\Salespayment;
use App\Models\Event;
use App\Models\Signature;
use App\Models\Salespackage;
use App\Models\PageContent;
use App\Models\Gatepass;
use App\Models\Talento;
use App\Models\Studentpackage;
use App\Models\Reporting;
use App\Models\OldPayment;
use Session;
use Carbon\Carbon;
use Auth;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware("auth");
    }


    private function getPaymentHistory($startOfMonth, $endOfMonth, $status)
    {
        $query = Payment::join('users', 'users.id', '=', 'ev_payment.student_id')
            ->where('ev_payment.finance_status', $status)
            ->whereBetween('ev_payment.date', [$startOfMonth, $endOfMonth])
            ->orderBy('ev_payment.id', 'desc');
    
        if (Auth::user()->role == 2) {
            $query->join('sales_payment', 'sales_payment.payment_id', '=', 'ev_payment.id')
                  ->where('sales_payment.sales_id', Auth::user()->id);
        }
    
        return $query->get([
            'ev_payment.paid_amount as payment_gst',
            'ev_payment.fees_income as payment',
            'ev_payment.tax',
            'ev_payment.payment_type',
            'ev_payment.screenshot',
            'ev_payment.transaction_id',
            'ev_payment.student_id',
            'ev_payment.finance_status',
            'ev_payment.date',
            'ev_payment.id as payment_id',
            'ev_payment.reason',
            'ev_payment.action_by',
            'ev_payment.reason_type',
            'users.name',
        ])->map(function ($item) {
            $item->source = 'payment';
            return $item;
        });
    }
private function getOldPaymentHistory($startOfMonth, $endOfMonth, $status)
{
    $query = OldPayment::leftJoin('ev_oldstudents', 'ev_oldstudents.id', '=', 'olddata_payment.oldstudent_id')
        ->where('olddata_payment.finance_status', $status)
        ->whereBetween('olddata_payment.date', [$startOfMonth, $endOfMonth])
        ->orderBy('olddata_payment.id', 'desc');

    if (Auth::user()->role == 2) {
        $query->where('olddata_payment.sales_id', Auth::user()->id);
    }
    return $query->get([
        'olddata_payment.paid_amount as payment_gst',
        'olddata_payment.fees_income as payment',
        'olddata_payment.tax',
        'olddata_payment.date',
        'ev_oldstudents.name',
        'olddata_payment.payment_type',
        'olddata_payment.screenshot',
        'olddata_payment.transaction_id',
        'olddata_payment.oldstudent_id as student_id',
        'olddata_payment.id as payment_id',
        'olddata_payment.sales_id',
        'olddata_payment.reason',
        'olddata_payment.action_by',
        'olddata_payment.reason_type',
    ])->map(function ($item) {
        $item->source = 'old_payment';
        return $item;
    });
}

private function getPaymentTotals($startOfMonth, $endOfMonth,$status)
    {
        $paymentTotals = Payment::where('finance_status',$status)
            ->whereBetween('date', [$startOfMonth, $endOfMonth])
            ->selectRaw('SUM(fees_income) as total_payment, SUM(tax) as total_tax, SUM(fees_income + tax) as total_payment_withgst')
            ->first();
    
        $oldPaymentTotals = OldPayment::where('finance_status',$status)
            ->whereBetween('date', [$startOfMonth, $endOfMonth])
            ->selectRaw('SUM(fees_income) as total_payment, SUM(tax) as total_tax, SUM(fees_income + tax) as total_payment_withgst')
            ->first();
    
        return [
            'total_payment' => ($paymentTotals->total_payment ?? 0) + ($oldPaymentTotals->total_payment ?? 0),
            'total_tax' => ($paymentTotals->total_tax ?? 0) + ($oldPaymentTotals->total_tax ?? 0),
            'total_payment_withgst' => ($paymentTotals->total_payment_withgst ?? 0) + ($oldPaymentTotals->total_payment_withgst ?? 0),
        ];
    }   
    private function getMonths()
    {
        return [
            '01' => 'January',
            '02' => 'February',
            '03' => 'March',
            '04' => 'April',
            '05' => 'May',
            '06' => 'June',
            '07' => 'July',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        ];
    }
    public function pending_payment()
    {
        
        if (!Auth::check()) {
            return redirect('/');
        }
        if (!in_array(Auth::user()->role, [7,2])) {
            return redirect('/accessdenied');
        }

        if (!empty($_GET['year']) && !empty($_GET['month'])) {
            $year = $_GET['year'];
            $month = $_GET['month'];

            $currentYear =$year;
            $currentMonth =$month;
    
            $startOfMonth = Carbon::create($year, $month, 1)->startOfMonth()->format('Y-m-d');
            $endOfMonth = Carbon::create($year, $month, 1)->endOfMonth()->format('Y-m-d');
        } else {
            $startOfMonth = Carbon::now()->startOfMonth()->format('Y-m-d');
            $endOfMonth = Carbon::now()->endOfMonth()->format('Y-m-d');

            $currentYear = Carbon::now()->year;
            $currentMonth = Carbon::now()->format('m');
        }


        $paymentHistory = $this->getPaymentHistory($startOfMonth, $endOfMonth,0);
        $oldReport = $this->getOldPaymentHistory($startOfMonth, $endOfMonth,0);
    
        $combinedResults = $paymentHistory->concat($oldReport);
        $totals = $this->getPaymentTotals($startOfMonth, $endOfMonth,0);
    
     
        $data = [
            'payment_history' => $combinedResults,
            'total_payment_withgst' => $totals['total_payment_withgst'],
            'total_tax' => $totals['total_tax'],
            'total_payment' => $totals['total_payment'],
            'years' => range(Carbon::now()->year - 10, Carbon::now()->year),
            'months' => $this->getMonths(),
            'currentYear' =>$currentYear,
            'currentMonth' =>$currentMonth,
            'title' => 'Payments - Pending Payments',
        ];
    
        return view('pending_payments', $data);
    }





 public function approved_payment()
{
    
    if (!Auth::check()) {
        return redirect('/');
    }

    if (!in_array(Auth::user()->role, [7, 2])) {
        return redirect('/accessdenied');
    }

   
    if (!empty($_GET['year']) && !empty($_GET['month'])) {
        $year = $_GET['year'];
        $month = $_GET['month'];

        $currentYear =$year;
        $currentMonth =$month;

        $startOfMonth = Carbon::create($year, $month, 1)->startOfMonth()->format('Y-m-d');
        $endOfMonth = Carbon::create($year, $month, 1)->endOfMonth()->format('Y-m-d');
    } else {
        $startOfMonth = Carbon::now()->startOfMonth()->format('Y-m-d');
        $endOfMonth = Carbon::now()->endOfMonth()->format('Y-m-d');
        $currentYear = Carbon::now()->year;
        $currentMonth = Carbon::now()->month;
    }

    $paymentHistory = $this->getPaymentHistory($startOfMonth, $endOfMonth,1);


    $oldReport = $this->getOldPaymentHistory($startOfMonth, $endOfMonth,1);

    $combinedResults = $paymentHistory->concat($oldReport);
    $totals = $this->getPaymentTotals($startOfMonth, $endOfMonth,1);



    // Prepare the data for the view
    $data = [
        'payment_history' => $combinedResults,
        'total_payment_withgst' => $totals['total_payment_withgst'],
        'total_tax' => $totals['total_tax'],
        'total_payment' => $totals['total_payment'],
        'years' => range($currentYear - 10, $currentYear),
        'months' => $this->getMonths(),
        'currentYear' => $currentYear,
        'currentMonth' => $currentMonth,
        'title' => 'Payments - Approved Payments',
    ];

    return view('approved_payments', $data);
}


public function rejected_payment()
{
    if (!Auth::check()) {
        return redirect('/');
    }

    if (!in_array(Auth::user()->role, [7, 2])) {
        return redirect('/accessdenied');
    }

    // Determine the date range
    if (!empty($_GET['year']) && !empty($_GET['month'])) {
        $year = $_GET['year'];
        $month = $_GET['month'];

        $currentYear =$year;
        $currentMonth =$month;

        $startOfMonth = Carbon::create($year, $month, 1)->startOfMonth()->format('Y-m-d');
        $endOfMonth = Carbon::create($year, $month, 1)->endOfMonth()->format('Y-m-d');
    } else {
        $startOfMonth = Carbon::now()->startOfMonth()->format('Y-m-d');
        $endOfMonth = Carbon::now()->endOfMonth()->format('Y-m-d');
        $currentYear = Carbon::now()->year;
        $currentMonth = Carbon::now()->month;
    }

    $paymentHistory = $this->getPaymentHistory($startOfMonth, $endOfMonth,2);
    $oldReport = $this->getOldPaymentHistory($startOfMonth, $endOfMonth,2);

    $combinedResults = $paymentHistory->concat($oldReport);
    $totals = $this->getPaymentTotals($startOfMonth, $endOfMonth,2);

    $data = [
        'payment_history' => $combinedResults,
        'total_payment_withgst' => $totals['total_payment_withgst'],
        'total_tax' => $totals['total_tax'],
        'total_payment' => $totals['total_payment'],
        'years' => range($currentYear - 10, $currentYear),
        'months' => $this->getMonths(),
        'currentYear' => $currentYear,
        'currentMonth' => $currentMonth,
        'title' => 'Payments - Rejected Payments',
    ];

    return view('rejected_payments', $data);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////











    
public function handleAccountStatus(Request $request, $status)
{
   
    if (Auth::check() && Auth::user()->role == 7) {

        $paymentid = $request->get('payment_id');
        $source = $request->get('source');
        $reason = $request->get('reason', null);
        $reason_type = $request->get('reason_type');

        $financeStatus = ($status == 'approve') ? 1 : 2;

        $data = [
            'finance_status' => $financeStatus,
            'action_by' => Auth::user()->id,
            'reason' => $reason,
            'reason_type'=> $reason_type
        ];

        if ($source == 'payment') {
            Payment::where('id', $paymentid)->update($data);
        } elseif ($source == 'old_payment') {
            OldPayment::where('id', $paymentid)->update($data);
        }

        echo 1;
    } else {
        return redirect('/acessdenied');
    }
}

public function accountsreject(Request $request)
{
    return $this->handleAccountStatus($request, 'reject');
}

public function accountsapprove(Request $request)
{
    return $this->handleAccountStatus($request, 'approve');
}

}
