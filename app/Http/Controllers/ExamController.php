<?php

namespace App\Http\Middleware\CheckStatus;

namespace App\Http\Controllers;

use Auth;
use Session;
use App\Models\Exam;
use App\Models\User;
use App\Models\College;
use App\Models\Package;
use App\Models\Questions;
use App\Models\Technology;
use App\Imports\ImportQues;
use App\Models\Designation;
use App\Models\Employeeinfo;
use App\Models\Examquestion;
use Illuminate\Http\Request;
use App\Models\Examtechnology;
use App\Models\Questiontechnology;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ExcelcommonController;

class ExamController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $ExcelcommonController;

    public function __construct()
    {
        $this->middleware("auth");
        $this->ExcelcommonController = new ExcelcommonController();
    }
    //Questions Upload

    public function viewQuestionUpload()
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }
        $technologies = Technology::get(['id', 'technology']);
        $data = [
            'title' => 'Upload Question',
            'technologies' => $technologies,
        ];
        return view('questions_upload', $data);
    }
    public function questionsUpload(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        if (Auth::user()->role != 3) {
            return redirect('/accessdenied');
        }
        $request->validate([
            'technology' => 'required|array|min:1',
            'csv_file' => 'required|mimes:csv,txt,xlsx,xls|max:2048'
        ]);
        $technologies = $request->input('technology');
        $csv_file = $request->file('csv_file');
        $result = $this->ExcelcommonController->processCsvUpload($csv_file, $technologies);
        return $this->handleRedirect($result);
    }
    private function handleRedirect($responseData)
    {
        if (isset($responseData['error'])) {
            Session::flash('error', $responseData['error']);
            return redirect()->back();
        }
        if (isset($responseData['duplicates']) && !empty($responseData['duplicates'])) {
            Session::flash('duplicates', $responseData['duplicates']);
            return redirect()->back();
        } else {
            Session::flash('success', 'Successfully Updated');
            return redirect('/manageQuestions');
        }
    }

    public function manageQuestions(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }

        $query = Questions::select(
            'ev_questions.id',
            'ev_questions.status',
            'ev_questions.questions',
            'ev_questions.optiona',
            'ev_questions.optionb',
            'ev_questions.optionc',
            'ev_questions.optiond',
            'ev_questions.answer',
            DB::raw("GROUP_CONCAT(ev_technology.technology SEPARATOR ', ') as technology_list")
        )
            ->join("ev_questiontechnology", "ev_questiontechnology.question_id", "=", "ev_questions.id")
            ->join("ev_technology", "ev_questiontechnology.technology", "=", "ev_technology.id")
            ->groupBy(
                'ev_questions.id',
                'ev_questions.status',
                'ev_questions.questions',
                'ev_questions.optiona',
                'ev_questions.optionb',
                'ev_questions.optionc',
                'ev_questions.optiond',
                'ev_questions.answer'
            )
            ->orderBy('ev_questions.id');

        if ($request->filled('technology')) {
            $technologies = $request->get('technology');
            $query->whereIn('ev_questiontechnology.technology', $technologies);
        }
        $data = [
            'questions' => $query->get(),
            'technologies' => Technology::all()
        ];

        return view('manage_questions', $data);
    }

   

    public function getQuestion($id)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }
        $question = Questions::findOrFail($id);
        return response()->json($question);
    }

    public function updateQuestion(Request $request)
    {
        $request->validate([
            'questions' => 'required|string',
            'optiona' => 'required|string',
            'optionb' => 'required|string',
            'optionc' => 'required|string',
            'optiond' => 'required|string',
            'answer' => 'required|in:A,B,C,D',
        ]);

        $question = Questions::findOrFail($request->id);
        $question->update([
            'questions' => $request->questions,
            'optiona' => $request->optiona,
            'optionb' => $request->optionb,
            'optionc' => $request->optionc,
            'optiond' => $request->optiond,
            'answer' => $request->answer,
        ]);
        Session::flash('success', 'Question updated successfully');
        return response()->json(['success' => true, 'message' => 'Question updated successfully']);
    }

    public function addExamquestions($exam_id, Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }
        $exam = Exam::where('id', '=', $exam_id)->value('exam_name');
        $assignedTechnologies = Examtechnology::where('exam_id', $exam_id)
            ->pluck('technology_id')
            ->toArray();
        $assignedQuestions = Examquestion::where('exam_id', $exam_id)->pluck('question_id')->toArray();

        $selectedTechnologies = $request->get('technology', $assignedTechnologies);
        $questions = $this->getFilteredQuestions($selectedTechnologies);

        $data = [
            'questions' => $questions,
            'technologies' => Technology::all(),
            'exam_id' => $exam_id,
            'selectedTechnologies' => $selectedTechnologies,
            'exam' => $exam,
            'assignedQuestions' => $assignedQuestions,
        ];
        return view('manage_examquestion', $data);
    }

    private function getFilteredQuestions(array $selectedTechnologies)
    {
        return Questions::join('ev_questiontechnology', 'ev_questions.id', '=', 'ev_questiontechnology.question_id')
            ->join('ev_technology', 'ev_questiontechnology.technology', '=', 'ev_technology.id')
            ->whereIn('ev_questiontechnology.technology', $selectedTechnologies)
            ->where('ev_questions.status', 1)
            ->groupBy(
                'ev_questions.id',
                'ev_questions.status',
                'ev_questions.questions',
                'ev_questions.optiona',
                'ev_questions.optionb',
                'ev_questions.optionc',
                'ev_questions.optiond',
                'ev_questions.answer',
                'ev_questions.updated_at',
                'ev_questions.created_at'
            )
            ->get(['ev_questions.*', DB::raw('GROUP_CONCAT(ev_technology.technology ORDER BY ev_technology.technology DESC) as technologies')]);
    }

    // function for assigning questions to an exam
    public function saveExamquestion(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }

        $exam_id = $request->exam_id;
        $question_ids = array_filter(explode(',', $request->question_ids));


        if (empty($question_ids)) {
            return redirect()->back()->with('error', 'Please select at least one question.');
        }
        [$duplicateMessages, $nonDuplicateQuestions] = $this->filterDuplicateQuestions($exam_id, $question_ids);
        $this->insertQuestions($exam_id, $nonDuplicateQuestions);

        if (!empty($nonDuplicateQuestions) && !empty($duplicateMessages)) {
            return redirect()->back()->with([
                'success' => 'Successfully uploaded.',
                'duplicates' => $duplicateMessages,
            ]);
        } elseif (!empty($nonDuplicateQuestions)) {
            return redirect('/viewAssignedquestions/' . $exam_id)->with([
                'success' => 'Successfully uploaded questions.',
            ]);
        } else {
            return redirect()->back()->with('duplicates', $duplicateMessages);
        }
    }

    private function filterDuplicateQuestions($exam_id, $question_ids)
    {
        $duplicateMessages = [];
        $nonDuplicateQuestions = [];

        foreach ($question_ids as $question_id) {
            if (Examquestion::where('exam_id', $exam_id)->where('question_id', $question_id)->exists()) {
                $duplicateMessages[] = "For Exam: " . Exam::find($exam_id)->exam_name .
                    ", question: " . Questions::find($question_id)->questions . " is duplicate.";
            } else {
                $nonDuplicateQuestions[] = $question_id;
            }
        }
        return [$duplicateMessages, $nonDuplicateQuestions];
    }

    private function insertQuestions($exam_id, $question_ids)
    {
        foreach ($question_ids as $question_id) {
            Examquestion::create(['exam_id' => $exam_id, 'question_id' => $question_id]);
        }
    }

    //Function for removing assigned question
    public function removeExamQuestion(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }
        $exam_id = $request->input('exam_id');
        $question_id = $request->input('question_id');
        Examquestion::where('exam_id', $exam_id)
            ->where('question_id', $question_id)
            ->delete();

        return response()->json(['status' => 'success', 'message' => 'Question removed successfully.']);
    }


    // function for viewing assigned questions to an Exam
    public function viewAssignedquestions($exam_id)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }
        $exam = Exam::where('id', '=', $exam_id)->value('exam_name');
        $questions = Examquestion::where('exam_id', $exam_id)
            ->join('ev_questions', 'ev_examquestion.question_id', '=', 'ev_questions.id')
            ->where('ev_questions.status', 1)
            ->orderBy('ev_examquestion.id','desc')
            ->get(['ev_questions.*']);

        $data = [
            'exam' => $exam,
            'exam_id' => $exam_id,
            'questions' => $questions
        ];
        return view('view_assignedquestions', $data);
    }





    /////////////////////////////////////////////////////////////////////////////////



    //// Exams from rohan

    public function getPackagesByTechnology(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        $technologyIds = $request->input('techtrainerselect');
        $packages = Package::whereIn('tech_id', $technologyIds)->get(['pac_id', 'pac_name']);
        if ($packages->isEmpty()) {
            return response()->json(['message' => 'No packages found for the selected technologies'], 404);
        }
        return response()->json([
            'message' => 'Packages retrieved successfully',
            'data' => $packages
        ]);
    }
    public function getPackages(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        $technologyId = $request->input('techselect');
        $data = Package::where('tech_id', $technologyId)->get(['pac_id', 'pac_name']);
        if ($data->isEmpty()) {
            return response()->json(['message' => 'No packages found for the selected technologies'], 404);
        }
        return response()->json([
            'message' => 'Packages retrieved successfully',
            'data' => $data
        ]);
    }

    public function fetchExam(Request $request)
    {

        if (!Auth::check()) {
            return redirect("/");
        }
        if (Auth::user()->role != 3) {
            return redirect("/accessdenied");
        }

        $data = [
            "title" => "View Exams",
            "selectedExamType" => $request->input('examtechnology'),
            "selectedTechnology" => $request->input('techselect'),
            "selectedPackage" => $request->input('techpackages'),
            "technology" => $this->getTechnologies(Auth::user()->id),
        ];

        $data['exams'] = $this->getExamListQuery($data['selectedExamType'], $data['selectedTechnology'], $data['selectedPackage'], 10);
        return view('view_exam', $data);
    }


    private function hasSelectedFilters(array $data): bool
    {
        return !empty($data['selectedExamType']) ||
            !empty($data['selectedTechnology']) ||
            !empty($data['selectedPackage']);
    }

    private function getTechnologies($trainerId)
    {
        return Employeeinfo::join('ev_technology', function ($join) {
            $join->on(DB::raw("FIND_IN_SET(ev_technology.id, employee_info.technology)"), '>', DB::raw('0'));
        })
            ->where('employee_info.user_id', $trainerId)
            ->where("ev_technology.status", 1)
            ->get(['ev_technology.*']);
    }

    public function examDeclaration()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 3) {
                $trainerId = Auth::user()->id;

                $data['technology'] = Employeeinfo::join('ev_technology', function ($join) {
                    $join->on(DB::raw("FIND_IN_SET(ev_technology.id, employee_info.technology)"), '>', DB::raw('0'));
                })

                    ->where('employee_info.user_id', $trainerId)
                    ->where("ev_technology.status", 1)
                    ->get(['ev_technology.*']);



                return view('exam_declaration', $data);
            } else {
                return redirect('/accessdenied');
            }
        } else {
            return redirect('/');
        }
    }


    public function examDeclarationSubmit(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        if (Auth::user()->role !== 3) {
            return redirect('/accessdenied');
        }

        $validator = $this->validateExamDeclaration($request);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if ($this->examExists($request->examtechnology, $request->examname)) {
            Session::flash('error', "The exam '{$request->examname}' for technology '{$request->examtechnology}' already exists.");
            return redirect()->back()->withInput();
        }

        $examId = $this->createExam($request->examtechnology, $request->examname,$request->percentages);
        $this->attachTechnologiesAndPackages($request, $examId);
        // $this->attachPercentages($request->percentages, $examId);

        Session::flash('success', ucfirst($request->examname) . " has been declared.");
        return redirect('/fetchExam');
    }


    /**
     * Validates the exam declaration form request.
     */
    protected function validateExamDeclaration($request)
    {
        return Validator::make($request->all(), [
            'examtechnology' => 'required|string',
            'examname' => 'required|string|max:255',
            'techtrainerselect' => 'required|array|min:1',
            'techtrainerselect.*' => 'integer|exists:ev_technology,id',
            'packages' => 'nullable|array',
            'packages.*' => 'integer|exists:ev_package,pac_id',
            'percentages' => 'nullable',
            'percentages.*' => 'in:25,50,75,100',
        ]);
    }

    /*
     * Checks if the exam already exists for a given technology and name.
     */
    protected function examExists($technology, $examName)
    {
        return DB::table('ev_exam')
            ->where('exam_type', $technology)
            ->where('exam_name', $examName)
            ->exists();
    }

    /**
     * Creates a new exam record and returns its ID.
     */
    protected function createExam($technology, $examName,$percentages)
    {
        return Exam::insertGetId([
            'exam_type' => $technology,
            'exam_name' => $examName,
            'percentage'=>$percentages,
            'trainer_id' => Auth::user()->id
        ]);
    }
    /**
     * Attaches the selected technologies and packages to the exam.
     */
    protected function attachTechnologiesAndPackages($request, $examId)
    {
        $insertedPackages = [];

        foreach ($request->techtrainerselect as $technologyId) {
            if ($technologyId) {
                $examtechnologyId = DB::table('ev_examtechnology')->insertGetId([
                    'exam_id' => $examId,
                    'technology_id' => $technologyId,
                ]);

                $this->attachPackages($request->packages, $examId, $examtechnologyId, $technologyId, $insertedPackages);
            }
        }
    }

    /**
     * Attaches the packages to the exam for the selected technology.
     */
    protected function attachPackages($selectedPackages, $examId, $examtechnologyId, $technologyId, &$insertedPackages)
    {

        $selectedPackages = is_array($selectedPackages) ? $selectedPackages : [];
        $packages = DB::table('ev_package')
            ->where('tech_id', $technologyId)
            ->pluck('pac_id');

        foreach ($packages as $packageId) {
            if (in_array($packageId, $selectedPackages)) {
                $key = "{$examId}-{$packageId}-{$examtechnologyId}";

                if (!isset($insertedPackages[$key])) {
                    DB::table('ev_exampackage')->insert([
                        'exam_id' => $examId,
                        'package_id' => $packageId,
                    ]);
                    $insertedPackages[$key] = true;
                }
            }
        }
    }

    /**
     * Attaches the percentages to the exam.
     */
    protected function attachPercentages($percentages, $examId)
    {
        if (is_array($percentages)) {
            foreach ($percentages as $percentage) {
                DB::table('ev_exampercentage')->insert([
                    'exam_id' => $examId,
                    'percentage' => $percentage,
                ]);
            }
        }
    }

    protected function getExamListQuery($examType = null, $technologyId = null, $packageId = null)
    {

        $query = DB::table('ev_exam')
            ->join('ev_examtechnology', 'ev_exam.id', '=', 'ev_examtechnology.exam_id')
            ->join('ev_technology', 'ev_examtechnology.technology_id', '=', 'ev_technology.id')
            ->join('ev_exampackage', 'ev_exampackage.exam_id', '=', 'ev_exam.id')
            ->join('ev_package', 'ev_exampackage.package_id', '=', 'ev_package.pac_id');


        if ($examType) {
            $query->where('ev_exam.exam_type', $examType);
        }

        if ($technologyId) {
            $query->where('ev_examtechnology.technology_id', $technologyId);
        }

        if ($packageId) {
            $query->where('ev_exampackage.package_id', $packageId);
        }
        $results = $query->select([
            'ev_exam.exam_type as examtype',
            'ev_exam.id as exam_id',
            'ev_exam.exam_name',
            'ev_technology.id as technology_id',
            'ev_technology.technology as technology_name',
            'ev_package.pac_name as package_name',
            'ev_exam.percentage',
            'ev_exam.status'
        ])
            ->orderBy('ev_exam.id', 'desc')
            ->get();

        $groupedResults = [];
        foreach ($results as $result) {
            $examId = $result->exam_id;
            if (!isset($groupedResults[$examId])) {
                $groupedResults[$examId] = [
                    'examtype' => $result->examtype,
                    'exam_id' => $examId,
                    'exam_name' => $result->exam_name,
                    'technologies' => [],
                    'packages' => [],
                    'percentage' => $result->percentage ?? 'N/A',
                    'status' => $result->status ?? 'N/A',
                ];
            }
            $groupedResults[$examId]['technologies'][] = $result->technology_name;
            $groupedResults[$examId]['packages'][] = $result->package_name;
        }


        $groupedResults = collect($groupedResults)->map(function ($exam) {
            return (object) [
                'examtype' => $exam['examtype'],
                'exam_id' => $exam['exam_id'],
                'exam_name' => $exam['exam_name'],
                'technologies' => implode(', ', array_unique($exam['technologies'])),
                'technologies' => implode(', ', array_unique($exam['technologies'])),
                'packages' => implode(', ', array_unique($exam['packages'])),
                'percentage' => $exam['percentage'],
                'status' => $exam['status']

            ];
        });

        $results = $groupedResults->values();

        return $results;
    }

    /////////////////

}
