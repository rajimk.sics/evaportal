<?php
namespace App\Http\Middleware\CheckStatus;
namespace App\Http\Controllers;
use App\Http\Controllers\StatusController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Models\Technology;
use App\Models\Designation;
use App\Models\Qualification;
use App\Models\Source;
use App\Models\College;
use App\Models\Tax;
use App\Models\Package;
use App\Models\User;
use App\Models\Specialization;
use App\Models\Employee;
use App\Models\Employeeinfo;
use App\Models\Employeepriv;
use App\Models\Email;
use Carbon\Carbon;
use App\Models\OldPayment;
use App\Helpers\CustomHelper;
use App\Models\Othereference;
use App\Models\Payment;
use App\Models\Salespayment;
use App\Models\Event;
use App\Models\Signature;
use App\Models\Salespackage;
use App\Models\PageContent;
use App\Models\Gatepass;
use App\Models\Talento;
use App\Models\Reporting;
use App\Models\Paymentrequest;
use App\Models\Studentfeessplit;
use App\Models\Studentpackage;
use App\Models\Company;
use App\Models\Oldstudent;
use App\Models\Prerequisite;
use App\Models\Topic;
use App\Models\Subtopic;
use Session;
use Auth;

class EVacommonController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
     protected $statusController;

    public function __construct()
    {
        $this->middleware("auth");
    }


    public function activateSingle($modelName, $id, $status)
    {
        $modelClass = "App\\Models\\" . ucfirst($modelName); 

        if (!class_exists($modelClass)) {
            return false; 
        }
        
      
        
       
        $updated = $modelClass::where('id', $id)->update(['status' => $status]);
        
        return $updated; 
    }
    



  
    
   
}
