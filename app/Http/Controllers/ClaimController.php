<?php

namespace App\Http\Middleware\CheckStatus;

namespace App\Http\Controllers;

use Auth;
use Session;
use Carbon\Carbon;
use Dompdf\Dompdf;
use App\Models\Poc;
use App\Models\Tax;
use Dompdf\Options;
use App\Models\User;
use App\Mail\PayMail;
use App\Models\State;
use App\Models\Source;
use App\Models\College;
use App\Models\Package;
use App\Models\Payment;
use App\Models\Talento;
use App\Mail\SimpleMail;
use App\Models\Claimfee;
use App\Models\Contacts;
use App\Models\Duration;
use App\Models\Employee;
use App\Models\Followup;
use App\Models\Gatepass;
use App\Models\Reporting;
use App\Models\Signature;

use App\Models\Collegedep;
use App\Models\OldPayment;
use App\Models\Oldstudent;
use App\Models\Technology;
use App\Models\Updatepocs;
use App\Models\Designation;
use App\Models\JobRegister;
use App\Models\Studentinfo;
use App\Models\Collegeclaim;
use App\Models\Employeeinfo;
use App\Models\Employeepriv;
use App\Models\Salespackage;
use App\Models\Salespayment;
use Illuminate\Http\Request;
use App\Helpers\CustomHelper;
use App\Models\Departmentpoc;
use App\Models\Masterrequest;
use App\Models\Othereference;
use App\Models\Qualification;
use App\Models\Collegeupdates;
use App\Models\Studentpackage;
use App\Models\Studentfeessplit;
use App\Models\Candidatefollowup;
use App\Models\Candidateinterview;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\CommonController;
use Illuminate\Contracts\Session\Session as SessionSession;
use App\Http\Controllers\EvacommonController; // Import the class

class ClaimController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $EvacommonController;

    public function __construct()
    {
        $this->middleware("auth");
        $this->EvacommonController = new EvacommonController();
    }

    public function claimdetailsAllocation(Request $request)
    {
        if (!Auth::check()) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $user = Auth::user();

        if (!in_array($user->role, [12])) {
            return response()->json(['error' => 'Access Denied'], 403);
        }



        $claimDetails = Claimfee::where('ev_claimfeedetails.claim_id', $request->claimid)
            ->join('users', 'users.id', '=', 'ev_claimfeedetails.student_id')
            ->orderBy('ev_claimfeedetails.id', 'desc')
            ->get([
                'users.name',
                'users.email',
                'users.phone',
                'ev_claimfeedetails.id',
                'ev_claimfeedetails.joining_date',
                'ev_claimfeedetails.package_fullamount'
            ]);

        if ($claimDetails->isEmpty()) {
            return response()->json(['error' => 'No claim details found'], 404);
        }


        $response = [

            'claim_details' => $claimDetails
        ];

        return response()->json($response);
    }

    public function claimMasterAllocate($collegeId)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        $user = Auth::user();


        if (!in_array($user->role, [12, 2])) {
            return redirect('/accessdenied');
        }


        $data = [
            'title' => 'Eligibility',

            'claimdetails' => Salespackage::selectRaw(
                'SUM(sales_package.package_fullamount) as total_package_fullamount,
                            studinfo.college_id, 
                            sales_package.sales_id,
                            ev_college.college as college_name,
                             GROUP_CONCAT(DISTINCT ev_package.pac_name ORDER BY ev_package.pac_name ASC) as package_names,
                           YEAR(sales_package.joining_date) as joiningyear'
            )
                ->join('ev_student_info as studinfo', 'studinfo.student_id', '=', 'sales_package.student_id')
                ->join('ev_college', 'ev_college.id', '=', 'studinfo.college_id')
                ->join('ev_package', 'ev_package.pac_id', '=', 'sales_package.package_id')
                ->where('studinfo.college_id', $collegeId)
                ->groupBy(
                    'studinfo.college_id',
                    'sales_package.sales_id',
                    'ev_college.college',
                    'joiningyear'
                )
                ->orderBy('joiningyear', 'desc')
                ->get(),

            'collegeId' => $collegeId

        ];

        return view('claim_allocationdetails', $data);
    }

    public function claimedCollegesmaster(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        $user = Auth::user();

        if (!in_array($user->role, [12])) {
            return redirect('/accessdenied');
        }



        $query = Claimfee::selectRaw('SUM(ev_claimfeedetails.package_amount) as total_package_amount, 
            SUM(ev_claimfeedetails.package_tax) as total_package_tax, 
            SUM(ev_claimfeedetails.package_fullamount) as total_package_fullamount,
            cc.college_id, 
            cc.dept_id, 
            cc.package_id,
            cc.status,
            ev_department.department,
            cc.id as claimid,
            ev_claimfeedetails.claim_status,
            ev_claimfeedetails.sales_id,
            users.name as sales_person,
            cc.current_datetime
        ')

            ->join('ev_collegeclaims as cc', 'cc.id', '=', 'ev_claimfeedetails.claim_id')
            ->join('ev_college', 'ev_college.id', '=', 'cc.college_id')
            ->join('ev_department', 'ev_department.id', '=', 'cc.dept_id')
            ->join('users', 'ev_claimfeedetails.sales_id', '=', 'users.id')
            ->where('cc.status', 0)
            ->groupBy('cc.college_id', 'cc.dept_id', 'cc.package_id', 'cc.id', 'cc.current_datetime', 'cc.status', 'ev_department.department', 'ev_claimfeedetails.sales_id', 'ev_claimfeedetails.claim_status', 'users.name')
            ->orderBy('cc.current_datetime', 'desc');



        $claimDetails = $query->get();

        return view('claimed_collegemaster', [
            'title' => 'Claimed Colleges',
            'claimdetails' => $claimDetails,

        ]);
    }

    public function claimstudentsList(Request $request, $claimId)
    {

        if (Auth::check()) {

            if (Auth::user()->role == 2) {

                $claimdetails = Collegeclaim::where('id', $claimId)->first();
                $package = Package::where('pac_id', $claimdetails->package_id)->first(['pac_type', 'pac_name']);
                $data['pac_type'] = $package->pac_type;



                $query  = Claimfee::join('ev_collegeclaims', 'ev_claimfeedetails.claim_id', '=', 'ev_collegeclaims.id')
                    ->join('users', 'users.id', '=', 'ev_claimfeedetails.student_id')
                    ->where('ev_collegeclaims.package_id', $claimdetails->package_id)
                    ->where('ev_collegeclaims.college_id', $claimdetails->college_id)
                    ->where('ev_collegeclaims.dept_id',  $claimdetails->dept_id)
                    ->where('ev_collegeclaims.package_type', 2)
                    ->where('ev_collegeclaims.sales_id', Auth::user()->id)
                    ->orderBy('ev_claimfeedetails.id', 'desc');

                $studentlist  = $query->get([
                    'users.name',
                    'users.email',
                    'users.phone',
                    'ev_claimfeedetails.id',
                    'ev_claimfeedetails.joining_date',
                    'ev_claimfeedetails.package_fullamount'
                ]);

                $data = [


                    'pac_type' => $package->pac_type,
                    'title' => 'Students of ' . $package->pac_name,
                    'studentlist' =>  $studentlist,
                    'claimid' => $claimId,

                ];



                return view("claimstudentlist", $data);
            } else {

                return redirect('/acessdenied');
            }
        } else {

            return redirect('/');
        }
    }



    public function claimNames(Request $request)
    {
        if (!Auth::check()) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        $currentyear = date('Y');
        $collegeName = CustomHelper::collegeName($request->input("colId"));
        $claimnames1 = Collegeclaim::join('users', 'ev_collegeclaims.sales_id', '=', 'users.id')
            ->where('ev_collegeclaims.id', $request->input("claimid"))
            ->distinct('ev_collegeclaims.sales_id')
            ->get(['ev_collegeclaims.id', 'users.id as userid', 'users.name']);


        $useridToExclude = $claimnames1->pluck('userid')->toArray();
        $claimnames2  =     Salespackage::join('users', 'sales_package.sales_id', '=', 'users.id')
            ->join('ev_student_info as studinfo', 'studinfo.sales_id', '=', 'sales_package.sales_id')
            ->whereNotIn('sales_package.sales_id', $useridToExclude)
            ->whereYear('sales_package.joining_date', '=', $currentyear)
            ->where('studinfo.college_id', $request->input("colId"))
            ->distinct()
            ->get(['users.id as userid', 'users.name as name']);
        $combinedResults = array_merge($claimnames1->toArray(), $claimnames2->toArray());;


        return response()->json([
            'collegeName' => $collegeName,
            'claimNames' => $combinedResults,
        ]);
    }


    public function masterStudentdetails(Request $request)
    {
        if (!Auth::check()) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $user = Auth::user();

        if (!in_array($user->role, [12])) {
            return response()->json(['error' => 'Access Denied'], 403);
        }


        $claimDetails =          Salespackage::join('users', 'users.id', '=', 'sales_package.student_id')
            ->join('ev_package', 'ev_package.pac_id', '=', 'sales_package.package_id')
            ->join('ev_technology', 'ev_package.tech_id', '=', 'ev_technology.id')
            ->join('ev_student_info', 'ev_student_info.student_id', '=', 'sales_package.student_id')
            ->where('ev_student_info.college_id', $request->collegeid)
            ->where(DB::raw('YEAR(sales_package.joining_date)'), $request->year)
            ->where('sales_package.sales_id', $request->salesid)
            ->orderBy('sales_package.joining_date', 'desc')
            ->get([
                'users.name',
                'users.email',
                'users.phone',
                'sales_package.package_fullamount',
                'sales_package.joining_date',
                'ev_package.pac_name',
                'ev_technology.technology'
            ]);

        if ($claimDetails->isEmpty()) {
            return response()->json(['error' => 'No claim details found'], 404);
        }


        $response = [

            'claim_details' => $claimDetails
        ];

        return response()->json($response);
    }

    public function getCollegeUpdateHistory(Request $request)
    {
        $college = DB::table('ev_college')->where('id', $request->collegeid)->first();
        if (!$college) {
            return response()->json(['error' => 'College not found'], 404);
        }
        $reportedIds = Reporting::join("users", "users.id", "=", "ev_reporting.employee_id")
            ->where('ev_reporting.reported_to', Auth::user()->id)
            ->pluck('users.id')
            ->toArray();


        array_push($reportedIds, Auth::user()->id);



        $collegeUpdates = DB::table('ev_collegeupdates')
            ->where('ev_collegeupdates.college_id', $request->collegeid)
            ->join('ev_department', 'ev_collegeupdates.department', '=', 'ev_department.id')
            ->leftjoin('ev_updatepocs', 'ev_collegeupdates.id', '=', 'ev_updatepocs.update_id')
            ->join('users', 'ev_collegeupdates.sales_id', '=', 'users.id')
            ->leftjoin('poc_department', 'ev_updatepocs.poc', '=', 'poc_department.id')
            ->leftJoin('ev_reporting', 'ev_reporting.employee_id', '=', 'ev_collegeupdates.sales_id')
            ->leftJoin('users as reporting_users', 'ev_reporting.reported_to', '=', 'reporting_users.id')
            ->select(
                'ev_collegeupdates.date',
                'ev_collegeupdates.end_date',
                'ev_collegeupdates.comment',
                'users.name as sales_person',
                'ev_department.department as department_name',
                DB::raw("GROUP_CONCAT(DISTINCT poc_department.department_poc ORDER BY poc_department.department_poc ASC SEPARATOR ', ') as poc_names"),
                DB::raw("CASE WHEN FIND_IN_SET(ev_collegeupdates.sales_id, '" . implode(',', $reportedIds) . "') THEN ev_collegeupdates.updatestatus ELSE NULL END as updatestatus"),
                DB::raw("CASE WHEN FIND_IN_SET(ev_collegeupdates.sales_id, '" . implode(',', $reportedIds) . "') THEN 
                GROUP_CONCAT(DISTINCT 
                    CASE 
                        WHEN reporting_users.id != " . Auth::user()->id . " 
                        THEN reporting_users.name 
                    END 
                    ORDER BY reporting_users.name ASC SEPARATOR ', ') 
                ELSE NULL END as reporting_persons"),
                DB::raw("CASE WHEN ev_collegeupdates.sales_id != " . Auth::user()->id . " THEN users.name ELSE NULL END as sales_id_person") // Include sales_person if not the logged-in user
            )

            ->groupBy(
                'ev_collegeupdates.date',
                'ev_collegeupdates.end_date',
                'ev_collegeupdates.comment',
                'users.name',
                'ev_department.department',
                'ev_collegeupdates.updatestatus',
                'ev_collegeupdates.sales_id',
                'ev_collegeupdates.id',
                //'reporting_users.name' 
            )
            ->orderBy('ev_collegeupdates.id', 'desc')
            ->get();

        return response()->json([
            'college' => $college->college,
            'updates' => $collegeUpdates,

        ]);
    }



    public function saveAppeal(Request $request)
    {

        if (!Auth::check()) return redirect('/');


        if (Auth::user()->role != 2) return redirect('/accessdenied');



        $result =     Masterrequest::where('id', $request->input("reqid"))->update([
            'appeal_status' => 1,
            'appeal_reason' => $request->get("inputValue"),

        ]);
    }


    public function listAppealfromsales(Request $request)
    {
        if (!Auth::check()) return redirect('/');


        if (Auth::user()->role != 12) return redirect('/accessdenied');


        $salesFilter = $request->query('salesperson');
        $salesperson = null;

        $query = Masterrequest::join("users", "ev_masterrequests.sales_id", "=", "users.id")
            ->join("ev_college", "ev_masterrequests.college_id", "=", "ev_college.id")
            ->leftJoin('ev_department', 'ev_department.id', '=', 'ev_masterrequests.department')
            ->leftJoin('poc_department', function ($join) {
                $join->on(DB::raw("FIND_IN_SET(poc_department.id, ev_masterrequests.poc)"), '>', DB::raw('0'));
            });
            // ->where('ev_masterrequests.appeal_status', '1');
        if (!empty($salesFilter)) {
            $query->where('ev_masterrequests.sales_id', $salesFilter);
            $salesperson = User::where('id', $salesFilter)->first(['name']);
        }
        $countQuery = clone $query;
        $query->orderBy("ev_masterrequests.id", "desc")
            ->groupBy(
                'ev_masterrequests.id',
                'ev_college.college',
                'users.name',
                'ev_masterrequests.from_date',
                'ev_masterrequests.to_date',
                'ev_masterrequests.department',
                'ev_masterrequests.poc',
                'ev_masterrequests.comments',
                'ev_masterrequests.college_id',
                'ev_masterrequests.sales_id',
                'ev_masterrequests.supporting_documents',
                'ev_masterrequests.status',
                'ev_masterrequests.master_comment',
                'ev_masterrequests.reason',
                'ev_masterrequests.appeal_reason',
                'ev_masterrequests.appeal_status',
                'ev_department.department'
            );


        $listings = $query->get([
            "ev_college.college as college",
            "users.name as name",
            'ev_masterrequests.id',
            'ev_masterrequests.department',
            "ev_masterrequests.from_date",
            "ev_masterrequests.to_date",
            'ev_masterrequests.comments',
            'ev_masterrequests.college_id',
            'ev_masterrequests.appeal_reason',
            'ev_masterrequests.appeal_status',
            'ev_masterrequests.supporting_documents',
            'ev_masterrequests.status',
            'ev_masterrequests.master_comment',
            'ev_masterrequests.reason',
            'ev_department.department as dept',
            DB::raw('GROUP_CONCAT(poc_department.department_poc) as department_pocs')
        ]);


        $count = $countQuery->distinct()->count('ev_masterrequests.id');
        $saleslists = User::where('role', 2)
            ->where('status', 1)
            ->get(['id', 'name']);

        $data = [
            'title' => "Appeal From Sales",
            'listings' => $listings,
            'saleslist' => $saleslists,
            'count' => $count,
            'salesName' => $salesperson,
        ];

        return view("appealfromsales", $data);
    }


    public function addClaimstudents($id)
    {



        $package = Package::where('status', 1)
            ->where('pac_type', 2)
            ->get(['pac_id', 'pac_name']);
        $college = College::leftJoin('ev_district', 'ev_district.id', '=', 'ev_college.district')
            ->where('ev_college.id', $id)
            ->first([
                'ev_district.name as district',
                'ev_college.college as colname',
                'ev_college.id as colId',
            ]);
        $deptIds1 = Collegeupdates::where('college_id', $id)
            ->where('sales_id', '!=', Auth::user()->id)
            ->where(function ($query) {
                $query->whereRaw("DATE_ADD(date, INTERVAL 1 MONTH) >= CURDATE()")
                    ->orWhere(function ($subQuery) {
                        $subQuery->where('end_date', '>', now())
                            ->whereNotNull('end_date');
                    });
            })
            ->pluck('department')
            ->toArray();
        $deptIds2 = Collegeupdates::where('college_id', $id)
            ->where('sales_id', '=', Auth::user()->id)
            ->where(function ($query) {
                $query->whereRaw("DATE_ADD(date, INTERVAL 1 MONTH) >= CURDATE()")
                    ->orWhere(function ($subQuery) {
                        $subQuery->where('end_date', '>', now())
                            ->whereNotNull('end_date');
                    });
            })
            ->pluck('department')
            ->toArray();
        $deptIds = array_diff($deptIds1, $deptIds2);


        $department = Collegedep::join('ev_department', 'ev_department.id', '=', 'ev_collegedep.department')
            ->where('ev_collegedep.college_id', $id)
            ->where('ev_collegedep.status', 1)
            ->whereIn('ev_collegedep.department', $deptIds)
            ->distinct()
            ->get(['ev_department.id', 'ev_department.department']);




        $data = [
            'college' => $college,
            'packages' => $package,
            'departments' => $department,
            'poc' => Departmentpoc::where('status', '1')->get()
        ];

        return view('add_claimstudents', $data);
    }



    public function getPackageDetails($packageId)
    {

        $package = Package::where('pac_id', $packageId)->first();


        return response()->json([
            'packagedetails' => $package
        ]);
    }



    public function claimdetailsMaster(Request $request, $id)
    {
        if (!Auth::check()) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $user = Auth::user();

        if (!in_array($user->role, [12])) {
            return response()->json(['error' => 'Access Denied'], 403);
        }

        // Retrieve the master request details
        $request_details = Masterrequest::find($id);
       
        
        if (!$request_details) {
            return response()->json(['error' => 'Request not found'], 404);
        }

        // Fetch the claim details along with college name and department
        $claimDetails = Salespackage::selectRaw(
            'SUM(sales_package.package_amount) as total_package_amount, 
         SUM(sales_package.package_tax) as total_package_tax, 
         SUM(sales_package.package_fullamount) as total_package_fullamount,
         cc.college_id, 
         cc.dept_id, 
         cc.sales_id,
         cc.status,
         ev_college.college as college_name,
         ev_department.department as department_name,
         ev_package.pac_name'
        )
            ->join('ev_collegeclaims as cc', 'cc.id', '=', 'sales_package.claim_id')
            ->join('ev_college', 'ev_college.id', '=', 'cc.college_id')
            ->join('ev_department', 'ev_department.id', '=', 'cc.dept_id')
            ->join('ev_package', 'ev_package.pac_id', '=', 'cc.package_id')
            ->where('cc.sales_id', $request_details->sales_id)
            ->where('cc.college_id', $request_details->college_id)
            ->where('cc.dept_id', $request_details->department)
            ->groupBy('cc.college_id', 'cc.dept_id', 'ev_package.pac_name', 'cc.sales_id', 'cc.status', 'ev_college.college', 'ev_department.department')
            ->orderBy('cc.current_datetime', 'desc')
            ->get();

        if ($claimDetails->isEmpty()) {
            return response()->json(['error' => 'No claim details found'], 404);
        }


        $response = [
            'college_name' => $claimDetails->first()->college_name,
            'department_name' => $claimDetails->first()->department_name,
            'claim_details' => $claimDetails
        ];

        return response()->json($response);
    }

    public function claimedCollegeSales(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        $user = Auth::user();

        if (!in_array($user->role, [2])) {
            return redirect('/accessdenied');
        }
        $departmentFilter = $request->query('deptname');

        $departments = DB::table('ev_department')->pluck('department', 'department');
        $query = Claimfee::selectRaw('SUM(ev_claimfeedetails.package_amount) as total_package_amount, 
            SUM(ev_claimfeedetails.package_tax) as total_package_tax, 
            SUM(ev_claimfeedetails.package_fullamount) as total_package_fullamount,
            cc.college_id, 
            cc.dept_id, 
            cc.sales_id,
            cc.package_id,
            cc.status,
            ev_department.department,
            ev_claimfeedetails.claim_id
        ')
            ->join('ev_collegeclaims as cc', 'cc.id', '=', 'ev_claimfeedetails.claim_id')
            ->join('ev_college', 'ev_college.id', '=', 'cc.college_id')
            ->join('ev_department', 'ev_department.id', '=', 'cc.dept_id')
            ->where('cc.sales_id', $user->id)
            ->groupBy('ev_claimfeedetails.claim_id', 'cc.college_id', 'cc.dept_id', 'cc.package_id', 'cc.sales_id', 'cc.status', 'ev_department.department')
            ->orderBy('cc.current_datetime', 'desc');
        if (!empty($departmentFilter)) {
            $query->where('ev_department.department', $departmentFilter);
        }


        $claimDetails = $query->get();


        return view('claimed_collegesales', [
            'title' => 'Claimed Colleges',
            'claimdetails' => $claimDetails,
            'departments' => $departments,
            'selectedDepartment' => $departmentFilter,
        ]);
    }




    public function getCollegeDepartments(Request $request)
    {


        $collegeId = $request->input('id');
        $isUpdate = $request->input('isUpdate');
        $depid = $request->input('depId');


        if ($isUpdate == 'true') {
            $response = $this->getCollegeDepartmentsForUpdate($collegeId, $depid);
        } else {
            $response = $this->getCollegeDepartmentsForNew($collegeId);
        }

        return response()->json($response);
    }

    private function getCollegeDepartmentsForUpdate($collegeId, $depid = null)
    {
        $college = $this->getCollegeDetails($collegeId);
        $isActive = [];
        if ($depid != null) {
            $isActive =  Collegedep::where('ev_collegedep.department', $depid)
                ->where('ev_collegedep.college_id', $collegeId)
                ->pluck('status');
        }
        $deptIds = Collegeupdates::where('college_id', $collegeId)
            ->where('sales_id', '!=', Auth::user()->id)
            ->where(function ($query) {
                $query->whereRaw("DATE_ADD(date, INTERVAL 1 MONTH) >= CURDATE()")
                    ->orWhere(function ($subQuery) {
                        $subQuery->where('end_date', '>', now())
                            ->whereNotNull('end_date');
                    });
            })
            ->pluck('department')
            ->toArray();
        $dd = $deptIds;
        $checkrequests = Collegeupdates::where('college_id', $collegeId)
            ->where('sales_id', '=', Auth::user()->id)
            ->where(function ($query) {
                $query->whereRaw("DATE_ADD(date, INTERVAL 1 MONTH) >= CURDATE()")
                    ->orWhere(function ($subQuery) {
                        $subQuery->where('end_date', '>', now())
                            ->whereNotNull('end_date');
                    });
            })
            ->pluck('department')
            ->toArray();
        if (!empty($checkrequests) && !empty($deptIds)) {

            $commonValues = array_intersect($checkrequests, $deptIds);
            if (!empty($commonValues)) {
                $deptIds = array_diff($deptIds, $checkrequests);
            }
        }

        $departments = Collegedep::join('ev_department', 'ev_department.id', '=', 'ev_collegedep.department')
            ->where('ev_collegedep.college_id', $collegeId)
            ->where('ev_collegedep.status', 1)
            ->whereNotIn('ev_collegedep.department', $deptIds)
            ->distinct()
            ->get(['ev_department.department', 'ev_department.id']);

        $selectedDep = $this->getSelectedDep($collegeId);
        $depPocs = $this->getDepPocDetails($collegeId);


        $salesPeople = Collegeupdates::where('ev_collegeupdates.college_id',  $collegeId)
            ->where('ev_collegeupdates.sales_id', '!=', Auth::user()->id)
            ->when($depid, function ($query, $depid) {
                return $query->where('ev_collegeupdates.department', $depid);
            })
            ->where(function ($query) {
                $query->whereRaw("DATE_ADD(date, INTERVAL 1 MONTH) >= CURDATE()")
                    ->orWhere(function ($subQuery) {
                        $subQuery->where('end_date', '>', now())
                            ->whereNotNull('end_date');
                    });
            })
            ->join('users', 'ev_collegeupdates.sales_id', '=', 'users.id')
            ->select('users.name')
            ->distinct()
            ->get();
        return [
            'departments' => $departments,
            'pocs' => Departmentpoc::where('status', 1)->get(),
            'dep_pocs' => $depPocs,
            'salesPeople' => $salesPeople,
            'selectedDep' => $selectedDep,
            'college' => $college,
            'check' =>  $checkrequests,
            'depId' => $deptIds,
            'dd' => $dd,
            'isActive' =>  $isActive
        ];
    }

    private function getCollegeDepartmentsForNew($collegeId)
    {
        $college = $this->getCollegeDetails($collegeId);

        $departments = Collegedep::join('ev_department', 'ev_department.id', '=', 'ev_collegedep.department')
            ->where('ev_collegedep.college_id', $collegeId)
            ->where('ev_collegedep.status', 1)
            ->get(['ev_department.department', 'ev_department.id']);

        $selectedDep = $this->getSelectedDep($collegeId);
        $depPocs = $this->getDepPocDetails($collegeId);

        return [
            'departments' => $departments,
            'pocs' => Departmentpoc::where('status', 1)->get(),
            'selectedDep' => $selectedDep,
            'dep_pocs' => $depPocs,
            'college' => $college,
        ];
    }
    public function getCollegeDepartmentsForReqForm(Request $request)
    {
        $collegeId = $request->input('id');
        $college = $this->getCollegeDetails($collegeId);

      
        $departmentsQuery = Collegedep::join('ev_collegeupdates', 'ev_collegedep.college_id', '=', 'ev_collegeupdates.college_id')
        ->join('ev_department', 'ev_department.id', '=', 'ev_collegeupdates.department')
        ->where('ev_collegedep.college_id', $collegeId)
        ->where('ev_collegedep.status', 1)
        ->distinct()
        ->select(['ev_department.department', 'ev_department.id']);
      $departments = $departmentsQuery->get();
        



        $deptIds = Collegeupdates::where('college_id', $collegeId)
            ->where('sales_id', '=', Auth::user()->id)
            ->where(function ($query) {
                $query->whereRaw("DATE_ADD(date, INTERVAL 1 MONTH) >= CURDATE()")
                    ->orWhere(function ($subQuery) {
                        $subQuery->where('end_date', '>', now())
                            ->whereNotNull('end_date');
                    });
            })
            ->pluck('department')
            ->toArray();

        $filteredDepartments = $departments->reject(function ($department) use ($deptIds) {
            return in_array($department->id, $deptIds);
        });

        $selectedDep = $this->getSelectedDep($collegeId);
        $depPocs = $this->getDepPocDetails($collegeId);

        $back =  [
            'departments' => $filteredDepartments->values(),
            'pocs' => Departmentpoc::where('status', 1)->get(),
            'selectedDep' => $selectedDep,
            'dep_pocs' => $depPocs,
            'college' => $college,
        ];
        return response()->json($back);
    }

    private function getCollegeDetails($collegeId)
    {
        return College::leftjoin('ev_district', 'ev_college.district', '=', 'ev_district.id')
            ->where('ev_college.id', $collegeId)
            ->first([
                DB::raw("UPPER(ev_college.college) as colname"),
                DB::raw("UPPER(ev_district.name) as district_name"),
            ]);
    }
    private function getSelectedDep($collegeId)
    {
        return  Collegeupdates::where('college_id', $collegeId)
            ->where('sales_id', '=', Auth::user()->id)
            ->distinct()
            ->pluck('department');
    }
    private function getDepPocDetails($collegeId)
    {
        return  Collegeupdates::where('college_id', $collegeId)
            ->where('sales_id', '=', Auth::user()->id)
            ->join('ev_updatepocs', 'ev_collegeupdates.id', '=', 'ev_updatepocs.update_id')
            ->join('poc_department', 'ev_updatepocs.poc', '=', 'poc_department.id')
            ->distinct()
            ->select('ev_collegeupdates.department', 'poc_department.id as poc_id', 'poc_department.department_poc')
            ->groupBy('poc_department.id', 'ev_collegeupdates.department', 'poc_department.department_poc')
            ->get();
    }

    public function getrequestDetails(Request $request)
    {

        $college = College::leftjoin('ev_district', 'ev_college.district', '=', 'ev_district.id')
            ->orderBy('ev_college.id', 'desc')
            ->get([
                'ev_college.id',
                DB::raw("UPPER(ev_college.college) as colname"),
                DB::raw("UPPER(ev_district.name) as district_name")
            ]);

        return response()->json([

            'college' => $college
        ]);
    }


    public function collegeUpdateReport(Request $request)
    {
        if (!Auth::check() || !in_array(Auth::user()->role, [2])) {
            return redirect('/')->with('error', 'Access Denied');
        }

        $user_id = Auth::user()->id;
        $year = $request->query('year');
        $month = $request->query('month');
        $report_of = $request->query('reported_to') != '' ? $request->query('reported_to') : [];
        $start_date = $request->query('start_collegeupdate');
        $end_date = $request->query('end_collegeupdate');
        $startOfMonth = $endOfMonth = null;

        if (!empty($report_of)) {
            $user_id = $report_of;
        }


        $reportedlist = Reporting::join("users", "users.id", "=", "ev_reporting.employee_id")
            ->where('ev_reporting.reported_to', Auth::user()->id)->get(['users.id', 'users.name']);

        $query = Collegeupdates::join('ev_department', 'ev_department.id', '=', 'ev_collegeupdates.department')
            ->join('ev_college', 'ev_college.id', '=', 'ev_collegeupdates.college_id')
            ->leftJoin('ev_updatepocs', 'ev_collegeupdates.id', '=', 'ev_updatepocs.update_id')
            ->leftJoin('poc_department', 'ev_updatepocs.poc', '=', 'poc_department.id')
            ->select(
                'ev_department.department as dept',
                'ev_collegeupdates.id',
                'ev_college.college',
                'ev_collegeupdates.college_id',
                'ev_collegeupdates.department',
                'ev_collegeupdates.date',
                'ev_collegeupdates.comment',
                DB::raw("GROUP_CONCAT(poc_department.id ORDER BY poc_department.id ASC SEPARATOR ', ') as poc_ids"),
                DB::raw('GROUP_CONCAT(poc_department.department_poc ORDER BY poc_department.department_poc ASC) as pocs')
            )

            ->whereRaw('ev_collegeupdates.date = (
            SELECT MAX(subquery.date)
            FROM ev_collegeupdates AS subquery
            WHERE subquery.college_id = ev_collegeupdates.college_id
            AND subquery.department = ev_collegeupdates.department
            AND subquery.sales_id = ev_collegeupdates.sales_id
        )')
            ->groupBy(
                'ev_department.department',
                'ev_collegeupdates.id',
                'ev_college.college',
                'ev_collegeupdates.college_id',
                'ev_collegeupdates.department',
                'ev_collegeupdates.date',
                'ev_collegeupdates.comment'
            )
            ->where(function ($query) use ($user_id) {
                if (is_array($user_id)) {
                    $query->whereIn('ev_collegeupdates.sales_id', $user_id);
                } else {
                    $query->where('ev_collegeupdates.sales_id', $user_id);
                }
            })
            ->orderBy('ev_collegeupdates.date', 'desc');
        if (!empty($year) && !empty($month)) {

            $startOfMonth = Carbon::create($year, $month, 1)->startOfMonth()->format('Y-m-d');
            $endOfMonth = Carbon::create($year, $month, 1)->endOfMonth()->format('Y-m-d');
        } elseif (!empty($year)) {

            $startOfMonth = Carbon::create($year, 1, 1)->startOfYear()->format('Y-m-d');
            $endOfMonth = Carbon::create($year, 12, 31)->endOfYear()->format('Y-m-d');
        }
        if (!empty($month) && empty($year)) {

            $query->whereMonth('ev_collegeupdates.date', '=', $month);
        } elseif ($startOfMonth && $endOfMonth) {

            $query->whereBetween('ev_collegeupdates.date', [$startOfMonth, $endOfMonth]);
        }


        if (!empty($start_date) && !empty($end_date)) {
            $start_dates  = Carbon::createFromFormat('d-m-Y', $start_date)->format('Y-m-d');
            $end_dates  = Carbon::createFromFormat('d-m-Y', $end_date)->format('Y-m-d');
            $query->whereBetween('ev_collegeupdates.date', [$start_dates, $end_dates]);
        }


        $collegeUpdates = $query->get();
        //     echo '<pre>';
        //     print_r($collegeUpdates);
        //     echo '<pre>';
        // die();
        return view('collegeupdatereport', [
            'years' => CommonController::generateYearRange(date('Y') - 10),
            'months' => CommonController::generateMonthOptions(),
            'currentYear' => $year,
            'currentMonth' => $month,
            'start_date' => $request->input('start_collegeupdate', ''),
            'end_date' => $request->input('end_collegeupdate', ''),
            'reported_to' => $report_of,
            'title' => 'Colleges Update Report',
            'report' => $collegeUpdates,
            'reportedlist' => $reportedlist
        ]);
    }
    public function updateDetails($Id)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        if (!in_array(Auth::user()->role, [12, 2])) {
            return redirect('/accessdenied');
        }

        $updatedetails = Collegeupdates::where('id', $Id)->first();
        $collegeUpdates = Collegeupdates::join('ev_department', 'ev_department.id', '=', 'ev_collegeupdates.department')
            ->join('ev_college', 'ev_college.id', '=', 'ev_collegeupdates.college_id')
            ->leftjoin('ev_updatepocs', 'ev_collegeupdates.id', '=', 'ev_updatepocs.update_id')
            ->leftjoin('poc_department', 'ev_updatepocs.poc', '=', 'poc_department.id')
            ->where('ev_collegeupdates.sales_id', $updatedetails->sales_id)
            ->where('ev_collegeupdates.college_id', $updatedetails->college_id)
            ->where('ev_collegeupdates.department', $updatedetails->department)
            ->select([
                'ev_collegeupdates.id',
                'ev_department.department',
                'ev_college.college',
                DB::raw("DATE_FORMAT(ev_collegeupdates.end_date, '%d-%m-%Y') as end_date"),
                DB::raw("DATE_FORMAT(ev_collegeupdates.date, '%d-%m-%Y') as date"),
                DB::raw("GROUP_CONCAT(poc_department.department_poc ORDER BY poc_department.department_poc ASC SEPARATOR ', ') as poc_names"),
            ])
            ->groupBy('ev_collegeupdates.id', 'ev_collegeupdates.end_date', 'ev_department.department', 'ev_college.college', 'ev_collegeupdates.date')
            ->orderBy('ev_collegeupdates.date', 'desc')
            ->get();

        return response()->json([
            'collegeupdates' => $collegeUpdates
        ]);
    }
    public function claimDetails($collegeId, $packageId)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        $user = Auth::user();


        if (!in_array($user->role, [12, 2])) {
            return redirect('/accessdenied');
        }
        $data = [
            'title' => 'Eligibility',
            'claimdetails' => Salespackage::selectRaw('
                                     sales_package.claim_id, 
                                     SUM(sales_package.package_amount) as total_package_amount, 
                                     SUM(sales_package.package_tax) as total_package_tax, 
                                     SUM(sales_package.package_fullamount) as total_package_fullamount,
                                     ev_collegeclaims.college_id, 
                                        ev_collegeclaims.depoc_id, 
                                        ev_collegeclaims.sales_id,
                                        ev_collegeclaims.package_id,
                                        ev_collegeclaims.current_datetime, 
                                        ev_collegeclaims.status         
                                    ')
                ->join('ev_collegeclaims', 'ev_collegeclaims.id', '=', 'sales_package.claim_id')
                ->where('ev_collegeclaims.college_id', $collegeId)
                ->where('ev_collegeclaims.package_id', $packageId)
                ->groupBy(
                    'sales_package.claim_id',
                    'ev_collegeclaims.college_id',
                    'ev_collegeclaims.dept_id',
                    'ev_collegeclaims.sales_id',
                    'ev_collegeclaims.package_id',
                    'ev_collegeclaims.current_datetime',
                    'ev_collegeclaims.status'
                )
                ->orderBy('ev_collegeclaims.current_datetime', 'desc')
                ->get(),
            'collegeId' => $collegeId,
            'packageId' => $packageId

        ];

        return view('claim_details', $data);
    }

public function saveAllocation(Request $request)
{
    if (!Auth::check()) {
        return redirect('/');
    }

    $user = Auth::user();

    if (!in_array($user->role, [12, 2])) {
        return redirect('/accessdenied');
    }

    $salespersons = $request->input('salesperson', []);
    $percentages = $request->input('perallocate', []);
    $claimId = $request->input('claimId');
    $students = Claimfee::where('claim_id', $claimId)->get();

    foreach ($students as $student) {

        $student_id = $student->student_id;
        $package_fullamount = $student->package_fullamount;
        $package_amount = $student->package_amount;
        $package_tax = $student->package_tax;
        $package_id = $student->package_id;
        $joining_date = $student->joining_date;

        foreach ($salespersons as $index => $salesperson) {
            $percentage = $percentages[$index] ?? 0;

            $allocated_fullamount = ($package_fullamount * $percentage) / 100;
            $allocated_amount = ($package_amount * $percentage) / 100;
            $allocated_tax = ($package_tax * $percentage) / 100;

          
                Salespackage::insert([
                    'sales_id' => $salesperson,
                    'package_type' => 2,
                    'student_id' => $student_id,
                    'package_fullamount' => $allocated_fullamount,
                    'package_amount' => $allocated_amount,
                    'package_tax' => $allocated_tax,
                    'package_id' => $package_id,
                    'claim_status' => 1,
                    'joining_date' => $joining_date,
                ]);

                
                Session::flash('success', 'Allocation was successful');
            
        }
    }
    Claimfee::where('claim_id', $claimId)->update(['claim_status' => 1]);

    return redirect()->back();
}







    public function claimsCollegeSales(Request $request)
    {

        if (!Auth::check()) {
            return redirect('/');
        }
        $user = Auth::user();
        if (!in_array($user->role, [2])) {
            return redirect('/accessdenied');
        }
        $data = [
            'title' => 'Claim Colleges',
            'collegeupdate'     => Collegeupdates::join('ev_department', 'ev_department.id', '=', 'ev_collegeupdates.department')
                ->join('ev_college', 'ev_college.id', '=', 'ev_collegeupdates.college_id')
                ->leftjoin('ev_updatepocs', 'ev_collegeupdates.id', '=', 'ev_updatepocs.update_id')
                ->leftjoin('poc_department', 'ev_updatepocs.poc', '=', 'poc_department.id')
                ->where('sales_id', Auth::user()->id)
                ->where(function ($query) {
                    $query->whereRaw("DATE_ADD(date, INTERVAL 1 MONTH) >= CURDATE()")
                        ->orWhere(function ($subQuery) {
                            $subQuery->where('end_date', '>', now())
                                ->whereNotNull('end_date');
                        });
                })
                ->groupBy('ev_department.department', 'ev_collegeupdates.id', 'ev_college.college') // Group by dept, college update id, and college name
                ->orderBy('ev_collegeupdates.id', 'desc')

                ->get([
                    'ev_department.department as dept',
                    'ev_collegeupdates.id',
                    'ev_college.college',

                    DB::raw("GROUP_CONCAT(poc_department.department_poc ORDER BY poc_department.department_poc ASC SEPARATOR ', ') as poc_names")
                ])
        ];


        return view('claim_details', $data);
    }



    public function updateColleges(Request $request)
    {
        if (!Auth::check()) return redirect('/');


        $user = Auth::user();
        if ($user->role != 2) return redirect('/accessdenied');

        $collegeId = $request->input('collegeid');
        $departmentId = $request->input('collegedep');
        $salesId = $user->id;


        $recentUpdate = Collegeupdates::where('college_id', $collegeId)
            ->where('department', $departmentId)
            ->where('sales_id', $salesId)
            ->orderBy('date', 'desc')
            ->first();

        if (empty($recentUpdate) || now()->subMonths(3)->lte($recentUpdate->date)) {

            $updateid = Collegeupdates::insertGetId([
                'college_id' => $collegeId,
                'sales_id' => $salesId,
                'department' => $departmentId,
                'comment' => $request->input('comments'),
                'updatestatus' => $request->input('updatestatus'),
                'date' => now(),
            ]);


            $pocs = $request->input("collegepoc");

            if (!empty($pocs)) {
                $data = array_map(fn($pocs) => [
                    'update_id' =>  $updateid,
                    'poc' => $pocs
                ], $pocs);

                Updatepocs::insert($data);
            }
            Session::flash("success", "Successfully Updated");
        } else {

            $recentClaimExists = Collegeclaim::where('college_id', $collegeId)
                ->where('depoc_id', $departmentId)
                ->where('sales_id', $salesId)
                ->where('current_datetime', '>=', now()->subMonths(3))
                ->exists();

            if ($recentClaimExists) {

                $updateid = Collegeupdates::insertGetId([
                    'college_id' => $collegeId,
                    'sales_id' => $salesId,
                    'department' => $departmentId,
                    'comment' => $request->input('comments'),
                    'updatestatus' => $request->input('updatestatus'),
                    'date' => now(),
                ]);

                $pocs = $request->input("collegepoc");

                if (!empty($pocs)) {
                    $data = array_map(fn($pocs) => [
                        'update_id' =>  $updateid,
                        'poc' => $pocs
                    ], $pocs);

                    Updatepocs::insert($data);
                }
                Session::flash("success", "Successfully Updated");
            } else {

                Session::flash('errormessage', 'WARNING: Sorry, you cannot submit this update as no closing exists in the past 3 months. Please request an extension.');
            }
        }

        return $request->input('updatetype') == 'true' ? redirect('/manage_college') : redirect('/college_updateReport');
    }
    public function saveMasterRequest(Request $request)
    {

        if (Auth::check()) {
            if (Auth::user()->role == 2) {

                $validator = Validator::make($request->all(), [
                    'from_date' => 'required|date|before_or_equal:to_date',
                    'to_date' => 'required|date|after_or_equal:from_date',
                    'comment' => 'nullable|string|max:255',
                    'collegeId' => 'required|integer',
                    'department' => 'required',
                    'pocId' => 'required',
                    'documents.*' => 'nullable|file|mimes:pdf,jpg,jpeg,png|max:2048',
                ]);

                $documentNames = [];


                if ($validator->fails()) {
                    return response()->json(['errors' => $validator->errors()], 422);
                }

                $data['documents'] = [];

                if ($request->hasFile('documents')) {
                    foreach ($request->file('documents') as $file) {
                        $originalName = $file->getClientOriginalName();
                        $file->move(public_path('uploads/masterdocuments'), $originalName);
                        $documentNames[] = $originalName;
                    }
                } else {
                    $data['documents'] = '';
                }


                $documents = implode(',', $documentNames);

                Masterrequest::insert([
                    'from_date' => date("Y-m-d", strtotime($request->from_date)),
                    'to_date' => date("Y-m-d", strtotime($request->to_date)),
                    'department' => $request->department,
                    'poc' => implode(',', $request->input('pocId')),
                    'comments' => $request->comment,
                    'college_id' => $request->collegeId,
                    'sales_id' => Auth::user()->id,
                    'supporting_documents' => $documents,
                    'created_at' => now(),
                ]);
                Session::flash("success", "Request sent successfully");
            }
            return redirect("/request_tomaster");
        } else {
            return redirect("/accessdenied");
        }
    }

    public function requestTomaster(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 2) {

                $listings = Masterrequest::join("ev_college", "ev_masterrequests.college_id", "=", "ev_college.id")
                    ->leftJoin('ev_department', 'ev_department.id', '=', 'ev_masterrequests.department')
                    ->leftJoin('poc_department', function ($join) {
                        $join->on(DB::raw("FIND_IN_SET(poc_department.id, ev_masterrequests.poc)"), '>', DB::raw('0'));
                    })
                    ->where('ev_masterrequests.sales_id', Auth::user()->id)
                    ->when(request()->has('status'), function ($query) {
                        return $query->where('ev_masterrequests.status', request('status'));
                    })
                    ->orderBy("ev_masterrequests.id", "desc")
                    ->groupBy(
                        'ev_masterrequests.id',
                        'ev_college.college',
                        'ev_masterrequests.from_date',
                        'ev_masterrequests.to_date',
                        'ev_masterrequests.department',
                        'ev_masterrequests.poc',
                        'ev_masterrequests.comments',
                        'ev_masterrequests.master_comment',
                        'ev_masterrequests.reason',
                        'ev_masterrequests.college_id',
                        'ev_masterrequests.sales_id',
                        'ev_masterrequests.supporting_documents',
                        'ev_masterrequests.status',
                        'ev_department.department',
                        'ev_masterrequests.appeal_status',
                        'ev_masterrequests.appeal_reason'
                    )
                    ->get([
                        "ev_college.college as college",
                        'ev_masterrequests.id',
                        'ev_masterrequests.department',
                        "ev_masterrequests.from_date",
                        "ev_masterrequests.to_date",
                        'ev_masterrequests.comments',
                        'ev_masterrequests.master_comment',
                        'ev_masterrequests.appeal_status',
                        'ev_masterrequests.appeal_reason',
                        'ev_masterrequests.reason',
                        'ev_masterrequests.college_id',
                        'ev_masterrequests.supporting_documents',
                        'ev_masterrequests.status',
                        'ev_department.department as dept',
                        DB::raw('GROUP_CONCAT(poc_department.department_poc) as department_pocs')
                    ]);
                $data = [
                    'title' => "Request To Master",
                    'listings' => $listings
                ];

                return view("request_tomaster", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }



    public function listRequestfromsales(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 12) {

                $year = $request->query('year');
                $month = $request->query('month');
                $start_date = $request->query('start_collegeupdate');
                $end_date = $request->query('end_collegeupdate');
                $startOfMonth = $endOfMonth = null;
                $query = Masterrequest::join("users", "ev_masterrequests.sales_id", "=", "users.id")
                    ->join("ev_college", "ev_masterrequests.college_id", "=", "ev_college.id")
                    ->leftJoin('ev_department', 'ev_department.id', '=', 'ev_masterrequests.department')
                    ->leftJoin('poc_department', function ($join) {
                        $join->on(DB::raw("FIND_IN_SET(poc_department.id, ev_masterrequests.poc)"), '>', DB::raw('0'));
                    })
                    ->orderBy("ev_masterrequests.id", "desc")
                    ->groupBy(
                        'ev_masterrequests.id',
                        'ev_college.college',
                        'users.name',
                        'ev_masterrequests.from_date',
                        'ev_masterrequests.to_date',
                        'ev_masterrequests.department',
                        'ev_masterrequests.poc',
                        'ev_masterrequests.comments',
                        'ev_masterrequests.college_id',
                        'ev_masterrequests.sales_id',
                        'ev_masterrequests.supporting_documents',
                        'ev_masterrequests.status',
                        'ev_masterrequests.master_comment',
                        'ev_masterrequests.reason',
                        'ev_department.department'
                    );

                if (!empty($year) && !empty($month)) {

                    $startOfMonth = Carbon::create($year, $month, 1)->startOfMonth()->format('Y-m-d');
                    $endOfMonth = Carbon::create($year, $month, 1)->endOfMonth()->format('Y-m-d');
                } elseif (!empty($year)) {

                    $startOfMonth = Carbon::create($year, 1, 1)->startOfYear()->format('Y-m-d');
                    $endOfMonth = Carbon::create($year, 12, 31)->endOfYear()->format('Y-m-d');
                }
                if (!empty($month) && empty($year)) {

                    $query->whereMonth('ev_masterrequests.from_date', '=', $month);
                } elseif ($startOfMonth && $endOfMonth) {

                    $query->whereBetween('ev_masterrequests.from_date', [$startOfMonth, $endOfMonth]);
                }


                if (!empty($start_date) && !empty($end_date)) {
                    $start_dates  = Carbon::createFromFormat('d-m-Y', $start_date)->format('Y-m-d');
                    $end_dates  = Carbon::createFromFormat('d-m-Y', $end_date)->format('Y-m-d');
                    $query->whereBetween('ev_masterrequests.from_date', [$start_dates, $end_dates]);
                }
                $listings = $query->get([
                    "ev_college.college as college",
                    "users.name as name",
                    'ev_masterrequests.id',
                    'ev_masterrequests.department',
                    "ev_masterrequests.from_date",
                    "ev_masterrequests.to_date",
                    'ev_masterrequests.comments',
                    'ev_masterrequests.college_id',
                    'ev_masterrequests.supporting_documents',
                    'ev_masterrequests.status',
                    'ev_masterrequests.master_comment',
                    'ev_masterrequests.reason',
                    'ev_department.department as dept',
                    DB::raw('GROUP_CONCAT(poc_department.department_poc) as department_pocs')
                ]);

                $data = [

                    'years' => CommonController::generateYearRange(date('Y') - 10),
                    'months' => CommonController::generateMonthOptions(),
                    'title' => "Request From Sales",
                    'listings' => $listings
                ];

                return view("requestfromsales", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }


    public function getUpdateDetails(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 12) {

                $request_details = Masterrequest::where('id', $request->reqid)->first();

                $updateDetails = Collegeupdates::join('ev_department', 'ev_collegeupdates.department', '=', 'ev_department.id')
                    ->leftjoin('ev_updatepocs', 'ev_collegeupdates.id', '=', 'ev_updatepocs.update_id')
                    ->leftjoin('poc_department', 'ev_updatepocs.poc', '=', 'poc_department.id')
                    ->join('ev_college', 'ev_collegeupdates.college_id', '=', 'ev_college.id')
                    ->where('ev_collegeupdates.sales_id', $request_details->sales_id)
                    ->where('ev_collegeupdates.college_id', $request_details->college_id)
                    ->where('ev_collegeupdates.department', $request_details->department)
                    ->groupBy('ev_department.department', 'ev_collegeupdates.id', 'ev_college.college', 'ev_collegeupdates.date')
                    ->get([
                        'ev_collegeupdates.id',
                        'ev_collegeupdates.date',
                        'ev_department.department as department_name',
                        'ev_college.college as college',
                        DB::raw("GROUP_CONCAT(poc_department.department_poc ORDER BY poc_department.department_poc ASC SEPARATOR ', ') as poc_names")
                    ]);
                if ($updateDetails->isNotEmpty()) {
                    return response()->json($updateDetails);
                } else {
                    return response()->json(['error' => 'No data found'], 404);
                }
            } else {
                return response()->json(['error' => 'Access denied'], 403);
            }
        } else {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }
    public function requestDetails(Request $request)
    {
        $requestdetails = Masterrequest::where('id', $request->id)->first();
        if (!empty($requestdetails)) {
            return response()->json($requestdetails);
        } else {
            return response()->json(['error' => 'No data found'], 404);
        }
    }

    public function approveRequest(Request $request)
    {

        if (!Auth::check()) return redirect('/');


        if (Auth::user()->role != 12) return redirect('/accessdenied');



        $masterRequest = Masterrequest::where('id', $request->input("reqid"))->first();

        Masterrequest::where('id', $request->input("reqid"))->update([
            'status' => 1,
            'master_comment' => $request->get("mcomments"),
            'from_date' => date("Y-m-d", strtotime($request->editfromdate)),
            'to_date' => date("Y-m-d", strtotime($request->edittodate)),
        ]);
        $masterRequest = Masterrequest::where('id', $request->input("reqid"))->first();
        $updateid = Collegeupdates::insertGetId([
            'college_id' => $masterRequest->college_id,
            'sales_id' => $masterRequest->sales_id,
            'department' => $masterRequest->department,
            'date' => $masterRequest->from_date,
            'end_date' => $masterRequest->to_date,
            'comment' => $masterRequest->comments,
            'created_at' => now(),
            'updated_at' => now(),
        ]);


        $pocIds = explode(',', $masterRequest->poc);

        foreach ($pocIds as $pocId) {
            Updatepocs::insert([
                'update_id' => $updateid,
                'poc' => trim($pocId),

            ]);
        }
        Session::flash("success", "Request approved successfully");
        return redirect('/list_requestfromsales');
    }
    public function approveRequestAppeal(Request $request)
    {

        if (!Auth::check()) return redirect('/');


        if (Auth::user()->role != 12) return redirect('/accessdenied');



        $masterRequest = Masterrequest::where('id', $request->input("reqid"))->first();

        Masterrequest::where('id', $request->input("reqid"))->update([
            'appeal_status'=>3,
            'status' => 1,
            'master_comment' => $request->get("mcomments"),
            'from_date' => date("Y-m-d", strtotime($request->editfromdate)),
            'to_date' => date("Y-m-d", strtotime($request->edittodate)),
        ]);
        $masterRequest = Masterrequest::where('id', $request->input("reqid"))->first();
        $updateid = Collegeupdates::insertGetId([
            'college_id' => $masterRequest->college_id,
            'sales_id' => $masterRequest->sales_id,
            'department' => $masterRequest->department,
            'date' => $masterRequest->from_date,
            'end_date' => $masterRequest->to_date,
            'comment' => $masterRequest->comments,
            'created_at' => now(),
            'updated_at' => now(),
        ]);


        $pocIds = explode(',', $masterRequest->poc);

        foreach ($pocIds as $pocId) {
            Updatepocs::insert([
                'update_id' => $updateid,
                'poc' => trim($pocId),

            ]);
        }
        Session::flash("success", "Request approved successfully");
        return redirect('/list_appealfromsales');
    }



    public function rejectRequest(Request $request)
    {

        if (!Auth::check()) return redirect('/');


        if (Auth::user()->role != 12) return redirect('/accessdenied');



        $result =     Masterrequest::where('id', $request->input("reqid"))->update([
            'status' => 2,
            'reason' => $request->get("inputValue"),

        ]);
    }
    public function rejectRequestAppeal(Request $request)
    {

        if (!Auth::check()) return redirect('/');


        if (Auth::user()->role != 12) return redirect('/accessdenied');



        $result =     Masterrequest::where('id', $request->input("reqid"))->update([
            'appeal_status' => 2,
            'reason' => $request->get("inputValue"),

        ]);
    }
}
