<?php
namespace App\Http\Middleware\CheckStatus;
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Technology;
use App\Models\Designation;
use App\Models\Qualification;
use App\Models\Source;
use App\Models\College;
use App\Models\User;
use App\Models\Contacts;
use App\Models\Followup;
use App\Models\Package;
use App\Models\Studentpackage;
use App\Models\Studentfeessplit;
use App\Models\Payment;
use App\Models\Tax;
use App\Models\Salespayment;
use App\Models\Salespackage;
use App\Models\Studentinfo;
use App\Models\Othereference;
use Illuminate\Support\Facades\View;
use Dompdf\Dompdf;
use Dompdf\Options;
use Illuminate\Support\Facades\Storage;
use App\Models\Employee;
use App\Models\Employeeinfo;
use App\Models\Employeepriv;
use Carbon\Carbon;
use App\Mail\SimpleMail;
use App\Mail\PayMail;
use App\Models\OldPayment;
use App\Models\Collegeupdates;
use App\Http\Controllers\CommonController; // Import the class

use App\Models\Signature;
use Illuminate\Support\Facades\Mail;
use App\Models\Talento;
use App\Models\Gatepass;
use App\Models\Oldstudent;
use App\Models\Reporting;
use Session;
use Auth;
class SalesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function add_students_bk($pacid)
    {
        $data['packagedetails'] = Package::where('pac_id', $pacid)->first();

        $data['title'] = 'Add New Students';
        $data['pacid'] = $pacid;

        $data['collegelist']    = Collegeupdates::join('ev_college', 'ev_college.id', '=', 'ev_collegeupdates.college_id')
            ->leftJoin('ev_district', 'ev_district.id', '=', 'ev_college.district')
            ->where('sales_id', Auth::user()->id)
            ->where(function ($query) {
                $query->whereRaw("DATE_ADD(date, INTERVAL 1 MONTH) >= CURDATE()")
                    ->orWhere(function ($subQuery) {
                        $subQuery->where('end_date', '>', now())
                            ->whereNotNull('end_date');
                    });
            })
            ->distinct('ev_college.id')
            ->get([
                'ev_college.id',
                'ev_college.college',
                'ev_district.name as district_name'
            ]);

        return view('add_students', $data);
    }

    public function monthlyadmissionreport_regular()
    {
        if (Auth::check()) {
            if (isset($_GET['search_date1']) != '' && isset($_GET['search_date2']) != '') {
                $data['search_date1'] = date("d-m-Y", strtotime($_GET['search_date1']));
                $data['search_date2'] = date("d-m-Y", strtotime($_GET['search_date2']));

                $first_date = date("Y-m-d", strtotime($_GET['search_date1']));
                $today = date("Y-m-d", strtotime($_GET['search_date2']));
            } else {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');

                $data['search_date1'] = '';
                $data['search_date2'] = '';
            }

            if (Auth::user()->role == 2) {
                $user_id = Auth::user()->id;
                $data['empstatus'] = 0;
                $data['title'] = 'Regular Admission Revenue' . ' ' . date('F Y');
                $data['pac_type'] = 1;

                $data['report'] = Salespackage::join('users', 'users.id', '=', 'sales_package.student_id')
                    ->where("sales_package.sales_id", $user_id)
                    ->where("sales_package.package_type", 1)
                    ->whereBetween("sales_package.joining_date", [$first_date, $today])
                    ->orderby("sales_package.id", "desc")
                    ->distinct()
                    ->get(['sales_package.*', 'users.name', 'users.student_admissionid']);

                $data['oldreport'] = Oldstudent::where("sales_id", Auth::user()->id)
                    ->whereBetween("date_joining", [$first_date, $today])
                    ->get(['ev_oldstudents.name', 'ev_oldstudents.reg_no as student_admissionid', 'ev_oldstudents.package as pac_name']);

                $data["monthadmi_regular_withgst"] = round(
                    Salespackage::where("sales_package.sales_id", $user_id)
                        ->where("sales_package.package_type", 1)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_fullamount")
                );
                $data['reg_old_withgst'] = round(
                    Oldstudent::where("ev_oldstudents.sales_id", $user_id)
                        ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                        ->sum("ev_oldstudents.totalfees")
                );
                $data['monthly_tot_admissionwithgst'] = $data["monthadmi_regular_withgst"] + $data['reg_old_withgst'];

                return view('monthlyregold_admission', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function old_student()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 2) {
                $data['report'] = OldPayment::join('ev_oldstudents', 'ev_oldstudents.id', '=', 'olddata_payment.oldstudent_id')
                    ->where("olddata_payment.sales_id", Auth::user()->id)
                    ->get(['olddata_payment.paid_amount as total', 'olddata_payment.fees_income as payment', 'olddata_payment.tax', 'olddata_payment.date', 'ev_oldstudents.name', 'ev_oldstudents.reg_no']);

                $data['pac_type'] = 0;
                $data['empstatus'] = 0;

                $data['title'] = 'Total Revenue From Old Students';

                return view('sales_monthlyreport', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function checkeditno(Request $request)
    {
        $no = $request->get("no");

        $id = $request->get("id");

        $count = Contacts::where(function ($query) use ($no) {
            $query->where('contact1', $no)->orWhere('contact2', $no);
        })
            ->where('id', '<>', $id)
            ->exists();
        if ($count == 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function oldpayment_history($studentid)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 2 || Auth::user()->role == 1) {
                $data['payment_history'] = OldPayment::orderBy('id', 'desc')
                    ->where('oldstudent_id', $studentid)
                    ->get();
                $data['totalfees'] = Oldstudent::find($studentid)->totalfees;

                $data['paidfees'] = OldPayment::where('oldstudent_id', $studentid)->sum('paid_amount');

                $data['student_id'] = $studentid;

                $data['name'] = Oldstudent::find($studentid)->name;
                $data['title'] = 'Payment History of ' . $data['name'];

                return view('oldpayment_history', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function upload_contacts()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 2) {
                $data['sourcelist'] = Source::where('status', 1)
                    ->orderby('id', 'desc')
                    ->get();

                $data['title'] = 'Bulk Upload Contacts';

                return view('BulkContacts', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function sales_chrysalis_employee()
    {
        if (Auth::check()) {
            $data['pac_type'] = 2;

            if (Auth::user()->role == 1 || Auth::user()->role == 2) {
                $arrayVariable = Session::get('array_key');

                $data1['names'] = User::whereIn('id', $arrayVariable)->pluck('name');

                $keysString = '';
                foreach ($data1['names'] as $names) {
                    $keysString .= $names . ', ';
                }
                $name = rtrim($keysString, ', ');

                $data['monthlyreport'] = Salespackage::join('users', 'users.id', '=', 'sales_package.student_id')
                    ->where("sales_package.package_type", 2)

                    ->whereIn("sales_package.sales_id", $arrayVariable)
                    ->orderby("sales_package.id", "desc")
                    ->get(['sales_package.*', 'users.name']);
                $data['url'] = Session::get('fullurl');
                $data['title'] = 'Total Chrysalis' . ' of ' . ucfirst($name);
                $data['subtitle'] = 'Reports of ' . ucfirst($name);

                return view('sales_admissionreport_employee', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function sales_admission_employee()
    {
        if (Auth::check()) {
            $data['pac_type'] = 1;

            if (Auth::user()->role == 1 || Auth::user()->role == 2) {
                $arrayVariable = Session::get('array_key');

                $data1['names'] = User::whereIn('id', $arrayVariable)->pluck('name');

                $keysString = '';
                foreach ($data1['names'] as $names) {
                    $keysString .= $names . ', ';
                }
                $name = rtrim($keysString, ', ');

                $data['monthlyreport'] = Salespackage::join('users', 'users.id', '=', 'sales_package.student_id')

                    ->whereIn("sales_package.sales_id", $arrayVariable)
                    ->where("sales_package.package_type", 1)
                    ->orderby("sales_package.id", "desc")
                    ->get(['sales_package.*', 'users.name', 'users.student_admissionid']);

                $data['url'] = Session::get('fullurl');
                $data['title'] = 'Total Admissions' . ' of ' . ucfirst($name);
                $data['subtitle'] = 'Reports of ' . ucfirst($name);

                return view('sales_admissionreport_employee', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function chrysalis_revenue_employee()
    {
        if (Auth::check()) {
            $data['pac_type'] = 2;

            if (Auth::user()->role == 1 || Auth::user()->role == 2) {
                $arrayVariable = Session::get('array_key');

                $data1['names'] = User::whereIn('id', $arrayVariable)->pluck('name');

                $keysString = '';
                foreach ($data1['names'] as $names) {
                    $keysString .= $names . ', ';
                }
                $name = rtrim($keysString, ', ');

                $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                    ->where("sales_payment.package_type", 2)
                    ->whereIn("sales_payment.sales_id", $arrayVariable)
                    ->orderby("sales_payment.id", "desc")
                    ->get(['sales_payment.*', 'users.name', 'users.student_admissionid']);

                $data['title'] = 'Total Revenue For Chrysalis Reports' . ' of ' . ucfirst($name);
                $data['subtitle'] = 'Reports of ' . ucfirst($name);

                $data['url'] = Session::get('fullurl');
                return view('sales_monthlyreport_employee', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function admission_revenue_employee()
    {
        if (Auth::check()) {
            $data['pac_type'] = 1;

            if (Auth::user()->role == 1 || Auth::user()->role == 2) {
                $arrayVariable = Session::get('array_key');

                $data1['names'] = User::whereIn('id', $arrayVariable)->pluck('name');

                $keysString = '';
                foreach ($data1['names'] as $names) {
                    $keysString .= $names . ', ';
                }
                $name = rtrim($keysString, ', ');

                $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                    ->where("sales_payment.package_type", 1)
                    ->whereIn("sales_payment.sales_id", $arrayVariable)
                    ->orderby("sales_payment.id", "desc")
                    ->get(['sales_payment.payment', 'sales_payment.tax', 'sales_payment.total', 'sales_payment.date', 'users.name', 'users.student_admissionid']);

                $data['title'] = 'Total Revenue For Admission Reports' . ' of ' . ucfirst($name);
                $data['subtitle'] = 'Reports of ' . ucfirst($name);
                $data['url'] = Session::get('fullurl');
                return view('sales_monthlyreport_employee', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function chrysalis_monthlyreport_employee()
    {
        if (Auth::check()) {
            $today = date('Y-m-d');
            $first_date = date('Y-m-01');

            $data['pac_type'] = 2;

            if (Auth::user()->role == 1 || Auth::user()->role == 2) {
                $arrayVariable = Session::get('array_key');

                $data1['names'] = User::whereIn('id', $arrayVariable)->pluck('name');

                $keysString = '';
                foreach ($data1['names'] as $names) {
                    $keysString .= $names . ', ';
                }
                $name = rtrim($keysString, ', ');

                $data['title'] = date('F Y') . ' ' . 'Chrysalis Reports' . ' of ' . ucfirst($name);
                $data['subtitle'] = 'Reports of ' . ucfirst($name);
                $data['url'] = Session::get('fullurl');

                $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                    ->where("sales_payment.package_type", 2)
                    ->whereIn("sales_payment.sales_id", $arrayVariable)

                    ->whereBetween('date', [$first_date, $today])
                    ->orderby("sales_payment.id", "desc")

                    ->get(['sales_payment.*', 'users.name']);

                return view('sales_monthlyreport_employee', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function update_contacts(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 2) {
                $data['name'] = $request->get("name");
                $data['contact1'] = $request->get("editcnum1");
                $data['contact2'] = $request->get("editcnum2");
                $data['source'] = $request->get("source");
                $data['email'] = $request->get("email");
                $data['college'] = $request->get("college");
                $data['qualification'] = $request->get("qualification");
                $data['year'] = $request->get("year");
                $data['added_user'] = Auth::user()->id;
                $data['contacts_assigned'] = Auth::user()->id;

                $count1 = Contacts::where(function ($query) {
                    $query->where('contact1', $_POST['editcnum1'])->orWhere('contact2', $_POST['editcnum1']);
                })
                    ->where('id', '!=', $_POST['contact_id'])
                    ->exists();
                $count2 = Oldstudent::where('contact_no', $data['contact1'])->count();
                $count3 = User::where('phone', $data['contact1'])->count();

                if ($request->get("email") == '') {
                    if ($count1 == 0 && $count2 == 0 && $count3 == 0) {
                        Contacts::where('id', $request->get("contact_id"))->update($data);
                        echo 2;
                    } else {
                        echo 1;
                    }
                } else {
                    $emcount1 = Contacts::where('email', $data['email'])
                        ->where('id', '!=', $_POST['contact_id'])
                        ->exists();
                    $emcount2 = Oldstudent::where('email_id', $data['email'])->count();
                    $emcount3 = User::where('email', $data['email'])->count();

                    if ($emcount1 == 0 && $emcount2 == 0 && $emcount3 == 0 && $count1 == 0 && $count2 == 0 && $count3 == 0) {
                        Contacts::where('id', $request->get("contact_id"))->update($data);
                        echo 2;
                    } elseif ($count1 != 0 || $count2 != 0 || $count3 != 0) {
                        echo 1;
                    } elseif ($emcount1 != 0 || $emcount2 != 0 || $emcount3 != 0) {
                        echo 3;
                    }
                }
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    /* public function update_contacts(Request $request){

        if(Auth::check()) {
    
            if(Auth::user()->role==2){
                $data['name']       = $request->get("name");
                $data['contact1']       = $request->get("editcnum1");
                $data['contact2']       = $request->get("editcnum2");
                $data['source']         = $request->get("source");
                $data['email']          = $request->get("email");
                $data['college']        = $request->get("college");
                $data['qualification']  = $request->get("qualification");
                $data['year']           = $request->get("year");
                $data['added_user']     =Auth::user()->id;
                $data['contacts_assigned'] =Auth::user()->id;  
               
                    Contacts::where('id',$request->get("contact_id"))->update($data);
                    Session::flash('success', 'Sucessfully Updated');
                    return redirect('/outboundcalls');
                

                }
    
            
            else{
                return redirect('/acessdenied'); 
            }
    
        }
        else{
    
            return redirect('/');
    
        }
    
       }*/

    public function edit_contact($contactid)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 2) {
                $data['sourcelist'] = Source::where('status', 1)
                    ->orderby('id', 'desc')
                    ->get();
                $data['collegelist'] = College::where('status', 1)
                    ->orderby('id', 'desc')
                    ->get();
                $data['qualification'] = Qualification::where('status', 1)
                    ->orderby('id', 'desc')
                    ->get();
                $data['title'] = 'Edit Contact';

                $data['contactdetails'] = Contacts::where('id', $contactid)->first();

                return view('edit_contact', $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    /*public function gatepass_download($reqid)
    {
        $signatute = Signature::find(1)->signature;
        $seal = Signature::find(1)->seal;
        $logo = Signature::find(1)->header;
        $contact_name = Signature::find(1)->name;
        $contact_no = Signature::find(1)->contact_no;
        $passdetails = Gatepass::join("users", "users.id", "=", "ev_gatepassrequest.user_id")
            ->where('ev_gatepassrequest.id', $reqid)
            ->first(['ev_gatepassrequest.*', 'users.name']);

        $data = [
            'date' => date('d-m-Y'),
            'name' => ucfirst($passdetails['name']),

            'photo' => $passdetails['photo'],
            'days' => ucfirst($passdetails['days']),
            'start_date' => $passdetails['start_date'],
            'end_date' => $passdetails['end_date'],
            'signatute' => $signatute,
            'seal' => $seal,
            'logo' => $logo,
            'contact_name' => $contact_name,
            'contact_no' => $contact_no,
        ];

        $html = view('pass')
            ->with($data)
            ->render();
        // Setup Dompdf
        $options = new Options();
        $options->set('isHtml5ParserEnabled', true);
        $options->set('isRemoteEnabled', true);
        $dompdf = new Dompdf($options);
        // Load HTML content
        $dompdf->loadHtml($html);

        $dompdf->setPaper('A4', 'portrait');

        $dompdf->render();
        return $dompdf->stream('gatepass.pdf');
    }*/

    public function gatepass_download($reqid)
    {
        // Retrieve signature details
        $signature = Signature::find(1);
        $signatute = $signature->signature;
        $seal = $signature->seal;
        $logo = $signature->header;
        $contact_name = $signature->name;
        $contact_no = $signature->contact_no;

        // Fetch gatepass details along with the user's name
        $passdetails = Gatepass::join('users', 'users.id', '=', 'ev_gatepassrequest.user_id')
            ->where('ev_gatepassrequest.id', $reqid)
            ->first(['ev_gatepassrequest.*', 'users.name']);

        // Prepare data for the view
        $data = [
            'date' => date('d-m-Y'),
            'name' => ucfirst($passdetails->name),
            'photo' => $passdetails->photo,
            'days' => ucfirst($passdetails->days),
            'start_date' => $passdetails->start_date,
            'end_date' => $passdetails->end_date,
            'signatute' => $signatute,
            'seal' => $seal,
            'logo' => $logo,
            'contact_name' => $contact_name,
            'contact_no' => $contact_no,
        ];

        // Generate HTML content from the view
        $html = view('pass', $data)->render();

        // Configure Dompdf options
        $options = new Options();
        $options->set('isHtml5ParserEnabled', true);
        $options->set('isRemoteEnabled', true);
        $dompdf = new Dompdf($options);

        // Load and render HTML content
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        // Stream the generated PDF
        return $dompdf->stream('gatepass.pdf');
    }

    public function assign_talento_regularpackage(Request $request)
    {
        $datatax['tax'] = Tax::find(1)->tax;
        $talentoid = $request->get('talentoid');
        $finalamount = $request->get('finalamount');
        $data['package_id'] = $request->get('packname');
        $data['student_id'] = $request->get('userid');

        $maxval = Studentinfo::max('student_admissionid');

        if (isset($_POST['reductioncheck'])) {
            $data['reduction_check'] = 1;
            $data['reduction_amount'] = $request->get('reduction_amount');
            $data['reduced_fees'] = $request->get('reducedfees');
            $data['tax'] = $request->get('reducedtax');
            $data['totalfees_afterreduction'] = $request->get('after_reduction');
            $data['ineffect_total_red'] = $request->get('ineffect_offered');
            $data['referalcode'] = $this->random_string(6);
        } else {
            $data['totalfees_afterreduction'] = $finalamount;
        }

        $datauser['status'] = 1;
        $insertid = User::where('id', $data['student_id'])->update($datauser);

        Studentinfo::insert(['student_id' => $data['student_id'], 'student_admissionid' => $maxval + 1, 'college_id' => Talento::find($talentoid)->college_id, 'sales_id' => Auth::user()->id, 'created_at' => date("Y-m-d")]);

        //1.User table insert*******************************************

        $data['registration_fees'] = $request->get('regfees');
        $data['due_date_first'] = $request->get('inst_red');
        $data['reference_type'] = $request->get('referenece');
        $data['refence_id'] = $request->get('referenece_list');
        $data['package_type'] = 1;
        $data['joining_date'] = date("Y-m-d", strtotime($request->get('oldpay_date')));

        $data['sales_id'] = Auth::user()->id;
        Studentpackage::insert($data);

        //2.Student Package table insert*******************************************
        $regfees = $request->get('regfees');

        $payment_type = $request->get('payMethod_bank');
        $payment_date = date("Y-m-d", strtotime($request->get('paydate')));

        if ($payment_type == 'bank') {
            $transaction_id = $request->get('trans_id');
        } else {
            $transaction_id = '';
        }

        if ($request->hasFile('package_reciept')) {
            $extension = $request->file("package_reciept")->getClientOriginalExtension();
            $destinationPath = public_path('uploads/screenshot'); // upload path
            $prd_image_name = 'screenshot_' . time() . '.' . $extension;
            $request->file("package_reciept")->move($destinationPath, $prd_image_name);
            $imageName = $prd_image_name;
        } else {
            $imageName = '';
        }

        if ($data['reference_type'] == 1) {
            //payment transfer to sales
            $datasales['sales_id'] = Auth::user()->id;
            $datasales['student_id'] = $data['student_id'];
            $datasales['package_fullamount'] = $data['totalfees_afterreduction'] / 2;
            $x1 = 1 + $datatax['tax'] / 100;
            $datasales['package_amount'] = $datasales['package_fullamount'] / $x1;

            $datasales['package_tax'] = $datasales['package_fullamount'] - $datasales['package_amount'];
            $datasales['package_type'] = 1;
            $datasales['joining_date'] = date("Y-m-d", strtotime($request->get('oldpay_date')));
            $datasales['package_id'] = $data['package_id'];

            Salespackage::insert($datasales);

            //payment transfer to reference

            $dataref['sales_id'] = $data['refence_id'];
            $dataref['student_id'] = $data['student_id'];
            $dataref['package_fullamount'] = $data['totalfees_afterreduction'] / 2;
            $dataref['package_amount'] = $dataref['package_fullamount'] / $x1;
            $dataref['package_tax'] = $dataref['package_fullamount'] - $dataref['package_amount'];
            $dataref['package_type'] = 1;
            $dataref['joining_date'] = date("Y-m-d", strtotime($request->get('oldpay_date')));
            $dataref['package_id'] = $data['package_id'];
            Salespackage::insert($dataref);
        } else {
            $datasales['sales_id'] = Auth::user()->id;
            $datasales['student_id'] = $data['student_id'];
            $datasales['package_fullamount'] = $data['totalfees_afterreduction'];
            $x1 = 1 + $datatax['tax'] / 100;
            $datasales['package_amount'] = $data['totalfees_afterreduction'] / $x1;
            $datasales['package_tax'] = $datasales['package_fullamount'] - $datasales['package_amount'];
            $datasales['package_type'] = 1;
            $datasales['joining_date'] = date("Y-m-d", strtotime($request->get('oldpay_date')));
            $datasales['package_id'] = $data['package_id'];
            Salespackage::insert($datasales);
        }

        //3.Sales Package table insert*******************************************

        $monthlyfees = $request->get('monthlyfees');
        $splitdate = $request->get('splitdate');
        $total = $regfees;
        $balance = 0;
        $sum = 0;
        for ($i = 0; $i < count($monthlyfees); $i++) {
            $monthlyfees1 = $monthlyfees[$i];
            $splitdate1 = $splitdate[$i];

            if ($total > $monthlyfees1) {
                if ($i == 0) {
                    $paidffes = $monthlyfees1;
                    $sum += $paidffes;
                    $balance = $total - $monthlyfees1;
                    $pendingfees = 0;
                } else {
                    if ($i == count($monthlyfees) - 1) {
                        $paidffes = $balance;

                        $pendingfees = $monthlyfees1 - $balance;

                        if ($pendingfees > 0) {
                            $balance = 0;
                        }
                    } else {
                        if ($balance > $monthlyfees1) {
                            $paidffes = $monthlyfees1;
                            $sum += $paidffes;
                            $pendingfees = 0;
                            $balance = $balance - $monthlyfees1;
                        } else {
                            $paidffes = $balance;
                            $sum += $paidffes;
                            $pendingfees = $monthlyfees1 - $balance;

                            if ($pendingfees > 0) {
                                $balance = 0;
                            }
                        }
                    }
                }
            } else {
                if ($i == 0) {
                    $paidffes = $request->get('regfees');
                    $pendingfees = $monthlyfees1 - $paidffes;
                    $sum += $paidffes;
                } else {
                    $paidffes = 0;
                    $pendingfees = $monthlyfees1;
                    $sum += $paidffes;
                }
            }

            Studentfeessplit::insert(['student_id' => $data['student_id'], 'package_type' => 1, 'package_id' => $data['package_id'], 'fees' => $monthlyfees1, 'date' => $splitdate1, 'paidfees' => $paidffes, 'pendingfees' => $pendingfees]);
        }

        //4.Student Fees  splitup  table insert*******************************************

        if ($data['reference_type'] == 1) {
            $x = 1 + $datatax['tax'] / 100;

            $income_sales = $regfees / 2 / $x;
            $income_ref = $regfees / 2 / $x;

            $taxval_sale = $regfees / 2 - $income_sales;

            $taxval_ref = $regfees / 2 - $income_ref;

            $income = $regfees / $x;
            $taxval = $regfees - $income;
            $payment_id = Payment::insertGetId([
                'package_id' => $request->get('packname'),
                'student_id' => $data['student_id'],
                'payment_type' => $payment_type,
                'screenshot' => $imageName,
                'transaction_id' => $transaction_id,
                'package_type' => 1,
                'paid_amount' => $total,
                'pending_amount' => $finalamount - $total,
                'date' => $payment_date,
                'fees_income' => round($income),
                'tax' => round($taxval),
            ]);
            Salespayment::insert([
                'sales_id' => Auth::user()->id,
                'package_type' => 1,
                'payment' => round($income_sales),
                'tax' => round($taxval_sale),
                'total' => round($regfees / 2),
                'student_id' => $data['student_id'],
                'package_id' => $request->get('packname'),
                'payment_id' => $payment_id,
                'date' => $payment_date,
            ]);

            Salespayment::insert([
                'sales_id' => $data['refence_id'],
                'package_type' => 1,
                'payment' => round($income_ref),
                'tax' => round($taxval_ref),
                'total' => round($regfees / 2),
                'student_id' => $data['student_id'],
                'package_id' => $request->get('packname'),
                'payment_id' => $payment_id,
                'date' => $payment_date,
            ]);
        } else {
            $x = 1 + $datatax['tax'] / 100;
            $income = $regfees / $x;
            $taxval = $regfees - $income;
            $payment_id = Payment::insertGetId([
                'package_id' => $request->get('packname'),
                'payment_type' => $payment_type,
                'screenshot' => $imageName,
                'transaction_id' => $transaction_id,
                'student_id' => $data['student_id'],
                'package_type' => 1,
                'paid_amount' => $total,
                'pending_amount' => $finalamount - $total,
                'date' => $payment_date,
                'fees_income' => round($income),
                'tax' => round($taxval),
            ]);
            Salespayment::insert([
                'sales_id' => Auth::user()->id,
                'package_type' => 1,
                'payment' => round($income),
                'tax' => round($taxval),
                'total' => round($regfees),
                'student_id' => $data['student_id'],
                'package_id' => $request->get('packname'),
                'payment_id' => $payment_id,
                'date' => $payment_date,
            ]);
        }

        //5.Sales- Payment Table  insert*******************************************

        Talento::where('id', $talentoid)->update(['status' => 1]);

        //5.Talento registration Table

        Session::flash('success', 'Sucessfully Assigned');
        return redirect('/students_list/' . $data['package_id']);
    }
    public function assign_chrysalis_package(Request $request)
    {
        $type = $request->get('type');
        $data['student_id'] = $request->get('userid');
        $student_id = $data['student_id'];
        $datatax['tax'] = Tax::find(1)->tax;
        $data['package_type'] = 2;
        $data['package_id'] = $request->get('packname');

        $finalamount = $request->get('finalamount');
        $data['registration_fees'] = 0;
        $data['due_date_first'] = $request->get('start_date');
        $data['reference_type'] = $request->get('referenece');
        $data['refence_id'] = $request->get('referenece_list');
        $data['enddate'] = $request->get('end_date');

        if ($type == 'talento' || $type == 'contact' || $type == 'all') {
            $count = Studentpackage::where('student_id', $data['student_id'])
                ->where('package_id', $data['package_id'])
                ->count();
            if ($count == 0) {
                if (isset($_POST['reductioncheck'])) {
                    $data['reduction_check'] = 1;
                    $data['reduction_amount'] = $request->get('reduction_amount');
                    $data['reduced_fees'] = $request->get('reducedfees');
                    $data['tax'] = $request->get('reducedtax');
                    $data['totalfees_afterreduction'] = $request->get('after_reduction');
                    $data['ineffect_total_red'] = $request->get('ineffect_offered');
                    $data['referalcode'] = $this->random_string(6);
                } else {
                    $data['totalfees_afterreduction'] = $finalamount;
                }

                $data['joining_date'] = date("Y-m-d", strtotime($request->get('oldpay_date')));

                $data['sales_id'] = Auth::user()->id;
                Studentpackage::insert($data);

                //1.insert student package

                if ($data['reference_type'] == 1 && $data['refence_id'] != '') {
                    //payment transfer to sales

                    $datasales['sales_id'] = Auth::user()->id;
                    $datasales['package_type'] = 2;
                    $datasales['student_id'] = $student_id;
                    $datasales['package_fullamount'] = $data['totalfees_afterreduction'] / 2;
                    $x1 = 1 + $datatax['tax'] / 100;
                    $datasales['package_amount'] = $datasales['package_fullamount'] / $x1;
                    $datasales['package_tax'] = $datasales['package_fullamount'] - $datasales['package_amount'];
                    $datasales['package_type'] = 2;
                    $datasales['joining_date'] = $data['joining_date'];

                    $datasales['package_id'] = $request->get('packname');

                    Salespackage::insert($datasales);
                    //payment transfer to reference
                    $dataref['joining_date'] = $data['joining_date'];
                    $dataref['sales_id'] = $data['refence_id'];
                    $dataref['package_type'] = 2;
                    $dataref['student_id'] = $student_id;
                    $dataref['package_type'] = 2;
                    $dataref['package_fullamount'] = $data['totalfees_afterreduction'] / 2;
                    $dataref['package_amount'] = $dataref['package_fullamount'] / $x1;
                    $dataref['package_tax'] = $dataref['package_fullamount'] - $dataref['package_amount'];
                    $dataref['package_id'] = $request->get('packname');
                    Salespackage::insert($dataref);
                } else {
                    $datasales['joining_date'] = $data['joining_date'];
                    $datasales['sales_id'] = Auth::user()->id;
                    $datasales['package_type'] = 2;
                    $datasales['student_id'] = $student_id;
                    $datasales['package_fullamount'] = $data['totalfees_afterreduction'];
                    $x1 = 1 + $datatax['tax'] / 100;
                    $datasales['package_amount'] = $data['totalfees_afterreduction'] / $x1;
                    $datasales['package_tax'] = $datasales['package_fullamount'] - $datasales['package_amount'];
                    $datasales['package_id'] = $request->get('packname');
                    Salespackage::insert($datasales);
                }

                if ($type == 'talento') {
                    $talentoid = $request->get('talentoid');
                    Talento::where('id', $talentoid)->update(['status' => 1]);

                    Session::flash('success', 'Sucessfully Assigned');
                    return redirect('/students_list/' . $data['package_id']);
                }
                if ($type == 'all') {
                    Session::flash('success', 'Sucessfully Assigned');
                    return redirect('/students_list/' . $data['package_id']);
                }
            } else {
                if ($type == 'talento') {
                    Session::flash('errormessage', 'Already Assigned this package');
                    return redirect('/manage_talento_registration/');
                }
                if ($type == 'all') {
                    Session::flash('errormessage', 'Already Assigned this package');
                    return redirect('/get_students_chrysalis/');
                }
            }
        } else {
            return redirect("/acessdenied");
        }
    }

    public function manage_talento_registration()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 2) {
                $codes = Employeeinfo::where('user_id', Auth::user()->id)->first(['poc_code']);

                $code = $codes['poc_code'];

                $data["studentlist"] = User::join('talento_registration', 'users.id', '=', 'talento_registration.user_id')
                                            ->join('ev_student_info', 'ev_student_info.student_id', '=', 'users.id')
                                            ->leftjoin('ev_college', 'ev_college.id', '=', 'ev_student_info.college_id')
                                            ->where('talento_registration.staff_code', $code)
                                            ->where('talento_registration.status', 0)
                                            ->orderby("talento_registration.id", "desc")
                                            ->get(['users.name', 'users.email', 'users.phone', 'ev_college.college', 'talento_registration.*', 'ev_student_info.student_admissionid', 'ev_student_info.student_id']);

                $data['title'] = 'Talento Registration';
                return view("manage_talento_registration", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function assign_regular_package($student_id)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 2) {
                $type = 1;

                $data['title'] = 'Package Details';

                $data['package'] = Package::leftJoin("ev_technology", "ev_technology.id", "=", "ev_package.tech_id")
                    ->where("ev_package.pac_type", $type)
                    ->where("ev_technology.status", 1)
                    ->where("ev_package.status", 1)
                    ->get();

                return view('assignregularpackage', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function select_chrysalis_package($student_id, $type)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 2) {
                $data['title'] = 'Chrysalis Package';
                $user = $student_id;

                if ($type == 'talento' || $type == 'contact' || $type == 'all') {
                    $data['type'] = $type;

                    if ($type == 'talento') {
                        $talento = Talento::where('user_id', $user)->first();
                        $type = $talento['registration_type'];
                        $data['closed_date'] = $talento['date'];
                        $data['talentoid'] = $talento['id'];
                    }

                    $subquery = Studentpackage::where('student_id', $student_id)
                        ->where('package_type', 2)
                        ->pluck('package_id'); // Use pluck to get a collection of package IDs

                    // Main query to get all packages not in the subquery list
                    $data['package'] = Package::leftJoin('ev_technology', 'ev_technology.id', '=', 'ev_package.tech_id')
                        ->where('ev_package.pac_type', 2)
                        ->where('ev_technology.status', 1)
                        ->where('ev_package.status', 1)
                        ->whereNotIn('ev_package.pac_id', $subquery) // Pass the collection to whereNotIn
                        ->select('ev_package.pac_id', 'ev_package.pac_name')
                        ->get(); // Make sure to call get() to execute the query

                    $data['user_details'] = Studentinfo::leftjoin('ev_college', 'ev_student_info.college_id', '=', 'ev_college.id')
                        ->join('users', 'users.id', '=', 'ev_student_info.student_id')
                        ->where('ev_student_info.student_id', $user)
                        ->first(['users.*', 'ev_college.college']);

                  

                    return view('select_chrysalis_package', $data);
                } else {
                    return redirect('/acessdenied');
                }
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }
    public function payment_history_package($studentpackage_id)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 2 || Auth::user()->role == 1) {
                $data['package_details'] = Studentpackage::join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
                    ->where('ev_student_package.id', $studentpackage_id)
                    ->first(['ev_package.total', 'ev_student_package.*', 'ev_package.pac_name']);

                $studentid = $data['package_details']->student_id;
                $pac_id = $data['package_details']->package_id;
                $pac_type = $data['package_details']->package_type;
                $data['pac_type'] = $pac_type;

                $data['name'] = User::find($studentid)->name;

                $data['pac_id'] = $pac_id;

                if ($pac_type == 1) {
                    $sname = 'Regular';
                } else {
                }

                if (Auth::user()->role == 2) {
                    $data['subtitle'] = 'Students of ' . $data['package_details']->pac_name;
                    $data['title'] = ucfirst($data['name']) . '-' . 'Payment History of ' . $data['package_details']->pac_name;
                } else {
                    $data['subtitle'] = 'students of ' . $data['package_details']->pac_name;
                    $data['title'] = 'Payment History of ' . $data['name'];
                }

                $data['payment_history'] = Payment::orderBy('id', 'desc')
                    ->where('student_id', $studentid)
                    ->where('package_type', $pac_type)
                    ->where('package_id', $pac_id)
                    ->get();

                $data['paidfees'] = Payment::where('student_id', $studentid)
                    ->where('package_type', $pac_type)
                    ->where('package_id', $pac_id)
                    ->sum('paid_amount');

                $data['student_id'] = $studentid;
                $data['studentpackage_id'] = $studentpackage_id;
                return view('payment_history_package', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function students_list(Request $request,$pac_id)
    {
        // Check if user is authenticated
        if (Auth::check()) {
            // Check if user role is admin (1) or sales (2)
            if (Auth::user()->role == 2 || Auth::user()->role == 1) {
                // Fetch package type and name based on $pac_id
                $package = Package::where('pac_id', $pac_id)->first(['pac_type', 'pac_name']);
                $data['pac_type'] = $package->pac_type;
                $userIds = [];
                // If user role is sales (2), fetch reported users and add current user's ID

                // Get year and month from query parameters with default values
                $year = $request->query('year', Carbon::now()->year);
                $month = $request->query('month', Carbon::now()->format('m'));

                // Validate year and month
                if (!checkdate($month, 1, $year)) {
                    return redirect('/accessdenied');
                }

                // Calculate start and end of the month
                $startOfMonth = Carbon::create($year, $month, 1)->startOfMonth()->format('Y-m-d');
                $endOfMonth = Carbon::create($year, $month, 1)->endOfMonth()->format('Y-m-d');

                if (Auth::user()->role == 2) {


                    $reportedIds = Reporting::where('ev_reporting.reported_to', Auth::user()->id)
                        ->get(['ev_reporting.employee_id'])
                        ->pluck('employee_id')
                        ->toArray();
                    $userIds = array_merge($reportedIds, [Auth::user()->id]);
                }
                // Prepare title and fetch student list based on package type

                if ($data['pac_type'] == 1) {
                    $data['title'] = 'Students of ' . $package->pac_name;

                    $studentlist = Studentpackage::join('users', 'users.id', '=', 'ev_student_package.student_id')
                        ->join('ev_student_info', 'ev_student_info.student_id', '=', 'ev_student_package.student_id')
                        ->leftJoin('ev_college', 'ev_college.id', '=', 'ev_student_info.college_id')
                        ->where('ev_student_package.package_id', $pac_id)
                        ->where('ev_student_package.package_type', 1)
                        ->whereBetween('ev_student_package.joining_date', [$startOfMonth, $endOfMonth])
                        ->where(function ($query) use ($userIds) {
                            if (Auth::user()->role == 2) {
                                $query->whereIn('ev_student_package.sales_id', $userIds);
                            }
                        })
                        ->orderBy('ev_student_package.id', 'desc')
                        ->get([
                            'users.name',
                            'users.email',
                            'users.phone',
                            'ev_college.college as collegename',
                            'ev_student_package.due_date_first',
                            'ev_student_package.enddate',
                            'ev_student_package.id',
                            'ev_student_info.student_admissionid',
                            'ev_student_package.joining_date',
                            
                        ]);

                        $data = [
                            
                            'years' => CommonController::generateYearRange(date('Y') - 10),
                            'months' => CommonController::generateMonthOptions(),
                            'currentYear' => $year,
                            'currentMonth' => $month,
                            'pac_type'=> $package->pac_type,
                            'title'=>'Students of ' . $package->pac_name,
                            'studentlist'=>  $studentlist,
                            'pac_id'=>$pac_id
                        ];


                } else {
                   

                    $studentlist = Studentpackage::join('users', 'users.id', '=', 'ev_student_package.student_id')
                        ->join('ev_student_info', 'ev_student_info.student_id', '=', 'ev_student_package.student_id')
                        ->leftJoin('ev_college', 'ev_college.id', '=', 'ev_student_info.college_id')
                        ->where('ev_student_package.package_id', $pac_id)
                        ->where('ev_student_package.package_type', 2)
                        ->whereBetween('ev_student_package.joining_date', [$startOfMonth, $endOfMonth])
                        ->where(function ($query) use ($userIds) {
                            if (Auth::user()->role == 2) {
                                $query->whereIn('ev_student_package.sales_id', $userIds);
                            }
                        })
                        ->orderBy('ev_student_package.id', 'desc')
                        ->get([
                            'users.name',
                            'users.email',
                            'users.phone',
                            'ev_college.college as collegename',
                            'ev_student_package.due_date_first',
                            'ev_student_package.enddate',
                            'ev_student_package.id',
                            'ev_student_info.student_admissionid',
                            'ev_student_package.joining_date',
                        ]);

                        $data = [
                            
                            'years' => CommonController::generateYearRange(date('Y') - 10),
                            'months' => CommonController::generateMonthOptions(),
                            'currentYear' => $year,
                            'currentMonth' => $month,
                            'pac_type'=> $package->pac_type,
                            'title'=>'Students of ' . $package->pac_name,
                            'studentlist'=>  $studentlist,
                            'pac_id'=>$pac_id
                        ];


                       
                }

                // Return view with data
                return view("studentlist", $data);
            } else {
                // Redirect to access denied page if user role is not admin or sales
                return redirect('/acessdenied');
            }
        } else {
            // Redirect to login page if user is not authenticated
            return redirect('/');
        }
    }
    public function receipt_download($paymentid)
    {
        $paymenthistory = OldPayment::where('id', $paymentid)->first();

        $student_id = $paymenthistory['oldstudent_id'];
        $date = $paymenthistory['date'];
        $payment_type = $paymenthistory['payment_type'];
        $amount = $paymenthistory['paid_amount'];

        $student_name = Oldstudent::find($student_id)->name;
        $student_phone = Oldstudent::find($student_id)->contact_no;
        $package = Oldstudent::find($student_id)->package;

        $signatute = Signature::find(1)->signature;
        $seal = Signature::find(1)->seal;
        $logo = Signature::find(1)->header;

        $datatax['tax'] = Tax::find(1)->tax;
        $x = 1 + $datatax['tax'] / 100;

        $income = round($amount / $x, 2);
        $taxval = round($amount - $income, 2);

        $data = [
            'receipt_no' => $paymentid,
            'date' => $date,
            'name_student' => ucfirst($student_name),
            'name_phone' => $student_phone,
            'payment_type' => ucfirst($payment_type),
            'amount' => $amount,
            'status' => 'Success',
            'cource' => $package,
            'actual_amount' => $income,
            'cgst' => number_format($taxval / 2, 2),
            'gst' => number_format($taxval / 2, 2),
            'signatute' => $signatute,
            'seal' => $seal,
            'logo' => $logo,
        ];
        $html = view('receipt')
            ->with($data)
            ->render();
        // Setup Dompdf
        $options = new Options();
        $options->set('isHtml5ParserEnabled', true);
        $options->set('isRemoteEnabled', true);
        $dompdf = new Dompdf($options);
        // Load HTML content
        $dompdf->loadHtml($html);

        $dompdf->setPaper('A4', 'portrait');

        $dompdf->render();
        return $dompdf->stream('document.pdf');
    }

    public function pdf_download($paymentid)
    {
        $payment_id = $paymentid;
        $paymenthistory = Payment::where('id', $payment_id)->first();
        $student_id = $paymenthistory['student_id'];
        $date = $paymenthistory['date'];
        $payment_type = $paymenthistory['payment_type'];
        $amount = $paymenthistory['paid_amount'];
        $package_id = $paymenthistory['package_id'];

        $student_name = User::find($student_id)->name;
        $student_phone = User::find($student_id)->phone;
        $packagedetails = Package::where('pac_id', $package_id)->first(['course_id']);
        $signatute = Signature::find(1)->signature;
        $seal = Signature::find(1)->seal;
        $logo = Signature::find(1)->header;

        $datatax['tax'] = Tax::find(1)->tax;
        $x = 1 + $datatax['tax'] / 100;
        $income = round($amount / $x, 2);
        $taxval = round($amount - $income, 2);

        $data = [
            'receipt_no' => $payment_id,
            'date' => $date,
            'name_student' => ucfirst($student_name),
            'name_phone' => $student_phone,
            'payment_type' => ucfirst($payment_type),
            'amount' => number_format($amount, 2),
            'status' => 'Success',
            'cource' => $packagedetails['course_id'],
            'actual_amount' => number_format($income, 2),
            'cgst' => number_format($taxval / 2, 2),
            'gst' => number_format($taxval / 2, 2),
            'signatute' => $signatute,
            'seal' => $seal,
            'logo' => $logo,
        ];

        $html = view('receipt')
            ->with($data)
            ->render();
        // Setup Dompdf
        $options = new Options();
        $options->set('isHtml5ParserEnabled', true);
        $options->set('isRemoteEnabled', true);
        $dompdf = new Dompdf($options);
        // Load HTML content
        $dompdf->loadHtml($html);

        $dompdf->setPaper('A4', 'portrait');

        $dompdf->render();
        return $dompdf->stream('document.pdf');
    }

    public function add_students($pacid)
    {

        $userId = Auth::user()->id;
        $data['packagedetails'] = Package::where('pac_id', $pacid)->first();
        //$data['collegelist'] = College::where('status', 1)->get();

        //$college_Ids = Collegeupdates::where('sales_id', '=', $userId)->pluck('college_id');
        //$data['collegelist'] = College::whereIn('id', $college_Ids)->where('status', 1)->get();

        $college_Ids = Collegeupdates::where('sales_id', '=', Auth::user()->id)->pluck('college_id');
        $data['collegelist'] = College::whereIn('id', $college_Ids)->where('status', 1)->get();


        $data['title'] = 'Add New Students';
        $data['pacid'] = $pacid;

        return view('add_students', $data);
    }

    public function add_students_registered($pacid)
    {
        $data['packagedetails'] = Package::where('pac_id', $pacid)->first();
        //$data['collegelist'] = College::where('status', 1)->get();


        $college_Ids = Collegeupdates::where('sales_id', '=', Auth::user()->id)->pluck('college_id');
        $data['collegelist'] = College::whereIn('id', $college_Ids)->where('status', 1)->get();




        $data['title'] = 'Add Registered Students';
        $data['pacid'] = $pacid;
        return view('add_students_registered', $data);
    }

    public function sendemail()
    {
        $recipient = 'rajimk.sics@gmail.com'; // Replace with the recipient's email address

        // $subject = 'Dynamic Data Test';
        $dynamicData = ['name' => 'John Doe', 'message' => 'Hello, world!', 'subject' => 'ss', 'email' => 't@gmail.com', 'password' => 'test@123', 'sales_phone' => '345354'];

        //Mail::to($recipient)->send(new SimpleMail($dynamicData));

        return "Email sent successfully!";
    }
    public function admission_revenue($empid = null)
    {
        if (Auth::check()) {
            $data['pac_type'] = 1;

            if (Auth::user()->role == 1) {
                if ($empid != '') {
                    $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                        ->where("sales_payment.package_type", 1)
                        ->where("sales_payment.sales_id", $empid)
                        ->orderby("sales_payment.id", "desc")
                        ->get(['sales_payment.payment', 'sales_payment.tax', 'sales_payment.total', 'sales_payment.date', 'users.name', 'users.student_admissionid']);

                    $name = User::find($empid)->name;
                    $data['empstatus'] = 1;
                    $data['title'] = 'Total Revenue From Regular Admissions';
                    $data['subtitle'] = 'Reports of ' . ucfirst($name);
                    $data['empid'] = $empid;
                } else {
                    $data['report'] = Payment::join('users', 'users.id', '=', 'ev_payment.student_id')
                        ->where("ev_payment.package_type", 1)
                        ->orderby("ev_payment.id", "desc")
                        ->get(['ev_payment.fees_income as payment', 'ev_payment.tax', 'ev_payment.paid_amount as total', 'ev_payment.date', 'users.name', 'users.student_admissionid']);
                    $data['title'] = 'Total Revenue From Regular Admissions';
                    $data['empstatus'] = 0;
                }
                return view('sales_monthlyreport', $data);
            } elseif (Auth::user()->role == 2) {
                $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')

                    ->where("sales_payment.sales_id", Auth::user()->id)
                    ->where("sales_payment.package_type", 1)
                    ->orderby("sales_payment.id", "desc")
                    ->get(['sales_payment.*', 'users.name', 'users.student_admissionid']);

                $data['empstatus'] = 0;

                $data['title'] = 'Total Revenue From  Regular Admissions';

                return view('sales_monthlyreport', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function chrysalis_revenue($empid = null)
    {
        if (Auth::check()) {
            $data['pac_type'] = 2;

            if (Auth::user()->role == 1) {
                if ($empid != '') {
                    $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                        ->where("sales_payment.package_type", 2)
                        ->where("sales_payment.sales_id", $empid)
                        ->orderby("sales_payment.id", "desc")
                        ->get(['sales_payment.*', 'users.name', 'users.student_admissionid']);

                    $name = User::find($empid)->name;
                    $data['empstatus'] = 1;
                    $data['title'] = 'Total Revenue From Chrysalis';
                    $data['subtitle'] = 'Reports of ' . ucfirst($name);
                    $data['empid'] = $empid;
                } else {
                    $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                        ->where("sales_payment.package_type", 2)
                        ->orderby("sales_payment.id", "desc")
                        ->get(['sales_payment.*', 'users.name', 'users.student_admissionid']);

                    $data['title'] = 'Total Revenue From Chrysalis';
                    $data['empstatus'] = 0;
                }

                return view('sales_monthlyreport', $data);
            } elseif (Auth::user()->role == 2) {
                $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                    ->where("sales_payment.package_type", 2)
                    ->where("sales_payment.sales_id", Auth::user()->id)
                    ->orderby("sales_payment.id", "desc")
                    ->get(['sales_payment.*', 'users.name', 'users.student_admissionid']);

                $data['title'] = 'Total Revenue From Chrysalis';

                $data['empstatus'] = 0;

                return view('sales_monthlyreport', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function admission_monthlyreport_employees()
    {
        if (Auth::check()) {
            $data['pac_type'] = 1;
            $arrayVariable = Session::get('array_key');

            if (Auth::user()->role == 1 || Auth::user()->role == 2) {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');
                //$name           =User::find($empid)->name;

                $data1['names'] = User::whereIn('id', $arrayVariable)->pluck('name');

                $keysString = '';
                foreach ($data1['names'] as $names) {
                    $keysString .= $names . ', ';
                }
                $name = rtrim($keysString, ', ');

                $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                    ->where("sales_payment.package_type", 1)
                    ->whereIn("sales_payment.sales_id", $arrayVariable)
                    ->orderby("sales_payment.id", "desc")
                    ->whereBetween('date', [$first_date, $today])
                    ->get(['sales_payment.*', 'users.name', 'users.student_admissionid']);

                $data['title'] = 'Expected Revenue From New Admissions' . ' ' . date('F Y');
                $data['subtitle'] = 'Reports of ' . ucfirst($name);
                $data['url'] = Session::get('fullurl');
                return view('sales_monthlyreport_employee', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function admission_monthlyreport($empid = null)
    {
        if (Auth::check()) {
            $data['pac_type'] = 1;
            if (Auth::user()->role == 1) {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');
                if ($empid != '') {
                    $name = User::find($empid)->name;
                    $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                        ->where("sales_payment.package_type", 1)
                        ->where("sales_payment.sales_id", $empid)
                        ->orderby("sales_payment.id", "desc")
                        ->whereBetween('date', [$first_date, $today])
                        ->get(['sales_payment.*', 'users.name', 'users.student_admissionid']);
                    $data['empstatus'] = 1;
                    $data['title'] = 'Expected Revenue From New Admissions' . ' ' . date('F Y');
                    $data['subtitle'] = 'Reports of ' . ucfirst($name);
                    $data['empid'] = $empid;
                } else {
                    $data['report'] = Payment::join('users', 'users.id', '=', 'ev_payment.student_id')
                        ->where("ev_payment.package_type", 1)
                        ->orderby("ev_payment.id", "desc")

                        ->whereBetween('date', [$first_date, $today])
                        ->get(['ev_payment.fees_income as payment', 'ev_payment.tax', 'ev_payment.paid_amount as total', 'ev_payment.date', 'users.name', 'users.student_admissionid']);
                    $data['title'] = 'Expected Revenue From New Admissions' . ' ' . date('F Y');
                    $data['empstatus'] = 0;
                }

                return view('sales_monthlyreport', $data);
            } elseif (Auth::user()->role == 2) {
                $today = date('Y-m-d');
                $first_date = date('Y-m-01');

                $empid = Auth::user()->id;
                $data['empstatus'] = 0;

                $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'users.id')
                    ->where("sales_payment.package_type", 1)
                    ->whereBetween('date', [$first_date, $today])
                    ->where("sales_payment.sales_id", $empid)
                    ->orderby("sales_payment.id", "desc")
                    ->get(['sales_payment.*', 'users.name', 'ev_student_info.student_admissionid']);

                $data['title'] = 'Expected Revenue From New Admissions' . ' ' . date('F Y');

                return view('sales_monthlyreport', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function chrysalis_monthlyreport($empid = null)
    {
        if (Auth::check()) {
            $today = date('Y-m-d');
            $first_date = date('Y-m-01');

            $data['pac_type'] = 2;

            if (Auth::user()->role == 1) {
                if ($empid != '') {
                    $name = User::find($empid)->name;
                    $data['empstatus'] = 1;
                    $data['title'] = 'Expected Revenue From Chrysalis' . ' ' . date('F Y');
                    $data['subtitle'] = 'Reports of ' . ucfirst($name);
                    $data['empid'] = $empid;

                    $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                        ->where("sales_payment.package_type", 2)
                        ->where("sales_payment.sales_id", $empid)
                        ->orderby("sales_payment.id", "desc")
                        ->whereBetween('date', [$first_date, $today])
                        ->get(['sales_payment.*', 'users.name']);
                } else {
                    $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                        ->where("sales_payment.package_type", 2)
                        ->orderby("sales_payment.id", "desc")
                        ->whereBetween('date', [$first_date, $today])
                        ->get(['sales_payment.*', 'users.name']);

                    $data['empstatus'] = 0;
                    $data['title'] = 'Expected Revenue From Chrysalis' . ' ' . date('F Y');
                }

                return view('sales_monthlyreport', $data);
            } elseif (Auth::user()->role == 2) {
                $empid = Auth::user()->id;
                $data['report'] = Salespayment::join('users', 'users.id', '=', 'sales_payment.student_id')
                    ->where("sales_payment.package_type", 2)
                    ->where("sales_payment.sales_id", $empid)
                    ->orderby("sales_payment.id", "desc")
                    ->whereBetween('date', [$first_date, $today])
                    ->get(['sales_payment.*', 'users.name']);

                $data['title'] = 'Expected Revenue From Chrysalis' . ' ' . date('F Y');
                $data['empstatus'] = 0;

                return view('sales_monthlyreport', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function sales_admission($empid = null)
    {
        if (Auth::check()) {
            $data['pac_type'] = 1;

            if (Auth::user()->role == 1) {
                if (isset($_GET['salesname']) != '') {
                    $data['salesname'] = $_GET['salesname'];
                } else {
                    $data['salesname'] = '';
                }
                if ($empid != '') {
                    $data['monthlyreport'] = Salespackage::join('users', 'users.id', '=', 'sales_package.student_id')

                        ->where("sales_package.sales_id", $empid)
                        ->where("sales_package.package_type", 1)
                        ->orderby("sales_package.id", "desc")
                        ->get(['sales_package.*', 'users.name', 'users.student_admissionid']);
                    $name = User::find($empid)->name;
                    $data['empstatus'] = 1;
                    $data['title'] = 'Admissions';
                    $data['subtitle'] = 'Reports of ' . ucfirst($name);
                    $data['empid'] = $empid;
                } else {
                    $data['sales'] = User::where('role', 2)
                        ->where('status', 1)
                        ->get(['id', 'name']);
                    $data['empstatus'] = 0;
                    $data['monthlyreport'] = Salespackage::join('users', 'users.id', '=', 'sales_package.student_id')

                        ->where(function ($query) {
                            if (!empty($_GET['salesname'])) {
                                $query->where("sales_package.sales_id", $_GET['salesname']);
                            }
                        })
                        ->where("sales_package.package_type", 1)
                        ->orderby("sales_package.id", "desc")
                        ->get(['sales_package.*', 'users.name', 'users.student_admissionid']);

                    $data['title'] = 'Admissions';
                }

                return view('sales_admissionreport', $data);
            } elseif (Auth::user()->role == 2) {
                $data['monthlyreport'] = Salespackage::join('users', 'users.id', '=', 'sales_package.student_id')

                    ->where("sales_package.package_type", 1)
                    ->where("sales_package.sales_id", Auth::user()->id)
                    ->orderby("sales_package.id", "desc")
                    ->get(['sales_package.*', 'users.name', 'users.student_admissionid']);
                $data['title'] = 'Admissions';
                $data['empstatus'] = 0;
                return view('sales_admissionreport', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function sales_chrysalis($empid = null)
    {
        if (Auth::check()) {
            $data['pac_type'] = 2;

            if (Auth::user()->role == 1) {
                if (isset($_GET['salesname']) != '') {
                    $data['salesname'] = $_GET['salesname'];
                } else {
                    $data['salesname'] = '';
                }

                if ($empid != '') {
                    $data['monthlyreport'] = Salespackage::join('users', 'users.id', '=', 'sales_package.student_id')
                        ->where("sales_package.package_type", 2)
                        ->where("sales_package.sales_id", $empid)
                        ->orderby("sales_package.id", "desc")
                        ->get(['sales_package.*', 'users.name', 'users.student_admissionid']);
                    $name = User::find($empid)->name;
                    $data['empstatus'] = 1;
                    $data['title'] = 'Chrysalis';
                    $data['subtitle'] = 'Reports of ' . ucfirst($name);
                    $data['empid'] = $empid;
                } else {
                    $data['sales'] = User::where('role', 2)
                        ->where('status', 1)
                        ->get(['id', 'name']);
                    $data['monthlyreport'] = Salespackage::join('users', 'users.id', '=', 'sales_package.student_id')
                        ->where("sales_package.package_type", 2)
                        ->where(function ($query) {
                            if (!empty($_GET['salesname'])) {
                                $query->where("sales_package.sales_id", $_GET['salesname']);
                            }
                        })
                        ->orderby("sales_package.id", "desc")
                        ->get(['sales_package.*', 'users.name', 'users.student_admissionid']);

                    $data['title'] = 'Chrysalis';
                    $data['empstatus'] = 0;
                }

                return view('sales_admissionreport', $data);
            } elseif (Auth::user()->role == 2) {
                $data['monthlyreport'] = Salespackage::join('users', 'users.id', '=', 'sales_package.student_id')
                    ->where("sales_package.package_type", 2)
                    ->where("sales_package.sales_id", Auth::user()->id)
                    ->orderby("sales_package.id", "desc")
                    ->get(['sales_package.*', 'users.name', 'users.student_admissionid']);

                $data['empstatus'] = 0;

                $data['title'] = 'Chrysalis';

                return view('sales_admissionreport', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function test()
    {
        $data = [
            'receipt_no' => 1,
            'date' => date('d-m-Y'),
            'name_student' => 2,
            'name_phone' => 3,
            'payment_type' => 3,
            'amount' => 4,
            'status' => 'Sucess',
            'cource' => 'dynamic',
            'actual_amount' => 4,
            'cgst' => 4,
            'gst' => 5,
        ];

        // Render the receipt HTML using a Blade view
        //$html = View::make('receipt', $data)->render();

        $html = view('receipt_1')
            ->with($data)
            ->render();

        // Setup Dompdf
        $options = new Options();
        $options->set('isRemoteEnabled', true);
        $options->set('isHtml5ParserEnabled', true);
        $dompdf = new Dompdf($options);

        // Load HTML content
        $dompdf->loadHtml($html);

        // Render PDF (optional settings)
        $dompdf->setPaper('A4', 'portrait');
        //$dompdf->setPaper('auto', 'auto');

        // Render the HTML as PDF
        $dompdf->render();

        $uniqueId = uniqid();

        // Save the generated PDF to a folder
        $pdfContent = $dompdf->output();
        $pdfFilePath = 'pdfs/generated_pdf_' . $uniqueId . '.pdf'; // Path to save PDF file
        Storage::put($pdfFilePath, $pdfContent);

        return response($pdfContent)->header('Content-Type', 'application/pdf');

        // return 'PDF generated and saved successfully at: ' . $pdfFilePath;
    }
    public function payment_old(Request $request)
    {
        $datatax['tax'] = Tax::find(1)->tax;
        $amount = $request->get('amount');
        $payment_type = $request->get('payMethod');
        $studentid = $request->get('student_id');
        $data1['totalfees'] = Oldstudent::find($studentid)->totalfees;
        $data1['paidfees'] = OldPayment::where('oldstudent_id', $studentid)->sum('paid_amount');
        $pendingamount = $data1['totalfees'] - ($data1['paidfees'] + $amount);
        $data['oldstudent_id'] = $studentid;
        $data['paid_amount'] = $amount;
        $data['pending_amount'] = $pendingamount;

        if ($payment_type == 'bank') {
            $data['transaction_id'] = $request->get('trans_id');
        } else {
            $data['transaction_id'] = $request->get('trans_id');
        }

        $data['date'] = date("Y-m-d", strtotime($request->get('oldpay_date')));

        $data['sales_id'] = Auth::user()->id;

        if ($request->hasFile('package_reciept')) {
            $extension = $request->file("package_reciept")->getClientOriginalExtension();
            $destinationPath = public_path('uploads/screenshot'); // upload path
            $prd_image_name = 'screenshot_' . time() . '.' . $extension;
            $request->file("package_reciept")->move($destinationPath, $prd_image_name);
            $imageName = $prd_image_name;
        } else {
            $imageName = '';
        }

        $x = 1 + $datatax['tax'] / 100;
        $income_sales = $amount / 2 / $x;
        $income = round($amount / $x, 2);
        $taxval = round($amount - $income, 2);

        $data['fees_income'] = $income;
        $data['tax'] = $taxval;
        $data['payment_type'] = $payment_type;
        $data['screenshot'] = $imageName;
        OldPayment::insert($data);
        Session::flash('success', 'Sucessfully Paid');
        return redirect('/oldpayment_history/' . $studentid);
    }

    public function payment_package(Request $request)
    {
        $datatax['tax'] = Tax::find(1)->tax;
        $referenece_type = $request->get('reference_type');
        $amount = $request->get('amount');
        $payment_type = $request->get('payMethod');
        $student_id = $request->get('student_id');
        $studentpackage_id = $request->get('studentpackage_id');
        
        $pac_type = $request->get('pac_type');
        $studentpackage = Studentpackage::join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
            ->where('ev_student_package.id', $studentpackage_id)
            ->first(['ev_student_package.totalfees_afterreduction', 'ev_student_package.package_id']);

        $fullamount = $studentpackage['totalfees_afterreduction'];
        $pac_id = $studentpackage['package_id'];

        $date = date("Y-m-d", strtotime($request->get('oldpay_date')));

        if ($payment_type == 'bank') {
            $transaction_id = $request->get('trans_id');
        } else {
            $transaction_id = '';
        }

        $sumofpaidamount = round(
            Payment::where('student_id', $student_id)
                ->where('package_type', $pac_type)
                ->where('package_id', $pac_id)
                ->sum('paid_amount')
        );
        $pendingamount = $fullamount - ($sumofpaidamount + $amount);
        $total = $amount;
        if ($request->hasFile('package_reciept')) {
            $extension = $request->file("package_reciept")->getClientOriginalExtension();
            $destinationPath = public_path('uploads/screenshot'); // upload path
            $prd_image_name = 'screenshot_' . time() . '.' . $extension;
            $request->file("package_reciept")->move($destinationPath, $prd_image_name);
            $imageName = $prd_image_name;
        } else {
            $imageName = '';
        }
        if ($pac_type == 1) {
            $splitup = Studentfeessplit::where('student_id', $student_id)
                ->where('package_type', 1)
                ->where('package_id', $pac_id)
                ->get();

            foreach ($splitup as $split) {
                $pendingfees = $split->pendingfees;
                if ($pendingfees != 0) {
                    if ($total > $split->pendingfees) {
                        Studentfeessplit::where('id', $split->id)->update(['paidfees' => $split->fees, 'pendingfees' => 0]);
                        $total -= $split->pendingfees;
                    } else {
                        if ($total <= $split->pendingfees) {
                            Studentfeessplit::where('id', $split->id)->update(['paidfees' => $split->paidfees + $total, 'pendingfees' => $split->pendingfees - ($split->pendingfeespaidfees + $total)]);
                            $total = 0;
                        }
                    }
                }
            }
        }
        if ($referenece_type == 1) {
            //payment transfer to sales

            $x = 1 + $datatax['tax'] / 100;
            $income_sales = $amount / 2 / $x;
            $income_ref = $amount / 2 / $x;
            $taxval_sale = $amount / 2 - $income_sales;
            $taxval_ref = $amount / 2 - $income_ref;
            $reference_id = $request->get('reference_id');

            $income = round($amount / $x);
            $taxval = round($amount - $income);
            $payment_id = Payment::insertGetId([
                'transaction_id' => $transaction_id,
                'package_id' => $pac_id,
                'student_id' => $student_id,
                'package_type' => $pac_type,
                'screenshot' => $imageName,
                'payment_type' => $payment_type,
                'paid_amount' => $amount,
                'pending_amount' => $pendingamount,
                'date' => $date,
                'fees_income' => round($income),
                'tax' => round($taxval),
            ]);

            Salespayment::insert([
                'student_id' => $student_id,
                'package_type' => $pac_type,
                'sales_id' => Auth::user()->id,
                'payment' => round($income_sales),
                'tax' => round($taxval_sale),
                'total' => round($amount / 2),
                'date' => $date,
                'package_id' => $pac_id,
                'payment_id' => $payment_id,
            ]);
            Salespayment::insert([
                'student_id' => $student_id,
                'package_type' => $pac_type,
                'sales_id' => $reference_id,
                'payment' => round($income_ref),
                'tax' => round($taxval_ref),
                'total' => round($amount / 2),
                'date' => $date,
                'package_id' => $pac_id,
                'payment_id' => $payment_id,
            ]);
        } else {
            $x = 1 + $datatax['tax'] / 100;
            $income = round($amount / $x, 2);
            $taxval = round($amount - $income, 2);
            $payment_id = Payment::insertGetId([
                'transaction_id' => $transaction_id,
                'package_id' => $pac_id,
                'student_id' => $student_id,
                'package_type' => $pac_type,
                'screenshot' => $imageName,
                'payment_type' => $payment_type,
                'paid_amount' => $amount,
                'pending_amount' => $pendingamount,
                'date' => $date,
                'fees_income' => round($income),
                'tax' => round($taxval),
            ]);
            Salespayment::insert([
                'student_id' => $student_id,
                'package_type' => $pac_type,
                'sales_id' => Auth::user()->id,
                'payment' => round($income),
                'tax' => round($taxval),
                'total' => round($amount),
                'date' => $date,
                'package_id' => $pac_id,
                'payment_id' => $payment_id,
            ]);
        }

        $student_name = User::find($student_id)->name;
        $student_email = User::find($student_id)->email;
        $recipient = $student_email; // Replace with the recipient's email address
        $sales_phone = User::find(Auth::user()->id)->phone;
        $dynamicData = ['name' => ucfirst($student_name), 'subject' => 'Eva Portal Login Information', 'email' => $student_email, 'sales_phone' => $sales_phone];

        // Mail::to($recipient)->send(new PayMail($dynamicData));
        Session::flash('success', 'Sucessfully Paid');
        return redirect('/payment_history_package/' . $studentpackage_id);
    }

    public function pdfgeneration(Request $request)
    {
        $student_id = $request->get('student_id');
        $referenece_type = $request->get('reference_type');
        $amount = $request->get('amount');
        $datatax['tax'] = Tax::find(1)->tax;
        $payment_type = $request->get('payMethod');

        $studentpackage = Studentpackage::join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
            ->where('ev_student_package.student_id', $student_id)
            ->first(['totalfees_afterreduction', 'course_id']);

        $fullamount = $studentpackage['totalfees_afterreduction'];

        $sumofpaidamount = round(Payment::where('student_id', $student_id)->sum('paid_amount'));
        $pendingamount = $fullamount - ($sumofpaidamount + $amount);
        $splitup = Studentfeessplit::where('student_id', $student_id)->get();
        $total = $amount;
        $balance = 0;
        $image = $request->file('package_reciept');

        if ($request->hasFile('package_reciept')) {
            $extension = $request->file("package_reciept")->getClientOriginalExtension();
            $destinationPath = public_path('uploads/screenshot'); // upload path
            $prd_image_name = 'screenshot_' . time() . '.' . $extension;
            $request->file("package_reciept")->move($destinationPath, $prd_image_name);
            $imageName = $prd_image_name;
        } else {
            $imageName = '';
        }

        foreach ($splitup as $split) {
            $pendingfees = $split->pendingfees;
            if ($pendingfees != 0) {
                if ($total > $split->pendingfees) {
                    Studentfeessplit::where('id', $split->id)->update(['paidfees' => $split->fees, 'pendingfees' => 0]);
                    $total -= $split->pendingfees;
                } else {
                    if ($total <= $split->pendingfees) {
                        Studentfeessplit::where('id', $split->id)->update(['paidfees' => $split->paidfees + $total, 'pendingfees' => $split->pendingfees - ($split->pendingfeespaidfees + $total)]);
                        $total = 0;
                    }
                }
            }
        }

        if ($referenece_type == 1) {
            //payment transfer to sales

            $x = 1 + $datatax['tax'] / 100;
            $income_sales = $amount / 2 / $x;
            $income_ref = $amount / 2 / $x;
            $taxval_sale = $amount / 2 - $income_sales;
            $taxval_ref = $amount / 2 - $income_ref;
            $reference_id = $request->get('reference_id');

            $income = round($amount / $x);
            $taxval = round($amount - $income);
            $payment_id = Payment::insertGetId([
                'student_id' => $student_id,
                'package_type' => 1,
                'screenshot' => $imageName,
                'payment_type' => $payment_type,
                'paid_amount' => $amount,
                'pending_amount' => $pendingamount,
                'date' => date('d-m-Y'),
                'fees_income' => round($income),
                'tax' => round($taxval),
            ]);
            Salespayment::insert([
                'sales_id' => Auth::user()->id,
                'package_type' => 1,
                'payment' => round($income_sales),
                'tax' => round($taxval_sale),
                'total' => round($amount / 2),
                'student_id' => $student_id,
                'payment_id' => $payment_id,
                'date' => date('Y-m-d'),
            ]);
            Salespayment::insert([
                'sales_id' => $reference_id,
                'package_type' => 1,
                'payment' => round($income_ref),
                'tax' => round($taxval_ref),
                'total' => round($amount / 2),
                'student_id' => $student_id,
                'payment_id' => $payment_id,
                'date' => date('Y-m-d'),
            ]);
        } else {
            $x = 1 + $datatax['tax'] / 100;
            $income = round($amount / $x, 2);
            $taxval = round($amount - $income, 2);
            $payment_id = Payment::insertGetId([
                'student_id' => $student_id,
                'package_type' => 1,
                'screenshot' => $imageName,
                'payment_type' => $payment_type,
                'paid_amount' => $amount,
                'pending_amount' => $pendingamount,
                'date' => date('d-m-Y'),
                'fees_income' => round($income),
                'tax' => round($taxval),
            ]);
            Salespayment::insert([
                'sales_id' => Auth::user()->id,
                'package_type' => 1,
                'payment' => round($income),
                'tax' => round($taxval),
                'total' => round($amount),
                'student_id' => $student_id,
                'payment_id' => $payment_id,
                'date' => date('Y-m-d'),
            ]);
        }

        Payment::where('id', $payment_id)->update(['file_path' => $pdfFilePath]);
        Session::flash('success', 'Sucessfully Paid');
        return redirect('/payment_history/' . $student_id);
    }

    public function reference_details(Request $request)
    {
        $referenece = $request->get('referenece');

        if ($referenece == 1) {
            $data['list'] = User::where('role', 2)
                ->where('status', 1)
                ->where('id', '!=', Auth::user()->id)
                ->get(['id', 'name']);
        } elseif ($referenece == 2) {
            $data['list'] = Employee::where('status', 1)->get(['id', 'emp_name as name']);
        } elseif ($referenece == 3) {
            $data['list'] = Othereference::get(['id', 'name']);
        } else {
            $data['list'] = '';
        }

        echo json_encode($data);
    }
    public function payment_history($studentid)
    {
        $data['payment_history'] = Payment::orderBy('id', 'desc')
            ->where('student_id', $studentid)
            ->get();
        $data['paidfees'] = Payment::where('student_id', $studentid)->sum('paid_amount');
        $data['package_details'] = Studentpackage::join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
            ->where('ev_student_package.student_id', $studentid)
            ->first(['ev_package.total', 'ev_student_package.*']);

        $data['student_id'] = $studentid;
        return view('payment_history', $data);
    }

    public function packdetails(Request $request)
    {
        $pactype = $request->type;
        $data['package'] = Package::where('status', 1)
            ->where('pac_type', $pactype)
            ->get();

        echo json_encode($data);
    }

    public function package_details($studentpackage_id)
    {
        $data['package_details'] = Studentpackage::join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
            ->where('ev_student_package.id', $studentpackage_id)
            ->first(['ev_package.total', 'ev_student_package.*', 'ev_package.pac_name']);

        $studentid = $data['package_details']->student_id;
        $pac_id = $data['package_details']->package_id;
        $pac_type = $data['package_details']->package_type;
        $data['pac_name'] = $data['package_details']->pac_name;
        $data['name'] = User::find($studentid)->name;

        $data['pac_type'] = $pac_type;
        $data['pac_id'] = $pac_id;

        $result = Payment::orderBy('id', 'desc')
            ->where('student_id', $studentid)
            ->where('package_type', $pac_type)
            ->where('package_id', $pac_id)
            ->first();

        if ($result) {
            $data['pending_amount'] = $result->pending_amount;
        } else {
            // Handle the case where no payment record is found
            $data['pending_amount'] = null; // or any default value you want
        }

        $data['packagedetails'] = Studentpackage::join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
            ->where('ev_student_package.id', $studentpackage_id)
            ->first(['ev_student_package.*', 'ev_package.pac_name', 'ev_package.duration', 'ev_package.fee', 'ev_package.tax as maintax', 'ev_package.total']);

        $data['pac_type'] = $pac_type;
        $data['studentlogin'] = User::where('id', $studentid)->first(['email', 'password_text']);
        $data['feessplitup'] = Studentfeessplit::where('student_id', $studentid)->get();
        return view('student_packagedetails', $data);
    }

    public function emailexist(Request $request)
    {
        $email = $request->get('email');
        $count = User::where('email', $email)->count();
        if ($count == 0) {
            echo 1;
        } else {
            echo 0;
        }
    }
    public function random_string($length)
    {
        $str = random_bytes($length);
        $str = base64_encode($str);
        $str = str_replace(["+", "/", "="], "", $str);
        $str = substr($str, 0, $length);
        return $str;
    }

    public function pack_student(Request $request)
    {
        $datatax['tax'] = Tax::find(1)->tax;
        $type = $request->get('type');
        $student_id = $request->get('student_id');
        $finalamount = $request->get('finalamount');
        $data['package_id'] = $request->get('packname');

        if ($type == 'talento' || $type == 'contact' || $type == 'all') {
            $count = Studentpackage::where('student_id', $student_id)
                ->where('package_id', $data['package_id'])
                ->count();

            if ($count == 0) {
                if (isset($_POST['reductioncheck'])) {
                    $data['reduction_check'] = 1;
                    $data['reduction_amount'] = $request->get('reduction_amount');
                    $data['reduced_fees'] = $request->get('reducedfees');
                    $data['tax'] = $request->get('reducedtax');
                    $data['totalfees_afterreduction'] = $request->get('after_reduction');
                    $data['ineffect_total_red'] = $request->get('ineffect_offered');
                    $data['referalcode'] = $this->random_string(6);
                } else {
                    $data['totalfees_afterreduction'] = $finalamount;
                }

                $data['registration_fees'] = $request->get('regfees');
                $data['joining_date'] = date("Y-m-d", strtotime($request->get('oldpay_date')));

                $data['due_date_first'] = $request->get('inst_red');
                $data['student_id'] = $student_id;
                $data['reference_type'] = $request->get('referenece');
                $data['refence_id'] = $request->get('referenece_list');
                $data['package_type'] = 1;
                $data['sales_id'] = Auth::user()->id;

                Studentpackage::insert($data);

                //2.Student Package table insert*******************************************

                if ($data['reference_type'] == 1 && $data['refence_id'] != '') {
                    //payment transfer to sales
                    $datasales['sales_id'] = Auth::user()->id;
                    $datasales['student_id'] = $student_id;
                    $datasales['package_fullamount'] = $data['totalfees_afterreduction'] / 2;
                    $x1 = 1 + $datatax['tax'] / 100;
                    $datasales['package_amount'] = $datasales['package_fullamount'] / $x1;
                    $datasales['package_tax'] = $datasales['package_fullamount'] - $datasales['package_amount'];
                    $datasales['package_type'] = 1;
                    $datasales['package_id'] = $request->get('packname');
                    $datasales['joining_date'] = date("Y-m-d", strtotime($request->get('oldpay_date')));

                    Salespackage::insert($datasales);
                    //payment transfer to reference
                    $dataref['sales_id'] = $data['refence_id'];
                    $dataref['student_id'] = $student_id;
                    $dataref['package_fullamount'] = $data['totalfees_afterreduction'] / 2;
                    $dataref['package_amount'] = $dataref['package_fullamount'] / $x1;
                    $dataref['package_tax'] = $dataref['package_fullamount'] - $dataref['package_amount'];
                    $dataref['package_type'] = 1;
                    $dataref['package_id'] = $request->get('packname');
                    $dataref['joining_date'] = date("Y-m-d", strtotime($request->get('oldpay_date')));
                    Salespackage::insert($dataref);
                } else {
                    $datasales['sales_id'] = Auth::user()->id;
                    $datasales['student_id'] = $student_id;
                    $datasales['package_fullamount'] = $data['totalfees_afterreduction'];
                    $x1 = 1 + $datatax['tax'] / 100;
                    $datasales['package_amount'] = $data['totalfees_afterreduction'] / $x1;
                    $datasales['package_tax'] = $datasales['package_fullamount'] - $datasales['package_amount'];
                    $datasales['package_type'] = 1;
                    $datasales['package_id'] = $request->get('packname');
                    $datasales['joining_date'] = date("Y-m-d", strtotime($request->get('oldpay_date')));
                    Salespackage::insert($datasales);
                }

                //3.Sales Package table insert*******************************************

                $regfees = $request->get('regfees');
                $payment_type = $request->get('payMethod_bank');
                $payment_date = date("Y-m-d", strtotime($request->get('paydate')));

                if ($payment_type == 'bank') {
                    $transaction_id = $request->get('trans_id');
                } else {
                    $transaction_id = '';
                }

                if ($request->hasFile('package_reciept')) {
                    $extension = $request->file("package_reciept")->getClientOriginalExtension();
                    $destinationPath = public_path('uploads/screenshot'); // upload path
                    $prd_image_name = 'screenshot_' . time() . '.' . $extension;
                    $request->file("package_reciept")->move($destinationPath, $prd_image_name);
                    $imageName = $prd_image_name;
                } else {
                    $imageName = '';
                }

                $monthlyfees = $request->get('monthlyfees');
                $splitdate = $request->get('splitdate');
                $total = $regfees;
                $balance = 0;
                $sum = 0;
                for ($i = 0; $i < count($monthlyfees); $i++) {
                    $monthlyfees1 = $monthlyfees[$i];
                    $splitdate1 = $splitdate[$i];

                    if ($total > $monthlyfees1) {
                        if ($i == 0) {
                            $paidffes = $monthlyfees1;
                            $sum += $paidffes;
                            $balance = $total - $monthlyfees1;
                            $pendingfees = 0;
                        } else {
                            if ($i == count($monthlyfees) - 1) {
                                $paidffes = $balance;

                                $pendingfees = $monthlyfees1 - $balance;

                                if ($pendingfees > 0) {
                                    $balance = 0;
                                }
                            } else {
                                if ($balance > $monthlyfees1) {
                                    $paidffes = $monthlyfees1;
                                    $sum += $paidffes;
                                    $pendingfees = 0;
                                    $balance = $balance - $monthlyfees1;
                                } else {
                                    $paidffes = $balance;
                                    $sum += $paidffes;
                                    $pendingfees = $monthlyfees1 - $balance;

                                    if ($pendingfees > 0) {
                                        $balance = 0;
                                    }
                                }
                            }
                        }
                    } else {
                        if ($i == 0) {
                            $paidffes = $request->get('regfees');
                            $pendingfees = $monthlyfees1 - $paidffes;
                            $sum += $paidffes;
                        } else {
                            $paidffes = 0;
                            $pendingfees = $monthlyfees1;
                            $sum += $paidffes;
                        }
                    }

                    Studentfeessplit::insert(['package_type' => 1, 'package_id' => $data['package_id'], 'student_id' => $student_id, 'fees' => $monthlyfees1, 'date' => $splitdate1, 'paidfees' => $paidffes, 'pendingfees' => $pendingfees]);
                }

                //4.Student Fees  splitup  table insert*******************************************

                if ($data['reference_type'] == 1 && $data['refence_id'] != '') {
                    $x = 1 + $datatax['tax'] / 100;

                    $income_sales = $regfees / 2 / $x;
                    $income_ref = $regfees / 2 / $x;

                    $taxval_sale = $regfees / 2 - $income_sales;

                    $taxval_ref = $regfees / 2 - $income_ref;

                    $income = $regfees / $x;
                    $taxval = $regfees - $income;
                    $payment_id = Payment::insertGetId([
                        'package_id' => $data['package_id'],
                        'payment_type' => $payment_type,
                        'screenshot' => $imageName,
                        'transaction_id' => $transaction_id,
                        'student_id' => $student_id,
                        'package_type' => 1,
                        'paid_amount' => $total,
                        'pending_amount' => $finalamount - $total,
                        'date' => $payment_date,
                        'fees_income' => round($income),
                        'tax' => round($taxval),
                    ]);

                    Salespayment::insert([
                        'sales_id' => Auth::user()->id,
                        'package_id' => $data['package_id'],
                        'package_type' => 1,
                        'payment' => round($income_sales),
                        'tax' => round($taxval_sale),
                        'total' => round($regfees / 2),
                        'student_id' => $student_id,
                        'payment_id' => $payment_id,
                        'date' => $payment_date,
                    ]);
                    Salespayment::insert([
                        'sales_id' => $data['refence_id'],
                        'package_id' => $data['package_id'],
                        'package_type' => 1,
                        'payment' => round($income_ref),
                        'tax' => round($taxval_ref),
                        'total' => round($regfees / 2),
                        'student_id' => $student_id,
                        'payment_id' => $payment_id,
                        'date' => $payment_date,
                    ]);
                } else {
                    $x = 1 + $datatax['tax'] / 100;
                    $income = $regfees / $x;
                    $taxval = $regfees - $income;
                    $payment_id = Payment::insertGetId([
                        'package_id' => $data['package_id'],
                        'student_id' => $student_id,
                        'payment_type' => $payment_type,
                        'screenshot' => $imageName,
                        'transaction_id' => $transaction_id,
                        'package_type' => 1,
                        'paid_amount' => $total,
                        'pending_amount' => $finalamount - $total,
                        'date' => $payment_date,
                        'fees_income' => round($income),
                        'tax' => round($taxval),
                    ]);
                    Salespayment::insert([
                        'sales_id' => Auth::user()->id,
                        'package_id' => $data['package_id'],
                        'package_type' => 1,
                        'payment' => round($income),
                        'tax' => round($taxval),
                        'total' => round($regfees),
                        'student_id' => $student_id,
                        'payment_id' => $payment_id,
                        'date' => $payment_date,
                    ]);
                }

                //5.Sales- Payment Table  insert*******************************************

                if ($type == 'contact') {
                    $contactid = $request->get('contactid');
                    Contacts::where('id', $contactid)->update(['package_add' => 1]);

                    Session::flash('success', 'Sucessfully Assigned Package');
                    return redirect('/students_list/' . $data['package_id']);
                }
                if ($type == 'talento') {
                    $talentoid = $request->get('talentoid');
                    Talento::where('id', $talentoid)->update(['status' => 1]);

                    Session::flash('success', 'Sucessfully Assigned Package');
                    return redirect('/students_list/' . $data['package_id']);
                }
                if ($type == 'all') {
                    Session::flash('success', 'Sucessfully Assigned Package');
                    return redirect('/students_list/' . $data['package_id']);
                }
            } else {
                if ($type == 'contact') {
                    Session::flash('errormessage', 'Already Assigned this package');
                    return redirect('/closed_contacts');
                }
                if ($type == 'talento') {
                    Session::flash('errormessage', 'Already Assigned this package');
                    return redirect('/manage_talento_registration');
                }
                if ($type == 'all') {
                    Session::flash('errormessage', 'Already Assigned this package');
                    return redirect('/all_students');
                }
            }
        } else {
            return redirect('/acessdenied');
        }
    }

    //6.Contacts- updated Table  *******************************************
    //$student_name = User::find($student_id)->name;
    //$recipient = $request->get('email'); // Replace with the recipient's email address

    //$sales_phone = User::find(Auth::user()->id)->phone;

    //$dynamicData = ['name' => ucfirst($student_name), 'subject' => 'Eva Portal Login Information', 'email' => $request->get('email'), 'password' => $request->get('password'), 'sales_phone' => $sales_phone];

    //Mail::to($recipient)->send(new SimpleMail($dynamicData));

    //7.Email-  Send  *******************************************

    public function select_package($student_id, $type)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 2) {
                $data['type'] = $type;
                $data['student_id'] = $student_id;

                if ($type == 'talento' || $type == 'contact' || $type == 'all') {
                    if ($type == 'talento') {
                        $user = $student_id;
                        $talento = Talento::where('user_id', $user)->first();

                        $type = $talento['registration_type'];
                        $data['closed_date'] = date('Y-m-d');
                        $data['talentoid'] = $talento['id'];
                    }
                    if ($type == 'contact') {
                        $contactid = User::find($student_id)->contact_id;
                        $data['closed_date'] = Contacts::find($contactid)->closed_date;
                        $data['contactid'] = $contactid;
                    }
                    if ($type == 'all') {
                        $data['closed_date'] = date('Y-m-d');
                    }
                    $data['default_finstall'] = date('Y-m-d', strtotime("+10 days"));

                    // Subquery to get package_ids for a specific student with package_type 1
                    $subquery = Studentpackage::where('student_id', $student_id)
                        ->where('package_type', 1)
                        ->pluck('package_id'); // Use pluck to get a collection of package IDs

                    // Main query to get all packages not in the subquery list
                    $data['package'] = Package::leftJoin('ev_technology', 'ev_technology.id', '=', 'ev_package.tech_id')
                        ->where('ev_package.pac_type', 1)
                        ->where('ev_technology.status', 1)
                        ->where('ev_package.status', 1)
                        ->whereNotIn('ev_package.pac_id', $subquery) // Pass the collection to whereNotIn
                        ->select('ev_package.pac_id', 'ev_package.pac_name')
                        ->get(); // Make sure to call get() to execute the query

                    $data['title'] = 'Regular Package';

                    return view('selectpackage', $data);
                } else {
                    return redirect('/acessdenied');
                }
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function assign_package(Request $request)
    {
        $contact_id = $request->get("contact_id");
        $package_id = $request->get("packname");
    }

    public function packagedetails(Request $request)
    {
        $package_id = $request->get("package");
        $data['package_details'] = Package::where('pac_id', $package_id)->first();
        echo json_encode($data);
    }
    public function checkno(Request $request)
    {
        $no = $request->get("no");

        $count = Contacts::orwhere('contact1', $no)
            ->orwhere('contact2', $no)
            ->count();
        if ($count == 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function closedcontacts(Request $request)
    {
        $contactid = $request->get("contactid");
        $college_id = Contacts::find($contactid)->college;
        $student_admissionid = Studentinfo::max('student_admissionid') + 1;

        $datauser['email'] = $request->get('email');
        $datauser['password'] = Hash::make($request->password);
        $datauser['password_text'] = $request->get("password");
        $datauser['name'] = Contacts::find($contactid)->name;
        $datauser['phone'] = Contacts::find($contactid)->contact1;
        $datauser['phone2'] = Contacts::find($contactid)->contact2;
        $datauser['contact_id'] = $request->get("contactid");
        $datauser['created_at'] = date('Y-m-d');
        $datauser['role'] = 5;

        $insertid = User::insertGetId($datauser);
        Studentinfo::insert(['student_id' => $insertid, 'student_admissionid' => $student_admissionid, 'college_id' => $college_id, 'sales_id' => Auth::user()->id, 'created_at' => date("Y-m-d")]);

        $data1['status'] = 2;
        $data1['closed_date'] = date("Y-m-d");
        Contacts::where('id', $contactid)->update($data1);
    }

    public function followup_history(Request $request)
    {
        $id = $request->get("id");
        $data['history'] = Followup::where('contact_id', $id)
            ->orderBy("id", "desc")
            ->get();
        echo json_encode($data);
    }
    public function updatefollow(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 2) {
                $data2['follow_date'] = $request->get("folloupdate_edit");
                $data2['comments'] = $request->get("comments_edit");
                $data2['contact_id'] = $request->get("contactid_update");

                $data1['followup_date'] = date('Y-m-d', strtotime($request->get("folloupdate_edit")));

                $data1['followup_comments'] = $request->get("comments_edit");

                Followup::insert($data2);

                Contacts::where('id', $data2['contact_id'])->update($data1);

                Session::flash('success', 'Sucessfully Updated');
                return redirect('/followup');
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function followupsave(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 2) {
                $data['follow_date'] = $request->get("folloupdate");
                $data['comments'] = $request->get("comments");
                $data['contact_id'] = $request->get("contactid");
                Followup::insert($data);

                $data1['status'] = 1;
                $data1['followup_date'] = date('Y-m-d', strtotime($data['follow_date']));

                $data1['followup_comments'] = $data['comments'];

                Contacts::where('id', $data['contact_id'])->update($data1);
                Session::flash('success', 'Sucessfully Saved');
                return redirect('/outboundcalls');
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }
    public function contact_details(Request $request)
    {
        $id = $request->get("id");
        $type = $request->get("type");

        if($type=='contact'){


            $data['contactdetails'] = Contacts::leftjoin("ev_source", "ev_source.id", "=", "ev_st_contacts.source")
                                            ->leftjoin("ev_college", "ev_college.id", "=", "ev_st_contacts.college")
                                            ->leftjoin("ev_qualification", "ev_qualification.id", "=", "ev_st_contacts.qualification")
                                            ->where('ev_st_contacts.id', $id)
                                            ->first(['ev_st_contacts.*', 'ev_source.source', 'ev_qualification.qualification', 'ev_college.college','ev_st_contacts.id as contact_id','ev_st_contacts.followup_date']);

        }
        else{

        
        $data['contactdetails'] = Contacts::join('users', 'users.contact_id', '=', 'ev_st_contacts.id')
                                            ->leftjoin("ev_source", "ev_source.id", "=", "ev_st_contacts.source")
                                            ->leftjoin("ev_college", "ev_college.id", "=", "ev_st_contacts.college")
                                            ->leftjoin("ev_qualification", "ev_qualification.id", "=", "ev_st_contacts.qualification")
                                            ->where('ev_st_contacts.id', $id)
                                            ->first(['users.*', 'ev_source.source', 'ev_qualification.qualification', 'ev_college.college','ev_st_contacts.id as contact_id','ev_st_contacts.followup_date']);
        }
        echo json_encode($data);
    }
    public function closed_contacts(Request $request)
    {
        // Check if the user is authenticated and has the correct role
        if (!Auth::check() || Auth::user()->role != 2) {
            return redirect('/accessdenied');
        }

        // Get year and month from query parameters with default values
        $year = $request->query('year', Carbon::now()->year);
        $month = $request->query('month', Carbon::now()->format('m'));

        // Validate year and month
        if (!checkdate($month, 1, $year)) {
            return redirect('/accessdenied');
        }

        // Calculate start and end of the month
        $startOfMonth = Carbon::create($year, $month, 1)
            ->startOfMonth()
            ->format('Y-m-d');
        $endOfMonth = Carbon::create($year, $month, 1)
            ->endOfMonth()
            ->format('Y-m-d');

        // Fetch the closed contacts
        $contactlist = Contacts::join('users', 'users.contact_id', '=', 'ev_st_contacts.id')
            ->where('ev_st_contacts.contacts_assigned', Auth::user()->id)
            ->whereBetween('ev_st_contacts.closed_date', [$startOfMonth, $endOfMonth])
            ->where('ev_st_contacts.status', 2)
            ->orderBy('ev_st_contacts.id', 'desc')
            ->get(['users.*', 'ev_st_contacts.closed_date', 'ev_st_contacts.package_add', 'ev_st_contacts.id as contact_id']);

        // Prepare the data for the view
        $data = [
            'title' => 'Closed Contacts',
            'contactlist' => $contactlist,
            'years' => CommonController::generateYearRange(Carbon::now()->year - 10),
            'months' => CommonController::generateMonthOptions(),
            'currentYear' => $year,
            'currentMonth' => $month,
        ];

        // Return the view with data
        return view('closed_contacts', $data);
    }

    public function followup(Request $request)
    {
        if (!Auth::check() || Auth::user()->role != 2) {
            return redirect('/accessdenied');
        }

        // Get year and month from query parameters with default values
        $year = $request->query('year', Carbon::now()->year);
        $month = $request->query('month', Carbon::now()->format('m'));

        // Validate year and month
        if (!checkdate($month, 1, $year)) {
            // If invalid date, redirect or handle error as appropriate
            return redirect('/accessdenied');
        }

        // Calculate start and end of the month
        $startOfMonth = Carbon::create($year, $month, 1)
            ->startOfMonth()
            ->format('Y-m-d');
        $endOfMonth = Carbon::create($year, $month, 1)
            ->endOfMonth()
            ->format('Y-m-d');

        // Fetch data from the database
        $contactlist = Contacts::where('contacts_assigned', Auth::user()->id)
            ->where('status', 1)
            ->whereBetween('followup_date', [$startOfMonth, $endOfMonth])
            ->orderBy('id', 'desc')
            ->get();

        // Prepare data for the view
        $data = [
            'title' => 'Follow Up',
            'contactlist' => $contactlist,
            'years' => CommonController::generateYearRange(date('Y') - 10),
            'months' => CommonController::generateMonthOptions(),
            'currentYear' => $year,
            'currentMonth' => $month,
        ];

        return view('followup', $data);
    }

    public function outboundcalls(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        if (Auth::user()->role != 2) {
            return redirect('/accessdenied');
        }

        // Get year and month from query parameters with default values
        $year = $request->query('year', Carbon::now()->year);
        $month = $request->query('month', Carbon::now()->format('m'));

        // Validate year and month
        if (!checkdate($month, 1, $year)) {
            // If invalid date, redirect or handle error as appropriate
            return redirect('/accessdenied');
        }

        // Calculate start and end of the month
        $startOfMonth = Carbon::create($year, $month, 1)
            ->startOfMonth()
            ->format('Y-m-d');
        $endOfMonth = Carbon::create($year, $month, 1)
            ->endOfMonth()
            ->format('Y-m-d');

        // Fetch data from the database





        $contactlist = Contacts::where('contacts_assigned', Auth::user()->id)
                        ->whereBetween('added_date', [$startOfMonth, $endOfMonth])
                        ->where('status', 0)
                        ->orderBy('id', 'desc')
                        ->get();


 



        // Prepare data for the view
        $data = [
            'title' => 'Outbound calls',
            'contactlist' => $contactlist,
            'years' => CommonController::generateYearRange(date('Y') - 10),
            'months' => CommonController::generateMonthOptions(),
            'currentYear' => $year,
            'currentMonth' => $month,
        ];

        return view('outboundcalls', $data);
    }

    public function add_contacts()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 2) {
                $data['sourcelist'] = Source::where('status', 1)
                    ->orderby('id', 'desc')
                    ->get();
                $data['collegelist'] = College::where('status', 1)
                    ->orderby('id', 'desc')
                    ->get();
                $data['qualification'] = Qualification::where('status', 1)
                    ->orderby('id', 'desc')
                    ->get();
                $data['title'] = 'Add Contacts';
                return view('add_contacts', $data);
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }
    public function save_contacts(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 2) {
                $data['name'] = $request->get("name");
                $data['contact1'] = $request->get("cno1");
                $data['contact2'] = $request->get("cno2");
                $data['source'] = $request->get("source");
                $data['email'] = $request->get("email");
                $data['college'] = $request->get("college");
                $data['qualification'] = $request->get("qualification");
                $data['year'] = $request->get("year");
                $data['added_user'] = Auth::user()->id;
                $data['contacts_assigned'] = Auth::user()->id;
                $data['added_date'] = date('Y-m-d');

                $count1 = Contacts::orwhere('contact1', $data['contact1'])
                    ->orwhere('contact2', $data['contact1'])
                    ->count();

                $count2 = Oldstudent::where('contact_no', $data['contact1'])->count();
                $count3 = User::where('phone', $data['contact1'])->count();

                if ($request->get("email") == '') {
                    if ($count1 == 0 && $count2 == 0 && $count3 == 0) {
                        Contacts::insert($data);

                        echo 2;
                    } else {
                        echo 1;
                    }
                } else {
                    $emcount1 = Contacts::where('email', $data['email'])->count();
                    $emcount2 = Oldstudent::where('email_id', $data['email'])->count();
                    $emcount3 = User::where('email', $data['email'])->count();

                    if ($emcount1 == 0 && $emcount2 == 0 && $emcount3 == 0 && $count1 == 0 && $count2 == 0 && $count3 == 0) {
                        Contacts::insert($data);
                        echo 2;
                    } elseif ($count1 != 0 || $count2 != 0 || $count3 != 0) {
                        echo 1;
                    } elseif ($emcount1 != 0 || $emcount2 != 0 || $emcount3 != 0) {
                        echo 3;
                    }
                }
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }
}
