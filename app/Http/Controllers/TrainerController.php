<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\Models\User;
use App\Models\Topic;
use League\Csv\Reader;

use App\Models\Package;
use App\Models\Subtopic;
use App\Models\Questions;
use App\Models\Employeeinfo;
use App\Models\Prerequisite;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TrainerController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
    }
    private function getTrainerPackages($trainerId)
    {
        return Employeeinfo::join('ev_package', function ($join) {
            $join->on(DB::raw("FIND_IN_SET(ev_package.tech_id, employee_info.technology)"), '>', DB::raw('0'));
        })
            ->leftJoin("ev_technology", "ev_technology.id", "=", "ev_package.tech_id")
            ->where('employee_info.user_id', $trainerId)
            ->where('ev_package.status', 1)
            //->where('ev_package.pac_type', 1)
            ->where("ev_technology.status", 1)
            ->get(['ev_package.*']);
    }

    private function getPackagePrerequisites($packageList)
    {
        $packageIds = $packageList->pluck('pac_id');

        return Prerequisite::whereIn('pac_id', $packageIds)->get();
    }

    private function attachPrerequisitesToPackages($packageList, $prerequisites)
    {
        $groupedPrerequisites = $prerequisites->groupBy('pac_id');

        foreach ($packageList as $package) {
            $package->pre_req = collect($groupedPrerequisites->get($package->pac_id, []));
        }
    }
    private function getPackageDetails($id)
    {
        return [
            'prerequisite' => Prerequisite::where('pac_id', $id)->get(),
            'singlelist' => Package::where('pac_id', $id)->first()
           
        ];
    }

    private function updatePrerequisites($pac_id, $prerequisites)
    {
        Prerequisite::where('pac_id', $pac_id)->delete();


    if(!empty($prerequisites)){
        foreach ($prerequisites as $preReq) {
            Prerequisite::create([
                'pac_id' => $pac_id,
                'pre_req' => $preReq,
            ]);
        }
    }
    }
    private function getsinglePackageDetails($id)
    {
        return Package::where('pac_id', $id)->first(); 
    }
    private function getTopiclisting($id)
    {
        return Topic::where('pac_id', $id)->get(); 
    }
    public function packageTrainer()
    {
        if (!Auth::check()) {
            return redirect("/");
        }
        if (Auth::user()->role != 3) {
            return redirect("/accessdenied");
        }
        $trainerId = Auth::user()->id;
        $packageList = $this->getTrainerPackages($trainerId);
        $prerequisites = $this->getPackagePrerequisites($packageList);
        $this->attachPrerequisitesToPackages($packageList, $prerequisites);
        $data = [
            'packageList' => $packageList,
            'title' => 'Package List',
        ];
        return view("package_trainer", $data);
    }

    public function packageEdit($id)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        if (Auth::user()->role != 3) {
            return redirect('/accessdenied');
        }
        $package = $this->getPackageDetails($id);
        return view('package_edit', $package);
    }

    public function packageUpdate(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        if (Auth::user()->role != 3) {
            return redirect('/accessdenied');
        }
       $pac_id = $request->get('pac_id');
      $this->updatePrerequisites($pac_id,$request->get('pre_req'));

        Session::flash('success', 'Successfully Updated');
        return redirect('/packagelist_trainer');
    }

public function syllabusViewTrainer($id)
{
    if (!Auth::check()) {
        return redirect('/');
    }

    if (Auth::user()->role !== 3) {
        return redirect('/accessdenied');
    }

    $topicsCount = Topic::where('pac_id', $id)->count();
    
    $data = [
        'title' => 'Syllabus Upload',
        'singlelist' => $this->getsinglePackageDetails($id),
        'pac_id' => $id,
        'topicscount' => $topicsCount,
    ];

    return view('syllabus_upload', $data);
}

    public function topicListing($id)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        if (Auth::user()->role != 3) {
            return redirect('/accessdenied');
        }

        $data = [
            "singlelist" => $this->getsinglePackageDetails($id),
            "topics" => $this->getTopiclisting($id),
            "title" => "Syllabus Upload",
        ];

        $topicIds = $data['topics']->pluck('id');
        $subtopics = Subtopic::whereIn('topic_id', $topicIds)
            ->get()
            ->groupBy('topic_id');

        foreach ($data['topics'] as $topic) {
            $topic->subtopics = $subtopics->get($topic->id, collect());
        }

        $data['topic_ids'] = $topicIds;

        return view("topic_listing", $data);
    }


    public function getPackagesByTechnology(Request $request)
    { 
        if (!Auth::check()) {
            return redirect('/');
        }
        $technologyIds = $request->input('techtrainerselect');
        $packages = Package::whereIn('tech_id', $technologyIds)->get(['pac_id','pac_name']);
        if ($packages->isEmpty()) {
            return response()->json(['message' => 'No packages found for the selected technologies'], 404);
        }
        return response()->json([
            'message' => 'Packages retrieved successfully',
            'data' => $packages
        ]);
    }

public function fetchExam(Request $request)
{
   
    if (!Auth::check()) {
        return redirect("/");
    }
    if (Auth::user()->role != 3) {
        return redirect("/accessdenied");
    }

    $data = [
        "title" => "View Exams",
        "selectedExamType" => $request->input('examtechnology'),
        "selectedTechnology" => $request->input('techselect'),
        "selectedPackage" => $request->input('techpackages'),
        "technology" => $this->getTechnologies(Auth::user()->id),
    ];
    $data['exams'] = $this->getExamListQuery($data['selectedExamType'], $data['selectedTechnology'], $data['selectedPackage'],10);


    if ($data['selectedPackage']) {
        
        $data['packages'] = DB::table('ev_package')
            ->where('tech_id', $data['selectedTechnology'])
            ->get(['pac_name', 'pac_id as id']);
    }



    return view('view_exam', $data);

}





private function hasSelectedFilters(array $data): bool
{
    return !empty($data['selectedExamType']) || 
           !empty($data['selectedTechnology']) || 
           !empty($data['selectedPackage']);
}

private function getTechnologies($trainerId)
{
    return Employeeinfo::join('ev_technology', function ($join) {
            $join->on(DB::raw("FIND_IN_SET(ev_technology.id, employee_info.technology)"), '>', DB::raw('0'));
        })
        ->where('employee_info.user_id', $trainerId)
        ->where("ev_technology.status", 1)
        ->get(['ev_technology.*']);
}


 //Common start
 protected function createExam($technology, $examName,$percentage)
 {
     return DB::table('ev_exam')->insertGetId([
         'exam_type' => $technology,
         'exam_name' => $examName,
         'trainer_id' => Auth::user()->id,
         'percentage'=>$percentage
     ]);
 }

 protected function attachTechnologiesAndPackages($request, $examId)
{
    $insertedPackages = [];


    foreach ($request->techtrainerselect as $technologyId) {
       
        if ($technologyId) {
           
            $examTechnologyId = DB::table('ev_examtechnology')->insertGetId([
                'exam_id' => $examId,
                'technology_id' => $technologyId,
            ]);    
           
        }
    }

    foreach ($request->packages as $packid) {
       
        if ($packid) {
           
            $exampackId = DB::table('ev_exampackage')->insertGetId([
                'exam_id' => $examId,
                'package_id' => $packid,
            ]);    
           
        }
    }

}

protected function validateExamDeclaration($request)
{
    return Validator::make($request->all(), [
        'examtechnology' => 'required|string',
        'examname' => 'required|string|max:255',
        'techtrainerselect' => 'required|array|min:1',
        'techtrainerselect.*' => 'integer|exists:ev_technology,id',
        'packages' => 'nullable|array',
        'packages.*' => 'integer|exists:ev_package,pac_id'
       
    ]);
}


protected function examExists($technology, $examName)
{
    return DB::table('ev_exam')
        ->where('exam_type', $technology)
        ->where('exam_name', $examName)
        ->exists();
}





 //Commmon end
    
public function examDeclaration()
{  
    if (!Auth::check()) {
        return redirect('/');
    }
    if (Auth::user()->role !== 3) {
        return redirect('/accessdenied');
    }
    $trainerId = Auth::user()->id;
    $data['technology'] = Employeeinfo::join('ev_technology', function ($join) {
            $join->on(DB::raw("FIND_IN_SET(ev_technology.id, employee_info.technology)"), '>', DB::raw('0'));
        })
        ->where('employee_info.user_id', $trainerId)
        ->where('ev_technology.status', 1)
        ->get(['ev_technology.*']);

    return view('exam_declaration', $data);
}

public function examDeclarationSubmit(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        if (Auth::user()->role !== 3) {
            return redirect('/accessdenied');
        }

        $validator = $this->validateExamDeclaration($request);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if ($this->examExists($request->examtechnology, $request->examname)) {
            Session::flash('error', "The exam '{$request->examname}' for technology '{$request->examtechnology}' already exists.");
            return redirect()->back()->withInput();
        }

      

        $examId = $this->createExam($request->examtechnology, $request->examname,$request->percentages);

        $this->attachTechnologiesAndPackages($request, $examId);
        
        Session::flash('success', ucfirst($request->examname) . " has been declared.");
        return redirect('/examDeclaration/');
    }

    public function viewQuestions($id)
    {
        if (Auth::check()) {
            if ((Auth::user()->role == 1) || (Auth::user()->role == 3)) {

                $data['exam_id'] = $id;
                $data["queslist"] = Questions::orderby("id", "desc")
                    ->whereRaw("FIND_IN_SET(?, exam_id)", [$id])
                    ->get();
                return view("view_questions", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }


    protected function getExamsWithTechnologiesAndPackages($examType = null)
    {
        $query = DB::table('ev_exam')
            ->join('ev_examtechnology', 'ev_exam.id', '=', 'ev_examtechnology.exam_id')
            ->join('ev_technology', 'ev_examtechnology.technology_id', '=', 'ev_technology.id')
            ->join('ev_exampackage', 'ev_examtechnology.id', '=', 'ev_exampackage.examtechnology_id')
            ->join('ev_package', 'ev_exampackage.package_id', '=', 'ev_package.pac_id')
            ->leftJoin('ev_exampercentage', 'ev_exam.id', '=', 'ev_exampercentage.exam_id');

        if (!empty($examType)) {
            $query->where('ev_exam.exam_type', $examType);
        }

        return $query->get([
            'ev_exam.id as exam_id',
            'ev_exam.exam_name',
            'ev_technology.id as technology_id',
            'ev_technology.technology',
            'ev_package.pac_name',
            'ev_exampercentage.percentage',
        ]);
    }

    /**
     * Fetch all technologies with their associated packages.
     */
    protected function getTechnologyPackages()
    {
        return DB::table('ev_technology')
            ->join('ev_package', 'ev_technology.id', '=', 'ev_package.tech_id')
            ->select('ev_technology.id', 'ev_package.pac_name')
            ->get()
            ->groupBy('id')
            ->map(function ($packages) {
                return $packages->pluck('pac_name')->toArray();
            });
    }

    /**
     * Group exams by technology and their packages.
     */
    protected function groupExamsByTechnologyAndPackage($exams, $technologyPackages)
    {
        $groupedExams = [];

        foreach ($exams as $exam) {
            if (!isset($groupedExams[$exam->exam_id])) {
                $groupedExams[$exam->exam_id] = [
                    'exam_name' => $exam->exam_name,
                    'percentage' => $exam->percentage,
                    'technologies' => [],
                ];
            }

            if (!isset($groupedExams[$exam->exam_id]['technologies'][$exam->technology_id])) {
                $groupedExams[$exam->exam_id]['technologies'][$exam->technology_id] = [
                    'technology' => $exam->technology,
                    'packages' => [],
                ];
            }

            if (isset($technologyPackages[$exam->technology_id]) && in_array($exam->pac_name, $technologyPackages[$exam->technology_id])) {
                $groupedExams[$exam->exam_id]['technologies'][$exam->technology_id]['packages'][] = $exam->pac_name;
            }
        }

        return $groupedExams;
    }

    /**
     * Format the grouped exams data for the response.
     */
    protected function formatExamsResponse($groupedExams)
    {
        $responseData = [];

        foreach ($groupedExams as $examId => $data) {
            $percentage = $data['percentage'] !== null ? $data['percentage'] : 'N/A';

            $responseData[] = [
                'exam_name' => $data['exam_name'],
                'exam_id' => $examId,
                'percentage' => $percentage,
                'technologies' => array_values($data['technologies']),
            ];
        }

        return $responseData;
    }

    public function addQuestions(Request $request)
    {
        // Check for authentication
        if (!Auth::check()) {
            return redirect("/");
        }

        // Check for user role
        if (Auth::user()->role != 3) {
            return redirect("/accessdenied");
        }

        $data["title"] = "Exam List";
        $trainerId = Auth::user()->id;

        $data['selectedExamType'] = $request->input('examtechnology');
        $data['selectedTechnology'] = $request->input('techselect');
        $data['selectedPackage'] = $request->input('techpackages');

        // Get technology options available to the trainer
        $data['technology'] = Employeeinfo::join('ev_technology', function ($join) {
            $join->on(DB::raw("FIND_IN_SET(ev_technology.id, employee_info.technology)"), '>', DB::raw('0'));
        })
            ->where('employee_info.user_id', $trainerId)
            ->where("ev_technology.status", 1)
            ->get(['ev_technology.*']);

        // Fetch filtered exam list if filter values are provided
        if (
            !empty($data['selectedExamType'])  ||
            !empty($data['selectedTechnology']) ||
            !empty($data['selectedPackage'])
        ) {

            // Fetch exams based on filters
            $exams = $this->getExamListQuery($data['selectedExamType'], $data['selectedTechnology'], $data['selectedPackage']);
            $technologyPackages = $this->getTechnologyPackages();
            $groupedExams = $this->groupExamsByTechnologyAndPackage($exams, $technologyPackages);
            $data['exams'] = $this->formatExamsResponse($groupedExams);
        } else {
            // If no filters applied, exams can be empty or set to some default behavior
            $data['exams'] = []; // Optionally, handle the case when no exams are found or needed.
        }

        return view('add_examquestions', $data);
    }



    public function getPackage(Request $request)
    {
        $technologyId = $request->input('techselect');

        if (empty($technologyId)) {
            return response()->json(['error' => 'Technology IDs are required'], 400);
        }

        $packages = DB::table('ev_package')
            ->where('tech_id', $technologyId)
            ->get();

        if ($packages->isEmpty()) {
            return response()->json(['message' => 'No packages found for the selected technologies'], 404);
        }

        return response()->json($packages);
    }


    protected function getExamListQuery($examType = null, $technologyId = null, $packageId = null)
{
    
    $query = DB::table('ev_exam')
        ->join('ev_examtechnology', 'ev_exam.id', '=', 'ev_examtechnology.exam_id')
        ->join('ev_technology', 'ev_examtechnology.technology_id', '=', 'ev_technology.id')
        ->join('ev_exampackage', 'ev_exampackage.exam_id', '=', 'ev_exam.id')
        ->join('ev_package', 'ev_exampackage.package_id', '=', 'ev_package.pac_id');


    if ($examType) {
        $query->where('ev_exam.exam_type', $examType);
    }

    if ($technologyId) {
        $query->where('ev_examtechnology.technology_id', $technologyId);
    }

    if ($packageId) {
        $query->where('ev_exampackage.package_id', $packageId);
    }

  
    $results = $query->select([
            'ev_exam.exam_type as examtype',
            'ev_exam.id as exam_id',
            'ev_exam.exam_name',
            'ev_technology.id as technology_id',
            'ev_technology.technology as technology_name',
            'ev_package.pac_name as package_name',
            'ev_exam.percentage'
        ])
        ->orderBy('ev_exam.id', 'desc')
        ->paginate(10); 

 
    $groupedResults = [];
    foreach ($results as $result) {
        $examId = $result->exam_id;
        if (!isset($groupedResults[$examId])) {
            $groupedResults[$examId] = [
                'examtype' => $result->examtype,
                'exam_id' => $examId,
                'exam_name' => $result->exam_name,
                'technologies' => [],
                'packages' => [],
                'percentage' => $result->percentage,
            ];
        }


        $groupedResults[$examId]['technologies'][] = $result->technology_name;
        $groupedResults[$examId]['packages'][] = $result->package_name;
    }


    foreach ($groupedResults as $examId => &$exam) {
        $exam['technologies'] = implode(', ', array_unique($exam['technologies']));
        $exam['packages'] = implode(', ', array_unique($exam['packages']));
    }


    $results->setCollection(collect(array_values($groupedResults))); 

    return $results; 
}

    
   



    
    


  
    






   


}
