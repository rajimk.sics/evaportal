<?php
namespace App\Http\Middleware\CheckStatus;
namespace App\Http\Controllers;
use App\Http\Controllers\EvacommonController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Models\Technology;
use App\Models\Designation;
use App\Models\Qualification;
use App\Models\Source;
use App\Models\Questions;
use App\Models\College;
use App\Models\Tax;
use App\Models\Package;
use App\Models\User;
use App\Models\Collegedep;
use App\Models\Exam;
use App\Models\Specialization;
use App\Models\Employee;
use App\Models\Employeeinfo;
use App\Models\Employeepriv;
use App\Models\Email;
use Carbon\Carbon;
use App\Models\OldPayment;
use App\Helpers\CustomHelper;
use App\Models\Othereference;
use App\Models\Payment;
use App\Models\Salespayment;
use App\Models\Event;
use App\Models\Signature;
use App\Models\Salespackage;
use App\Models\PageContent;
use App\Models\Gatepass;
use App\Models\Talento;
use App\Models\Reporting;
use App\Models\Paymentrequest;
use App\Models\Studentfeessplit;
use App\Models\Studentpackage;
use App\Models\Company;
use App\Models\Oldstudent;
use App\Models\Prerequisite;
use App\Models\Topic;
use App\Models\Subtopic;
use App\Models\Department;
use App\Models\Departmentpoc;
use Session;
use Auth;

class ActdeactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */  
public function __construct()
{
        $this->middleware("auth");
       
}

public function changeExamStatus(Request $request)
{

    if(!Auth::check()) {
        return redirect('/');
    }
    $user = Auth::user();
    if ($user->role != 3) {
        return redirect('/accessdenied');
    }
    $newStatus = $request->status;
    Exam::where('id', $request->id)->update(['status' =>$newStatus]);
   
}






public function departmentStatus(Request $request)
{
    if(!Auth::check()) {
        return redirect('/');
    }
    $user = Auth::user();
    if ($user->role !== 1) {
        return redirect('/accessdenied');
    }
    $newStatus = $request->status;
    Department::where('id', $request->id)->update(['status' =>$newStatus]);
   
}
public function departmentpocstatus(Request $request)
{
    if(!Auth::check()) {
        return redirect('/');
    }
    $user = Auth::user();
    if ($user->role !== 1) {
        return redirect('/accessdenied');
    }
    $newStatus = $request->status;
    Departmentpoc::where('id', $request->id)->update(['status' =>$newStatus]);
   
}

public function managequestionstatus(Request $request)
{
    if(!Auth::check()) {
        return redirect('/');
    }
    $user = Auth::user();
    if ($user->role != 3) {
        return redirect('/accessdenied');
    }
    $newStatus = $request->status;
    Questions::where('id', $request->id)->update(['status' =>$newStatus]);
    echo 1 ;
   
}
public function collegeStatus(Request $request)
{
    if(!Auth::check()) {
        return redirect('/');
    }
    $user = Auth::user();
    if (!in_array($user->role, [1,12])) {
        return redirect('/accessdenied');
    }

    $newStatus = $request->status;
    College::where('id', $request->id)->update(['status' =>$newStatus]);
    Collegedep::where("college_id", "=", $request->id)->update([
        "status" => $newStatus,
    ]);
   
}
public function changeTopicStatus(Request $request)
{

    if(!Auth::check()) {
        return redirect('/');
    }
    $user = Auth::user();
    if ($user->role != 3) {
        return redirect('/accessdenied');
    }
    $newStatus = $request->status;
    Topic::where('id', $request->id)->update(['status' =>$newStatus]);
   
}





}
