<?php
namespace App\Http\Middleware\CheckStatus;
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Mail\SimpleMail;
use Illuminate\Support\Facades\Mail;


class MailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       
    }
    public function checkmail(){
        $recipient = 'rajimk.sics@gmail.com'; // Replace with the recipient's email address

        // $subject = 'Dynamic Data Test';
         $dynamicData = ['name' => 'John Doe', 'message' => 'Hello, world!','subject'=>'ss','email'=>'t@gmail.com','password'=>'test@123','sales_phone'=>'345354']; 
 
         Mail::to($recipient)->send(new SimpleMail($dynamicData));
     
         return "Email sent successfully!";

    }

}
