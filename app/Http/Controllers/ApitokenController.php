<?php
namespace App\Http\Controllers;

use App\WorldlineSDK\PaymentRequest; // Import PaymentRequest from WorldlineSDK
use Illuminate\Http\Request;

class ApitokenController extends Controller
{




    public function generateToken(Request $request)
    {
        require_once base_path('WorldlineSDK/payment_request.php');
        
        $merchantCode = 'L892576';
        
        $payload = [
            'txnId' => md5(uniqid(mt_rand(), true)),
            'totalAmount' => $request->input('totalAmount'),
            'accountNo' => $request->input('accountNo'),
            'consumerId' => 'C458681',
            'consumerMobileNo' => $request->input('consumerMobileNo'),
            'consumerEmailId' => $request->input('consumerEmailId'),
            'debitStartDate' => $request->input('debitStartDate'),
            'debitEndDate' => $request->input('debitEndDate'),
            'maxAmount' => $request->input('maxAmount'),
            'amountType' => $request->input('amountType'),
            'frequency' => $request->input('frequency'),
            'cardNumber' => $request->input('cardNumber'),
            'expMonth' => $request->input('expMonth'),
            'expYear' => $request->input('expYear'),
            'cvvCode' => $request->input('cvvCode'),
            'salt' => '9237383558KRXCVK' 
        ];
        
        try {
            $paymentRequest = new PaymentRequest();
            $tokenString = $paymentRequest->generateTokenString($merchantCode,$payload);
            $hashedToken = $paymentRequest->generateHashedToken($tokenString);
            
            return response()->json([
                'status' => true,
                'token' => $tokenString,  
                'hashed_token' => $hashedToken, 
            ]);
        } catch (\Exception $e) {
            Log::error('Payment Request Error: ' . $e->getMessage());
            return response()->json([
                'status' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
    }


   











    



}
              