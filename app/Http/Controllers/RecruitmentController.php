<?php

namespace App\Http\Middleware\CheckStatus;

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Technology;
use App\Models\Designation;
use App\Models\Qualification;
use App\Models\JobRegister;
use App\Models\Candidatefollowup;
use App\Models\Source;
use App\Models\College;
use App\Models\User;
use App\Models\Contacts;
use App\Models\Followup;
use App\Models\Package;
use App\Models\Studentpackage;
use App\Models\Studentfeessplit;
use App\Models\Payment;
use App\Models\Candidateinterview;
use App\Models\Salespayment;
use App\Models\Salespackage;
use App\Models\Studentinfo;
use App\Models\Othereference;
use Illuminate\Support\Facades\View;
use Dompdf\Dompdf;
use Dompdf\Options;
use Illuminate\Support\Facades\Storage;
use App\Models\Employee;
use App\Models\Employeeinfo;
use App\Models\Employeepriv;
use Carbon\Carbon;
use App\Mail\SimpleMail;
use App\Mail\PayMail;
use App\Models\OldPayment;
use App\Http\Controllers\CommonController; // Import the class
use App\Models\Signature;
use Illuminate\Support\Facades\Mail;
use App\Models\Talento;
use App\Models\Gatepass;
use App\Models\Oldstudent;
use App\Models\Reporting;
use Session;
use Auth;

class RecruitmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function  manageCandidate(Request $request)
    {

        if (!Auth::check()) {
            return redirect('/');
        }
        $user = Auth::user();
        // Check user roles
        $year = $request->query('year', Carbon::now()->year);
        $month = $request->query('month', Carbon::now()->format('m'));

        if (!checkdate($month, 1, $year)) {

            return redirect('/accessdenied');
        }

        $startOfMonth = Carbon::create($year, $month, 1)
            ->startOfMonth()
            ->format('Y-m-d');
        $endOfMonth = Carbon::create($year, $month, 1)
            ->endOfMonth()
            ->format('Y-m-d');

        if (in_array($user->role, [10])) {

            $candlist = JobRegister::where('status', 0)
                ->whereBetween('created_at', [$startOfMonth, $endOfMonth])
                ->orderby("id", "desc")->get();

            $data = [
                'title' => 'Manage Candidate',
                'candlist' => $candlist,
                'years' => CommonController::generateYearRange(date('Y') - 10),
                'months' => CommonController::generateMonthOptions(),
                'currentYear' => $year,
                'currentMonth' => $month,
            ];
            return view("manage_candidate", $data);
        } else {
            return redirect('/accessdenied');
        }
    }

    public function  candidateDetails(Request $request)
    {

        if (!Auth::check()) {
            return redirect('/');
        }


        $id = $request->get("id");
        //   $data1=[];
        $data["candidatedetails"] = JobRegister::leftjoin('ev_candidatefollowup', 'ev_candidatefollowup.jobreg_id', '=', 'ev_jobinformation.id')
            ->where('ev_jobinformation.id', $id)
            ->orderBy('ev_candidatefollowup.id', 'desc')
            ->first([
                'ev_jobinformation.name',
                'ev_jobinformation.contact_sec',
                'ev_jobinformation.yearof_passout',
                'ev_jobinformation.address',
                'ev_jobinformation.ref_last_organisation',
                'ev_jobinformation.organisation_ref_contact',
                'ev_jobinformation.carrier_break',
                'ev_jobinformation.curr_organisation',
                'ev_jobinformation.curr_designation',
                'ev_candidatefollowup.date',
                'ev_candidatefollowup.comment',
            ]);



        return response()->json($data);
    }

    public function bulkCandidateUpload()
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        $user = Auth::user();
        if (in_array($user->role, [10])) {
            return view('add_candidatebulk', ['title' => 'Candidate Bulk Upload']);
        }

        return redirect('/accessdenied');
    }

    public function saveFollowup(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        if (Auth::user()->role == 10) {
            $data = [
                'date' => date('Y-m-d', strtotime($request->input('followupdate'))),
                'comment' => $request->input('comments'),
                'jobreg_id' => $request->input('jobregid'),
            ];

            Candidatefollowup::insert($data);

            JobRegister::where('id', $data['jobreg_id'])->update(['status' => 1]);

            Session::flash('success', 'Sucessfully Saved');
            return redirect('/manage_candidate');
        }
        return redirect('/acessdenied');
    }
    public function saveSchedule(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        if (Auth::user()->role == 10) {
            $data = [
                'scheduledate' => date('Y-m-d'),
                'interviewdate' => date('Y-m-d', strtotime($request->input('interviewdate'))),
                'company_id' => $request->input('company'),
                'position' => $request->input('position'),
                'jobreg_id' => $request->input('jobreg_id'),
                'recruiter_id' => Auth::id(),
                'recruiter_name' => Auth::user()->name,
            ];

            Candidateinterview::insert($data);

            JobRegister::where('id', $data['jobreg_id'])->update(['status' => 2]);
        }
        return redirect('/acessdenied');
    }

   
    public function candidateFollowups(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        $year = $request->query('year', Carbon::now()->year);
        $month = $request->query('month', Carbon::now()->format('m'));

        if (!checkdate($month, 1, $year)) {

            return redirect('/accessdenied');
        }

        $startOfMonth = Carbon::create($year, $month, 1)
            ->startOfMonth()
            ->format('Y-m-d');
        $endOfMonth = Carbon::create($year, $month, 1)
            ->endOfMonth()
            ->format('Y-m-d');

        if (Auth::user()->role == 10) {
            $candislist = Candidatefollowup::leftjoin('ev_jobinformation', 'ev_candidatefollowup.jobreg_id', '=', 'ev_jobinformation.id')
                ->where('ev_jobinformation.status', 1)
                ->whereBetween('ev_candidatefollowup.date', [$startOfMonth, $endOfMonth])
                ->orderBy("ev_jobinformation.id", "desc")->distinct()
                ->get([
                    'ev_jobinformation.id',
                    'ev_jobinformation.email',
                    'ev_jobinformation.app',
                    'ev_jobinformation.contact_pri',
                    'ev_jobinformation.name',
                    'ev_jobinformation.resume_upload',
                    'ev_jobinformation.position_applying',
                    'ev_jobinformation.tech_skills',
                    'ev_jobinformation.edu_qualification',
                    'ev_jobinformation.exp_salary',
                    'ev_jobinformation.exp_years',
                    'ev_jobinformation.contact_sec',
                    'ev_jobinformation.yearof_passout',
                    'ev_jobinformation.address',
                    'ev_jobinformation.ref_last_organisation',
                    'ev_jobinformation.organisation_ref_contact',
                    'ev_jobinformation.carrier_break',
                    'ev_jobinformation.curr_organisation',
                    'ev_jobinformation.curr_designation',
                    'ev_candidatefollowup.*',
                ]);

            $candlist = $candislist->unique(function ($item) {
                return $item['jobreg_id'];
            })->values()->all();

            $data = [
                'title' => 'Follow Up',
                'candlist' => $candlist,
                'years' => CommonController::generateYearRange(date('Y') - 10),
                'months' => CommonController::generateMonthOptions(),
                'currentYear' => $year,
                'currentMonth' => $month,
            ];
            return view('candidate_followup', $data);
        }
        return redirect('/acessdenied');
    }

    public function candidateInterview(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        $year = $request->query('year', Carbon::now()->year);
        $month = $request->query('month', Carbon::now()->format('m'));

        if (!checkdate($month, 1, $year)) {

            return redirect('/accessdenied');
        }

        $startOfMonth = Carbon::create($year, $month, 1)
            ->startOfMonth()
            ->format('Y-m-d');
        $endOfMonth = Carbon::create($year, $month, 1)
            ->endOfMonth()
            ->format('Y-m-d');

        if (Auth::user()->role == 10) {
            $candislist = Candidateinterview::leftjoin('ev_jobinformation', 'ev_candidateinterview.jobreg_id', '=', 'ev_jobinformation.id')
                ->join('ev_company', 'ev_candidateinterview.company_id', '=', 'ev_company.id')
                ->where('ev_jobinformation.status', 2)
                ->whereBetween('ev_candidateinterview.scheduledate', [$startOfMonth, $endOfMonth])
                ->orderBy("ev_jobinformation.id", "desc")
                ->get([
                    'ev_jobinformation.email',
                    'ev_jobinformation.app',
                    'ev_jobinformation.contact_pri',
                    'ev_jobinformation.name',
                    'ev_jobinformation.resume_upload',
                    'ev_jobinformation.position_applying',
                    'ev_jobinformation.tech_skills',
                    'ev_jobinformation.edu_qualification',
                    'ev_jobinformation.exp_salary',
                    'ev_jobinformation.exp_years',
                    'ev_jobinformation.contact_sec',
                    'ev_jobinformation.yearof_passout',
                    'ev_jobinformation.address',
                    'ev_jobinformation.ref_last_organisation',
                    'ev_jobinformation.organisation_ref_contact',
                    'ev_jobinformation.carrier_break',
                    'ev_jobinformation.curr_organisation',
                    'ev_jobinformation.curr_designation',
                    'ev_company.name as company_name',
                    'ev_candidateinterview.*'
                ]);

            $candlist = $candislist->unique(function ($item) {
                return $item['jobreg_id'];
            })->values()->all();

            $data = [
                'title' => 'Interview Scheduled Candidates',
                'candlist' => $candlist,
                'years' => CommonController::generateYearRange(date('Y') - 10),
                'months' => CommonController::generateMonthOptions(),
                'currentYear' => $year,
                'currentMonth' => $month,

            ];
            return view('candidate_interview', $data);
        }
        return redirect('/acessdenied');
    }

    public function updatefollowup(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        if (Auth::user()->role == 10) {

            $data = [
                'date' => date('Y-m-d', strtotime($request->input('editfollowupdate'))),
                'comment' => $request->input('comments_edit'),
                'jobreg_id' => $request->input('jobregid'),
            ];

            Candidatefollowup::insert($data);


            Session::flash('success', 'Sucessfully Updated');
            return redirect('/candidate_followups');
        }
        return redirect('/acessdenied');
    }





    public function candidatefollowupHistory(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        if (Auth::user()->role == 10) {
            $id = $request->get("id");
            $data['history'] = Candidatefollowup::where('jobreg_id', $id)
                ->orderBy("id", "desc")
                ->get();
            return response()->json($data);
        }
        return redirect('/acessdenied');
    }
    public function saveCandidateOffer(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        if (Auth::user()->role == 10) {

            Candidateinterview::where('id', $request->interviewid)
                ->update([
                    'interview_status' => $request->interviewstatus,
                    'comment' => $request->comments,
                ]);


            return response()->json([
                'newStatus' => $this->getStatusText($request->interviewstatus),
            ]);
        }
        return redirect('/acessdenied');
    }

    private function getStatusText($status)
    {
        switch ($status) {
            case 1:
                return 'Shortlisted';
            case 2:
                return 'Rejected';
            case 3:
                return 'On Hold';
            default:
                return 'NA';
        }
    }
}
