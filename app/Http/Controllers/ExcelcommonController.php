<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\Models\User;
use App\Models\Topic;
use League\Csv\Reader;
use App\Models\Package;
use App\Models\Subtopic;
use App\Models\Questions;
use App\Models\Technology;
use App\Models\Employeeinfo;
use App\Models\Prerequisite;
use Illuminate\Http\Request;
use App\Models\Topictechnology;
use App\Models\Questiontechnology;
use App\Models\Subtopictechnology;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;

class ExcelcommonController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
    }

    public function processSyllabusUpload($file, $technologies)
    {


        if (!$file || !$file->isValid()) {
            return [
                'error' =>
                'Invalid file upload.',

            ];
        }

        $rows = array_map('str_getcsv', file($file));

        $duplicates = [];

        $expectedHeaders = ['Chapter No', 'Topic Name'];

        foreach ($rows as $index => $row) {
            if ($index === 0) {
                $headers = array_map('trim', $row); 
                if ($headers !== $expectedHeaders) {
                    return [
                        'error' => 'Invalid format. Expected columns: ' . implode(', ', $expectedHeaders),
                    ];     
                      }
                continue;
            }

            if (empty(array_filter($row))) {
                continue;
            }

            $rowData = array_map('trim', $row);
            $Id = $rowData[0];
            $syllabus = strtoupper($rowData[1]);

            if (!empty($Id) && !empty($syllabus)) {
                $topicId = $this->processTopic($technologies, $Id, $syllabus,  $duplicates);
            }
        }

        return  [
            
            'duplicates' => $duplicates,
        ];
    }

    // function for topic upload based on syllabus
    private function processTopic($technologies, $chapterId, $syllabus, &$duplicates)
    {
        $insertedIds = [];

        $existingTopic = Topic::join('ev_topictechnology', 'ev_topics.id', '=', 'ev_topictechnology.topic_id')
            ->where('ev_topics.topics', '=', $syllabus)
            ->exists();

        if ($existingTopic) {
            $this->handleDuplicatesAndInsertTechnologies($technologies, $syllabus, $duplicates);
        } else {
            $insertedTopicId = $this->createNewTopic($chapterId, $syllabus);
            $insertedIds = $this->insertTechnologies($insertedTopicId, $technologies);
        }

        return $insertedIds;
    }

    private function handleDuplicatesAndInsertTechnologies($technologies, $syllabus, &$duplicates)
    {
        $topicId = Topic::where('topics', '=', $syllabus)->value('id');

        foreach ($technologies as $techId) {
            $technologyName = Technology::where('id', $techId)->value('technology');

            if ($this->isTechnologyAlreadyLinkedToTopic($techId, $syllabus)) {
                $duplicates[] = [
                    'dupli' => $syllabus,
                    'technology' => $technologyName,
                ];
            } elseif ($topicId) {
                $this->insertTechnologies($topicId, [$techId]);
            }
        }
    }

    private function isTechnologyAlreadyLinkedToTopic($techId, $syllabus)
    {
        return Topic::join('ev_topictechnology', 'ev_topics.id', '=', 'ev_topictechnology.topic_id')
            ->where('ev_topics.topics', '=', $syllabus)
            ->where('ev_topictechnology.technology', '=', $techId)
            ->exists();
    }

    private function insertTechnologies($topicId, $technologies)
    {
        $insertedIds = [];

        foreach ($technologies as $techId) {
            if (!$this->isTechnologyAlreadyLinkedToTopic($techId, $topicId)) {
                $data = [
                    'topic_id' => $topicId,
                    'technology' => $techId,
                ];
                $insertedIds[] = Topictechnology::insertGetId($data);
            }
        }

        return $insertedIds;
    }

    private function createNewTopic($chapterId, $syllabus)
    {
        $newTopicData = [
            'trainer_id' => Auth::user()->id,
            'chapter_number' => $chapterId,
            'topics' => $syllabus,
            'status' => 1,
            'updated_at' => now(),
        ];

        $insertedTopicId = Topic::insertGetId($newTopicData);
        return $insertedTopicId;
    }

    public function processSubtopicUpload($topicId, $file, $technologies)
    {
        if (!$file || !$file->isValid()) {
            return [
                'error' => 'Invalid file upload.',
            ];
        }
    
        $rows = array_map('str_getcsv', file($file));
    
        $duplicates = [];
        $subtopicsProcessed = false; 
    
        $expectedHeaders = 'Subtopic';
    
        foreach ($rows as $index => $row) {
            if ($index === 0) {
                $headers = array_map('trim', $row); 
                if ($headers[0] !== $expectedHeaders) {
                    return [
                        'error' => 'Invalid format. Expected column: ' . $expectedHeaders,
                    ];     
                      }
                continue;
            }
            if (empty(array_filter($row))) {
                continue;
            }
    
            $rowData = array_map('trim', $row);
            $subtopic = $rowData[0] ?? '';
    
            if (!empty($topicId) && !empty($subtopic)) {
                $subtopicId = $this->processSubtopic($topicId, $subtopic, $technologies, $duplicates);
                
                if ($subtopicId ) {
                    $subtopicsProcessed = true; 
                }
            }
        }
    
        if ($subtopicsProcessed) {
            $flashMessage = 'Subtopics uploaded successfully!';
        } else {
            $flashMessage = 'No valid subtopics were processed.';
        }
    
        // Session::flash('message', $flashMessage);
    
        return [
            'message' => $flashMessage,
            'duplicates' => $duplicates,
        ];
    }




  


    // function for subtopic insertion linking topic and technology
    private function processSubtopic($topicId, $subtopic, $technologies, &$duplicates)
    {
        $insertedIds = [];

       
        $existingSubtopic = Subtopic::where('sub_topics', '=', $subtopic)->exists();

        if ($existingSubtopic) {
            $this->handleDuplicatesAndInsertSubtopics($topicId, $subtopic, $technologies, $duplicates);
        } else {
            $insertedSubtopicId = $this->createNewSubtopic($subtopic);
            $insertedIds = $this->insertTechForSubtopics($topicId, $technologies, $insertedSubtopicId);
        }

        return $insertedIds;
    }


    private function handleDuplicatesAndInsertSubtopics($topicId, $subtopic, $technologies, &$duplicates)
    {
        $subtopicId = Subtopic::where('sub_topics', '=', $subtopic)->value('id');

        foreach ($technologies as $techId) {
            $technologyName = Technology::where('id', $techId)->value('technology');

            if ($this->isTechnologyAlreadyLinkedToSubtopic($techId, $topicId, $subtopic)) {
                $duplicates[] = [
                    'dupli' => $subtopic,
                    'technology' => $technologyName,
                ];
            } elseif ($subtopicId) {
                $this->insertTechForSubtopics($topicId, [$techId], $subtopicId);
            }
        }
    }





    private function isTechnologyAlreadyLinkedToSubtopic($techId, $topicId, $subtopic)
    {
        return Subtopic::join('ev_subtopictechnology', 'ev_subtopics.id', '=', 'ev_subtopictechnology.subtopic_id')
            ->where('ev_subtopics.sub_topics', '=', $subtopic)
            ->where('ev_subtopictechnology.topic_id', '=', $topicId)
            ->where('ev_subtopictechnology.technology', '=', $techId)
            ->exists();
    }

    private function insertTechForSubtopics($topicId, $technologies, $subtopicId)
    {
        $insertedIds = [];

        foreach ($technologies as $techId) {
            if (!$this->isTechnologyAlreadyLinkedToSubtopic($techId, $topicId, $subtopicId)) {
                $data = [
                    'subtopic_id' => $subtopicId,
                    'topic_id' => $topicId,
                    'technology' => $techId,
                ];
                $insertedIds[] = Subtopictechnology::insertGetId($data);
            }
        }

        return $insertedIds;
    }

    private function createNewSubtopic($subtopic)
    {
        $newSubTopicData = [
            'sub_topics' => $subtopic,
            'status' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ];

        $insertedSubtopicId = Subtopic::insertGetId($newSubTopicData);

        return $insertedSubtopicId;
    }

    public function processCsvUpload($file, $technologies)
    {
        if (!$file || !$file->isValid()) {
            return ['error' => 'Invalid file upload'];
        }

        $rows = array_map('str_getcsv', file($file));
        $expectedHeaders = ['questions', 'optiona', 'optionb', 'optionc', 'optiond', 'answer'];

        $headers = array_map('trim', $rows[0]);
        if ($headers !== $expectedHeaders) {
            return ['error' => 'Invalid file format. Expected columns: ' . implode(', ', $expectedHeaders)];
        }
        $duplicates = [];

        foreach (array_slice($rows, 1) as $row) {
            if (empty(array_filter($row))) continue;

            $rowData = array_map('trim', $row);
            $this->processQuestions($technologies, $rowData, $duplicates);
        }
        return  [
            
            'duplicates' => $duplicates,
        ];
    }

    private function processQuestions($technologies, $rowData, &$duplicates)
    {
        $questionText = $rowData[0];
        $existingQuestion = Questions::where('questions', $questionText)->first();

        if ($existingQuestion) {
            $existingTechnologyIds = Questiontechnology::where('question_id', $existingQuestion->id)
                ->pluck('technology')
                ->toArray();

            foreach ($technologies as $technologyId) {
                if (in_array($technologyId, $existingTechnologyIds)) {
                    $technologyName = Technology::find($technologyId)->technology;
                    $duplicates[] = ['questions' => $questionText, 'technology' => $technologyName];
                } else {
                    Questiontechnology::create([
                        'question_id' => $existingQuestion->id,
                        'technology' => $technologyId,
                    ]);
                   
                }
            }
        } else {
            $newQuestion = Questions::create([
                'questions' => $questionText,
                'optiona' => $rowData[1],
                'optionb' => $rowData[2],
                'optionc' => $rowData[3],
                'optiond' => $rowData[4],
                'answer' => $rowData[5],
            ]);
            foreach ($technologies as $technologyId) {
                Questiontechnology::create([
                    'question_id' => $newQuestion->id,
                    'technology' => $technologyId,
                ]);
               
            }
        }
    }
}
