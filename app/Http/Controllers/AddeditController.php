<?php
namespace App\Http\Middleware\CheckStatus;
namespace App\Http\Controllers;
use App\Http\Controllers\EvacommonController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Models\Technology;
use App\Models\Designation;
use App\Models\Qualification;
use App\Models\Source;
use App\Models\College;
use App\Models\Tax;
use App\Models\Package;
use App\Models\User;
use App\Models\Specialization;
use App\Models\Employee;
use App\Models\Employeeinfo;
use App\Models\Employeepriv;
use App\Models\Email;
use Carbon\Carbon;
use App\Models\OldPayment;
use App\Helpers\CustomHelper;
use App\Models\Othereference;
use App\Models\Payment;
use App\Models\Salespayment;
use App\Models\Event;
use App\Models\Signature;
use App\Models\Salespackage;
use App\Models\PageContent;
use App\Models\Gatepass;
use App\Models\Talento;
use App\Models\Reporting;
use App\Models\Paymentrequest;
use App\Models\Studentfeessplit;
use App\Models\Studentpackage;
use App\Models\Company;
use App\Models\Oldstudent;
use App\Models\Prerequisite;
use App\Models\Topic;
use App\Models\Subtopic;
use App\Models\Department;
use App\Models\Departmentpoc;
use Session;
use Auth;

class AddeditController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */  
public function __construct()
{
        $this->middleware("auth");
       
}

//Deaprtment Save

public function saveDepartment(Request $request)
{
  
    if (!Auth::check()) {
        return redirect('/');
    }
    $user = Auth::user();
    if ($user->role !== 1) {
        return redirect('/accessdenied');
    }
    $depName = strtoupper($request->get('department'));
    $existingDepartment = Department::where('department', $depName)->exists();

    if ($existingDepartment) {
        Session::flash('errormessage', 'Department already exists.');
    } else {
        Department::create([
            'department' => $depName,
        ]);

        Session::flash('success', 'Department successfully saved.');
    }
    return redirect('/departments');
}

//Department Update

public function editDepartment(Request $request)
{
    
    if (!Auth::check()) {
        return redirect('/');
    }

    $user = Auth::user();

   
    if ($user->role !== 1) {
        return redirect('/accessdenied');
    }

    
    $departmentId = $request->editdepid;
    $departmentName = strtoupper($request->editdepname);

  
    Department::where('id', $departmentId)
        ->update(['department' => $departmentName]);

    Session::flash('success', 'Department successfully updated.');
    return redirect('/departments');
}


//Department POc Update

public function edit_departmentPoc(Request $request)
{
   
    if (!Auth::check()) {
        return redirect('/');
    }

   
    if (Auth::user()->role != 1) {
        return redirect('/accessdenied');
    }

    Departmentpoc::where('id', $request->editdepartid)
        ->update([
            'department_poc' => strtoupper($request->editdepartPoc),
        ]);

    Session::flash('success', 'Successfully Updated');
    return redirect('/departmentPoc');
}


//Department Save

public function add_departmentPoc(Request $request)
{
   
    if (Auth::check()) {
       
        if (Auth::user()->role == 1) {
           
            $count = Departmentpoc::where("department_poc", strtoupper($request->get("departmentPoc")))->count();
            
            if ($count == 0) {
               
                Departmentpoc::insert([
                    "department_poc" => strtoupper($request->get("departmentPoc"))
                ]);
               
                Session::flash("success", "Successfully Saved");
            } else {
             
                Session::flash("errormessage", "Already Saved");
            }

          
            return redirect("/departmentPoc");

        } else {
           
            return redirect('/accessdenied');
        }
    } else {
        return redirect('/');
    }
}








}
