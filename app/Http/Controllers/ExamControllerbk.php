<?php
namespace App\Http\Middleware\CheckStatus;
namespace App\Http\Controllers;

use Auth;
use Session;
use App\Models\Tax;
use App\Models\Exam;
use App\Models\User;
use App\Models\Event;
use App\Models\Source;
use App\Models\College;
use App\Models\Package;
use App\Models\Payment;
use App\Models\Student;
use App\Models\Employee;
use App\Models\Questions;
use App\Models\Signature;
use App\Models\Technology;
use App\Imports\ImportQues;
use App\Models\Designation;
use App\Models\Salespackage;
use App\Models\Salespayment;
use Illuminate\Http\Request;
use App\Models\Othereference;
use App\Models\Qualification;
use App\Models\Examtechnology;
use App\Models\Specialization;
use App\Models\Studentpackage;
use App\Models\Studentfeessplit;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ExcelcommonController;

class ExamController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $ExcelcommonController;

    public function __construct()
    {
        $this->middleware("auth");
        $this->ExcelcommonController = new ExcelcommonController();
    }

//Questions Upload

public function viewQuestionUpload()
{
    if (!Auth::check()) {
        return redirect('/');
    }
    if (!in_array(Auth::user()->role, [1, 3])) {
        return redirect('/accessdenied');
    }
    $technologies = Technology::get(['id', 'technology']);
    $data = [
        'title' => 'Upload Question',
        'technologies' => $technologies,
    ];
    return view('questions_upload', $data);
}
public function questionsUpload(Request $request)
{
    if (!Auth::check()) {
        return redirect('/');
    }
    if (Auth::user()->role != 3) {
        return redirect('/accessdenied');
    }
    $request->validate([
        'technology' => 'required|array|min:1',
        'csv_file' => 'required|mimes:csv,txt,xlsx,xls|max:2048'
    ]);
    $technologies = $request->input('technology');
    $csv_file = $request->file('csv_file');
    $result = $this->ExcelcommonController->processCsvUpload($csv_file, $technologies);
    return $this->handleRedirect($result);
}
private function handleRedirect($responseData)
{

    if (isset($responseData['error'])) {
        Session::flash('error', $responseData['error']);
        return redirect()->back();
    }
    if (isset($responseData['duplicates']) && !empty($responseData['duplicates'])) {
        Session::flash('duplicates', $responseData['duplicates']);
        return redirect()->back();
    }
    else{
        Session::flash('success', 'Successfully Updated');
        return redirect('/manageQuestions');
    }
    
}

public function manageQuestions(Request $request)
{
    if (!Auth::check()) {
        return redirect('/');
    }
    if (!in_array(Auth::user()->role, [1, 3])) {
        return redirect('/accessdenied');
    }

    $query = Questions::select(
        'ev_questions.id',
         'ev_questions.status',
        'ev_questions.questions',
        'ev_questions.optiona',
        'ev_questions.optionb',
        'ev_questions.optionc',
        'ev_questions.optiond',
        'ev_questions.answer',
        DB::raw("GROUP_CONCAT(ev_technology.technology SEPARATOR ', ') as technology_list")
    )
        ->join("ev_questiontechnology", "ev_questiontechnology.question_id", "=", "ev_questions.id")
        ->join("ev_technology", "ev_questiontechnology.technology", "=", "ev_technology.id")
        ->groupBy(
            'ev_questions.id',
             'ev_questions.status',
            'ev_questions.questions',
            'ev_questions.optiona',
            'ev_questions.optionb',
            'ev_questions.optionc',
            'ev_questions.optiond',
            'ev_questions.answer'
        )
        ->orderBy('ev_questions.id');

    if ($request->filled('technology')) {
        $technologies = $request->get('technology');
        $query->whereIn('ev_questiontechnology.technology', $technologies);
    }
    $data = [
        'questions' => $query->get(),
        'technologies' => Technology::all()
    ];

    return view('manage_questions', $data);
}


/////////////////////////////////////////////////////



// function for adding questions 
public function addExamquestions($exam_id, Request $request)
{
    if (!Auth::check()) {
        return redirect('/');
    }
    if (!in_array(Auth::user()->role, [1, 3])) {
        return redirect('/accessdenied');
    }
    $exam = Exam::where('id', '=', $exam_id)->value('exam_name');


    $assignedTechnologies = Examtechnology::where('exam_id', $exam_id)
        ->pluck('technology_id')
        ->toArray();

    $selectedTechnologies = $request->get('technology', $assignedTechnologies);
    $questions = $this->getFilteredQuestions($selectedTechnologies);
    $data = [
        'questions' => $questions,
        'technologies' => Technology::all(),
        'exam_id' => $exam_id,
        'selectedTechnologies' => $selectedTechnologies,
        'exam' => $exam

    ];
    return view('manage_examquestion', $data);
}

private function getFilteredQuestions(array $selectedTechnologies)
{
    return Questions::join('ev_questiontechnology', 'ev_questions.id', '=', 'ev_questiontechnology.question_id')
        ->join('ev_technology', 'ev_questiontechnology.technology', '=', 'ev_technology.id')
        ->whereIn('ev_questiontechnology.technology', $selectedTechnologies)
        ->orderBy('ev_technology.technology', 'desc')
        ->get(['ev_questions.*', 'ev_technology.technology as technology']);
}

}



