<?php
namespace App\Http\Middleware\CheckStatus;
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Technology;
use App\Models\Designation;
use App\Models\Qualification;
use App\Models\Source;
use App\Models\College;
use App\Models\Tax;
use App\Models\Package;
use App\Models\User;
use App\Models\Specialization;
use App\Models\Employee;
use App\Models\Othereference;
use App\Models\Payment;
use App\Models\Salespayment;
use App\Models\Event;
use App\Models\Signature;
use App\Models\Salespackage;
use App\Models\PageContent;
use App\Models\Gatepass;
use App\Models\Talento;
use App\Models\Studentpackage;
use App\Models\Reporting;
use App\Models\OldPayment;

use App\Models\StudentDay;
use App\Models\AssignedStudent;
use App\Models\StudentTrainer;

use Carbon\Carbon;
use Session;
use DB;
use Auth;

class MainController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware("auth");
    }

    public function transaction_unique(Request $request)
    {
        $trans_id = $request->get("trans_id");
        $count = Payment::where('transaction_id', $trans_id)->count();
        $ocount = OldPayment::where('transaction_id', $trans_id)->count();

        if ($count == 0 && $ocount == 0) {
            echo 0;
        } else {
            echo 1;
        }
    }

    public function changedate(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 4 || Auth::user()->role == 8 || Auth::user()->role == 1) {
                $id = $request->get("id");
                $start_date = date("Y-m-d", strtotime($request->get("start_date")));
                $end_date = date("Y-m-d", strtotime($request->get("end_date")));
                Gatepass::where('id', $id)->update(['start_date' => $start_date, 'end_date' => $end_date]);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }


    public function fetchTrainers(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
    
        $studentId = $request->input('studentId');
        $pacId = $request->input('pacId');
    
        $trainers = $this->getAvailableTrainers($pacId);
        $assignedTrainers = $this->getAssignedTrainers($studentId);
        $assignedDays = $this->getAssignedDays($studentId);
        $timeSlots = $this->getAssignedTimeSlots($studentId);
    
        return response()->json([
            'trainers' => $trainers,
            'assignedTrainers' => $assignedTrainers,
            'assigneddays' => $assignedDays,
            'start_time' => $timeSlots['start_time'],
            'end_time' => $timeSlots['end_time'],
            'message' => $trainers->isEmpty() ? 'No Trainers Available' : ''
        ]);
    }
    
  
    protected function getAvailableTrainers($pacId)
    {
        return Package::where('ev_package.pac_id', $pacId)
            ->join('employee_info', function ($join) {
                $join->on(DB::raw("FIND_IN_SET(ev_package.tech_id, employee_info.technology)"), '>', DB::raw('0'));
            })
            ->join('users', 'employee_info.user_id', '=', 'users.id')
            ->get(['users.name as trainer_name', 'users.id as trainer_id']);
    }
    
  
    protected function getAssignedTrainers($studentId)
    {
        return StudentTrainer::where('student_id', $studentId)
            ->pluck('trainer_id')
            ->toArray();
    }
    
   
    protected function getAssignedDays($studentId)
    {
        return StudentDay::where('student_id', $studentId)
            ->pluck('day')
            ->toArray();
    }
    
   
    protected function getAssignedTimeSlots($studentId)
    {
        return [
            'start_time' => AssignedStudent::where('student_id', $studentId)->value('start_time'),
            'end_time' => AssignedStudent::where('student_id', $studentId)->value('end_time'),
        ];
    }

    public function pass_return(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 4 || Auth::user()->role == 8 || Auth::user()->role == 1) {
                $id = $request->get("gpass_id");

                if ($request->file("gate_file") != '') {
                    $extension = $request->file("gate_file")->getClientOriginalExtension();
                    $destinationPath = "public/uploads/gatepass"; // upload path
                    $fileName = rand(11111, 99999) . "." . $extension;
                    $request->file("gate_file")->move($destinationPath, $fileName);
                } else {
                    $fileName = '';
                }

                Gatepass::where('id', $id)->update(['status' => 3, 'files' => $fileName, 'return_by' => Auth::user()->id]);

                echo 1;
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function your_students(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 2) {
               



                // Get year and month from query parameters with default values
                $year = $request->query('year', Carbon::now()->year);
                $month = $request->query('month', Carbon::now()->format('m'));

                // Validate year and month
                if (!checkdate($month, 1, $year)) {
                    // If invalid date, redirect or handle error as appropriate
                    return redirect('/accessdenied');
                }

                // Calculate start and end of the month
                $startOfMonth = Carbon::create($year, $month, 1)
                    ->startOfMonth()
                    ->format('Y-m-d');
                $endOfMonth = Carbon::create($year, $month, 1)
                    ->endOfMonth()
                    ->format('Y-m-d');


                $data1['studlist'] = Studentpackage::join('users', 'users.id', '=', 'ev_student_package.student_id')
                    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'ev_student_package.student_id')
                    ->join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
                    ->whereBetween('ev_student_package.joining_date', [$startOfMonth, $endOfMonth])
                    ->where('ev_student_package.sales_id', Auth::user()->id)
                    ->orderby("users.id", "desc")
                    ->get([
                        'users.name',
                        'users.phone',
                        'users.email',
                        'ev_student_package.sales_id',
                        'ev_student_package.joining_date',
                        'users.id',
                        'ev_student_info.student_admissionid',
                        'ev_package.pac_name',
                        'ev_student_package.id as studentpackage_id',
                        'ev_package.pac_type',
                    ]);

                    $data = [
                        'title' => 'Your  Students',
                       'studlist'=> $data1['studlist'],
                        'years' => CommonController::generateYearRange(date('Y') - 10),
                        'months' => CommonController::generateMonthOptions(),
                        'currentYear' => $year,
                        'currentMonth' => $month,
                    ];

                return view("manage_students_your", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function update_doj(Request $request)
    {
        $student_package_id = $request->get('student_package_id');
        $doj = $request->get('doj');
        $data['joining_date'] = date("Y-m-d", strtotime($doj));
        Studentpackage::where('id', $student_package_id)->update($data);
    }

    public function get_students_regular($flag = null)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 4 || Auth::user()->role == 8 || Auth::user()->role == 9 || Auth::user()->role == 2) {
                $data['title'] = 'Regular Students';

                /*if (Auth::user()->role == 2){

                    $reportedList           =Reporting::join("users","users.id", "=","ev_reporting.employee_id")
                                                        -> where('ev_reporting.reported_to',Auth::user()->id)->get(['users.id']);

                 // Loop through the results and add another value
                    $reportedList->each(function ($item, $key) {
                        $item->id = Auth::user()->id;
                    });


                    $userIds = $reportedList->pluck('id')->toArray();

                   
                }*/

                $data['studlist'] = Studentpackage::join('users', 'users.id', '=', 'ev_student_package.student_id')
                    ->join('ev_student_info', 'ev_student_info.student_id', '=', 'ev_student_package.student_id')

                    ->join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
                    ->distinct('ev_student_package.student_id')
                    ->orderby("users.id", "desc")
                    ->get(['users.name', 'users.phone', 'users.email', 'ev_student_info.sales_id', 'ev_student_package.stud_doj', 'users.id', 'ev_student_info.student_admissionid']);

                return view("manage_students", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }




    public function assignTrainer(Request $request)
    {
        $validatedData = $this->validateTrainerAssignment($request);
    
        $this->createAssignedStudent($validatedData);
    
            $this->assignTrainersToStudent($validatedData['student_id'],  $validatedData['trainer_id'], $validatedData['studentpackageid']);
    
            $this->assignDaysToStudent($validatedData['student_id'], $validatedData['days'], $validatedData['studentpackageid']);
    
            Session::flash('success', 'Successfully Updated');
       
    
        return redirect()->back();
    }
    protected function validateTrainerAssignment(Request $request)
    {
        return $request->validate([
            'student_id' => 'required|integer',
            'pac_id' => 'required|integer',
            'studentpackageid' => 'required|integer',
            'trainer_id' => 'required|array',
            'days' => 'required|array',
            'start_time' => 'required',
            'end_time' => 'required',
        ]);
    }
    
    protected function createAssignedStudent(array $data)
    {
        AssignedStudent::where('student_id',$data['student_id'])->delete();

         AssignedStudent::insertGetId([
            'student_id' => $data['student_id'],
            'pac_id' => $data['pac_id'],
            'studentpackageid' => $data['studentpackageid'],
            'start_time' => $data['start_time'],
            'end_time' => $data['end_time'],
        ]);
    }
 
    
    
 
    
    protected function assignTrainersToStudent($studentId, array $trainerIds, $studentpackageid)
    {
        StudentTrainer::where('student_id', $studentId)->delete();
        foreach ($trainerIds as $trainerId) {
            StudentTrainer::insert([
                'student_id' => $studentId,
                'trainer_id' => $trainerId,
                'studentpackageid' => $studentpackageid,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
    
    protected function assignDaysToStudent($studentId, array $days,$studentpackageid)
    {
        $assignedDays = StudentDay::where('student_id', $studentId)
                                  ->delete();
    
    
        foreach ($days as $day) {
            StudentDay::insert([
                'student_id' => $studentId,
                'day' => $day,
                'studentpackageid' => $studentpackageid,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }

    public function all_students(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        if (!in_array(Auth::user()->role, [2, 4, 8, 9])) {
            return redirect('/accessdenied');
        }

        $data['title'] = 'All Students';
        $year = $request->query('year', Carbon::now()->year);
        $month = $request->query('month', Carbon::now()->format('m'));
        $pac_type = $request->query('packtype','1');

        if (!checkdate($month, 1, $year)) {
            return redirect('/accessdenied');   
        }

        $startOfMonth = Carbon::create($year, $month, 1)
            ->startOfMonth()
            ->format('Y-m-d');

        $endOfMonth = Carbon::create($year, $month, 1)
            ->endOfMonth()
            ->format('Y-m-d');


        $query                    = Studentpackage::join('users', 'users.id', '=', 'ev_student_package.student_id')
            ->join('ev_student_info', 'ev_student_info.student_id', '=', 'users.id')
            ->join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
            ->where('ev_package.pac_type',$pac_type)
            ->whereBetween('ev_student_package.joining_date', [$startOfMonth, $endOfMonth]);

        $data1['studlist']      = $query->orderby('users.id', 'desc')
            ->get([
                'users.name',
                'users.id',
                'users.phone',
                'ev_package.pac_name',
                'users.email',
                'ev_student_info.sales_id',
                'ev_student_package.due_date_first',
                'ev_student_package.enddate',
                'ev_student_package.package_type as pac_type',
                'ev_student_package.id as studentpackageid',
                'ev_student_package.package_id as package_id',
                'ev_student_package.joining_date',
                'ev_package.tech_id',
                'ev_student_info.student_admissionid',
            ]);

        $data                           = [
            'title' => 'All  Students',
            'studlist' => $data1['studlist'],
            'years' => CommonController::generateYearRange(date('Y') - 10),
            'months' => CommonController::generateMonthOptions(),
            'currentYear' => $year,
            'currentMonth' => $month,
            'pac_type' => $pac_type
        ];
        return view("manage_students_all", $data);
    }






    /*public function all_students(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 4 || Auth::user()->role == 8 || Auth::user()->role == 9 || Auth::user()->role == 2) {
                $data['title'] = 'All Students';
                $year = $request->query('year', Carbon::now()->year);
                $month = $request->query('month', Carbon::now()->format('m'));

                if (!checkdate($month, 1, $year)) {
                    return redirect('/accessdenied');
                }


                $startOfMonth = Carbon::create($year, $month, 1)
                    ->startOfMonth()
                    ->format('Y-m-d');

                $endOfMonth = Carbon::create($year, $month, 1)
                    ->endOfMonth()
                    ->format('Y-m-d');
                    
                $query                          = Studentpackage::join('users', 'users.id', '=', 'ev_student_package.student_id')
                                                                ->join('ev_student_info', 'ev_student_info.student_id', '=', 'users.id')
                                                                ->join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
                                                                ->whereBetween('ev_student_package.joining_date', [$startOfMonth, $endOfMonth]);

                $data1['studlist']              = $query->orderby('users.id', 'desc')
                                                        ->get([
                                                            'users.name',
                                                            'users.id',
                                                            'users.phone',
                                                            'ev_package.pac_name',
                                                            'users.email',
                                                            'ev_student_info.sales_id',
                                                            'ev_student_package.due_date_first',
                                                            'ev_student_package.enddate',
                                                            'ev_student_package.package_type as pac_type',
                                                            'ev_student_package.joining_date',
                                                            'ev_package.tech_id',
                                                            'ev_student_info.student_admissionid',
                                                        ]);

                $data                           = ['title' => 'All  Students',
                                                    'studlist'=> $data1['studlist'],
                                                    'years' => CommonController::generateYearRange(date('Y') - 10),
                                                    'months' => CommonController::generateMonthOptions(),
                                                    'currentYear' => $year,
                                                    'currentMonth' => $month,
                                                ];
    
                
               
               /* if (!empty($request->packname) && !empty($request->start_date1) && !empty($request->end_date1) && !empty($request->packtype)) {
                    
                    $start_date_formatted = \Carbon\Carbon::createFromFormat('d-m-Y', $request->start_date1)->format('Y-m-d');
                    $end_date_formatted = \Carbon\Carbon::createFromFormat('d-m-Y', $request->end_date1)->format('Y-m-d');

                    $data['packname'] = $request->packname;
                    $data['start_date1'] = $request->start_date1;
                    $data['end_date1'] = $request->end_date1;
                    $data['packtype']=$request->packtype;

                    $pac_type=$request->packtype;


                    $data['package'] = Package::where('status', 1)->where('pac_type',$pac_type)->get();


                    // Assuming $start_date_formatted and $end_date_formatted are properly formatted date strings

                    // Base query
                   

                    /*if ($data['packtype'] == 1) {
                        // Additional filter for pac_type == 2
                        $query->where('ev_student_package.package_type',1);
                        $query->whereBetween('due_date_first', [$start_date_formatted, $end_date_formatted]);
                    }

                    // Apply additional filter based on $pac_type
                    if ($data['packtype'] == 2) {
                        // Additional filter for pac_type == 2
                        $query->where('ev_student_package.package_type',2);
                        $query->whereRaw("STR_TO_DATE(due_date_first, '%d-%m-%Y') BETWEEN ? AND ?", [$start_date_formatted, $end_date_formatted]);
                        $query->whereRaw("STR_TO_DATE(enddate, '%d-%m-%Y') BETWEEN ? AND ?", [$start_date_formatted, $end_date_formatted]);
                    }

                    // Finalize query
                    $data['studlist'] = $query
                        ->orderby('users.id', 'desc')
                        ->get([
                            'users.name',
                            'users.id',
                            'users.phone',
                            'ev_package.pac_name',
                            'users.email',
                            'ev_student_info.sales_id',
                            'ev_student_package.due_date_first',
                            'ev_student_package.enddate',
                            'ev_student_package.package_type as pac_type',
                            'ev_student_package.joining_date',
                            'ev_package.tech_id',
                            'ev_student_info.student_admissionid',
                        ]);

                        


                } else {
                    $data['packname'] = '';
                    $data['start_date1'] = '';
                    $data['end_date1'] = '';

                    $pac_type='';
                    $data['packtype'] = '';

                    $data['package'] = Package::where('status', 1)->get();



                        $today = Carbon::now();

                        // Calculate the start date of the current month and today's date
                        $start_date_formatted = $today->copy()->startOfMonth()->format('Y-m-d');
                        $end_date_formatted = $today->copy()->endOfDay()->format('Y-m-d');



                
                    // Initialize the query
                    $query = Studentpackage::join('users', 'users.id', '=', 'ev_student_package.student_id')
                        ->join('ev_student_info', 'ev_student_info.student_id', '=', 'ev_student_package.student_id')
                        ->join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
                        ->whereBetween('ev_student_package.joining_date', [$start_date_formatted, $end_date_formatted])
                        ->select([
                            'users.name',
                            'users.id',
                            'ev_package.pac_name',
                            'users.phone',
                            'users.email',
                            'ev_student_package.joining_date',
                            'ev_student_package.sales_id',
                            'ev_student_package.due_date_first',
                            'ev_student_package.enddate',
                            'ev_package.tech_id',
                            'ev_student_info.student_admissionid',
                            'ev_package.pac_type',
                        ]);
                
                    // Apply filters based on the package type
                   /* if ($data['packtype'] == 1) {
                        $query->where('ev_student_package.package_type', 1)
                              ->whereBetween('due_date_first', [$start_date_formatted, $end_date_formatted]);
                    } elseif ($data['packtype'] == 2) {
                        $query->where('ev_student_package.package_type', 2)
                              ->whereRaw("STR_TO_DATE(due_date_first, '%Y-%m-%d') BETWEEN ? AND ?", [$start_date_formatted, $end_date_formatted])
                              ->whereRaw("STR_TO_DATE(enddate, '%Y-%m-%d') BETWEEN ? AND ?", [$start_date_formatted, $end_date_formatted]);
                    }
                
                    // Order and get the results
                    $data['studlist'] = $query->orderBy('users.id', 'desc')->get();


                    
                }
                return view("manage_students_all", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }*/
    
}
