<?php
namespace App\Http\Middleware\CheckStatus;
namespace App\Http\Controllers;
use App\Http\Controllers\EvacommonController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Models\Technology;
use App\Models\Designation;
use App\Models\Qualification;
use App\Models\Collegedep;
use App\Models\Source;
use App\Models\College;
use App\Models\Tax;
use App\Models\Package;
use App\Models\User;
use App\Models\Specialization;
use App\Models\Employee;
use App\Models\Employeeinfo;
use App\Models\Employeepriv;
use App\Models\Email;
use Carbon\Carbon;
use DB;
use App\Models\OldPayment;
use App\Helpers\CustomHelper;
use App\Models\Othereference;
use App\Models\Payment;
use App\Models\Salespayment;
use App\Models\Event;
use App\Models\Signature;
use App\Models\Salespackage;
use App\Models\PageContent;
use App\Models\Gatepass;
use App\Models\Talento;
use App\Models\Reporting;
use App\Models\Paymentrequest;
use App\Models\Studentfeessplit;
use App\Models\Studentpackage;
use App\Models\Company;
use App\Models\Oldstudent;
use App\Models\Prerequisite;
use App\Models\Topic;
use App\Models\Subtopic;
use App\Models\Department;
use App\Models\Departmentpoc;
use App\Models\State;
use App\Models\District;

use Session;
use Auth;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

     protected $EvacommonController;
    public function __construct()
    {
        $this->middleware("auth");
        $this->EvacommonController = new EvacommonController();
    }

    //College
    public function manage_college()
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        $user = Auth::user();


        if (!in_array($user->role, [1, 2, 12])) {
            return redirect('/accessdenied');
        }
        $collegelist = College::leftJoin('ev_district', 'ev_district.id', '=', 'ev_college.district')
        ->leftJoin('ev_collegedep', 'ev_collegedep.college_id', '=', 'ev_college.id')
        ->select([
            'ev_college.id',
            'ev_college.college',
            'ev_district.name as district',
            'ev_college.status as college_status',
            DB::raw('GROUP_CONCAT(ev_collegedep.department SEPARATOR ", ") as departments') 
        ])
        ->distinct()
        ->groupBy('ev_college.id', 'ev_college.college', 'ev_district.name', 'ev_college.status'); 
    
    if ($user->role == 2) {
        $collegelist = $collegelist->where('ev_college.status', '1');
    }
    
    $collegelist = $collegelist->orderBy('ev_college.id', 'desc')->get();
    

        $data = [
            'title' => 'Manage College',
            'collegelist' =>   $collegelist,
            'states' => State::all(),
            
        ];

        return view('manage_college', $data);
    }
   

    public function getCollegeDetails(Request $request,$id)
    {
        
        $colleges = College::where('ev_college.id', $id)->first();

      
        $states = State::all();
        $districts = District::where('state_id', $colleges->state)->get();


        $department=Department::where('status',1)->get();

        $selectedDepartmentIds = Collegedep::where('college_id', $id)
                                    -> where('status', 1)
                                    ->pluck('department') 
                                    ->toArray();

        
        return view("edit_college",[
            'college_id' => $colleges->id,
            'depoc_id' => $colleges->depoc_id,
            'college_name' => $colleges->college,
            'college_state' => $colleges->state,
            'college_district' => $colleges->district,
            'college_place' => $colleges->city,
            'selected_department_ids' => $selectedDepartmentIds, 
            'states' => $states,
            'districts' => $districts,
            'departments' => $department,
        ]);
    }


    public function edit_college(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        $user = Auth::user();


        if (!in_array($user->role, [1, 12])) {
            return redirect('/accessdenied');
        }
        College::where("id", "=", $request->editcollegeid)->update([
            "college" => strtoupper($request->editcolname),
            'state' => $request->get('editcollegestate'),
            'district' => $request->get('editcollegedist'),
            'city' => $request->get('collegeplace'),
        ]);

    
    $selectedDepartments = $request->get('editcollegedep', []);
    $existingDepartments = Collegedep::where('college_id', $request->editcollegeid)
                            ->where('status',1)
                            ->pluck('department')
                             ->toArray();
    $deactdepartment = array_diff($existingDepartments, $selectedDepartments);
    Collegedep::where('college_id', $request->editcollegeid)
             ->whereIn('department', $deactdepartment)->update(['status'=>2]);
    $departmentToadd = array_diff($selectedDepartments,$existingDepartments);
    Collegedep::where('college_id', $request->editcollegeid)
    ->whereIn('department', $departmentToadd)
    ->where('status', 2)
    ->delete();
   
    foreach ($departmentToadd as $departmentId) {
        Collegedep::create([
            'college_id' => $request->editcollegeid,
            'department' => $departmentId
        ]);
    }
        Session::flash("success", "Successfully Updated");

        return redirect("/manage_college");
    }

    public function addCollege(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        $user = Auth::user();
        if (!in_array($user->role, [1, 12])) {
            return redirect('/accessdenied');
        }
        $data = [
            'title' => 'Add College',
            'states' => State::all(),
            'depts' => Department::where('status', 1)->get()
        ];
        return view('add_college', $data);
    }

public function save_college(Request $request)
{
    if (!Auth::check()) {
        return redirect('/');
    }

    $user = Auth::user();

    if (!in_array($user->role, [1, 12])) {
        return redirect('/accessdenied');
    }

    $collegeName = strtoupper($request->get('college'));
    $collegeState = $request->get('collegestate');
    $collegeDistrict = $request->get('collegedist');
    $collegeCity = strtoupper($request->get('collegeplace'));

    
    $existingCollege = College::where('college', $collegeName)
        ->where('district', $collegeDistrict)
        ->exists();

    if (!$existingCollege) {
        
            $collegeId = College::insertGetId([
                'college' => $collegeName,
                'state' => $collegeState,
                'district' => $collegeDistrict,
                'city' => $collegeCity,
            ]);

            $departments = $request->get('collegedep');

            if (!empty($departments)) {
                $data = [];
                foreach ($departments as $department) {
                   
                        $data[] = [
                            'college_id' => $collegeId,
                            'department' => $department,
                        ];
                    }
                }

               
                if (!empty($data)) {
                    Collegedep::insert($data);
                }
                Session::flash('success', 'Successfully Saved');
            }
 else {
        Session::flash('errormessage', 'College already exists');
    }

    return redirect('/manage_college');
}

    //College


    public function getDistricts(Request $request)
    {

        $districts = District::where('state_id',$request->state_id)->get();
        return response()->json(['districts' => $districts]);
    }


    public function manageMaster()
    {

        if (Auth::check()) {


            $employeeId = Auth::user()->id;
            $empprivillage = CustomHelper::employeeprivillages($employeeId);

            if (Auth::user()->role == 1 || (Auth::user()->role == 12 && $empprivillage['employeeprivillege']['add_employee'] == 1)) {


                $data['title'] = 'Manage Master';
                $data["emplist"] = User::where("role", 12)
                    ->join('employee_info', 'employee_info.user_id', '=', 'users.id')
                    ->join('employee_privillege', 'employee_privillege.empid', '=', 'users.id')
                    ->where(function ($query) {
                        if (Auth::user()->role == 12) {
                            $query->where('employee_info.created_userid', Auth::user()->id);
                        }
                    })
                    ->orderby("users.id", "desc")
                    ->get(['users.*', 'employee_info.empid', 'employee_info.office_email', 'employee_info.poc_code', 'employee_privillege.add_employee as addemployee_privilage']);
                return view("manage_master", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }









    public function departmentPoc()
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        if (Auth::user()->role !== 1) {
            return redirect('/accessdenied');
        }
        $departments = Departmentpoc::orderBy('id', 'desc')->get();
        $data = [
            'departments' => $departments,
            'title' => 'Add Department Poc',
        ];
        return view('add_departmentPoc', $data);
    }
    
    public function manageDepartment()
    {  
        if (!Auth::check()) {
            return redirect('/');
        }
        $user = Auth::user();
        if ($user->role !== 1) {
            return redirect('/accessdenied');
        }
        $departments = Department::orderBy('id', 'desc')->get();
    
        return view('manage_department', [
            'title' => 'Manage Department',
            'deplist' => $departments
        ]);
    }

 public function saveDepartment(Request $request)
{
  
    if (!Auth::check()) {
        return redirect('/');
    }
    $user = Auth::user();
    if ($user->role !== 1) {
        return redirect('/accessdenied');
    }
    $depName = strtoupper($request->get('department'));
    $existingDepartment = Department::where('department', $depName)->exists();

    if ($existingDepartment) {
        Session::flash('errormessage', 'Department already exists.');
    } else {
        Department::create([
            'department' => $depName,
        ]);

        Session::flash('success', 'Department successfully saved.');
    }
    return redirect('/manage_department');
}

public function editDepartment(Request $request)
{
    
    if (!Auth::check()) {
        return redirect('/');
    }

    $user = Auth::user();

   
    if ($user->role !== 1) {
        return redirect('/accessdenied');
    }

    
    $departmentId = $request->editdepid;
    $departmentName = strtoupper($request->editdepname);

  
    Department::where('id', $departmentId)
        ->update(['department' => $departmentName]);

    Session::flash('success', 'Department successfully updated.');
    return redirect('/manage_department');
}






















    public function activateDeactivesingle(Request $request)
    {
       
        $validated = $request->validate([
            'action' => 'required|string|in:activate,deactivate',
            'id' => 'required|integer', 
            'type' => 'required|string|in:topic,subtopic'
        ]);
        $status = ($validated['action'] == 'activate') ? 1 : 0;
        if($validated['type'] == 'topic'){
            $result = $this->EvacommonController->activateSingle('topic', $validated['id'], $status);
 
        }else{
            $result = $this->EvacommonController->activateSingle('subtopic', $validated['id'], $status);

        }

       
        if ($result) {
            return response()->json(['success' => true, 'status' => $status]);
        } else {
            return response()->json(['success' => false, 'message' => 'Update failed or no change made.'], 400);
        }
    
}


public function activateDeactivemultiple(Request $request) {
    $validated = $request->validate([
        'Ids' => 'required|array',
        'action' => 'required|in:activate,deactivate',
        'type' => 'required|string|in:topic,subtopic',
    ]);

    $status = ($validated['action'] == 'activate') ? 1 : 0;

    $results = [];

    foreach ($validated['Ids'] as $id) {
        if ($validated['type'] == 'topic') {
            $result = $this->EvacommonController->activateSingle('topic', $id, $status);
        } else {
            $result = $this->EvacommonController->activateSingle('subtopic', $id, $status);
        }

        $results[] = $result;
    }

    if (in_array(true, $results, true)) {
        return response()->json(['success' => true, 'status' => $status]);
    } else {
        return response()->json(['success' => false, 'message' => 'Update failed or no change made.']);
    }
}










    





   







    public function toggleMultipleStatus(Request $request)
    {
        $request->validate([
            'topicIds' => 'required|array',
            'action' => 'required|in:activate,deactivate',
            'type' => 'required|string',
        ]);
        $type = $request->input('type');
        $topicIds = $request->input('topicIds'); 
        $action = $request->input('action'); 

        if($action === 'activate'){ 
            $newStatus = 1;
        }else{
            $newStatus = 0;
        }
        if($type == "topic"){
            Topic::whereIn('id', $topicIds)->update(['status' => $newStatus]);

        }else{
            Subtopic::whereIn('id', $topicIds)->update(['status' => $newStatus]);
            $parentTopicId = Subtopic::whereIn('id', $topicIds)->value('topic_id');

        }

        return response()->json([
            'success' => true,
            'message' => "Topics successfully $action+d.",
            'topicIds' => $topicIds, 
            'parentTopicId' => isset($parentTopicId) ? $parentTopicId : null
        ]);    }





























    public function subtopicView($pac_id,$topic_id)
    {

        if (Auth::check()) {
            if (Auth::user()->role == 1 || 3) {

                
                $data["title"] = "Subtopic View";
                $data['pac_id'] = $pac_id;
                $data['topic_id'] = $topic_id;
                $data['package'] = Package::where('pac_id', $pac_id)->first();

                $data['TopicList'] = Topic::join('ev_subtopics', 'ev_topics.id', '=', 'ev_subtopics.topic_id')

                    ->where('ev_topics.id', $topic_id)
                    ->where(function ($query) {
                        if (Auth::user()->role == 3) {
                            $query->where('ev_topics.trainer_id', Auth::user()->id);
                        }
                    })
                    ->get([
                        'ev_topics.id as topic_id',
                        'ev_topics.topics',
                        'ev_subtopics.id as subtopic_id',
                        'ev_subtopics.sub_topics',
                        'ev_subtopics.status as subtopic_status',
                        'ev_topics.status as topic_status'
                    ]);

                return view('subtopic_view', $data);
            } else {
                return redirect('/accessdenied');
            }
        } else {
            return redirect('/');
        }
    }



    public function saveReissueGatepass(Request $request)
    {  
        if (!Auth::check()) {
            return redirect('/');
        }
        $user = Auth::user();
        if (!in_array($user->role, [4, 8])) {
            return redirect('/accessdenied');
        }
        $gatepass = Gatepass::find($request->get("id"));
        if (!$gatepass) {
            return redirect()->back()->withErrors(['Gatepass not found.']);
        }
        $data = [
            'user_id'        => $request->get("student_id"),
            'start_date'     => $this->formatDate($request->get("redate1")),
            'end_date'       => $this->formatDate($request->get("redate2")),
            'type'           => $gatepass->type,
            'type_id'        => $gatepass->type_id,
            'sales_id'       => $gatepass->sales_id,
            'status'         => 4,
            'photo'          => $gatepass->photo,
            'requested_date' => date("Y-m-d"),
            'reissued_by'    => $user->id,
        ];
        Gatepass::insert($data);
        Gatepass::where("id", $request->get("id"))->update([
            'reissue_status' => '1'
        ]);
    

       
    }
    
  
    private function formatDate($date)
    {
        return date("Y-m-d", strtotime($date));
    }



    public function reissuedGatepass(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        $user = Auth::user();

       
        if (!in_array($user->role, [1, 2, 4, 8])) {
            return redirect('/accessdenied');
        }

        $today = Carbon::today();
        $fiveDaysFromToday = Carbon::today()->addDays(4);

       
        $year = $request->query('year', Carbon::now()->year);
        $month = $request->query('month', Carbon::now()->format('m'));

       
        if (!checkdate($month, 1, $year)) {
            return redirect('/accessdenied');
        }

        $startOfMonth = Carbon::create($year, $month, 1)->startOfMonth()->format('Y-m-d');
        $endOfMonth = Carbon::create($year, $month, 1)->endOfMonth()->format('Y-m-d');



        $gatepassRequests = Gatepass::join('users', 'users.id', '=', 'ev_gatepassrequest.user_id')
            ->where('ev_gatepassrequest.status', '4')
            ->whereBetween('ev_gatepassrequest.requested_date', [$startOfMonth, $endOfMonth])
            ->when($user->role == 2, function ($query) use ($user) {
                $query->where('ev_gatepassrequest.sales_id', $user->id);
            })
            ->orderBy('ev_gatepassrequest.id', 'desc')
            ->get(['ev_gatepassrequest.*', 'users.name']);

      
        $data = [
            'title' => 'Reissued Gate Pass',
            'gatelist' => $gatepassRequests,
            'years' => CommonController::generateYearRange(date('Y') - 10),
            'months' => CommonController::generateMonthOptions(),
            'currentYear' => $year,
            'currentMonth' => $month,
        ];

        return view('reissued_gatepass', $data);
    }


    public function topicActivate(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $id = $request->get("id");
                $pac_id = $request->get("pac_id");
                Subtopic::where('id', $id)->update(['status' => 1]);
             
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
}
    public function topicDeactivate(Request $request)
    {
        if (Auth::check()) {
                    if (Auth::user()->role == 1) {
                        $id = $request->get("id");
                        $pac_id = $request->get("pac_id");

                         Subtopic::where('id', $id)->update(['status' => 0]);
                       
                        //  return redirect('/syllabusview/'.$pac_id);
                        echo $pac_id;

        
                    } else {
                        return redirect("/acessdenied");
                    }
                } else {
                    return redirect("/");
                }
        
    }

    public function topicDelete(Request $request){
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $id = $request->get("id");
                $pac_id = $request->get("pac_id");
    
                $subtopic = Subtopic::find($id);
                $topic_id = $subtopic->topic_id;
    
                Subtopic::where('id', $id)->delete();
    
                $remainingSubtopics = Subtopic::where('topic_id', $topic_id)->count();
                if ($remainingSubtopics == 0) {
                    Topic::where('id', $topic_id)->delete();
                }
                

                // return redirect('/syllabusview/'.$pac_id);

            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }

    }
    public function syllabusViewAdmin($id)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1 || 3) {



                $data["title"] = "Syllabus View";
                $data['pac_id'] = $id;
                $data['package'] = Package::where('pac_id', $id)->first();

                $data['TopicList'] = Topic::where('pac_id', $id)
                ->where(function ($query) {
                    if (Auth::user()->role == 3) {
                        $query->where('trainer_id', Auth::user()->id);
                    }
                })
                ->get([
                    'id as topic_id',
                    'topics',
                    'status as topic_status'
                ]);

                return view('syllabusView', $data);
            } else {
                return redirect('/accessdenied');
            }
        } else {
            return redirect('/');
        }
    }
 public function expiringGatepass(Request $request)
{
    if (!Auth::check()) {
        return redirect('/');
    }
    $user = Auth::user();
    // Check user roles
    if (!in_array($user->role, [1, 2, 4, 8])) {
        return redirect('/accessdenied');
    }
    $noDays = $request->input('no_days', 5);
    $today = Carbon::today();

    // Query to get expiring gatepass requests
    $gatepassRequests = Gatepass::join('users', 'users.id', '=', 'ev_gatepassrequest.user_id')
    ->whereIn('ev_gatepassrequest.status', [1, 4])
        ->when($noDays != 5, function ($query) use ($today, $noDays) {
            $targetDate = $today->copy()->addDays($noDays);
            return $query->whereDate('ev_gatepassrequest.end_date', $targetDate);
        })
        ->when($noDays == 5, function ($query) use ($today) {
            $targetDate = $today->copy()->addDays(4);
            return $query->whereBetween('ev_gatepassrequest.end_date', [$today, $targetDate]);
        })
        ->when($user->role == 2, function ($query) use ($user) {
            return $query->where('ev_gatepassrequest.sales_id', $user->id);
        })
        ->orderBy('ev_gatepassrequest.id', 'desc')
        ->get(['ev_gatepassrequest.*', 'users.name']);

    // Prepare data for the view
    $data = [
        'title' => 'Expiring Gate Pass',
        'gatelist' => $gatepassRequests,
        'nodays' => $noDays,
    ];

    return view('expiring_gatepass', $data);
}

    public function manageRecruitment()
    {
             
        if (Auth::check()) {

            
            $employeeId = Auth::user()->id;
            $empprivillage = CustomHelper::employeeprivillages($employeeId);
           
        if (Auth::user()->role == 1 || (Auth::user()->role == 10 && $empprivillage['employeeprivillege']['add_employee'] == 1)) {


                $data['title'] = 'Manage Recruitment';
                $data["emplist"] = User::where("role", 10)
                ->join('employee_info', 'employee_info.user_id', '=', 'users.id')
                ->join('employee_privillege', 'employee_privillege.empid', '=', 'users.id')
                    ->where(function ($query) {
                        if (Auth::user()->role == 10) {
                            $query->where('employee_info.created_userid', Auth::user()->id);
                        }
                    })
                    ->orderby("users.id", "desc")
                    ->get(['users.*','employee_info.empid','employee_info.office_email','employee_info.poc_code','employee_privillege.add_employee as addemployee_privilage']);
                return view("manage_recruitment", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }  }

  


public function addEmail()
{
    if (!Auth::check()) {
        return redirect('/');
    }

    if (Auth::user()->role != 1) {
        return redirect('/accessdenied');
    }

    $data = [
        'title' => 'Add Email',
        'emailist' => Email::orderBy('id', 'asc')->get(),
    ];

    return view('add_email', $data);
}

public function saveEmails(Request $request)
{
    if (!Auth::check()) {
        return redirect('/');
    }

    if (Auth::user()->role != 1) {
        return redirect('/accessdenied');
    }

    $emails = $request->input('emails');

    // Clear existing emails
    Email::truncate();

    if (!empty($emails)) {
        $data = array_map(fn($email) => ['email' => $email], $emails);
        Email::insert($data);
    }

    Session::flash('success', 'Successfully Saved');
    return redirect('/add_email');
}

 
    public function bulkCompanyUpload()
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        $user = Auth::user();
        if (in_array($user->role, [1, 6, 9])) {
            return view('add_companybulk', ['title' => 'Company Bulk Upload']);
        }

        return redirect('/accessdenied');
    }

    public function save_company(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1 || Auth::user()->role == 9 || Auth::user()->role == 6) {
                $data['name'] = $request->get("name");
                $data['contact_name'] = $request->get("cpname");
                $data['contact_number'] = $request->get("cnumber");
                $data['email'] = $request->get("cemail");
                $data['created_userid'] = Auth::user()->id;
               
                $logo = $request->file("file");

                if ($request->hasFile('file')) {
                    $compLogo = $logo->getClientOriginalName();
                    $logo->move(public_path('uploads/logo'), $compLogo);
                    $data['company_logo'] = $compLogo;
                } else {
                    $data['company_logo'] = '';
                }

                $countNumber = Company::where('contact_number', $data['contact_number'])->count();
                $countEmail = Company::where('email', $data['email'])->count();
                if ($countNumber > 0) {
                    echo 1;
                } elseif ($countEmail > 0) {
                    echo 2;
                } elseif ($countEmail == 0 && $countNumber == 0) {
                    Company::insert($data);
                    echo 3;
                }
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function add_email()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $data['title'] = 'Add Email';
                $data["emailist"] = Email::orderby("id", "desc")->get();
                return view("add_email", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function save_emails(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role ==1) {
                $emails = $request->get("emails");

                Email::truncate();

                if (!empty($emails)) {
                    foreach ($emails as $emails) {
                        $data['email'] = $emails;

                        Email::insert($data);
                    }
                }
                Session::flash('success', 'Sucessfully Saved');
                return redirect("/add_email");
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function manage_company(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1 || Auth::user()->role == 9 || Auth::user()->role == 6 ) {

                $year = $request->query('year', Carbon::now()->year);
                $month = $request->query('month', Carbon::now()->format('m'));

                 // Calculate start and end of the month
                 $startOfMonth = Carbon::create($year, $month, 1)->startOfMonth()->format('Y-m-d');
                 $endOfMonth = Carbon::create($year, $month, 1)->endOfMonth()->format('Y-m-d');


               
                $data1['complist'] = Company::orderby("id", "desc")
                                            ->whereBetween('ev_company.created_at', [$startOfMonth, $endOfMonth])
                                            ->where(function ($query) {
                                                $query->orWhere('ev_company.status',0)
                                                      ->orWhere('ev_company.status',1);
                                            })->get();

              $data = [
                            
                  'years' => CommonController::generateYearRange(date('Y') - 10),
                   'months' => CommonController::generateMonthOptions(),
                   'currentYear' => $year,
                   'currentMonth' => $month,                   
                   'title'=>'Manage Company',
                   'complist'=>   $data1['complist'],                                              
               ];                                                                     
                return view("manage_company", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }
    public function add_company()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1 || Auth::user()->role == 9 || Auth::user()->role == 6 ) {
                $data['title'] = 'Add Company';

                return view("add_company", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function edit_company($compid)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1 || Auth::user()->role == 9 || Auth::user()->role == 6 ) {
                $data['title'] = 'Edit Company';
                $data['compdetails'] = Company::where('id', $compid)->first();

                return view("edit_company", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function update_company(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1 || Auth::user()->role == 9 || Auth::user()->role == 6 ) {
                $data['name'] = $request->get("name");
                $data['contact_name'] = $request->get("cpname");
                $data['contact_number'] = $request->get("cnumber");
                $data['email'] = $request->get("cemail");
                $data['updated_userid'] = Auth::user()->id;
                $logo = $request->file("file");

                if ($request->hasFile('file')) {
                    $compLogo = $logo->getClientOriginalName();
                    $logo->move(public_path('uploads/logo'), $compLogo);
                    $data['company_logo'] = $compLogo;
                } else {
                    $files = Company::where('contact_number', $data['contact_number'])
                        ->where('email', $data['email'])
                        ->first(['company_logo']);
                    $data['company_logo'] = $files->company_logo;
                }

                Company::where('id', $request->get("id"))->update($data);
                Session::flash('success', 'Sucessfully Updated');
                return redirect('/manage_company');
            } else {
                return redirect('/acessdenied');
            }
        } else {
            return redirect('/');
        }
    }

    public function checkphone(Request $request)
    {
        $no = $request->get("no");

        $count = Company::where('contact_number', $no)->count();
        if ($count == 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function checkemail(Request $request)
    {
        $email = $request->get("email");

        $count = Company::where('email', $email)->count();
        if ($count == 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function activate_company(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1 || Auth::user()->role == 9 || Auth::user()->role == 6 ) {
                $id = $request->get("id");
                $data["status"] = 1;
                Company::where("id", $id)->update($data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }
    public function deactivate_company(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1 || Auth::user()->role == 9 || Auth::user()->role == 6 ) {
                $id = $request->get("id");
                $data["status"] = 0;
                Company::where("id", $id)->update($data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function employeelisting(Request $request)
    {
        $emptype = $request->get("emptype");

        $data["username"] = User::where('role', $emptype)
            ->where('status', 1)
            ->get(['id', 'name']);
        echo json_encode($data);
    }

    public function rejected_paymentrequest(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1 || Auth::user()->role == 2) {


              

                  // Get year and month from query parameters with default values
                  $year = $request->query('year', Carbon::now()->year);
                  $month = $request->query('month', Carbon::now()->format('m'));

                    // Calculate start and end of the month
                $startOfMonth = Carbon::create($year, $month, 1)->startOfMonth()->format('Y-m-d');
                $endOfMonth = Carbon::create($year, $month, 1)->endOfMonth()->format('Y-m-d');
  
                $data1["paylist"] = Paymentrequest::join('users', 'users.id', '=', 'ev_paymentrequest.student_id')
                                                ->whereBetween('ev_paymentrequest.date', [$startOfMonth, $endOfMonth])
                                                ->where('ev_paymentrequest.status',2)
                                                    ->where(function ($query) {
                                                        if (Auth::user()->role == 2) {
                                                            $query->where('ev_paymentrequest.sales_id', Auth::user()->id);
                                                        }
                                                    })
                    ->orderby("ev_paymentrequest.id", "desc")
                    
                    ->get(['users.name', 'ev_paymentrequest.*']);

                    $data = [
                            
                        'years' => CommonController::generateYearRange(date('Y') - 10),
                        'months' => CommonController::generateMonthOptions(),
                        'currentYear' => $year,
                        'currentMonth' => $month,                        
                        'title'=>'Rejected Payment Request',
                        'paylist'=>$data1["paylist"],                      
                    ];
                return view("rejected_paymentrequest", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }
    public function pending_paymentrequest(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1 || Auth::user()->role == 2) {


                $data['title'] = 'Pending Payment Request';

                  // Get year and month from query parameters with default values
                  $year = $request->query('year', Carbon::now()->year);
                  $month = $request->query('month', Carbon::now()->format('m'));

                    // Calculate start and end of the month
                $startOfMonth = Carbon::create($year, $month, 1)->startOfMonth()->format('Y-m-d');
                $endOfMonth = Carbon::create($year, $month, 1)->endOfMonth()->format('Y-m-d');
  



                $data1["paylist"] = Paymentrequest::join('users', 'users.id', '=', 'ev_paymentrequest.student_id')
                                                ->whereBetween('ev_paymentrequest.date', [$startOfMonth, $endOfMonth])
                                                ->where('ev_paymentrequest.status',0)
                                                    ->where(function ($query) {
                                                        if (Auth::user()->role == 2) {
                                                            $query->where('ev_paymentrequest.sales_id', Auth::user()->id);
                                                        }
                                                    })
                    ->orderby("ev_paymentrequest.id", "desc")
                    
                    ->get(['users.name', 'ev_paymentrequest.*']);

                    $data = [
                            
                        'years' => CommonController::generateYearRange(date('Y') - 10),
                        'months' => CommonController::generateMonthOptions(),
                        'currentYear' => $year,
                        'currentMonth' => $month,                        
                        'title'=>'Pending Payment Request',
                        'paylist'=>$data1["paylist"],                      
                    ];

                return view("pending_paymentrequest", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function approved_paymentrequest(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1 || Auth::user()->role == 2) {


                $data['title'] = 'Pending Payment Request';

                  // Get year and month from query parameters with default values
                  $year = $request->query('year', Carbon::now()->year);
                  $month = $request->query('month', Carbon::now()->format('m'));

                    // Calculate start and end of the month
                $startOfMonth = Carbon::create($year, $month, 1)->startOfMonth()->format('Y-m-d');
                $endOfMonth = Carbon::create($year, $month, 1)->endOfMonth()->format('Y-m-d');
  



                $data1["paylist"] = Paymentrequest::join('users', 'users.id', '=', 'ev_paymentrequest.student_id')
                                                ->whereBetween('ev_paymentrequest.date', [$startOfMonth, $endOfMonth])
                                                ->where('ev_paymentrequest.status',1)
                                                    ->where(function ($query) {
                                                        if (Auth::user()->role == 2) {
                                                            $query->where('ev_paymentrequest.sales_id', Auth::user()->id);
                                                        }
                                                    })
                    ->orderby("ev_paymentrequest.id", "desc")
                    
                    ->get(['users.name', 'ev_paymentrequest.*']);

                    $data = [
                            
                        'years' => CommonController::generateYearRange(date('Y') - 10),
                        'months' => CommonController::generateMonthOptions(),
                        'currentYear' => $year,
                        'currentMonth' => $month,                        
                        'title'=>'Approved Payment Request',
                        'paylist'=>$data1["paylist"],                      
                    ];

                return view("approved_paymentrequest", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }


    public function approve_payment(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1 || Auth::user()->role == 2) {
                $id = $request->get("id");
                $data["status"] = 1;
                $data["action_by"] = Auth::user()->id;

                $requestdetails = Paymentrequest::where('id', $id)->first();
                $studentpackage_id = $requestdetails['studentpackage_id'];
                $studentpackage = Studentpackage::join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
                    ->where('ev_student_package.id', $studentpackage_id)
                    ->first([
                        'ev_student_package.totalfees_afterreduction',
                        'ev_student_package.package_id',
                        'ev_student_package.reference_type',
                        'ev_student_package.refence_id',
                        'ev_student_package.student_id',
                        'ev_student_package.package_type',
                    ]);

                $datatax['tax'] = Tax::find(1)->tax;
                $amount = $requestdetails['amount'];
                $payment_type = $requestdetails['payment_method'];
                $student_id = $requestdetails['student_id'];
                $sales_id = $requestdetails['sales_id'];

                $pac_type = $studentpackage['package_type'];
                $referenece_type = $studentpackage['reference_type'];
                $fullamount = $studentpackage['totalfees_afterreduction'];
                $pac_id = $studentpackage['package_id'];

                $date = date("Y-m-d");

                if ($payment_type == 'bank') {
                    $transaction_id = $requestdetails['transaction_id'];

                    $imageName = $requestdetails['payment_screenshort'];
                } else {
                    $transaction_id = '';
                    $imageName = '';
                }

                $sumofpaidamount = round(
                    Payment::where('student_id', $student_id)
                        ->where('package_type', $pac_type)
                        ->where('package_id', $pac_id)
                        ->sum('paid_amount')
                );
                $pendingamount = $fullamount - ($sumofpaidamount + $amount);
                $total = $amount;

                if ($pac_type == 1) {
                    $splitup = Studentfeessplit::where('student_id', $student_id)
                        ->where('package_type', 1)
                        ->where('package_id', $pac_id)
                        ->get();

                    foreach ($splitup as $split) {
                        $pendingfees = $split->pendingfees;
                        if ($pendingfees != 0) {
                            if ($total > $split->pendingfees) {
                                Studentfeessplit::where('id', $split->id)->update(['paidfees' => $split->fees, 'pendingfees' => 0]);
                                $total -= $split->pendingfees;
                            } else {
                                if ($total <= $split->pendingfees) {
                                    Studentfeessplit::where('id', $split->id)->update(['paidfees' => $split->paidfees + $total, 'pendingfees' => $split->pendingfees - ($split->pendingfeespaidfees + $total)]);
                                    $total = 0;
                                }
                            }
                        }
                    }
                }


               
                if ($referenece_type == 1) {
                    //payment transfer to sales

                    $x = 1 + $datatax['tax'] / 100;
                    $income_sales = $amount / 2 / $x;
                    $income_ref = $amount / 2 / $x;
                    $taxval_sale = $amount / 2 - $income_sales;
                    $taxval_ref = $amount / 2 - $income_ref;
                    $reference_id = $studentpackage['refence_id'];

                    $income = round($amount / $x);
                    $taxval = round($amount - $income);
                    $payment_id = Payment::insertGetId([
                        'transaction_id' => $transaction_id,
                        'package_id' => $pac_id,
                        'student_id' => $student_id,
                        'package_type' => $pac_type,
                        'screenshot' => $imageName,
                        'payment_type' => $payment_type,
                        'paid_amount' => $amount,
                        'pending_amount' => $pendingamount,
                        'date' => $date,
                        'fees_income' => round($income),
                        'tax' => round($taxval),
                    ]);

                   
                    Salespayment::insert([
                        'student_id' => $student_id,
                        'package_id' => $pac_id,
                        'package_type' => $pac_type,
                        'sales_id' => $sales_id,
                        'payment' => round($income_sales),
                        'tax' => round($taxval_sale),
                        'payment_id' => $payment_id,
                        'total' => round($amount / 2),
                        'date' => $date,
                    ]);


                   
                    Salespayment::insert([
                        'student_id' => $student_id,
                        'package_id' => $pac_id,
                        'package_type' => $pac_type,
                        'sales_id' => $reference_id,
                        'payment' => round($income_ref),
                        'tax' => round($taxval_ref),
                        'payment_id' => $payment_id,
                        'total' => round($amount / 2),
                        'date' => $date,
                    ]);

                   


                   

                   
                } else {
                    $x = 1 + $datatax['tax'] / 100;
                    $income = round($amount / $x, 2);
                    $taxval = round($amount - $income, 2);
                    $payment_id = Payment::insertGetId([
                        'transaction_id' => $transaction_id,
                        'package_id' => $pac_id,
                        'student_id' => $student_id,
                        'package_type' => $pac_type,
                        'screenshot' => $imageName,
                        'payment_type' => $payment_type,
                        'paid_amount' => $amount,
                        'pending_amount' => $pendingamount,
                        'date' => $date,
                        'fees_income' => round($income),
                        'tax' => round($taxval),
                    ]);
                    Salespayment::insert([
                        'student_id' => $student_id,
                        'package_id' => $pac_id,
                        'payment_id' => $payment_id,
                        'package_type' => $pac_type,
                        'sales_id' => $sales_id,
                        'payment' => round($income),
                        'tax' => round($taxval),
                        'total' => round($amount),
                        'date' => $date,
                    ]);
                }

                $data['payment_id'] = $payment_id;

                Paymentrequest::where("id", $id)->update($data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function reject_payment(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1 || Auth::user()->role == 2) {
                $id = $request->get("id");
                $data["status"] = 2;
                $data['reason'] = $request->get("inputValue");
                $data["action_by"] = Auth::user()->id;
                Paymentrequest::where("id", $id)->update($data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function manage_oldstudent(Request $request)
    {
        if (Auth::check()) {

            // Get year and month from query parameters with default values
            $year = $request->query('year', Carbon::now()->year);
            $month = $request->query('month', Carbon::now()->format('m'));
            // Calculate start and end of the month
            $startOfMonth = Carbon::create($year, $month, 1)
                                ->startOfMonth()
                                ->format('Y-m-d');
             $endOfMonth = Carbon::create($year, $month, 1)
                                ->endOfMonth()
                                ->format('Y-m-d');

            if (Auth::user()->role == 1 || Auth::user()->role == 2) {
               
                $data1["studlist"] = Oldstudent::whereBetween('ev_oldstudents.date_joining', [$startOfMonth, $endOfMonth])
                                            ->where(function ($query) {
                                                if (Auth::user()->role == 2) {
                                                    $query->where('ev_oldstudents.sales_id', Auth::user()->id);
                                                }
                                            })->orderby("id", "desc")->get();

                $data = ['title' => 'Upload Old Students',
                        'studlist'=> $data1['studlist'],
                        'years' => CommonController::generateYearRange(date('Y') - 10),
                        'months' => CommonController::generateMonthOptions(),
                        'currentYear' => $year,
                         'currentMonth' => $month,
                        ];


                return view("manage_oldstudent", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }




    }

    public function manage_management()
    {
        if (Auth::check()) {

            $employeeId = Auth::user()->id;
            $empprivillage = CustomHelper::employeeprivillages($employeeId);


            if (Auth::user()->role == 1 || (Auth::user()->role == 6 && $empprivillage['employeeprivillege']['add_employee'] == 1)) {
           
                $data['title'] = 'Manage Management';
                $data["emplist"] = User::where("role", 6)
                ->join('employee_info', 'employee_info.user_id', '=', 'users.id')
                ->join('employee_privillege', 'employee_privillege.empid', '=', 'users.id')
                
                    ->where(function ($query) {
                        if (Auth::user()->role == 6) {
                            $query->where('employee_info.created_userid', Auth::user()->id);
                        }
                    })
                    ->orderby("users.id", "desc")
                    ->get(['users.*','employee_info.empid','employee_info.office_email','employee_info.poc_code','employee_privillege.add_employee as addemployee_privilage']);
                return view("manage_management", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function manage_finance()
    {
        if (Auth::check()) {


            $employeeId = Auth::user()->id;
            $empprivillage = CustomHelper::employeeprivillages($employeeId);

            if (Auth::user()->role == 1 || (Auth::user()->role == 7 && $empprivillage['employeeprivillege']['add_employee'] == 1)) {



                $data['title'] = 'Manage Finance';
                $data["emplist"] = User::where("role", 7)
                ->join('employee_info', 'employee_info.user_id', '=', 'users.id')
                ->join('employee_privillege', 'employee_privillege.empid', '=', 'users.id')

                    ->where(function ($query) {
                        if (Auth::user()->role == 7) {
                            $query->where('employee_info.created_userid', Auth::user()->id);
                        }
                    })
                    ->orderby("users.id", "desc")
                    ->get(['users.*','employee_info.empid','employee_info.office_email','employee_info.poc_code','employee_privillege.add_employee as addemployee_privilage']);
                return view("manage_finance", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function manage_subadmin()
    {
        if (Auth::check()) {

            
            $employeeId = Auth::user()->id;
            $empprivillage = CustomHelper::employeeprivillages($employeeId);
           
                       if (Auth::user()->role == 1 || (Auth::user()->role == 8 && $empprivillage['employeeprivillege']['add_employee'] == 1)) {



                $data['title'] = 'Manage Subadmin';
                $data["emplist"] = User::where("role", 8)
                ->join('employee_info', 'employee_info.user_id', '=', 'users.id')
                ->join('employee_privillege', 'employee_privillege.empid', '=', 'users.id')
                    ->where(function ($query) {
                        if (Auth::user()->role == 8) {
                            $query->where('employee_info.created_userid', Auth::user()->id);
                        }
                    })
                    ->orderby("users.id", "desc")
                    ->get(['users.*','employee_info.empid','employee_info.office_email','employee_info.poc_code','employee_privillege.add_employee as addemployee_privilage']);
                return view("manage_subadmin", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function manage_placement()
    {
        if (Auth::check()) {


           $employeeId = Auth::user()->id;
 $empprivillage = CustomHelper::employeeprivillages($employeeId);

            if (Auth::user()->role == 1 || (Auth::user()->role == 9 && $empprivillage['employeeprivillege']['add_employee'] == 1)) {



                $data['title'] = 'Manage Placement';
                $data["emplist"] = User::where("role", 9)
                ->join('employee_info', 'employee_info.user_id', '=', 'users.id')
                ->leftjoin('employee_privillege', 'employee_privillege.empid', '=', 'users.id')

                    ->where(function ($query) {
                        if (Auth::user()->role == 9) {
                            $query->where('employee_info.created_userid', Auth::user()->id);
                        }
                    })
                    ->orderby("users.id", "desc")
                    ->get(['users.*','employee_info.empid','employee_info.office_email','employee_info.poc_code','employee_privillege.add_employee as addemployee_privilage']);
                return view("manage_placement", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function bulk_employee(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $data["designationlist"] = Designation::where("status", 1)
                    ->orderby("id", "desc")
                    ->get();
                $data["technologylist"] = Technology::where("status", 1)
                    ->orderby("id", "desc")
                    ->get();
                $data["username"] = User::whereIn('role', [2, 3, 4])
                    ->where('status', 1)
                    ->get(['id', 'name']);

                return view("bulk_employee", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function pendingGatepass(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
    
        $user = Auth::user();
    
        // Check user roles
        if (!in_array($user->role, [1, 2, 4, 8])) {
            return redirect('/accessdenied');
        }
    
        // Get year and month from query parameters with default values
        $year = $request->query('year', Carbon::now()->year);
        $month = $request->query('month', Carbon::now()->format('m'));
    
        // Validate year and month
        if (!checkdate($month, 1, $year)) {
            return redirect('/accessdenied');
        }
    
        // Calculate start and end of the month
        $startOfMonth = Carbon::create($year, $month, 1)->startOfMonth()->format('Y-m-d');
        $endOfMonth = Carbon::create($year, $month, 1)->endOfMonth()->format('Y-m-d');
    
        // Query to get pending gatepass requests
        $gatepassRequests = Gatepass::join('users', 'users.id', '=', 'ev_gatepassrequest.user_id')
            ->where('ev_gatepassrequest.status', '0')
            ->whereBetween('ev_gatepassrequest.requested_date', [$startOfMonth, $endOfMonth])
            ->when($user->role == 2, function ($query) use ($user) {
                $query->where('ev_gatepassrequest.sales_id', $user->id);
            })
            ->orderBy('ev_gatepassrequest.id', 'desc')
            ->get(['ev_gatepassrequest.*', 'users.name']);
    
        // Prepare data for the view
        $data = [
            'title' => 'Pending Gate Pass Request',
            'gatelist' => $gatepassRequests,
            'years' => CommonController::generateYearRange(date('Y') - 10),
            'months' => CommonController::generateMonthOptions(),
            'currentYear' => $year,
            'currentMonth' => $month,
        ];
    
        return view('pending_gatepass', $data);
    }

    public function returnedGatepass(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
    
        $user = Auth::user();
    
        // Check user roles
        if (!in_array($user->role, [1, 2, 4, 8])) {
            return redirect('/accessdenied');
        }
    
        // Get year and month from query parameters with default values
        $year = $request->query('year', Carbon::now()->year);
        $month = $request->query('month', Carbon::now()->format('m'));
    
        // Validate year and month
        if (!checkdate($month, 1, $year)) {
            return redirect('/accessdenied');
        }
    
        // Calculate start and end of the month
        $startOfMonth = Carbon::create($year, $month, 1)->startOfMonth()->format('Y-m-d');
        $endOfMonth = Carbon::create($year, $month, 1)->endOfMonth()->format('Y-m-d');
    
        // Query to get pending gatepass requests
        $gatepassRequests = Gatepass::join('users', 'users.id', '=', 'ev_gatepassrequest.user_id')
            ->where('ev_gatepassrequest.status', '3')
            ->whereBetween('ev_gatepassrequest.requested_date', [$startOfMonth, $endOfMonth])
            ->when($user->role == 2, function ($query) use ($user) {
                $query->where('ev_gatepassrequest.sales_id', $user->id);
            })
            ->orderBy('ev_gatepassrequest.id', 'desc')
            ->get(['ev_gatepassrequest.*', 'users.name']);
    
        // Prepare data for the view
        $data = [
            'title' => 'Returned Gate Pass',
            'gatelist' => $gatepassRequests,
            'years' => CommonController::generateYearRange(date('Y') - 10),
            'months' => CommonController::generateMonthOptions(),
            'currentYear' => $year,
            'currentMonth' => $month,
        ];
    
        return view('returned_gatepass', $data);
    }

    public function rejectedGatepass(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
    
        $user = Auth::user();
    
        // Check user roles
        if (!in_array($user->role, [1, 2, 4, 8])) {
            return redirect('/accessdenied');
        }
    
        // Get year and month from query parameters with default values
        $year = $request->query('year', Carbon::now()->year);
        $month = $request->query('month', Carbon::now()->format('m'));
    
        // Validate year and month
        if (!checkdate($month, 1, $year)) {
            return redirect('/accessdenied');
        }
    
        // Calculate start and end of the month
        $startOfMonth = Carbon::create($year, $month, 1)->startOfMonth()->format('Y-m-d');
        $endOfMonth = Carbon::create($year, $month, 1)->endOfMonth()->format('Y-m-d');
    
        // Query to get pending gatepass requests
        $gatepassRequests = Gatepass::join('users', 'users.id', '=', 'ev_gatepassrequest.user_id')
            ->where('ev_gatepassrequest.status', '2')
            ->whereBetween('ev_gatepassrequest.requested_date', [$startOfMonth, $endOfMonth])
            ->when($user->role == 2, function ($query) use ($user) {
                $query->where('ev_gatepassrequest.sales_id', $user->id);
            })
            ->orderBy('ev_gatepassrequest.id', 'desc')
            ->get(['ev_gatepassrequest.*', 'users.name']);
    
        // Prepare data for the view
        $data = [
            'title' => 'Rejected Gate Pass Request',
            'gatelist' => $gatepassRequests,
            'years' => CommonController::generateYearRange(date('Y') - 10),
            'months' => CommonController::generateMonthOptions(),
            'currentYear' => $year,
            'currentMonth' => $month,
        ];
    
        return view('rejected_gatepass', $data);
    }


    public function approvedGatepass(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
    
        $user = Auth::user();
    
        // Check user roles
        if (!in_array($user->role, [1, 2, 4, 8])) {
            return redirect('/accessdenied');
        }
    
        // Get year and month from query parameters with default values
        $year = $request->query('year', Carbon::now()->year);
        $month = $request->query('month', Carbon::now()->format('m'));
    
        // Validate year and month
        if (!checkdate($month, 1, $year)) {
            return redirect('/accessdenied');
        }
    
        // Calculate start and end of the month
        $startOfMonth = Carbon::create($year, $month, 1)->startOfMonth()->format('Y-m-d');
        $endOfMonth = Carbon::create($year, $month, 1)->endOfMonth()->format('Y-m-d');
    
        // Query to get pending gatepass requests
        $gatepassRequests = Gatepass::join('users', 'users.id', '=', 'ev_gatepassrequest.user_id')
            ->where('ev_gatepassrequest.status', '1')
            ->whereBetween('ev_gatepassrequest.requested_date', [$startOfMonth, $endOfMonth])
            ->when($user->role == 2, function ($query) use ($user) {
                $query->where('ev_gatepassrequest.sales_id', $user->id);
            })
            ->orderBy('ev_gatepassrequest.id', 'desc')
            ->get(['ev_gatepassrequest.*', 'users.name']);
    
        // Prepare data for the view
        $data = [
            'title' => 'Approved Gate Pass Request',
            'gatelist' => $gatepassRequests,
            'years' => CommonController::generateYearRange(date('Y') - 10),
            'months' => CommonController::generateMonthOptions(),
            'currentYear' => $year,
            'currentMonth' => $month,
        ];
    
        return view('approved_gatepass', $data);
    }
    
    
    public function manage_gatepass()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 4 || Auth::user()->role == 8) {
                $data['title'] = 'Gate Pass Request';
                $data["gatelist"] = Gatepass::join("users", "users.id", "=", "ev_gatepassrequest.user_id")

                    ->where(function ($query) {
                        if (Auth::user()->role == 2) {
                            $query->where('ev_gatepassrequest.sales_id', Auth::user()->id);
                        }
                    })

                    ->orderby("ev_gatepassrequest.id", "desc")
                    ->get(['ev_gatepassrequest.*', 'users.name', 'users.name']);
                $data['today'] = date('d-m-Y');
                return view("manage_gatepass", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function approve_gatepass(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 4 || Auth::user()->role == 8) {
                $id = $request->get("id");
                $data["status"] = 1;
                $data["action_by"] = Auth::user()->id;
                Gatepass::where("id", $id)->update($data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }
    public function reject_gatepass(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 4 || Auth::user()->role == 8) {
                $id = $request->get("id");
                $reason = $request->get("inputValue");
                $data["reason"] = $reason;
                $data["status"] = 2;
                $data["action_by"] = Auth::user()->id;
                Gatepass::where("id", $id)->update($data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function manage_page_contents(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $data['title'] = 'Help Request';

                $year = $request->query('year', Carbon::now()->year);
                $month = $request->query('month', Carbon::now()->format('m'));
                $startOfMonth = Carbon::create($year, $month, 1)->startOfMonth()->format('Y-m-d');
                $endOfMonth = Carbon::create($year, $month, 1)->endOfMonth()->format('Y-m-d');


                $data1["contents"] = PageContent::join("users", "users.id", "=", "ev_pagecontents.user_id")
                    ->whereBetween('ev_pagecontents.created_at', [$startOfMonth, $endOfMonth])
                    ->orderby("id", "desc")
                    ->get(['ev_pagecontents.*', 'users.name']);

                    $data = [
                            
                        'years' => CommonController::generateYearRange(date('Y') - 10),
                        'months' => CommonController::generateMonthOptions(),
                        'currentYear' => $year,
                        'currentMonth' => $month,
                       
                        'title'=>'Help Request',
                        'contents'=>  $data1["contents"]
                        
                    ];

                return view("manage_page_contents", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function save_content(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $count = PageContent::where("type", $request->get("type"))->count();
                if ($count == 0) {
                    $data["type"] = $request->get("type");
                    $data["content"] = $request->get("content");

                    PageContent::insert($data);
                    Session::flash("success", "Successfully Saved");
                } else {
                    Session::flash("errormessage", "Already Saved");
                }

                return redirect("/manage_page_contents");
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function edit_content(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $id = $request->get("id");
                $data['contents'] = PageContent::where('id', $id)->first();
                echo json_encode($data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function update_content(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $id = $request->get("editcontentid");
                $data["type"] = $request->get("edittype");
                $data["content"] = $request->get("editcontent");

                PageContent::where("id", $id)->update($data);

                Session::flash("success", "Successfully Updated");
                return redirect("/manage_page_contents");
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function manage_settings()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $data["signaturelist"] = Signature::where('id', 1)->first();

                $data['title'] = 'Manage Settings';

                if (!empty($data['signaturelist'])) {
                    $data['name'] = $data["signaturelist"]->name;
                    $data['contact_no'] = $data["signaturelist"]->contact_no;
                    $data['header'] = $data["signaturelist"]->header;
                    $data['signature'] = $data["signaturelist"]->signature;
                    $data['seal'] = $data["signaturelist"]->seal;
                    $data['id'] = $data["signaturelist"]->id;
                } else {
                    $data['description'] = '';
                    $data['header'] = '';
                    $data['signature'] = '';
                    $data['seal'] = '';
                    $data['id'] = '';
                }

                return view("manage_signature", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }
    private function resizeImage($filePath, $maxWidth, $maxHeight, $quality)
    {
        list($width, $height, $type) = getimagesize($filePath);
        $src = null;
    
        switch ($type) {
            case IMAGETYPE_JPEG:
                $src = @imagecreatefromjpeg($filePath);
                break;
            case IMAGETYPE_PNG:
                $src = @imagecreatefrompng($filePath);
                break;
            case IMAGETYPE_GIF:
                $src = @imagecreatefromgif($filePath);
                break;
            default:
                // Unsupported image type
                return;
        }
    
        if ($src === null) {
            return;
        }
    
        $ratio = $width / $height;
        if ($width > $maxWidth) {
            $width = $maxWidth;
            $height = $width / $ratio;
        }
        if ($height > $maxHeight) {
            $height = $maxHeight;
            $width = $height * $ratio;
        }
    
        $dst = imagecreatetruecolor($width, $height);
    
        // Maintain transparency for PNG and GIF images
        if ($type === IMAGETYPE_PNG) {
            imagealphablending($dst, false);
            imagesavealpha($dst, true);
            $transparent = imagecolorallocatealpha($dst, 0, 0, 0, 127);
            imagefill($dst, 0, 0, $transparent);
        } elseif ($type === IMAGETYPE_GIF) {
            $transparentIndex = imagecolortransparent($src);
            if ($transparentIndex >= 0) {
                $transparentColor = imagecolorsforindex($src, $transparentIndex);
                $transparentIndex = imagecolorallocate($dst, $transparentColor['red'], $transparentColor['green'], $transparentColor['blue']);
                imagefill($dst, 0, 0, $transparentIndex);
                imagecolortransparent($dst, $transparentIndex);
            }
        }
    
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $width, $height);
    
        switch ($type) {
            case IMAGETYPE_JPEG:
                imagejpeg($dst, $filePath, $quality);
                break;
            case IMAGETYPE_PNG:
                imagepng($dst, $filePath, 9); // Compression level 0 (no compression) to 9 (maximum compression)
                break;
            case IMAGETYPE_GIF:
                imagegif($dst, $filePath);
                break;
        }
    
        imagedestroy($src);
        imagedestroy($dst);
    }
    


    public function update_signature(Request $request)
{
    if (Auth::check()) {
        if (Auth::user()->role == 1) {
            $count = Signature::where('id', 1)->count();

            $data = [
                'name' => $request->get('name'),
                'contact_no' => $request->get('contact_no'),
            ];

            $sign = $request->file('signature');
            $seal = $request->file('seal');
            $header = $request->file('header');

            // Function to handle image upload, compression, and resizing
            $handleImageUpload = function ($file, $oldName) {
                if ($file) {
                    
                    $fileName = time() . '-' . $file->getClientOriginalName();
                    $filePath = public_path('uploads/credentials/' . $fileName);

                    // Move the file to the destination
                    $file->move(public_path('uploads/credentials'), $fileName);

                    // Resize and compress the image
                    $this->resizeImage($filePath, 800, 800, 75);

                    return $fileName;
                }

                return $oldName;
            };

            if ($count != 0) {
                $details = Signature::where('id', 1)->first();

                $data['signature'] = $handleImageUpload($sign, $details->signature);
                $data['seal'] = $handleImageUpload($seal, $details->seal);
                $data['header'] = $handleImageUpload($header, $details->header);

                Signature::where('id', 1)->update($data);
            } else {
                $data['signature'] = $handleImageUpload($sign, '');
                $data['seal'] = $handleImageUpload($seal, '');
                $data['header'] = $handleImageUpload($header, '');

                Signature::insert($data);
            }

            Session::flash('success', 'Successfully Saved');
            return redirect('/manage_settings');
        } else {
            return redirect('/acessdenied');
        }
    } else {
        return redirect('/');
    }
}


   
    
    


    public function manage_events()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $data['title'] = 'Manage Events';
                $data["eventlist"] = Event::orderby("id", "desc")->get();
                return view("manage_events", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function save_events(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $count = Event::where("event_name", ucfirst($request->get("event_name")))->count();
                if ($count == 0) {
                    $data["event_name"] = ucfirst($request->get("event_name"));
                    $data["start_date"] = date('Y-m-d', strtotime($request->get("start_date")));
                    if ($request->get("end_date") != '') {
                        $data["end_date"] = date('Y-m-d', strtotime($request->get("end_date")));
                    }

                    $data["status"] = 1;
                    Event::insert($data);
                    Session::flash("success", "Successfully Saved");
                } else {
                    Session::flash("errormessage", "Already Saved");
                }

                return redirect("/manage_events");
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function activate_events(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $id = $request->get("id");
                $data["status"] = 1;
                Event::where("id", $id)->update($data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }
    public function deactivate_events(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $id = $request->get("id");
                $data["status"] = 0;
                Event::where("id", $id)->update($data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }
    public function regular_students()
    {
        $data['pac_type'] = 1;
        $data['userlist'] = User::where('role', 5)
            ->where('student_type', 1)
            ->get();
        $data['title'] = 'Regular Students';
        return view("students", $data);
    }

    public function chrysalis_students()
    {
        $data['pac_type'] = 2;

        $data['userlist'] = User::where('role', 5)
            ->where('student_type', 2)
            ->get();
        $data['title'] = 'Chrysalis Students';
        return view("students", $data);
    }

    public function employee_sales_report($empid)
    {
        //Monthly Reports
        $name = User::find($empid)->name;
        $data['title'] = 'Reports of ' . ucfirst($name);

        $today = date("Y-m-d");
        $first_date = date("Y-m-01");

        $data['empid'] = $empid;

        $user_id=$empid;



            // Monthly chrysalis 
            $data["monthrevenue_chry_withgst"]      = round(Salespayment::where("sales_payment.sales_id",$user_id)->where("sales_payment.package_type",2)
                                                         ->whereBetween("sales_payment.date", [$first_date, $today])
                                                         ->sum("sales_payment.total"));


           $data["monthrevenue_chry_withoutgst"]  = round(Salespayment::where("sales_payment.sales_id",$user_id)->where("sales_payment.package_type",2)
                                                         ->whereBetween("sales_payment.date", [$first_date, $today])
                                                         ->sum("sales_payment.payment"));

           $data["monthrevenue_chry_tax"]  = round(Salespayment::where("sales_payment.sales_id",$user_id)->where("sales_payment.package_type",2)
                                                         ->whereBetween("sales_payment.date", [$first_date, $today])
                                                         ->sum("sales_payment.tax"));

        // Monthly Regular and old  

            
            $data["monthrevenue_regular_withgst"] = round( Salespayment::where("sales_payment.sales_id",$user_id)
                                                         ->where("sales_payment.package_type",1)
                                                        ->whereBetween("sales_payment.date", [$first_date, $today])
                                                        ->sum("sales_payment.total"));

            $data["monthrevenue_old_withgst"] = round(OldPayment::where("olddata_payment.sales_id",$user_id)
                                                    ->whereBetween("olddata_payment.date", [$first_date, $today])
                                                     ->sum("olddata_payment.paid_amount"));

            $data['monthrevenue_regold_withgst']         = $data["monthrevenue_regular_withgst"] +   $data["monthrevenue_old_withgst"];

            $data["monthrevenue_regular_tax"] = round( Salespayment::where("sales_payment.sales_id",$user_id)
                                                            ->where("sales_payment.package_type",1)
                                                        ->whereBetween("sales_payment.date", [$first_date, $today])
                                                        ->sum("sales_payment.tax"));

            $data["monthrevenue_old_tax"] = round(OldPayment::where("olddata_payment.sales_id",$user_id)
                                                    ->whereBetween("olddata_payment.date", [$first_date, $today])
                                                        ->sum("olddata_payment.tax"));

            $data['monthrevenue_regold_tax']         = $data["monthrevenue_regular_tax"] +   $data["monthrevenue_old_tax"];

           
            $data["monthrevenue_regular_withoutgst"] = round( Salespayment::where("sales_payment.sales_id",$user_id)->where("sales_payment.package_type",1)
                                                     ->whereBetween("sales_payment.date", [$first_date, $today])
                                                     ->sum("sales_payment.payment"));
            $data["monthrevenue_old_withoutgst"] = round(OldPayment::where("olddata_payment.sales_id",$user_id)
                                                    ->whereBetween("olddata_payment.date", [$first_date, $today])
                                                     ->sum("olddata_payment.fees_income"));
            $data['monthrevenue_regold_withoutgst']         = $data["monthrevenue_regular_withoutgst"] +   $data["monthrevenue_old_withoutgst"];

             // Monthly total
             $data['monthlytotal_withgst']      = $data["monthrevenue_chry_withgst"] +  $data['monthrevenue_regold_withgst'] ;
             $data['monthlytotal_withoutgst']   = $data["monthrevenue_chry_withoutgst"] +  $data['monthrevenue_regold_withoutgst'] ;
       
              $data['total_tax']  = $data["monthrevenue_chry_tax"] +$data["monthrevenue_regular_tax"]+$data["monthrevenue_old_tax"];
       
             //MONTHLY REVENUE END

        //MONTHLY ADMISSION  START

            // Monthly chrysalis 
            $data["monthadmi_chry_withgst"]         =round(Salespackage::where("sales_package.sales_id",$user_id)
                                                        ->where("sales_package.package_type",2)
                                                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                                                        ->sum("sales_package.package_fullamount"));

            $data["monthadmi_chry_withoutgst"]      =round(Salespackage::where("sales_package.sales_id",$user_id)
                                                        ->where("sales_package.package_type",2)
                                                        ->whereBetween("sales_package.joining_date",[$first_date, $today])
                                                        ->sum("sales_package.package_amount")); 
            $data["monthadmi_chry_tax"]             =round(Salespackage::where("sales_package.sales_id",$user_id)
                                                        ->where("sales_package.package_type",2)
                                                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                                                        ->sum("sales_package.package_tax")); 
            //Monthly Admissions

            $data["monthadmi_regular_withgst"]      = round(Salespackage::where("sales_package.sales_id",$user_id)
                                                            ->where("sales_package.package_type",1)
                                                             ->whereBetween("sales_package.joining_date", [$first_date, $today])
                                                            ->sum("sales_package.package_fullamount"));

            $data['reg_old_withgst']                 = round(Oldstudent::where("ev_oldstudents.sales_id",$user_id)
                                                            ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                                                            ->sum("ev_oldstudents.totalfees"));
            

            $data['monthly_tot_admissionwithgst']   =  $data["monthadmi_regular_withgst"] +  $data['reg_old_withgst'];






            $data["monthadmi_regular_withoutgst"]      = round(Salespackage::where("sales_package.sales_id",$user_id)
                                                        ->where("sales_package.package_type",1)
                                                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                                                        ->sum("sales_package.package_amount"));

            $data['reg_old_withoutgst']                 = round(Oldstudent::where("ev_oldstudents.sales_id",$user_id)
                                                            ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                                                            ->sum("ev_oldstudents.fees"));
            $data['monthly_tot_admissionwithoutgst']    =  $data["monthadmi_regular_withoutgst"] +  $data['reg_old_withoutgst'];




            $data["monthadmi_regular_tax"]      = round(Salespackage::where("sales_package.sales_id",$user_id)
                                                        ->where("sales_package.package_type",1)
                                                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                                                        ->sum("sales_package.package_tax"));

            $data['reg_old_tax']                 = round(Oldstudent::where("ev_oldstudents.sales_id",$user_id)
                                                            ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                                                            ->sum("ev_oldstudents.tax"));
            $data['monthly_tot_admissiontax']    =  $data["monthadmi_regular_tax"] +  $data['reg_old_tax'];


            //Total(Admission_Old_Chrysalis)


            $data['total_reg_chry_old_withgst']  =  $data['monthly_tot_admissionwithgst'] + $data["monthadmi_chry_withgst"];

            $data['total_reg_chry_old_withoutgst']  =  $data['monthly_tot_admissionwithoutgst'] + $data["monthadmi_chry_withoutgst"] ;

            $data['total_reg_chry_old_tax']  =  $data['monthadmi_chry_tax'] + $data["monthly_tot_admissiontax"] ;



       
        return view("sales_report", $data);




    }


    public function edit_employee($id)
    {
       
        if (Auth::check()) {
          
            $allowed_roles = [1, 2, 3, 4, 6, 7, 8, 9,10,12];
    
           
            if (in_array(Auth::user()->role, $allowed_roles)) {
                // Retrieve employee details for editing
                $data["saleuser"] = User::join('employee_info', 'employee_info.user_id', '=', 'users.id')
                    ->leftjoin('employee_privillege', 'employee_privillege.empid', '=', 'users.id')
                    ->where("users.id", $id)
                    ->first([
                        'users.*',
                        'employee_info.empid',
                        'employee_info.office_email',
                        'employee_info.poc_code',
                        'employee_privillege.add_employee as addemployee_privilage',
                        'employee_privillege.designation as desig',
                        'employee_privillege.technology as tech',
                        'employee_privillege.source',
                        'employee_privillege.qualification',
                        'employee_privillege.specialization',
                        'employee_privillege.college',
                        'employee_privillege.interview',
                        'employee_privillege.add_employee',
                        'employee_privillege.add_package',
                        'employee_privillege.add_event',

                        'employee_info.experience',
                        'employee_info.designation',
                        'employee_info.technology'
                    ]);
    
                // Set page title
                $data['title'] = 'Edit Employee';

                $data['tech'] = explode(',', $data['saleuser']->technology);
    
                // Map roles to respective types and details

                //1-admin,2-sales,3-trainers,4-process,5-student,6-management,7-finance,8-subadmin,9-placement


                switch ($data["saleuser"]->role) {
                    case 1:
                        $data["type"] = "Admin";
                        break;
                    case 2:
                        $data["type"] = "Marketing";
                        $data['subtitle'] = "Manage Marketing";
                        $data['url'] = 'manage_sales';
                        break;
                    case 3:
                        $data["type"] = "Trainers";
                        $data['subtitle'] = "Manage Trainers";
                        $data['url'] = 'manage_trainers';
                        break;
                    case 4:
                        $data["type"] = "Process";
                        $data['subtitle'] = "Manage Process Associates";
                        $data['url'] = 'manage_process';
                        break;
                    case 6:
                        $data["type"] = "Management";
                        $data['subtitle'] = "Manage Management";
                        $data['url'] = 'manage_management';
                        break;
                    case 7:
                        $data["type"] = "Finance";
                        $data['subtitle'] = "Manage Finance";
                        $data['url'] = 'manage_finance';
                        break;
                    case 8:
                        $data["type"] = "Sub Admin";
                        $data['subtitle'] = "Manage Sub admin";
                        $data['url'] = 'manage_subadmin';
                        break;
                    case 9:
                        $data["type"] = "Placement";
                        $data['subtitle'] = "Manage Placement";
                        $data['url'] = 'manage_placement';
                        break;
                    case 10:
                            $data["type"] = "Recruitment";
                            $data['subtitle'] = "Manage Recruitment";
                            $data['url'] = 'manage_recruitment';
                            break;
                    case 12:
                            $data["type"] = "Master";
                            $data['subtitle'] = "Manage Master";
                            $data['url'] = 'manage_master';
                            break;
                    default:
                        $data["type"] = "Unknown";
                        break;
                }
    
                // Fetch usernames for select list (excluding current user)
                $data['username'] = User::where('role', $data["saleuser"]->role)
                                        ->where('status', 1)
                                        ->where('id', '!=', $id)
                                        ->get(['id', 'name']);
    
                // Fetch reporting names associated with the employee
                $data["names"] = Reporting::where('employee_id', $data["saleuser"]->id)
                    ->pluck('reported_to')
                    ->toArray();
    
                // Ensure names is an empty array if no reports found
                if (empty($data["names"])) {
                    $data["names"] = [];
                }
    
                // Fetch designation and technology lists
                $data["designationlist"] = Designation::where("status", 1)
                    ->orderBy("id", "desc")
                    ->get();
                $data["technologylist"] = Technology::where("status", 1)
                    ->orderBy("id", "desc")
                    ->get();
    
                // Load the view with data
                return view("edit_employee", $data);
            } else {
                // Redirect to access denied if user's role doesn't match allowed_roles
                return redirect("/acessdenied");
            }
        } else {
            // Redirect to login if user is not authenticated
            return redirect("/");
        }
    }

private function isPhoneUnique($phone, $userId)
{
    // Query to check if the phone number exists for other users
    return User::where('phone', $phone)
               ->where('id', '!=', $userId)
               ->doesntExist();
}



    public function edit_save(Request $request)
{
    // Check if user is authenticated
    if (Auth::check()) {
       
        $allowed_roles = [1, 2, 3, 4, 6, 7, 8, 9,10,12];

        if (in_array(Auth::user()->role, $allowed_roles)) {

            if ($this->isPhoneUnique($request->input("phone"), $request->input("userid"))) {

            $datauser = [
                "name" => ucfirst($request->input("empname")),
                "phone" => $request->input("phone"),
            ];

            $datainfo = [
                "office_email" => $request->input("office_email"),
                "designation" => $request->input("designation"),
               "technology" => $request->filled('technology') ?implode(',',$request->input('technology')): 0, // Default to 0 if empty
                "experience" => $request->input("experience"),
            ];

           

            $datapriv = [
                "designation" => $request->input("cdesignation",0),
                "technology" => $request->input("ctechnology",0),
                "source" => $request->input("csource",0),
                "college" => $request->input("ccollege",0),
                "qualification" => $request->input("cqualification",0),
                "specialization" => $request->input("cspecialization",0),
                "interview" => $request->input("cinterview",0),
                "add_employee" => $request->input("cadd_employee",0),
                "add_package"=> $request->input("cadd_package",0),
                "add_event"=> $request->input("cadd_event",0),
                ];

            // Update user, employee info, and employee privilege
            User::where("id", $request->input("userid"))->update($datauser);
            Employeeinfo::where("user_id", $request->input("userid"))->update($datainfo);

            Employeepriv::where("empid", $request->input("userid"))->update($datapriv);

            // Delete existing reporting records and insert new ones
            Reporting::where('employee_id', $request->input("userid"))->delete();
            $reportids = $request->input("reported_to");
            if (!empty($reportids)) {
                foreach ($reportids as $reportId) {
                    Reporting::insert([
                        'reported_to' => $reportId,
                        'employee_id' => $request->input("userid"),
                    ]);
                }
            }

            // Redirect based on employee role after update
            switch ($request->input("role")) {
                case "2":
                    Session::flash("success", "Successfully Updated");
                    return redirect("/manage_sales");
                case "3":
                    Session::flash("success", "Successfully Updated");
                    return redirect("/manage_trainers");
                case "4":
                    Session::flash("success", "Successfully Updated");
                    return redirect("/manage_process");
                case "6":
                    Session::flash("success", "Successfully Saved");
                    return redirect("/manage_management");
                case "7":
                    Session::flash("success", "Successfully Saved");
                    return redirect("/manage_finance");
                case "8":
                    Session::flash("success", "Successfully Saved");
                    return redirect("/manage_subadmin");
                case "9":
                    Session::flash("success", "Successfully Saved");
                    return redirect("/manage_placement");
                case "10":
                        Session::flash("success", "Successfully Saved");
                        return redirect("/manage_recruitment");
                case "12":
                            Session::flash("success", "Successfully Saved");
                            return redirect("/manage_master");  
                default:
                    return redirect("/"); // Redirect to home page for unknown role
            }


        }
        else{

            Session::flash("errormessage", "Phone number already exists for another user.");
            return redirect()->back()->withInput();

        }





        } else {
            // Redirect to access denied if user's role doesn't match allowed_roles
            return redirect("/accessdenied");
        }
    } else {
        // Redirect to login if user is not authenticated
        return redirect("/");
    }
}

    

    public function add_othereference(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                return view("add_othereference");
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }
    public function save_othereference(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $data["email"] = $request->get("email");

                $validator = Validator::make(
                    $request->all(),
                    [
                        "email" => "required|unique:ev_othereference|max:255",
                        "phone" => "required|unique:ev_othereference",
                    ],
                    [
                        "email" => [
                            "required" => "Reference Email  field is required",
                            "unique" => "Reference Email  has already been taken.",
                            "email" => "Reference enter a valid email address.",
                        ],
                        "phone" => [
                            "required" => "Reference Email  field is required",
                            "unique" => "Reference Email  has already been taken.",
                           
                        ],
                    ]
                );
                if ($validator->fails()) {
                    return redirect("/add_othereference")
                        ->withErrors($validator)
                        ->withInput();
                } else {
                    $data["name"] = ucfirst($request->get("refname"));
                    $data["relation"] = ucfirst($request->get("refrelation"));
                    $data["phone"] = $request->get("phone");

                    Othereference::insert($data);
                    Session::flash("success", "Successfully Saved");
                    return redirect("/manage_reference");
                }
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function manage_reference()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $data["emplist"] = Othereference::orderby("id", "desc")->get();
                return view("manage_reference", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function activate_employee(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $id = $request->get("id");
                $data["status"] = 1;
                Employee::where("id", $id)->update($data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }
    public function deactivate_employee(Request $request)
    {
        if (Auth::check()) {
          
               
               
                $id = $request->get("id");
                $data["status"] = 0;
                Employee::where("id", $id)->update($data);
           
        } else {
            return redirect("/");
        }
    }

    public function add_srishtians(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                return view("add_srishtians");
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function save_srishtians(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $data["empid"] = $request->get("empid");
                $data["email"] = $request->get("email");

                $validator = Validator::make(
                    $request->all(),
                    [
                        "email" => "required|email|unique:ev_employees|max:255",
                        "empid" => "required|unique:ev_employees|",
                        "phone" => "required|unique:ev_employees|",
                    ],
                    [
                        "email" => [
                            "required" => "Employee Email  field is required",
                            "unique" => "Employee Email  has already been taken.",
                            "email" => "Please enter a valid email address.",
                        ],
                        "empid" => [
                            "required" => "Employee Id field is required",
                            "unique" => "Employee Id  has already been taken.",
                        ],
                        "phone" => [
                            "required" => "Phone  field is required",
                            "unique" => "Phone  has already been taken.",
                           
                        ],
                    ]
                );
                if ($validator->fails()) {
                    return redirect("/add_srishtians")
                        ->withErrors($validator)
                        ->withInput();
                } else {
                    $data["emp_name"] = ucfirst($request->get("empname"));
                    $data["phone"] = $request->get("phone");
                    $data["status"] = $request->get("status");

                    Employee::insert($data);
                    Session::flash("success", "Successfully Saved");
                    return redirect("/manage_employee");
                }
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function manage_employee()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $data["emplist"] = Employee::orderby("id", "desc")->get();
                return view("manage_employee", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function add_package()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $data['title'] = 'Add Package';
                $data["technologylist"] = Technology::where("status", 1)
                    ->orderby("id", "desc")
                    ->get();
                $data["tax"] = Tax::find(1)->tax;

                return view("add_package", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }
    public function update_tax(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $id = $request->get("tax_id");
                $data["tax"] = $request->get("tax");

                Tax::where("id", $id)->update($data);
                Session::flash("success", "Successfully Updated");

                return redirect("/manage_tax");
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function manage_tax()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $data["tax"] = Tax::find(1);

                return view("manage_tax", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function deact_package(Request $request)
    {
        // echo $request->id;
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                Package::where("pac_id", "=", $request->get("id"))->update([
                    "status" => "2",
                ]);

                $ptype = Package::where("pac_id", "=", $request->get("id"))->first(['pac_type']);

                echo $ptype->pac_type;
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function act_package(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                Package::where("pac_id", "=", $request->get("id"))->update([
                    "status" => "1",
                ]);

                $ptype = Package::where("pac_id", "=", $request->get("id"))->first(['pac_type']);

                echo $ptype->pac_type;
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function employee_details(Request $request)
    {
        $id = $request->get("id");
        $data["employeedetails"] = User::join('employee_info', 'employee_info.user_id', '=', 'users.id')
        ->leftjoin('employee_privillege', 'employee_privillege.empid', '=', 'users.id')
        ->leftjoin("ev_designation", "ev_designation.id", "=", "employee_info.designation")
        ->leftjoin("ev_technology", "ev_technology.id", "=", "employee_info.technology")
        ->where("users.id", $id)
            ->first(["users.*",  "ev_designation.designation", "ev_technology.technology","employee_info.empid","employee_info.technology as tech",
            'employee_privillege.designation as desig',
            'employee_privillege.technology as techs',
            'employee_privillege.source',
            'employee_privillege.qualification',
            'employee_privillege.specialization',
            'employee_privillege.college',
            'employee_privillege.interview',
            'employee_privillege.add_employee as employee',
            'employee_privillege.add_package as package',
            'employee_privillege.add_event as event']);


if (!empty($data['employeedetails']->tech)) {
$techIds = explode(',', $data['employeedetails']->tech);
$technologies = Technology::whereIn('id', $techIds)->orderBy('id', 'desc')->pluck('technology')->toArray();
$data['techno'] = implode(',', $technologies);
}





        echo json_encode($data);
    }

    public function manage_specialization()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $data['title'] = 'Manage Specialization';
                $data["specialization"] = Specialization::orderby("id", "desc")->get();
                return view("manage_specialization", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }
    public function save_specialization(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $count = Specialization::where("specialization", strtoupper($request->get("specialization")))->count();
                if ($count == 0) {
                    $data["specialization"] = strtoupper($request->get("specialization"));
                    Specialization::insert($data);
                    Session::flash("success", "Successfully Saved");
                } else {
                    Session::flash("errormessage", "Already Saved");
                }

                return redirect("/manage_specialization");
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function edit_specialization(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                //   echo "hi";
                Specialization::where("id", "=", $request->editspecid)->update([
                    "specialization" => strtoupper($request->editspecname),
                ]);
                Session::flash("success", "Successfully Updated");

                return redirect("/manage_specialization");
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }
    public function dact_specialization(Request $request)
    {
        // echo $request->id;
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                Specialization::where("id", "=", $request->id)->update([
                    "status" => "2",
                ]);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function act_specialization(Request $request)
    {
        // echo $request->id;
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                Specialization::where("id", "=", $request->id)->update([
                    "status" => "1",
                ]);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function edit_qualification(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                //   echo "hi";
                Qualification::where("id", "=", $request->editqualiid)->update([
                    "qualification" => strtoupper($request->editqualiname),
                ]);
                Session::flash("success", "Successfully Updated");

                return redirect("/manage_qualification");
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function dact_qualification(Request $request)
    {
        // echo $request->id;
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                Qualification::where("id", "=", $request->id)->update([
                    "status" => "2",
                ]);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function act_qualification(Request $request)
    {
        // echo $request->id;
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                Qualification::where("id", "=", $request->id)->update([
                    "status" => "1",
                ]);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }


    public function edit_source(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                //   echo "hi";
                Source::where("id", "=", $request->editsourceid)->update([
                    "source" => strtoupper($request->editsourcename),
                ]);
                Session::flash("success", "Successfully Updated");

                return redirect("/manage_source");
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }
    public function dact_source(Request $request)
    {
        // echo $request->id;
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                Source::where("id", "=", $request->id)->update([
                    "status" => "2",
                ]);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }
    public function act_source(Request $request)
    {
        // echo $request->id;
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                Source::where("id", "=", $request->id)->update([
                    "status" => "1",
                ]);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function edit_technology(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                //   echo "hi";
                Technology::where("id", "=", $request->edittechid)->update([
                    "technology" => strtoupper($request->edittechname),
                ]);
                Session::flash("success", "Successfully Updated");

                return redirect("/manage_technology");
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }
    public function dact_technology(Request $request)
    {
        // echo $request->id;
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                Technology::where("id", "=", $request->id)->update([
                    "status" => "2",
                ]);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function act_technology(Request $request)
    {
        // echo $request->id;
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                Technology::where("id", "=", $request->id)->update([
                    "status" => "1",
                ]);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function edit_designation(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                Designation::where("id", "=", $request->editdesigid)->update([
                    "designation" => strtoupper($request->editdesigname),
                ]);
                Session::flash("success", "Successfully Updated");

                return redirect("/manage_designation");
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function dact_designation(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                Designation::where("id", "=", $request->id)->update([
                    "status" => "2",
                ]);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function act_designation(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                Designation::where("id", "=", $request->id)->update([
                    "status" => "1",
                ]);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function activate_user(Request $request)
{
    if (!Auth::check()) {
        return redirect("/");
    }

    $employeeId = Auth::user()->id;
    $userRole = Auth::user()->role;

    // Retrieve employee privileges
    $empprivillage = CustomHelper::employeeprivillages($employeeId);

    // Default canAddEmployee to false
    $canAddEmployee = false;

    // Ensure $empprivillage is an array and has the required structure
    if (is_array($empprivillage) && isset($empprivillage['employeeprivillege'])) {
        $canAddEmployee = isset($empprivillage['employeeprivillege']['add_employee']) 
            ? $empprivillage['employeeprivillege']['add_employee'] == 1 
            : false;
    }

    // Define roles that are allowed to activate users
    $allowedRoles = [1, 2, 3, 4, 6, 7, 8, 9];

    // Check if the user's role is allowed and whether they need the privilege check
    if (in_array($userRole, $allowedRoles) && ($userRole == 1 || $canAddEmployee)) {
        $id = $request->get("id");

        // Validate the ID to ensure it's numeric and exists
        if (is_numeric($id) && User::where("id", $id)->exists()) {
            User::where("id", $id)->update(['status' => 1]);
        } else {
            return redirect("/acessdenied");
        }
    } else {
        return redirect("/acessdenied");
    }
}


public function deactivate_user(Request $request)
{
    if (!Auth::check()) {
        return redirect("/");
    }

    $employeeId = Auth::user()->id;
    $userRole = Auth::user()->role;

    // Retrieve employee privileges
    $empprivillage = CustomHelper::employeeprivillages($employeeId);

    // Default canAddEmployee to false
    $canAddEmployee = false;

    // Ensure $empprivillage is an array and has the required structure
    if (is_array($empprivillage) && isset($empprivillage['employeeprivillege'])) {
        $canAddEmployee = isset($empprivillage['employeeprivillege']['add_employee']) 
            ? $empprivillage['employeeprivillege']['add_employee'] == 1 
            : false;
    }

    // Define roles that are allowed to activate users
    $allowedRoles = [1, 2, 3, 4, 6, 7, 8, 9];

    // Check if the user's role is allowed and whether they need the privilege check
    if (in_array($userRole, $allowedRoles) && ($userRole == 1 || $canAddEmployee)) {
        $id = $request->get("id");

        // Validate the ID to ensure it's numeric and exists
        if (is_numeric($id) && User::where("id", $id)->exists()) {
            User::where("id", $id)->update(['status' => 0]);
        } else {
            return redirect("/acessdenied");
        }
    } else {
        return redirect("/acessdenied");
    }
}










   
    public function manage_package()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1 || Auth::user()->role == 2) {
                $data["package"] = Package::leftJoin("ev_technology", "ev_technology.id", "=", "ev_package.tech_id")
                    ->where("ev_package.pac_type", 1)
                    ->where("ev_technology.status", 1)

                    ->where(function ($query) {
                        if (Auth::user()->role == 2) {
                            $query->where('ev_package.status', 1);
                        }
                    })

                    ->orderby("pac_id", "desc")
                    ->get(["ev_package.*", "ev_technology.technology"]);

                return view("manage_package", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function manage_package_chrysallis()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1 || Auth::user()->role == 2) {
                $data['title'] = 'Chrysalis Package';

                $data["package"] = Package::leftJoin("ev_technology", "ev_technology.id", "=", "ev_package.tech_id")
                    ->where("ev_technology.status", 1)
                    ->where("ev_package.pac_type", 2)

                    ->where(function ($query) {
                        if (Auth::user()->role == 2) {
                            $query->where('ev_package.status', 1);
                        }
                    })
                    ->orderby("pac_id", "desc")
                    ->get(["ev_package.*", "ev_technology.technology"]);

                // print_r($data)
                return view("manage_package_chrysallis", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }
    public function save_package(Request $request)
    {
        if (!Auth::check()) {
            return redirect("/");
        }
        if (Auth::user()->role != 1) {
            return redirect("/acessdenied");
        }
        $packageName = $request->get("packname");
        $count = Package::where('pac_name', $packageName)->count();
    
        if ($count > 0) {
            Session::flash("errormessage", "Package Name Already Exists");
            return $this->redirectToManagePackage($request->get("pac_type"));
        }
        $data = [
            'pac_name'   => $packageName,
            'duration'   => $request->get("duration"),
            'tech_id'    => $request->get("technology"),
            'fee'        => round($request->get("packfee")),
            'tax'        => $request->get("packtax"),
            'total'      => $request->get("packtot"),
            'course_id'  => $request->get("packcourseid"),
            'hours'      => $request->get("packhrs"),
            'project'    => $request->get("packpro"),
            'pac_type'   => $request->get("pac_type"),
        ];
        $insertId = Package::insertGetId($data);
        $prerequisiteData = [
            'pac_id'   => $insertId,
            'pre_req'  => $request->get("packpre"),
        ];
        Prerequisite::insert($prerequisiteData);
        Session::flash("success", "Successfully Saved");
        return $this->redirectToManagePackage($request->get("pac_type"));
    }

public function edit_package($id)
{
    if (!Auth::check()) {
        return redirect("/");
    }
    if (Auth::user()->role != 1) {
        return redirect("/acessdenied");
    }

    $data = [
        'title'            => 'Edit Package',
        'package'          => Package::where("pac_id", $id)->first(),
        'tax'              => Tax::find(1)->tax,
        'pre_req'          => Prerequisite::where('pac_id', $id)->get(),
        'technologylist'   => Technology::where("status", 1)
                                        ->orderBy("id", "desc")
                                        ->get(),
    ];

    return view("edit_package", $data);
}




















    
    private function redirectToManagePackage($packageType)
    {
        return $packageType == "1" 
            ? redirect("/manage_package") 
            : redirect("/manage_package_chrysallis");
    }
    
   

    






    

    public function edit_savepack(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $data["pac_name"] = $request->get("packname");
                $data["duration"] = $request->get("duration");
                $data["tech_id"] = $request->get("technology");
                $data["fee"] = round($request->get("packfee"));
                $data["tax"] = $request->get("packtax");
                $data["total"] = $request->get("packtot");
                $data["course_id"] = $request->get("packcourseid");
                $data["hours"] = $request->get("packhrs");
                $data["project"] = $request->get("packpro");
                //$data["pre_req"] = $request->get("packpre");
                Package::where("pac_id", "=", $request->packid)->update($data);

                Prerequisite::where('pac_id', $request->input("packid"))->delete();
               
                $preReqs = $request->input("pack_pre");
               
                if (!empty($preReqs)) {
                    $data = array_map(fn($preReq) => [
                        'pac_id' =>  $request->input('packid'),
                        'pre_req' => $preReq
                    ], $preReqs);
                
                    Prerequisite::insert($data);
                }

                Session::flash("success", "Successfully Updated");

                if ($request->get("pac_type") == 1) {
                    return redirect("/manage_package");
                } else {
                    return redirect("/manage_package_chrysallis");
                }
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function delete_designation(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $id = $request->get("id");
                $data["status"] = 0;
                Designation::where("id", $id)->update($data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }
    public function manage_source()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $data['title'] = 'Manage Source';
                $data["sourcelist"] = Source::orderby("id", "desc")->get();
                return view("manage_source", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function save_source(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $count = Source::where("source", strtoupper($request->get("source")))->count();
                if ($count == 0) {
                    $data["source"] = strtoupper($request->get("source"));
                    Source::insert($data);
                    Session::flash("success", "Successfully Saved");
                } else {
                    Session::flash("errormessage", "Already Saved");
                }

                return redirect("/manage_source");
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function save_qualification(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $count = Qualification::where("qualification", strtoupper($request->get("qualification")))->count();
                if ($count == 0) {
                    $data["qualification"] = strtoupper($request->get("qualification"));
                    Qualification::insert($data);
                    Session::flash("success", "Successfully Saved");
                } else {
                    Session::flash("errormessage", "Already Saved");
                }

                return redirect("/manage_qualification");
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }


    public function manage_qualification()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $data['title'] = 'Manage Qualification';
                $data["qualification"] = Qualification::orderby("id", "desc")->get();
                return view("manage_qualification", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }




    public function manage_designation()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $data['title'] = 'Manage Designation';
                $data["designationlist"] = Designation::orderby("id", "desc")->get();
                return view("manage_designation", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function save_designation(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $count = Designation::where("designation", strtoupper($request->get("designation")))->count();
                if ($count == 0) {
                    $data["designation"] = strtoupper($request->get("designation"));
                    Designation::insert($data);
                    Session::flash("success", "Successfully Saved");
                } else {
                    Session::flash("errormessage", "Already Saved");
                }

                return redirect("/manage_designation");
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function manage_technology()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $data['title'] = 'Manage Technology';
                $data["technologylist"] = Technology::orderby("id", "desc")->get();
                return view("manage_technology", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function save_technology(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->role == 1) {
                $count = Technology::where("technology", strtoupper($request->get("technology")))->count();
                if ($count == 0) {
                    $data["technology"] = strtoupper($request->get("technology"));
                    Technology::insert($data);
                    Session::flash("success", "Successfully Saved");
                } else {
                    Session::flash("errormessage", "Already Saved");
                }

                return redirect("/manage_technology");
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function manage_sales()
    {
        if (Auth::check()) {

            $employeeId = Auth::user()->id;
            $empprivillage = CustomHelper::employeeprivillages($employeeId);

            if (Auth::user()->role == 1 || (Auth::user()->role == 2 && $empprivillage['employeeprivillege']['add_employee'] == 1)) {
                
                $data['title'] = 'Manage Marketing';
                $data["emplist"] = User::where("role", 2)
                                    ->join('employee_info', 'employee_info.user_id', '=', 'users.id')
                                    ->leftjoin('employee_privillege', 'employee_privillege.empid', '=', 'users.id')
                    ->where(function ($query) {
                        if (Auth::user()->role == 2) {
                            $query->where('employee_info.created_userid', Auth::user()->id);
                        }
                    })
                    ->orderby("users.id", "desc")
                    ->get(['users.*','employee_info.empid','employee_info.office_email','employee_info.poc_code','employee_privillege.add_employee as addemployee_privilage']);

                return view("sales_user", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function manage_trainers()
    {
        if (Auth::check()) {

            $employeeId = Auth::user()->id;
            $empprivillage = CustomHelper::employeeprivillages($employeeId);

            if (Auth::user()->role == 1 || (Auth::user()->role == 3 && $empprivillage['employeeprivillege']['add_employee'] == 1)) {


                $data['title'] = 'Manage Trainers';
                $data["emplist"] = User::where("role", 3)
                ->join('employee_info', 'employee_info.user_id', '=', 'users.id')
                                    ->join('employee_privillege', 'employee_privillege.empid', '=', 'users.id')
                    ->where(function ($query) {
                        if (Auth::user()->role == 3) {
                            $query->where('employee_info.created_userid', Auth::user()->id);
                        }
                    })
                    ->orderby("users.id", "desc")
                    ->get(['users.*','employee_info.empid','employee_info.office_email','employee_info.poc_code','employee_privillege.add_employee as addemployee_privilage']);
                return view("trainer_user", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }

    public function manage_process()
    {
        if (Auth::check()) {
            $employeeId = Auth::user()->id;
 $empprivillage = CustomHelper::employeeprivillages($employeeId);

 if (Auth::user()->role == 1 || (Auth::user()->role == 4 && $empprivillage['employeeprivillege']['add_employee'] == 1)) {
                $data['title'] = 'Manage Process Associates';
                $data["emplist"] = User::where("role", 4)
                ->join('employee_info', 'employee_info.user_id', '=', 'users.id')
                ->join('employee_privillege', 'employee_privillege.empid', '=', 'users.id')
                    ->where(function ($query) {
                        if (Auth::user()->role == 4) {
                            $query->where('employee_info.created_userid', Auth::user()->id);
                        }
                    })
                    ->orderby("users.id", "desc")
                    ->get(['users.*','employee_info.empid','employee_info.office_email','employee_info.poc_code','employee_privillege.add_employee as addemployee_privilage']);
                return view("processor_user", $data);
            } else {
                return redirect("/acessdenied");
            }
        } else {
            return redirect("/");
        }
    }


    public function add_employee(Request $request)
    {
        // Check if user is authenticated
        if (Auth::check()) {
            $data = [];
    
            // Admin role (role == 1) has full access
            if (Auth::user()->role == 1) {
                $data['title'] = 'Add Employee';
                $data['designationlist'] = Designation::where("status", 1)
                    ->orderBy("id", "desc")
                    ->get();
                $data['technologylist'] = Technology::where("status", 1)
                    ->orderBy("id", "desc")
                    ->get();
            } else {
                // For other roles, check specific privilege using CustomHelper
                $employeeId = Auth::user()->id;
                $empprivillage = CustomHelper::employeeprivillages($employeeId);
    
                // Define roles that have access to add_employee privilege
                $allowed_roles = [2, 3, 4, 6, 7, 8, 9,10];
    
                // Check if user's role is in allowed_roles and has add_employee privilege
                if (in_array(Auth::user()->role, $allowed_roles) && $empprivillage['employeeprivillege']['add_employee'] == 1) {
                    $data['title'] = 'Add Employee';
                    $data['designationlist'] = Designation::where("status", 1)
                        ->orderBy("id", "desc")
                        ->get();
                    $data['technologylist'] = Technology::where("status", 1)
                        ->orderBy("id", "desc")
                        ->get();
    
                    // Fetch usernames for select list (excluding current user)
                    $data['username'] = User::where('role', Auth::user()->role)
                        
                        ->where('status', 1)
                        ->get(['id', 'name']);
                } else {
                    return redirect("/acessdenied"); // Redirect to access denied page if no access
                }
            }
    
            return view("add_employee", $data); // Load the view with the prepared data
        } else {
            return redirect("/"); // Redirect to homepage if user is not authenticated
        }
    }


public function save_employee(Request $request)
{
    // Check if user is authenticated
    if (Auth::check()) {
        // Validation rules
        $validator = Validator::make($request->all(), [
            "empid" => "required|unique:employee_info|max:255",
            "email" => "required|email|unique:users|max:255",
            "phone"=>"required|unique:users|max:255",
        ], [
            "empid.required" => "Employee Id field is required",
            "empid.unique" => "Employee Id has already been taken.",
            "email.required" => "Employee Email field is required",
            "email.unique" => "Employee Email has already been taken.",
            "email.email" => "Please enter a valid email address.",
            "phone.unique" => "Phone Number has already been taken.",
            "phone.required" => "Phone Number field is required.",
        ]);

        // If validation fails, redirect back with errors and input
        if ($validator->fails()) {
            return redirect("/add_employee")
                ->withErrors($validator)
                ->withInput();
        }

        // Prepare user data for insertion
        $userData = [
            "role" => $request->input("emptype"),
            "name" => ucfirst($request->input("empname")),
            "phone" => $request->input("phone"),
            "email" => $request->input("email"),
            "password" => Hash::make($request->input("password")),
            "password_text" => $request->input("password"),
        ];

        // Insert user and get the inserted ID
        $insertedUserId = User::insertGetId($userData);

        // Prepare employee information data
        $employeeInfo = [
            "empid" => $request->input("empid"),
            "office_email" => $request->input("office_email"),
            "designation" => $request->input("designation"),
            "user_id" => $insertedUserId,
            "poc_code" => substr("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", mt_rand(0, 50), 2) . substr(md5(time()), 1, 4),
            
            "technology" => $request->filled('technology') ?implode(',',$request->input('technology')): 0,
            "experience" => $request->input("experience"),
            "created_userid" => Auth::user()->id,
        ];

        // Insert employee information
        Employeeinfo::insert($employeeInfo);

        // Prepare employee privileges data
        

        $employeePrivileges = [
            "empid" => $insertedUserId,
            "designation" => $request->input("cdesignation",0),
            "technology" => $request->input("ctechnology",0),
            "source" => $request->input("csource",0),
            "college" => $request->input("ccollege",0),
            "qualification" => $request->input("cqualification",0),
            "specialization" => $request->input("cspecialization",0),
            "interview" => $request->input("cinterview",0),
            "add_employee" => $request->input("cadd_employee",0),
            "add_package"=> $request->input("cadd_package",0),
            "add_event"=> $request->input("cadd_event",0),
            
        ];

        // Insert employee privileges
        Employeepriv::insert($employeePrivileges);

        // Handle reporting data
        $reportedToIds = $request->input("reported_to", []);

        foreach ($reportedToIds as $reportedToId) {
            $reportingData = [
                'reported_to' => $reportedToId,
                'employee_id' => $insertedUserId,
            ];
            Reporting::insert($reportingData);
        }

        // Redirect based on employee type
        switch ($request->input("emptype")) {
            case 2:
                return redirect("/manage_sales")->with("success", "Successfully Saved");
            case 3:
                return redirect("/manage_trainers")->with("success", "Successfully Saved");
            case 4:
                return redirect("/manage_process")->with("success", "Successfully Saved");
            case 6:
                return redirect("/manage_management")->with("success", "Successfully Saved");
            case 7:
                return redirect("/manage_finance")->with("success", "Successfully Saved");
            case 8:
                return redirect("/manage_subadmin")->with("success", "Successfully Saved");
            case 9:
                return redirect("/manage_placement")->with("success", "Successfully Saved");
            case 10:
                    return redirect("/manage_recruitment")->with("success", "Successfully Saved");
            case 12:
                return redirect("/manage_master")->with("success", "Successfully Saved");

            default:
                return redirect("/add_employee")->with("error", "Invalid employee type");
        }
    } else {
        // Redirect to login if user is not authenticated
        return redirect("/");
    }
}

    
   

    public function acessdenied()
    {
        return view("acess_denied");
    }
}
