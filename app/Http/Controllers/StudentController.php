<?php
namespace App\Http\Middleware\CheckStatus;
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Technology;
use App\Models\Designation;
use App\Models\Qualification;
use App\Models\Source;
use App\Models\College;
use App\Models\User;
use App\Models\Contacts;
use App\Models\Specialization;
use App\Models\Studentpackage;
use App\Models\Studentfeessplit;
use App\Models\Event;
use App\Models\Employee;
use App\Models\Employeeinfo;
use App\Models\Employeepriv;
use App\Models\Studentinfo;

use App\Models\Payment;
use App\Models\Student;
use App\Models\Gatepass;
use App\Models\StudentEvent;


use Session;
use Auth;

class StudentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

  


     public function payment_details_student($pactype,$pac_id)
     {
         if (Auth::check()) {
             if (Auth::user()->role == 5) {

                 $studentid =Auth::user()->id;

                 $data['package_details'] = Studentpackage::join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
                     ->where('ev_student_package.package_type', $pactype)
                     ->where('ev_student_package.package_id', $pac_id)
                     ->where('ev_student_package.student_id', $studentid)
                    ->first(['ev_package.total', 'ev_student_package.*', 'ev_package.pac_name']);
 
                
                 $pac_id = $pac_id;
                 $pac_type = $pactype;

                 $data['pac_type'] = $pac_type;
 
                 $data['name'] = User::find($studentid)->name;
                 $data['title'] = 'Payment History of ' .  $data['package_details']->pac_name;
                 $data['pac_id'] = $pac_id;
                 $data['subtitle'] = '';
 
                 $data['payment_history'] = Payment::orderBy('id', 'desc')
                     ->where('student_id', $studentid)
                     ->where('package_type', $pac_type)
                     ->where('package_id', $pac_id)
                     ->get();
 
                 $data['paidfees'] = Payment::where('student_id', $studentid)
                     ->where('package_type', $pac_type)
                     ->where('package_id', $pac_id)
                     ->sum('paid_amount');
 
                 $data['student_id'] = $studentid;
                 $data['studentpackage_id'] ='';
                 return view('payment_history_package', $data);
             } else {
                 return redirect('/acessdenied');
             }
         } else {
             return redirect('/');
         }
     }

     public function paymentrequest(Request $request)
     {
         $data['payment_method'] = $request->get("payment_method");
         $data['student_id'] = $request->get("student_id");
         $data['date'] = date('Y-m-d');
         $data['amount'] = $request->get("amount");
         $data['sales_id'] = $request->get("sales_id");
         $data['studentpackage_id'] = $request->get("studentpackage_id");
 
         //ANy pending
 
         $payreqcount = Paymentrequest::where('student_id', $data['student_id'])
             ->where('studentpackage_id', $data['studentpackage_id'])
             ->where('status', 0)
             ->count();
 
         if ($payreqcount == 0) {
             if ($data['payment_method'] == 'bank') {
                 $data['transaction_id'] = $request->get("transaction_id");
                 $count = Payment::where('transaction_id', $data['transaction_id'])->count();
                 $ocount = OldPayment::where('transaction_id', $data['transaction_id'])->count();
                 $payreq = Paymentrequest::where('transaction_id', $data['transaction_id'])
                     ->where('status', 0)
                     ->count();
 
                 if ($count == 0 && $ocount == 0 && $payreq == 0) {
                     if ($request->file("payment_screenshort") != '') {
                         $extension = $request->file("payment_screenshort")->getClientOriginalExtension();
                         $destinationPath = "public/uploads/screenshot"; // upload path
                         $fileName = rand(11111, 99999) . "." . $extension;
                         $request->file("payment_screenshort")->move($destinationPath, $fileName);
                         $data['payment_screenshort'] = $fileName;
                     }
                     $insert = Paymentrequest::insert($data);
                     $datastatus['status'] = true;
                     $datastatus['msg'] = "Sucessfully Send";
                 } else {
                     $datastatus['status'] = false;
                     $datastatus['msg'] = "Transaction id must be unique";
                 }
             } else {
                 $insert = Paymentrequest::insert($data);
                 $datastatus['status'] = true;
                 $datastatus['msg'] = "Sucessfully Send";
             }
         } else {
             $datastatus['status'] = false;
             $datastatus['msg'] = "Your Payment Request is Pending";
         }
         return json_encode($datastatus);
     }

    public function  activated_packages(){


        $student_id =Auth::user()->id;

        $data['activated_packages'] = Studentpackage::join('ev_package','ev_package.pac_id','=','ev_student_package.package_id')
                                                    ->where('ev_student_package.student_id',$student_id)
                                                    ->orderBy('ev_student_package.id','desc')
                                                    ->distinct('ev_student_package.package_id')
                                                    ->get(['ev_student_package.package_id','ev_student_package.package_type','ev_package.pac_name','ev_student_package.id as studentpackageid','ev_student_package.sales_id','ev_student_package.joining_date','ev_package.total','ev_student_package.reduction_check','ev_student_package.totalfees_afterreduction','ev_student_package.student_id']);

       return view('activated_package',$data);                                          
    }

    public function package_details(Request $request)
        {

            if(Auth::check()) {

                if(Auth::user()->role==5){

                $type              =$request->get('type');
                if($type !=3){
                    $data['gatepass']   =   Studentpackage::join('ev_package','ev_package.pac_id','=','ev_student_package.package_id')
                                                           ->join("ev_technology","ev_technology.id","=","ev_package.tech_id")
                                                            ->where('ev_student_package.student_id',Auth::user()->id)
                                                            ->where('ev_student_package.package_type',$type)
                                                            ->where("ev_package.status",1)
                                                            ->where("ev_technology.status",1)
                                                            ->get(['pac_id as id','pac_name as name']);
                }
                else{

                    $data['gatepass']   =   StudentEvent::join("ev_event","ev_event.id","=","ev_student_events.event_id")->where("ev_event.status",1)->where("ev_student_events.student_id",Auth::user()->id)->get(['ev_event.id','ev_event.event_name as name']);
                }
                echo json_encode($data);  
            }               
           
            
            }
            else{
                return redirect('/');
            }

      }
     public function add_gatepass()
        {
            if(Auth::check()) {
                if((Auth::user()->role==5)){

                $user_id                       = Auth::user()->id;
                $data1['gatepassrequestfirst'] = Gatepass::where('user_id', $user_id)
                                                        ->orderBy('id', 'desc')
                                                        ->first();
                //0-notapply,1-apply
                if (!empty($data1['gatepassrequestfirst'])) {

                    if ($data1['gatepassrequestfirst']['status'] == 3 && $data1['gatepassrequestfirst']['files'] != '') {
                        $data['aplied_status'] = 1;
                    } elseif ($data1['gatepassrequestfirst']['status'] == 2) {
                        $data['aplied_status'] = 1;
                    } else {
                        $data['aplied_status'] = 0;
                    }
                } else {
                    $data['aplied_status'] = 1;
                }
                
                    $data['title']              ='Request Gate Pass';
                    $data['userdetails']        = User::join('ev_student_info', 'ev_student_info.student_id', '=', 'users.id')
                                                        ->where('users.id',Auth::user()->id)->first(['users.id','users.name','users.photo','ev_student_info.sales_id']);

                    $data['enddate']            = Gatepass::where('user_id',Auth::user()->id)->where('status',1)->max('end_date');
                    $data['requested_date']     =date("Y-m-d");
                    return view('add_gatepass',$data);
                                     
            }
            else{
                return redirect('/acessdenied');
            }
        }
        else{
            return redirect('/');
        }

    }

    public function save_gatepass(Request $request)
{
    if (Auth::check()) {
        if (Auth::user()->role == 5) {

            $data['user_id'] = $request->get("userid");
            $data['start_date'] = date("Y-m-d", strtotime($request->get("start_date1")));
            $data['end_date'] = date("Y-m-d", strtotime($request->get("end_date1")));
             $data['type'] = $request->get("package_type");
            $data['type_id'] = $request->get("package_name");
            $data['sales_id'] = $request->get("salesid");
            $data['status'] = 0;
            $data['requested_date'] = date("Y-m-d");

        
            if ($request->hasFile('photo')) {
                $file = $request->file('photo');
                $extension = $file->getClientOriginalExtension();
                $destinationPath = public_path('uploads/photo'); // upload path
                $fileName = rand(11111, 99999) . "." . $extension;

                // Create an image resource from the uploaded file
                $imagePath = $file->getRealPath();
                $image = imagecreatefromstring(file_get_contents($imagePath));

                if ($image !== false) {
                    // Get original dimensions
                    $width = imagesx($image);
                    $height = imagesy($image);

                    // Define the new dimensions
                    $newWidth = 800; // Maximum width
                    $newHeight = ($height / $width) * $newWidth; // Maintain aspect ratio

                    // Create a new true color image with the new dimensions
                    $newImage = imagecreatetruecolor($newWidth, $newHeight);

                    // Copy and resize the original image into the new image
                    imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

                    // Save the new image
                    $newImagePath = $destinationPath . '/' . $fileName;
                    imagejpeg($newImage, $newImagePath, 75); // 75 is the quality

                    // Free up memory
                    imagedestroy($image);
                    imagedestroy($newImage);

                    // Update the user photo field in the database
                    User::where('id', $data['user_id'])->update(['photo' => $fileName]);

                    $datastatus['filename'] = $fileName;
                    $data['photo'] = $fileName;
                } else {
                    // Handle error: image creation failed
                    return redirect('/student_gatepass')->with('error', 'Image processing failed.');
                }
            } else {
                $datastatus['filename'] = User::find($data['user_id'])->photo;
                $data['photo'] = $datastatus['filename'];
            }
            // Insert data into Gatepass
            Gatepass::insert($data);
            Session::flash('success', 'Successfully Send Request');
            return redirect('/student_gatepass');
        } else {
            return redirect('/acessdenied');
        }
    } else {
        return redirect('/');
    }
}



    
    public function student_gatepass(){

        if(Auth::check()) {
        if((Auth::user()->role==5)){

        $data['title']='Applied Gate Pass';


           
            $data1['gatelist'] = Gatepass::join("users", "users.id", "=", "ev_gatepassrequest.user_id")
            ->where('ev_gatepassrequest.user_id', Auth::user()->id)
            ->orderBy("ev_gatepassrequest.id", "desc")
            ->get(['ev_gatepassrequest.*']);

           
            if (count($data1['gatelist']) > 1 && !empty($data1['gatelist'])) {

                $x=count($data1['gatelist'])-1;

           
            $activeGatepass = Gatepass::join("users", "users.id", "=", "ev_gatepassrequest.user_id")
                ->where('ev_gatepassrequest.user_id', Auth::user()->id)
                ->whereNotNull('ev_gatepassrequest.return_by') 
                ->limit($x)
                ->get(['ev_gatepassrequest.*']);


            if(count($activeGatepass)==$x){

                $data['gatelist'] = $data1['gatelist'];

            }
            else{
                $data['gatelist'] = Gatepass::join("users", "users.id", "=", "ev_gatepassrequest.user_id")
                ->where('ev_gatepassrequest.user_id', Auth::user()->id)
                ->limit($x) 
                ->orderBy("ev_gatepassrequest.id", "desc")
                ->get(['ev_gatepassrequest.*']);
            }

            
            } else {
           
            $data['gatelist'] = $data1['gatelist'];
            }




        return view('student_gatepass',$data);

            }
            else{
                return redirect('/acessdenied');
        }

        }
        else{
            return redirect('/');
        }

     }
    public function discountdetails(Request $request){

        $code=$request->get('code');
        $student_id=Auth::user()->id;
        $codecount=Studentpackage::where('student_id',$student_id)->where('referalcode',$code)->count();
        $data['codecount']=$codecount;

        if($codecount==1){

            $data['packagedetails']=Studentpackage::join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
            ->where('ev_student_package.student_id',$student_id)->first(['ev_student_package.*','ev_package.pac_name','ev_package.duration','ev_package.fee','ev_package.tax as maintax','ev_package.total']);

        }
        else{
            $data['packagedetails']='';
        }

        echo json_encode($data);

    }

    public function student_admission_details()
    {
        if(Auth::check()) {

            if(Auth::user()->role==5){

               
                if(Auth::user()->admission_completed==1){

                $data['packagedetails']=Studentpackage::join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
                                                        ->where('ev_student_package.student_id',Auth::user()->id)->first(['ev_student_package.*','ev_package.pac_name','ev_package.duration','ev_package.fee','ev_package.tax as maintax','ev_package.total']);
                
                
                
                    $data['feessplitup']=Studentfeessplit::where('student_id',Auth::user()->id)->get();
               
                $data['feessplitup']=Studentfeessplit::where('student_id',Auth::user()->id)->get();
                $data['studentdetails']=Student::join('users', 'users.id', '=', 'ev_stud_admission.stud_id')->where('ev_stud_admission.stud_id',Auth::user()->id)->first(['ev_stud_admission.*','users.name','users.phone']);


                $data['gatepass']=Gatepass::where('user_id',Auth::user()->id)->get(['ev_gatepassrequest.*']);
                

              


                return view('student_admission_setails',$data);
                }
                else{
                    return redirect('/acessdenied');
                }
            }
            else{
                return redirect('/acessdenied');
            }
        }
        else{
            return redirect('/');
        }            
    }
     public function student_admission()
    {
        if(Auth::check()) {

        if(Auth::user()->role==5){

          
        $regularcount = Studentpackage::where('ev_student_package.student_id',Auth::user()->id)
                                                    ->where('ev_student_package.package_type',1)
                                                    ->count();
              
          

                if((Auth::user()->admission_completed==0)&& ($regularcount !=0)){

                   

                $data['qualification']=Qualification::where('status',1)->get();
                $data['specialization']=Specialization::where('status',1)->get();
                $data['technology']=Technology::where('status',1)->get();


                $data['name']       =Auth::user()->name;
                $data['phone']      =Auth::user()->phone;
                $data['phone1']     =Auth::user()->phone1;
                $data['contact_id'] =Auth::user()->contact_id;
                $data['email']      =Auth::user()->email;

                
                if(Auth::user()->created_at !=''){
                    $data['joining_date']=date("Y-m-d", strtotime(Auth::user()->created_at));
                }
                else{
                    $data['joining_date']=date("d-m-Y");
                }


                $studentin =Studentinfo::where('student_id',Auth::user()->id)->first(['sales_id']);

                $sales_id            =$studentin['sales_id'];
                

                $code=Employeeinfo::where('user_id',$sales_id)->first(['poc_code']);
                $data['poc_code']   =$code->poc_code;

                $rcode =Studentpackage::where('student_id',Auth::user()->id)->where('package_type',1)->first('referalcode');
                $data['feessplitup']=Studentfeessplit::where('student_id',Auth::user()->id)->get();
                $data['referal_code']=$rcode->referalcode;

               
                return view('student_admission',$data);

                }
                else{
                    return redirect('/activated_packages');
                    
                }
            }
            else{
                return redirect('/acessdenied');
            }
        }
        else{
            return redirect('/');
        }            
    }

    public function save_student(Request $request){

            if(Auth::check()) {
                if(Auth::user()->role==5){

                    $count= Student::where('stud_id',Auth::user()->id)->count();

                if($count==0){

                    $data['stud_id']=Auth::user()->id;
                    $data['stud_name']=$request->get("stud_name");
                    $data['stud_phone']=$request->get("stud_phone");
                    $data['stud_sec_phone']=$request->get("stud_sec_phone");

                    $data['stud_whatsapp_no']=$request->get("stud_whatsapp_no");
                    $data['stud_email']=$request->get("stud_email");
                    $data['stud_sec_email']=$request->get("stud_sec_email");
                    $data['stud_dob']=$request->get("stud_dob");
                    $data['stud_blood']=$request->get("stud_blood");
                    $data['stud_father_name']=$request->get("stud_father_name");
                    $data['stud_father_occu']=$request->get("stud_father_occu");
                    $data['stud_father_no']=$request->get("stud_father_no");
                    $data['stud_father_email']=$request->get("stud_father_email");
                    $data['stud_mother_name']=$request->get("stud_mother_name");
                    $data['stud_mother_occu']=$request->get("stud_mother_occu");
                    $data['stud_mother_no']=$request->get("stud_mother_no");
                    $data['stud_mother_email']=$request->get("stud_mother_email");
                    $data['stud_per_add']=$request->get("stud_per_add");
                    $data['stud_temp_add']=$request->get("stud_temp_add");
                    $data['stud_city']=$request->get("stud_city");
                    $data['stud_town']=$request->get("stud_town");
                    $image = $request->file('stud_photo');
                    if($request->hasFile('stud_photo')){
                        $imageName =$image->getClientOriginalName();  
                        $image->move(public_path('uploads/pic'), $imageName);
                       }
                     else{
                        $imageName=''; 
                     } 
                    $data['stud_photo']=$imageName;
                   
                    $aahr_image = $request->file('stud_aadh_photo');
                    if($request->hasFile('stud_aadh_photo')){
                        $aahrName =$aahr_image->getClientOriginalName();  
                        $aahr_image ->move(public_path('uploads/aadhar'), $aahrName);
                       }
                     else{
                        $aahrName=''; 
                     } 
                   
                    $data['stud_aadh_photo']=$aahrName;
                    $data['stud_qualification']=$request->get("stud_qualification");
                    $data['stud_institute']=$request->get("stud_institute");
                    $data['stud_year_of_pass']=$request->get("stud_year_of_pass");
                    $data['stud_arrears']=$request->get("stud_arrears");

                    if( $data['stud_arrears']=='No'){

                        $data['stud_arrears_no']=0;

                    }
                    if( $data['stud_arrears']=='Yes'){

                        $data['stud_arrears_no']=$request->get("stud_arrears_no");

                    }
                    if( $data['stud_arrears']=='Awaiting Result'){

                        $data['stud_arrears_no']=0;

                    }

                    $data['stud_specialization']=$request->get("stud_specialization");
                    $data['stud_percentage']=$request->get("stud_percentage");
                    $data['stud_hsst_name']=$request->get("stud_hsst_name");
                    $data['stud_hsst_per']=$request->get("stud_hsst_per");
                    $data['stud_hsst_year']=$request->get("stud_hsst_year");
                    $data['stud_ss_name']=$request->get("stud_ss_name");
                    $data['stud_ss_per']=$request->get("stud_ss_per");
                    $data['stud_ss_year']=$request->get("stud_ss_year");
                    $data['stud_doj']=$request->get("stud_doj");
                    $data['stud_pref_doj']=$request->get("stud_pref_doj");
                   
                   
                    $data['stud_ad_on']=$request->get("stud_ad_on");
                    
                    $data['stud_place_training']=$request->get("stud_place_training");
    
                    $resume = $request->file('stud_resume');
                    if($request->hasFile('stud_resume')){
                        $resumeName =$resume->getClientOriginalName();  
                        $resume->move(public_path('uploads/receipt'), $resumeName);
                       }
                     else{
                        $resumeName=''; 
                     }
                    $data['stud_receipt']=$resumeName;
    
                    Student::insert($data);

                    $datauser['name']=$request->get("stud_name");
                    $datauser['phone']=$request->get("stud_phone");
                    $datauser['phone2']=$request->get("stud_sec_phone");
                    $datauser['admission_completed']=1;

                    User::where('id',Auth::user()->id)->update($datauser);

                    Session::flash('success', 'Successfully Saved');
                    }
                   
                    return redirect('/home');
                }
                else{
                    return redirect('/acessdenied');
                }
            }
            else{
                return redirect('/');
            }
    
       
        }

}
