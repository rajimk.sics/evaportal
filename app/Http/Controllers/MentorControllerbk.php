<?php

namespace App\Http\Controllers;
use Auth;
use Session;
use App\Models\User;
use App\Models\Topic;
use League\Csv\Reader;
use App\Models\Package;
use App\Models\Subtopic;
use App\Models\Questions;
use App\Models\Employeeinfo;
use App\Models\Prerequisite;
use App\Models\Technology;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ExcelcommonController;


class MentorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $ExcelcommonController;

    public function __construct()
    {
        $this->middleware("auth");
        $this->ExcelcommonController = new ExcelcommonController();
    }

//TOPIC AND SUBTOPIC

private function handleRedirect($responseData)
{
    if (is_array($responseData) && isset($responseData['duplicates']) && $responseData['duplicates']) {
        Session::flash('duplicates', $responseData['duplicates']);
        return redirect()->back();
    }
    if (isset($responseData['error']) && !empty($responseData['error'])) {
        return redirect()->back()->withErrors(['error' => $responseData['error']]);
            }
    else {
        Session::flash('success', 'Successfully Updated');
        return redirect('/topicListingByTechnology');
    }
}



private function redirectWithSubtopics($responseData)
{
    if (isset($responseData['duplicates']) && !empty($responseData['duplicates'])) {
        Session::flash('duplicates', $responseData['duplicates']);
        return redirect()->back();
    } if (isset($responseData['error']) && !empty($responseData['error'])) {
        return redirect()->back()->withErrors(['error' => $responseData['error']]);
            }
    else {
        Session::flash('success', 'Successfully Updated');
        return redirect('/topicListingByTechnology');
    }
}








private function getTechnologyOfTrainer($id)
{
    return  Employeeinfo::join('ev_technology', function ($join) {
        $join->on(DB::raw("FIND_IN_SET(ev_technology.id, employee_info.technology)"), '>', DB::raw('0'));
    })
        ->where('employee_info.user_id', $id)
        ->where("ev_technology.status", 1)
        ->get();
}

public function uploadSylabus()
    {

        if (!Auth::check()) {
            return redirect('/');
        }

        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }

        $data = [
            'title' => 'Syllabus Upload',
            'technologies' =>  Technology::where('status', '1')->get()
        ];
        return view('syllabusupload', $data);
    }
    public function UploadSyllabusByTechnology(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }

        $request->validate([
            'csv_file' => 'required|mimes:csv',
            'technology' => 'required|array|min:1',
            'technology.*' => 'exists:ev_technology,id',
        ]);

        $technologyId = $request->input('technology');
        $file = $request->file('csv_file');
        $result = $this->ExcelcommonController->processSyllabusUpload($file, $technologyId);
        return $this->handleRedirect($result);
    }

    private function getTopicListing($techId = null)
    {
        $query = Topic::join('ev_topictechnology as main', 'ev_topics.id', '=', 'main.topic_id')
            ->join('ev_topictechnology as other', 'ev_topics.id', '=', 'other.topic_id')
            ->join('ev_technology', 'other.technology', '=', 'ev_technology.id');

        if ($techId) {
            $query->whereIn('ev_technology.id', (array)$techId);
        }

        $query->select(
            'ev_topics.id',
            'ev_topics.topics',
            'ev_topics.trainer_id',
            DB::raw("GROUP_CONCAT(DISTINCT ev_technology.technology SEPARATOR ' , ') as all_technologies")
        )
            ->groupBy('ev_topics.id', 'ev_topics.topics', 'ev_topics.trainer_id')
            ->orderBy('ev_topics.id', 'desc');

        return $query->get();
    }
    public function topicListingByTechnology(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
    
        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }

        $techId = $request->input('techname');
        $topics = $this->getTopicListing($techId);
        $subtopics = $this->getSubtopicsByTopics($topics,$techId);
        foreach ($topics as $topic) {
            $topic->subtopics = $subtopics->get($topic->id, collect());
        }
    
        $data = [
            "technologies" => Technology::all(),
            "topics" => $topics,
            "title" => "Topic Listing",
            "tech_id" => $techId,
        ];
    
        return view("topic_listing", $data);
    }
    private function getSubtopicsByTopics($topics, $techId = null)
    {
        $topicIds = $topics->pluck('id');
    
        $query = Subtopic::leftJoin('ev_subtopictechnology', 'ev_subtopics.id', '=', 'ev_subtopictechnology.subtopic_id')
            ->leftJoin('ev_technology', 'ev_subtopictechnology.technology', '=', 'ev_technology.id')
            ->whereIn('ev_subtopictechnology.topic_id', $topicIds);
    
        if ($techId) {
            $query->whereIn('ev_technology.id', (array)$techId);
        }
    
        return $query->select(
            'ev_subtopics.id as subtopic_id',
            'ev_subtopictechnology.topic_id',
            'ev_subtopics.sub_topics as subtopic_name',
            DB::raw("GROUP_CONCAT(DISTINCT ev_technology.technology SEPARATOR ', ') as technologies")
        )
        ->groupBy('ev_subtopics.id', 'ev_subtopictechnology.topic_id', 'ev_subtopics.sub_topics')
        ->get()
        ->groupBy('topic_id');
    }

public function fetchLinkedTechnologies(Request $request)
{
    $request->validate([
        'topic_id' => 'required|integer',
    ]);

    $topic_id = $request->get('topic_id');

    $technologies = Topic::join('ev_topictechnology', 'ev_topics.id', '=', 'ev_topictechnology.topic_id')
                         ->join('ev_technology', 'ev_topictechnology.technology', '=', 'ev_technology.id')
                         ->where('ev_topics.id', $topic_id)
                         ->get(['ev_technology.technology', 'ev_technology.id']);

    return response()->json(['tech' => $technologies]);
}


public function subtopicUploadByTechnology(Request $request)
    {
        $topicId = $request->input('topic_id');
        $tech_id = $request->input('technology');


        $request->validate([
            'csv_file' => 'required|mimes:csv,txt',
        ]);

        $file = $request->file('csv_file');
        $result = $this->ExcelcommonController->processSubtopicUpload($topicId, $file, $tech_id);



        return $this->redirectWithSubtopics($result);
    }


    public function viewTechnologySyllabus($pacid,Request $request)
    {

        if (!Auth::check()) {
            return redirect('/');
        }

        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }

        $package = Package::where('pac_id', $pacid)->first(['tech_id', 'pac_name']);
        $selectedTechId = $request->input('techname') ?? $package->tech_id;
                $topics = $this->getTopicListing($selectedTechId);

        
        $subtopics = $this->getSubtopicsByTopics($topics, $selectedTechId);
        if ($request->ajax()) {
            $topicId = $request->input('topic_id');

            $linkedtech = $this->fetchLinkedTechnologies($topicId);
            return response()->json(['linkedtech' => $linkedtech]);
        }
    


        foreach ($topics as $topic) {
            $topic->subtopics = $subtopics->get($topic->id, collect());
        }

        $data = [
            "technologies" => Technology::all(),
            "topics" => $topics,
            "title" => "$package->pac_name Topic Listing",
            "tech_id" => $selectedTechId,
            'pac_name' => $package->pac_name,
            "pac_id" => $pacid
        ];


        return view("add_packagesyllabus", $data);
    }

   



  






}
