<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Technology;
use App\Models\Designation;
use App\Models\Qualification;
use App\Models\Source;
use App\Models\College;
use App\Models\User;
use App\Models\Contacts;
use App\Models\Followup;
use App\Models\Package;
use App\Models\Studentpackage;
use App\Models\Studentfeessplit;
use App\Models\Payment;
use App\Models\Salespayment;
use App\Models\Tax;
use App\Models\Salespackage;
use App\Models\Reporting;
use App\Models\OldPayment;
use App\Models\Oldstudent;
use Illuminate\Support\Facades\Hash;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware("auth");
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     public function editProfile()
     {
            
                 $data['title']='Edit Profile';
                 return view("edit_profile",$data);
     }

     private function processImage($file)
     {
         $extension = $file->getClientOriginalExtension();
         $destinationPath = public_path('img');
         $fileName = rand(11111, 99999) . "." . $extension;
     
         $imagePath = $file->getRealPath();
         $image = imagecreatefromstring(file_get_contents($imagePath));
     
         if ($image === false) {
             return null; // Image creation failed
         }
     
         // Resize image
         $newImage = $this->resizeImage($image, 800);
         if ($newImage === false) {
             imagedestroy($image);
             return null; // Image resizing failed
         }
     
         // Save the new image
         imagejpeg($newImage, $destinationPath . '/' . $fileName, 75);
     
         // Free up memory
         imagedestroy($image);
         imagedestroy($newImage);
     
         return $fileName;
     }
     
     private function resizeImage($image, $newWidth)
     {
         $width = imagesx($image);
         $height = imagesy($image);
         $newHeight = ($height / $width) * $newWidth;
     
         $newImage = imagecreatetruecolor($newWidth, $newHeight);
         if ($newImage === false) {
             return false; // Image resizing failed
         }
     
         imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
     
         return $newImage;
     }


public function updateProfile(Request $request)
{
  
    $request->validate([
        'userid' => 'required|exists:users,id',
        'name' => 'required|string|max:255',
        'photo' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048', // Add validation rules for the photo
    ]);

    $userId = $request->input('userid');
    $user = User::find($userId);
    $fileName = $user->photo; 

    if ($request->hasFile('photo')) {
        $fileName = $this->processImage($request->file('photo'));

        if (!$fileName) {
            return redirect('/edit_profile')->with('error', 'Image processing failed.');
        }
    }

    $updated =    User::where('id', $request->input('userid'))->update([
        'name'  => $request->input('name'),
        'photo' => $fileName,
    ]);




    if ($updated) {
        return redirect('/edit_profile')->with('success', 'Profile successfully updated.');
    }

    return redirect('/edit_profile')->with('error', 'Error in updating profile.');
}

public function changePassword()
    {
            
                $data['title']='Change Password';
                return view("change_password",$data);
    }

    public function passwordMatch($userid)
    {
        $user = User::where('id', $request->id)->first();
       
        if ($user && Hash::check($request->password, $user->password)) {
            return response()->json(1);
        }
    
        return response()->json(0);
    }

    public function updatePassword(Request $request)
    {
        $user = User::find($request->userid);

        if ($user && Hash::check($request->current_password, $user->password)) {
            $updated = $user->update([
                'password' => Hash::make($request->new_password),
                'password_text' => $request->new_password, 
            ]);

            return redirect('/')->with(
                $updated==true ? 'success' : 'errormessage', 
                $updated=='success' ? 'Password successfully updated.' : 'Error in updation.'
            );
        }
        return redirect('/change_password')->with('errormessage', 'The current password entered is incorrect');

    }

    public function index()
    {
        if (Auth::user()->role == 6) {
            return view("management_dashboard");
        }
        if (Auth::user()->role == 7) {
            return view("finance_dashboard");
        }
        if (Auth::user()->role == 8) {
            return view("subadmin_dashboard");
        }
        if (Auth::user()->role == 9) {

            return view("placement_dashboard");

        }
        if (Auth::user()->role == 12) {

            return view("master_dashboard");

        }

        if (Auth::user()->role == 2) {

            $user_id =Auth::user()->id;
            $data['reportedlist']           =Reporting::join("users","users.id", "=","ev_reporting.employee_id")
                                                    -> where('ev_reporting.reported_to',$user_id)->get(['users.id','users.name']);
            $user_id = Auth::user()->id;
            //Monthly Reports
            $today = date("Y-m-d");
            $first_date = date("Y-m-01");


            $data["claim_withgst"]             =round(Salespackage::where("sales_package.sales_id",$user_id)
            ->where("sales_package.claim_status",1)
            ->where("sales_package.package_type",2)
            ->whereBetween("sales_package.joining_date", [$first_date, $today])
            ->sum("sales_package.package_fullamount")); 
            $data["claim_withoutgst"]             =round(Salespackage::where("sales_package.sales_id",$user_id)
                        ->where("sales_package.claim_status",1)
                        ->where("sales_package.package_type",2)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_amount")); 
            $data["claim_tax"]             =round(Salespackage::where("sales_package.sales_id",$user_id)
                        ->where("sales_package.claim_status",1)
                        ->where("sales_package.package_type",2)
                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                        ->sum("sales_package.package_tax")); 



        //MONTHLY REVENUE START

            // Monthly chrysalis 
            $data["monthrevenue_chry_withgst"]      = round(Salespayment::where("sales_payment.sales_id",$user_id)->where("sales_payment.package_type",2)
                                                         ->whereBetween("sales_payment.date", [$first_date, $today])
                                                         ->sum("sales_payment.total"));


           $data["monthrevenue_chry_withoutgst"]  = round(Salespayment::where("sales_payment.sales_id",$user_id)->where("sales_payment.package_type",2)
                                                         ->whereBetween("sales_payment.date", [$first_date, $today])
                                                         ->sum("sales_payment.payment"));

           $data["monthrevenue_chry_tax"]  = round(Salespayment::where("sales_payment.sales_id",$user_id)->where("sales_payment.package_type",2)
                                                         ->whereBetween("sales_payment.date", [$first_date, $today])
                                                         ->sum("sales_payment.tax"));

        // Monthly Regular and old  

            
            $data["monthrevenue_regular_withgst"] = round( Salespayment::where("sales_payment.sales_id",$user_id)
                                                         ->where("sales_payment.package_type",1)
                                                        ->whereBetween("sales_payment.date", [$first_date, $today])
                                                        ->sum("sales_payment.total"));

            $data["monthrevenue_old_withgst"] = round(OldPayment::where("olddata_payment.sales_id",$user_id)
                                                    ->whereBetween("olddata_payment.date", [$first_date, $today])
                                                     ->sum("olddata_payment.paid_amount"));

            $data['monthrevenue_regold_withgst']         = $data["monthrevenue_regular_withgst"] +   $data["monthrevenue_old_withgst"];

            $data["monthrevenue_regular_tax"] = round( Salespayment::where("sales_payment.sales_id",$user_id)
                                                            ->where("sales_payment.package_type",1)
                                                        ->whereBetween("sales_payment.date", [$first_date, $today])
                                                        ->sum("sales_payment.tax"));

            $data["monthrevenue_old_tax"] = round(OldPayment::where("olddata_payment.sales_id",$user_id)
                                                    ->whereBetween("olddata_payment.date", [$first_date, $today])
                                                        ->sum("olddata_payment.tax"));

            $data['monthrevenue_regold_tax']         = $data["monthrevenue_regular_tax"] +   $data["monthrevenue_old_tax"];

           
            $data["monthrevenue_regular_withoutgst"] = round( Salespayment::where("sales_payment.sales_id",$user_id)->where("sales_payment.package_type",1)
                                                     ->whereBetween("sales_payment.date", [$first_date, $today])
                                                     ->sum("sales_payment.payment"));
            $data["monthrevenue_old_withoutgst"] = round(OldPayment::where("olddata_payment.sales_id",$user_id)
                                                    ->whereBetween("olddata_payment.date", [$first_date, $today])
                                                     ->sum("olddata_payment.fees_income"));
            $data['monthrevenue_regold_withoutgst']         = $data["monthrevenue_regular_withoutgst"] +   $data["monthrevenue_old_withoutgst"];

             // Monthly total
             $data['monthlytotal_withgst']      = $data["monthrevenue_chry_withgst"] +  $data['monthrevenue_regold_withgst'] ;
             
             $data['monthlytotal_withoutgst']   = $data["monthrevenue_chry_withoutgst"] +  $data['monthrevenue_regold_withoutgst'] ;
       
              $data['total_tax']  = $data["monthrevenue_chry_tax"] +$data["monthrevenue_regular_tax"]+$data["monthrevenue_old_tax"];
       
             //MONTHLY REVENUE END

        //MONTHLY ADMISSION  START

            // Monthly chrysalis 
            $data["monthadmi_chry_withgst"]         =round(Salespackage::where("sales_package.sales_id",$user_id)
                                                        ->where("sales_package.package_type",2)
                                                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                                                        ->sum("sales_package.package_fullamount"));

            $data["monthadmi_chry_withoutgst"]      =round(Salespackage::where("sales_package.sales_id",$user_id)
                                                        ->where("sales_package.package_type",2)
                                                        ->whereBetween("sales_package.joining_date",[$first_date, $today])
                                                        ->sum("sales_package.package_amount")); 
            $data["monthadmi_chry_tax"]             =round(Salespackage::where("sales_package.sales_id",$user_id)
                                                        ->where("sales_package.package_type",2)
                                                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                                                        ->sum("sales_package.package_tax")); 
            //Monthly Admissions

            $data["monthadmi_regular_withgst"]      = round(Salespackage::where("sales_package.sales_id",$user_id)
                                                            ->where("sales_package.package_type",1)
                                                             ->whereBetween("sales_package.joining_date", [$first_date, $today])
                                                            ->sum("sales_package.package_fullamount"));

            $data['reg_old_withgst']                 = round(Oldstudent::where("ev_oldstudents.sales_id",$user_id)
                                                            ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                                                            ->sum("ev_oldstudents.totalfees"));
            

            $data['monthly_tot_admissionwithgst']   =  $data["monthadmi_regular_withgst"] +  $data['reg_old_withgst'];






            $data["monthadmi_regular_withoutgst"]      = round(Salespackage::where("sales_package.sales_id",$user_id)
                                                        ->where("sales_package.package_type",1)
                                                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                                                        ->sum("sales_package.package_amount"));

            $data['reg_old_withoutgst']                 = round(Oldstudent::where("ev_oldstudents.sales_id",$user_id)
                                                            ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                                                            ->sum("ev_oldstudents.fees"));
            $data['monthly_tot_admissionwithoutgst']    =  $data["monthadmi_regular_withoutgst"] +  $data['reg_old_withoutgst'];




            $data["monthadmi_regular_tax"]      = round(Salespackage::where("sales_package.sales_id",$user_id)
                                                        ->where("sales_package.package_type",1)
                                                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                                                        ->sum("sales_package.package_tax"));

            $data['reg_old_tax']                 = round(Oldstudent::where("ev_oldstudents.sales_id",$user_id)
                                                            ->whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                                                            ->sum("ev_oldstudents.tax"));
            $data['monthly_tot_admissiontax']    =  $data["monthadmi_regular_tax"] +  $data['reg_old_tax'];


            //Total(Admission_Old_Chrysalis)


            $data['total_reg_chry_old_withgst']  =  $data['monthly_tot_admissionwithgst'] + $data["monthadmi_chry_withgst"];

            $data['total_reg_chry_old_withoutgst']  =  $data['monthly_tot_admissionwithoutgst'] + $data["monthadmi_chry_withoutgst"] ;

            $data['total_reg_chry_old_tax']  =  $data['monthadmi_chry_tax'] + $data["monthly_tot_admissiontax"] ;



            return view("sales_dashboard", $data);
        }

        if (Auth::user()->role == 1) {


            //Monthly Reports
            $today = date("Y-m-d");
            $first_date = date("Y-m-01");

        //MONTHLY REVENUE START

            // Monthly chrysalis 
            $data["monthrevenue_chry_withgst"]      = round(Salespayment::where("sales_payment.package_type",2)
                                                         ->whereBetween("sales_payment.date", [$first_date, $today])
                                                         ->sum("sales_payment.total"));


           $data["monthrevenue_chry_withoutgst"]  = round(Salespayment::where("sales_payment.package_type",2)
                                                         ->whereBetween("sales_payment.date", [$first_date, $today])
                                                         ->sum("sales_payment.payment"));

           $data["monthrevenue_chry_tax"]  = round(Salespayment::where("sales_payment.package_type",2)
                                                         ->whereBetween("sales_payment.date", [$first_date, $today])
                                                         ->sum("sales_payment.tax"));

        // Monthly Regular and old  

            
            $data["monthrevenue_regular_withgst"] = round( Salespayment::where("sales_payment.package_type",1)
                                                        ->whereBetween("sales_payment.date", [$first_date, $today])
                                                        ->sum("sales_payment.total"));

            $data["monthrevenue_old_withgst"] = round(OldPayment::whereBetween("olddata_payment.date", [$first_date, $today])
                                                     ->sum("olddata_payment.paid_amount"));

            $data['monthrevenue_regold_withgst']         = $data["monthrevenue_regular_withgst"] +   $data["monthrevenue_old_withgst"];

            $data["monthrevenue_regular_tax"] = round( Salespayment::where("sales_payment.package_type",1)
                                                        ->whereBetween("sales_payment.date", [$first_date, $today])
                                                        ->sum("sales_payment.tax"));

            $data["monthrevenue_old_tax"] = round(OldPayment::whereBetween("olddata_payment.date", [$first_date, $today])
                                                        ->sum("olddata_payment.tax"));

            $data['monthrevenue_regold_tax']         = $data["monthrevenue_regular_tax"] +   $data["monthrevenue_old_tax"];

           
            $data["monthrevenue_regular_withoutgst"] = round( Salespayment::where("sales_payment.package_type",1)
                                                     ->whereBetween("sales_payment.date", [$first_date, $today])
                                                     ->sum("sales_payment.payment"));

            $data["monthrevenue_old_withoutgst"] = round(OldPayment::whereBetween("olddata_payment.date", [$first_date, $today])
                                                     ->sum("olddata_payment.fees_income"));

            $data['monthrevenue_regold_withoutgst']         = $data["monthrevenue_regular_withoutgst"] +   $data["monthrevenue_old_withoutgst"];

             // Monthly total
             $data['monthlytotal_withgst']      = $data["monthrevenue_chry_withgst"] +  $data['monthrevenue_regold_withgst'] ;
             $data['monthlytotal_withoutgst']   = $data["monthrevenue_chry_withoutgst"] +  $data['monthrevenue_regold_withoutgst'] ;
       
              $data['total_tax']  = $data["monthrevenue_chry_tax"] +$data["monthrevenue_regular_tax"]+$data["monthrevenue_old_tax"];
       
             //MONTHLY REVENUE END

        //MONTHLY ADMISSION  START

            // Monthly chrysalis 
            $data["monthadmi_chry_withgst"]         =round(Salespackage::where("sales_package.package_type",2)
                                                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                                                        ->sum("sales_package.package_fullamount"));

            $data["monthadmi_chry_withoutgst"]      =round(Salespackage::where("sales_package.package_type",2)
                                                        ->whereBetween("sales_package.joining_date",[$first_date, $today])
                                                        ->sum("sales_package.package_amount")); 

            $data["monthadmi_chry_tax"]             =round(Salespackage::where("sales_package.package_type",2)
                                                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                                                        ->sum("sales_package.package_tax")); 
            //Monthly Admissions

            $data["monthadmi_regular_withgst"]      = round(Salespackage::where("sales_package.package_type",1)
                                                             ->whereBetween("sales_package.joining_date", [$first_date, $today])
                                                            ->sum("sales_package.package_fullamount"));

            $data['reg_old_withgst']                 = round(Oldstudent::whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                                                            ->sum("ev_oldstudents.totalfees"));
            

            $data['monthly_tot_admissionwithgst']   =  $data["monthadmi_regular_withgst"] +  $data['reg_old_withgst'];


            $data["monthadmi_regular_withoutgst"]      = round(Salespackage::where("sales_package.package_type",1)
                                                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                                                        ->sum("sales_package.package_amount"));



            $data['reg_old_withoutgst']                 = round(Oldstudent::whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                                                            ->sum("ev_oldstudents.fees"));
            $data['monthly_tot_admissionwithoutgst']    =  $data["monthadmi_regular_withoutgst"] +  $data['reg_old_withoutgst'];




            $data["monthadmi_regular_tax"]      = round(Salespackage::where("sales_package.package_type",1)
                                                        ->whereBetween("sales_package.joining_date", [$first_date, $today])
                                                        ->sum("sales_package.package_tax"));

            $data['reg_old_tax']                 = round(Oldstudent::whereBetween("ev_oldstudents.date_joining", [$first_date, $today])
                                                            ->sum("ev_oldstudents.tax"));
            $data['monthly_tot_admissiontax']    =  $data["monthadmi_regular_tax"] +  $data['reg_old_tax'];

            //Total(Admission_Old_Chrysalis)

            $data['total_reg_chry_old_withgst']  =  $data['monthly_tot_admissionwithgst'] + $data["monthadmi_chry_withgst"];

            $data['total_reg_chry_old_withoutgst']  =  $data['monthly_tot_admissionwithoutgst'] + $data["monthadmi_chry_withoutgst"] ;

            $data['total_reg_chry_old_tax']  =  $data['monthadmi_chry_tax'] + $data["monthly_tot_admissiontax"] ;
            return view("admin_dashboard", $data);


        }

        if (Auth::user()->role == 3) {
            return view("trainer_dashboard");
        }
        if (Auth::user()->role == 4) {
            return view("processor_dashboard");
        }
        if (Auth::user()->role == 5) {

           $status = Auth::user()->admission_completed;

           if($status==0){

           return redirect('/student_admission');

           }
           else{

            return redirect('/student_admission_details');

           }
        
        }
        if (Auth::user()->role == 10) {

            return view("recruitment_dashboard");

        }


    }
}
