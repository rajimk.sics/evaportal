<?php
namespace App\Http\Middleware\CheckStatus;
namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Questions;
use App\Models\Examresult;
use App\Models\Examhistory;
use App\Models\Examquestion;
use Illuminate\Http\Request;
use App\Models\Examtechnology;
use App\Models\Exam;
use DB;
use App\Models\Studentpackage;
use Illuminate\Support\Facades\Log;

class ApiexamController extends Controller
{
        //function for fetching exams on student_id
       
       
       // FUNCTION for showing the EXAM RESULTS
       public function apiExamresult(Request $request)
       {
               $student_id = $request->get('student_id');

               $results = Examresult::where('ev_examresult.stud_id', $student_id)
                       ->join('ev_exam', 'ev_examresult.exam_id', '=', 'ev_exam.id')
                       ->join('ev_examquestion', 'ev_examresult.exam_id', '=', 'ev_examquestion.exam_id')
                       ->select(
                               'ev_exam.exam_name',
                               'ev_examresult.score',
                               DB::raw('COUNT(DISTINCT ev_examquestion.question_id) as out_of')
                       )
                       ->groupBy('ev_examresult.exam_id', 'ev_exam.exam_name', 'ev_examresult.score')
                       ->get();

               $data = [
                       'status' => $results->isNotEmpty(),
                       'results' => $results,
               ];

               return response()->json($data);
       }

       public function apiExamlist(Request $request)
       {
           $student_id = $request->get('student_id');
       
           $studentExamsQuery = Studentpackage::join('ev_package', 'ev_student_package.package_id', '=', 'ev_package.pac_id')
               ->join('ev_examtechnology', 'ev_package.tech_id', '=', 'ev_examtechnology.technology_id')
               ->join('ev_exam', 'ev_examtechnology.exam_id', '=', 'ev_exam.id')
               ->where('ev_student_package.student_id', $student_id)
               ->where('ev_exam.exam_type', 'Talento')
               ->where('ev_exam.status', 1)
               ->select('ev_exam.id as exam_id', 'ev_exam.exam_name as exam_name');
       
           $generalExamsQuery = Examtechnology::join('ev_exam', 'ev_examtechnology.exam_id', '=', 'ev_exam.id')
               ->where('ev_examtechnology.technology_id', 62)
               ->where('ev_exam.exam_type', 'Talento')
               ->select('ev_exam.id as exam_id', 'ev_exam.exam_name as exam_name');
       
           $exams = $studentExamsQuery->union($generalExamsQuery)->get();
       
           $attendedExamIds = Examresult::where('stud_id', $student_id)
               ->pluck('exam_id')
               ->toArray();
       
           $commonImagePath = '';
       
           $processedExams = $exams->map(function ($exam) use ($attendedExamIds, $commonImagePath) {
               $exam->examStatus = !in_array($exam->exam_id, $attendedExamIds);
               $exam->image_path = $commonImagePath;
               return $exam;
           });
       
           $data = [
               'status' => $processedExams->isNotEmpty(),
               'exams' => $processedExams,
           ];
       
           return response()->json($data);
       }

       
       
       


        //fetch questions based on exam_id
        public function apiExamquestions(Request $request)
        {
                $exam_id = $request->get('exam_id');
                $questions = Examquestion::join('ev_questions', 'ev_examquestion.question_id', '=', 'ev_questions.id')
                        ->where('ev_examquestion.exam_id', $exam_id)
                        ->where('ev_questions.status', 1)
                        ->inRandomOrder()
                        ->get('ev_questions.*');

                $data = [
                        'status' => $questions->isNotEmpty(),
                        'data' => $questions
                ];

                return response()->json($data);
        }
        // FUNCTION for saving the EXAMS RESULT 

        public function apiExamsubmit(Request $request)
        {
            $student_id = $request->get('student_id');
            $exam_id = $request->get('exam_id');
            $selectedOptions = $request->get('selected_options');
            $score = 0;
            $wrong = 0;
            $skipped = 0;
        
            $totalQuestions = Examquestion::where('exam_id', $exam_id)->count();
            $examName = Exam::where('id',$exam_id)->value('exam_name');
        
            $examResult = Examresult::insertGetId([
                'stud_id' => $student_id,
                'exam_id' => $exam_id,
                'score' => 0,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        
            foreach ($selectedOptions as $option) {
                $question_id = $option['question_id'];
                $selected_option = $option['selected_option'];
                $correctAnswer = Questions::where('id', $question_id)->value('answer');
        
                if (empty($selected_option)) {
                    $skipped++;
                    $isCorrect = false;
                } else {
                    $isCorrect = ($selected_option == $correctAnswer);
                    if ($isCorrect) {
                        $score++;
                    } else {
                        $wrong++;
                    }
                }
        
                Examhistory::insert([
                    'examresult_id' => $examResult,
                    'question_id' => $question_id,
                    'marked_answer' => $selected_option,
                    'correct_answer' => $correctAnswer,
                    'score_status' => $isCorrect ? 1 : 0,
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
            }
        
            Examresult::where('id', $examResult)->update(['score' => $score]);
        
            $data = [
                'status' => true,
                'message' => 'Exam submitted successfully.',
                'exam_name' => $examName,
                'score' => $score,
                'out_of' => $totalQuestions,
                'wrong' => $wrong,
                'skipped' => $skipped,
            ];
        
            return response()->json($data);
        }

}
