<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use DateTime;

use App\Models\User;
use App\Models\Topic;
use League\Csv\Reader;
use App\Models\Package;
use App\Models\Subtopic;
use App\Models\Questions;
use App\Models\Attendance;
use App\Models\Technology;
use App\Models\Employeeinfo;
use App\Models\Prerequisite;
use Illuminate\Http\Request;
use App\Helpers\CustomHelper;
use App\Models\Studentpackage;
use App\Models\StudentTrainer;
use Illuminate\Support\Carbon;
use App\Models\AssignedStudent;
use App\Models\Packagesubtopic;
use App\Models\Packagesyllabus;
use App\Models\Evaluationresult;
use App\Models\BatchplanStudents;
use App\Models\Subtopictechnology;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ExcelcommonController;

class MentorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $ExcelcommonController;

    public function __construct()
    {
        $this->middleware("auth");
        $this->ExcelcommonController = new ExcelcommonController();
    }

    public function trainerStudents(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        if (!in_array(Auth::user()->role, [3])) {
            return redirect('/accessdenied');
        }

        $trainer_id = Auth::user()->id;
        $data['title'] = 'Your Students';
        $year = $request->query('year', Carbon::now()->year);
        $month = $request->query('month', Carbon::now()->format('m'));
        $pac_type = $request->query('packtype', '1');
        $allStudents = $request->query('all', false);

        if (!checkdate($month, 1, $year) && !$allStudents) {
            return redirect('/accessdenied');
        }

        $query = Studentpackage::join('users', 'users.id', '=', 'ev_student_package.student_id')
            ->join('ev_student_info', 'ev_student_info.student_id', '=', 'users.id')
            ->join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
            ->leftJoin('ev_student_trainer', 'ev_student_trainer.student_id', '=', 'users.id')
            ->where('ev_student_trainer.trainer_id', $trainer_id)
            ->where('ev_package.pac_type', $pac_type);

        if (!$allStudents) {
            $startOfMonth = Carbon::create($year, $month, 1)->startOfMonth()->format('Y-m-d');
            $endOfMonth = Carbon::create($year, $month, 1)->endOfMonth()->format('Y-m-d');
            $query->whereBetween('ev_student_package.joining_date', [$startOfMonth, $endOfMonth]);
        }

        $data1['studlist'] = $query->orderby('users.id', 'desc')
            ->get([
                'users.name',
                'users.id',
                'users.phone',
                'ev_package.pac_name',
                'users.email',
                'ev_student_info.sales_id',
                'ev_student_package.due_date_first',
                'ev_student_package.enddate',
                'ev_student_package.package_type as pac_type',
                'ev_student_package.package_id as package_id',
                'ev_student_package.id as studentpackageid',
                'ev_student_package.joining_date',
                'ev_package.tech_id',
                'ev_student_info.student_admissionid',
            ]);

        $data = [
            'title' => 'Your Students',
            'studlist' => $data1['studlist'],
            'years' => CommonController::generateYearRange(date('Y') - 10),
            'months' => CommonController::generateMonthOptions(),
            'currentYear' => $year,
            'currentMonth' => $month,
            'pac_type' => $pac_type,
        ];

        return view("manage_students_mentor", $data);
    }







    public function getTopics(Request $request, $techId = null, $assigning = null)
    {
        $techId = $request->input('tech_id');
        $assigning = $request->input('assigning');

        $topics = Topic::join('ev_topictechnology', 'ev_topics.id', '=', 'ev_topictechnology.topic_id')
            ->join('ev_technology', 'ev_topictechnology.technology', '=', 'ev_technology.id');

        if ($assigning === "filter") {
            $topics->where('ev_topics.status', '=', 1);
        }

        if ($techId) {
            $topics->whereIn('ev_technology.id', (array)$techId);
        }

        $topics->select(
            'ev_topics.id',
            'ev_topics.topics',
            'ev_topics.trainer_id',
            'ev_topics.status',
            DB::raw("GROUP_CONCAT(DISTINCT ev_technology.technology SEPARATOR ', ') as all_technologies")
        )
            ->groupBy('ev_topics.id', 'ev_topics.topics', 'ev_topics.trainer_id', 'ev_topics.status')
            ->orderBy('ev_topics.id', 'desc');

        $paginatedTopics = $topics->paginate(10);
        $hasMore = $paginatedTopics->hasMorePages();
        $nextPageUrl = $paginatedTopics->appends([
            'tech_id' => $techId,
            'assigning' => $assigning,
        ])->nextPageUrl();

        $subtopicsByTopics = $this->getSubtopicsByTopics($paginatedTopics, $techId);

        $topicsWithSubtopics = $paginatedTopics->map(function ($topic) use ($subtopicsByTopics) {
            $topic->subtopics = $subtopicsByTopics[$topic->id] ?? [];
            return $topic;
        });

        return response()->json([
            'data' => $topicsWithSubtopics,
            'next_page_url' => $nextPageUrl,
            'hasMore' => $hasMore,

        ]);
    }
    //TOPIC AND SUBTOPIC
    private function handleRedirect($responseData)
    {
        if (is_array($responseData) && isset($responseData['duplicates']) && $responseData['duplicates']) {
            Session::flash('duplicates', $responseData['duplicates']);
            return redirect()->back();
        }
        if (isset($responseData['error']) && !empty($responseData['error'])) {
            return redirect()->back()->withErrors(['error' => $responseData['error']]);
        } else {
            Session::flash('success', 'Successfully Updated');
            return redirect('/topicListingByTechnology');
        }
    }

    private function redirectWithSubtopics($responseData)
    {
        if (isset($responseData['duplicates']) && !empty($responseData['duplicates'])) {
            Session::flash('duplicates', $responseData['duplicates']);
            return redirect()->back();
        }
        if (isset($responseData['error']) && !empty($responseData['error'])) {
            return redirect()->back()->withErrors(['error' => $responseData['error']]);
        } else {
            Session::flash('success', 'Successfully Updated');
            return redirect('/topicListingByTechnology');
        }
    }
    private function getTechnologyOfTrainer($id)
    {
        return  Employeeinfo::join('ev_technology', function ($join) {
            $join->on(DB::raw("FIND_IN_SET(ev_technology.id, employee_info.technology)"), '>', DB::raw('0'));
        })
            ->where('employee_info.user_id', $id)
            ->where("ev_technology.status", 1)
            ->get();
    }

    public function uploadSylabus()
    {

        if (!Auth::check()) {
            return redirect('/');
        }

        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }

        $data = [
            'title' => 'Syllabus Upload',
            'technologies' =>  Technology::where('status', '1')->get()
        ];
        return view('syllabusupload', $data);
    }
    public function UploadSyllabusByTechnology(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }

        $request->validate([
            'csv_file' => 'required|mimes:csv',
            'technology' => 'required|array|min:1',
            'technology.*' => 'exists:ev_technology,id',
        ]);

        $technologyId = $request->input('technology');
        $file = $request->file('csv_file');
        $result = $this->ExcelcommonController->processSyllabusUpload($file, $technologyId);
        return $this->handleRedirect($result);
    }
    private function getTopicListing($techId = null, $assigning = null)
    {
        $query = Topic::join('ev_topictechnology', 'ev_topics.id', '=', 'ev_topictechnology.topic_id')
            ->join('ev_technology', 'ev_topictechnology.technology', '=', 'ev_technology.id');

        if ($assigning == "filter") {
            $query->where('ev_topics.status', '=', 1);
        }

        if ($techId) {
            $query->whereIn('ev_technology.id', (array)$techId);
        }

        $query->select(
            'ev_topics.id',
            'ev_topics.topics',
            'ev_topics.trainer_id',
            'ev_topics.status',

            DB::raw("GROUP_CONCAT(DISTINCT ev_technology.technology SEPARATOR ', ') as all_technologies")
        )
            ->groupBy('ev_topics.id', 'ev_topics.topics', 'ev_topics.trainer_id', 'ev_topics.status')
            ->orderBy('ev_topics.id', 'desc');

        return $query->get();
    }


    private function selectedTopics(Request $request)
    {
        return $request->input('selected_topics');
    }

    public function topicListingByTechnology(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }

        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }

        $selectedTopics = $this->selectedTopics($request);
        $techId = $request->input('techname');
        $data = [
            "technologies" => Technology::all(),
            "selectedTopics" => $selectedTopics,
            "title" => "Topic Listing",
            "tech_id" => $techId,
        ];

        return view("topic_listing", $data);
    }
    private function getSubtopicsByTopics($topics, $techId = null)
    {
        $topicIds = $topics->pluck('id');

        $query = Subtopic::leftJoin('ev_subtopictechnology', 'ev_subtopics.id', '=', 'ev_subtopictechnology.subtopic_id')
            ->leftJoin('ev_technology', 'ev_subtopictechnology.technology', '=', 'ev_technology.id')
            ->whereIn('ev_subtopictechnology.topic_id', $topicIds);

        if ($techId) {
            $query->whereIn('ev_technology.id', (array)$techId);
        }

        return $query->select(
            'ev_subtopics.id as subtopic_id',
            'ev_subtopictechnology.topic_id',
            'ev_subtopics.sub_topics as subtopic_name',
            DB::raw("GROUP_CONCAT(DISTINCT ev_technology.technology SEPARATOR ', ') as technologies")
        )
            ->groupBy('ev_subtopics.id', 'ev_subtopictechnology.topic_id', 'ev_subtopics.sub_topics')
            ->get()
            ->groupBy('topic_id');
    }

    public function fetchLinkedTechnologies(Request $request)
    {
        $request->validate([
            'topic_id' => 'required|integer',
        ]);

        $topic_id = $request->get('topic_id');

        $technologies = Topic::join('ev_topictechnology', 'ev_topics.id', '=', 'ev_topictechnology.topic_id')
            ->join('ev_technology', 'ev_topictechnology.technology', '=', 'ev_technology.id')
            ->where('ev_topics.id', $topic_id)
            ->get(['ev_technology.technology', 'ev_technology.id']);

        return response()->json(['tech' => $technologies]);
    }


    public function subtopicUploadByTechnology(Request $request)
    {
        $topicId = $request->input('topic_id');
        $tech_id = $request->input('technology');


        $request->validate([
            'csv_file' => 'required|mimes:csv,txt',
        ]);

        $file = $request->file('csv_file');
        $result = $this->ExcelcommonController->processSubtopicUpload($topicId, $file, $tech_id);



        return $this->redirectWithSubtopics($result);
    }




    public function viewTechnologySyllabus($pacid, Request $request)
    {

        if (!Auth::check()) {
            return redirect('/');
        }

        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }

        $package = Package::where('pac_id', $pacid)->first(['tech_id', 'pac_name']);
        $selectedTechId = $request->input('techname') ?? $package->tech_id;





        $data = [
            "technologies" => Technology::all(),
            "tech_id" => $selectedTechId,
            'pac_name' => $package->pac_name,
            "pac_id" => $pacid
        ];


        return view("add_packagesyllabus", $data);
    }
    //function for assigning syllabus to packages
    public function assignSyllabus(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'pac_id' => 'required',
            'selected_topics' => 'required',
            'selected_topics.*' => 'required',
        ]);


        if ($validator->fails()) {
            return redirect()->back()->with('error', 'Please select at least one topic');
        }

        $pac_id = $request->input('pac_id');
        $topicIds = $request->input('selected_topics', []);
        $subtopicIds = $request->input('selected_subtopics');
        $subtopicDuplicates = [];

        $topicDuplicates = $this->processSyllabusAssignment($pac_id, $topicIds);
        if ($subtopicIds) {
            $subtopicDuplicates = $this->processSubtopicAssignment($pac_id, $subtopicIds, $topicIds);
        }


        $duplicates = array_merge($topicDuplicates, $subtopicDuplicates);

        if (!empty($duplicates)) {
            return redirect()->back()->with('duplicates', $duplicates)
                ->with('success', 'Syllabus assigned successfully!');
        } else {
            return redirect('/viewPacSyllabus/' . $pac_id)->with('success', 'Syllabus assigned successfully!');
        }
    }

    private function processSyllabusAssignment($pac_id, array $topicIds)
    {
        $duplicates = [];

        foreach ($topicIds as $topicId) {
            if ($this->isDuplicateSyllabus($pac_id, $topicId)) {
                $duplicates[] = $this->getDuplicateDetails($pac_id, $topicId);
            } else {
                $this->createSyllabusEntry($pac_id, $topicId);
            }
        }

        return $duplicates;
    }
    private function isDuplicateSyllabus($pac_id, $topicId)
    {
        return Packagesyllabus::where('pac_id', $pac_id)
            ->where('topic_id', $topicId)
            ->exists();
    }
    private function getDuplicateDetails($pac_id, $topicId)
    {
        $topic = Topic::where('id', '=', $topicId)->value('topics');
        $package = Package::where('pac_id', '=', $pac_id)->value('pac_name');

        return [
            'dupli' => $topic,
            'technology' => $package,
        ];
    }
    private function createSyllabusEntry($pac_id, $topicId)
    {
        Packagesyllabus::create([
            'pac_id' => $pac_id,
            'topic_id' => $topicId,
        ]);
    }

    ///// for subtopics
    private function processSubtopicAssignment($pac_id, array $subtopicIds, array $topics)
    {
        $duplicates = [];

        foreach ($subtopicIds as $subtopicId) {
            $topicIds = Subtopictechnology::where('subtopic_id', $subtopicId)->pluck('topic_id')->toArray();

            foreach ($topics as $topicId) {
                if (!in_array($topicId, $topicIds)) {
                    continue;
                }

                if ($this->isDuplicateSubtopic($pac_id, $topicId, $subtopicId)) {
                    $duplicates[] = $this->getSubtopicDuplicateDetails($pac_id, $topicId, $subtopicId);
                } else {
                    $this->createSubtopicMapping($pac_id, $topicId, $subtopicId);
                }
            }
        }

        return $duplicates;
    }

    private function isDuplicateSubtopic($pac_id, $topicId, $subtopicId)
    {
        return Packagesubtopic::where('pac_id', $pac_id)
            ->where('topic_id', $topicId)
            ->where('subtopic_id', $subtopicId)
            ->exists();
    }

    private function getSubtopicDuplicateDetails($pac_id, $topicId, $subtopicId)
    {
        $topicName = Topic::where('id', $topicId)->value('topics');
        $subtopicName = Subtopic::where('id', $subtopicId)->value('sub_topics');
        $packageName = Package::where('pac_id', $pac_id)->value('pac_name');

        return [
            'technology' => $packageName,
            'dupli' => $topicName,
            'subtopic' => $subtopicName,
        ];
    }

    private function createSubtopicMapping($pac_id, $topicId, $subtopicId)
    {
        Packagesubtopic::create([
            'pac_id' => $pac_id,
            'topic_id' => $topicId,
            'subtopic_id' => $subtopicId,
        ]);
    }



    public function viewPacSyllabus($pacId)
    {
        $data['pac_id'] = $pacId;
        $data['pac_name'] = Package::where('pac_id', $pacId)->value('pac_name');
        $topicIds = Packagesyllabus::where('pac_id', '=', $pacId)->pluck('topic_id')->toArray();
        $data['topics'] = Topic::whereIn('id', $topicIds)->get();


        $topicsWithSubtopics = [];

        foreach ($topicIds as $topicId) {
            $subtopicIds = Packagesubtopic::where('topic_id', '=', $topicId)
                ->where('pac_id', '=', $pacId)
                ->pluck('subtopic_id')->toArray();

            $subtopics = Subtopic::whereIn('id', $subtopicIds)->get(['id', 'sub_topics']);

            $topicsWithSubtopics[$topicId] = $subtopics;
        }

        foreach ($data['topics'] as $topic) {
            if (isset($topicsWithSubtopics[$topic->id])) {
                $topic->subtopics = $topicsWithSubtopics[$topic->id];
            } else {
                $topic->subtopics = collect();
            }
        }

        return view('syllabus_listing', $data);
    }

    function removeTopic(Request $request)
    {
        $pacid = $request->input('pacid');
        $topicid = $request->input('topicid');

        Packagesyllabus::where('pac_id', $pacid)->where('topic_id', $topicid)->delete();
        Packagesubtopic::where('pac_id', $pacid)->where('topic_id', $topicid)->delete();

        return response()->json(['status' => 'success', 'message' => 'Topic removed successfully.']);
    }

    //////Batchplan starts///////////////////////////////////////////////////////

    //function to fetching all students attendance
    public function studentAttendance(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }

        $trainerId = Auth::user()->id;
        $attendanceYear = $request->query('AttendanceYear', Carbon::now()->year);
        $attendanceMonth = $request->query('AttendanceMonth', Carbon::now()->format('m'));
        $formattedMonthYear = Carbon::createFromFormat('Y-m', "{$attendanceYear}-{$attendanceMonth}")->format('Y - F');
        $daysInMonth = range(1, Carbon::parse($formattedMonthYear)->daysInMonth);

        $attendanceData = $this->getStudentAttendanceData($trainerId, $attendanceYear, $attendanceMonth);


        $data = [
            'studAttendance' => $attendanceData,
            'AttendanceYear' => CommonController::generateYearRange(date('Y') - 10),
            'AttendanceMonth' => CommonController::generateMonthOptions(),
            'currentYear' => $attendanceYear,
            'currentMonth' => $attendanceMonth,
            'formattedMonthYear' => $formattedMonthYear,
            'daysInMonth' => $daysInMonth,
        ];
        return view('batchplan_attendance', $data);
    }

    private function getStudentAttendanceData($trainerId, $attendanceYear, $attendanceMonth)
    {
        $studAttendance = StudentTrainer::join('ev_attendence', 'ev_student_trainer.student_id', '=', 'ev_attendence.student_id')
            ->join('users', 'ev_student_trainer.student_id', '=', 'users.id')
            ->where('ev_student_trainer.trainer_id', $trainerId)
            ->whereYear('ev_attendence.date', $attendanceYear)
            ->whereMonth('ev_attendence.date', $attendanceMonth)
            ->get(['users.name as student_name', 'ev_attendence.attendance_status', 'ev_attendence.date', 'ev_student_trainer.student_id'])
            ->groupBy('student_id');

        return $studAttendance->map(function ($attendances) {
            return $attendances->mapWithKeys(function ($attendance) {
                $day = Carbon::parse($attendance->date)->day;
                return [$day => $attendance->attendance_status];
            });
        });
    }


    ///function for viewing full attendance of a student
    public function studentFullAttendance($studentId)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }

        $attendanceRecords = StudentTrainer::join('ev_attendence', 'ev_student_trainer.student_id', '=', 'ev_attendence.student_id')
            ->join('ev_student_day', 'ev_student_trainer.student_id', '=', 'ev_student_day.student_id')
            ->join('ev_student_package', 'ev_student_trainer.student_id', '=', 'ev_student_package.student_id')
            ->join('ev_package', 'ev_package.pac_id', '=', 'ev_student_package.package_id')
            ->where('ev_student_trainer.student_id', $studentId)
            ->orderBy('ev_attendence.date', 'DESC')
            ->get([
                'ev_attendence.attendance_status',
                'ev_attendence.date',
                'ev_student_trainer.student_id',
                'ev_package.pac_name as package',
                'ev_student_day.day as assigned_days',
                'ev_student_package.due_date_first as start_date',
                'ev_student_package.enddate as end_date',
            ]);

        $groupedAttendance = $this->groupAttendanceByMonth($attendanceRecords);
        $firstRecordDate = Carbon::parse($attendanceRecords->first()->date);
        $daysInMonth = range(1, $firstRecordDate->daysInMonth);
        $currentYear = $firstRecordDate->year;
        $currentMonth = $firstRecordDate->month;

        $studentName = User::find($studentId)->name ?? 'Unknown Student';
        $attendanceData = $this->calculateAttendance($attendanceRecords);

        $data = [
            'groupedAttendance' => $groupedAttendance,
            'studentName' => $studentName,
            'studentId' => $studentId,
            'daysInMonth' => $daysInMonth,
            'currentYear' => $currentYear,
            'currentMonth' => $currentMonth,
            'totalPresent' => $attendanceData['totalPresent'],
            'totalAbsent' => $attendanceData['totalAbsent'],
            'package' => $attendanceRecords->first()->package,
            'attendancePercentage' => $attendanceData['attendancePercentage'],
        ];

        return view('batchplan_fullattendance', $data);
    }

    private function groupAttendanceByMonth($attendanceRecords)
    {
        return $attendanceRecords->groupBy(function ($record) {
            return Carbon::parse($record->date)->format('Y-m');
        })->map(function ($attendances) {
            return $attendances->mapWithKeys(function ($attendance) {
                $day = Carbon::parse($attendance->date)->day;
                return [$day => $attendance->attendance_status];
            });
        });
    }

    private function calculateAttendance($attendanceRecords)
    {
        $startDate = Carbon::parse($attendanceRecords->first()->start_date);
        $endDate = $this->calculateEndDate($attendanceRecords->first()->package, $attendanceRecords->first()->start_date);
        $currentDate = Carbon::now()->endOfDay();
        $endCalculationDate = $currentDate->lessThanOrEqualTo($endDate) ? $currentDate : $endDate;

        $assignedDays = array_map('trim', $attendanceRecords->pluck('assigned_days')->unique()->toArray());

        $totalDays = $this->calculateTotalDays($startDate, $endCalculationDate, $assignedDays);
        $attendanceData = Attendance::where('student_id', $attendanceRecords->first()->student_id)
            ->whereBetween('date', [$startDate, $endCalculationDate])
            ->get();

        $extraDays = $this->calculateExtraDays($attendanceData, $assignedDays);
        $totalPresent = $attendanceData->where('attendance_status', 1)->count();
        $totalAbsent = $attendanceData->where('attendance_status', 0)->count();
        $totalClasses = $totalDays + $extraDays;

        $attendancePercentage = $totalClasses > 0 ? round(($totalPresent / $totalClasses) * 100, 2) : 0;

        return [
            'totalPresent' => $totalPresent,
            'totalAbsent' => $totalAbsent,
            'attendancePercentage' => $attendancePercentage,
        ];
    }

    private function calculateTotalDays($startDate, $endCalculationDate, $assignedDays)
    {
        $totalDays = 0;
        $dateIterator = clone $startDate;

        while ($dateIterator <= $endCalculationDate) {
            if (in_array($dateIterator->format('l'), $assignedDays)) {
                $totalDays++;
            }
            $dateIterator->addDay();
        }
        return $totalDays;
    }
    private function calculateExtraDays($attendanceData, $assignedDays)
    {
        $extraDays = 0;

        foreach ($attendanceData as $attendance) {
            $attendanceDayName = Carbon::parse($attendance->date)->format('l');
            if (!in_array($attendanceDayName, $assignedDays) && $attendance->attendance_status == 1) {
                $extraDays++;
            }
        }
        return $extraDays;
    }
    private function calculateEndDate($pac_name, $startdate)
    {
        $pac_duration = Package::where('pac_name', $pac_name)->value('duration');
        $startdate = Carbon::parse(trim($startdate));

        if ($pac_duration) {
            $endDate = $startdate->addMonths($pac_duration);
            return $endDate->format('Y-m-d H:i:s.u');
        }
        return null;
    }

    // public function batchplanAllStudentsList()
    // {
    //     if (!Auth::check()) {
    //         return redirect('/');
    //     }
    //     if (!in_array(Auth::user()->role, [1, 3])) {
    //         return redirect('/accessdenied');
    //     }

    //     $StudentData = StudentTrainer::join('ev_student_day', 'ev_student_trainer.student_id', '=', 'ev_student_day.student_id')
    //         ->join('ev_student_package', 'ev_student_trainer.student_id', '=', 'ev_student_package.student_id')
    //         ->join('ev_package', 'ev_student_package.package_id ', '=', 'ev_package.pac_id')
    //         ->join('users', 'ev_student_trainer.student_id', '=', 'users.id')
    //         ->where('ev_student_trainer.trainer_id', Auth::user()->id)
    //         ->groupBy(
    //             'ev_student_trainer.student_id',
    //             'users.name',
    //             'ev_package.pac_name',
    //             'ev_student_package.due_date_first',
    //             'ev_student_package.enddate'
    //         )
    //         ->get([
    //             'users.name as student_name',
    //             'ev_student_trainer.student_id',
    //             'ev_package.pac_name as package',
    //             DB::raw("GROUP_CONCAT(DISTINCT ev_student_day.day SEPARATOR ', ') as assigned_days"),
    //             DB::raw("DATE_FORMAT(ev_student_package.due_date_first, '%d-%m-%Y') as start_date"),
    //             DB::raw("DATE_FORMAT(ev_student_package.enddate, '%d-%m-%Y') as end_date"),
    //         ]);

    //     $data = [
    //         'StudentData' => $StudentData
    //     ];

    //     return view('batchplan_allstudents', $data);
    // }



    public function batchplanUpdate($studentpackageid)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }

        $StudentPackageDetails =  $this->getStudentPackageDetails($studentpackageid);
        $package_id = $StudentPackageDetails->package_id;
        $student_id = $StudentPackageDetails->student_id;
        $studentClassDetails = $this->getstudentClassDetails($studentpackageid)->first();
        $packageSyllabus = $this->getPackageSyllabus($package_id);
        $evaluationResult = $this->getEvaluationResult($student_id);

        $data = [
            'studentpackageid' => $studentpackageid,
            'start_date' => $StudentPackageDetails->startdate1,
            'end_date' => $StudentPackageDetails->enddate1,
            'student_id' => $StudentPackageDetails->student_id,
            'student_name' => $StudentPackageDetails->student_name,
            'package_id' => $package_id,
            'package_name' => $StudentPackageDetails->package_name,
            'assigned_days' => $studentClassDetails->assigned_days,
            'evaluationResult' => $evaluationResult,
            'syllabus' => $packageSyllabus,

        ];

        return view('batchplan_update', $data);
    }

    private function getStudentPackageDetails($studentpackageid)
    {
        return StudentPackage::join('ev_package', 'ev_student_package.package_id', '=', 'ev_package.pac_id')
            ->join('users', 'users.id', '=', 'ev_student_package.student_id')
            ->where('ev_student_package.id', '=', $studentpackageid)
            ->select('users.name as student_name', 'ev_package.pac_name as package_name', 'ev_student_package.student_id', 'ev_student_package.package_id', 'ev_student_package.due_date_first as startdate1', 'ev_student_package.enddate as enddate1')
            ->first();
    }

    private function getStudentClassDetails($studentpackageid)
    {
        return Studentpackage::join('ev_student_day', 'ev_student_day.studentpackageid', '=', 'ev_student_package.id')
            ->where('ev_student_package.id', $studentpackageid)
            ->get([
                DB::raw("GROUP_CONCAT(DISTINCT ev_student_day.day SEPARATOR ', ') as assigned_days")
            ]);
    }

    private function getPackageSyllabus($package_id)
    {
        $topics = Packagesyllabus::join('ev_topics', 'ev_packagesyllabus.topic_id', '=', 'ev_topics.id')
            ->where('ev_packagesyllabus.pac_id', '=', $package_id)
            ->distinct()
            ->orderBy('ev_topics.chapter_number', 'asc')
            ->get(['ev_topics.id as topic_id', 'ev_topics.topics as topic', 'ev_topics.chapter_number']);

        $totalTopics = $topics->count();
        $syllabus = [];

        foreach ($topics as $index => $topic) {
            $subtopics = Packagesubtopic::leftJoin('ev_subtopics', 'ev_packagesubtopics.subtopic_id', '=', 'ev_subtopics.id')
                ->where('ev_packagesubtopics.topic_id', '=', $topic->topic_id)
                ->distinct()
                ->get(['ev_packagesubtopics.subtopic_id as subtopic_id', 'ev_subtopics.sub_topics as subtopic']);

            if ($subtopics->isNotEmpty()) {
                $syllabus[] = [
                    'chapter_number' => $topic->chapter_number,
                    'topic_id' => $topic->topic_id,
                    'topic' => $topic->topic,
                    'subtopics' => $subtopics->isNotEmpty() ? $subtopics->toArray() : [],
                ];
            }
        }
        return $syllabus;
    }
    private function getEvaluationResult($student_id)
    {
        return Evaluationresult::where('student_id', $student_id)->get();
    }

    public function saveEvaluationResults(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }

        $examData = $request->input('exam_data');
        $studentId = $request->input('student_id');

        Evaluationresult::where('student_id', $studentId)->delete();

        foreach ($examData as $exam) {
            if (!empty($exam['exam_number']) && $examData !== null) {
                Evaluationresult::create([
                    'student_id' => $studentId,
                    'exam_number' => $exam['exam_number'],
                    'date' => $exam['date'],
                    'theory' => $exam['theory'],
                    'viva' => $exam['viva'],
                ]);
            }
        }
        return redirect()->back()->with('success', 'Evaluation result updated');
    }

    public function saveBatchPlan(Request $request)
    {
        $studentId = $request->studId;
        $trainerId = $request->trainer_id;
        $topicIds = explode(',', $request->topic_ids ?? $request->topic_ids);
        $subtopicIds = explode(',', $request->subtopic_ids ?? $request->subtopic_ids);
        $startDate = $request->Bp_start_date;
        $endDate = $request->Bp_end_date;
        $comments = $request->comments;

        BatchplanStudents::where('student_id', $studentId)
            ->where('trainer_id', $trainerId)
            ->whereIn('topic_id', $topicIds)
            ->whereIn('subtopic_id', $subtopicIds)
            ->delete();

        if (!empty($topicIds) && !empty($subtopicIds)) {
            foreach ($topicIds as $topicId) {
                foreach ($subtopicIds as $subtopicId) {
                    BatchplanStudents::create([
                        'student_id' => $studentId,
                        'trainer_id' => $trainerId,
                        'topic_id' => $topicId,
                        'subtopic_id' => $subtopicId,
                        'start_date' => $startDate,
                        'end_date' => $endDate,
                        'comments' => $comments,
                    ]);
                }
            }
        }

        return redirect()->back()->with('success', 'Batchplan Updated Successfully');
    }


    public function editBatchPlan(Request $request)
    {
        if (!Auth::check()) {
            return redirect('/');
        }
        if (!in_array(Auth::user()->role, [1, 3])) {
            return redirect('/accessdenied');
        }

        $batchplan = BatchplanStudents::where('student_id', $request->student_id)
            ->where('trainer_id', $request->trainer_id)
            ->where('topic_id', $request->topic_id)
            ->where('subtopic_id', $request->subtopic_id)
            ->first();

        return response()->json([
            'batchplan' => $batchplan
        ]);
    }
}
