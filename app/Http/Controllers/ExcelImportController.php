<?php
namespace App\Http\Controllers;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportUser;
use App\Imports\ImportReference;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Employee;
use App\Models\User;
use App\Models\Company;
use App\Models\Studentpackage;
use App\Models\Tax;
use App\Models\Salespackage;
use App\Models\Salespayment;
use App\Models\Payment;
use App\Models\Studentinfo;
use PhpOffice\PhpSpreadsheet\Shared\Date as ExcelDate;
use Illuminate\Support\Facades\Log;
use App\Models\Designation;
use App\Models\Technology;
use App\Models\Qualification;
use App\Models\Source;
use App\Models\College;
use App\Models\Specialization;
use App\Models\Claimfee;
use App\Models\Collegeclaim;
use App\Models\Contacts;
use App\Models\OldPayment;
use App\Models\Oldstudent;
use App\Models\JobRegister;
use App\Models\Topic;
use App\Models\Subtopic;
use Carbon\Carbon;
use Session;
use Auth;
use DB;
class ExcelImportController extends Controller
{

    private function handleRedirectionClaim($duplicates, $digit,$college)
    {
        if (empty($duplicates) && empty($digit)) {
            Session::flash('success', 'Excel Data Imported successfully.');
            return redirect('/claimed_collegeSales/');

        } else {
            if (!empty($digit)) {
                Session::flash('digit', $digit);
            }
            if (!empty($duplicates)) {
                Session::flash('duplicates', $duplicates);
            }
           
            return redirect('/add_claimstudents/' .$college );
        }
    }

    public function uploadClaimstudents(Request $request)
    {
       
        
            $pocsId=implode(',', $request->input('pocId'));
            $college = $request->input('collegeId');
     
        $request->validate([
            'claimfile' => 'required|mimes:xlsx,xls',
        ]);
        
        $file = $request->file('claimfile');
        $rows = Excel::toCollection([], $file)->first();
        
        $datatax['tax'] = Tax::find(1)->tax;
        $i = 0;
        $duplicates = [];
        $digit = [];
        $validEntries = [];
        $skipFirstRow = true;
        $insertedId=null;
        // Process the rows
        foreach ($rows as $row) {
            if ($this->isRowBlank($row)) continue;
            if ($skipFirstRow) {
                $skipFirstRow = false;
                continue;
            }
            
            $rowData = $row->toArray();
            
            // Validate phone number
            $digitsOnly = $this->isValidPhoneNumber($rowData[1]);
            // Count email occurrences
            $emailCount = $this->countEmailOccurrences($rowData[2]);
            // Count phone occurrences
            $phoneCount = $this->countPhoneOccurrences($rowData[1]);
            
            // If both email and phone are valid and not duplicates
            if ($digitsOnly && $emailCount == 0 && $phoneCount == 0) {
                // Add to valid entries
                $validEntries[] = $rowData;
                $i++;
            } else {
                // Track duplicates and invalid phone numbers
                if ($emailCount > 0) {
                    $duplicates[] = $rowData[2];
                }
                if ($phoneCount > 0) {
                    $duplicates[] = $rowData[1];
                }
                if ($digitsOnly == 0) {
                    $digit[] = $rowData[1];
                }
            }
        }
    
        if (empty($validEntries)) {
            return $this->handleRedirectionClaim($duplicates, $digit,$college);
        }
    
        $insertedId = Collegeclaim::insertGetId([
            'college_id' => $college,
            'dept_id' => $request->department,
            'poc' => $pocsId,
            'package_id' => $request->get('packname'),
            'package_type' => '2',
            'sales_id' =>  Auth::user()->id,
            'current_datetime' => now(),
            'status' => 0
        ]);
        foreach ($validEntries as $rowData) {
            $datauser['role'] = 5;
            $datauser['phone'] = preg_replace('/\s+/', '', $rowData[1]);
            $datauser['email'] = $rowData[2];
            $datauser['password'] = Hash::make($rowData[3]);
            $datauser['password_text'] = $rowData[3]; 
            $datauser['name'] = $rowData[0];
            $datauser['status'] = 1;
            $datauser['created_at'] = now();

            $student = User::create($datauser);
            $student_id = $student->id;
            $maxval = Studentinfo::max('student_admissionid');

            Studentinfo::create([
                'student_id' => $student_id,
                'student_admissionid' => $maxval + 1,
                'sales_id' => Auth::user()->id,
                'college_id' => $college,
                'date' => date('Y-m-d'),
            ]);
            $finalamount = $request->get('finalamount');
            
            $datastudent['registration_fees'] = 0;
            $datastudent['due_date_first'] = $request->get('claimstart_date');
            $datastudent['reference_type'] = $request->get('referenece');
            $datastudent['refence_id'] = $request->get('referenece_list');
            $datastudent['enddate'] = $request->get('claimend_date');
            $datastudent['student_id'] = $student_id;
            $datastudent['package_id'] = $request->get('packname');
            $datastudent['package_type'] = 2;
            if (isset($_POST['reductioncheck'])) {
                $datastudent['reduction_check'] = 1;
                $datastudent['reduction_amount'] = $request->get('reduction_amount');
                $datastudent['reduced_fees'] = $request->get('reducedfees');
                $datastudent['tax'] = $request->get('reducedtax');
                $datastudent['totalfees_afterreduction'] = $request->get('after_reduction');
                $datastudent['ineffect_total_red'] = $request->get('ineffect_offered');
                $datastudent['referalcode'] = $this->random_string(6);
            } else {
                $datastudent['totalfees_afterreduction'] = $finalamount;
            }
            $datastudent['joining_date'] = date("Y-m-d", strtotime($request->get('oldpay_date')));
            $datastudent['sales_id'] = Auth::user()->id;
            $datastudent['claim_id'] = $insertedId;
            $datastudent['claim_status'] = 0;
           $studentpackageId = Studentpackage::insertGetId($datastudent);
            ////////////////////
            $data['student_id'] =$student_id;
            $data['package_id'] = $request->get('packname');
            $data['start_date'] =  date("Y-m-d", strtotime($request->get('claimstart_date')));
            $data['reference_type'] = $request->get('referenece');
            $data['reference_id'] = $request->get('referenece_list');
            $data['end_date'] =  date("Y-m-d", strtotime($request->get('claimend_date')));
            $data['studentpackage_id'] =$studentpackageId;
            $data['sales_id'] = Auth::user()->id;
            $data['joining_date'] = date("Y-m-d", strtotime($request->get('oldpay_date')));
            if ($data['reference_type'] == 1) {
             
                $package_fullamount = $datastudent['totalfees_afterreduction'] / 2;
                $x1 = 1 + $datatax['tax'] / 100;
                $package_amount = $package_fullamount / $x1;
                $package_tax = $package_fullamount - $package_amount;
                
                
            } else {
               
                $package_fullamount = $datastudent['totalfees_afterreduction'];
                $x1 = 1 + $datatax['tax'] / 100;
                $package_amount = $package_fullamount / $x1;
                $package_tax = $package_fullamount - $package_amount;
            }
               
                    $data['package_fullamount'] = $package_fullamount;
                     $data['package_amount'] = $package_amount;
                     $data['package_tax'] = $package_tax;
                    $data['claim_id'] = $insertedId;
             
              Claimfee::insert($data);
              
        }
    
        return $this->handleRedirectionClaim($duplicates, $digit,$college);
    }

    public function random_string($length)
    {
        $str = random_bytes($length);
        $str = base64_encode($str);
        $str = str_replace(["+", "/", "="], "", $str);
        $str = substr($str, 0, $length);
        return $str;
    }
    private function countEmailOccurrences($email)
    {
        return Contacts::where('email', $email)->count() + Oldstudent::where('email_id', $email)->count() + User::where('email', $email)->count();
    }
    private function isValidPhoneNumber($string)
    {
        return preg_match('/^[0-9]{10,13}$/', $string);
    }
    private function countPhoneOccurrences($phone)
    {
        return Contacts::where('contact1', preg_replace('/\s+/', '', $phone))
            ->orWhere('contact2', preg_replace('/\s+/', '', $phone))
            ->count() +
            Oldstudent::where('contact_no', preg_replace('/\s+/', '', $phone))->count() +
            User::where('phone', preg_replace('/\s+/', '', $phone))->count();
    }
    private function processValidData($rowData, $datatax)
    {
        // Implement your logic to process valid data
        // Example: Create user, insert into tables, etc.
    }
    private function handleRedirection($duplicates, $digit, $packageId)
    {
        if (empty($duplicates) && empty($digit)) {
            Session::flash('success', 'Excel Data Imported successfully.');
            return redirect('/students_list/' . $packageId);
        } else {
            if (!empty($digit)) {
                Session::flash('digit', $digit);
            }
            if (!empty($duplicates)) {
                Session::flash('duplicates', $duplicates);
            }
            return redirect('/add_students/' . $packageId);
        }
    }
    private function handleRedirectionregistered($duplicates, $notexist, $packageId)
    {

       
        if (empty($duplicates) && empty($notexist)) {
            Session::flash('success', 'Excel Data Imported successfully.');
            return redirect('/students_list/' . $packageId);
        } else {
            if (!empty($notexist)) {
                Session::flash('notexist', $notexist);
            }
            if (!empty($duplicates)) {
                Session::flash('duplicates', $duplicates);
            }
            return redirect('/add_students_registered/' . $packageId);
        }
    }


    private function countCompanyByEmail(string $email): int
    {
        return Company::where('email', $email)->count();
    }
    
    private function countCompanyByPhone(string $contactNo): int
    {
        return Company::where('contact_number', $contactNo)->count();
    }

    public function subtopicUpload(Request $request)
    {
        $pacId = $request->input('pac_id');
        $topicId = $request->input('topic_id');
        $request->validate([
            'csv_file' => 'required|mimes:csv,txt',
        ]);

        $file = $request->file('csv_file');
        $rows = array_map('str_getcsv', file($file));
        $skipFirstRow = true;
        $duplicates = [];

        foreach ($rows as $row) {
            if ($skipFirstRow) {
                $skipFirstRow = false;
                continue;
            }

            if (empty(array_filter($row))) {
                continue; 
            }

            $rowData = array_map('trim', $row);
            $subtopic = $rowData[0] ?? ''; 

            if (!empty($subtopic)) {
                $this->processSubtopic($topicId, $subtopic, $duplicates);
            }
        }

        return $this->redirectWithSubtopics($duplicates, $pacId);
    }

    private function redirectWithSubtopics($duplicates, $pacId)
    {
        if (empty($duplicates)) {
            Session::flash('success', 'Subtopics Imported successfully.');
            return redirect('/topicListing/' . $pacId);
        } else {
            Session::flash('duplicates', $duplicates);
            return redirect('/topicListing/' . $pacId);
        }
    }


    public function syllabusUpload(Request $request)
    {
        $request->validate([
            'csv_file' => 'required|mimes:csv,txt',
        ]);

        $pacId = $request->input('pac_id');
        $file = $request->file('csv_file');
        $rows = array_map('str_getcsv', file($file));

        $duplicates = [];
        $lastTopicId = null;
        $lastTopic = null;

        foreach ($rows as $index => $row) {
            if ($index === 0) {
                continue;
            }

            if (empty(array_filter($row))) {
                continue;
            }

            $rowData = array_map('trim', $row);
            $currentTopicId = $rowData[0] ?? null;
            $currentTopic = strtoupper($rowData[1] ?? '');
            $subtopic = $rowData[2] ?? null;

            if (!empty($currentTopicId) && !empty($currentTopic)) {
                $topicId = $this->processTopic($pacId, $currentTopicId, $currentTopic, $duplicates);
                $lastTopicId = $topicId;
                $lastTopic = $currentTopic;
            }

        }

        return $this->redirect($duplicates, $pacId);
    }






    private function processTopic($pacId, $currentTopicId, $currentTopic, &$duplicates)
    {
        $existingTopic = Topic::where([
            ['pac_id', '=', $pacId],
            ['chapter_number', '=', $currentTopicId],
            ['topics', '=', $currentTopic],
        ])->first();

        if ($existingTopic) {
            $duplicates[] = "Topic: " . $currentTopic;
            return $existingTopic->id;
        }

        $data = [
            'pac_id' => $pacId,
            'chapter_number' => $currentTopicId,
            'topics' => $currentTopic,
            'trainer_id' => Auth::user()->id,
            'created_date' => now(),
        ];

        return Topic::insertGetId($data);
    }

    private function processSubtopic($topicId, $subtopic, &$duplicates)
    {
        if (empty($topicId) || empty($subtopic)) {
            return;
        }

        $subtopic = trim($subtopic);

        $existingSubtopic = Subtopic::where([
            ['topic_id', '=', $topicId],
            ['sub_topics', '=', $subtopic],
        ])->first();

        if ($existingSubtopic) {
            $duplicates[] = "Subtopic: " . $subtopic;
        } else {
            Subtopic::insert([
                'topic_id' => $topicId,
                'sub_topics' => $subtopic,
            ]);
        }
    }

    private function redirect($duplicates, $pacId)
    {
        if (empty($duplicates)) {
            Session::flash('success', 'Excel Data Imported successfully.');
            return redirect('/topicListing/' . $pacId);
        } else {
            Session::flash('duplicates', $duplicates);
            return redirect('/topicListing/' . $pacId);
        }
    }


   

public function bulkUploadCompany(Request $request)
{
    // Validate the uploaded file
    $request->validate([
        'company_file' => 'required|mimes:xlsx,xls',
    ]);

    $file = $request->file('company_file');
    $rows = Excel::toCollection([], $file)->first();
    $duplicates = [];
    $invalidPhones = [];
    $skipFirstRow = true;

    foreach ($rows as $row) {
        // Skip blank rows
        if ($this->isRowBlank($row)) {
            continue;
        }

        // Skip the header row
        if ($skipFirstRow) {
            $skipFirstRow = false;
            continue;
        }

        $rowData = $row->toArray();
        $contactNo = preg_replace('/\s+/', '', $rowData[2]);
        $email = preg_replace('/\s+/', '', $rowData[3]);

        $emailExists = $this->countCompanyByEmail($email) > 0;
        $phoneExists = $this->countCompanyByPhone($contactNo) > 0;
        $validPhone = $this->isValidPhoneNumber($contactNo);

        if ($validPhone && !$emailExists && !$phoneExists) {

            // Insert new record
            Company::create([
                'name' => $rowData[0],
                'contact_name' => $rowData[1],
                'contact_number' => $contactNo,
                'email' => $email,
                'created_userid' => Auth::id(),
            ]);

        } else {

            // Collect duplicates and invalid phone numbers
            if ($emailExists) {
                $duplicates[] = $email;
            }
            if ($phoneExists) {
                $duplicates[] = $contactNo;
            }
            if (!$validPhone) {

               
                $invalidPhones[] = $contactNo;
            }
        }
    }

    // Flash messages and redirect based on results
    if (empty($duplicates) && empty($invalidPhones)) {

     
        Session::flash('success', 'Excel data imported successfully.');
        return redirect('/manage_company/');
    } else {

      
        if (!empty($duplicates)) {
            Session::flash('duplicates', $duplicates);
        }
        if (!empty($invalidPhones)) {
            Session::flash('invalidPhones', $invalidPhones);
        }
        return redirect('/bulk_companyupload/');
    }
}

public function import_chrysallis_registered(Request $request)
{
    // Validate the request
    $request->validate([
        'file' => 'required|mimes:xlsx,xls',
    ]);
    // Get the file and its rows
    $file = $request->file('file');
    $rows = Excel::toCollection([], $file)->first();
    // Get existing email addresses from the database
    $existingEmails = User::pluck('email')->toArray();


    // Initialize arrays for tracking duplicates and non-existent emails
    $datatax['tax'] = Tax::find(1)->tax;
    $duplicates = [];
    $notexist = [];
    $skipFirstRow = true;
    // Iterate through each row in the uploaded file
    foreach ($rows as $row) {
        // Skip blank rows
        if ($this->isRowBlank($row)) {
            continue;
        }
        // Skip the first row if necessary
        if ($skipFirstRow) {
            $skipFirstRow = false;
            continue;
        }
        // Extract data from the row
        $rowData = $row->toArray();
       
        $email = preg_replace('/\s+/', '', $rowData[0]);
      
        // Check if the email already exists in the database
        if (in_array($email, $existingEmails)) {
            $user = User::where('email', $email)->first(['id']);
            $student_id = $user->id;
            // Check if the student is already associated with the specified package
            $count = Studentpackage::where('student_id', $student_id)
                                    ->where('package_id', $request->input('packname'))
                                    ->count();
            if ($count == 0) {
                // Handle data insertion for new student package
                $data = [
                    'package_id' => $request->input('packname'),
                    'registration_fees' => 0,
                    'due_date_first' => $request->input('start_date'),
                    'reference_type' => $request->input('referenece'),
                    'refence_id' => $request->input('referenece_list'),
                    'enddate' => $request->input('end_date'),
                    'student_id' => $student_id,
                    'package_type' => 2,
                    'joining_date' => date("Y-m-d", strtotime($request->input('oldpay_date'))),
                    'sales_id' => Auth::user()->id,
                ];
                // Handle optional reduction data
                if ($request->has('reductioncheck')) {
                    $data['reduction_check'] = 1;
                    $data['reduction_amount'] = $request->input('reduction_amount');
                    $data['reduced_fees'] = $request->input('reducedfees');
                    $data['tax'] = $request->input('reducedtax');
                    $data['totalfees_afterreduction'] = $request->input('after_reduction');
                    $data['ineffect_total_red'] = $request->input('ineffect_offered');
                    $data['referalcode'] = $this->random_string(6);

                } else {
                    $data['totalfees_afterreduction'] = $request->input('finalamount');
                }
                // Insert the student package
                Studentpackage::insert($data);
                // Handle sales and reference packages
                if ($request->input('referenece') == 1) {
                    // Payment transfer to sales and reference
                    $package_fullamount = $data['totalfees_afterreduction'] / 2;
                    $x1 = 1 + $datatax['tax'] / 100;
                    $package_amount = $package_fullamount / $x1;
                    $package_tax = $package_fullamount - $package_amount;
                    // Insert sales package
                    Salespackage::insert([
                        'sales_id' => Auth::user()->id,
                        'package_type' => 2,
                        'student_id' => $student_id,
                        'package_fullamount' => $package_fullamount,
                        'package_amount' => $package_amount,
                        'package_tax' => $package_tax,
                        'package_id' => $data['package_id'],
                        'joining_date' => $data['joining_date'],
                    ]);
                    // Insert reference package
                    Salespackage::insert([
                        'sales_id' => $data['refence_id'],
                        'package_type' => 2,
                        'student_id' => $student_id,
                        'package_fullamount' => $package_fullamount,
                        'package_amount' => $package_amount,
                        'package_tax' => $package_tax,
                        'package_id' => $data['package_id'],
                        'joining_date' => $data['joining_date'],
                    ]);
                } else {
                    // Insert sales package
                    $package_fullamount = $data['totalfees_afterreduction'];
                    $x1 = 1 + $datatax['tax'] / 100;
                    $package_amount = $package_fullamount / $x1;
                    $package_tax = $package_fullamount - $package_amount;
                    Salespackage::insert([
                        'sales_id' => Auth::user()->id,
                        'package_type' => 2,
                        'student_id' => $student_id,
                        'package_fullamount' => $package_fullamount,
                        'package_amount' => $package_amount,
                        'package_tax' => $package_tax,
                        'package_id' => $data['package_id'],
                        'joining_date' => $data['joining_date'],
                    ]);
                }
               
            } else {

                $duplicates[] = $rowData[0];
               
            }
        } else {
            $notexist[] = $email;
        }
    }
    // Handle redirection based on results
    return $this->handleRedirectionregistered($duplicates, $notexist, $request->input('packname'));
}
    public function import_chrysallis(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:xlsx,xls',
        ]);
        $file = $request->file('file');
        $rows = Excel::toCollection([], $file)->first();
        $existingEmails = User::pluck('email')->toArray();
        $rowsWithExistingEmails = [];
        $datatax['tax'] = Tax::find(1)->tax;
        $i = 0;
        $duplicates = [];
        $digit = [];
        $skipFirstRow = true;
        foreach ($rows as $row) {
            if ($this->isRowBlank($row)) {
                continue;
            }
            if ($skipFirstRow) {
                $skipFirstRow = false;
                continue;
            }
            $rowData = $row->toArray();
            // Validate phone number
            $digitsOnly = $this->isValidPhoneNumber($rowData[1]);
            // Count email occurrences
            $emailCount = $this->countEmailOccurrences($rowData[2]);
            // Count phone occurrences
            $phoneCount = $this->countPhoneOccurrences($rowData[1]);
            $data['package_id'] = $request->get('packname');
            // Process data if conditions are met
            if ($digitsOnly && $emailCount == 0 && $phoneCount == 0) {
                $datauser['role'] = 5;
                $datauser['phone'] = preg_replace('/\s+/', '', $rowData[1]);
                $datauser['email'] = $rowData[2];
                $datauser['password'] = Hash::make($rowData[3]);
                $datauser['password_text'] = $rowData[3];
                $datauser['name'] = $rowData[0];
                $datauser['status'] = 1;
                $datauser['created_at'] = date('Y-m-d');
                $student = User::create($datauser);
                $student_id = $student->id;
                $maxval = Studentinfo::max('student_admissionid');
                Studentinfo::create([
                    'student_id' => $student_id,
                    'student_admissionid' => $maxval + 1,
                    'sales_id' => Auth::user()->id,
                    'college_id' => $request->get('college'),
                    'date' => date('Y-m-d'),
                ]);
                $finalamount = $request->get('finalamount');
                $data['registration_fees'] = 0;
                $data['due_date_first'] = $request->get('start_date');
                $data['reference_type'] = $request->get('referenece');
                $data['refence_id'] = $request->get('referenece_list');
                $data['enddate'] = $request->get('end_date');
                $data['student_id'] = $student_id;
                $data['package_type'] = 2;
                if (isset($_POST['reductioncheck'])) {
                    $data['reduction_check'] = 1;
                    $data['reduction_amount'] = $request->get('reduction_amount');
                    $data['reduced_fees'] = $request->get('reducedfees');
                    $data['tax'] = $request->get('reducedtax');
                    $data['totalfees_afterreduction'] = $request->get('after_reduction');
                    $data['ineffect_total_red'] = $request->get('ineffect_offered');
                    $data['referalcode'] = $this->random_string(6);
                } else {
                    $data['totalfees_afterreduction'] = $finalamount;
                }
                $data['joining_date'] = date("Y-m-d", strtotime($request->get('oldpay_date')));
                $data['sales_id'] = Auth::user()->id;
                Studentpackage::insert($data);
                // Handle sales and reference packages
                if ($data['reference_type'] == 1) {
                    // Payment transfer to sales and reference
                    $package_fullamount = $data['totalfees_afterreduction'] / 2;
                    $x1 = 1 + $datatax['tax'] / 100;
                    $package_amount = $package_fullamount / $x1;
                    $package_tax = $package_fullamount - $package_amount;
                    // Insert sales package
                    Salespackage::insert([
                        'sales_id' => Auth::user()->id,
                        'package_type' => 2,
                        'student_id' => $student_id,
                        'package_fullamount' => $package_fullamount,
                        'package_amount' => $package_amount,
                        'package_tax' => $package_tax,
                        'package_id' => $data['package_id'],
                        'joining_date' => $data['joining_date'],
                    ]);
                    // Insert reference package
                    Salespackage::insert([
                        'sales_id' => $data['refence_id'],
                        'package_type' => 2,
                        'student_id' => $student_id,
                        'package_fullamount' => $package_fullamount,
                        'package_amount' => $package_amount,
                        'package_tax' => $package_tax,
                        'package_id' => $data['package_id'],
                        'joining_date' => $data['joining_date'],
                    ]);
                } else {
                    // Insert sales package
                    $package_fullamount = $data['totalfees_afterreduction'];

                    $x1 = 1 + $datatax['tax'] / 100;
                    $package_amount = $package_fullamount / $x1;
                    $package_tax = $package_fullamount - $package_amount;
                    
                    Salespackage::insert([
                        'sales_id' => Auth::user()->id,
                        'package_type' => 2,
                        'student_id' => $student_id,
                        'package_fullamount' => $package_fullamount,
                        'package_amount' => $package_amount,
                        'package_tax' => $package_tax,
                        'package_id' => $data['package_id'],
                        'joining_date' => $data['joining_date'],
                    ]);
                }




                $i++;
            } else {
                if ($emailCount > 0) {
                    $duplicates[] = $rowData[2];
                }
                if ($phoneCount > 0) {
                    $duplicates[] = $rowData[1];
                }
                if ($digitsOnly == 0) {
                    $digit[] = $rowData[1];
                }
            }
        }
        // Handle redirection based on results
        return $this->handleRedirection($duplicates, $digit, $data['package_id']);
    }
public function bulkupload_student(Request $request)
{
    $request->validate([
        'file' => 'required|mimes:xlsx,xls',
    ]);
    $file = $request->file('file');
    $rows = Excel::toCollection([], $file)->first();
    $duplicates = [];
    $digit = [];
    $skipFirstRow = true;
    foreach ($rows as $row) {
        if ($this->isRowBlank($row)) {
            continue;
        }
        if ($skipFirstRow) {
            $skipFirstRow = false;
            continue;
        }
        $rowData = $row->toArray();
        $reg_no = preg_replace('/\s+/', '', $rowData[0]);
        $contact_no = preg_replace('/\s+/', '', $rowData[3]);
        $email = preg_replace('/\s+/', '', $rowData[4]);
        $emailCount = $this->countEmailOccurrences($email);
        $phoneCount = $this->countPhoneOccurrences($contact_no);
        $regCount = Oldstudent::where('reg_no', $reg_no)->count();
        $digitsOnly = $this->isValidPhoneNumber(preg_replace('/\s+/', '', $contact_no));
        if ($digitsOnly && $emailCount == 0 && $phoneCount == 0 && $regCount == 0) {
            // Insert new record
        $data = [
            'reg_no' => $reg_no,
            'date_joining' => ExcelDate::excelToDateTimeObject($rowData[1])->format('Y-m-d'),
            'name' => $rowData[2],
            'contact_no' => $contact_no,
            'email_id' => $email,
            'technology' => $rowData[6],
            'package' => $rowData[7],
            'category' => $rowData[8],
            'source' => $rowData[9],
            'closed_by' => $rowData[10],
            'sales_id' => Auth::user()->id,
            'password' => Hash::make($rowData[5]),
            'password_text' => $rowData[5],
            'totalfees' => $rowData[11],
            'fees' => $rowData[12],
            'tax' => $rowData[13],
            'created_at' => now()->format('Y-m-d'),
        ];
        $oldstudent = Oldstudent::insertGetId($data);
        $paymentData = [
            'oldstudent_id' => $oldstudent,
            'payment_type' => 'Cash',
            'date' => now()->format('Y-m-d'),
            'sales_id' => Auth::user()->id,
            'paid_amount' => $rowData[16],
            'fees_income' => $rowData[14],
            'tax' => $rowData[15],
            'pending_amount' => $rowData[11] - $rowData[16],
        ];
        OldPayment::insert($paymentData);
        }
        else{
            if ($emailCount > 0) {
                    $duplicates[] = $rowData[4];
                }
                if ($phoneCount > 0) {
                    $duplicates[] = $rowData[3];
                }
                if ($regCount > 0) {
                    $duplicates[] = $rowData[0];
                }
                if ($digitsOnly == 0) {
                    $digit[] = $rowData[3];
                }
        }
    }
    // Flash messages and redirect based on results
    if (empty($duplicates) && empty($digit)) {
        Session::flash('success', 'Excel Data Imported successfully.');
    } else {
        if (!empty($duplicates)) {
            Session::flash('duplicates', $duplicates);
        }
        if (!empty($digit)) {
            Session::flash('digit', $digit);
        }
    }
    return redirect('/manage_oldstudent/');
}
    public function import(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:xlsx,xls',
        ]);
        $file = $request->file('file');
        (new ImportUser())->import($file);
        return back()->with('success', 'Excel Data Imported successfully.');
    }



    
    public function bulkupload_contact(Request $request)
{
    // Validate the uploaded file
    $request->validate([
        'file' => 'required|mimes:xlsx,xls',
    ]);
    // Get the uploaded file
    $file = $request->file('file');
    // Read the Excel file into a collection
    $rows = Excel::toCollection([], $file)->first();
    // Initialize arrays for storing duplicates and invalid digits
    $duplicates = [];
    $digit = [];

    $expectedHeaders = ['name', 'Contact'];
    foreach ($rows as $index => $row) {
        if ($index === 0) {
            $headers = array_map('trim', $row->toArray());
            if ($headers !== $expectedHeaders) {
                return redirect()->back()->withErrors([
                    'file' => 'Invalid format. Expected columns: ' . implode(', ', $expectedHeaders),
                ]);
            }
            continue;
        }
    }
    if ($rows->count() <= 1) {
        return redirect()->back()->withErrors([
            'file' => 'This file has no data.',
        ]);
    }

    // Flag to skip the first row (assumed header)
    $skipFirstRow = true;
    // Process each row of the Excel file
    foreach ($rows as $row) {
        // Convert the row to an array
        $rowData = $row->toArray();
        // Skip processing if the phone number field is not set
        if (!isset($rowData[1])) {
            continue;
        }
        // Skip the first row if it's a header
        if ($skipFirstRow) {
            $skipFirstRow = false;
            continue;
        }
        // Count occurrences of the phone number in the database
        $phoneCount = $this->countPhoneOccurrences($rowData[1]);
        // Check if the phone number is valid (digits only)
        $digitsOnly = $this->isValidPhoneNumber(preg_replace('/\s+/', '', $rowData[1]));
        // Check conditions for inserting into database
        if ($phoneCount == 0 && $digitsOnly == 1) {
            // Prepare data for insertion
            $data = [
                'name' => $rowData[0],
                'contact1' => preg_replace('/\s+/', '', $rowData[1]),
                'source' => $request->get("source"),
                'added_user' => Auth::user()->id,
                'contacts_assigned' => Auth::user()->id,
                'added_date' => date('Y-m-d'),
            ];
            // Insert data into 'Contacts' table
            Contacts::insert($data);
        } elseif ($phoneCount > 0) {
            // Store duplicates
            $duplicates[] = $rowData[1];
        } elseif ($digitsOnly == 0) {
            // Store invalid phone numbers
            $digit[] = $rowData[1];
        }
    }
    // Flash success message and redirect if no duplicates and all digits are valid
    if (empty($duplicates) && empty($digit)) {
        Session::flash('success', 'Excel Data Imported successfully.');
        return redirect('/outboundcalls/');
    } else {
        // Flash messages for duplicates and invalid digits
        if (!empty($duplicates)) {
            Session::flash('duplicates', $duplicates);
        }
        if (!empty($digit)) {
            Session::flash('digit', $digit);
        }
        return redirect('/upload_contacts/');
    }
}
    public function import_reference(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:xlsx,xls',
        ]);
        $file = $request->file('file');
        (new ImportReference())->import($file);
        return back()->with('success', 'Excel Data Imported successfully.');
    }
    private function isRowBlank($row)
    {
        // Check if all cells in the row are empty
        return empty(array_filter($row->toArray()));
    }
    public function bulkupload_designation(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:xlsx,xls',
        ]);
        $file = $request->file('file');
        $rows = Excel::toCollection([], $file)->first();
        $uniqueRows = collect([]);
        $duplicates = [];
        $i = 0;
        $skipFirstRow = true;
        foreach ($rows as $row) {
            if ($this->isRowBlank($row)) {
                continue;
            } else {
                if ($skipFirstRow) {
                    $skipFirstRow = false;
                    continue;
                }
                $rowData = $row->toArray();
                $existingItem = Designation::where('designation', strtoupper($rowData[0]))->first();
                if (!$existingItem) {
                    $data["designation"] = strtoupper($rowData[0]);
                    Designation::insert($data);
                } else {
                    $duplicates[] = $rowData[0];
                }
            }
        }
        if (empty($duplicates)) {
            Session::flash('success', 'Excel Data Imported successfully.');
        } else {
            Session::flash('duplicates', $duplicates);
        }
        return redirect('/manage_designation/');
    }
    public function bulkupload_technology(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:xlsx,xls',
        ]);
        $file = $request->file('file');
        $rows = Excel::toCollection([], $file)->first();
        $uniqueRows = collect([]);
        $duplicates = [];
        $i = 0;
        $skipFirstRow = true;
        foreach ($rows as $row) {
            if ($this->isRowBlank($row)) {
                continue;
            } else {
                if ($skipFirstRow) {
                    $skipFirstRow = false;
                    continue;
                }
                $rowData = $row->toArray();
                $existingItem = Technology::where('technology', strtoupper($rowData[0]))->first();
                if (!$existingItem) {
                    $data["technology"] = strtoupper($rowData[0]);
                    Technology::insert($data);
                } else {
                    $duplicates[] = $rowData[0];
                }
            }
        }
        if (empty($duplicates)) {
            Session::flash('success', 'Excel Data Imported successfully.');
        } else {
            Session::flash('duplicates', $duplicates);
        }
        return redirect('/manage_technology/');
    }
    public function bulkupload_source(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:xlsx,xls',
        ]);
        $file = $request->file('file');
        $rows = Excel::toCollection([], $file)->first();
        $uniqueRows = collect([]);
        $duplicates = [];
        $i = 0;
        $skipFirstRow = true;
        foreach ($rows as $row) {
            if ($this->isRowBlank($row)) {
                continue;
            } else {
                if ($skipFirstRow) {
                    $skipFirstRow = false;
                    continue;
                }
                $rowData = $row->toArray();
                $existingItem = Source::where('source', strtoupper($rowData[0]))->first();
                if (!$existingItem) {
                    $data["source"] = strtoupper($rowData[0]);
                    Source::insert($data);
                } else {
                    $duplicates[] = $rowData[0];
                }
            }
        }
        if (empty($duplicates)) {
            Session::flash('success', 'Excel Data Imported successfully.');
        } else {
            Session::flash('duplicates', $duplicates);
        }
        return redirect('/manage_source/');
    }
    public function bulkupload_college(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:xlsx,xls',
        ]);
        $file = $request->file('file');
        $rows = Excel::toCollection([], $file)->first();
        $uniqueRows = collect([]);
        $duplicates = [];
        $i = 0;
        $skipFirstRow = true;
        foreach ($rows as $row) {
            if ($this->isRowBlank($row)) {
                continue;
            } else {
                if ($skipFirstRow) {
                    $skipFirstRow = false;
                    continue;
                }
                $rowData = $row->toArray();
                $existingItem = College::where('college', strtoupper($rowData[0]))->first();
                if (!$existingItem) {
                    $data["college"] = strtoupper($rowData[0]);
                    College::insert($data);
                } else {
                    $duplicates[] = $rowData[0];
                }
            }
        }
        if (empty($duplicates)) {
            Session::flash('success', 'Excel Data Imported successfully.');
        } else {
            Session::flash('duplicates', $duplicates);
        }
        return redirect('/manage_college/');
    }
    public function bulkupload_qualification(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:xlsx,xls',
        ]);
        $file = $request->file('file');
        $rows = Excel::toCollection([], $file)->first();
        $uniqueRows = collect([]);
        $duplicates = [];
        $i = 0;
        $skipFirstRow = true;
        foreach ($rows as $row) {
            if ($this->isRowBlank($row)) {
                continue;
            } else {
                if ($skipFirstRow) {
                    $skipFirstRow = false;
                    continue;
                }
                $rowData = $row->toArray();
                $existingItem = Qualification::where('qualification', strtoupper($rowData[0]))->first();
                if (!$existingItem) {
                    $data["qualification"] = strtoupper($rowData[0]);
                    Qualification::insert($data);
                } else {
                    $duplicates[] = $rowData[0];
                }
            }
        }
        if (empty($duplicates)) {
            Session::flash('success', 'Excel Data Imported successfully.');
        } else {
            Session::flash('duplicates', $duplicates);
        }
        return redirect('/manage_qualification/');
    }
    public function bulkupload_specialization(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:xlsx,xls',
        ]);
        $file = $request->file('file');
        $rows = Excel::toCollection([], $file)->first();
        $uniqueRows = collect([]);
        $duplicates = [];
        $i = 0;
        $skipFirstRow = true;
        foreach ($rows as $row) {
            if ($this->isRowBlank($row)) {
                continue;
            } else {
                if ($skipFirstRow) {
                    $skipFirstRow = false;
                    continue;
                }
                $rowData = $row->toArray();
                $existingItem = Specialization::where('specialization', strtoupper($rowData[0]))->first();
                if (!$existingItem) {
                    $data["specialization"] = strtoupper($rowData[0]);
                    Specialization::insert($data);
                } else {
                    $duplicates[] = $rowData[0];
                }
            }
        }
        if (empty($duplicates)) {
            Session::flash('success', 'Excel Data Imported successfully.');
        } else {
            Session::flash('duplicates', $duplicates);
        }
        return redirect('/manage_specialization/');
    }
    public function bulkupload_employee(Request $request)
    {
        $request->validate([
            'excel_file' => 'required|mimes:xlsx,xls',
        ]);
        $file = $request->file('excel_file');
        $rows = Excel::toCollection([], $file)->first();
        $existingEmails = User::pluck('email')->toArray();
        $rowsWithExistingEmails = [];
        $skipFirstRow = true;
        $data3 = [];
        if ($request->get("technology") == "") {
            $technology = 0;
        } else {
            $technology = $request->technology;
        }
        foreach ($rows as $row) {
            if ($skipFirstRow) {
                $skipFirstRow = false;
                continue;
            }
            $rowData = $row->toArray();
            $data['email'] = $rowData[2];
            $data['empid'] = $rowData[0];
            $data['poc_code'] = $rowData[6];
            $emailcount = User::where('email', $data['email'])->count();
            $poccount = User::where('poc_code', $data['poc_code'])->count();
            $empcount = User::where('empid', $data['empid'])->count();
            if (isset($rowData[2])) {
                $emails[] = $rowData[2];
            }
            if (isset($rowData[6])) {
                $poccode[] = $rowData[6];
            }
            if (isset($rowData[0])) {
                $empid[] = $rowData[0];
            }
            $existingEmails = User::whereIn('email', $emails)->pluck('email');
            $existingPoccode = User::whereIn('poc_code', $poccode)->pluck('poc_code');
            $existingempid = User::whereIn('empid', $empid)->pluck('empid');
            if ($emailcount == 0 && $poccount == 0 && $empcount == 0) {
                $data['designation'] = $request->designation;
                $data['technology'] = $technology;
                $data['experience'] = $rowData[5];
                $data['phone'] = $rowData[3];
                $data['name'] = $rowData[1];
                $data['email'] = $rowData[2];
                $data['office_email'] = $rowData[4];
                $data['role'] = $request->emptype;
                //  $data['reported_to'] = $reported_to;
                $data['password'] = Hash::make($rowData[7]);
                $data['password_text'] = $rowData[7];
                $insertid = User::insertGetId($data);
                $reportids = $request->get("reported_to");
                if (!empty($reportids)) {
                    foreach ($reportids as $reportids) {
                        $datareporting['reported_to'] = $reportids;
                        $datareporting['employee_id'] = $insertid;
                        Reporting::insert($datareporting);
                    }
                }
            }
        }
        if (!empty($existingEmails) && !empty($existingPoccode) && !empty($existingempid)) {
            return redirect()
                ->back()
                ->withErrors(compact($existingEmails, $existingPoccode, $existingempid));
        } else {
            Session::flash('success', 'Excel Data Imported successfully.');
            return redirect('/manage_sales/');
        }
    }



    protected function areRowsAfterFirstEmpty($rows)
    {
        // Skip the first row (header) and check if all subsequent rows are empty
        foreach ($rows->slice(1) as $row) {
            if (!$this->isRowBlank($row)) {
                return false; // Found a non-empty row after the first row
            }
        }

        // All rows after the first are blank
        return true;
    }

 private function countEmailCandidate($email)
    {
        return JobRegister::where('email', $email)->count();
    }
    private function countPhoneCandidate($contact_no)
    {
        return JobRegister::where('contact_pri', $contact_no)->count();

    }

    public function bulkUploadCandidate(Request $request)
    {
        // Validate the incoming request
        $request->validate([
            'candidate_file' => 'required|mimes:xlsx,xls',
        ]);
    
        // Retrieve and process the uploaded file
        $file = $request->file('candidate_file');
        $rows = Excel::toCollection([], $file)->first();
    
        if ($rows === null || $this->areRowsAfterFirstEmpty($rows)) {
            return redirect()->back()->withErrors(['candidate_file' => 'The uploaded Excel file is empty. Please upload a file with data.']);
        }
    
        $duplicates = [];
        $invalidPhones = [];
        $skipFirstRow = true;
    
        foreach ($rows as $row) {
            if ($this->isRowBlank($row)) {
                continue;
            }
            
            if ($skipFirstRow) {
                $skipFirstRow = false;
                continue;
            }
    
            $rowData = $row->toArray();
            $contactNo = preg_replace('/\s+/', '', $rowData[2]);
            $email = preg_replace('/\s+/', '', $rowData[1]);
    
            $emailCount = $this->countEmailCandidate($email);
            $phoneCount = $this->countPhoneCandidate($contactNo);
            $isValidPhone = $this->isValidPhoneNumber($contactNo);
    
            if ($isValidPhone && $emailCount === 0 && $phoneCount === 0) {
                $userId = $this->getOrCreateUser($rowData, $email);
                $this->insertJobRegisterData($rowData, $contactNo, $userId);
            } else {
                if ($emailCount > 0) {
                    $duplicates[] = $email;
                }
                if ($phoneCount > 0) {
                    $duplicates[] = $contactNo;
                }
                if (!$isValidPhone) {
                    $invalidPhones[] = $contactNo;
                }
            }
        }
    
        return $this->handleRedirects($duplicates, $invalidPhones);
    }

    private function getOrCreateUser(array $rowData, string $email)
{
    $user = User::where('email', $email)->first();

    if (!$user) {
        return DB::transaction(function () use ($rowData, $email) {
            $user = User::create([
                'name' => $rowData[0],
                'email' => $email,
                'role' => 11,
                'password' => Hash::make('Srishti@123'),
                'password_text' => 'Srishti@123',
            ]);
            return $user->id;
        });
    }

    return $user->id;
}

private function insertJobRegisterData(array $rowData, string $contactNo, int $userId)
{
    $data = [
        'name' => $rowData[0],
        'email' => preg_replace('/\s+/', '', $rowData[1]),
        'user_id' => $userId,
        'contact_pri' => $contactNo,
        'address' => $rowData[3],
        'position_applying' => $rowData[4],
        'tech_skills' => $rowData[5],
        'edu_qualification' => $rowData[6],
        'yearof_passout' => $rowData[7],
        'exp_years' => $rowData[8],
        'curr_designation' => $rowData[9],
        'curr_organisation' => $rowData[10],
        'exp_salary' => $rowData[11],
        'ifcarrier_break' => $rowData[12],
        'resume_upload' => $rowData[13],
        'recruiter_id' => Auth::user()->id,
        'app' => 0,
    ];

    JobRegister::insert($data);
}
private function handleRedirects(array $duplicates, array $invalidPhones)
{
    if (empty($duplicates) && empty($invalidPhones)) {
        Session::flash('success', 'Excel Data Imported successfully.');
        return redirect('/manage_candidate/');
    }

    if (!empty($duplicates)) {
        Session::flash('duplicates', $duplicates);
    }
    if (!empty($invalidPhones)) {
        Session::flash('invalidPhones', $invalidPhones);
    }

    return redirect('/bulk_candidateupload/');
}

}
