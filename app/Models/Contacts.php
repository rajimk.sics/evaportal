<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $table = 'ev_st_contacts';
    public $timestamps = true;

    protected $fillable = ["id","technology","status",      
    ];

    
}
