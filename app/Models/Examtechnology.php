<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class  Examtechnology extends Model
{
    protected $table = 'ev_examtechnology';
    public $timestamps = true;

    protected $fillable = [
        'exam_id', 'technology', 
    ];
    
}
