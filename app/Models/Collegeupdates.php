<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Collegeupdates extends Model
{
    protected $table = 'ev_collegeupdates';
    public $timestamps = true;

    protected $fillable = ['college_id','sales_id','department','poc','comment'
      
];

    
}
