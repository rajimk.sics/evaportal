<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subtopic extends Model
{
    use HasFactory;
    protected $table = 'ev_subtopics';
    public $timestamps = true;
    protected $fillable = [ 'topics_id','subtopics' 
      
];


}
