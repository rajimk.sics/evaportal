<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Allocation extends Model
{
    protected $table = 'ev_claimallocation';
    public $timestamps = true;
    protected $guarded = [];  
    
}
