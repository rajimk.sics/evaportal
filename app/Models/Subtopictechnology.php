<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subtopictechnology extends Model
{
    use HasFactory;
    protected $table = 'ev_subtopictechnology';
    public $timestamps = true;
    protected $fillable = ['topic_id','technology','subtopic_id'
      
];


}
