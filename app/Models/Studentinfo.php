<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Studentinfo extends Model
{
    protected $table = 'ev_student_info';
    protected $fillable = ['student_id', 'student_admissionid','sales_id','college_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    
}
