<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class  Examhistory extends Model
{
    protected $table = 'ev_examhistory';
    public $timestamps = true;

    protected $fillable = [
        'examresult_id', 'question_id',  'marked_answer',  'correct_answer',  'score_status'
    ];
    
}
