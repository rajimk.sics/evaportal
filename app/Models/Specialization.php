<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Specialization extends Model
{
    protected $table = 'ev_specialization';
    public $timestamps = true;

    protected $fillable = ["id", "specialization",
      
    ];

    
}
