<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Candidatefollowup extends Model
{
    protected $table = 'ev_candidatefollowup';
    public $timestamps = true;
    protected $guarded = [];  
    
}
