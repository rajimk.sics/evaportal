<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class  Examquestion extends Model
{
    protected $table = 'ev_examquestion';
    public $timestamps = true;

    protected $fillable = [
        'exam_id', 'question_id', 
    ];
    
}
