<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Evaluationresult extends Model
{
    protected $table = 'ev_student_evaluationresult';
    public $timestamps = true;

    protected $fillable = ["exam_number","student_id","date","theory","viva"
      
    ];

    
}
