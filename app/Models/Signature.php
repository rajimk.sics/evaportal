<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Signature extends Model
{
    protected $table = 'ev_signature';
    public $timestamps = true;

    protected $fillable = ["id", "name",
      
    ];

    
}
