<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    protected $table = 'ev_source';
    public $timestamps = true;

    protected $fillable = ["id", "source",
      
    ];

    
}
