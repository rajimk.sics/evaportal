<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Paymentrequest  extends Model
{
    protected $table = 'ev_paymentrequest';
    public $timestamps = true;

}
