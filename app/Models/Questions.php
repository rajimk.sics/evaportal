<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    protected $table = 'ev_questions';
    public $timestamps = true;

    protected $fillable = ["id", "questions","optiona","optionb","optionc","optiond","answer"
      
];
    
}
