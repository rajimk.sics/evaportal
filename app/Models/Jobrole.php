<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jobrole extends Model
{
    protected $table = 'ev_jobroles';
    public $timestamps = true;
 
}
