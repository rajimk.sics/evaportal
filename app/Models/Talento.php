<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Talento extends Model
{
    protected $table = 'talento_registration';
    public $timestamps = true;

    protected $fillable = ["id","user_id","registration_type","staff_code","status","college_id" ];

    
}
