<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Topictechnology extends Model
{
    use HasFactory;
    protected $table = 'ev_topictechnology';
    public $timestamps = true;
    protected $fillable = ['chapter_number','topics'
      
];


}
