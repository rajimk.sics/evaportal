<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Topic extends Model
{
   
    protected $table = 'ev_topics';
    public $timestamps = true;
    protected $fillable = ["pac_id",'chapter_number','topics','trainer_id','created_date'];


}
