<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table = 'ev_package';
    public $timestamps = true;

    protected $fillable = ["pac_id", "pac_name","duration","fee","pac_type"
      
    ];

    
}
