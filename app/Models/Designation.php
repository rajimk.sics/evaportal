<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $table = 'ev_designation';
    public $timestamps = true;

    protected $fillable = ["id", "designation",
      
    ];

    
}
