<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Questiontechnology extends Model
{
    protected $table = 'ev_questiontechnology';
    public $timestamps = true;

    protected $fillable = [
        'question_id', 'technology', 
    ];
    
}
