<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'ev_company';
    public $timestamps = true;

    protected $fillable = ["id","name","contact_name","contact_number","email","created_userid"];

    
}
