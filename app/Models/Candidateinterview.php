<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Candidateinterview extends Model
{
    protected $table = 'ev_candidateinterview';
    public $timestamps = true;
    protected $guarded = [];  
    
}
