<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class College extends Model
{
    protected $table = 'ev_college';
    public $timestamps = true;

    protected $fillable = ["id", "college",
      
    ];

    
}
