<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'ev_employees';
    public $timestamps = true;

    protected $guarded = [];  

    
}