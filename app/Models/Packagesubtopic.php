<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Packagesubtopic extends Model
{
    protected $table = 'ev_packagesubtopics';
    public $timestamps = true;

    protected $fillable = ["pac_id", "topic_id", "subtopic_id"
      
    ];

    
}
