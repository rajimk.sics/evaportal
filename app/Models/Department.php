<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
   
   
    protected $table = 'ev_department';
    public $timestamps = true;

    protected $fillable = ["id", "department"];

    
}
