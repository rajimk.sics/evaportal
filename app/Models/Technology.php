<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Technology extends Model
{
    protected $table = 'ev_technology';
    public $timestamps = true;

    protected $fillable = ["id","technology","status",      
    ];

    
}
