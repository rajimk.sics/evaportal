<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Departmentpoc extends Model
{
    protected $table = 'poc_department';
   
    protected $fillable = ["id","department_poc","status"];

}
