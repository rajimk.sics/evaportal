<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobRegister extends Model
{
    use HasFactory;
    protected $table = 'ev_jobinformation';
    public $timestamps = true;

    protected $fillable = ["id","user_id","name","email","contact_pri","contact_sec","position_applying","edu_qualification","last_studied","yearof_passout","exp_years",
    "curr_organisation","	curr_designation" ,"carrier_break" ,"ifcarrier_break" ,"tech_skills" ,"	notice_period" ,"	nego_notice_period" ,"min_notice_period" ,"address" ,"city" ,
    "curr_salary","exp_salary","ref_last_organisation","organisation_ref_contact","ref_college","college_ref_contact","resume_upload","hear_about_us"  ];

}
