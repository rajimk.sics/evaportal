<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prerequisite extends Model
{
    protected $table = 'ev_prerequisite';
    public $timestamps = false;
    protected $fillable = ['pac_id', 'pre_req'];

}
