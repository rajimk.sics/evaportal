<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class  Examresult extends Model
{
    protected $table = 'ev_examresult';
    public $timestamps = true;

    protected $fillable = [
        'stud_id', 'exam_id', 'score' 
    ];
    
}
