<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BatchplanStudents extends Model
{
    protected $table = 'ev_batchplan_students';
    public $timestamps = true;

    protected $fillable = ["student_id","trainer_id","topic_id","subtopic_id","start_date","end_date","comments"
      
    ];

    
}
