<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $table = 'ev_exam';
    public $timestamps = false;

    protected $fillable = [
        'exam_type', 'exam_name','trainer_id' ,'percentage'
    ];
    
}
