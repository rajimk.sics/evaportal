<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Masterrequest extends Model
{
    protected $table = 'ev_masterrequests';
    public $timestamps = false;

    protected $fillable = [
        "from_date",
        "to_date",
        "supporting_documents",
        "comments",
        "college_id",
        "sales_id",
        "created_at"
    ];
}
