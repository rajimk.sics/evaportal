<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OldPayment extends Model
{
    protected $table = 'olddata_payment';
    public $timestamps = true;
 
}
