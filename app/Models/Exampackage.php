<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exampackage extends Model
{
    protected $table = 'ev_exampackage';
    public $timestamps = true;

    protected $fillable = [
        'exam_id', 'package_id', 
    ];
    
}
