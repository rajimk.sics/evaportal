<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentEvent extends Model
{
    protected $table = 'ev_student_events';

    protected $fillable = [
        'student_id','event_id','sales_id' // Add any other columns you want to be mass assignable here
    ];
 
}
