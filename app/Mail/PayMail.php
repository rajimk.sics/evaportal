<?php
  
  namespace App\Mail;

  use Illuminate\Bus\Queueable;
  use Illuminate\Mail\Mailable;
  use Illuminate\Queue\SerializesModels;
  
  class PayMail extends Mailable
  {
      use Queueable, SerializesModels;
  
      public $message;
  
      /**
       * Create a new message instance.
       *
       * @param string $message
       * @return void
       */
      public function __construct($message)
      {
          $this->message = $message;
      }
  
      /**
       * Build the message.
       *
       * @return $this
       */
      public function build()
      {
          return $this->subject('Eva Portal Payments')
                      ->view('emails.payment')
                      ->with(['data' => $this->message]); 
      }
  }