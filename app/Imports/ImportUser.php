<?php

namespace App\Imports;


use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use App\Models\Employee;

class ImportUser implements ToModel,WithHeadingRow,WithValidation
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
    
        return new Employee([
            'empid'     => $row['empid'],
            'emp_name'   => $row['name'],
            'email'      =>$row['email'],
            'phone'      =>$row['phone'],
            'status'     =>$row['active'],
        ]);
    }
    public function rules():array{
        return[
            '*.email'  =>'required|email|unique:ev_employees',
            '*.empid'  =>'required|unique:ev_employees',
            '*.phone'  =>'required|unique:ev_employees'
            
            
        ];
        
    }

    public function customValidationMessages()
    {
        return [
            '*.empid.unique' => 'The Employee Id has already been taken.',
            '*.email.unique' => 'The Email Id has already been taken.',
        ];
    }
}
