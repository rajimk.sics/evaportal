<?php

namespace App\Imports;


use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use App\Models\Othereference;

class ImportReference implements ToModel,WithHeadingRow,WithValidation
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        
        return new Othereference([
            'name'     => $row['name'],
            'relation'   => $row['relation'],
            'email'    =>$row['email'],
            'phone'      =>$row['phone'],
        ]);
    }
    public function rules():array{
        return[
          
            '*.email.unique' => 'The  Email has already been taken.',
            '*.phone.unique' => 'The Phone has already been taken.',
        ];
    }
}
