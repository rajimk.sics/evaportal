<?php

namespace App\Imports;


use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use App\Models\Questions;

class ImportQues implements ToModel,WithHeadingRow
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
           // dd($row);
            //exit;
        return new Questions([
            'questions'     => $row['questions'],
            'optiona'   => $row['optiona'],
            'optionb'      =>$row['optionb'],
            'optionc'      =>$row['optionc'],
            'optiond'     =>$row['optiond'],
            'answer'     =>$row['answer'],
        ]);
    }

    public function rules():array{
        return[
            '*.questions'  =>'required|unique:ev_questions'
        ];
    }
    
}
