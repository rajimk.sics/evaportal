<?php

namespace App\Imports;


use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use App\Models\User;

class ImportChrysallis implements ToModel,WithHeadingRow,WithValidation
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
      
        return new User([
            'name'     => $row['name'],
            'email'    =>$row['email'],
            'phone'    =>$row['phone'],
            'password' =>$row['password'],
        ]);
    }
    public function rules():array{
        return[
            '*.email'  => ['email', 'unique:users,email']
        ];
    }
}
