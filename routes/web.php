<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});
Route::post('/login', [
    'uses'          => 'Auth\AuthController@login',
    'middleware'    => 'checkstatus',
]);
Route::get('logout','App\Http\Controllers\Auth\LoginController@logout');

Route::get('checkmail','App\Http\Controllers\MailController@checkmail');



Auth::routes();
Route::post('/save_technology', [
    'uses'          => 'AdminController@save_technology',
    'middleware'    => 'checkstatus',
]);
//Nov
Route::post('/changeExamStatus', 'App\Http\Controllers\ActdeactController@changeExamStatus'); 
Route::get('departments', 'App\Http\Controllers\ListController@departments');
Route::get('/departmentPoc', 'App\Http\Controllers\ListController@departmentPoc');
Route::post('save_department', 'App\Http\Controllers\AddeditController@saveDepartment');
Route::post('edit_department', 'App\Http\Controllers\AddeditController@editDepartment');
Route::post('/departmentpocstatus', 'App\Http\Controllers\ActdeactController@departmentpocstatus');
Route::post('/add_departmentPoc', 'App\Http\Controllers\AddeditController@add_departmentPoc');
Route::post('/edit_departmentPoc', 'App\Http\Controllers\AddeditController@edit_departmentPoc');

/////// Syllabus-Mentor-06-11
Route::get('/uploadSylabus', 'App\Http\Controllers\MentorController@uploadSylabus');
Route::post('/UploadSyllabusByTechnology', 'App\Http\Controllers\MentorController@UploadSyllabusByTechnology');
Route::get('/topicListingByTechnology', 'App\Http\Controllers\MentorController@topicListingByTechnology');
Route::post('/subtopicUploadByTechnology', 'App\Http\Controllers\MentorController@subtopicUploadByTechnology');
Route::get('/viewTechnologySyllabus/{pacid}', 'App\Http\Controllers\MentorController@viewTechnologySyllabus'); 
Route::post('/fetchLinkedTechnologies', 'App\Http\Controllers\MentorController@fetchLinkedTechnologies');

Route::post('/assignSyllabus', 'App\Http\Controllers\MentorController@assignSyllabus'); 
Route::get('/viewPacSyllabus/{pacid}', 'App\Http\Controllers\MentorController@viewPacSyllabus'); 
Route::post('/changeTopicStatus', 'App\Http\Controllers\ActdeactController@changeTopicStatus'); 
Route::post('/getTopics', 'App\Http\Controllers\MentorController@getTopics')->name('getTopics'); 
Route::post('/removeTopic', 'App\Http\Controllers\MentorController@removeTopic');
Route::get('trainerStudents','App\Http\Controllers\MentorController@trainerStudents');
////// questions bank section 
Route::get('/viewQuestionUpload', 'App\Http\Controllers\ExamController@viewQuestionUpload');
Route::post('/questionsUpload', 'App\Http\Controllers\ExamController@questionsUpload'); 
Route::get('/manageQuestions','App\Http\Controllers\ExamController@manageQuestions');
Route::post('/managequestionstatus', 'App\Http\Controllers\ActdeactController@managequestionstatus'); 
Route::get('/addExamquestions/{exam_id}', 'App\Http\Controllers\ExamController@addExamquestions');

Route::get('/examDeclaration', 'App\Http\Controllers\ExamController@examDeclaration');
Route::post('/examDeclarationSubmit', 'App\Http\Controllers\ExamController@examDeclarationSubmit');
Route::post('/getPackages', 'App\Http\Controllers\ExamController@getPackages');
Route::post('/getPackagesByTechnology', 'App\Http\Controllers\ExamController@getPackagesByTechnology')->name('getPackagesByTechnology');
Route::get('/viewExam', 'App\Http\Controllers\ExamController@viewExam');
Route::get('/fetchExam', 'App\Http\Controllers\ExamController@fetchExam');

Route::get('/addExamquestions/{exam_id}', 'App\Http\Controllers\ExamController@addExamquestions');
Route::post('/saveExamquestion', 'App\Http\Controllers\ExamController@saveExamquestion'); 
Route::get('/viewAssignedquestions/{exam_id}', 'App\Http\Controllers\ExamController@viewAssignedquestions'); 
Route::post('/removeExamQuestion', 'App\Http\Controllers\ExamController@removeExamQuestion'); 
Route::post('/getQuestion/{id}', 'App\Http\Controllers\ExamController@getQuestion'); 
Route::post('/updateQuestion', 'App\Http\Controllers\ExamController@updateQuestion'); 


Route::get('all_students','App\Http\Controllers\MainController@all_students');
Route::post('fetchTrainers','App\Http\Controllers\MainController@fetchTrainers');
Route::post('assignTrainer','App\Http\Controllers\MainController@assignTrainer');


Route::get('/studentAttendance', 'App\Http\Controllers\MentorController@studentAttendance');
Route::get('/studentFullAttendance/{studentId}', 'App\Http\Controllers\MentorController@studentFullAttendance');
Route::get('/batchplanUpdate/{studentpackageid}/', 'App\Http\Controllers\MentorController@batchplanUpdate')->name('batchplanUpdate');




Route::post('/saveEvaluationResults', 'App\Http\Controllers\MentorController@saveEvaluationResults');
Route::post('/saveBatchPlan', 'App\Http\Controllers\MentorController@saveBatchPlan')->name('saveBatchPlan');
Route::post('/editBatchPlan', 'App\Http\Controllers\MentorController@editBatchPlan')->name('editBatchPlan');

Route::post('/rejectrequestappeal', 'App\Http\Controllers\ClaimController@rejectRequestAppeal');
Route::post('/approveRequestAppeal', 'App\Http\Controllers\ClaimController@approveRequestAppeal');


Route::get('/add_claimstudent/{id}', 'App\Http\Controllers\ClaimController@addClaimStudent');
Route::get('/getpackage-details/{packageId}', 'App\Http\Controllers\ClaimController@getPackageDetails');
Route::post('/college_update', 'App\Http\Controllers\ClaimController@updateColleges');
Route::get('/college_updateReport', 'App\Http\Controllers\ClaimController@collegeUpdateReport');
Route::post('/updateDetails/{updateId}', 'App\Http\Controllers\ClaimController@updateDetails');
Route::post('/get-requsettodetails', 'App\Http\Controllers\ClaimController@getRequsettodetails');
Route::post('save_masterRequest','App\Http\Controllers\ClaimController@saveMasterRequest');
Route::get('/list_requestfromsales', 'App\Http\Controllers\ClaimController@listRequestfromsales');
Route::post('/getUpdateDetails', 'App\Http\Controllers\ClaimController@getUpdateDetails');
Route::post('/approve_request', 'App\Http\Controllers\ClaimController@approveRequest');

Route::post('/college-update-history', 'App\Http\Controllers\ClaimController@getCollegeUpdateHistory');

Route::post('/request_details', 'App\Http\Controllers\ClaimController@requestDetails');
Route::post('/reject_request', 'App\Http\Controllers\ClaimController@rejectRequest');
Route::get('/claimdetails-master/{id}', 'App\Http\Controllers\ClaimController@claimdetailsMaster');
Route::get('/request_tomaster', 'App\Http\Controllers\ClaimController@requestTomaster');
Route::post('/getrequest-details', 'App\Http\Controllers\ClaimController@getrequestDetails');
Route::get('claimstudents_list/{claimid}','App\Http\Controllers\ClaimController@claimstudentsList');

Route::get('claimed_collegeSales', 'App\Http\Controllers\ClaimController@claimedCollegeSales');
Route::get('claims_collegeSales', 'App\Http\Controllers\ClaimController@claimsCollegeSales');
Route::get('claimed_collegesmaster', 'App\Http\Controllers\ClaimController@claimedCollegesmaster');
Route::get('claim_masterallocate/{id}','App\Http\Controllers\ClaimController@claimMasterAllocate');
Route::post('claimdetails-allocation', 'App\Http\Controllers\ClaimController@claimdetailsAllocation');
Route::post('claim_names','App\Http\Controllers\ClaimController@claimNames');

Route::get('/add_claimstudents/{collegeId}', 'App\Http\Controllers\ClaimController@addClaimstudents');
Route::post('upload_claimstudents', 'App\Http\Controllers\ExcelImportController@uploadClaimstudents');
Route::post('master-studentdetails', 'App\Http\Controllers\ClaimController@masterStudentdetails');

Route::post('/save-appeal', 'App\Http\Controllers\ClaimController@saveAppeal');

Route::get('/list_appealfromsales', 'App\Http\Controllers\ClaimController@listAppealfromsales');


Route::get('/add_college','App\Http\Controllers\AdminController@addCollege');
Route::post('edit_college','App\Http\Controllers\AdminController@edit_college');
Route::post('/get-districts', 'App\Http\Controllers\AdminController@getDistricts');
Route::post('/getcollege-departments', 'App\Http\Controllers\ClaimController@getCollegeDepartments');
Route::post('/collegeStatus', 'App\Http\Controllers\ActdeactController@collegeStatus');
Route::get('get_college_details/{collegeId}', 'App\Http\Controllers\AdminController@getCollegeDetails');


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('manage_designation','App\Http\Controllers\AdminController@manage_designation');
Route::get('manage_technology','App\Http\Controllers\AdminController@manage_technology');
Route::post('save_technology','App\Http\Controllers\AdminController@save_technology');
Route::post('save_designation','App\Http\Controllers\AdminController@save_designation');
Route::post('save_source','App\Http\Controllers\AdminController@save_source');
Route::post('save_college','App\Http\Controllers\AdminController@save_college');
Route::post('save_qualification','App\Http\Controllers\AdminController@save_qualification');
Route::get('add_employee','App\Http\Controllers\AdminController@add_employee');

Route::post('departmentStatus', 'App\Http\Controllers\ActdeactController@departmentStatus');
Route::post('/departmentpocStatus', 'App\Http\Controllers\ActdeactController@departmentpocStatus');

Route::get('manage_master','App\Http\Controllers\AdminController@manageMaster');




Route::get('manage_qualification','App\Http\Controllers\AdminController@manage_qualification');
Route::get('manage_college','App\Http\Controllers\AdminController@manage_college');
Route::get('manage_source','App\Http\Controllers\AdminController@manage_source');
Route::get('manage_source','App\Http\Controllers\AdminController@manage_source');


















Route::get('manage_sales','App\Http\Controllers\AdminController@manage_sales');
Route::get('manage_process','App\Http\Controllers\AdminController@manage_process');
Route::get('manage_trainers','App\Http\Controllers\AdminController@manage_trainers');
Route::post('save_employee','App\Http\Controllers\AdminController@save_employee');
Route::get('add_contacts','App\Http\Controllers\SalesController@add_contacts');
Route::post('save_contacts','App\Http\Controllers\SalesController@save_contacts');
Route::get('acessdenied','App\Http\Controllers\AdminController@acessdenied');
Route::get('outboundcalls','App\Http\Controllers\SalesController@outboundcalls');
Route::get('followup','App\Http\Controllers\SalesController@followup');
Route::get('closed_contacts','App\Http\Controllers\SalesController@closed_contacts');

Route::get('student_admission','App\Http\Controllers\StudentController@student_admission');
Route::get('activated_packages','App\Http\Controllers\StudentController@activated_packages');

Route::get('payment_details_student/{pactype}/{pacid}','App\Http\Controllers\StudentController@payment_details_student');


Route::post('delete_designation','App\Http\Controllers\AdminController@delete_designation');
Route::post('edit_designation','App\Http\Controllers\AdminController@edit_designation');
Route::post('contact_details','App\Http\Controllers\SalesController@contact_details');
Route::post('followup_history','App\Http\Controllers\SalesController@followup_history');
Route::post('/followupsave','App\Http\Controllers\SalesController@followupsave');
Route::post('/closed_contacts','App\Http\Controllers\SalesController@closed_contacts');

Route::get('/edit_employee/{id}','App\Http\Controllers\AdminController@edit_employee');

Route::post('/edit_save','App\Http\Controllers\AdminController@edit_save');
Route::post('/updatefollow','App\Http\Controllers\SalesController@updatefollow');
Route::post('/closedcontacts','App\Http\Controllers\SalesController@closedcontacts');
Route::post('/checkno','App\Http\Controllers\SalesController@checkno');
Route::get('manage_package','App\Http\Controllers\AdminController@manage_package');

Route::get('manage_package_chrysallis','App\Http\Controllers\AdminController@manage_package_chrysallis');


Route::post('save_package','App\Http\Controllers\AdminController@save_package');
Route::get('/edit_package/{id}','App\Http\Controllers\AdminController@edit_package');
Route::post('/edit_savepack','App\Http\Controllers\AdminController@edit_savepack');
Route::post('/deactivate_user','App\Http\Controllers\AdminController@deactivate_user');
Route::post('/activate_user','App\Http\Controllers\AdminController@activate_user');
Route::post('/packagedetails','App\Http\Controllers\SalesController@packagedetails');


Route::post('edit_designation','App\Http\Controllers\AdminController@edit_designation');
Route::post('dact_designation','App\Http\Controllers\AdminController@dact_designation');
Route::post('act_designation','App\Http\Controllers\AdminController@act_designation');


Route::post('edit_technology','App\Http\Controllers\AdminController@edit_technology');
Route::post('dact_technology','App\Http\Controllers\AdminController@dact_technology');
Route::post('act_technology','App\Http\Controllers\AdminController@act_technology');



Route::post('edit_source','App\Http\Controllers\AdminController@edit_source');
Route::post('dact_source','App\Http\Controllers\AdminController@dact_source');
Route::post('act_source','App\Http\Controllers\AdminController@act_source');


Route::post('dact_qualification','App\Http\Controllers\AdminController@dact_qualification');
Route::post('act_qualification','App\Http\Controllers\AdminController@act_qualification');
Route::post('edit_qualification','App\Http\Controllers\AdminController@edit_qualification');





Route::post('edit_college','App\Http\Controllers\AdminController@edit_college');
Route::get('manage_specialization','App\Http\Controllers\AdminController@manage_specialization');
Route::post('save_specialization','App\Http\Controllers\AdminController@save_specialization');
Route::post('edit_specialization','App\Http\Controllers\AdminController@edit_specialization');
Route::post('dact_specialization','App\Http\Controllers\AdminController@dact_specialization');
Route::post('act_specialization','App\Http\Controllers\AdminController@act_specialization');
Route::post('employee_details','App\Http\Controllers\AdminController@employee_details');
Route::post('deact_package','App\Http\Controllers\AdminController@deact_package');
Route::post('act_package','App\Http\Controllers\AdminController@act_package');
Route::post('/assign_package','App\Http\Controllers\SalesController@assign_package');


Route::get('select_package/{id}/{type}','App\Http\Controllers\SalesController@select_package');

Route::get('/subtopicView/{pacid}/{topicid}', 'App\Http\Controllers\AdminController@subtopicView');


Route::get('assign_regular_package/{id}','App\Http\Controllers\SalesController@assign_regular_package');

Route::post('discountdetails','App\Http\Controllers\StudentController@discountdetails');
Route::post('pack_student','App\Http\Controllers\SalesController@pack_student');
//Route::get('pdfgeneration/{id}','App\Http\Controllers\SalesController@pdfgeneration');
Route::post('emailexist','App\Http\Controllers\SalesController@emailexist');
Route::get('package_details/{id}','App\Http\Controllers\SalesController@package_details');


Route::post('save_student','App\Http\Controllers\StudentController@save_student');
Route::get('student_admission_details','App\Http\Controllers\StudentController@student_admission_details');
Route::get('manage_tax','App\Http\Controllers\AdminController@manage_tax');
Route::post('update_tax','App\Http\Controllers\AdminController@update_tax');
Route::get('add_package','App\Http\Controllers\AdminController@add_package');
Route::get('payment_history/{studentid}','App\Http\Controllers\SalesController@payment_history');


Route::get('payment_history_package/{studentpackageid}','App\Http\Controllers\SalesController@payment_history_package');





Route::get('manage_employee','App\Http\Controllers\AdminController@manage_employee');
Route::get('add_srishtians','App\Http\Controllers\AdminController@add_srishtians');
Route::post('save_srishtians','App\Http\Controllers\AdminController@save_srishtians');
Route::post('upload-excel', 'App\Http\Controllers\ExcelImportController@import')->name('upload-excel');
Route::post('activate_employee','App\Http\Controllers\AdminController@activate_employee');
Route::post('deactivate_employee','App\Http\Controllers\AdminController@deactivate_employee');
Route::get('manage_reference','App\Http\Controllers\AdminController@manage_reference');
Route::post('upload-reference', 'App\Http\Controllers\ExcelImportController@import_reference')->name('upload-reference');
Route::get('add_othereference','App\Http\Controllers\AdminController@add_othereference');
Route::post('save_othereference','App\Http\Controllers\AdminController@save_othereference');
Route::post('reference_details','App\Http\Controllers\SalesController@reference_details');
Route::post('pdfgeneration','App\Http\Controllers\SalesController@pdfgeneration');
Route::post('payment_package','App\Http\Controllers\SalesController@payment_package');
Route::post('payment_old','App\Http\Controllers\SalesController@payment_old');


Route::get('manage_talento_registration','App\Http\Controllers\SalesController@manage_talento_registration');


Route::get('select_regular_package/{id}','App\Http\Controllers\SalesController@select_regular_package');



Route::get('select_chrysalis_package/{id}/{type}','App\Http\Controllers\SalesController@select_chrysalis_package');
Route::post('assign_chrysalis_package','App\Http\Controllers\SalesController@assign_chrysalis_package');
Route::post('assign_talento_regularpackage','App\Http\Controllers\SalesController@assign_talento_regularpackage');







Route::get('test','App\Http\Controllers\SalesController@test');
Route::get('pdf_download/{paymentid}','App\Http\Controllers\SalesController@pdf_download');

Route::get('receipt_download/{paymentid}','App\Http\Controllers\SalesController@receipt_download');

Route::get('admission_monthlyreport','App\Http\Controllers\SalesController@admission_monthlyreport');


//REPORTS
Route::get('monthlyrevenuewithgst_chry/{userid}','App\Http\Controllers\ReportsController@monthlyrevenuewithgst_chry');
Route::get('monthlyrevenuewithgst_chry','App\Http\Controllers\ReportsController@monthlyrevenuewithgst_chry');
Route::get('monthlyrevenuewithoutgst_chry/{userid}','App\Http\Controllers\ReportsController@monthlyrevenuewithoutgst_chry');
Route::get('monthlyrevenuewithoutgst_chry','App\Http\Controllers\ReportsController@monthlyrevenuewithoutgst_chry');
Route::get('monthlyrevenuewithgst_regular/{userid}','App\Http\Controllers\ReportsController@monthlyrevenuewithgst_regular');
Route::get('monthlyrevenuewithgst_regular','App\Http\Controllers\ReportsController@monthlyrevenuewithgst_regular');
Route::get('monthlyrevenuewithoutgst_regular/{userid}','App\Http\Controllers\ReportsController@monthlyrevenuewithoutgst_regular');
Route::get('monthlyrevenuewithoutgst_regular','App\Http\Controllers\ReportsController@monthlyrevenuewithoutgst_regular');


Route::get('totalrevenue_withgst/{userid}','App\Http\Controllers\ReportsController@totalrevenue_withgst');
Route::get('totalrevenue_withoutgst/{userid}','App\Http\Controllers\ReportsController@totalrevenue_withoutgst');
Route::get('monthlyadmissionwithgst_chrysalis/{userid}','App\Http\Controllers\ReportsController@monthlyadmissionwithgst_chrysalis');
Route::get('monthlyadmissionwithoutgst_chrysalis/{userid}','App\Http\Controllers\ReportsController@monthlyadmissionwithoutgst_chrysalis');
Route::get('monthlyadmissionwithgst_regular/{userid}','App\Http\Controllers\ReportsController@monthlyadmissionwithgst_regular');
Route::get('monthlyadmissionwithoutgst_regular/{userid}','App\Http\Controllers\ReportsController@monthlyadmissionwithoutgst_regular');
Route::get('totaladmission_withgst/{userid}','App\Http\Controllers\ReportsController@totaladmission_withgst');
Route::get('totaladmission_withoutgst/{userid}','App\Http\Controllers\ReportsController@totaladmission_withoutgst');

Route::get('totalrevenue_withgst','App\Http\Controllers\ReportsController@totalrevenue_withgst');
Route::get('totalrevenue_withoutgst','App\Http\Controllers\ReportsController@totalrevenue_withoutgst');
Route::get('monthlyadmissionwithgst_chrysalis','App\Http\Controllers\ReportsController@monthlyadmissionwithgst_chrysalis');
Route::get('monthlyadmissionwithoutgst_chrysalis','App\Http\Controllers\ReportsController@monthlyadmissionwithoutgst_chrysalis');
Route::get('monthlyadmissionwithgst_regular','App\Http\Controllers\ReportsController@monthlyadmissionwithgst_regular');
Route::get('monthlyadmissionwithoutgst_regular','App\Http\Controllers\ReportsController@monthlyadmissionwithoutgst_regular');
Route::get('totaladmission_withgst','App\Http\Controllers\ReportsController@totaladmission_withgst');
Route::get('totaladmission_withoutgst','App\Http\Controllers\ReportsController@totaladmission_withoutgst');



//REPORTS
Route::get('rpmonthlyrevenuewithgst_chry','App\Http\Controllers\ReportsController@rpmonthlyrevenuewithgst_chry');
Route::get('rpmonthlyrevenuewithoutgst_chry','App\Http\Controllers\ReportsController@rpmonthlyrevenuewithoutgst_chry');
Route::get('rpmonthlyrevenuewithgst_regular','App\Http\Controllers\ReportsController@rpmonthlyrevenuewithgst_regular');
Route::get('rpmonthlyrevenuewithoutgst_regular','App\Http\Controllers\ReportsController@rpmonthlyrevenuewithoutgst_regular');
Route::get('rptotalrevenue_withgst','App\Http\Controllers\ReportsController@rptotalrevenue_withgst');
Route::get('rptotalrevenue_withoutgst','App\Http\Controllers\ReportsController@rptotalrevenue_withoutgst');
Route::get('rpmonthlyadmissionwithgst_chrysalis','App\Http\Controllers\ReportsController@rpmonthlyadmissionwithgst_chrysalis');
Route::get('rpmonthlyadmissionwithoutgst_chrysalis','App\Http\Controllers\ReportsController@rpmonthlyadmissionwithoutgst_chrysalis');
Route::get('rpmonthlyadmissionwithgst_regular','App\Http\Controllers\ReportsController@rpmonthlyadmissionwithgst_regular');
Route::get('rpmonthlyadmissionwithoutgst_regular','App\Http\Controllers\ReportsController@rpmonthlyadmissionwithoutgst_regular');
Route::get('rptotaladmission_withgst','App\Http\Controllers\ReportsController@rptotaladmission_withgst');
Route::get('rptotaladmission_withoutgst','App\Http\Controllers\ReportsController@rptotaladmission_withoutgst');


//SALES Reporting










Route::get('admission_monthlyreport_employees','App\Http\Controllers\SalesController@admission_monthlyreport_employees');
Route::get('chrysalis_monthlyreport','App\Http\Controllers\SalesController@chrysalis_monthlyreport');
Route::get('chrysalis_monthlyreport_employee','App\Http\Controllers\SalesController@chrysalis_monthlyreport_employee');

Route::get('admission_revenue','App\Http\Controllers\SalesController@admission_revenue');

Route::get('admission_revenue_employee','App\Http\Controllers\SalesController@admission_revenue_employee');

Route::get('chrysalis_revenue','App\Http\Controllers\SalesController@chrysalis_revenue');
Route::get('chrysalis_revenue_employee','App\Http\Controllers\SalesController@chrysalis_revenue_employee');

Route::get('sales_admission','App\Http\Controllers\SalesController@sales_admission');

Route::get('sales_admission_employee','App\Http\Controllers\SalesController@sales_admission_employee');

Route::get('sales_chrysalis','App\Http\Controllers\SalesController@sales_chrysalis');
Route::get('sales_chrysalis_employee','App\Http\Controllers\SalesController@sales_chrysalis_employee');

Route::get('admission_monthlyreport/{empid}','App\Http\Controllers\SalesController@admission_monthlyreport');
Route::get('chrysalis_monthlyreport/{empid}','App\Http\Controllers\SalesController@chrysalis_monthlyreport');
Route::get('admission_revenue/{empid}','App\Http\Controllers\SalesController@admission_revenue');


Route::get('chrysalis_revenue/{empid}','App\Http\Controllers\SalesController@chrysalis_revenue');
Route::get('sales_admission/{empid}','App\Http\Controllers\SalesController@sales_admission');

Route::get('sales_chrysalis/{empid}','App\Http\Controllers\SalesController@sales_chrysalis');
Route::get('sendemail','App\Http\Controllers\SalesController@sendemail');
Route::get('employee_sales_report/{empid}','App\Http\Controllers\AdminController@employee_sales_report');
Route::get('admission_monthlyreport','App\Http\Controllers\SalesController@admission_monthlyreport');

Route::get('regular_students','App\Http\Controllers\AdminController@regular_students');
Route::get('chrysalis_students','App\Http\Controllers\AdminController@chrysalis_students');
Route::get('add_students/{pacid}','App\Http\Controllers\SalesController@add_students');

Route::get('add_students_registered/{pacid}','App\Http\Controllers\SalesController@add_students_registered');




Route::get('students_list/{pacid}','App\Http\Controllers\SalesController@students_list');
Route::post('upload_chrysallis', 'App\Http\Controllers\ExcelImportController@import_chrysallis');

Route::post('import_chrysallis_registered', 'App\Http\Controllers\ExcelImportController@import_chrysallis_registered');


Route::get('manage_events','App\Http\Controllers\AdminController@manage_events');
Route::post('save_events','App\Http\Controllers\AdminController@save_events');
Route::post('activate_events','App\Http\Controllers\AdminController@activate_events');
Route::post('deactivate_events','App\Http\Controllers\AdminController@deactivate_events');
Route::get('manage_settings','App\Http\Controllers\AdminController@manage_settings');
Route::post('update_signature','App\Http\Controllers\AdminController@update_signature');
Route::get('add_questions','App\Http\Controllers\ExamController@add_questions');
Route::post('save_questions','App\Http\Controllers\ExamController@save_questions');
Route::get('manage_questions','App\Http\Controllers\ExamController@manage_questions');
Route::post('activate_questions','App\Http\Controllers\ExamController@activate_questions');
Route::post('deactivate_questions','App\Http\Controllers\ExamController@deactivate_questions');
Route::post('edit_question','App\Http\Controllers\ExamController@edit_question');
Route::post('update_questions','App\Http\Controllers\ExamController@update_questions');
Route::get('manage_page_contents','App\Http\Controllers\AdminController@manage_page_contents');
Route::post('save_content','App\Http\Controllers\AdminController@save_content');
Route::post('edit_content','App\Http\Controllers\AdminController@edit_content');
Route::post('update_content','App\Http\Controllers\AdminController@update_content');


Route::get('pending_gatepass','App\Http\Controllers\AdminController@pendingGatepass');
Route::get('approved_gatepass','App\Http\Controllers\AdminController@approvedGatepass');
Route::get('rejected_gatepass','App\Http\Controllers\AdminController@rejectedGatepass');
Route::get('returned_gatepass','App\Http\Controllers\AdminController@returnedGatepass');

Route::get('manage_gatepass','App\Http\Controllers\AdminController@manage_gatepass');





Route::post('reject_gatepass','App\Http\Controllers\AdminController@reject_gatepass');
Route::post('approve_gatepass','App\Http\Controllers\AdminController@approve_gatepass');
Route::post('students_list_event/{eventid}','App\Http\Controllers\AdminController@students_list_event');
Route::get('gatepass_download/{reqid}','App\Http\Controllers\SalesController@gatepass_download');
Route::get('/view_salesreport','App\Http\Controllers\ReportsController@view_salesreport');
Route::get('/bulk_employee','App\Http\Controllers\AdminController@bulk_employee');
Route::post('bulkupload_employee', 'App\Http\Controllers\ExcelImportController@bulkupload_employee');

Route::post('bulkupload_designation', 'App\Http\Controllers\ExcelImportController@bulkupload_designation');
Route::post('bulkupload_technology', 'App\Http\Controllers\ExcelImportController@bulkupload_technology');
Route::post('bulkupload_source', 'App\Http\Controllers\ExcelImportController@bulkupload_source');
Route::post('bulkupload_college', 'App\Http\Controllers\ExcelImportController@bulkupload_college');
Route::post('bulkupload_qualification', 'App\Http\Controllers\ExcelImportController@bulkupload_qualification');
Route::post('bulkupload_specialization', 'App\Http\Controllers\ExcelImportController@bulkupload_specialization');


Route::get('manage_management','App\Http\Controllers\AdminController@manage_management');
Route::get('manage_finance','App\Http\Controllers\AdminController@manage_finance');
Route::get('manage_subadmin','App\Http\Controllers\AdminController@manage_subadmin');
Route::get('manage_placement','App\Http\Controllers\AdminController@manage_placement');

Route::get('edit_contact/{contactid}','App\Http\Controllers\SalesController@edit_contact');
Route::post('update_contacts','App\Http\Controllers\SalesController@update_contacts');





Route::get('your_students','App\Http\Controllers\MainController@your_students');


Route::post('update_doj','App\Http\Controllers\MainController@update_doj');




Route::post('/checkeditno','App\Http\Controllers\SalesController@checkeditno');
Route::get('old_student','App\Http\Controllers\SalesController@old_student');
//Company Start
Route::get('manage_company','App\Http\Controllers\AdminController@manage_company');
Route::get('add_company','App\Http\Controllers\AdminController@add_company');
Route::post('/checkphone','App\Http\Controllers\AdminController@checkphone');
Route::post('/checkemail','App\Http\Controllers\AdminController@checkemail');
Route::post('/save_company','App\Http\Controllers\AdminController@save_company');
Route::post('/update_company','App\Http\Controllers\AdminController@update_company');
Route::post('/activate_company','App\Http\Controllers\AdminController@activate_company');
Route::post('/deactivate_company','App\Http\Controllers\AdminController@deactivate_company');
Route::get('edit_company/{compid}','App\Http\Controllers\AdminController@edit_company');

Route::get('/syllabusupload/{id}', 'App\Http\Controllers\TrainerController@syllabusViewTrainer');
//Company End

Route::post('changedate','App\Http\Controllers\MainController@changedate');

Route::post('pass_return','App\Http\Controllers\MainController@pass_return');

Route::get('student_gatepass','App\Http\Controllers\StudentController@student_gatepass');


Route::get('add_gatepass','App\Http\Controllers\StudentController@add_gatepass');
Route::post('save_gatepass','App\Http\Controllers\StudentController@save_gatepass');
Route::post('package_details','App\Http\Controllers\StudentController@package_details');
Route::get('upload_contacts','App\Http\Controllers\SalesController@upload_contacts');
Route::post('bulkupload_contact','App\Http\Controllers\ExcelImportController@bulkupload_contact');

Route::get('oldpayment_history/{studentpackageid}','App\Http\Controllers\SalesController@oldpayment_history');

Route::get('manage_oldstudent','App\Http\Controllers\AdminController@manage_oldstudent');



Route::post('bulkupload_student','App\Http\Controllers\ExcelImportController@bulkupload_student');
Route::post('transaction_unique','App\Http\Controllers\MainController@transaction_unique');


Route::get('pending_paymentrequest','App\Http\Controllers\AdminController@pending_paymentrequest');
Route::get('rejected_paymentrequest','App\Http\Controllers\AdminController@rejected_paymentrequest');
Route::get('approved_paymentrequest','App\Http\Controllers\AdminController@approved_paymentrequest');
Route::post('approve_payment','App\Http\Controllers\AdminController@approve_payment');
Route::post('reject_payment','App\Http\Controllers\AdminController@reject_payment');
Route::post('employeelisting','App\Http\Controllers\AdminController@employeelisting');
Route::get('add_email','App\Http\Controllers\AdminController@add_email');

Route::post('packdetails','App\Http\Controllers\SalesController@packdetails');
Route::post('paymentrequest','App\Http\Controllers\StudentController@paymentrequest');
Route::get('pending_payment','App\Http\Controllers\PaymentController@pending_payment');
Route::get('approved_payment','App\Http\Controllers\PaymentController@approved_payment');
Route::get('rejected_payment','App\Http\Controllers\PaymentController@rejected_payment');
Route::post('accountsapprove','App\Http\Controllers\PaymentController@accountsapprove');
Route::post('accountsreject','App\Http\Controllers\PaymentController@accountsreject');
Route::get('bulk_companyupload','App\Http\Controllers\AdminController@bulkCompanyUpload');
Route::post('bulkupload_company','App\Http\Controllers\ExcelImportController@bulkUploadCompany');
Route::get('add_email','App\Http\Controllers\AdminController@addEmail');
Route::post('save_emails','App\Http\Controllers\AdminController@saveEmails');
Route::get('manage_recruitment','App\Http\Controllers\AdminController@manageRecruitment');
Route::get('manage_candidate','App\Http\Controllers\RecruitmentController@manageCandidate');
Route::post('candidate_details','App\Http\Controllers\RecruitmentController@candidateDetails');

Route::get('expiring_gatepass','App\Http\Controllers\AdminController@expiringGatepass');

//RECRUITMENT
Route::get('manage_candidate','App\Http\Controllers\RecruitmentController@manageCandidate');
Route::post('candidate_details','App\Http\Controllers\RecruitmentController@candidateDetails');
Route::get('bulk_candidateupload','App\Http\Controllers\RecruitmentController@bulkCandidateUpload');
Route::post('bulkupload_candidate','App\Http\Controllers\ExcelImportController@bulkUploadCandidate');
Route::post('saveschedule','App\Http\Controllers\RecruitmentController@saveSchedule');
Route::get('candidate_followups','App\Http\Controllers\RecruitmentController@candidateFollowups');
Route::get('candidate_interview','App\Http\Controllers\RecruitmentController@candidateInterview');
Route::post('updatefollowup','App\Http\Controllers\RecruitmentController@updateFollowup');
Route::post('candidatefollowup_history','App\Http\Controllers\RecruitmentController@candidatefollowupHistory');

Route::post('savecandidateoffer','App\Http\Controllers\RecruitmentController@saveCandidateOffer');

Route::get('claim_withgst/{userid}','App\Http\Controllers\ReportsController@claim_withgst');
Route::get('claim_withgst','App\Http\Controllers\ReportsController@claim_withgst');
Route::get('claim_withoutgst/{userid}','App\Http\Controllers\ReportsController@claim_withoutgst');
Route::get('claim_withoutgst','App\Http\Controllers\ReportsController@claim_withoutgst');

Route::post('/getCollegeDepartmentsForReqForm', 'App\Http\Controllers\ClaimController@getCollegeDepartmentsForReqForm');


Route::post('savefollowup','App\Http\Controllers\RecruitmentController@saveFollowup');
Route::post('save_allocation','App\Http\Controllers\ClaimController@saveAllocation');
Route::get('change_password','App\Http\Controllers\HomeController@changePassword');
Route::post('password_match','App\Http\Controllers\HomeController@passwordMatch');
Route::post('update_password','App\Http\Controllers\HomeController@updatePassword');
Route::get('edit_profile','App\Http\Controllers\HomeController@editProfile');
Route::post('update_profile','App\Http\Controllers\HomeController@updateProfile');

Route::get('reissued_gatepass','App\Http\Controllers\AdminController@reissuedGatepass');
Route::post('savereissuegatepass','App\Http\Controllers\AdminController@saveReissueGatepass');

// trainer section
Route::get('/packagelist_trainer', 'App\Http\Controllers\TrainerController@packageTrainer');
Route::get('/packageEdit/{id}', 'App\Http\Controllers\TrainerController@packageEdit');
Route::post('/packageUpdate', 'App\Http\Controllers\TrainerController@packageUpdate');

Route::get('/uploadSylabus', 'App\Http\Controllers\MentorController@uploadSylabus');






 Route::post('/syllabusUpload', 'App\Http\Controllers\ExcelImportController@syllabusUpload');
 Route::get('/topicListing/{id}', 'App\Http\Controllers\TrainerController@topicListing');
 Route::post('/subtopicUpload', 'App\Http\Controllers\ExcelImportController@subtopicUpload');
 Route::get('/examDeclaration', 'App\Http\Controllers\TrainerController@examDeclaration');
 Route::post('/examDeclaration', 'App\Http\Controllers\TrainerController@examDeclarationSubmit');

 Route::post('/getPackagesByTechnology', 'App\Http\Controllers\TrainerController@getPackagesByTechnology');



 Route::get('/viewExam', 'App\Http\Controllers\TrainerController@viewExam');

 Route::get('/addQuestions', 'App\Http\Controllers\TrainerController@addQuestions');
 Route::post('/getPackage', 'App\Http\Controllers\TrainerController@getPackage')->name('getPackage');
 Route::post('/fetchExamList', 'App\Http\Controllers\TrainerController@fetchExamList');
 Route::post('/save_excelquestions', 'App\Http\Controllers\ExcelImportController@saveExcelQuestions');
 Route::get('/viewQuestions/{id}', 'App\Http\Controllers\TrainerController@viewQuestions');

  //Admin syllabus view
  Route::get('/syllabusview/{pacid}', 'App\Http\Controllers\AdminController@syllabusViewAdmin');
  Route::post('topicActivate', 'App\Http\Controllers\AdminController@topicActivate');
  Route::post('topicDeactivate', 'App\Http\Controllers\AdminController@topicDeactivate');
  Route::post('topicDelete', 'App\Http\Controllers\AdminController@topicDelete');

  Route::post('/activateDeactivesingle', 'App\Http\Controllers\AdminController@activateDeactivesingle');
  Route::post('/activateDeactivemultiple', 'App\Http\Controllers\AdminController@activateDeactivemultiple');


Route::get('/examDeclaration', 'App\Http\Controllers\TrainerController@examDeclaration');
Route::post('/examDeclarationSubmit', 'App\Http\Controllers\TrainerController@examDeclarationSubmit');
Route::post('/getPackagesByTechnology', 'App\Http\Controllers\TrainerController@getPackagesByTechnology')->name('getPackagesByTechnology');
Route::get('/viewExam', 'App\Http\Controllers\TrainerController@viewExam');

Route::get('/addQuestions', 'App\Http\Controllers\TrainerController@addQuestions');
Route::post('/getPackage', 'App\Http\Controllers\TrainerController@getPackage')->name('getPackage');
Route::post('/fetchExamList', 'App\Http\Controllers\TrainerController@fetchExamList');
Route::post('/save_excelquestions', 'App\Http\Controllers\ExcelImportController@saveExcelQuestions');
Route::get('/viewQuestions/{id}', 'App\Http\Controllers\TrainerController@viewQuestions');
Route::post('/updateTopicOrder', 'App\Http\Controllers\TrainerController@updateTopicOrder');

Route::post('/activateDeactivemultipleQues', 'App\Http\Controllers\ExamController@activateDeactivemultipleQues');
Route::post('/activateDeactivemultiple', 'App\Http\Controllers\AdminController@activateDeactivemultiple');









