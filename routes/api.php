<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/email-validation', 'App\Http\Controllers\TalentoDetails@email_validation');
Route::post('/login', 'App\Http\Controllers\TalentoDetails@login');
Route::post('/mark-attendance', 'App\Http\Controllers\TalentoDetails@mark_attendance');
Route::post('/logincheck', 'App\Http\Controllers\TalentoDetails@logincheck');
Route::post('/user-registration', 'App\Http\Controllers\TalentoDetails@user_registration');
Route::post('/gatepass_request', 'App\Http\Controllers\TalentoDetails@gatepass_request');
Route::post('/gatepass_list', 'App\Http\Controllers\TalentoDetails@gatepass_list');
Route::post('/get-details', 'App\Http\Controllers\TalentoDetails@get_details');
Route::post('/email-validation', 'App\Http\Controllers\TalentoDetails@email_validation');
Route::post('/helpcontent', 'App\Http\Controllers\TalentoDetails@helpcontent');
Route::post('/edit-profile', 'App\Http\Controllers\TalentoDetails@edit_profile');
Route::post('/change-password', 'App\Http\Controllers\TalentoDetails@change_password');
Route::post('/mark-attendance', 'App\Http\Controllers\TalentoDetails@mark_attendance');
Route::post('/pass_details', 'App\Http\Controllers\TalentoDetails@pass_details');
Route::post('/forgot_password', 'App\Http\Controllers\TalentoDetails@forgot_password');
Route::post('/student_package', 'App\Http\Controllers\TalentoDetails@student_package');
Route::post('/paymentrequest', 'App\Http\Controllers\TalentoDetails@paymentrequest');
Route::post('/allpayment_request', 'App\Http\Controllers\TalentoDetails@allpayment_request');
Route::post('/receipt', 'App\Http\Controllers\TalentoDetails@receipt');
Route::post('/updateversion', 'App\Http\Controllers\TalentoDetails@updateversion');
Route::post('/getversion', 'App\Http\Controllers\TalentoDetails@getversion');
Route::post('/student_admission', 'App\Http\Controllers\TalentoDetails@student_admission');
Route::post('/admission_details', 'App\Http\Controllers\TalentoDetails@admission_details');
Route::post('/package_details', 'App\Http\Controllers\TalentoDetails@package_details');
Route::post('/userdetails', 'App\Http\Controllers\TalentoDetails@userdetails');
Route::post('/deleteattendance', 'App\Http\Controllers\TalentoDetails@deleteattendance');
Route::post('/tvsection', 'App\Http\Controllers\TalentoDetails@tvsection');
Route::post('/job_register', 'App\Http\Controllers\TalentoDetails@job_register');

Route::post('/jobinfo_view', 'App\Http\Controllers\TalentoDetails@jobinfo_view');
Route::post('/updateJobInfo', 'App\Http\Controllers\TalentoDetails@updateJobInfo');

Route::post('/apiExamlist', 'App\Http\Controllers\ApiexamController@apiExamlist');
Route::post('/apiExamquestions', 'App\Http\Controllers\ApiexamController@apiExamquestions');
Route::post('/apiExamsubmit', 'App\Http\Controllers\ApiexamController@apiExamsubmit');

Route::post('/apiExamresult', 'App\Http\Controllers\ApiexamController@apiExamresult');

Route::post('/generateToken', 'App\Http\Controllers\ApitokenController@generateToken');



