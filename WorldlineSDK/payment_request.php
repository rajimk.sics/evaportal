<?php

namespace App\WorldlineSDK;

class PaymentRequest
{
    public function generateTokenString( $merchantCode, $consumerData)
    {
        $dataString = implode('|', [
            $merchantCode,
            $consumerData['txnId'] ?? '',
            $consumerData['totalAmount'] ?? '',
            $consumerData['accountNo'] ?? '',
            $consumerData['consumerId'] ?? '',
            $consumerData['consumerMobileNo'] ?? '',
            $consumerData['consumerEmailId'] ?? '',
            $consumerData['debitStartDate'] ?? '',
            $consumerData['debitEndDate'] ?? '',
            $consumerData['maxAmount'] ?? '',
            $consumerData['amountType'] ?? '',
            $consumerData['frequency'] ?? '',
            $consumerData['cardNumber'] ?? '',
            $consumerData['expMonth'] ?? '',
            $consumerData['expYear'] ?? '',
            $consumerData['cvvCode'] ?? '',
            $consumerData['salt'] ?? ''
        ]);
        
        return $dataString;
    }

    public function generateHashedToken($tokenString)
    {
        return hash('sha256', $tokenString);
    }
}

